import { FormControl, Validators, FormArray } from '@angular/forms';
import { CRESSectionModel } from '.';

export class CREvaluationSheetModel {
  id: number;
  title: string;
  status: string;
  isRecommendedForApproval: number;
  remarks: string;
  sections: CRESSectionModel[];
  comments:string;

  static attributesLabels = {
    title: 'Title',
    remarks: 'Remarks'
  };

  constructor() { }

  /**
    * Form Validation Rules
    */
  public validationRules?() {
    return {
      id: new FormControl(null, [Validators.required]),
      status: new FormControl('', [Validators.required]),
      isRecommendedForApproval: new FormControl(null),
      remarks: new FormControl(''),
      sections: new FormArray([]),
      questions: new FormArray([]),
    };
  }

}