import { GLOBALS } from '../../app/config/globals';
import { ConfigurationModel } from '../../shared/models';
import { CRApplicant } from '../../public/models';
import { FormControl, Validators, ValidatorFn, FormGroup, AbstractControl } from '@angular/forms';
import { WFStateModel } from '../../settings/models';

export class CampusPostRegistration {
  id?: number;
  fullName?: string;
  cnic?: string;
  mobileNumber?: string;
  email?: string;
  instituteTypeId?: number;
  applicationType: string;
  campus?: string;
  cityId?: number;
  tentativeSessionStart?: Date;
  password?: string;
  confirmPassword?: string;
  cityName?: string;
  applicant?: CRApplicant = new CRApplicant();
  city?: ConfigurationModel = new ConfigurationModel();
  state?: string;
  wfState?: WFStateModel;
  stateId: number;

  createdBy: number;
  updatedBy: number;
  createdAt: Date;
  updatedAt: Date;

  listLabels = {
    fullName: 'Applicant Name',
    email: 'Email',
    applicationType: 'Application Type',
    instituteType: 'Institute Type',
    city: 'City',
    state: 'State',
    filter: 'Filter'
  };

  labels = {
    fullName: 'Full Name',
    cnic: 'CNIC',
    mobileNumber: 'Mobile Number',
    email: 'Email',
    instituteType: 'Institute Type',
    applicationType: 'Application Type',
    cityId: 'City ( Target Campus Location )',
    tentativeSessionStart: 'Tentative Session Start',
    password: 'Password',
    confirmPassword: 'Confirm Password',
    sessionStartDate: 'Session Start Date',
    createdAt: 'Created At',
    state: 'State'
  };

  constructor() { }
}
