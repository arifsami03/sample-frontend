import { FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

export class ProspectusSaleModel {
    id?: number;
    programDetailId?: number;
    fullName: string;
    cnic: string;
    mobileNumber: string;
    email: string;
    shift: boolean;
    prospectusNumber: number;
    price: number;
    createdBy?: number;
    updatedBy?: number;
    createdAt?: Date;
    updatedAt?: Date;

    static attributesLabels = {
        programDetailId: 'Program Detail',
        fullName: 'Full Name',
        cnic: 'CNIC',
        mobileNumber: 'Mobile Number',
        email: 'Email',
        shift: 'Shift',
        prospectusNumber: 'Prospectus No.',
        price: 'Price',
        countryCode:'Country'
    };

    constructor() { }

    /**
         * Form Validation Rules
         */
    public validationRules?() {
        return {
            
            fullName: new FormControl('', [<any>Validators.required, Validators.maxLength(100)]),
            cnic: new FormControl('', [Validators.required, Validators.pattern('([0-9]{5})-([0-9]{7})-([0-9]{1})'), Validators.maxLength(16)]),
            mobileNumber: new FormControl('', [Validators.required, Validators.pattern('([0-9]{3})-([0-9]{7})'), Validators.maxLength(12)]),
            email: new FormControl('', [Validators.required, Validators.email, Validators.maxLength(30)]),
            prospectusNumber: new FormControl('', [<any>Validators.required]),
            price: new FormControl('', [<any>Validators.required]),
            countryCode: new FormControl('', Validators.required),
        };
    }
}
