import { FormControl, Validators, FormArray } from '@angular/forms';
import { CRESQuestionModel } from '.';


export class CRESSectionModel {
  id: number;
  title: string;
  questions: CRESQuestionModel[];

  static attributesLabels = {
    title: 'Title',
  };

  constructor() { }

  /**
    * Form Validation Rules
    */
  public validationRules?() {
    return {
      id: new FormControl(null, [Validators.required]),
      questions: new FormArray([]),
    };
  }
}