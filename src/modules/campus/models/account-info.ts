import {
    FormControl,
    Validators,
    ValidatorFn,
    AbstractControl
} from '@angular/forms';

export class AccountInfoModel {
    id?: number;
    bankId: number;
    branch: string;
    account: string;
    challanType: string;
    programId: number;
    createdBy: number;
    updatedBy: number;
    createdAt: Date;
    updatedAt: Date;

    static attributesLabels = {
        bankId: 'Bank',
        branch: 'Branch',
        account: 'Account',
        challanType: 'Challan Type',
        programId: 'Select Programs'
    };

    constructor() { }

    /**
     * Form Validation Rules
     */
    public validationRules?() {
        return {
            bankId: new FormControl('', [<any>Validators.required]),
            branch: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
            account: new FormControl('', [<any>Validators.required, Validators.maxLength(40)]),
            challanType: new FormControl('', [<any>Validators.required]),
        };
    }
}
