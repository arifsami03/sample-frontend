import {
    FormControl,
    Validators,
    ValidatorFn,
    AbstractControl
} from '@angular/forms';

export class CRMouVerificationModel {
    id?: number;
    CRApplicationId?: number;
    instrumentTypeId: number;
    currentDate: Date;
    instrumentDate: Date;
    instrumentNumber: string;
    bankId: number;
    bankName: string;
    amount: number;
    status: string;
    purpose: string;
    remarks: string;
    account: string;
    createdBy: number;
    updatedBy: number;
    createdAt: Date;
    updatedAt: Date;

    static attributesLabels = {
        instrumentTypeId: 'Instrument Category',
        currentDate: 'Current Date',
        instrumentDate: 'Instrument Date',
        instrumentNumber: 'Instrument Number',
        bankId: 'Bank',
        bankName: 'Branch Name',
        amount: 'Amount',
        status: 'Status',
        purpose: 'Purpose',
        remarks: 'Remarks',
        account: 'Account',
    };

    constructor() { }
    

    /**
     * Form Validation Rules
     */
    public validationRules?() {
        return {
            instrumentTypeId: new FormControl('', [<any>Validators.required]),
            bankId: new FormControl('', [<any>Validators.required]),
            currentDate: new FormControl('', [<any>Validators.required]),
            instrumentDate: new FormControl('', [<any>Validators.required]),
            instrumentNumber: new FormControl('', [<any>Validators.required]),
            bankName: new FormControl('', [<any>Validators.required]),
            amount: new FormControl('', [<any>Validators.required]),
            status: new FormControl('', [<any>Validators.required]),
            account: new FormControl('', [<any>Validators.required]),
            purpose: new FormControl('', [<any>Validators.required, Validators.maxLength(400)]),
            remarks: new FormControl('', [<any>Validators.required, Validators.maxLength(1000)])
        };
    }
}
