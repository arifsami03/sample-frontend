import { FormControl, Validators } from '@angular/forms';

export class CRESQuestionModel {
  id: number;
  question: string;
  countField: number;
  countValue: number;
  remarksField: number;
  remarks: string;
  optionType: string;
  optionsCSV: string;
  options: string[];
  answer: string;

  static attributesLabels = {
    question: 'Question',
    remarks: 'Remarks',
  };

  constructor() { }

  /**
    * Form Validation Rules
    */
  public validationRules?() {
    return {
      id: new FormControl(null, [Validators.required]),
      countValue: new FormControl(null),
      remarks: new FormControl(''),
      answer: new FormControl(''),
    };
  }
}