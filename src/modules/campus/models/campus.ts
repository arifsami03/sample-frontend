import { FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

export class CampusModel {
  id?: number;
  CRApplicationId: number;
  campusId: string;
  codeNumber: string;
  campusName: string;
  title: string;
  email: string;
  mobileNumber: string;
  address: string;
  landLineNumber: string;
  programId: number;
  user: string;
  createdBy: number;
  updatedBy: number;
  createdAt: Date;
  updatedAt: Date;
  academicMonth?: number;
    academicYear?: number;

  static attributesLabels = {
    CRApplicationId: 'Application',
    campusId: 'Campus Id',
    codeNumber: 'Code Number',
    campusName: 'Campus Name',
    email: 'Official Email',
    mobileNumber: 'Mobile Number',
    address: 'Address',
    landLineNumber: 'Land Line Number',
    programId: 'Program',
    user: 'List of Users',
    countryCode: 'Country',
    countryCodeForLandLine: 'Country',
    filter: 'Filter',
    academicYear: 'Academic Year',
    academicMonth: 'Academic Month',
  };
  constructor() { }

  /**
   *
   * @param equalControlName
   */
  public greaterThan?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      let controlMatch = 1;

      return (controlMatch <= control.value && 999999999999 >= control.value) || control.value === null
        ? null
        : {
          equalTo: true
        };
    };
  }
  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      campusId: new FormControl('', [<any>Validators.required]),
      codeNumber: new FormControl('', [<any>Validators.required, Validators.maxLength(12)]),
      campusName: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      email: new FormControl('', [<any>Validators.required, Validators.email]),
      mobileNumber: new FormControl('', [Validators.required, Validators.pattern('([0-9]{3})-([0-9]{7})'), Validators.maxLength(12)]),
      address: new FormControl('', [<any>Validators.required, Validators.maxLength(50)]),
      landLineNumber: new FormControl('', [<any>Validators.required, this.greaterThan('landLineNumber')]),
      programId: new FormControl(''),
      user: new FormControl(''),
      countryCode: new FormControl('', Validators.required),
      countryCodeForLandLine: new FormControl('', Validators.required),
      academicMonth: new FormControl('', [<any>Validators.required]),
      academicYear: new FormControl('', [<any>Validators.required]),
    };
  }
}
