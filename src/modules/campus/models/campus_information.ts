import { FormControl, Validators, ValidatorFn, FormBuilder, AbstractControl } from '@angular/forms';
import { MinLengthValidator } from '@angular/forms/src/directives/validators';
import { CRApplicant } from '../../public/models';

export class CampusInformationModel {
  id?: number;
  campusType: boolean;
  campusName: string;
  instituteTypeId: number;
  educationLevelId: number;
  tentativeSessionStart: Date;
  buildingAvailable: boolean;
  buildingType: string;
  rentAgreementUpTo: string;
  coverdArea: number;
  openArea: number;
  totalArea: number;
  roomsQuantity: number;
  washroomsQuantity: number;
  teachingStaffQuantity: number;
  labsQuantity: number;
  nonTeachingStaffQty: number;
  studentsQuantity: number;
  playGround: boolean;
  swimmingPool: boolean;
  healthClinic: boolean;
  mosque: boolean;
  cafeteria: boolean;
  transport: boolean;
  library: boolean;
  bankBranch: boolean;
  website: string;
  officialEmail: string;
  establishedSince: string;
  noOfCampusTransfer: number;
  applicant?: CRApplicant;
  campusLocation: string;
  campusAdresses: string;

  static attributesLabels = {
    fullName: 'Full Name',
    mobileNumber: 'Mobile Number',
    email: 'Email',
    address: 'Address',
    campusName: 'Name',
    website: 'Website',
    officialEmail: 'Official Email',
    campusLocation: 'Location',
    campusAddress: 'Address'
  };

  constructor() { }

  /**
   *
   * @param equalControlName
   */
  public establishedSinceValidator?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      var d = new Date();
      var currentYear = d.getFullYear();

      let controlMatch = 1850;
      return (controlMatch <= control.value && currentYear >= control.value) || !control.value
        ? null
        : {
          equalTo: true
        };
    };
  }

  /**
   *
   * @param equalControlName
   */
  public greaterThan?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      let controlMatch = 0;

      return controlMatch <= control.value && 9999999999 >= control.value
        ? null
        : {
          equalTo: true
        };
    };
  }
  
  /**
   * Form Validation Rules
   */
  public validationRules?() {
    let fb = new FormBuilder();
    return {
      campusType: [''],
      campusName: new FormControl('', [Validators.maxLength(50)]),
      instituteTypeId: [''],
      educationLevelId: [null],
      tentativeSessionStart: new FormControl(''),
      buildingAvailable: [null],
      buildingType: [null],
      rentAgreementUpTo: new FormControl(''),
      totalArea: ['', [this.greaterThan('totalArea')]],
      coverdArea: ['', [this.greaterThan('coverdArea')]],
      openArea: ['', [this.greaterThan('openArea')]],
      roomsQuantity: ['', [this.greaterThan('roomsQuantity'), Validators.pattern('^(-)?[0-9]*$')]],
      washroomsQuantity: ['', [this.greaterThan('washroomsQuantity'), Validators.pattern('^(-)?[0-9]*$')]],
      teachingStaffQuantity: ['', [this.greaterThan('teachingStaffQuantity'), Validators.pattern('^(-)?[0-9]*$')]],
      labsQuantity: ['', [this.greaterThan('labsQuantity'), Validators.pattern('^(-)?[0-9]*$')]],
      nonTeachingStaffQty: ['', [this.greaterThan('nonTeachingStaffQty'), Validators.pattern('^(-)?[0-9]*$')]],
      studentsQuantity: ['', [this.greaterThan('studentsQuantity'), Validators.pattern('^(-)?[0-9]*$')]],
      playGround: [null],
      swimmingPool: [null],
      healthClinic: [null],
      mosque: [null],
      cafeteria: [null],
      transport: [null],
      library: [null],
      bankBranch: [null],
      website: [''],
      officialEmail: new FormControl('', [Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')]),
      addresses: fb.array([]),
      establishedSince: new FormControl('', [this.establishedSinceValidator('establishedSince')]),
      noOfCampusTransfer: new FormControl('', [this.greaterThan('noOfCampusTransfer')]),
      affiliations: fb.array([]),
      deletedAffiliationIds: fb.array([])
    };
  }
}
