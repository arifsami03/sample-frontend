import { FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

export class CRMouModel {
    id?: number;
    CRApplicationId?: number;
    MOUDate?: Date;
    campusTitle?: string;
    educationLevelId?: number;
    semSession?: number;
    academicYear?: number;
    purpose?: string;
    licenseFeeId?: number;
    createdBy?: number;
    updatedBy?: number;
    createdAt?: Date;
    updatedAt?: Date;

    static attributesLabels = {
        MOUDate: 'MOU Date',
        campusTitle: 'Campus Title',
        educationLevelId: 'Level of Education',
        academicYear: 'Academic Year',
        semSession: 'Semester',
        licenseFeeId: 'Management Fee', // as license fee label is changed to management fee but db table and all its relations are not changed by Arif
        purpose: 'Remarks'
    };

    constructor() { }

    /**
         * Form Validation Rules
         */
    public validationRules?() {
        return {
            MOUDate: new FormControl('', [<any>Validators.required]),
            campusTitle: new FormControl('', [<any>Validators.required, Validators.maxLength(100)]),
            educationLevelId: new FormControl('', [<any>Validators.required]),
            semSession: new FormControl('', [<any>Validators.required]),
            academicYear: new FormControl('', [<any>Validators.required]),
            licenseFeeId: new FormControl('', [<any>Validators.required]),
            purpose: new FormControl('', [<any>Validators.required, Validators.maxLength(400)])
        };
    }
}
