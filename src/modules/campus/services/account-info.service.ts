import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';

import { AccountInfoModel } from '../models';
import { BaseService } from '../../shared/services'; 
@Injectable()
export class AccountInfoService extends BaseService {
    private routeURL: String = 'campus/cpdAccounts';

    constructor(protected http: Http) {
        super(http)
    }

    /**
     * Get All records
     */
    index(): Observable<AccountInfoModel[]> {
        return this.__get(`${this.routeURL}/index`).map(data => {
            return <AccountInfoModel[]>data.json();
        });
    }

    /**
     * Get single record
     */
    find(id: number): Observable<AccountInfoModel> {
        return this.__get(`${this.routeURL}/find/${id}`).map(data => {
            return <AccountInfoModel>data.json();
        });
    }

    getProgramDetailsAccountInfo(id): Observable<any[]> {
        return this.__get(`${this.routeURL}/getProgramDetailsAccountInfo/${id}`).map(data => {
            return <any[]>data.json();
        });
    }

    getProgramDetails(id: number): Observable<any> {
        return this.__get(`${this.routeURL}/getProgramDetails/${id}`).map(data => {
            return <any>data.json();
        });
    }

    /**
     * Delete Record
     */
    delete(id: number) {
        return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
            return data.json();
        });
    }

    /**
     * Create Record
     */
    create(postData: any) {
        return this.__post(`${this.routeURL}/create`, postData).map(data => {
            return data.json();
        });
    }

    /**
     * Update Record
     */
    update(id: number, mou: AccountInfoModel) {
        return this.__put(`${this.routeURL}/update/${id}`, mou).map(data => {
            return data.json();
        });
    }
}
