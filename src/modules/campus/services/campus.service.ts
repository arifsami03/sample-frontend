import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';

import { CampusModel } from '../models';
import { BaseService } from '../../shared/services'; 

@Injectable()
export class CampusService extends BaseService {
    private routeURL: String = 'campus/campus';

    constructor(protected http: Http) {
        super(http)
    }

    /**
     * Get All records
     */
    index() {
        return this.__get(`${this.routeURL}/index`).map(data => {
            return <CampusModel[]>data.json();
        }).catch(this.handleError);
    }

    /**
     * Get All records
     */
    findAttributesList(): Observable<CampusModel[]> {
        return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
            return <CampusModel[]>data.json();
        });
    }

    /**
     * Get All records
     */
    getCampusByProgramDetailId(programDetailId): Observable<CampusModel[]> {
        return this.__get(`${this.routeURL}/getCampusByProgramDetailId/${programDetailId}`).map(data => {
            return <CampusModel[]>data.json();
        });
    }

    /**
     * Get single record
     */
    find(CRApplicationId: number): Observable<any> {
        return this.__get(`${this.routeURL}/find/${CRApplicationId}`).map(data => {
            return <any>data.json();
        });
    }

    /**
     * Get single record
     */
    findCampus(id: number): Observable<any> {
        return this.__get(`${this.routeURL}/findCampus/${id}`).map(data => {
            return <any>data.json();
        });
    }
    /**
     * Get single record
     */
    findAttributesData(id: number): Observable<any> {
        return this.__get(`${this.routeURL}/findAttributesData/${id}`).map(data => {
            return <any>data.json();
        });
    }

    /**
     * Delete Record
     */
    delete(id: number) {
        return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
            return data.json();
        });
    }

    /**
     * Create Record
     */
    create(mou: any) {
        return this.__post(`${this.routeURL}/create`, mou).map(data => {
            return data.json();
        });
    }

    /**
     * Update Record
     */
    update(id: number, mou: any) {
        return this.__put(`${this.routeURL}/update/${id}`, mou).map(data => {
            return data.json();
        });
    }

    /**
 * Find all campueses with status done
 */
    findAllCampuses(): Observable<CampusModel[]> {
        return this.__get(`${this.routeURL}/findAllCampuses`).map(data => {
            return <CampusModel[]>data.json();
        });
    }

    /**
     * update campus status to done
     */
    approveCampus(CRApplicationId: number) {
        return this.__get(`${this.routeURL}/approveCampus/${CRApplicationId}`).map(data => {
            return data.json();
        }).catch(this.handleError);
    }

    /**
     * update campus status to close
     */
    rejectCampus(CRApplicationId: number) {
        return this.__get(`${this.routeURL}/rejectCampus/${CRApplicationId}`).map(data => {
            return data.json();
        }).catch(this.handleError);
    }
}
