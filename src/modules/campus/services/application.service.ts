import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';

import { BaseService } from '../../shared/services'; 
import { CRApplication } from '../../public/models';

@Injectable()
export class ApplicationService extends BaseService {

    private routeURL: String = 'campus/applications';

    constructor(protected http: Http) {
        super(http)
    }

    /**
     * Get All records
     */
    public index(params?): Observable<CRApplication[]> {

        return this.__get(`${this.routeURL}/index`, params).map(data => {
            return <CRApplication[]>data.json();
        });
        
    }

    /**
     * Get single record
     */
    public find(id: number): Observable<CRApplication> {
        return this.__get(`${this.routeURL}/find/${id}`).map(data => {
            return <CRApplication>data.json();
        });
    }

}
