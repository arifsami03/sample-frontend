import 'rxjs/add/operator/map';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { BaseService } from '../../shared/services'; 
import { CampusPreRegistration } from '../../public/models';

@Injectable()
export class CampusPreRegistrationService extends BaseService {
  private _url: String = 'campus/preRegistrations';

  constructor(protected http: Http) {
    super(http)
  }

  index(params?) {
    return this.__get(`${this._url}/index`, params).map(data => {
      return <CampusPreRegistration[]>data.json();
    }).catch(this.handleError);
  }

  find(_id: number) {
    return this.__get(`${this._url}/find/${_id}`)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }
  changeStatus(_id: number, _status: any) {
    return this.__put(`${this._url}/status/${_id}`, _status)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }
  getUserId(id) {
    return this.__get(`${this._url}/findUserId/${id}`)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }
  activeUser(id, item) {
    return this.__put(`${this._url}/activeUser/${id}`, item)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }

  /**
   * Find all workflow states assigned to user
   */
  findAllAssignedWFStates() {
    return this.__get(`${this._url}/findAllAssignedWFStates`).map(data => {
      return <any[]>data.json();
    }).catch(this.handleError);
  }

}
