import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CRAAdditionalInfoService {
  // Observable string sources
  // private craAdditionalInfo$ = new Subject<any>();
  // private data = this.craAdditionalInfo$.asObservable();

  private craAdditionalInfoSource = new Subject<any>();
  craAdditionalInfo$ = this.craAdditionalInfoSource.asObservable();

  craAdditionalInfo(data: any) {
    this.craAdditionalInfoSource.next(data);
  }


  // sendCRAAdditionalInfo(data){

  //   return this.craAdditionalInfo$.next(data)

  // }


  
}
