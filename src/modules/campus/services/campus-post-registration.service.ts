import 'rxjs/add/operator/map';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { CREvaluationSheetModel, CRESSectionModel, CampusPostRegistration } from '../models';

import { environment } from 'environments/environment';
import { BaseService } from '../../shared/services';
import { WFStateModel } from '../../settings/models';
import { Subject } from 'rxjs';

@Injectable()
export class CampusPostRegistrationService extends BaseService {
  private countryName = new Subject<any>();


  private _url: String = 'campus/postRegistrations';

  constructor(protected http: Http) {
    super(http)
  }
  setCountryName(campusName: any) {
    this.countryName.next(campusName);
  }

  getCountryName(): Observable<any> {
    return this.countryName.asObservable();
  }
  index(params?) {
    return this.__get(`${this._url}/index`, params).map(data => {
      return <CampusPostRegistration[]>data.json();
    }).catch(this.handleError);
  }

  /**
   * Find data of campus with campus owner information
   * @param id 
   */
  find(id) {
    return this.__get(`${this._url}/find/${id}`)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }
  /**
   * Find data of campus with campus owner information
   * @param id 
   */
  findRemittanceStatus(crApplicantId : number) {
    return this.__get(`${this._url}/findRemittanceStatus/${crApplicantId}`)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }
  /**
   * Find data of campus with campus owner information
   * @param id 
   */
  findCRAAdditionalInfo(crApplicantId : number) {
    return this.__get(`${this._url}/findCRAAdditionalInfo/${crApplicantId}`)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }



  save(_id: number) {
    return this.__get(`${this._url}/save/${_id}`)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }

  getRegistrationStatus(_id: number) {
    return this.__get(`${this._url}/findStatus/${_id}`)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }


  changeStatus(_id: number, _status: any) {
    return this.__put(`${this._url}/status/${_id}`, _status)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }

  getUserId(id) {
    return this.__get(`${this._url}/findUserId/${id}`)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }

  getState(id) {
    return this.__get(`${this._url}/getState/${id}`)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }

  getSubmitAppData(id) {
    return this.__get(`${this._url}/getSubmitAppData/${id}`)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }

  /**
   * Get data of campus which is forwarded for evaluation
   * @param item 
   */
  forwardForAnalysis(item) {
    return this.__post(`${this._url}/forwardForAnalysis`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Find evaluation sheet for campus with status 'draft'
   * @param id 
   */
  findEvaluationSheet(id: number) {
    return this.__get(`${this._url}/findEvaluationSheet/${id}`)
      .map(data => {
        return <CREvaluationSheetModel>data.json();
      })
      .catch(this.handleError);
  }

  /**
   * Find all evaluation sheets for campus with status 'done'
   * @param id 
   */
  findAllEvaluationSheets(id: number) {
    return this.__get(`${this._url}/findAllEvaluationSheets/${id}`)
      .map(data => {
        return <CREvaluationSheetModel[]>data.json();
      })
      .catch(this.handleError);
  }

  /**
   * Find all evaluation sheets for campus with status 'done'
   * @param id 
   */
  findAllESSections(id: number) {
    return this.__get(`${this._url}/findAllESSections/${id}`)
      .map(data => {
        return <CRESSectionModel[]>data.json();
      })
      .catch(this.handleError);
  }

  updateEvaluationSheet(id: number, item: CREvaluationSheetModel) {
    return this.__put(`${this._url}/updateEvaluationSheet/${id}`, item)
      .map(data => {
        return <CREvaluationSheetModel>data.json();
      })
      .catch(this.handleError);
  }
  updateCommentsOfEvaluationSheet(id: number, item: CREvaluationSheetModel) {
    return this.__put(`${this._url}/updateCommentsOfEvaluationSheet/${id}`, item)
      .map(data => {
        return <CREvaluationSheetModel>data.json();
      })
      .catch(this.handleError);
  }

  updateESStatus(id: number, item) {
    return this.__put(`${this._url}/updateESStatus/${id}`, item)
      .map(data => {
        return <CREvaluationSheetModel>data.json();
      })
      .catch(this.handleError);
  }

  updateCRApplication(id: number, item) {
    return this.__put(`${this._url}/updateCRApplication/${id}`, item)
      .map(data => {
        return <any>data.json();
      })
      .catch(this.handleError);
  }

  /**
   * Find all workflow states assigned to user
   */
  findAllAssignedWFStates() {
    return this.__get(`${this._url}/findAllAssignedWFStates`).map(data => {
      return <any[]>data.json();
    }).catch(this.handleError);
  }

}
