import 'rxjs/add/operator/map';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { BaseService } from '../../shared/services'; 

@Injectable()
export class CRApplicantService extends BaseService {
  private _url: String = 'applicant';

  constructor(protected http: Http) {
    super(http)
  }

  findPersonInfo(userId:number){
    return this.__get(`${this._url}/registration/findPersonalInfo/${userId}`)
    .map(data => {
      return <any[]>data.json();
    })
    .catch(this.handleError);
  }
  getApplicantId(userId:number){
    return this.__get(`${this._url}/registration/getApplicantId/${userId}`)
    .map(data => {
      return <any[]>data.json();
    })
    .catch(this.handleError);
  }
  getApplicantIdByApplicationId(applicationId:number){
    return this.__get(`${this._url}/registration/getApplicantIdByApplicationId/${applicationId}`)
    .map(data => {
      return <any[]>data.json();
    })
    .catch(this.handleError);
  }
  update(id: Number, item) {
    return this.__put(`${this._url}/registration/update/${id}`, item).map(data => {
      return data.json();
    });
  }
  create(item) {
    return this.__post(`${this._url}/registration/create`, item).map(data => {
      return data.json();
    });
  }
}
