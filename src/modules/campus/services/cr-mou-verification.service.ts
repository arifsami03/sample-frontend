import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';

import { CRMouVerificationModel } from '../models';
import { BaseService } from '../../shared/services'; 
@Injectable()
export class CRMouVerificationService extends BaseService {
    private routeURL: String = 'campus/campusMOUVerifications';

    constructor(protected http: Http) {
        super(http)
    }

    /**
     * Get single record
     */
    find(CRApplicationId: number): Observable<CRMouVerificationModel> {
        return this.__get(`${this.routeURL}/find/${CRApplicationId}`).map(data => {
            return <CRMouVerificationModel>data.json();
        });
    }

    /**
     * Create Record
     */
    create(mou: CRMouVerificationModel) {
        return this.__post(`${this.routeURL}/create`, mou).map(data => {
            return data.json();
        });
    }

    /**
     * Update Record
     */
    update(id: number, mou: CRMouVerificationModel) {
        return this.__put(`${this.routeURL}/update/${id}`, mou).map(data => {
            return data.json();
        });
    }

    compareMOUAmounts(applicationId,amount){
        return this.__get(`${this.routeURL}/compareMOUAmounts/${applicationId}/${amount}`).map(data => {
            return data.json();
        });
    }
}
