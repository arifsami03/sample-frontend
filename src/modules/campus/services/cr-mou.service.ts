import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';

import { CRMouModel } from '../models';
import { BaseService } from '../../shared/services'; 
@Injectable()
export class CRMouService extends BaseService {
    private routeURL: String = 'campus/campusMOU';

    constructor(protected http: Http) {
        super(http)
    }

    /**
     * Get All records
     */
    index(): Observable<CRMouModel[]> {
        return this.__get(`${this.routeURL}/index`).map(data => {
            return <CRMouModel[]>data.json();
        });
    }

    /**
     * Get single record
     */
    find(CRApplicationId: number): Observable<CRMouModel> {

        return this.__get(`${this.routeURL}/find/${CRApplicationId}`).map(data => {
            return <CRMouModel>data.json();
        });
    }

    /**
     * Create Record
     */
    create(mou: CRMouModel) {
        return this.__post(`${this.routeURL}/create`, mou).map(data => {
            return data.json();
        });
    }

    /**
     * Update Record
     */
    update(id: number, mou: CRMouModel) {
        return this.__put(`${this.routeURL}/update/${id}`, mou).map(data => {
            return data.json();
        });
    }
}
