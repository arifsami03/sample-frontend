import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';

import { CampusModel } from '../models';
import { BaseService } from '../../shared/services'; 

@Injectable()
export class MapsService extends BaseService {
    private routeURL: String = 'https://maps.googleapis.com/maps/api/geocode/json';

    constructor(protected http: Http) {
        super(http)
    }

    /**
     * Get All records
     */
    getLocation(address: string): Observable<any> {
        // ,{address : 'Lahore' , key : 'AIzaSyAaYDoRGc-X4Q5DPpCBxqrbgx5XOp8YisE'}
        return this.http.get(`${this.routeURL}?address=${address}&key=AIzaSyAaYDoRGc-X4Q5DPpCBxqrbgx5XOp8YisE`).map(data => {
            return <any>data.json();
        });
    }

}
