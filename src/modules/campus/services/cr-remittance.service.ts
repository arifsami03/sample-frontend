import 'rxjs/add/operator/map';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { BaseService } from '../../shared/services'; 

@Injectable()
export class CRRemittanceService extends BaseService {
  private _url: String = 'campus/remittances';

  constructor(protected http: Http) {
    super(http)
  }

  save(_data: any) {
    return this.__post(`${this._url}/save`, _data)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }
  view(_id: number) {
    return this.__get(`${this._url}/view/${_id}`)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }
}
