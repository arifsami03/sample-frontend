import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CRProgramService {
  // Observable string sources
  private campusPrograms = new Subject<any>();

  private educationLevelId = new Subject<any>();


  sendCRProgram(campusProgram: any) {
    this.campusPrograms.next({ campusProgram: campusProgram });
  }

  resetCRProgram() {
    this.campusPrograms.next([]);
  }

  getCRPrograms(): Observable<any> {
    return this.campusPrograms.asObservable();
  }

  setEducationLevelId(educationLevelId: any) {
    this.educationLevelId.next(educationLevelId);
  }

  geteducationLevelId(): Observable<any> {
    return this.educationLevelId.asObservable();
  }

  
}
