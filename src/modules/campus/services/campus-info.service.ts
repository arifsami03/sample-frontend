import 'rxjs/add/operator/map';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { BaseService } from '../../shared/services'; 

@Injectable()
export class CampusInfoService extends BaseService {
  private _url: String = 'campus';

  constructor(protected http: Http) {
    super(http)
  }

  findCampusInfo(userId: number) {
    return this.__get(`${this._url}/campusInfo/findCampusInfo/${userId}`).map(data => {
      return <any[]>data.json();
    }).catch(this.handleError);
  }

  findCampusEducationLevel(userId: number) {
    return this.__get(`${this._url}/campusInfo/findCampusEducationLevel/${userId}`).map(data => {
      return <any[]>data.json();
    }).catch(this.handleError);
  }

  update(id: Number, item) {
    return this.__put(`${this._url}/campusInfo/updateCampusInfo/${id}`, item).map(data => {
      return data.json();
    }).catch(this.handleError);
  }
  create(item) {
    return this.__post(`${this._url}/campusInfo/createCampusInfo`, item).map(data => {
      return data.json();
    }).catch(this.handleError);
  }


}
