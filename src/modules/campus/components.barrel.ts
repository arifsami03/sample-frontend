/**
 * Import angular, material and other related modules here
 */
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatTableModule,
  MatSelectModule,
  MatMenuModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatListModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatTabsModule,
  MatSnackBarModule,
  MatAutocompleteModule,
  MatExpansionModule,
  MatSortModule,
  MatChipsModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TextMaskModule } from 'angular2-text-mask';
import 'hammerjs';
import { AgmCoreModule } from '@agm/core';
/**
 * Import (self created) Services, Components, models etc here.
 */

/**
 * Services
 */
import {
  ApplicationService,
  CampusPreRegistrationService,
  CampusPostRegistrationService,
  CampusInfoService,
  CRRemittanceService,
  CRApplicantService,
  CRProgramService,
  CRMouService,
  CRMouVerificationService,
  CampusService,
  AccountInfoService,
  CRAAdditionalInfoService
} from './services';

import { ManagementFeeService } from '../fee-management/services';
import { EducationLevelService } from '../institute/services';
import { ProgramService, ProgramDetailsService, AcademicCalendarService } from '../academic/services';
import { BankService, WFStateService, EvaluationSheetService } from '../settings/services';
import { DocStateManagerService } from '../shared/services';
import { CampusDefaultUserService } from '../settings/services';
import { InstituteService } from '../institute/services';
import { ConfigurationService } from '../shared/services';
// import { PrintService } from 'modules/layout/services';
import { CampusPreRegistrationFormService } from '../public/services';

/**
 * Components
 */
import {
  FilterComponent,
  ApplicationListComponent,
  PreRegistrationListComponent,
  PreRegistrationViewComponent,
  CampusRegistrationComponent,
  PersonalInformationComponent,
  CampusInfoComponent,
  CreateCampusComponent,
  CRAddressComponent,
  CampusInfrastructureComponent,
  CampusTransferComponent,
  MapComponent,
  RemittanceComponent,
  DashboardComponent,
  PostRegistrationViewComponent,
  SubmitComponent,
  CRProgramDetailsComponent,
  PostRegistrationListComponent,
  MouFormComponent,
  MouListComponent,
  MouVerificationFormComponent,
  MouVerificationListComponent,
  CreateCampusFormComponent,
  CreateCampusListComponent,
  AnalysisViewComponent,
  AnalysisEvaluationSheetViewComponent,
  AnalysisEvaluationSheetListComponent,
  AccountInfoListComponent,
  AccountInfoFormComponent,
  CampusDetailViewComponent,
  MouViewComponent,
  PersonalInfoViewComponent,
  RemittanceViewComponent,
  EditAccountInfoDialogComponent,
  CampusInfoViewComponent,
  CreateCampusViewComponent,
  CampusInfrastructureViewComponent,
  CampusTransferViewComponent,
  CRProgramDetailsViewComponent,
  CRAddressViewComponent,
  CampusListComponent,
  CampusViewComponent,
  EditPostRegistrationComponent,
  AddPreRegistrationComponent,
  ProspectusSaleFormComponent
  
  
} from './components';

/**
 * Export consts here.
 */
export const __IMPORTS = [
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatTableModule,
  MatSelectModule,
  MatMenuModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatListModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatSortModule,
  MatTabsModule,
  FormsModule,
  ReactiveFormsModule,
  MatAutocompleteModule,
  TextMaskModule,
  FlexLayoutModule,
  MatExpansionModule,
  MatChipsModule,
  AgmCoreModule.forRoot({
    apiKey: 'AIzaSyAaYDoRGc-X4Q5DPpCBxqrbgx5XOp8YisE'
  }),
  MatSnackBarModule
];

export const __DECLARATIONS = [
  FilterComponent,
  ApplicationListComponent,
  PreRegistrationListComponent,
  PreRegistrationViewComponent,
  CampusRegistrationComponent,
  PersonalInformationComponent,
  CampusInfoComponent,
  CreateCampusComponent,
  CRAddressComponent,
  MapComponent,
  CampusInfrastructureComponent,
  CampusTransferComponent,
  RemittanceComponent,
  DashboardComponent,
  PostRegistrationViewComponent,
  SubmitComponent,
  CRProgramDetailsComponent,
  PostRegistrationListComponent,
  MouFormComponent,
  MouListComponent,
  MouVerificationFormComponent,
  MouVerificationListComponent,
  CreateCampusFormComponent,
  CreateCampusListComponent,
  AnalysisViewComponent,
  AnalysisEvaluationSheetViewComponent,
  AnalysisEvaluationSheetListComponent,
  AccountInfoListComponent,
  AccountInfoFormComponent,
  CampusDetailViewComponent,
  MouViewComponent,
  PersonalInfoViewComponent,
  RemittanceViewComponent,
  CampusInfoViewComponent,
  CreateCampusViewComponent,
  CampusInfrastructureViewComponent,
  CampusTransferViewComponent,
  CRProgramDetailsViewComponent,
  CRAddressViewComponent,
  EditAccountInfoDialogComponent,
  CampusListComponent,
  CampusViewComponent,
  EditPostRegistrationComponent,
  AddPreRegistrationComponent,
  ProspectusSaleFormComponent

];

export const __PROVIDERS = [
  ApplicationService,
  CampusPreRegistrationService,
  CampusPostRegistrationService,
  CampusInfoService,
  CRRemittanceService,
  CRApplicantService,
  ProgramService,
  BankService,
  ProgramDetailsService,
  WFStateService,
  EducationLevelService,
  CRProgramService,
  CRMouService,
  CRMouVerificationService,
  CampusService,
  AccountInfoService,
  AcademicCalendarService,
  ManagementFeeService,
  DocStateManagerService,
  ConfigurationService,
  CampusDefaultUserService,
  InstituteService,
  EvaluationSheetService,
  CRAAdditionalInfoService

];

export const __ENTRY_COMPONENTS = [MapComponent, EditAccountInfoDialogComponent, AddPreRegistrationComponent];
