export * from './dashboard/dashboard.component';

export * from './map/map.component';

export * from './applications/list/applications-list.component';
export * from './applications/filter/filter.component';
export * from './campus_pre_registration/list/pre-registration-list.component';
export * from './campus_pre_registration/view/pre-registration-view.component';
export * from './campus_pre_registration/list/add/add-pre-registration.component';
export * from './campus_post_registration/campus-registration.component';

export * from './post_reg_forms/personal_information/personal-information.component';
export * from './post_reg_forms/remittance/remittance.component';
export * from './post_reg_forms/campus_info/campus-info.component';
export * from './post_reg_forms/submit/submit.component';
export * from './post_reg_forms/campus_info/create_campus/create-campus.component';
export * from './post_reg_forms/campus_info/campus_address/campus-address.component';
export * from './post_reg_forms/campus_info/campus_infrastructure/campus-infrastructure.component';
export * from './post_reg_forms/campus_info/campus_transfer/campus-transfer.component';
export * from './post_reg_forms/campus_info/campus-program-details/campus-program-details.component';


export * from './campus_post_registration/list/post-registration-list.component';
export * from './campus_post_registration/view/post-registration-view.component';
export * from './campus_post_registration/analysis_view/analysis-view.component';
export * from './campus_post_registration/analysis_view/evaluation_sheet/view/analysis-evaluation-sheet-view.component';
export * from './campus_post_registration/analysis_view/evaluation_sheet/list/analysis-evaluation-sheet-list.component';
export * from './campus_post_registration/edit/edit-post-registration.component';

export * from './mou/form/mou-form.component';
export * from './mou/list/mou-list.component';

export * from './mou-verification/form/mou-verification-form.component';
export * from './mou-verification/form/campus-detail-view/campus-detail-view.component';
export * from './mou-verification/form/mou-view/mou-view.component';
export * from './mou-verification/list/mou-verification-list.component';

export * from './create-campus/form/create-campus-form.component';
export * from './create-campus/list/create-campus-list.component';

export * from './account-info/form/account-info-form.component';
export * from './account-info/form/edit-account-info/edit-account-info-dialog.component';
export * from './account-info/list/account-info-list.component';


export * from './campus_post_registration/view/personal-info-view/personal-info-view.component';
export * from './campus_post_registration/view/remittance-view/remittance-view.component';

export * from './campus_post_registration/view/campus-info-view/campus-info-view.component';
export * from './campus_post_registration/view/campus-info-view/create-campus-view/create-campus-view.component';
export * from './campus_post_registration/view/campus-info-view/campus-address-view/campus-address-view.component';
export * from './campus_post_registration/view/campus-info-view/campus-infrastructure-view/campus-infrastructure-view.component';
export * from './campus_post_registration/view/campus-info-view/campus-transfer-view/campus-transfer-view.component';
export * from './campus_post_registration/view/campus-info-view/campus-program-details-view/campus-program-details-view.component';

export * from './campus/list/campus-list.component';
export * from './campus/view/campus-view.component';


export * from './prospectus-sale/form/prospectus-sale-form.component';