import { Component, ViewChild, OnInit } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatDialog,
  MatSort
} from '@angular/material';
import { Router } from '@angular/router';

import { LayoutService } from '../../../../layout/services';
import { CampusService } from '../../../services';

import { ConfirmDialogComponent } from '../../../../shared/components';
import { CampusModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'campus-create-campus-list',
  templateUrl: './create-campus-list.component.html',
  styleUrls: ['./create-campus-list.component.css']
})
export class CreateCampusListComponent implements OnInit {
  public loaded: boolean = true;
  public data: CampusModel[] = [new CampusModel()];
  public attrLabels = CampusModel.attributesLabels;

  displayedColumns = ['campusId', 'codeNumber', 'campusName', 'email', 'mobileNumber', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public success: Boolean;

  constructor(
    private CampusService: CampusService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Create Campus' });
    // this.getRecords();
  }

  /**
   * Get all records
   */
  // getRecords(): void {
  //   this.courseService.index().subscribe(
  //     response => {
  //       this.data = response;
  //       this.dataSource = new MatTableDataSource<CourseModel>(this.data);
  //       this.dataSource.paginator = this.paginator;
  //       this.dataSource.sort = this.sort;
  //     },
  //     error => {
  //       console.log(error);
  //       this.loaded = true;
  //     },
  //     () => {
  //       this.loaded = true;
  //     }

  //   );
  // }

  /**
   * Search in Grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete data against the id given
   */
  delete(id: number) {
    //   // Confirm dialog
    //   const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
    //     width: GLOBALS.deleteDialog.width,
    //     data: { message: GLOBALS.deleteDialog.message }
    //   });
    //   dialogRef.afterClosed().subscribe((accept: boolean) => {
    //     if (accept) {
    //       this.loaded = false;
    //       this.courseService.delete(id).subscribe(response => {
    //         this.layoutService.flashMsg({ msg: 'Course has been deleted.', msgType: 'success' });
    //         this.getRecords();
    //       },
    //         error => {
    //           this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
    //           console.log(error);
    //           this.loaded = true;
    //         },
    //         () => {
    //           //TODO
    //           this.getRecords();
    //           this.loaded = true;
    //         }
    //       );
    //     }
    //   });
  }
}
