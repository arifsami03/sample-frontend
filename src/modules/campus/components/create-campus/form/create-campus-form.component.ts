import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { PageAnimation } from '../../../../shared/helper';
import * as _ from 'lodash'

import { CampusService } from '../../../services';
import { LayoutService } from '../../../../layout/services';

import { ConfirmDialogComponent } from '../../../../shared/components';
import { CampusModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';
import { DocStateManagerService } from '../../../../shared/services/doc-state-manager.service';
import { CRMouService } from '../../../services/cr-mou.service';
import { AcademicCalendarService } from '../../../../academic/services';
import { ManagementFeeModel } from '../../../../fee-management/models';
import { EducationLevelModel } from '../../../../institute/models';
import { AcademicCalendarModel } from '../../../../academic/models';
import { ProgramService, ProgramDetailsService } from '../../../../academic/services';
import { InstituteService } from '../../../../institute/services';
import { CampusDefaultUserService } from '../../../../settings/services';
import { CampusDefaultUserModel } from '../../../../settings/models';
import { CRProgramService } from '../../../services';
import { Subscription } from 'rxjs/Subscription';
import { ProgramModel, ProgramDetailsModel } from '../../../../academic/models';
import { ConfigurationService } from '../../../../shared/services';
import { ConfigurationModel } from '../../../../shared/models';



// for Mobile Number
interface ICountryOption {
  abbreviation: string;
  countryCode: string;
}

@Component({
  selector: 'campus-create-campus-form',
  templateUrl: './create-campus-form.component.html',
  styleUrls: ['./create-campus-form.component.css'],
  animations: [PageAnimation]
})
export class CreateCampusFormComponent implements OnInit {

  @Input() CRApplicationId: number;

  public pageState = 'active';
  public loaded: boolean = false;
  public boxLoaded = false;
  public pageActions = GLOBALS.pageActions;
  public error: Boolean;
  public isRecordExists: Boolean;
  public fg: FormGroup;
  public userList = new FormControl();
  public pageAct: string;
  public selectedButton: string = 'createCampus';
  // public stateId: number;
  public createCampusModel: CampusModel;
  public academicCalendars = [new AcademicCalendarModel()];


  public componentLabels = CampusModel.attributesLabels;

  public programs: ProgramModel[] = [new ProgramModel()];
  public programDetails: ProgramDetailsModel[] = [new ProgramDetailsModel()];
  public programsWithProgramDetails = [];
  public campusDefaultUserList: CampusDefaultUserModel[];

  public selected = [];
  public domain = '';

  public addButton: number = 0;
  public multiple: any = [];
  public campusPrograms: any = [];

  public subscription: Subscription;
  public educationLevelId: number;


  // for Mobile Number
  public countriesOptions: ICountryOption[] = [];

  public academicMonths = GLOBALS.MONTHS;
  public academicYears: number[] = GLOBALS.academicYears;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private campusService: CampusService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private configurationService: ConfigurationService,
    private docStateManagerService: DocStateManagerService,
    private mouService: CRMouService,
    private academicCalendarService: AcademicCalendarService,
    private campusDefaultUserService: CampusDefaultUserService,
    private instituteService: InstituteService,
    private programService: ProgramService,
    private programDetailService: ProgramDetailsService,
    private campusProgramService: CRProgramService
  ) {
    this.resetValues();
    this.pageAct = activatedRoute.snapshot.data['act'];
  }


  users = ['Executive', 'Coordinator', 'Principal', 'Finance Represantative'];
  proId: number;
  public mobileMask = GLOBALS.masks.mobile;
  /**
   * ngOnInit
   */
  ngOnInit() {
    this.layoutService.setPageTitle({ title: 'Create Campus' });

    this.getAcademicYear();
    this.initializePage();
    this.findCountryAttrbutesList();
    this.getListOfCampusDefaultUsers();
    this.getDomainName();

  }
  /**
   * Initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new CampusModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      // this.loaded = true;
      this.getData();

    }
  }

  resetValues() {
    this.selected = [];
    this.programsWithProgramDetails = [];
    this.programs = [];
    this.campusPrograms = [];
    this.programDetails = [];
    this.multiple = [];
    this.addButton = 0;
  }

  public editUserIds = []

  /**
   * Get Data
   */
  private getData() {

    return new Promise((resolve, reject) => {
      this.campusService.find(this.CRApplicationId).subscribe(
        response => {
          if (response.campus) {
            this.isRecordExists = true;
            this.createCampusModel = response.campus;
            this.fg.patchValue(this.createCampusModel);

            for (let i = 0; i < response.user.length; i++) {
              this.editUserIds.push(response.user[i]['CDUId'])
              var res = response.user[i]['username'].split(".");
              this.selectedUser.push({
                CDUId: response.user[i]['CDUId'],
                id: response.user[i]['id'],
                username: res[0],
                roleId: '',
                //  + '.' + this.createCampusModel.codeNumber + '@' + this.domain,
                password: '$2b$10$6QytWBbOTMhzUF9Kppz3guIxXM0dbelPvGoAOKkwJnr.yKRGNEi2i',
                isActive: false,
                portal: GLOBALS.PORTAL.CAMPUS

              })

            }
            this.patchMobileNumber();
            this.patchlandLineNumber();
            this.loaded = true;
            this.getCampusData();
            response.cammpusProgramDetails.forEach(element => {

              let programId = element['programId'];

              let programs = [];
              element.programDetails.forEach(programDetail => {
                programs.push(programDetail.programDetailId);
              });

              this.selected.push({ programId: programId, programs: programs });

            });
            //this.getProgramsWithProgramDetails(this.educationLevelId);
            resolve(response);

          }
          else {
            this.isRecordExists = false;
            this.getCampusData();

          }
        },

        error => {
          console.log(error);
          this.loaded = true;
          reject(error);
        }, () => {
          this.loaded = true;
        }
      );
    })
  }

  /**
   * Initialize Create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Create Campus' });
    this.createCampusModel = new CampusModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * Function to get data from CRMOU
   */
  getCampusData() {
    this.mouService.find(this.CRApplicationId).subscribe(
      response => {
        if (response) {
          this.educationLevelId = response.educationLevelId;
          this.fg.patchValue({
            campusName: response.campusTitle,

          });
          this.getProgramsWithProgramDetails(this.educationLevelId);
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }

  getAcademicYear() {
    this.academicCalendarService.findAttributesList().subscribe(
      response => {
        this.academicCalendars = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      }, () => {

      }
    );
  }



  /**
   * Add or Update when save button is clicked
   */
  public saveData(item: CampusModel) {

    this.createUserArray(item);
    let tempArr = [];
    this.selectedUser.forEach(element => {
      tempArr.push(element['CDUId']);


    });
    this.editUserIds=_.difference(this.editUserIds, tempArr);

    this.boxLoaded = true;

    item.CRApplicationId = this.CRApplicationId;
    let postData = {};

    item.mobileNumber = item['countryCode'] + '-' + item.mobileNumber;
    item.landLineNumber = item['countryCodeForLandLine'] + '-' + item.landLineNumber;
    
    postData = {
      campus: item,
      campusProgram: this.campusPrograms,
      users: this.selectedUser,
      deletedUserIds:this.editUserIds
    }
    if (this.isRecordExists) {

      item.id = this.createCampusModel.id;

      postData = {
        campus: item,
        campusProgram: this.campusPrograms,
        users: this.selectedUser,
        deletedUserIds:this.editUserIds
      }

    }
    else {

      postData = {
        campus: item,
        campusProgram: this.campusPrograms,
        users: this.selectedUser,
        deletedUserIds:this.editUserIds
      }
    }

    if (!this.isRecordExists) {

      this.campusService.create(postData).subscribe(
        response => {
          if (!response.campusId && !response.codeNumber) {
            this.boxLoaded = false;
            this.layoutService.flashMsg({ msg: 'Campus has been created.', msgType: 'success' });
            this.resetValues();
            this.getData();
            // this.router.navigate([`/campus/post-registration/forward-for-create-mou/${this.CRApplicationId}`]);
          }
          else {
            this.boxLoaded = false;
            if (response.campusId) {
              this.fg.controls['campusId'].setErrors({
                invalid: true
              })
            }
            if (response.codeNumber) {
              this.fg.controls['codeNumber'].setErrors({
                invalid: true
              })
            }
          }
        },
        error => {
          this.boxLoaded = false;
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        }, () => { }
      );
    } else if (this.isRecordExists) {
      this.campusService.update(this.createCampusModel.id, postData).subscribe(
        response => {
          if (!response.campusId && !response.codeNumber) {
            this.boxLoaded = false;
            this.layoutService.flashMsg({ msg: 'Campus has been updated.', msgType: 'success' });
            this.resetValues();
            this.getData();
            // this.router.navigate([`/campus/post-registration/forward-for-create-mou/${this.CRApplicationId}`]);
          }
          else {
            this.boxLoaded = false;
            if (response.campusId) {
              this.fg.controls['campusId'].setErrors({
                invalid: true
              })
            }
            if (response.codeNumber) {
              this.fg.controls['codeNumber'].setErrors({
                invalid: true
              })
            }
          }

        },
        error => {
          this.boxLoaded = false;
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        }, () => {
        }
      );
    }
  }


  tabChange(type: string) {
    this.selectedButton = type;

    if (type == 'createCampus') {
      this.ngOnInit();
      this.resetValues();
    }
  }

  getProgramsWithProgramDetails(educationLevelId: number) {
    this.programService.getProgramsWithProgramDetails(educationLevelId).subscribe(res => {
      this.programsWithProgramDetails = res;
      this.getProgramList(educationLevelId);
    });
  }
  getProgramList(educationLevelId: number) {
    let selectedPrograms = [];

    this.selected.forEach(element => {
      selectedPrograms.push(element.programId);
    });

    this.programService.getAllPrograms(educationLevelId, selectedPrograms).subscribe(
      response => {
        this.programs = response;
        if (this.selected.length == 0) {
          this.multiple.push({ program: [], programId: '', programDetails: [] });
          for (var i = 0; i < this.programs.length; i++) {
            this.multiple[this.addButton]['program'].push({
              id: this.programs[i].id,
              name: this.programs[i].name
            });
          }
        } else {
          for (var i = 0; i < this.programsWithProgramDetails.length; i++) {
            let item = this.selected.find(element => {
              return element.programId == this.programsWithProgramDetails[i].programId;
            })
            if (item) {
              this.multiple.push({ program: [], programId: '', programDetails: [] });
              this.multiple[this.addButton][
                'programId'
              ] = this.programsWithProgramDetails[i].programId;
              for (var j = 0; j < this.programsWithProgramDetails.length; j++) {
                this.multiple[this.addButton]['program'].push({
                  id: this.programsWithProgramDetails[j].programId,
                  name: this.programsWithProgramDetails[j].programName
                });
              }
              for (var k = 0; k < this.programsWithProgramDetails[i]['programDetails'].length; k++) {
                var checked = true;

                if (
                  item.programs.includes(
                    this.programsWithProgramDetails[i]['programDetails'][k].id
                  )
                ) {
                  this.editChange(true, this.programsWithProgramDetails[i].programId, this.programsWithProgramDetails[i]['programDetails'][k].id);
                } else {
                  checked = false;
                  this.editChange(
                    false,
                    this.programsWithProgramDetails[i].programId,
                    this.programsWithProgramDetails[i]['programDetails'][k].id
                  );
                }
                this.multiple[this.addButton]['programDetails'].push({
                  id: this.programsWithProgramDetails[i]['programDetails'][k].id,
                  name: this.programsWithProgramDetails[i]['programDetails'][k].name,
                  checked: checked
                });
              }
              this.addButton = this.addButton + 1;
            }
          }

          if (
            this.programsWithProgramDetails.length != this.selected.length
          ) {
            this.multiple.push({ program: [], programId: '', programDetails: [] });
            for (var i = 0; i < this.programs.length; i++) {
              this.multiple[this.addButton]['program'].push({
                id: this.programs[i].id,
                name: this.programs[i].name
              });
            }
          }
        }
      },
      error => console.log(error),
      () => {
        this.loaded = true;
      }
    );
    return this.programs;
  }

  programChange(event) {
    let programId = event.value;
    this.selected.push({ programId: programId, programs: [] });
    this.programDetailService.getProgramProgramDetails(event.value).subscribe(
      response => {
        this.programDetails = response;
        for (var i = 0; i < this.programDetails.length; i++) {
          this.multiple[this.addButton]['programId'] = event.value;
          this.multiple[this.addButton]['programDetails'].push({
            id: this.programDetails[i].id,
            name: this.programDetails[i].name
          });
        }
        this.addMultiple(event.value);
      },
      error => console.log(error),
      () => { }
    );
  }

  addMultiple(programId) {
    this.addButton = this.addButton + 1;
    let selectedPrograms = [];

    this.selected.forEach(element => {
      selectedPrograms.push(element.programId);
    });
    this.programService.getAllPrograms(this.educationLevelId, selectedPrograms).subscribe(
      response => {
        this.programs = response;
        if (this.programs.length != 0) {
          this.multiple.push({ program: [], programId: '', programDetails: [] });
          for (var i = 0; i < this.programs.length; i++) {
            this.multiple[this.addButton]['program'].push({
              id: this.programs[i].id,
              name: this.programs[i].name
            });
          }
        }
      },
      error => console.log(error),
      () => {

      }
    );
  }
  deletePorgram(programId) {
    this.addButton = this.addButton - 1;
    for (var i = 0; i < this.multiple.length; i++) {
      if (this.multiple[i].programId == programId) {
        this.multiple.splice(i, 1);
      }
    }
    for (var i = 0; i < this.selected.length; i++) {
      if (this.selected[i]['programId'] == programId) {
        this.selected.splice(i, 1);
      }
    }
    for (var j = 0; j < this.campusPrograms.length; j++) {
      if (this.campusPrograms[j]['programId'] == programId) {
        this.campusPrograms.splice(j, 1);
      }
    }
    let selectedPrograms = [];

    this.selected.forEach(element => {
      selectedPrograms.push(element.programId);
    });

    this.programService.getAllPrograms(this.educationLevelId, selectedPrograms).subscribe(
      response => {
        this.programs = response;
        if (this.programs.length != 0) {
          if (this.multiple[this.multiple.length - 1]['programId'] == '') {
            this.multiple[this.multiple.length - 1]['program'] = [];
            for (var i = 0; i < this.programs.length; i++) {
              this.multiple[this.addButton]['program'].push({
                id: this.programs[i].id,
                name: this.programs[i].name
              });
            }
          } else {
            this.multiple.push({ program: [], programId: '', programDetails: [] });
            for (var i = 0; i < this.programs.length; i++) {
              this.multiple[this.addButton]['program'].push({
                id: this.programs[i].id,
                name: this.programs[i].name
              });
            }
          }
        }
      },
      error => console.log(error),
      () => { }
    );
  }
  checkChange(event, programId, proId) {
    var trueCondition = true;
    if (event.checked == true) {
      for (var i = 0; i < this.campusPrograms.length; i++) {
        if (this.campusPrograms[i]['programId'] == programId) {
          this.campusPrograms[i]['programDetails'].push(proId);
          trueCondition = false;
        }
      }
      if (trueCondition) {
        this.campusPrograms.push({
          programId: programId,
          programDetails: []
        });
        for (var i = 0; i < this.campusPrograms.length; i++) {
          if (this.campusPrograms[i]['programId'] == programId) {
            this.campusPrograms[i]['programDetails'].push(proId);
          }
        }
      }
    } else {
      for (var i = 0; i < this.campusPrograms.length; i++) {
        if (this.campusPrograms[i]['programId'] == programId) {
          for (var j = 0; j < this.campusPrograms[i]['programDetails'].length; j++) {
            if (this.campusPrograms[i]['programDetails'][j] == proId) {
              this.campusPrograms[i]['programDetails'].splice(j, 1);
            }
          }
          if (this.campusPrograms[i]['programDetails'].length != 0) {
          } else {
            this.campusPrograms.splice(i, 1);
          }
        }
      }
    }
    //this.campusProgramService.sendCRProgram(this.campusPrograms);
  }

  editChange(event, programId, proId) {
    var trueCondition = true;
    if (event == true) {
      for (var i = 0; i < this.campusPrograms.length; i++) {
        if (this.campusPrograms[i]['programId'] == programId) {
          this.campusPrograms[i]['programDetails'].push(proId);
          trueCondition = false;
        }
      }
      if (trueCondition) {
        this.campusPrograms.push({
          programId: programId,
          programDetails: []
        });
        for (var i = 0; i < this.campusPrograms.length; i++) {
          if (this.campusPrograms[i]['programId'] == programId) {
            this.campusPrograms[i]['programDetails'].push(proId);
          }
        }
      }
    } else {
      for (var i = 0; i < this.campusPrograms.length; i++) {
        if (this.campusPrograms[i]['programId'] == programId) {
          if (this.campusPrograms[i]['programDetails'].length != 0) {
            for (
              var j = 0;
              j < this.campusPrograms[i]['programDetails'].length;
              j++
            ) {
              if (this.campusPrograms[i]['programDetails'][j] == proId) {
                this.campusPrograms[i]['programDetails'].splice(j, 1);
              }
            }
          } else {
            this.campusPrograms.splice(i, 1);
          }
        }
      }
    }
    //this.campusProgramService.sendCRProgram(this.campusPrograms);
  }
  // For Mobile Number
  // Getting the countries from configuratiuon and then pushing into the array of object
  findCountryAttrbutesList() {
    this.configurationService.findAttributesList('country').subscribe(
      response => {
        // iterate countries
        response.forEach(country => {
          let abbr;
          let cCode;
          // iterate countries config options
          country.configurationOptions.forEach(configurationOption => {
            // check if option is abbreviation
            if (configurationOption.key === 'abbreviation') {
              abbr = configurationOption.value
              // check if option is country code
            } else if (configurationOption.key === 'countryCode') {
              cCode = configurationOption.value
            }
            // if both exists then push into the array
            if (abbr && cCode) {

              this.countriesOptions.push({ abbreviation: abbr, countryCode: cCode })
            }
          });
        });
        this.selectDefaultCountry(); 
      },
      error => console.log(error),
      () => { }
    );
  }
  patchMobileNumber() {
    // for first mobile number
    let mobileNumber: string = this.createCampusModel.mobileNumber;
    let countryCode: string = mobileNumber.substring(0, mobileNumber.indexOf('-'));
    let mobileNumberSecondPart: string = mobileNumber.substring(mobileNumber.indexOf('-') + 1, mobileNumber.length);

    this.fg.get('countryCode').patchValue(countryCode);
    this.fg.get('mobileNumber').patchValue(mobileNumberSecondPart);


  }
  patchlandLineNumber() {
    // for first land line number
    let landLineNumber: string = this.createCampusModel.landLineNumber;
    let countryCode: string = landLineNumber.substring(0, landLineNumber.indexOf('-'));
    let landLineNumberSecondPart: string = landLineNumber.substring(landLineNumber.indexOf('-') + 1, landLineNumber.length);

    this.fg.get('countryCodeForLandLine').patchValue(countryCode);
    this.fg.get('landLineNumber').patchValue(landLineNumberSecondPart);
  }
  onChangeStateEvent(item) {
    if (item) {
      this.router.navigate([`/campus/post-registrations`]);
    }
  }


  avoidSpace(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 32) {
      return false;
    }
    return true;

  }

  getListOfCampusDefaultUsers() {
    this.campusDefaultUserService.findAttributesList().subscribe(
      response => {
        this.campusDefaultUserList = response;

      },
      error => console.log(error),
      () => { }
    );
  }
  public selectedUser = [];

  userCheckChange(event, user: CampusDefaultUserModel) {


    if (event.checked) {
      this.selectedUser.push({
        CDUId: user.id,
        roleId: user.roleId,
        username: user.abbreviation,
        //  + '.' + this.createCampusModel.codeNumber + '@' + this.domain,
        password: '$2b$10$6QytWBbOTMhzUF9Kppz3guIxXM0dbelPvGoAOKkwJnr.yKRGNEi2i',
        isActive: false,
        portal: GLOBALS.PORTAL.CAMPUS

      })
    }
    else {
      for (let i = 0; i < this.selectedUser.length; i++) {

        if (user.id == this.selectedUser[i].CDUId) {
          this.selectedUser.splice(i, 1);
        }

      }
    }
  }

  getHostName(url) {
    var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
    if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
      return match[2];
    }
    else {
      return null;
    }
  }

  getDomainName() {
    this.instituteService.getAll().subscribe(
      response => {
        this.domain = 'http://' + response.website;
        this.domain = this.getHostName(this.domain);

      },
      error => console.log(error),
      () => { }
    );
  }

  createUserArray(item) {
    for (let i = 0; i < this.selectedUser.length; i++) {

      var res = this.selectedUser[i]['username'].split('.');

      this.selectedUser[i]['username'] = res[0] + '.' + item.codeNumber + '@' + this.domain

    }

  }

  checkedTrueOrNot(id) {
    if (this.editUserIds.includes(id)) {
      return true;
    }
    else {
      return false;
    }

  }
  selectDefaultCountry() {
    let pak: ICountryOption;
    this.countriesOptions.forEach(country => {
      if (country.abbreviation === 'PAK' || country.countryCode === '92' || country.countryCode === '092' || country.countryCode === '+92') {
        pak = country;
      }
    });
    if (pak) {
      this.fg.get('countryCode').setValue(pak.countryCode);
      this.fg.get('countryCodeForLandLine').setValue(pak.countryCode);
    }
  }

}
