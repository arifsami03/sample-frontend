import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { PageAnimation } from '../../../../shared/helper';


import { CampusService } from '../../../services';
import { LayoutService } from '../../../../layout/services';

import { ConfirmDialogComponent } from '../../../../shared/components';
import { CampusModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';
import { DocStateManagerService } from '../../../../shared/services/doc-state-manager.service';
import { CRMouService } from '../../../services/cr-mou.service';
import { AcademicCalendarService } from '../../../../academic/services';
import { ManagementFeeModel } from '../../../../fee-management/models';
import { EducationLevelModel } from '../../../../institute/models';
import { AcademicCalendarModel } from '../../../../academic/models';
import { ProgramService, ProgramDetailsService } from '../../../../academic/services';
import { CRProgramService } from '../../../services';
import { Subscription } from 'rxjs/Subscription';
import { ProgramModel, ProgramDetailsModel } from '../../../../academic/models';
import { ConfigurationService } from '../../../../shared/services';



// for Mobile Number
interface ICountryOption {
  abbreviation: string;
  countryCode: string;
}

@Component({
  selector: 'campus-campus-view',
  templateUrl: './campus-view.component.html',
  styleUrls: ['./campus-view.component.css'],
  animations: [PageAnimation]
})
export class CampusViewComponent implements OnInit {

  public pageState = 'active';
  public loaded: boolean = false;
  public boxLoaded = true;
  public pageActions = GLOBALS.pageActions;
  public error: Boolean;
  public isRecordExists: Boolean;
  public fg: FormGroup;
  public pageAct: string;
  public selectedButton: string = 'createCampus';
  public stateId: number;
  public createCampusModel: CampusModel;
  public CRApplicationId: number;
  public id: number;
  public academicCalendars = [new AcademicCalendarModel()];


  public componentLabels = CampusModel.attributesLabels;

  public programs: ProgramModel[] = [new ProgramModel()];
  public programDetails: ProgramDetailsModel[] = [new ProgramDetailsModel()];
  public programsWithProgramDetails = [];

  public selected = [];

  public addButton: number = 0;
  public multiple: any = [];
  public campusPrograms: any = [];

  public subscription: Subscription;
  public educationLevelId: number;


  // for Mobile Number
  public countriesOptions: ICountryOption[] = [];


  public academicMonths = GLOBALS.MONTHS;
  public academicYears: number[] = GLOBALS.academicYears;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private campusService: CampusService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private configurationService: ConfigurationService,
    private docStateManagerService: DocStateManagerService,
    private mouService: CRMouService,
    private academicCalendarService: AcademicCalendarService,
    private programService: ProgramService,
    private programDetailService: ProgramDetailsService,
    private campusProgramService: CRProgramService
  ) {
    this.resetValues();
    this.pageAct = activatedRoute.snapshot.data['act'];

  }


  users = ['Executive', 'Coordinator', 'Principal', 'Finance Represantative'];
  proId: number;
  public mobileMask = GLOBALS.masks.mobile;
  /**
   * ngOnInit
   */
  ngOnInit() {
    this.fg = this.fb.group(new CampusModel().validationRules());
    this.activatedRoute.params.subscribe(params => {
      this.id = +params['id'];
      this.getData();
    });

    this.layoutService.setPageTitle({ title: 'Campus Detail' });
    this.getAcademicYear();
    this.findCountryAttrbutesList();

  }

  resetValues() {
    this.selected = [];
    this.programsWithProgramDetails = [];
    this.programs = [];
    this.campusPrograms = [];
    this.programDetails = [];
    this.multiple = [];
    this.addButton = 0;
  }

  /**
   * Get Data
   */
  private getData() {

    return new Promise((resolve, reject) => {
      this.campusService.findCampus(this.id).subscribe(
        response => {
          if (response.campus) {
            this.CRApplicationId = response.campus.CRApplicationId;
            this.isRecordExists = true;
            this.createCampusModel = response.campus;
            this.fg.patchValue(this.createCampusModel);
            this.patchMobileNumber();
            this.patchlandLineNumber();
            this.loaded = true;
            this.getCampusData();
            response.cammpusProgramDetails.forEach(element => {

              let programId = element['programId'];

              let programs = [];
              element.programDetails.forEach(programDetail => {
                programs.push(programDetail.programDetailId);
              });

              this.selected.push({ programId: programId, programs: programs });

            });
            //this.getProgramsWithProgramDetails(this.educationLevelId);
            resolve(response);

          }
          else {
            this.isRecordExists = false;
            this.getCampusData();

          }
        },

        error => {
          console.log(error);
          this.loaded = true;
          reject(error);
        }, () => {
          this.fg.disable();
          this.loaded = true;
        }
      );
    })
  }

  /**
   * Function to get data from CRMOU
   */
  getCampusData() {
    this.mouService.find(this.CRApplicationId).subscribe(
      response => {
        if (response) {
          this.educationLevelId = response.educationLevelId;
          this.fg.patchValue({
            campusName: response.campusTitle,

          });
          this.getProgramsWithProgramDetails(this.educationLevelId);
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }

  getAcademicYear() {
    this.academicCalendarService.findAttributesList().subscribe(
      response => {
        this.academicCalendars = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      }, () => {

      }
    );
  }



  getProgramsWithProgramDetails(educationLevelId: number) {
    this.programService.getProgramsWithProgramDetails(educationLevelId).subscribe(res => {
      this.programsWithProgramDetails = res;
      this.getProgramList(educationLevelId);
    });
  }
  getProgramList(educationLevelId: number) {
    let selectedPrograms = [];

    this.selected.forEach(element => {
      selectedPrograms.push(element.programId);
    });

    this.programService.getAllPrograms(educationLevelId, selectedPrograms).subscribe(
      response => {
        this.programs = response;
        if (this.selected.length == 0) {
          this.multiple.push({ program: [], programId: '', programDetails: [] });
          for (var i = 0; i < this.programs.length; i++) {
            this.multiple[this.addButton]['program'].push({
              id: this.programs[i].id,
              name: this.programs[i].name
            });
          }
        } else {
          for (var i = 0; i < this.programsWithProgramDetails.length; i++) {
            let item = this.selected.find(element => {
              return element.programId == this.programsWithProgramDetails[i].programId;
            })
            if (item) {
              this.multiple.push({ program: [], programId: '', programDetails: [] });
              this.multiple[this.addButton][
                'programId'
              ] = this.programsWithProgramDetails[i].programId;
              for (var j = 0; j < this.programsWithProgramDetails.length; j++) {
                this.multiple[this.addButton]['program'].push({
                  id: this.programsWithProgramDetails[j].programId,
                  name: this.programsWithProgramDetails[j].programName
                });
              }
              for (var k = 0; k < this.programsWithProgramDetails[i]['programDetails'].length; k++) {
                var checked = true;

                if (
                  item.programs.includes(
                    this.programsWithProgramDetails[i]['programDetails'][k].id
                  )
                ) {
                  this.editChange(true, this.programsWithProgramDetails[i].programId, this.programsWithProgramDetails[i]['programDetails'][k].id);
                } else {
                  checked = false;
                  this.editChange(
                    false,
                    this.programsWithProgramDetails[i].programId,
                    this.programsWithProgramDetails[i]['programDetails'][k].id
                  );
                }
                this.multiple[this.addButton]['programDetails'].push({
                  id: this.programsWithProgramDetails[i]['programDetails'][k].id,
                  name: this.programsWithProgramDetails[i]['programDetails'][k].name,
                  checked: checked
                });
              }
              this.addButton = this.addButton + 1;
            }
          }

          if (
            this.programsWithProgramDetails.length != this.selected.length
          ) {
            this.multiple.push({ program: [], programId: '', programDetails: [] });
            for (var i = 0; i < this.programs.length; i++) {
              this.multiple[this.addButton]['program'].push({
                id: this.programs[i].id,
                name: this.programs[i].name
              });
            }
          }
        }
      },
      error => console.log(error),
      () => {
        this.loaded = true;
      }
    );
    return this.programs;
  }

  programChange(event) {
    let programId = event.value;
    this.selected.push({ programId: programId, programs: [] });
    this.programDetailService.getProgramProgramDetails(event.value).subscribe(
      response => {
        this.programDetails = response;
        for (var i = 0; i < this.programDetails.length; i++) {
          this.multiple[this.addButton]['programId'] = event.value;
          this.multiple[this.addButton]['programDetails'].push({
            id: this.programDetails[i].id,
            name: this.programDetails[i].name
          });
        }
        this.addMultiple(event.value);
      },
      error => console.log(error),
      () => { }
    );
  }

  addMultiple(programId) {
    this.addButton = this.addButton + 1;
    let selectedPrograms = [];

    this.selected.forEach(element => {
      selectedPrograms.push(element.programId);
    });
    this.programService.getAllPrograms(this.educationLevelId, selectedPrograms).subscribe(
      response => {
        this.programs = response;
        if (this.programs.length != 0) {
          this.multiple.push({ program: [], programId: '', programDetails: [] });
          for (var i = 0; i < this.programs.length; i++) {
            this.multiple[this.addButton]['program'].push({
              id: this.programs[i].id,
              name: this.programs[i].name
            });
          }
        }
      },
      error => console.log(error),
      () => {

      }
    );
  }
  deletePorgram(programId) {
    this.addButton = this.addButton - 1;
    for (var i = 0; i < this.multiple.length; i++) {
      if (this.multiple[i].programId == programId) {
        this.multiple.splice(i, 1);
      }
    }
    for (var i = 0; i < this.selected.length; i++) {
      if (this.selected[i]['programId'] == programId) {
        this.selected.splice(i, 1);
      }
    }
    for (var j = 0; j < this.campusPrograms.length; j++) {
      if (this.campusPrograms[j]['programId'] == programId) {
        this.campusPrograms.splice(j, 1);
      }
    }
    let selectedPrograms = [];

    this.selected.forEach(element => {
      selectedPrograms.push(element.programId);
    });

    this.programService.getAllPrograms(this.educationLevelId, selectedPrograms).subscribe(
      response => {
        this.programs = response;
        if (this.programs.length != 0) {
          if (this.multiple[this.multiple.length - 1]['programId'] == '') {
            this.multiple[this.multiple.length - 1]['program'] = [];
            for (var i = 0; i < this.programs.length; i++) {
              this.multiple[this.addButton]['program'].push({
                id: this.programs[i].id,
                name: this.programs[i].name
              });
            }
          } else {
            this.multiple.push({ program: [], programId: '', programDetails: [] });
            for (var i = 0; i < this.programs.length; i++) {
              this.multiple[this.addButton]['program'].push({
                id: this.programs[i].id,
                name: this.programs[i].name
              });
            }
          }
        }
      },
      error => console.log(error),
      () => { }
    );
  }
  checkChange(event, programId, proId) {
    var trueCondition = true;
    if (event.checked == true) {
      for (var i = 0; i < this.campusPrograms.length; i++) {
        if (this.campusPrograms[i]['programId'] == programId) {
          this.campusPrograms[i]['programDetails'].push(proId);
          trueCondition = false;
        }
      }
      if (trueCondition) {
        this.campusPrograms.push({
          programId: programId,
          programDetails: []
        });
        for (var i = 0; i < this.campusPrograms.length; i++) {
          if (this.campusPrograms[i]['programId'] == programId) {
            this.campusPrograms[i]['programDetails'].push(proId);
          }
        }
      }
    } else {
      for (var i = 0; i < this.campusPrograms.length; i++) {
        if (this.campusPrograms[i]['programId'] == programId) {
          for (var j = 0; j < this.campusPrograms[i]['programDetails'].length; j++) {
            if (this.campusPrograms[i]['programDetails'][j] == proId) {
              this.campusPrograms[i]['programDetails'].splice(j, 1);
            }
          }
          if (this.campusPrograms[i]['programDetails'].length != 0) {
          } else {
            this.campusPrograms.splice(i, 1);
          }
        }
      }
    }
    //this.campusProgramService.sendCRProgram(this.campusPrograms);
  }

  editChange(event, programId, proId) {
    var trueCondition = true;
    if (event == true) {
      for (var i = 0; i < this.campusPrograms.length; i++) {
        if (this.campusPrograms[i]['programId'] == programId) {
          this.campusPrograms[i]['programDetails'].push(proId);
          trueCondition = false;
        }
      }
      if (trueCondition) {
        this.campusPrograms.push({
          programId: programId,
          programDetails: []
        });
        for (var i = 0; i < this.campusPrograms.length; i++) {
          if (this.campusPrograms[i]['programId'] == programId) {
            this.campusPrograms[i]['programDetails'].push(proId);
          }
        }
      }
    } else {
      for (var i = 0; i < this.campusPrograms.length; i++) {
        if (this.campusPrograms[i]['programId'] == programId) {
          if (this.campusPrograms[i]['programDetails'].length != 0) {
            for (
              var j = 0;
              j < this.campusPrograms[i]['programDetails'].length;
              j++
            ) {
              if (this.campusPrograms[i]['programDetails'][j] == proId) {
                this.campusPrograms[i]['programDetails'].splice(j, 1);
              }
            }
          } else {
            this.campusPrograms.splice(i, 1);
          }
        }
      }
    }
    //this.campusProgramService.sendCRProgram(this.campusPrograms);
  }
  // For Mobile Number
  // Getting the countries from configuratiuon and then pushing into the array of object
  findCountryAttrbutesList() {
    this.configurationService.findAttributesList('country').subscribe(
      response => {
        // iterate countries
        response.forEach(country => {
          let abbr;
          let cCode;
          // iterate countries config options
          country.configurationOptions.forEach(configurationOption => {
            // check if option is abbreviation
            if (configurationOption.key === 'abbreviation') {
              abbr = configurationOption.value
              // check if option is country code
            } else if (configurationOption.key === 'countryCode') {
              cCode = configurationOption.value
            }
            // if both exists then push into the array
            if (abbr && cCode) {

              this.countriesOptions.push({ abbreviation: abbr, countryCode: cCode })
            }
          });
        });
      },
      error => console.log(error),
      () => { }
    );
  }
  patchMobileNumber() {
    // for first mobile number
    let mobileNumber: string = this.createCampusModel.mobileNumber;
    let countryCode: string = mobileNumber.substring(0, mobileNumber.indexOf('-'));
    let mobileNumberSecondPart: string = mobileNumber.substring(mobileNumber.indexOf('-') + 1, mobileNumber.length);

    this.fg.get('countryCode').patchValue(countryCode);
    this.fg.get('mobileNumber').patchValue(mobileNumberSecondPart);


  }
  patchlandLineNumber() {
    // for first land line number
    let landLineNumber: string = this.createCampusModel.landLineNumber;
    let countryCode: string = landLineNumber.substring(0, landLineNumber.indexOf('-'));
    let landLineNumberSecondPart: string = landLineNumber.substring(landLineNumber.indexOf('-') + 1, landLineNumber.length);

    this.fg.get('countryCodeForLandLine').patchValue(countryCode);
    this.fg.get('landLineNumber').patchValue(landLineNumberSecondPart);
  }
}
