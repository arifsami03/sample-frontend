import { GLOBALS } from '../../../../app/config/globals';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormBuilder } from '@angular/forms';

import { PageAnimation } from '../../../../shared/helper';
import { LayoutService } from '../../../../layout/services';
import { CampusService } from '../../../services';
import { CampusModel } from '../../../models';
import { WFStateModel } from '../../../../settings/models';
import { WFStateService } from '../../../../settings/services';

@Component({
  selector: 'campus-campus-list',
  templateUrl: './campus-list.component.html',
  styleUrls: ['./campus-list.component.css'],
  animations: [PageAnimation]
})
export class CampusListComponent implements OnInit {

  public pageState;
  public loaded: boolean = false;

  public data: CampusModel[] = [new CampusModel()];
  public componentLabels = CampusModel.attributesLabels;

  displayedColumns = ['campusId', 'campusName', 'codeNumber', 'email', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private campusService: CampusService,
    public layoutService: LayoutService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    const bc = this.activatedRoute.snapshot.data['breadcrumb'];

    this.layoutService.setPageTitle({ title: bc['title'] });

    this.getRecords();
  }

  /**
   * Getting pre registartion list
   */
  getRecords(): void {

    this.campusService.index().subscribe(
      response => {
        this.data = response;
        this.dataSource = new MatTableDataSource<CampusModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {

        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); //TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }

}
