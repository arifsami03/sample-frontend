import { Component, ViewChild, OnInit } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatDialog,
  MatSort
} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { LayoutService } from '../../../../layout/services';
import { CRMouService, CampusPostRegistrationService } from '../../../services';

import { ConfirmDialogComponent } from '../../../../shared/components';
import { CRMouModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'campus-mou-list',
  templateUrl: './mou-list.component.html',
  styleUrls: ['./mou-list.component.css']
})
export class MouListComponent implements OnInit {
  public loaded: boolean = true;
  public data: CRMouModel[] = [new CRMouModel()];
  public attrLabels = CRMouModel.attributesLabels;

  displayedColumns = ['campusTitle', 'email', 'applicationType', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public success: Boolean;
  public wfData: any;

  constructor(
    private mouService: CRMouService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService,
    public campusPostRegistrationService: CampusPostRegistrationService,
    private activatedRoute: ActivatedRoute,
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.wfData = this.activatedRoute.snapshot.data['wfData'];
    this.layoutService.setPageTitle({ title: 'MOUs' });
  }

  /**
   * Get all records
   */
  getRecords(): void {
    // this.campusPostRegistrationService.postRegistrationList(this.wfData).subscribe(
    //   response => {
    //     this.data = response;

    //     this.data.map(_data => {
    //       _data.campusTitle = response[0]['campusName'];
    //       // _data.le = _data.applicant.applicationType;
    //       // _data.cityName = '';//(_data.city ? _data.city.value : '');
    //     });
    //     this.dataSource = new MatTableDataSource<CRMouModel>(this.data);
    //   },
    //   error => { },
    //   () => {
    //     this.dataSource.paginator = this.paginator;
    //     this.dataSource.sort = this.sort;
    //     this.loaded = true;
    //   }
    // );
  }

  /**
   * Search in Grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete data against the id given
   */
  delete(id: number) {
    //   // Confirm dialog
    //   const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
    //     width: GLOBALS.deleteDialog.width,
    //     data: { message: GLOBALS.deleteDialog.message }
    //   });
    //   dialogRef.afterClosed().subscribe((accept: boolean) => {
    //     if (accept) {
    //       this.loaded = false;
    //       this.courseService.delete(id).subscribe(response => {
    //         this.layoutService.flashMsg({ msg: 'Course has been deleted.', msgType: 'success' });
    //         this.getRecords();
    //       },
    //         error => {
    //           this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
    //           console.log(error);
    //           this.loaded = true;
    //         },
    //         () => {
    //           //TODO
    //           this.getRecords();
    //           this.loaded = true;
    //         }
    //       );
    //     }
    //   });
  }
}
