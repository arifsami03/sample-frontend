import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatOptionSelectionChange } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { PageAnimation } from 'modules/shared/helper';
import { LayoutService, PrintService } from 'modules/layout/services';
import { DocStateManagerService, ConfigurationService } from 'modules/shared/services';
import { ConfirmDialogComponent } from 'modules/shared/components';
import { GLOBALS } from 'modules/app/config/globals';
import { ProspectusSaleModel } from 'modules/campus/models';
import { } from 'modules/settings/services';

// for Mobile Number
interface ICountryOption {
  id: number,
  abbreviation: string;
  countryCode: string;
}

@Component({
  selector: 'prospectus-sale-form',
  templateUrl: './prospectus-sale-form.component.html',
  styleUrls: ['./prospectus-sale-form.component.css'],
  animations: [PageAnimation]
})
export class ProspectusSaleFormComponent implements OnInit {


  public _cnicMask = GLOBALS.masks.cnic;
  public countriesOptions: ICountryOption[] = [];
  public pageState = 'active';
  public loaded: boolean = false;
  public boxLoaded = false;
  public pageActions = GLOBALS.pageActions;
  public _mobileNumberMask = GLOBALS.masks.mobile;
  public error: Boolean;
  public fg: FormGroup;
  public pageAct: string;
  public prospectusSaleModel: ProspectusSaleModel;
  public selectedButton: string = 'mou';
  public isRecordExists: boolean;
  programDetailId: number = 1;
  public pageTitle: string = 'Prospectus Sale';
  public programDetails = [
    { id: 1, name: 'F.SC - Pre Engineering', color: 'primary' },
    { id: 2, name: 'F.SC - Pre Medical', color: '' },
    { id: 3, name: 'I.COM - I.COM', color: '' },
    { id: 4, name: 'I.CS - I.CS', color: '' },
    { id: 5, name: 'F.A - General Science', color: '' },
  ]

  // public stateId: number;

  public componentLabels = ProspectusSaleModel.attributesLabels;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private configurationService: ConfigurationService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    public printService: PrintService,
    private layoutService: LayoutService,
  ) {
    this.pageAct = activatedRoute.snapshot.data['act'];
  }

  // public managementFees: ManagementFeeModel[];


  /**
   * ngOnInit
   */
  ngOnInit() {
    this.layoutService.setPageTitle({ title: this.pageTitle });

    this.initializePage();
    this.findCountryAttrbutesList();
  }
  /**
   * Initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new ProspectusSaleModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      //this.loaded = true;
      this.getData();
    }
  }

  /**
   * Get Data
   */
  private getData() {

    // this.mouService.find(this.CRApplicationId).subscribe(response => {

    //   if (response) {
    //     this.isRecordExists = true;
    //     this.prospectusSaleModel = response;
    //     this.fg.patchValue(this.prospectusSaleModel);
    //   }
    //   else {
    //     this.isRecordExists = false;
    //   }

    //   this.loaded = true;

    // , color:''}, error => {
    //   console.log(error);
    //   this.loaded = true;
    // });

  }

  /**
   * Initialize Create page
   */
  private initCreatePage() {


    this.prospectusSaleModel = new ProspectusSaleModel();

    this.fg.enable();

    this.loaded = true;
  }


  tabChange(type: string) {

    this.selectedButton = type;
  }


  chipClicked(id) {
    this.programDetailId = id;

    this.programDetails.forEach(element => {

      if (element['id'] == id) {
        element['color'] = 'primary';
      }
      else {
        element['color'] = '';
      }

    });

  }




  /**
   * Add or Update when save button is clicked
   */
  public saveData(item: ProspectusSaleModel) {
    this.boxLoaded = false;
    this.programDetails.forEach(element => {
      if (element['id'] == this.programDetailId) {
        item['programTitle'] = element['name']
      }

    });

    this.printService.sendDataMethod({ page: 'prospectus', data: item });

    // console.log(item);
    // var mywindow = window.open('', 'PRINT', 'height=1000,width=1200');

    // mywindow.document.write('<html><head><title>' + 'IDRAK University' + '</title>');
    // mywindow.document.write('</head><body style=" font-size:20  px;width:600px;height:1000px:">');
    // mywindow.document.write('<h1>' + 'IDRAK University' + '</h1>');
    // mywindow.document.write('<b style="font-size:30px; padding:10px">' + 'Prospectus No: '+ item.prospectusNumber  + '</b>');  
    // this.programDetails.forEach(element => {
    //   if(element['id']==this.programDetailId)
    //   {
    //     mywindow.document.write('<div style="font-size:30px; padding:10px">' +  element['name']  + '</div>');  
    //   }

    // });
    // var date = new Date().toLocaleString();
    // mywindow.document.write('<div style="font-size:30px;  padding:10px">' + 'Invoice Date: '+  date  + '</div>');  
    // mywindow.document.write('<div  style="font-size:20px;  padding-top:20px">');  
    // mywindow.document.write('<table  style="font-size:15px;  border: 1px solid black;  border-collapse: collapse; padding:40px">');  
    // mywindow.document.write('<tr>');  
    // mywindow.document.write('<td style=" border: 1px solid black;padding:10px">' + 'Full Name' + '</td>');  
    // mywindow.document.write('<td style=" border: 1px solid black;padding:10px">' + item.fullName+ '</td>');  
    // mywindow.document.write('</tr>');  
    // mywindow.document.write('<tr>');  
    // mywindow.document.write('<td style=" border: 1px solid black;padding:10px">' + 'CNIC' + '</td>');  
    // mywindow.document.write('<td style=" border: 1px solid black;padding:10px">' + item.cnic+ '</td>');  
    // mywindow.document.write('</tr>');  
    // mywindow.document.write('<tr>');  
    // mywindow.document.write('<td style=" border: 1px solid black;padding:10px">' + 'Mobile Number' + '</td>');  
    // mywindow.document.write('<td style=" border: 1px solid black;padding:10px">' + item.mobileNumber+ '</td>');  
    // mywindow.document.write('</tr>');  
    // mywindow.document.write('<tr>');  
    // mywindow.document.write('<td style=" border: 1px solid black;padding:10px">' + 'Email' + '</td>');  
    // mywindow.document.write('<td style=" border: 1px solid black;padding:10px">' + item.email+ '</td>');  
    // mywindow.document.write('</tr>');  
    // mywindow.document.write('<tr>');  
    // mywindow.document.write('<td style=" border: 1px solid black;padding:10px">' + 'Shift' + '</td>');  
    // mywindow.document.write('<td style=" border: 1px solid black;padding:10px">' + 'Morning'+ '</td>');  
    // mywindow.document.write('</tr>');  
    // mywindow.document.write('<tr>');  
    // mywindow.document.write('<td style=" border: 1px solid black;padding:10px">' + 'Price' + '</td>');  
    // mywindow.document.write('<td style=" border: 1px solid black;padding:10px">' + item.price+ '/= </td>');  
    // mywindow.document.write('</tr>');  


    // mywindow.document.write('</table');  
    // mywindow.document.write('</div');  


    // mywindow.document.write('</body></html>');

    // mywindow.document.close(); // necessary for IE >= 10
    // mywindow.focus(); // necessary for IE >= 10*/

    // window.print();


  }

  // For Mobile Number
  // Getting the countries from configuratiuon and then pushing into the array of object
  findCountryAttrbutesList() {
    this.configurationService.findAttributesList('country').subscribe(
      response => {
        // iterate countries
        response.forEach(country => {
          let abbr;
          let cCode;
          // iterate countries config options
          country.configurationOptions.forEach(configurationOption => {
            // check if option is abbreviation
            if (configurationOption.key === 'abbreviation') {
              abbr = configurationOption.value
              // check if option is country code
            } else if (configurationOption.key === 'countryCode') {
              cCode = configurationOption.value
            }
            // if both exists then push into the array
            if (abbr && cCode) {

              this.countriesOptions.push({ id: country.id, abbreviation: abbr, countryCode: cCode })
            }
          });
        });
        this.selectDefaultCountry();
      },
      error => console.log(error),
      () => { }
    );
  }

  /**
   * Setting default country and loading its cities
   */
  selectDefaultCountry() {
    let pak: ICountryOption;
    this.countriesOptions.forEach(country => {
      if (country.abbreviation === 'PAK' || country.countryCode === '92' || country.countryCode === '092' || country.countryCode === '+92') {
        pak = country;
      }
    });
    if (pak) {
      this.fg.get('countryCode').setValue(pak.countryCode);

    }
  }

}
