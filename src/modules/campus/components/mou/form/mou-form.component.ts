import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatOptionSelectionChange } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { PageAnimation } from '../../../../shared/helper';
import { CRMouService } from '../../../services';
import { LayoutService } from '../../../../layout/services';

import { ManagementFeeService } from '../../../../fee-management/services';
import { EducationLevelService } from '../../../../institute/services';
import { AcademicCalendarService } from '../../../../academic/services';

import { ConfirmDialogComponent } from '../../../../shared/components';
import { DocStateManagerService } from '../../../../shared/services';
import { CRMouModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';
import { ManagementFeeModel } from '../../../../fee-management/models';
import { EducationLevelModel } from '../../../../institute/models';
import { AcademicCalendarModel } from '../../../../academic/models';

@Component({
  selector: 'campus-mou-form',
  templateUrl: './mou-form.component.html',
  styleUrls: ['./mou-form.component.css'],
  animations: [PageAnimation]
})
export class MouFormComponent implements OnInit {

  @Input() CRApplicationId: number;

  public pageState = 'active';
  public loaded: boolean = false;
  public boxLoaded = false;
  public pageActions = GLOBALS.pageActions;
  public error: Boolean;
  public fg: FormGroup;
  public pageAct: string;
  public mouModel: CRMouModel;
  public selectedButton: string = 'mou';
  public isRecordExists: boolean;
  public pageTitle: string = 'MOU';

  // public stateId: number;

  public componentLabels = CRMouModel.attributesLabels;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private mouService: CRMouService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    public educationLevel: EducationLevelService,
    public academicCalendarService: AcademicCalendarService,
    public managementFeeService: ManagementFeeService,
    public docStateManagerService: DocStateManagerService
  ) {
    this.pageAct = activatedRoute.snapshot.data['act'];
  }

  public educationLevels: EducationLevelModel[] = [];
  public academicCalendars = [new AcademicCalendarModel()];
  public managementFees: ManagementFeeModel[];

  public academicMonths = GLOBALS.MONTHS;
  public academicYears: number[] = GLOBALS.academicYears;
  public semSessions: any[] = [GLOBALS.RMCategories.fall,GLOBALS.RMCategories.spring];

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.layoutService.setPageTitle({ title: this.pageTitle });
    this.getEducationLevel();
    this.getAcademicYear();
    this.initializePage();
  }
  /**
   * Initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new CRMouModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      //this.loaded = true;
      this.getData();
    }
  }

  /**
   * Get Data
   */
  private getData() {

    this.mouService.find(this.CRApplicationId).subscribe(response => {

      if (response) {
        this.isRecordExists = true;
        this.mouModel = response;
        this.fg.patchValue(this.mouModel);
      }
      else {
        this.isRecordExists = false;
      }

      this.loaded = true;

    }, error => {
      console.log(error);
      this.loaded = true;
    });

  }

  /**
   * Initialize Create page
   */
  private initCreatePage() {


    this.mouModel = new CRMouModel();

    this.fg.enable();

    this.loaded = true;
  }


  tabChange(type: string) {
    
    this.selectedButton = type;
  }

  getEducationLevel() {

    this.educationLevel.findAttributesList().subscribe(response => {

      this.educationLevels = response;

      this.loaded = true;

    }, error => {

      console.log(error);
      this.loaded = true;

    });


  }

  getAcademicYear() {

    this.academicCalendarService.findAttributesList().subscribe(response => {

      this.academicCalendars = response;
      this.loaded = true;

    }, error => {
      console.log(error);
      this.loaded = true;
    });

  }


  educationLevelChange(event: MatOptionSelectionChange, educationLevelId: number) {

    if (event.source.selected) {

      this.managementFeeService.findByEducationLevelId(educationLevelId).subscribe(response => {

        this.managementFees = response;
        this.loaded = true;

      }, error => {
        console.log(error);
        this.loaded = true;
      });

    }
  }

  /**
   * Add or Update when save button is clicked
   */
  public saveData(item: CRMouModel) {
    this.boxLoaded = true;

    item.CRApplicationId = this.CRApplicationId;


    if (!this.isRecordExists) {

      this.mouService.create(item).subscribe(response => {

        if (response) {

          this.boxLoaded = false;
          this.layoutService.flashMsg({ msg: 'MOU has been created.', msgType: 'success' });

          this.getData();
        }

      }, error => {

        this.boxLoaded = false;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });

      }, () => { });

    } else if (this.isRecordExists) {

      this.mouService.update(this.mouModel.id, item).subscribe(response => {

        if (response) {

          this.boxLoaded = false;
          this.layoutService.flashMsg({ msg: 'MOU has been updated.', msgType: 'success' });

          this.getData();
        }

      }, error => {

        this.boxLoaded = false;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });

      }, () => { });
    }
  }


  /**
 * Function to Get State ID of Post-Registration Work Flow
 */

  // getCurrentStateId() {

  //   var postData = {
  //     CRApplicationId: this.CRApplicationId
  //   };

  //   return new Promise((resolve, reject) => {

  //     this.docStateManagerService.getCurrentStateId(postData).subscribe(response => {

  //       this.stateId = response['stateId'];
  //       console.log('stateId: ', this.stateId);
  //       resolve(response);

  //     }, error => {
  //       console.log(error);
  //       this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
  //       reject(error);
  //     });

  //   });

  // }
  onChangeStateEvent(item) {
    if (item) {
      this.router.navigate([`/campus/post-registrations`]);
    }
  }
  
}
