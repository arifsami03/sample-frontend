import { BankModel } from '../../../../settings/models';
import { ConfigurationModel } from '../../../../shared/models';
import { Observable } from 'rxjs/Observable';
import { Component, Output, EventEmitter, OnInit, AfterViewInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { startWith } from 'rxjs/operators/startWith';
import { GLOBALS } from '../../../../app/config/globals';
import { map } from 'rxjs/operators/map';
import { CRRemittanceModel } from '../../../models';
import { LayoutService } from '../../../../layout/services';
import { PageAnimation } from '../../../../shared/helper';
import { BankService } from '../../../../settings/services';
import { ConfigurationService } from '../../../../shared/services';
import { CRRemittanceService, CRApplicantService, CampusPostRegistrationService, CampusInfoService } from '../../../services';

// for dat picker format
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
import { ManagementFeeService } from '../../../../fee-management/services';
const moment = _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL'
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};

@Component({
  selector: 'remittance',
  templateUrl: './remittance.component.html',
  styleUrls: ['./remittance.component.css'],
  animations: [PageAnimation],
  providers: [{ provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] }, { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }]
})
export class RemittanceComponent implements OnInit {
  public pageState = 'active';
  public loaded: boolean = false;
  public boxLoaded: boolean = false;
  public pageActions = GLOBALS.pageActions;
  @Input() campusApplicantId: number;
  @Input() createdCRApplicationId: number;  
  @Input() type: '';
  public fg: FormGroup;
  public pageAct: string;

  public pageTitle: string = 'Remittance';
  public instrumentCategory = ['Bank Draft', 'Cheque', 'Mail Transfer', 'Cash letter of credit', 'Telegraphic Transfer'];
  // public bankName = ['AlliedBank', 'Alfalah Bank', 'Habib Bank', 'Meezan Bank'];
  public purposes = ['Application Form', 'Registraion Form', 'Cancel Form'];
  purposeDisabled = true;
  public id: number;
  public banks: BankModel[];
  public isSubmit: boolean = true;
  public componentLabels = CRRemittanceModel.attributesLabels;
  public crRemiitanceModel: CRRemittanceModel;

  //Form Control for Custom Bank
  public bankFormControl: FormControl = new FormControl();
  public filteredOptions: Observable<string[]>;
  public bankId: number;
  public applicationId: number;
  public applicantId: number;
  public bankName: string = '';
  /**
   * for Datepicker
   */
  public currentDate = new FormControl(_moment());
  public instrumentDate = new FormControl(_moment());

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private layoutService: LayoutService, //public dialog: MatDialog // private instituteEditService: InstituteService
    private remittanceService: CRRemittanceService,
    public applicantService: CRApplicantService,
    private campusPostRegistrationService: CampusPostRegistrationService,
    private campusInfoService: CampusInfoService,
    private managementFeeService: ManagementFeeService,
    private banksService: BankService
  ) {
    this.pageAct = route.snapshot.data['act'];
    this.route.params.subscribe(param => {
      this.applicationId = param.id;
    });
  }

  /**
   * ngOnInit
   */

  ngOnInit() {
    this.layoutService.setPageTitle({ title: this.pageTitle });
    this.findBanks();

    if (this.pageAct == this.pageActions.create) {
      this.applicationId=this.createdCRApplicationId;
      this.getApplicantIdByApplicationId();
      this.initializePage();
      
    }
    else {
    if (this.pageAct == this.pageActions.update) {
      this.getApplicantIdByApplicationId();
      this.initializePage();
    }
    else {
      this.getApplicantId();
      this.initializePage();
    }
  }

    // this.getRegistrationStatus();
  }

  filter(val: string): any[] {
    if (this.banks) {
      if (val) {
        return this.banks.filter(option => option.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
      } else {
        return this.banks.filter(option => option.name.toLowerCase());
      }
    }
  }
  getApplicantIdByApplicationId() {
    this.loaded = false;
    return this.applicantService.getApplicantIdByApplicationId(this.applicationId).subscribe(
      response => {

        this.applicantId = response['CRApplicantId'];

        this.getRemittanceInfo();
        this.getRegistrationStatus();
        this.findCampusEducationLevel(this.applicantId)


      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }
  getApplicantId() {
    var _userId = Number(localStorage.getItem('id'));
    return this.applicantService.getApplicantId(_userId).subscribe(
      response => {

        this.applicantId = response['CRApplicantId'];
        this.getRemittanceInfo();
        this.getRegistrationStatus();
        this.findCampusEducationLevel(this.applicantId)

      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  getRemittanceInfo() {
    this.loaded = false;
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.applicantId;
    this.remittanceService.view(this.applicantId).subscribe(
      response => {

        if (response) {
          this.crRemiitanceModel = response;
          this.fg.patchValue(response);
          if (response['bankName'] != null) {
            this.bankFormControl.setValue(response['bankName']);
          } else {
            for (let i = 0; i < this.banks.length; i++) {
              if (this.banks[i].id == response['bankId']) {
                this.bankFormControl.setValue(this.banks[i].name);
              }
            }
          }
        }
        else {
          this.crRemiitanceModel = new CRRemittanceModel();
        }
      },
      error => { },
      () => {
        this.fg.controls['purpose'].setValue('Application Form');
        this.loaded = true;
      }
    );
  }

  private initializePage() {

    this.fg = this.fb.group(new CRRemittanceModel().validationRules());
    this.fg.addControl('bankFormControl', this.bankFormControl);



    // this.fg.disable();
  }

  private findCampusEducationLevel(campusApplicantId: number) {
    this.campusInfoService.findCampusEducationLevel(campusApplicantId).subscribe(
      response => {
        if (response) {
          this.managementFeeService.findFee(response.educationLevelId).subscribe(
            result => {
              if (result) {
                this.fg.controls['amount'].setValue(result.amount);
              } else {
                this.fg.controls['amount'].setValue(0);
              }
            },
            err => {
              console.log('err: ', err);
            }
          )
        } else {
          this.fg.controls['amount'].setValue(0);
        }

      },
      error => {
        console.log('error: ', error);
      }
    )
  }

  /**
   * Function to Validate Custom enterd Bank
   *
   * @param {CRRemittanceModel} item
   * @memberof RemittanceComponent
   */
  validateCustomBank(item: CRRemittanceModel) {
    for (let i = 0; i < this.banks.length; i++) {

      if (this.bankFormControl.value) {
        if (this.banks[i].name.toLowerCase().trim() == this.bankFormControl.value.toLowerCase().trim()) {
          this.bankName = null;
          this.bankId = this.banks[i].id;
          break;
        } else {
          this.bankName = this.bankFormControl.value;
          this.bankId = null;
        }
      }

    }

    if (this.bankName != null) {
      this.bankId = null;
      this.bankName = this.bankFormControl.value;
    }

    item.bankId = this.bankId;
    item.bankName = this.bankName;
  }

  public saveData(item: CRRemittanceModel) {

    this.boxLoaded = true;


    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.applicantId;
    this.validateCustomBank(item);

    const _data = {
      applicantId: this.campusApplicantId,
      remittance: item
    };

    this.remittanceService.save(_data).subscribe(
      response => {
        this.boxLoaded = false;
        if (this.pageAct == this.pageActions.update || this.createdCRApplicationId != null) {
          this.shiftToNextTab('submit');
        }
        else {
          this.router.navigate(['/post-registration/campus/submit']);
        }
      },
      error => {
        this.boxLoaded = false;
        if (this.pageAct == this.pageActions.update || this.createdCRApplicationId != null) {
          this.shiftToNextTab('submit');
        }
        else {
          this.router.navigate(['/post-registration/campus/submit']);
        }
      },
      () => {

      }
    );
  }

  findBanks() {
    this.loaded = false;

    this.banksService.findAttributesList().subscribe(
      response => {
        this.loaded = true;

        this.banks = response;
        this.filteredOptions = this.bankFormControl.valueChanges.pipe(startWith(''), map(val => this.filter(val)));
      },
      error => { },
      () => { }
    );
  }

  getRegistrationStatus() {
    var _userId = Number(localStorage.getItem('id'));
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : _userId;
    this.loaded = false;
    this.campusPostRegistrationService.getRegistrationStatus(this.applicantId).subscribe(
      res => {
        if (res['state'] != 'draft' && res['WFId'] == 'post-registration') {
          this.isSubmit = true;
          this.fg.disable();
        } else {
          this.fg.enable();
          this.isSubmit = false;
        }
      },
      error => { },
      () => {
        this.loaded = true;
      }
    );
  }

  //In case of Edit to shift the Tab after save we use this function
  //This Function Emits the nameOfTheTab on which to move
  @Output() shiftTab = new EventEmitter<object>();
  shiftToNextTab(tab) {
    this.shiftTab.emit(tab);
  }

}
