import { Observable } from 'rxjs/Observable';
import { Component, EventEmitter, Output, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { PageAnimation } from '../../../../shared/helper';
import { PersonalInformationModel } from '../../../models';
import { LayoutService } from '../../../../layout/services';
import { BankService } from '../../../../settings/services';
import { ConfigurationService } from '../../../../shared/services';
import { CRApplicantService, CampusPostRegistrationService } from '../../../services';

import { ConfirmDialogComponent } from '../../../../shared/components';

import { BankModel } from '../../../../settings/models';
import { ConfigurationModel } from '../../../../shared/models';
import { GLOBALS } from '../../../../app/config/globals';
import { CampusPreRegistration } from '../../../../public/models';

// for Mobile Number
interface ICountryOption {
  abbreviation: string;
  countryCode: string;
}

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.css'],
  animations: [PageAnimation],
  providers: [ConfigurationService, CRApplicantService]
})
export class PersonalInformationComponent implements OnInit {
  public pageState = 'active';
  @Input() campusApplicantId: number;
  @Input() CCRApplicationId: number;
  @Input() type: '';
  @Output() createdCRApplicationId = new EventEmitter<any>();
  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public boxLoaded: boolean = false;
  public id: number;
  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string = 'Personal Information';
  public personalInfo: PersonalInformationModel;
  public states: string[] = ['Punjab', 'Sindh', 'Balochistan', 'KPK', 'Gilgit'];
  public options: Observable<string[]>;
  public deletedReferenceIds: number[] = [];
  public isSubmit: boolean = true;
  public mobileMask = GLOBALS.masks.mobile;
  public test = false;

  public cnicMask = GLOBALS.masks.cnic;

  public componentLabels = PersonalInformationModel.attributesLabels;

  public countries: ConfigurationModel[];
  public province: ConfigurationModel[];
  public cities: ConfigurationModel[];
  public tehsils: ConfigurationModel[];

  public natureOfwork: ConfigurationModel[];
  public bank: BankModel[];
  public userId: number;
  public applicationId: number;
  public bankId: number;
  public applicantId: number;
  public bankName: string = '';
  public bankFormControl: FormControl = new FormControl();
  public filteredOptions: Observable<string[]>;

  // for Mobile Number
  public countriesOptions: ICountryOption[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    public configurationService: ConfigurationService,
    public bankService: BankService,
    public applicantService: CRApplicantService,
    private campusPostRegistrationService: CampusPostRegistrationService,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
    this.route.params.subscribe(param => {
      this.applicationId = param.id;
    });

  }

  /**
   * ngOnInit
   */

  ngOnInit() {

    this.loaded = false;
    this.layoutService.setPageTitle({ title: this.pageTitle });

    this.getNatureOfWorkList();

    this.getBankList();

    // For Mobile Number
    this.findCountryAttrbutesList();

    if (this.CCRApplicationId != null) {

      this.router.navigate([`/campus/post-registrations/update/${this.CCRApplicationId}`]);

    }
    else {

      if (this.pageAct == this.pageActions.create) {

        this.createForm();

      }
      else {

        if (this.pageAct == this.pageActions.update) {
        
          this.getApplicantIdByApplicationId();
          this.initializePage();

        }

        else {

          this.getApplicantId();
          this.initializePage();

        }
      }
    }






  }

  createForm() {
    this.fg = this.fb.group({
      fullName: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      email: ['', [<any>Validators.required, Validators.email]],
      mobileNumber: ['', [<any>Validators.maxLength(12), Validators.pattern('([0-9]{3})-([0-9]{7})')]],
      address: ['', [<any>Validators.maxLength(100)]],
      cnic: ['', [<any>Validators.required]],
      countryId: [null],
      provinceId: [null],
      cityId: [null],
      tehsilId: [null],
      state: [''],
      zip: [''],
      country: [''],
      description: [''],
      nearestBankId: [''],
      approxMonthlyIncome: new FormControl('', [this.greaterThan('approxMonthlyIncome')]),
      natureOfWorkId: [''],
      references: this.fb.array([]),
      countryCode: new FormControl(),
    });
    this.fg.addControl('bankFormControl', this.bankFormControl);
    let references = this.fg.get('references') as FormArray;
    references.push(this.createReferenceItem());
    this.loaded = true;
    this.isSubmit = false;
    this.personalInfo = new PersonalInformationModel();
    this.getCountryList();
  }

  getApplicantId() {
    this.loaded = false;

    this.userId = Number(localStorage.getItem('id'));

    return this.applicantService.getApplicantId(this.userId).subscribe(
      response => {

        this.applicantId = response['CRApplicantId'];
        this.getApplicantInfo();
        this.getRegistrationStatus();

      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }
  getApplicantIdByApplicationId() {

    this.loaded = false;

    return this.applicantService.getApplicantIdByApplicationId(this.applicationId).subscribe(
      response => {

        this.applicantId = response['CRApplicantId'];
        this.getApplicantInfo();
        this.getRegistrationStatus();

      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   *Checking validation of number
   * @param equalControlName
   */
  public greaterThan?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      let controlMatch = 0;
      return controlMatch <= control.value && 9999999999 >= control.value
        ? null
        : {
          equalTo: true
        };
    };
  }

  private initializePage() {
    //this.fg = this.fb.group(new PersonalInformationModel().validationRules());
    this.fg = this.fb.group({
      fullName: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      email: ['', [<any>Validators.required, Validators.email]],
      mobileNumber: ['', [<any>Validators.maxLength(12), Validators.pattern('([0-9]{3})-([0-9]{7})')]],
      address: ['', [<any>Validators.maxLength(100)]],
      cnic: ['', [<any>Validators.required]],
      countryId: [null],
      provinceId: [null],
      cityId: [null],
      tehsilId: [null],
      state: [''],
      zip: [''],
      country: [''],
      description: [''],
      nearestBankId: [''],
      approxMonthlyIncome: new FormControl('', [this.greaterThan('approxMonthlyIncome')]),
      natureOfWorkId: [''],
      references: this.fb.array([]),
      countryCode: new FormControl(),
    });
    this.fg.addControl('bankFormControl', this.bankFormControl);
    this.fg.disable();
  }

  createReferenceItem(): FormGroup {
    return this.fb.group({
      id: '',
      refName: [''],
      refMobileNumber: ['', [<any>Validators.maxLength(12), Validators.pattern('([0-9]{3})-([0-9]{7})')]],
      refCountryCode: [''],
      refAddress: ['']
    });
  }

  addReferenceClick() {
    let references = this.fg.get('references') as FormArray;
    references.push(this.createReferenceItem());
    // disabling on submit
    if (this.pageAct != this.pageActions.create) {
      this.getRegistrationStatus();
    }

  }


  /**
   * Function to get Campus owner id from user Table
   */
  getApplicantInfo() {
    this.loaded = false;
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.applicantId;
    this.applicantService.findPersonInfo(this.applicantId).subscribe(
      response => {
        //this.province = response;
        this.personalInfo = response;
        this.getCountryList();

        if (this.personalInfo.countryId) {
          this.getProvinceList(this.personalInfo.countryId);
        }
        if (this.personalInfo.provinceId) {
          this.getCitiesList(this.personalInfo.provinceId);
        }
        if (this.personalInfo.cityId) {
          this.getTehsilsList(this.personalInfo.cityId);
        }

        this.id = response['id'];
        if (this.personalInfo['references'].length == 0) {
          this.addReferenceClick();
        }
        for (var i = 0; i < this.personalInfo['references'].length; i++) {
          this.addReferenceClick();
        }
        this.fg.patchValue(this.personalInfo);
        this.patchMobileNumber();
        if (response['bankName'] != null) {
          this.bankFormControl.setValue(response['bankName']);
        } else {
          for (let i = 0; i < this.bank.length; i++) {
            if (this.bank[i].id == response['nearestBankId']) {
              this.bankFormControl.setValue(this.bank[i].name);
            }
          }
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  getEmailErrorMessage() {
    return this.fg.get('email').hasError('required') ? 'Email is Required' : this.fg.get('email').hasError('email') ? 'Not a valid email' : '';
  }

  public saveData(item: PersonalInformationModel) {

    this.boxLoaded = true;
    this.validateCustomBank(item);

    // for Mobile Numbers

    item['mobileNumber'] = item['countryCode'] + '-' + item['mobileNumber'];

    for (let i = 0; i < item['references'].length; i++) {
      let refMobileNumber = item['references'][i]['refMobileNumber'].replace(item['references'][i]['refCountryCode'] + '-', '');
      item['references'][i]['refMobileNumber'] = item['references'][i]['refCountryCode'] + '-' + refMobileNumber;

    }

    if (this.pageAct == this.pageActions.create) {

      item['applicationType'] = 'create';
      this.applicantService.create(item).subscribe(
        response => {

          this.boxLoaded = false;
          this.createdCRApplicationId.emit(response['CRApplicationId']);
          this.shiftToNextTab('campus');

        },

        error => {
          if (error.status == 403) {
            this.fg.controls['email'].setErrors({ exists: true });
          } else {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          }
          this.boxLoaded = false;
          console.log(error);
        },
        () => { }
      );

    }
    else {

      let postData = {
        personalInfo: item,
        deleteRefernceIds: this.deletedReferenceIds
      };

      this.applicantService.update(this.applicantId, postData).subscribe(
        response => {
          this.boxLoaded = false;
          if (this.pageAct == this.pageActions.update) {
            this.shiftToNextTab('campus');
          }
          else {
            this.router.navigate(['/post-registration/campus/campusinfo']);
          }

        },
        error => {
          this.boxLoaded = false;
          console.log(error);
        },
        () => { }
      );
    }

  }

  /**
   * Get List of Countries
   */
  getCountryList() {
    // this.loaded = false;
    this.configurationService.getAll(GLOBALS.configurationKeys.country).subscribe(
      response => {
        this.countries = response;
        if (!this.personalInfo.countryId) {
          this.selectByDefaultCountry();
        }
      },
      error => {
        console.log(error);
        this.loaded = true;

      },
      () => {
        this.loaded = true;

      }
    );
  }

  loadProvinces(parentKeyId) {
    this.getProvinceList(parentKeyId);
    this.fg.controls['provinceId'].setValue(null);
    this.fg.controls['cityId'].setValue(null);
    this.fg.controls['tehsilId'].setValue(null);
  }

  loadCities(parentKeyId) {
    this.getCitiesList(parentKeyId);
    this.fg.controls['cityId'].setValue(null);
    this.fg.controls['tehsilId'].setValue(null);
  }

  loadTehsils(parentKeyId) {
    this.getTehsilsList(parentKeyId);
    this.fg.controls['tehsilId'].setValue(null);
  }

  /**
   * Get List of Provinces
   */
  getProvinceList(parentKeyId: number) {
    //this.loaded = false;
    this.configurationService.findChildren(parentKeyId).subscribe(
      response => {
        this.province = response;
      },
      error => {
        this.loaded = true;
        console.log(error);
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Get List of Cities
   */
  getCitiesList(parentKeyId: number) {
    //this.loaded = false;
    this.configurationService.findChildren(parentKeyId).subscribe(
      response => {
        this.cities = response;
      },
      error => {
        this.loaded = true;
        console.log(error);
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Get List of Tehsils
   */
  getTehsilsList(parentKeyId: number) {
    //this.loaded = false;
    this.configurationService.findChildren(parentKeyId).subscribe(
      response => {
        this.tehsils = response;
      },
      error => {
        this.loaded = true;
        console.log(error);
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Get List of Nature of Work
   */
  getNatureOfWorkList() {
    //this.loaded = false;
    this.configurationService.getAll(GLOBALS.configurationKeys.natureOfWork).subscribe(
      response => {
        this.natureOfwork = response;
      },
      error => {
        console.log(error);
        this.loaded = true;

      },
      () => {
        this.loaded = true;

      }
    );
  }

  /**
   * Get List of Banks
   */
  getBankList() {

    //this.loaded = false;
    this.bankService.findAttributesList().subscribe(
      response => {
        this.bank = response;
        this.filteredOptions = this.bankFormControl.valueChanges.pipe(startWith(''), map(val => this.filter(val)));
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  removeReference(i: number, item) {
    this.deletedReferenceIds.push(item.value.id);
    const control = <FormArray>this.fg.controls['references'];
    control.removeAt(i);
  }
  getRegistrationStatus() {
    //this.loaded = false;
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.applicantId;

    this.campusPostRegistrationService.getRegistrationStatus(this.applicantId).subscribe(
      res => {
        if (res['state'] != 'draft' && res['WFId'] == 'post-registration') {
          this.isSubmit = true;
          this.fg.disable();
        } else {
          this.fg.enable();
          this.isSubmit = false;
        }
      },
      error => {
        console.log(error);
        this.loaded = true;

      },
      () => {
        this.loaded = true;

      }
    );
  }

  /**
   * Function to Filter Options of banks.
   */
  filter(val: string): any[] {

    if (this.bank) {
      if (val) {
        return this.bank.filter(option => option.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
      }
      else {
        return this.bank.filter(option => option.name.toLowerCase());

      }

    }
  }

  /**
   * Function to Validate Custom Bank
   *
   * @param {PersonalInformationModel} item model to add Bank Id or Bank name
   * @memberof PersonalInformationComponent
   */
  validateCustomBank(item: PersonalInformationModel) {
    for (let i = 0; i < this.bank.length; i++) {
      if (this.bankFormControl.value) {
        if (this.bank[i].name.toLowerCase().trim() == this.bankFormControl.value.toLowerCase().trim()) {
          this.bankName = null;
          this.bankId = this.bank[i].id;
          break;
        } else {
          this.bankName = this.bankFormControl.value;
          this.bankId = null;
        }
      }

    }

    if (this.bankName != null) {
      this.bankId = null;
      this.bankName = this.bankFormControl.value;
    }

    item.nearestBankId = this.bankId;
    item.bankName = this.bankName;
  }


  // For Mobile Number
  // Getting the countries from configuratiuon and then pushing into the array of object
  findCountryAttrbutesList() {
    //this.loaded = false;
    this.configurationService.findAttributesList('country').subscribe(
      response => {
        // iterate countries
        response.forEach(country => {
          let abbr;
          let cCode;
          // iterate countries config options
          country.configurationOptions.forEach(configurationOption => {
            // check if option is abbreviation
            if (configurationOption.key === 'abbreviation') {
              abbr = configurationOption.value
              // check if option is country code
            } else if (configurationOption.key === 'countryCode') {
              cCode = configurationOption.value
            }
            // if both exists then push into the array
            if (abbr && cCode) {

              this.countriesOptions.push({ abbreviation: abbr, countryCode: cCode })
            }
          });
        });
      },
      error => {
        this.loaded = true;
        console.log(error);
      },
      () => {
        this.loaded = true;
      }
    );
  }

  patchMobileNumber() {
    // for first mobile number
    let mobileNumber: string = this.personalInfo.mobileNumber;
    let countryCode: string = mobileNumber.substring(0, mobileNumber.indexOf('-'));
    let mobileNumberSecondPart: string = mobileNumber.substring(mobileNumber.indexOf('-') + 1, mobileNumber.length);

    this.fg.get('countryCode').patchValue(countryCode);
    this.fg.get('mobileNumber').patchValue(mobileNumberSecondPart);

    // for refernce mobile numbers
    this.personalInfo['references'].forEach((element, index) => {

      let refMobileNumber: string = element.refMobileNumber;
      let refCountryCode: string = refMobileNumber.substring(0, refMobileNumber.indexOf('-'));
      let refMobileNumberSecondPart: string = refMobileNumber.substring(refMobileNumber.indexOf('-') + 1, refMobileNumber.length);

      this.fg.controls['references']['controls'][index]['controls']['refCountryCode'].patchValue(refCountryCode);
      this.fg.controls['references']['controls'][index]['controls']['refMobileNumber'].patchValue(refMobileNumberSecondPart);
    });
  }

  //In case of Edit to shift the Tab after save we use this function
  //This Function Emits the nameOfTheTab on which to move
  @Output() shiftTab = new EventEmitter<object>();
  shiftToNextTab(tab) {
    this.shiftTab.emit(tab);
  }
  selectByDefaultCountry() {
    let pak: ConfigurationModel;
    this.countries.forEach(country => {
      if (country.value.toLowerCase() === 'pakistan') {
        pak = country;
      }
    });
    if (pak) {
      this.loadProvinces(pak.id)
      this.fg.get('countryId').patchValue(pak.id);
    }
  }
}
