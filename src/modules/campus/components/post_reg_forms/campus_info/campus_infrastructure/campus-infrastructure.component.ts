import { Component, OnInit, Input } from '@angular/core';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { GLOBALS } from 'modules/app/config/globals';
import { EvaluationSheetService } from 'modules/settings/services';
import { EvaluationSheetModel } from 'modules/settings/models';
import { CampusPostRegistrationService, CRAAdditionalInfoService } from 'modules/campus/services';

@Component({
  selector: 'campus-infrastructure',
  templateUrl: './campus-infrastructure.component.html',
  styleUrls: ['./campus-infrastructure.component.css']
})
export class CampusInfrastructureComponent implements OnInit {

  @Input() fg: FormGroup;
  @Input() staffAndStudentFields: boolean = true;
  @Input() crApplicantId: number;
  @Input() isSubmit: boolean;

  public ownRentedCondition: boolean;
  public pageAct;
  public campusEvaluationSheet: EvaluationSheetModel;

  building = GLOBALS.CRBuildingType;
  pageActions = GLOBALS.pageActions;

  public questionsArray = [];

  constructor(
    public esService: EvaluationSheetService,
    private route: ActivatedRoute,
    private campusPostRegistrationService: CampusPostRegistrationService,
    private craAdditionalInfoService: CRAAdditionalInfoService,
  ) {
    this.pageAct = route.snapshot.data['act'];

  }

  ngOnInit() {

    if (this.pageAct == this.pageActions.create) {
      
      this.getCampusAdditionalInfo();
      if (this.fg.value.buildingType == 'own') {
        this.ownRentedCondition = false;
      }
    }
    else {
      this.getCampusAdditionalInfo();
      if (this.fg.value.buildingType == 'own') {
        this.ownRentedCondition = false;
      }
    }
  }
  ownRentedChange(event) {

    this.fg.value.buildingType = event.value;
    if (event.value == this.building.lease || event.value == this.building.rented) {
      this.ownRentedCondition = true;
    } else {
      this.ownRentedCondition = false;
    }

  }
  Validate() {
    this.fg.get('openArea').setValue(this.fg.get('totalArea').value - this.fg.get('coverdArea').value)

  }


  // Function to get Campus Additional Information.
  getCampusAdditionalInfo() {

    this.campusPostRegistrationService.findCRAAdditionalInfo(this.crApplicantId).subscribe(
      response => {


        // if Records exists in CRAAdditonalInfo agianst Application ID
        if (response != null) {

          if (response.length != 0) {
            // Create an array to display item 3 in a row.
            this.campusEvaluationSheet = response
            let questionMainAry = [];
            let questionSubAry = [];

            if (this.campusEvaluationSheet) {


              response.forEach(question => {

                if (question['optionType'] == 'selection') {
                  let options = ['Yes', 'No'];
                  question['options'] = options;
                  question['optionsCSV'] = 'Yes,No';

                }
                else {
                  question['optionsCSV'] = '';
                }


                // Make a set of 3 records in one index
                if (questionSubAry.length < 3) {
                  questionSubAry.push(question);
                }
                else {
                  questionMainAry.push(questionSubAry);

                  questionSubAry = [];
                  questionSubAry.push(question);
                }

              });

            }

            if (questionSubAry.length > 0) {
              questionMainAry.push(questionSubAry);
            }

            this.questionsArray = questionMainAry;
          }
          else {
            this.getCampusAdditionalInfoFromES();
          }


        }

        // If records not exists agianst Application ID
        else {
          this.getCampusAdditionalInfoFromES();
        }


      },
      error => console.log(error),
      () => {


      }
    );
  }

  //Function to get Campus Additional Information from Evaluation Sheet.
  getCampusAdditionalInfoFromES() {

    this.esService.findCampusEvaluationSheet().subscribe(
      response => {

        this.campusEvaluationSheet = response

      },
      error => console.log(error),
      () => {

        let questionMainAry = [];
        let questionSubAry = [];

        if (this.campusEvaluationSheet) {

          this.campusEvaluationSheet['sections'].forEach((section, index) => {

            section.questions.forEach(question => {

              if (question['optionType'] == 'selection') {
                let options = ['Yes', 'No'];
                question['options'] = options;
                question['optionsCSV'] = 'Yes,No';
              }
              else {
                question['optionsCSV'] = '';
              }

              // Make a set of 3 records in one index
              if (questionSubAry.length < 3) {
                questionSubAry.push(question);
              }
              else {
                questionMainAry.push(questionSubAry);

                questionSubAry = [];
                questionSubAry.push(question);
              }

            });
          });
        }

        if (questionSubAry.length > 0) {
          questionMainAry.push(questionSubAry);
        }

        this.questionsArray = questionMainAry;
      }
    );

  }

  inputChange() {
    // This service sends data in Campus Info Component.
    this.craAdditionalInfoService.craAdditionalInfo(this.questionsArray);
  }
}
