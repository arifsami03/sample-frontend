import { Component, OnInit, Input } from '@angular/core';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { InstituteTypeService, EducationLevelService } from '../../../../../institute/services';
import { CRProgramService } from '../../../../services';
import { InstituteTypeModel, EducationLevelModel } from '../../../../../institute/models';

import { CampusInformationModel } from '../../../../models';

import * as _ from 'lodash';
import { GLOBALS } from 'modules/app/config/globals';


@Component({
  selector: 'create-campus',
  templateUrl: './create-campus.component.html',
  styleUrls: ['./create-campus.component.css']
})
export class CreateCampusComponent implements OnInit {
  public buildingType: string = 'available';
  public pageAct;
  public pageActions = GLOBALS.pageActions;
  instituteTypes: InstituteTypeModel[];
  educationLevels: EducationLevelModel[];

  @Input() fg: FormGroup;
  @Input() camModel: CampusInformationModel;
  @Input() isSubmit: boolean = false;
  @Input() crApplicantId: number;
  constructor(
    private instituteTypeService: InstituteTypeService,
    private educationLevelService: EducationLevelService,
    private campusProgramService: CRProgramService,
    private route: ActivatedRoute,
    
  ) {
    this.pageAct = route.snapshot.data['act'];
   }
  ngOnInit() {

    if(this.pageAct ==  this.pageActions.create){
      // this.getInstituteTypes();
      this.fg.controls['instituteTypeId'].setValue(this.camModel.instituteTypeId);
      this.fg.controls['educationLevelId'].setValue(this.camModel.educationLevelId);
      this.getEducationLevels(this.camModel.instituteTypeId);
      // this.getAllEducationLevels();
    }
    else{
      this.fg.controls['instituteTypeId'].setValue(this.camModel.instituteTypeId);
      this.fg.controls['educationLevelId'].setValue(this.camModel.educationLevelId);
      this.getEducationLevels(this.camModel.instituteTypeId);
    }
    
    
    this.getInstituteTypes();

  }

  getInstituteTypes() {
    this.instituteTypeService.findAttributesList().subscribe(
      response => {
        this.instituteTypes = response;
      },
      error => console.log(error),
      () => {
      }
    );
  }

  getAllEducationLevels(){
    this.educationLevelService.findAttributesList().subscribe(
      response => {
        this.educationLevels = response;
      },
      error => console.log(error),
      () => { }
    );
  }

  getEducationLevels(instituteTypeId: number) {

    this.educationLevelService.findAllEducationLevels(instituteTypeId).subscribe(
      response => {
        this.educationLevels = response;
      },
      error => console.log(error),
      () => { }
    );
  }

  loadEducationLevels(event) {
    this.fg.controls.educationLevelId.setValue(false);
    this.campusProgramService.setEducationLevelId(0);
    this.getEducationLevels(event.value);
  }

  loadPrograms(event) {
    this.campusProgramService.setEducationLevelId(event.value);
  }

 
}
