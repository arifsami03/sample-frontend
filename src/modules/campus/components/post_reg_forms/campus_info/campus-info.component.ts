import { GLOBALS } from '../../../../app/config/globals';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatRadioChange } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

import { CampusInformationModel } from '../../../models';
import { LayoutService } from '../../../../layout/services';
import { CampusInfoService, CRProgramService, CRApplicantService, CampusPostRegistrationService, CRAAdditionalInfoService } from '../../../services';
import { InstituteTypeService } from '../../../../institute/services';
import { InstituteTypeModel } from '../../../../institute/models';
import * as _ from 'lodash';
import { ConfigurationService } from '../../../../shared/services';
@Component({
  selector: 'campus-info',
  templateUrl: './campus-info.component.html',
  styleUrls: ['./campus-info.component.css']
})
export class CampusInfoComponent implements OnInit {
  public campusType: string = 'create';
  public pageActions = GLOBALS.pageActions;
  public subscription: Subscription;
  public loaded: boolean = false;
  public boxLoaded: boolean = false;
  public id: number;
  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string = 'Campus Information';
  public campusInfo: CampusInformationModel;
  public states: string[] = ['Punjab', 'Sindh', 'Balochistan', 'KPK', 'Gilgit'];
  public isSubmit: boolean = true;
  @Input() campusApplicantId: number;
  @Input() createdCRApplicationId: number;
  @Input() type: '';
  public remittanceStatus: boolean = false;


  public mobileMask = GLOBALS.masks.mobile;

  public cnicMask = GLOBALS.masks.cnic;

  public componentLabels = CampusInformationModel.attributesLabels;
  public userId: number;
  public applicantId: number;
  public applicationId: number;
  public campusPrograms = [];
  public instituteTypes: InstituteTypeModel[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private layoutService: LayoutService,
    private campusInfoService: CampusInfoService,
    public applicantService: CRApplicantService,
    private campusProgramService: CRProgramService,
    private campusPostRegistrationService: CampusPostRegistrationService,
    private instituteTypeService: InstituteTypeService,
    public configurationService: ConfigurationService,
    public craAdditionalInfoService: CRAAdditionalInfoService,

  ) {
    this.subscription = this.campusProgramService.getCRPrograms().subscribe(message => {
      this.campusPrograms = message.campusProgram;
    });
    this.pageAct = route.snapshot.data['act'];
    this.route.params.subscribe(param => {
      this.applicationId = param.id;
    });
  }

  /**
   * ngOnInit
   */

  ngOnInit() {

    this.craAdditionalInfoService.craAdditionalInfo$.subscribe(response => this.craAdditionInfo = response)
    this.layoutService.setPageTitle({ title: this.pageTitle });
    this.initializePage();
    this.getCityId();

    if (this.pageAct == this.pageActions.create) {
      this.applicationId = this.createdCRApplicationId;
      this.getApplicantIdByApplicationId();
    }
    else {


      if (this.pageAct == this.pageActions.update) {
        this.getApplicantIdByApplicationId();
      }
      else {
        this.getApplicantId();
      }

    }




  }

  getApplicantIdByApplicationId() {

    return this.applicantService.getApplicantIdByApplicationId(this.applicationId).subscribe(
      response => {

        this.applicantId = response['CRApplicantId'];
        this.getCampusInfo();
        this.getRegistrationStatus();

      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  getApplicantId() {
    this.userId = Number(localStorage.getItem('id'));
    return this.applicantService.getApplicantId(this.userId).subscribe(
      response => {

        this.applicantId = response['CRApplicantId'];
        this.getCampusInfo();;
        this.getRegistrationStatus();

      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }
  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }
  private initializePage() {
    this.fg = this.fb.group(new CampusInformationModel().validationRules());


  }

  public resetForm(event: MatRadioChange) {
    // this.fg = this.fb.group(new CampusInformationModel().validationRules());
    // this.fg.patchValue(this.campusInfo)
  }



  /**
   * Function to get Campus owner id from user Table
   */
  getCampusInfo() {
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.applicantId;
    this.campusInfoService.findCampusInfo(this.campusApplicantId).subscribe(
      response => {
        this.campusInfo = response;
        this.id = response['id'];

        this.fg.patchValue(this.campusInfo);
        if (this.campusInfo.campusName == '0') {
          this.fg.controls['campusName'].setValue('');
        }

        if (response['applicationType'] == 'create') {
          this.fg.controls['campusType'].setValue(true);
          if (!this.campusInfo.campusName) {
            this.getInstituteTypes();
          }
        } else {
          this.fg.controls['campusType'].setValue(false);
          this.transferCampusConf();
        }
        this.getRegistrationStatus();
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  getEmailErrorMessage() {
    return this.fg.get('email').hasError('required') ? 'Email is Required' : this.fg.get('email').hasError('email') ? 'Not a valid email' : '';
  }

  public craAdditionInfo = [];

  public saveData(item: CampusInformationModel) {

    this.boxLoaded = true;

    item['craAdditionalInfo'] = this.craAdditionInfo
    if (item.campusType) {
      item['applicationType'] = 'create';
    } else {
      item['applicationType'] = 'transfer';
    }

    item['campusPrograms'] = this.campusPrograms;


    this.campusInfoService.update(this.id, item).subscribe(
      response => {
        this.boxLoaded = false;
        this.checkremittanceStatus().then(isShow => {
          if (isShow) {
            if (this.pageAct == this.pageActions.update || this.createdCRApplicationId != null) {
              this.shiftToNextTab('remittance');
            }
            else {
              this.router.navigate(['/post-registration/campus/remittance']);
            }
          } else {
            if (this.pageAct == this.pageActions.update || this.createdCRApplicationId != null) {
              this.shiftToNextTab('submit');
            }
            else {
              this.router.navigate(['/post-registration/campus/submit']);
            }
          }
        });

      },
      error => {
        this.boxLoaded = false;
        this.checkremittanceStatus().then(isShow => {
          if (isShow) {
            if (this.pageAct == this.pageActions.update || this.createdCRApplicationId != null) {
              this.shiftToNextTab('remittance');
            }
            else {
              this.router.navigate(['/post-registration/campus/remittance']);
            }
          } else {
            if (this.pageAct == this.pageActions.update || this.createdCRApplicationId != null) {
              this.shiftToNextTab('submit');
            }
            else {
              this.router.navigate(['/post-registration/campus/submit']);
            }
          }
        });
      }
    );


  }

  getRegistrationStatus() {
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.applicantId;
    this.campusPostRegistrationService.getRegistrationStatus(this.campusApplicantId).subscribe(
      res => {
        if (res['state'] != 'draft' && res['WFId'] == 'post-registration') {
          this.isSubmit = true;
          this.fg.disable();
        } else {
          this.fg.enable();
          this.isSubmit = false;
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  //In case of Edit to shift the Tab after save we use this function
  //This Function Emits the nameOfTheTab on which to move
  @Output() shiftTab = new EventEmitter<object>();
  shiftToNextTab(tab) {
    this.shiftTab.emit(tab);
  }
  getInstituteTypes() {
    this.instituteTypeService.findAttributesList().subscribe(
      response => {
        this.instituteTypes = response;
        this.setCampusName(+this.campusInfo.instituteTypeId);
      },
      error => console.log(error),
      () => {
      }
    );
  }
  cityId: number;
  getCityId() {
    this.configurationService.findAttributesList('city').subscribe(
      response => {
        this.cityId = response[0].id;
      },
      error => console.log(error),
      () => {
      }
    );
  }

  /**
    * Setting campus name which is our education level
    * @param id education level id
    */
  setCampusName(id: number) {
    let intituteType: InstituteTypeModel = _.find(this.instituteTypes, { id: id });
    if (intituteType) {
      this.fg.get('campusName').patchValue(intituteType.name)

    }
  }
  newCampusConf() {
    if (!this.campusInfo.campusName) {
      this.getInstituteTypes();
    }
  }
  transferCampusConf() {
    this.fg.get('campusName').patchValue(this.campusInfo.campusName);
    if (!this.campusInfo.campusName) {
      this.fg.get('campusName').patchValue('');
    }
  }
  public checkremittanceStatus() {

    return new Promise((resolve, reject) => {
      // finding remittance
      this.campusPostRegistrationService.findRemittanceStatus(this.campusApplicantId).subscribe(
        response => {
          if (response) {
            this.remittanceStatus = response.remittanceStatus;
            resolve(this.remittanceStatus);
          }
        },
        error => {
          console.log(error);
          resolve(error);
        }
      );
    })
  }
}
