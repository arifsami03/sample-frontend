import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {
  NgControl,
  FormArray,
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { MapComponent } from '../../../map/map.component';
import { CampusInformationModel } from 'modules/campus/models';
import { CampusPostRegistrationService, CRApplicantService } from 'modules/campus/services';
import { ConfigurationService } from 'modules/shared/services';
import { ConfigurationModel } from 'modules/shared/models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'campus-address',
  templateUrl: './campus-address.component.html',
  styleUrls: ['./campus-address.component.css'],
  providers: [CRApplicantService, ConfigurationService]
})
export class CRAddressComponent implements OnInit {
  @Input() buildingAvailable: boolean;

  @Input() fg: FormGroup;
  @Input() camModel: CampusInformationModel;
  @Input() isSubmit: boolean = false;
  @Input() crApplicantId: number;
  private cityLocation: string;
  private pageActions = GLOBALS.pageActions;
  private pageAct;
  public city: ConfigurationModel;

  private formArrayAdresses: FormArray;
  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private campusPostRegistrationService: CampusPostRegistrationService,
    public configurationService: ConfigurationService,
    public applicantService: CRApplicantService,


  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  ngOnInit() {

    this.initializePage();
    this.getApplicant();
  }

  private initializePage() {

    this.fg.controls['addresses'] = this.fb.array([]);

    if (this.buildingAvailable) {
      this.pushAddres(1);
    } else {
      this.pushAddres(2);
    }
    // this.fg.patchValue(this.camModel);
  }

  pushAddres(num) {

    let addressssssss = this.camModel['addresses'];

    for (let c = 0; c < num; c++) {
      this.formArrayAdresses = this.fg.get('addresses') as FormArray;

      let addressItem = addressssssss[c] ? addressssssss[c] : {};

      this.formArrayAdresses.push(this.createAddressItem(addressItem));
    }

  }

  createAddressItem(addressItem): FormGroup {
    return this.fb.group({
      id: [addressItem['id']],
      address: [addressItem['address']],
      longitude: [addressItem['longitude']],
      latitude: [addressItem['latitude']]
    });
  }

  /**
   * Open Popup to add Location
   */
  addLocationClick(i, item) {

    let dialogRef = this.dialog.open(MapComponent, {
      width: '800px',
      height: '400px',
      data: {
        lat: this.formArrayAdresses.at(i).get('latitude').value, lng: this.formArrayAdresses.at(i).get('longitude').value, cityLocation: this.cityLocation
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let addresses = {
          latitude: result.lat,
          longitude: result.lng
        };

        this.formArrayAdresses.at(i).patchValue({
          latitude: addresses.latitude,
          longitude: addresses.longitude
        });
        this.addressChange(i, item);

        //  this.fg.patchValue(addresses);
      }
    });
  }

  addressChange(i, item) {
    this.fg.value.addresses[i]['address'] = item.value['address'];
    this.fg.value.addresses[i]['longitude'] = item.value['longitude'];
    this.fg.value.addresses[i]['latitude'] = item.value['latitude'];
  }
  getApplicant() {
    this.applicantService.findPersonInfo(this.crApplicantId).subscribe(response => {
      if (response.cityId) {
        this.getCity(response.cityId);
      }
    },
      error => console.log(error),
      () => {

      })
  }
  /**
   * Get List of Cities
   */
  getCity(parentKeyId: number) {
    //this.loaded = false;
    this.configurationService.getOneRecord('city', parentKeyId).subscribe(
      response => {
        this.city = response;
        this.cityLocation = this.city.value
      },
      error => {
        console.log(error);
      },
      () => {
      }
    );
  }

}
