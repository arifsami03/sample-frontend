import { Component, OnInit, Input } from '@angular/core';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { InstituteTypeService, EducationLevelService } from 'modules/institute/services';
import { CRProgramService } from 'modules/campus/services';
import { CampusInformationModel } from 'modules/campus/models';
import { InstituteTypeModel, EducationLevelModel } from 'modules/institute/models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'campus-transfer',
  templateUrl: './campus-transfer.component.html',
  styleUrls: ['./campus-transfer.component.css']
})
export class CampusTransferComponent implements OnInit {
  @Input() fg: FormGroup;
  @Input() isSubmit: boolean = false;

  @Input() camModel: CampusInformationModel;
  @Input() crApplicantId: number;

  public deletedAffiliationIds: number[] = [];
  public pageActions=GLOBALS.pageActions;
  public pageAct;
  instituteTypes: InstituteTypeModel[];
  educationLevels: EducationLevelModel[];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,    
    private instituteTypeService: InstituteTypeService,
    private educationLevelService: EducationLevelService,
    private campusProgramService: CRProgramService
  ) {
    this.pageAct = route.snapshot.data['act'];
    
   }
  ngOnInit() {

    if(this.pageAct ==  this.pageActions.create){
      
      // this.getAllEducationLevels();
      // this.fg.controls['affiliations'] = this.fb.array([]);
      // this.addAffiliationClick();
      this.fg.controls['instituteTypeId'].setValue(this.camModel.instituteTypeId);
      this.fg.controls['educationLevelId'].setValue(this.camModel.educationLevelId);
  
      this.fg.controls['affiliations'] = this.fb.array([]);
      if (this.camModel['affiliations'].length == 0) {
        this.addAffiliationClick();
      } else {
        for (var i = 0; i < this.camModel['affiliations'].length; i++) {
          this.addAffiliationClick();
        }
        this.fg.patchValue(this.camModel);
      }
      this.getInstituteTypes();
    }
    else{

      this.fg.controls['instituteTypeId'].setValue(this.camModel.instituteTypeId);
      this.fg.controls['educationLevelId'].setValue(this.camModel.educationLevelId);
  
      this.fg.controls['affiliations'] = this.fb.array([]);
      if (this.camModel['affiliations'].length == 0) {
        this.addAffiliationClick();
      } else {
        for (var i = 0; i < this.camModel['affiliations'].length; i++) {
          this.addAffiliationClick();
        }
        this.fg.patchValue(this.camModel);
      }
      this.getInstituteTypes();

    }
    
    
  }

  getAllEducationLevels(){
    this.educationLevelService.findAttributesList().subscribe(
      response => {
        this.educationLevels = response;
      },
      error => console.log(error),
      () => { }
    );
  }

  createAffiliationItem(): FormGroup {
    return this.fb.group({
      affiliation: '',
      id: ''
    });
  }
  deleteAffiliationItem(id): FormGroup {
    return this.fb.group({
      id: id
    });
  }

  addAffiliationClick(): void {
    let affiliation = this.fg.get('affiliations') as FormArray;
    affiliation.push(this.createAffiliationItem());
  }
  removeAffiliationLink(i: number, item) {
    let deletedIds = this.fg.get('deletedAffiliationIds') as FormArray;
    deletedIds.push(this.deleteAffiliationItem(item.value.id));
    const control1 = <FormArray>this.fg.controls['affiliations'];
    control1.removeAt(i);
  }

  getInstituteTypes() {
    this.instituteTypeService.findAttributesList().subscribe(
      response => {
        this.instituteTypes = response;
      },
      error => console.log(error),
      () => {
        this.getEducationLevels(this.camModel.instituteTypeId);
      }
    );
  }

  getEducationLevels(instituteTypeId: number) {

    this.educationLevelService.findAllEducationLevels(instituteTypeId).subscribe(
      response => {
        this.educationLevels = response;
      },
      error => console.log(error),
      () => { }
    );
  }

  loadEducationLevels(event) {
    this.fg.controls.educationLevelId.setValue(false);
    this.campusProgramService.setEducationLevelId(0);
    this.getEducationLevels(event.value);
  }

  loadPrograms(event) {
    this.campusProgramService.setEducationLevelId(event.value);
  }
}
