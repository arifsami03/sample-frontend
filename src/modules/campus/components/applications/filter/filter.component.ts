import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';

/** 
 * 
 * How to use: 
 *  HTML:
 *    <filter class="filter-box" [filterOptions]="filterOptions" (filterDataChange)="filterData($event);"></filter>
 *  TS:
 *  this.filterOptions = {
        search: {
          labels: CRApplication.attributesLabels,
          fields: [
            { field: 'campusName', input: 'filed' },
            { field: 'tentativeSessionStart', input: 'datepicker' },
            { field: 'applicationType', input: 'dropdown', options: [{ lbl: 'Create', val: 'create' }, { lbl: 'Transfer', val: 'transfer' }]},
            { field: 'buildingAvailable', input: 'dropdown', options: [{ lbl: 'Yes', val: 1 }, { lbl: 'No', val: '' }] },
            { field: 'campusLocation', input: 'filed' },
          ]
        },
        filter: [
          {
            heading: 'State', field: 'stateId', options: [
              { title: 'Awaiting', val: 20 },
              { title: 'Analysis', val: 21 },
              { title: 'Approved', val: 27 },
              { title: 'Reject', val: 28 },
            ]
          },
          {
            heading: 'Building Available', field: 'buildingAvailable', options: [
              { title: 'Yes', val: 1 },
              { title: 'No', val: 0 },
            ]
          },
          {
            heading: 'Application Type', field: 'applicationType', options: [
              { title: 'New Campus', val: 'create' },
              { title: 'Transfer Campus', val: 'transfer' }
            ]
          },
        ]
      }  
*/
@Component({
  selector: 'filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css'],
})
export class FilterComponent {

  // Filter options for filter & search.
  @Input() filterOptions: object;

  @Output() filterDataChange = new EventEmitter<object>();

  public searchIt: true;

  // To manage selected filter/search into one object
  public filterParams = {};

  // To manage search into a separat array
  public searchAry = [];


  constructor() {
    this.searchAry.push({});
  }

  /**
   * When filter option is clicked.
   * 
   * @param field string
   * @param value string
   */
  public filterData(field, value) {

    field = 'f_' + field;

    if (this.filterParams[field] && (this.filterParams[field] == value)) {

      delete this.filterParams[field];

    } else {
      this.filterParams[field] = value;
    }

    this.filterDataChange.emit(this.filterParams);
  }

  /**
   * When dropdown of fileds in search is changed, we need to save input type, options and re-set value to null.
   * So we can show the regarding input type.
   * 
   * @param evt $event
   * @param saItem object
   */
  public dropDownChange(evt, saItem) {

    this.filterOptions['search']['fields'].forEach(o => {

      if (o['field'] == evt.value) {
        saItem['input'] = o['input'];
        saItem['options'] = o['options'];
        saItem['val'] = '';
      }

    })
  }

  /** 
   * Add more search items
   * 
  */
  addMore() {
    this.searchAry.push({});
  }

  /**
   * Remove/delete search item.
   * 
   * @param index number
   */
  delItem(index) {
    this.searchAry.splice(index, 1);
  }

  /** 
   * When search button is clicked then sum up everything into one objct and emit back to parent.
   * 
  */
  public search() {

    let fm = {};

    Object.assign(fm, this.filterParams);

    if (this.searchAry.length > 0) {

      this.searchAry.forEach((item) => {
        if (item['key'] && item['val']) {
          let k = 's_' + item['key']
          fm[k] = item['val'];
        }
      })
    }

    this.filterDataChange.emit(fm);
  }
}
