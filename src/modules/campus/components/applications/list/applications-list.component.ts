import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';

import { PageAnimation } from '../../../../shared/helper';
import { LayoutService } from '../../../../layout/services';
import { GLOBALS } from '../../../../app/config/globals';
import { CRApplication } from '../../../../public/models';
import { ApplicationService } from '../../../services';

@Component({
  selector: 'applications-list',
  templateUrl: './applications-list.component.html',
  styleUrls: ['./applications-list.component.css'],
  animations: [PageAnimation]
})
export class ApplicationListComponent implements OnInit {

  public pageState;
  public loaded: boolean = false;

  public filterOptions;
  public filterParams = {};

  public applicationList: CRApplication[] = [new CRApplication()];
  public attrLabels = CRApplication.attributesLabels;

  public dataSource: any;
  public displayedColumns = ['campusName', 'levelOfEducation', 'tentativeSessionStart', 'cityId', 'stateId', 'options'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private aplService: ApplicationService,
    public layoutService: LayoutService,
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {

    let stateOptions = [
      { lbl: 'Awaiting', val: 20 },
      { lbl: 'Analysis', val: 21 },
      { lbl: 'Forward to Admin', val: 22 },
      { lbl: 'Create MOU', val: 23 },
      { lbl: 'MOU Approval', val: 24 },
      { lbl: 'New Campus', val: 25 },
      { lbl: 'Account Info', val: 26 },
      { lbl: 'Approved', val: 27 },
      { lbl: 'Reject', val: 28 },
    ]

    // CRApplication.attributesLabels
    this.filterOptions = {
      search: {
        labels: CRApplication.attributesLabels,
        fields: [
          { field: 'campusName', input: 'filed' },
          { field: 'tentativeSessionStart', input: 'datepicker' },
          { field: 'campusLocation', input: 'filed' },
          { field: 'applicationType', input: 'dropdown', options: [{ lbl: 'Create', val: 'create' }, { lbl: 'Transfer', val: 'transfer' }] },
          { field: 'buildingAvailable', input: 'dropdown', options: [{ lbl: 'Yes', val: 1 }, { lbl: 'No', val: '' }] },
          { field: 'stateId', input: 'dropdown', options: stateOptions },
          { field: 'campusLocation', input: 'filed' },
        ]
      },
      filter: [
        {
          heading: 'State', field: 'stateId', options: [
            { title: 'Awaiting', val: 20 },
            { title: 'Analysis', val: 21 },
            { title: 'Forward to Admin', val: 22 },
            { title: 'Create MOU', val: 23 },
            { title: 'MOU Approval', val: 24 },
            { title: 'New Campus', val: 25 },
            { title: 'Account Info', val: 26 },
            { title: 'Approved', val: 27 },
            { title: 'Reject', val: 28 },
          ]
        },
        {
          heading: 'Building Available', field: 'buildingAvailable', options: [
            { title: 'Yes', val: 1 },
            { title: 'No', val: 0 },
          ]
        },
        {
          heading: 'Application Type', field: 'applicationType', options: [
            { title: 'New Campus', val: 'create' },
            { title: 'Transfer Campus', val: 'transfer' }
          ]
        },
      ]
    }


    this.layoutService.setPageTitle({ title: 'Applications' });
    this.getRecords();
  }

  /**
   * Get all records
   */
  getRecords(): void {

    this.aplService.index(this.filterParams).subscribe(response => {

      this.applicationList = response;

      this.dataSource = new MatTableDataSource<CRApplication>(this.applicationList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      error => {
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {

        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  /**
   * Search in Grid
   * 
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  public filterData(fp) {

    this.filterParams = fp;
    this.getRecords();

  }

}
