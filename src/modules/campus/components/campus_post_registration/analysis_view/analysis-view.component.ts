import { GLOBALS } from '../../../../app/config/globals';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { PageAnimation } from '../../../../shared/helper';
import { LayoutService } from '../../../../layout/services';
import { CampusPostRegistrationService } from '../../../services';

import { CampusInformationModel } from '../../../models';

@Component({
  selector: 'campus-analysis-view',
  templateUrl: './analysis-view.component.html',
  styleUrls: ['./analysis-view.component.css'],
  animations: [PageAnimation]
})
export class AnalysisViewComponent implements OnInit {
  public loaded: boolean = false;
  public pageState = 'active';
  public campus: CampusInformationModel;
  public componentLabels = CampusInformationModel.attributesLabels;
  public selectedButton: string = 'application';

  constructor(
    private campusPostRegistrationService: CampusPostRegistrationService,
    public layoutService: LayoutService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {

    this.layoutService.setPageTitle({
      title: 'Application'
    });

    this.activatedRoute.params.subscribe(params => {
      this.getData(params['id']);
    });
  }

  getData(id: number) {

    this.campusPostRegistrationService.find(id).subscribe(
      response => {
        this.campus = response;

        this.loaded = true;
      },
      error => {
        this.loaded = true;
      },
      () => {

      }
    );
  }

  tabChange(type: string) {
    this.selectedButton = type;
    this.layoutService.setPageTitle({
      title: 'Application'
    });
  }

}
