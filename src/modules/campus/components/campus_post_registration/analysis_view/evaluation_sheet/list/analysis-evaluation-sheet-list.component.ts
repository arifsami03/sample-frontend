
import { Promise } from 'bluebird';
import { GLOBALS } from '../../../../../../app/config/globals';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { PageAnimation } from '../../../../../../shared/helper';
import { CampusPostRegistrationService } from '../../../../../services';

import { CREvaluationSheetModel } from '../../../../../models';
import { LayoutService } from '../../../../../../layout/services';

/**
 * This component is used for listing evaluation sheet for campus which are forwarded to admin after submiting by evaluator
 * and are no longer to be edit.
 */

@Component({
  selector: 'analysis-evaluation-sheet-list',
  templateUrl: './analysis-evaluation-sheet-list.component.html',
  styleUrls: ['./analysis-evaluation-sheet-list.component.css'],
  animations: [PageAnimation]
})
export class AnalysisEvaluationSheetListComponent implements OnInit {
  @Input() CRApplicationId: number;

  public pageState = 'active';
  public pageTitle = 'Evaluation Sheet';
  public sheetsLoaded: boolean = false;
  public questionsloaded: boolean = false;
  public comments = new FormControl();

  public previousEvaluationSheets: CREvaluationSheetModel[];
  public evaluationSheetModel: CREvaluationSheetModel;

  constructor(
    private campusPostRegistrationService: CampusPostRegistrationService,
    private layoutService: LayoutService,

  ) { }

  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: this.pageTitle });
    if (this.CRApplicationId) {
      this.findAllEvaluationSheets(this.CRApplicationId);
    }

  }

  findAllEvaluationSheets(id: number) {
    this.campusPostRegistrationService.findAllEvaluationSheets(id).subscribe(
      response => {
        this.previousEvaluationSheets = response;
        this.sheetsLoaded = true;

      },
      error => {
        this.sheetsLoaded = false;
      },
      () => {
      }
    );
  }

  findAllESSections(previousEvaluationSheetId: number) {
    let previousEvaluationSheet = this.previousEvaluationSheets.find(element => {
      return element.id == previousEvaluationSheetId;
    });

    if (previousEvaluationSheet && (previousEvaluationSheet.sections == undefined || previousEvaluationSheet.sections.length == 0)) {
      this.campusPostRegistrationService.findAllESSections(previousEvaluationSheetId).subscribe(
        response => {
          previousEvaluationSheet.sections = response;
          this.questionsloaded = true;
        },
        error => {
          console.log('error: ', error);
          this.questionsloaded = true;
        },
        () => {
        }
      );
    }
  }

  addComments(evaluationSheetId, item) {

    item.comments = this.comments.value;

    this.campusPostRegistrationService.updateCommentsOfEvaluationSheet(evaluationSheetId, item).subscribe(
      response => {
        // this.previousEvaluationSheets = response;
        this.layoutService.flashMsg({ msg: 'Comments have been added to evaluation sheet.', msgType: 'success' });
        this.ngOnInit();
        this.sheetsLoaded = true;

      },
      error => {
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        this.sheetsLoaded = false;
      },
      () => {
      }
    );

  }

}
