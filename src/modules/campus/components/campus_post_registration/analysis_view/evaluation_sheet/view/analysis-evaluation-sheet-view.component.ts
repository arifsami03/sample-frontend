import { AttachmentService } from '../../../../../../shared/services';

import { Promise } from 'bluebird';
import { GLOBALS } from '../../../../../../app/config/globals';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { PageAnimation } from '../../../../../../shared/helper';
import { CampusPostRegistrationService } from '../../../../../services';
import { DocStateManagerService } from '../../../../../../shared/services';
import { LayoutService } from '../../../../../../layout/services';

import { CREvaluationSheetModel } from '../../../../../models';

@Component({
  selector: 'analysis-evaluation-sheet-view',
  templateUrl: './analysis-evaluation-sheet-view.component.html',
  styleUrls: ['./analysis-evaluation-sheet-view.component.css'],
  animations: [PageAnimation]
})
export class AnalysisEvaluationSheetViewComponent implements OnInit {
  @Input() CRApplicationId: number;
  @Output() tabChange = new EventEmitter<string>();

  public pageState = 'active';
  public loaded: boolean = false;
  public boxLoaded: boolean = false;
  public stateId: number;
  public fg: FormGroup;

  public campusEvaluationSheet: CREvaluationSheetModel;

  constructor(
    private campusPostRegistrationService: CampusPostRegistrationService,
    private docStateManagerService: DocStateManagerService,
    private layoutService: LayoutService,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private attachmentService: AttachmentService
  ) {
  }

  ngOnInit(): void {
    if (!this.campusEvaluationSheet) {
      this.getData(this.CRApplicationId);
    }

    this.fg = this.fb.group(new CREvaluationSheetModel().validationRules());
  }

  getData(id: number) {
    this.campusPostRegistrationService.findEvaluationSheet(id).subscribe(
      response => {
        this.campusEvaluationSheet = response;
      },
      error => {
        this.loaded = true;
      },
      () => {
        if (this.campusEvaluationSheet) {
          this.campusEvaluationSheet.sections.forEach((section, index) => {
            section.questions.forEach(question => {
              let options = question.optionsCSV.split(',');
              question['options'] = options;
            });
          });
        }

        this.loaded = true;
        this.getCurrentStateId();
      }
    );
  }

  public saveData() {

    this.boxLoaded = true;

    this.campusPostRegistrationService.updateEvaluationSheet(this.campusEvaluationSheet.id, this.campusEvaluationSheet).subscribe(
      response => {
        this.boxLoaded = false;

        // Attachment Process
        this.attachmentService.uploadAttachments({ parent: 'CREvaluationSheet', parentId: this.campusEvaluationSheet.id, navigateUrl: null });
        this.layoutService.flashMsg({ msg: 'Evaluation Sheet has been saved Now, Forward it to admin.', msgType: 'success' });
      },
      error => {
        this.boxLoaded = false;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
      },
      () => { }
    );
  }

  /**
   * Function to Get State ID of Post-Registration Work Flow
   */

  getCurrentStateId() {
    var postData = {
      CRApplicationId: this.CRApplicationId
    };
    this.docStateManagerService.getCurrentStateId(postData).subscribe(
      response => {
        this.stateId = response['stateId'];
      },
      error => console.log(error),
      () => { }
    );
  }

  changeESStatus(event) {
    if (event && event.status) {
      this.campusPostRegistrationService.updateESStatus(this.campusEvaluationSheet.id, []).subscribe(
        response => {
          this.router.navigate([`/campus/post-registrations`]);
        },
        error => {
          console.log(error);
        },
        () => { }
      );
    }
  }

  public cancel() {
    this.tabChange.emit('application');
  }
}
