import { Observable } from 'rxjs/Observable';
import { Component, OnInit, Input } from '@angular/core';
import { FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { PageAnimation } from '../../../../../shared/helper';
import { PersonalInformationModel } from '../../../../models';
import { LayoutService } from '../../../../../layout/services';
import { BankService } from '../../../../../settings/services';
import { ConfigurationService } from '../../../../../shared/services';
import { CRApplicantService, CampusPostRegistrationService } from '../../../../services';

import { ConfirmDialogComponent } from '../../../../../shared/components';

import { BankModel } from '../../../../../settings/models';
import { ConfigurationModel } from '../../../../../shared/models';
import { GLOBALS } from '../../../../../app/config/globals';
import { CampusPreRegistration } from '../../../../../public/models';

@Component({
  selector: 'personal-info-view',
  templateUrl: './personal-info-view.component.html',
  styleUrls: ['./personal-info-view.component.css'],
  providers: [ConfigurationService, CRApplicantService],
  animations: [PageAnimation]
})
export class PersonalInfoViewComponent implements OnInit {
  public pageState = 'active';
  public pageActions = GLOBALS.pageActions;
  @Input() campusApplicantId: number;
  public loaded: boolean = false;
  public id: number;
  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string = 'Personal Information';
  public personalInfo: PersonalInformationModel;
  public states: string[] = ['Punjab', 'Sindh', 'Balochistan', 'KPK', 'Gilgit'];
  public options: Observable<string[]>;
  public deletedReferenceIds: number[] = [];
  public isSubmit: boolean = true;
  public mobileMask = GLOBALS.masks.mobile;

  public cnicMask = GLOBALS.masks.cnic;

  public componentLabels = PersonalInformationModel.attributesLabels;

  public countries: ConfigurationModel[];
  public province: ConfigurationModel[];
  public cities: ConfigurationModel[];
  public tehsils: ConfigurationModel[];

  public natureOfwork: ConfigurationModel[];
  public bank: BankModel[];
  public userId: number;
  public bankId: number;
  public applicantId: number;
  public applicationId: number;
  public bankName: string = '';
  public bankFormControl: FormControl = new FormControl();
  public filteredOptions: Observable<string[]>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public configurationService: ConfigurationService,
    public bankService: BankService,
    public applicantService: CRApplicantService,
    private campusPostRegistrationService: CampusPostRegistrationService,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
    this.route.params.subscribe(param => {
      this.applicationId = param.id;
    });
  }

  /**
   * ngOnInit
   */

  ngOnInit() {

    this.layoutService.setPageTitle({ title: this.pageTitle });

    if(this.pageAct == this.pageActions.view){
      this.getApplicantIdByApplicationId();
      this.initializePage();
    }
    else
    {
      this.initializePage();
      // this.getLoginUserId();
      this.getApplicantId();
    }
   



    this.getNatureOfWorkList();
    this.getBankList();

   
  }
  getApplicantId() {
    this.loaded = false;

    this.userId = Number(localStorage.getItem('id'));
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.userId;

    return this.applicantService.getApplicantId(this.campusApplicantId).subscribe(
      response => {

        this.applicantId = response['CRApplicantId'];
        this.getApplicantInfo();
        this.getRegistrationStatus();

      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }
  

  /**
   *Checking validation of number
   * @param equalControlName
   */
  public greaterThan?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      let controlMatch = 0;
      return controlMatch <= control.value && 9999999999 >= control.value
        ? null
        : {
          equalTo: true
        };
    };
  }

  getApplicantIdByApplicationId() {
    
        this.loaded = false;
    
        return this.applicantService.getApplicantIdByApplicationId(this.applicationId).subscribe(
          response => {
    
            this.applicantId = response['CRApplicantId'];
            this.getApplicantInfo();
            this.getRegistrationStatus();
    
          },
          error => {
            console.log(error);
            this.loaded = true;
          },
          () => {
            this.loaded = true;
          }
        );
      }

  private initializePage() {
    //this.fg = this.fb.group(new PersonalInformationModel().validationRules());
    this.fg = this.fb.group({
      fullName: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      email: ['', [<any>Validators.required, Validators.email]],
      mobileNumber: ['', [<any>Validators.maxLength(16), Validators.pattern('([0-9]{4})-([0-9]{10})')]],
      address: ['', [<any>Validators.maxLength(100)]],
      cnic: ['', [<any>Validators.required]],
      countryId: [null],
      provinceId: [null],
      cityId: [null],
      tehsilId: [null],
      state: [''],
      zip: [''],
      country: [''],
      description: [''],
      nearestBankId: [''],
      approxMonthlyIncome: new FormControl('', [this.greaterThan('approxMonthlyIncome')]),
      natureOfWorkId: [''],
      references: this.fb.array([])
    });
    this.fg.addControl('bankFormControl', this.bankFormControl);
    this.fg.disable();
  }

  createReferenceItem(): FormGroup {
    return this.fb.group({
      id: '',
      refName: [''],
      refMobileNumber: ['', [<any>Validators.maxLength(15), Validators.pattern('([0-9]{4})-([0-9]{10})')]],
      refAddress: ['']
    });

  }

  addReferenceClick() {
    let references = this.fg.get('references') as FormArray;
    references.push(this.createReferenceItem());
    // disabling on submit
    //this.getRegistrationStatus();
  }

  /**
   * Function to get Login user id from Local Storage;
   */
  getLoginUserId() {
    this.applicantId = Number(localStorage.getItem('id'));
  }

  /**
   * Function to get Campus owner id from user Table
   */
  getApplicantInfo() {
    this.loaded = false;
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.applicantId;
    this.applicantService.findPersonInfo(this.applicantId).subscribe(
      response => {
        //this.province = response;
        this.personalInfo = response;
        this.getCountryList();
        if (this.personalInfo.countryId) {
          this.getProvinceList(this.personalInfo.countryId);
        }
        if (this.personalInfo.provinceId) {
          this.getCitiesList(this.personalInfo.provinceId);
        }
        if (this.personalInfo.cityId) {
          this.getTehsilsList(this.personalInfo.cityId);
        }

        this.id = response['id'];
        if (this.personalInfo['references'].length == 0) {
          this.addReferenceClick();
        }
        for (var i = 0; i < this.personalInfo['references'].length; i++) {
          this.addReferenceClick();
        }
        this.fg.patchValue(this.personalInfo);
        if (response['bankName'] != null) {
          this.bankFormControl.setValue(response['bankName']);
        } else {
          for (let i = 0; i < this.bank.length; i++) {
            if (this.bank[i].id == response['nearestBankId']) {
              this.bankFormControl.setValue(this.bank[i].name);
            }
          }
        }
        this.fg.disable();
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }




  /**
   * Get List of Countries
   */
  getCountryList() {
    this.configurationService.getAll(GLOBALS.configurationKeys.country).subscribe(
      response => {
        this.countries = response;
      },
      error => {
        console.log(error);
        // this.loaded = true;
      },
      () => {
      }
    );
  }

  loadProvinces(parentKeyId) {
    this.getProvinceList(parentKeyId);
    this.fg.controls['provinceId'].setValue(null);
    this.fg.controls['cityId'].setValue(null);
    this.fg.controls['tehsilId'].setValue(null);
  }

  loadCities(parentKeyId) {
    this.getCitiesList(parentKeyId);
    this.fg.controls['cityId'].setValue(null);
    this.fg.controls['tehsilId'].setValue(null);
  }

  loadTehsils(parentKeyId) {
    this.getTehsilsList(parentKeyId);
    this.fg.controls['tehsilId'].setValue(null);
  }

  /**
   * Get List of Provinces
   */
  getProvinceList(parentKeyId: number) {
    this.configurationService.findChildren(parentKeyId).subscribe(
      response => {
        this.province = response;
      },
      error => {
        console.log(error);
      },
      () => {

      }
    );
  }

  /**
   * Get List of Cities
   */
  getCitiesList(parentKeyId: number) {
    this.configurationService.findChildren(parentKeyId).subscribe(
      response => {
        this.cities = response;
      },
      error => {
        console.log(error);
      },
      () => {

      }
    );
  }

  /**
   * Get List of Tehsils
   */
  getTehsilsList(parentKeyId: number) {
    this.configurationService.findChildren(parentKeyId).subscribe(
      response => {
        this.tehsils = response;
      },
      error => {
        console.log(error);
      },
      () => {

      }
    );
  }

  /**
   * Get List of Nature of Work
   */
  getNatureOfWorkList() {
    this.configurationService.getAll(GLOBALS.configurationKeys.natureOfWork).subscribe(
      response => {
        this.natureOfwork = response;
      },
      error => {
        console.log(error);
      },
      () => { }
    );
  }

  /**
   * Get List of Banks
   */
  getBankList() {
    this.bankService.findAttributesList().subscribe(
      response => {
        this.bank = response;
        this.filteredOptions = this.bankFormControl.valueChanges.pipe(startWith(''), map(val => this.filter(val)));
      },
      error => {
        console.log(error);
        // this.loaded = true;
      },
      () => {
        // this.loaded = true;
      }
    );
  }

  removeReference(i: number, item) {
    this.deletedReferenceIds.push(item.value.id);
    const control = <FormArray>this.fg.controls['references'];
    control.removeAt(i);
  }
  getRegistrationStatus() {
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.applicantId;

    this.campusPostRegistrationService.getRegistrationStatus(this.applicantId).subscribe(
      res => {
        if (res['state'] != 'approve' && res['WFId'] != 'pre-registration') {
          this.isSubmit = true;
          this.fg.disable();
        } else {
          this.fg.enable();
          this.isSubmit = false;
        }
      },
      error => {
        console.log(error);
        // this.loaded = true;
      },
      () => {
        // this.loaded = true;
      }
    );
  }

  /**
   * Function to Filter Options of banks.
   */
  filter(val: string): any[] {

    if (this.bank) {
      if (val) {
        return this.bank.filter(option => option.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
      }
      else {
        return this.bank.filter(option => option.name.toLowerCase());

      }

    }
  }

  /**
   * Function to Validate Custom Bank
   *
   * @param {PersonalInformationModel} item model to add Bank Id or Bank name
   * @memberof PersonalInformationComponent
   */
  validateCustomBank(item: PersonalInformationModel) {
    for (let i = 0; i < this.bank.length; i++) {
      if (this.bank[i].name.toLowerCase().trim() == this.bankFormControl.value.toLowerCase().trim()) {
        this.bankName = null;
        this.bankId = this.bank[i].id;
        break;
      } else {
        this.bankName = this.bankFormControl.value;
        this.bankId = null;
      }
    }

    if (this.bankName != null) {
      this.bankId = null;
      this.bankName = this.bankFormControl.value;
    }

    item.nearestBankId = this.bankId;
    item.bankName = this.bankName;
  }
  selectByDefaultCountry() {
    let pak: ConfigurationModel;
    this.countries.forEach(country => {
      if (country.value.toLowerCase() === 'pakistan') {
        pak = country;
      }
    });
    if (pak) {
      this.loadProvinces(pak.id)
      this.fg.get('countryId').patchValue(pak.id);
    }
  }
}
