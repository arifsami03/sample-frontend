import { Component, OnInit, Input } from '@angular/core';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GLOBALS } from '../../../../../../app/config/globals';
import { EvaluationSheetService } from '../../../../../../settings/services';
import { EvaluationSheetModel } from '../../../../../../settings/models';
import { CampusPostRegistrationService, CRAAdditionalInfoService } from '../../../../../services';
@Component({
  selector: 'campus-infrastructure-view',
  templateUrl: './campus-infrastructure-view.component.html',
  styleUrls: ['./campus-infrastructure-view.component.css']
})
export class CampusInfrastructureViewComponent implements OnInit {
  @Input() fg: FormGroup;
  @Input() crApplicantId: number;
  @Input() staffAndStudentFields: boolean = true;
  public ownRentedCondition: boolean = true;
  building = GLOBALS.CRBuildingType;

  public questionsArray = [];
  public campusEvaluationSheet: EvaluationSheetModel;

  constructor(
    public esService: EvaluationSheetService,
    private campusPostRegistrationService: CampusPostRegistrationService,
    private craAdditionalInfoService: CRAAdditionalInfoService,
  ) { }
  ngOnInit() {
    this.getCampusAdditionalInfo();
    if (this.fg.value.buildingType) {
      this.ownRentedCondition = this.fg.value.buildingType;
      this.fg.disable();
    }
  }
  ownRentedChange(event) {

    this.fg.value.buildingType = event.value;
    if (event.value == this.building.lease || event.value == this.building.rented) {
      this.ownRentedCondition = true;
    } else {
      this.ownRentedCondition = false;
    }

  }

  // Function to get Campus Additional Information.
  getCampusAdditionalInfo() {

    this.campusPostRegistrationService.findCRAAdditionalInfo(this.crApplicantId).subscribe(
      response => {

        // if Records exists in CRAAdditonalInfo agianst Application ID
        if (response.length != 0) {

          // Create an array to display item 3 in a row.
          this.campusEvaluationSheet = response
          let questionMainAry = [];
          let questionSubAry = [];

          if (this.campusEvaluationSheet) {


            response.forEach(question => {

              if (question['optionType'] == 'selection') {
                let options = ['Yes', 'No'];
                question['options'] = options;
                question['optionsCSV'] = 'Yes,No';

              }
              else {
                question['optionsCSV'] = '';
              }


              // Make a set of 3 records in one index
              if (questionSubAry.length < 3) {
                questionSubAry.push(question);
              }
              else {
                questionMainAry.push(questionSubAry);

                questionSubAry = [];
                questionSubAry.push(question);
              }

            });

          }

          if (questionSubAry.length > 0) {
            questionMainAry.push(questionSubAry);
          }

          this.questionsArray = questionMainAry;
        }

        // If records not exists agianst Application ID
        else {
          this.getCampusAdditionalInfoFromES();
        }


      },
      error => console.log(error),
      () => {


      }
    );
  }

  //Function to get Campus Additional Information from Evaluation Sheet.
  getCampusAdditionalInfoFromES() {

    this.esService.findCampusEvaluationSheet().subscribe(
      response => {

        this.campusEvaluationSheet = response

      },
      error => console.log(error),
      () => {

        let questionMainAry = [];
        let questionSubAry = [];

        if (this.campusEvaluationSheet) {

          this.campusEvaluationSheet['sections'].forEach((section, index) => {

            section.questions.forEach(question => {

              if (question['optionType'] == 'selection') {
                let options = ['Yes', 'No'];
                question['options'] = options;
                question['optionsCSV'] = 'Yes,No';
              }
              else {
                question['optionsCSV'] = '';
              }

              // Make a set of 3 records in one index
              if (questionSubAry.length < 3) {
                questionSubAry.push(question);
              }
              else {
                questionMainAry.push(questionSubAry);

                questionSubAry = [];
                questionSubAry.push(question);
              }

            });
          });
        }

        if (questionSubAry.length > 0) {
          questionMainAry.push(questionSubAry);
        }

        this.questionsArray = questionMainAry;
      }
    );

  }
  inputChange() {
    // This service sends data in Campus Info Component.
    this.craAdditionalInfoService.craAdditionalInfo(this.questionsArray);
  }
}
