import { Component, OnInit, Input, ChangeDetectorRef, Output,EventEmitter } from '@angular/core';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

import { PageAnimation } from '../../../../../../shared/helper';
import { ProgramService, ProgramDetailsService } from '../../../../../../academic/services';
import { CRProgramService } from '../../../../../services';
import { ProgramModel, ProgramDetailsModel } from '../../../../../../academic/models';
import { CampusInformationModel } from '../../../../../models';
@Component({
  selector: 'campus-program-details-view',
  templateUrl: './campus-program-details-view.component.html',
  styleUrls: ['./campus-program-details-view.component.css'],
  animations: [PageAnimation]
})
export class CRProgramDetailsViewComponent implements OnInit {
  
  public pageState = 'active';

  @Input() fg: FormGroup;
  @Input() campusInfo: CampusInformationModel;
  @Input() isSubmit: boolean = false;
  @Output() loadPage :EventEmitter<any> = new EventEmitter();
  public programs: ProgramModel[] = [new ProgramModel()];
  public programDetails: ProgramDetailsModel[] = [new ProgramDetailsModel()];
  public programsWithProgramDetails = [];
  public loaded: boolean = false;

  public selected = [];

  public addButton: number = 0;
  public multiple: any = [];
  public campusPrograms: any = [];

  public subscription: Subscription;
  public educationLevelId: number;

  constructor(
    private fb: FormBuilder,
    private programService: ProgramService,
    private programDetailService: ProgramDetailsService,
    private campusProgramService: CRProgramService
  ) {
    this.subscription = this.campusProgramService.geteducationLevelId().subscribe(educationLevelId => {
      this.educationLevelId = educationLevelId;
      this.resetValues();
      this.campusProgramService.resetCRProgram();
      this.getProgramsWithProgramDetails(this.educationLevelId);
    });
  }

  ngOnInit() {
    this.educationLevelId = this.campusInfo.educationLevelId;

    this.campusInfo['cammpusProgramDetails'].forEach(element => {

      let programId = element['programId'];

      let programs = [];
      element.programDetails.forEach(programDetail => {
        programs.push(programDetail.programDetailId);
      });

      this.selected.push({ programId: programId, programs: programs });

    });

    this.getProgramsWithProgramDetails(this.educationLevelId);
  }

  getProgramsWithProgramDetails(educationLevelId: number) {
    this.programService.getProgramsWithProgramDetails(educationLevelId).subscribe(res => {
      this.programsWithProgramDetails = res;
      this.getProgramList(educationLevelId);
    });
  }

  resetValues() {
    this.selected = [];
    this.programsWithProgramDetails = [];
    this.programs = [];
    this.campusPrograms = [];
    this.programDetails = [];
    this.multiple = [];
    this.addButton = 0;
  }


  getProgramList(educationLevelId: number) {
    let selectedPrograms = [];

    this.selected.forEach(element => {
      selectedPrograms.push(element.programId);
    });

    this.programService.getAllPrograms(educationLevelId, selectedPrograms).subscribe(
      response => {
        this.programs = response;
        if (this.selected.length == 0) {
          this.multiple.push({ program: [], programId: '', programDetails: [] });
          for (var i = 0; i < this.programs.length; i++) {
            this.multiple[this.addButton]['program'].push({
              id: this.programs[i].id,
              name: this.programs[i].name
            });
          }
        } else {
          for (var i = 0; i < this.programsWithProgramDetails.length; i++) {
            let item = this.selected.find(element => {
              return element.programId == this.programsWithProgramDetails[i].programId;
            })
            if (item) {
              this.multiple.push({ program: [], programId: '', programDetails: [] });
              this.multiple[this.addButton][
                'programId'
              ] = this.programsWithProgramDetails[i].programId;
              for (var j = 0; j < this.programsWithProgramDetails.length; j++) {
                this.multiple[this.addButton]['program'].push({
                  id: this.programsWithProgramDetails[j].programId,
                  name: this.programsWithProgramDetails[j].programName
                });
              }
              for (var k = 0; k < this.programsWithProgramDetails[i]['programDetails'].length; k++) {
                var checked = true;

                if (
                  item.programs.includes(
                    this.programsWithProgramDetails[i]['programDetails'][k].id
                  )
                ) {
                  this.editChange(true, this.programsWithProgramDetails[i].programId, this.programsWithProgramDetails[i]['programDetails'][k].id);
                } else {
                  checked = false;
                  this.editChange(
                    false,
                    this.programsWithProgramDetails[i].programId,
                    this.programsWithProgramDetails[i]['programDetails'][k].id
                  );
                }
                this.multiple[this.addButton]['programDetails'].push({
                  id: this.programsWithProgramDetails[i]['programDetails'][k].id,
                  name: this.programsWithProgramDetails[i]['programDetails'][k].name,
                  checked: checked
                });
              }
              this.addButton = this.addButton + 1;
            }
          }

          if (
            this.programsWithProgramDetails.length != this.selected.length
          ) {
            this.multiple.push({ program: [], programId: '', programDetails: [] });
            for (var i = 0; i < this.programs.length; i++) {
              this.multiple[this.addButton]['program'].push({
                id: this.programs[i].id,
                name: this.programs[i].name
              });
            }
          }
        }
      },
      error => console.log(error),
      () => {
        this.emitFunction();
        this.loaded = true;
      }
    );
    return this.programs;
  }

  programChange(event) {
    let programId = event.value;
    this.selected.push({ programId: programId, programs: [] });
    this.programDetailService.getProgramProgramDetails(event.value).subscribe(
      response => {
        this.programDetails = response;
        for (var i = 0; i < this.programDetails.length; i++) {
          this.multiple[this.addButton]['programId'] = event.value;
          this.multiple[this.addButton]['programDetails'].push({
            id: this.programDetails[i].id,
            name: this.programDetails[i].name
          });
        }
        this.addMultiple(event.value);
      },
      error => console.log(error),
      () => { }
    );
  }

  addMultiple(programId) {
    this.addButton = this.addButton + 1;
    let selectedPrograms = [];

    this.selected.forEach(element => {
      selectedPrograms.push(element.programId);
    });
    this.programService.getAllPrograms(this.educationLevelId, selectedPrograms).subscribe(
      response => {
        this.programs = response;
        if (this.programs.length != 0) {
          this.multiple.push({ program: [], programId: '', programDetails: [] });
          for (var i = 0; i < this.programs.length; i++) {
            this.multiple[this.addButton]['program'].push({
              id: this.programs[i].id,
              name: this.programs[i].name
            });
          }
        }
      },
      error => console.log(error),
      () => {
        if (this.isSubmit) {
        }
      }
    );
  }
  
  

  editChange(event, programId, proId) {
    var trueCondition = true;
    if (event == true) {
      for (var i = 0; i < this.campusPrograms.length; i++) {
        if (this.campusPrograms[i]['programId'] == programId) {
          this.campusPrograms[i]['programDetails'].push(proId);
          trueCondition = false;
        }
      }
      if (trueCondition) {
        this.campusPrograms.push({
          programId: programId,
          programDetails: []
        });
        for (var i = 0; i < this.campusPrograms.length; i++) {
          if (this.campusPrograms[i]['programId'] == programId) {
            this.campusPrograms[i]['programDetails'].push(proId);
          }
        }
      }
    } else {
      for (var i = 0; i < this.campusPrograms.length; i++) {
        if (this.campusPrograms[i]['programId'] == programId) {
          if (this.campusPrograms[i]['programDetails'].length != 0) {
            for (
              var j = 0;
              j < this.campusPrograms[i]['programDetails'].length;
              j++
            ) {
              if (this.campusPrograms[i]['programDetails'][j] == proId) {
                this.campusPrograms[i]['programDetails'].splice(j, 1);
              }
            }
          } else {
            this.campusPrograms.splice(i, 1);
          }
        }
      }
    }
    this.campusProgramService.sendCRProgram(this.campusPrograms);
  }

  emitFunction(){
    this.loadPage.emit(true);
  }
}
