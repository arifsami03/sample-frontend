import { Component, OnInit, Input } from '@angular/core';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';


import { InstituteTypeService, EducationLevelService } from '../../../../../../institute/services';
import { CRProgramService } from '../../../../../services';
import { CampusInformationModel } from '../../../../../models';
import { InstituteTypeModel, EducationLevelModel } from '../../../../../../institute/models';
@Component({
  selector: 'campus-transfer-view',
  templateUrl: './campus-transfer-view.component.html',
  styleUrls: ['./campus-transfer-view.component.css']
})
export class CampusTransferViewComponent implements OnInit {
  @Input() fg: FormGroup;
  @Input() isSubmit: boolean = false;

  @Input() camModel: CampusInformationModel;
  @Input() crApplicantId: number;


  public deletedAffiliationIds: number[] = [];
  instituteTypes: InstituteTypeModel[];
  educationLevels: EducationLevelModel[];

  constructor(
    private fb: FormBuilder,
    private instituteTypeService: InstituteTypeService,
    private educationLevelService: EducationLevelService,
    private campusProgramService: CRProgramService
  ) { }
  ngOnInit() {
    this.fg.controls['affiliations'] = this.fb.array([]);
    if (this.camModel['affiliations'].length == 0) {
      this.addAffiliationClick();
    } else {
      for (var i = 0; i < this.camModel['affiliations'].length; i++) {
        this.addAffiliationClick();
      }
      this.fg.patchValue(this.camModel);
    }
    this.getInstituteTypes();
  }

  createAffiliationItem(): FormGroup {
    return this.fb.group({
      affiliation: '',
      id: ''
    });
  }
  deleteAffiliationItem(id): FormGroup {
    return this.fb.group({
      id: id
    });
  }

  addAffiliationClick(): void {
    let affiliation = this.fg.get('affiliations') as FormArray;
    affiliation.push(this.createAffiliationItem());
  }
  removeAffiliationLink(i: number, item) {
    let deletedIds = this.fg.get('deletedAffiliationIds') as FormArray;
    deletedIds.push(this.deleteAffiliationItem(item.value.id));
    const control1 = <FormArray>this.fg.controls['affiliations'];
    control1.removeAt(i);
  }

  getInstituteTypes() {
    this.instituteTypeService.findAttributesList().subscribe(
      response => {
        this.instituteTypes = response;
      },
      error => console.log(error),
      () => {
        this.getEducationLevels(this.camModel.instituteTypeId);
      }
    );
  }

  getEducationLevels(instituteTypeId: number) {

    this.educationLevelService.findAllEducationLevels(instituteTypeId).subscribe(
      response => {
        this.educationLevels = response;
      },
      error => console.log(error),
      () => { }
    );
  }

  loadEducationLevels(event) {
    this.fg.controls.educationLevelId.setValue(false);
    this.campusProgramService.setEducationLevelId(undefined);
    this.getEducationLevels(event.value);
  }

  loadPrograms(event) {
    this.campusProgramService.setEducationLevelId(event.value);
  }
}
