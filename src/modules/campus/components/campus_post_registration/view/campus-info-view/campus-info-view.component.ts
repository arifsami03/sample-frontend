import { GLOBALS } from '../../../../../app/config/globals';
import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatRadioChange } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';
import { PageAnimation } from '../../../../../shared/helper';
import { CampusInformationModel } from '../../../../models';
import { LayoutService } from '../../../../../layout/services';
import { CampusInfoService,CRApplicantService, CRProgramService, CampusPostRegistrationService } from '../../../../services';

@Component({
  selector: 'campus-info-view',
  templateUrl: './campus-info-view.component.html',
  styleUrls: ['./campus-info-view.component.css'],
  animations: [PageAnimation]
})
export class CampusInfoViewComponent implements OnInit {

  public pageState = 'active';

  public campusType: string = 'create';
  public subscription: Subscription;
  public loaded: boolean = false;
  public id: number;
  public applicationId: number;
  public applicantId: number;
  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string = 'Campus Information';
  public campusInfo: CampusInformationModel;
  public states: string[] = ['Punjab', 'Sindh', 'Balochistan', 'KPK', 'Gilgit'];
  public isSubmit: boolean = true;
  @Input() campusApplicantId: number;
  public mobileMask = GLOBALS.masks.mobile;
  public pageActions = GLOBALS.pageActions;
  public cnicMask = GLOBALS.masks.cnic;

  public componentLabels = CampusInformationModel.attributesLabels;
  public userId: number;
  public campusPrograms = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private layoutService: LayoutService,
    private campusInfoService: CampusInfoService,
    public applicantService: CRApplicantService,
    private campusProgramService: CRProgramService,
    private campusPostRegistrationService: CampusPostRegistrationService
  ) {
    this.subscription = this.campusProgramService.getCRPrograms().subscribe(message => {
      this.campusPrograms = message.campusProgram;
    });
    this.pageAct = route.snapshot.data['act'];
    this.route.params.subscribe(param => {
      this.applicationId = param.id;
    });
  }

  /**
   * ngOnInit
   */

  ngOnInit() {
    this.layoutService.setPageTitle({ title: this.pageTitle });

    if (this.pageAct == this.pageActions.view) {
      this.getApplicantIdByApplicationId();
    }
    else {
      this.getApplicantId();
    }
    this.initializePage();
    this.getLoginUserId();
    // this.getCampusInfo();
  }

  getApplicantIdByApplicationId() {

    return this.applicantService.getApplicantIdByApplicationId(this.applicationId).subscribe(
      response => {

        this.applicantId = response['CRApplicantId'];
        this.getCampusInfo();
        this.getRegistrationStatus();

      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  getApplicantId() {
    this.userId = Number(localStorage.getItem('id'));
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.userId;
    return this.applicantService.getApplicantId(this.campusApplicantId).subscribe(
      response => {

        this.applicantId = response['CRApplicantId'];
        this.getCampusInfo();;
        this.getRegistrationStatus();

      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }
  
  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }
  private initializePage() {
    this.fg = this.fb.group(new CampusInformationModel().validationRules());
    this.fg.disable();
  }

  public resetForm(event: MatRadioChange) {
    // this.fg = this.fb.group(new CampusInformationModel().validationRules());
    // this.fg.patchValue(this.campusInfo)
  }

  /**
   * Function to get Login user id from Local Storage;
   */
  getLoginUserId() {
    this.applicantId = Number(localStorage.getItem('id'));
  }

  /**
   * Function to get Campus owner id from user Table
   */
  getCampusInfo() {
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.applicantId;
    this.campusInfoService.findCampusInfo(this.applicantId).subscribe(
      response => {
        this.campusInfo = response;

        this.id = response['id'];

        this.fg.patchValue(this.campusInfo);
        //this.fg.disable();
        if (this.campusInfo.campusName == '0') {
          this.fg.controls['campusName'].setValue('');
        }

        if (response['applicationType'] == 'create') {
          this.fg.controls['campusType'].setValue(true);
        } else {
          this.fg.controls['campusType'].setValue(false);
        }

        this.getRegistrationStatus();
      },
      error => {
        console.log(error);
        // this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  getEmailErrorMessage() {
    return this.fg.get('email').hasError('required') ? 'Email is Required' : this.fg.get('email').hasError('email') ? 'Not a valid email' : '';
  }

  public saveData(item: CampusInformationModel) {
    if (item.campusType) {
      item['applicationType'] = 'create';
    } else {
      item['applicationType'] = 'transfer';
    }

    item['campusPrograms'] = this.campusPrograms;

    this.campusInfoService.update(this.id, item).subscribe(
      response => {
        this.router.navigate(['/post-registration/campus/remittance']);
      },
      error => {
        this.router.navigate(['/post-registration/campus/remittance']);
      }
    );
  }
  getRegistrationStatus() {
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.applicantId;
    this.campusPostRegistrationService.getRegistrationStatus(this.applicantId).subscribe(
      res => {
        if (res['state'] != 'approve' && res['WFId'] != 'pre-registration') {
          this.isSubmit = true;
          // this.fg.disable();
        } else {
          this.fg.enable();
          this.isSubmit = false;
        }
      },
      error => {
        console.log(error);
        // this.loaded = true;
      },
      () => {
        // this.loaded = true;
      }
    );
  }

  emitFunction() {
    this.loaded = true;
  }
}
