import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {
  NgControl,
  FormArray,
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';

import { MapComponent } from '../../../../map/map.component';
import { CampusInformationModel } from '../../../../../models';
import { CampusPostRegistrationService } from '../../../../../services';

@Component({
  selector: 'campus-address-view',
  templateUrl: './campus-address-view.component.html',
  styleUrls: ['./campus-address-view.component.css']
})
export class CRAddressViewComponent implements OnInit {
  @Input() buildingAvailable: boolean;

  @Input() fg: FormGroup;
  @Input() camModel: CampusInformationModel;
  @Input() isSubmit: boolean = false;
  @Input() crApplicantId: number;
  private formArrayAdresses: FormArray;
  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private campusPostRegistrationService: CampusPostRegistrationService,
  ) { }

  ngOnInit() {
    this.initializePage();
    // this.getRegistrationStatus();
  }

  private initializePage() {
    this.fg.controls['addresses'] = this.fb.array([]);

    if (this.buildingAvailable) {
      this.pushAddres(1);
      this.fg.disable();
    } else {
      this.pushAddres(2);
      this.fg.disable();
    }

    // this.fg.patchValue(this.camModel);
  }

  pushAddres(num) {
    let addressssssss = this.camModel['addresses'];

    for (let c = 0; c < num; c++) {
      this.formArrayAdresses = this.fg.get('addresses') as FormArray;

      let addressItem = addressssssss[c] ? addressssssss[c] : {};

      this.formArrayAdresses.push(this.createAddressItem(addressItem));
    }
  }

  createAddressItem(addressItem): FormGroup {
    return this.fb.group({
      id: [addressItem['id']],
      address: [addressItem['address']],
      longitude: [addressItem['longitude']],
      latitude: [addressItem['latitude']]
    });
  }

  /**
   * Open Popup to add Location
   */
  addLocationClick(i) {
    let dialogRef = this.dialog.open(MapComponent, {
      width: '800px',
      height: '400px',
      data: {
        lat: this.formArrayAdresses.at(i).get('latitude').value, lng: this.formArrayAdresses.at(i).get('longitude').value
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let addresses = {
          latitude: result.lat,
          longitude: result.lng
        };

        this.formArrayAdresses.at(i).patchValue({
          latitude: addresses.latitude,
          longitude: addresses.longitude
        });
        //  this.fg.patchValue(addresses);
      }
    });
  }
}
