import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';


import { InstituteTypeService, EducationLevelService } from '../../../../../../institute/services';
import { CRProgramService } from '../../../../../services';
import { InstituteTypeModel, EducationLevelModel } from '../../../../../../institute/models';
import { CampusInformationModel } from '../../../../../models';
@Component({
  selector: 'create-campus-view',
  templateUrl: './create-campus-view.component.html',
  styleUrls: ['./create-campus-view.component.css']
})
export class CreateCampusViewComponent implements OnInit {
  public buildingType: string = 'available';
  instituteTypes: InstituteTypeModel[];
  educationLevels: EducationLevelModel[];
  public loaded:boolean=false;

  @Input() fg: FormGroup;
  @Input() camModel: CampusInformationModel;
  @Input() isSubmit: boolean = false;
  @Input() crApplicantId: number;

  @Output() loadPage: EventEmitter<any> = new EventEmitter();
  constructor(
    private instituteTypeService: InstituteTypeService,
    private educationLevelService: EducationLevelService,
    private campusProgramService: CRProgramService
  ) { }
  ngOnInit() {
    this.getInstituteTypes();
    

  }

  getInstituteTypes() {
    this.instituteTypeService.findAttributesList().subscribe(
      response => {
        this.instituteTypes = response;
      },
      error => console.log(error),
      () => {
        this.getEducationLevels(this.camModel.instituteTypeId);
       
      
      }
    );
  }

  getEducationLevels(instituteTypeId: number) {

    this.educationLevelService.findAllEducationLevels(instituteTypeId).subscribe(
      response => {
        this.educationLevels = response;
        //this.loaded=true;
       
      },
      error => console.log(error),
      () => {
        this.fg.disable();
       }
    );
  }

  loadEducationLevels(event) {
    this.fg.controls.educationLevelId.setValue(false);
    this.campusProgramService.setEducationLevelId(undefined);
    this.getEducationLevels(event.value);
  }

  loadPrograms(event) {
    this.campusProgramService.setEducationLevelId(event.value);
  }

  emitFunction() {

    this.loadPage.emit(true);

  }

}
