import { GLOBALS } from '../../../../app/config/globals';
import { Component, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { PageAnimation } from '../../../../shared/helper';
import { LayoutService } from '../../../../layout/services';
import { CampusPostRegistrationService } from '../../../services';
import { WFStateService } from '../../../../settings/services';
import { DocStateManagerService, ConfigurationService } from '../../../../shared/services';
import { CampusPostRegistration, CRRemittanceModel, PersonalInformationModel, CampusInformationModel } from '../../../models';


@Component({
  selector: 'campus-post-registration-view',
  templateUrl: './post-registration-view.component.html',
  styleUrls: ['./post-registration-view.component.css'],
  animations: [PageAnimation]
})
export class PostRegistrationViewComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public isEmailSending = false;
  public CRApplicationType = GLOBALS.CRApplicationType;
  public CRApplicationStatus = GLOBALS.CRApplicationStatus;
  public session = GLOBALS.session;
  public CAMPUS_STATE = GLOBALS.WORKFLOW.POST_REGISTRATION.states;
  campusPostRegistration: CampusPostRegistration = new CampusPostRegistration();
  public labels = new CampusPostRegistration().labels;
  public preRegStatus: string;
  public selectedButton: string = 'personal';
  public documentId: number;
  public stateId: number;
  public wfState: string;
  public data: any;
  public configuration: any = {
    title: 'Submitted Application',
    template: 'campusPostRegistration'
  };
  public componentLabels = {
    remittance: CRRemittanceModel.attributesLabels,
    personalInfo: PersonalInformationModel.attributesLabels,
    campusInfo: CampusInformationModel.attributesLabels
  };
  public showOnlyFields = {
    remittance: ['branchName', 'instrumentCategory', 'purpose', 'amount'],
    personalInfo: ['fullName', 'email', 'address', 'mobileNumber'],
    campusInfo: ['campusName', 'coverdArea', 'openArea', 'totalArea', 'roomsQuantity', 'washroomsQuantity']
  };

  CRApplicationId: number;
  public previousListUrl;
  public remittanceStatus: boolean = false;
  // public previousListUrl: string;
  constructor(
    private campusPostRegistrationService: CampusPostRegistrationService,
    public layoutService: LayoutService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private wfStateService: WFStateService,
    private docStateManagerService: DocStateManagerService,
    public configurationService: ConfigurationService,

    public snackBar: MatSnackBar
  ) { 
    this.activatedRoute.params.subscribe(param => {
      this.documentId = param.id;
    });
  }

  ngOnInit(): void {
    this.layoutService.setPageTitle({
      title: 'Campus Post Registration Submitted Form'
    });

    this.getParams();
    //this.getSubmitAppData();
    this.checkremittanceStatus();

  }

  getSubmitAppData() {
    this.campusPostRegistrationService.getSubmitAppData(this.documentId).subscribe(
      response => {
        this.data = response[0];
      },
      error => { },
      () => {
        this.loaded = true;
      }
    );
  }

  getParams(): void {
    this.activatedRoute.params.subscribe(params => {
      let cam = +params['id'];
      this.documentId = +params['id'];
      this.checkremittanceStatus();
      this.getRecords(cam);
      this.getState(cam);
      this.getCurrentStateId();
    });
  }

  getRecords(_id: number): void {
    this.campusPostRegistrationService.getUserId(_id).subscribe(
      response => {
        this.CRApplicationId = response['id'];
      },
      error => { },
      () => {
        this.loaded = true;
      }
    );
  }

  getState(_id: number): void {
    this.campusPostRegistrationService.getState(_id).subscribe(
      response => {
        this.wfState = response['wfState']['state'];
        this.changeCurrentTab(this.wfState);
      },
      error => { },
      () => {
        this.loaded = true;
      }
    );
  }

  private changeCurrentTab(wfState: string) {
    let currentState = this.CAMPUS_STATE.find(item => {
      return wfState == item.value;
    });
    this.selectedButton = currentState ? currentState.value : 'personal';

  }

  changeStatus(_status: string) {
    this.isEmailSending = true;
    this.campusPostRegistrationService.changeStatus(this.CRApplicationId, { status: _status }).subscribe(
      response => {
        this.snackBar.open('Email has been sent.', _status.toUpperCase(), {
          duration: 2000
        });
        this.preRegStatus = _status;
        // this.campusPostRegistration.status = _status;
        this.isEmailSending = false;
      },
      error => {
        this.snackBar.open('Email is not sent.', 'ERROR', {
          duration: 2000
        });
        this.isEmailSending = false;
      },
      () => { }
    );
  }

  tabChange(type: string) {
    this.selectedButton = type;

  }
  /**
   * Function to Get State ID of Post-Registration Work Flow
   */

  getCurrentStateId() {
    var postData = {
      CRApplicationId: this.documentId
    };
    this.docStateManagerService.getCurrentStateId(postData).subscribe(
      response => {
        this.stateId = response['stateId'];
      },
      error => console.log(error),
      () => { }
    );
  }

  onChangeStateEvent(item) {
    if (item) {
      this.router.navigate([`/campus/post-registrations`]);
    }
  }

  public checkremittanceStatus() {
    // finding remittance
    this.campusPostRegistrationService.findRemittanceStatus(this.documentId).subscribe(
      response => {
        if (response) {
          this.remittanceStatus = response.remittanceStatus;
        }
      },
      error => {
        console.log(error);
      }
    );

  }
}
