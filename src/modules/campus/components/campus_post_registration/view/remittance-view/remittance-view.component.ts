import { BankModel } from '../../../../../settings/models';
import { ConfigurationModel } from '../../../../../shared/models';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { startWith } from 'rxjs/operators/startWith';
import { GLOBALS } from '../../../../../app/config/globals';

import { PageAnimation } from '../../../../../shared/helper';
import { map } from 'rxjs/operators/map';
import { CRRemittanceModel } from '../../../../models';
import { LayoutService } from '../../../../../layout/services';
import { BankService } from '../../../../../settings/services';
import { ConfigurationService } from '../../../../../shared/services';
import { CRRemittanceService, CampusPostRegistrationService,CRApplicantService } from '../../../../services';

// for dat picker format


@Component({
  selector: 'remittance-view',
  templateUrl: './remittance-view.component.html',
  styleUrls: ['./remittance-view.component.css'],
  animations: [PageAnimation]
})
export class RemittanceViewComponent implements OnInit {
  public pageState = 'active';
  // public loaded: boolean = false;
  public loaded: boolean = false;
  @Input() campusApplicantId: number;
  public fg: FormGroup;
  public pageAct: string;
  bankId: number;
  bankName: string = '';
  public pageTitle: string = 'Remittance';
  public instrumentCategory = ['Bank Draft', 'Cheque', 'Mail Transfer', 'Cash letter of credit', 'Telegraphic Transfer'];
  // public bankName = ['AlliedBank', 'Alfalah Bank', 'Habib Bank', 'Meezan Bank'];
  public purposes = ['Application Form', 'Registraion Form', 'Cancel Form'];
  public id: number;
  public applicantId: number;
  public applicationId: number;
  public banks: BankModel[];
  public pageActions = GLOBALS.pageActions;
  //Form Control for Custom Bank
  public bankFormControl: FormControl = new FormControl();
  public componentLabels = CRRemittanceModel.attributesLabels;

  filteredOptions: Observable<string[]>;
  public isSubmit: boolean = true;

  /**
   * for Datepicker
   */
  public currentDate = new FormControl();
  public instrumentDate = new FormControl();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private layoutService: LayoutService, //public dialog: MatDialog // private instituteEditService: InstituteService
    private remittanceService: CRRemittanceService,
    public applicantService: CRApplicantService,
    private campusPostRegistrationService: CampusPostRegistrationService,
    private banksService: BankService
  ) {
    this.pageAct = route.snapshot.data['act'];
    this.route.params.subscribe(param => {
      this.applicationId = param.id;
    });
  }

  /**
   * ngOnInit
   */

  ngOnInit() {
    this.layoutService.setPageTitle({ title: this.pageTitle });
    this.findBanks();
    if(this.pageAct == this.pageActions.view){
      this.getApplicantIdByApplicationId();
      this.initializePage();
    }
    else
    {
    this.initializePage();
    this.getApplicantIdByApplicationId();
    }
    //this.getRegistrationStatus();
  }

  getApplicantIdByApplicationId() {
    this.loaded = false;
    return this.applicantService.getApplicantIdByApplicationId(this.applicationId).subscribe(
      response => {

        this.applicantId = response['CRApplicantId'];

        this.getRemittanceInfo();
        // this.getRegistrationStatus();


      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }


  filter(val: string): any[] {

    if (this.banks) {
      if (val) {
        return this.banks.filter(option => option.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
      }
      else {
        return this.banks.filter(option => option.name.toLowerCase());

      }

    }
  }

  getApplicantId() {
    var _userId = Number(localStorage.getItem('id'));
    return this.applicantService.getApplicantId(_userId).subscribe(
      response => {

        this.applicantId = response['CRApplicantId'];
        this.getRemittanceInfo();

      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  private initializePage() {
    this.loaded = false;
    this.fg = this.fb.group(new CRRemittanceModel().validationRules());
    this.fg.addControl('bankFormControl', this.bankFormControl);

    this.fg.disable();
  }

  getRemittanceInfo(){

    
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.applicantId;
    this.remittanceService.view(this.applicantId).subscribe(
      response => {
        this.loaded = true;
        if (response) {
          this.fg.patchValue(response);
          this.fg.disable();
          if (response['bankName'] != null) {
            this.bankFormControl.setValue(response['bankName']);
          } else {
            for (let i = 0; i < this.banks.length; i++) {
              if (this.banks[i].id == response['bankId']) {
                this.bankFormControl.setValue(this.banks[i].name);
              }
            }
          }
        }
      },
      error => { },
      () => {
        this.loaded = true;
      }
    );

  }





  findBanks() {


    this.banksService.findAttributesList().subscribe(
      response => {


        this.banks = response;
        this.filteredOptions = this.bankFormControl.valueChanges.pipe(startWith(''), map(val => this.filter(val)));
      },
      error => { },
      () => { }
    );
  }


}
