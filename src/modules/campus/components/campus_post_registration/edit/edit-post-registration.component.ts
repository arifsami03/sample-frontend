import { GLOBALS } from '../../../../app/config/globals';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import * as _ from 'lodash';

import { PageAnimation } from 'modules/shared/helper';
import { LayoutService } from 'modules/layout/services';
import { CampusPostRegistrationService, CRApplicantService } from 'modules/campus/services';
import { CampusPostRegistration } from 'modules/campus/models';
import { WFStateService } from 'modules/settings/services';
import { WFStateModel } from '../../../../settings/models';
import { ConfigurationService } from '../../../../shared/services';

@Component({
  selector: 'edit-post-registration',
  templateUrl: './edit-post-registration.component.html',
  styleUrls: ['./edit-post-registration.component.css'],
  animations: [PageAnimation]
})
export class EditPostRegistrationComponent implements OnInit {

  public pageState;
  public selectedButton = 'personal';
  public applicantId: number;
  public pageAct: string;
  public pageActions = GLOBALS.pageActions;

  public loaded: boolean = false;
  public remittanceStatus: boolean = false;
  constructor(
    public layoutService: LayoutService,
    private activatedRoute: ActivatedRoute,
    public configurationService: ConfigurationService,
    private campusPostRegistrationService: CampusPostRegistrationService,
    public applicantService: CRApplicantService,
    private router: Router) { }

  ngOnInit(): void {


    const bc = this.activatedRoute.snapshot.data['breadcrumb'];
    this.layoutService.setPageTitle({ title: bc['title'] });
    this.pageAct = this.activatedRoute.snapshot.data['act'];
    if (this.pageAct == this.pageActions.create) {
      this.checkremittanceStatusOfCreate();
    }
    else {
      this.getApplicantIdByApplicationId();
    }
  }

  tabChange(type: string) {
    this.selectedButton = type;

  }

  shiftTab(event) {
    this.selectedButton = event;
  }
  getApplicantIdByApplicationId() {
    this.activatedRoute.params.subscribe(params => {
      return this.applicantService.getApplicantIdByApplicationId(params['id']).subscribe(
        response => {

          this.applicantId = response['CRApplicantId'];
          this.checkremittanceStatus();


        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    })
  }
  public checkremittanceStatus() {
    // finding remittance

    this.campusPostRegistrationService.findRemittanceStatus(this.applicantId).subscribe(
      response => {
        if (response) {
          this.remittanceStatus = response.remittanceStatus;
        }
      },
      error => {
        console.log(error);
      }
    );


  }
  public createdCRApplicationId = null
  getcreatedCRApplicationId(event) {
    console.log(event)
    this.createdCRApplicationId = event;
  }

  public checkremittanceStatusOfCreate() {
    // finding remittance

    this.configurationService.findAttributesList('showRemittance').subscribe(
      response => {
        if (response) {
          if (response[0].value == '1') {
            this.remittanceStatus = true;
          }
          else {
            this.remittanceStatus = false;
          }

        }
      },
      error => {
        console.log(error);
      }
    );


  }
}
