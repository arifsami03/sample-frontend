import { GLOBALS } from '../../../../app/config/globals';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import * as _ from 'lodash';

import { PageAnimation } from '../../../../shared/helper';
import { LayoutService } from '../../../../layout/services';
import { CampusPostRegistrationService } from '../../../services';
import { CampusPostRegistration } from '../../../models';
import { WFStateService } from '../../../../settings/services';
import { WFStateModel } from '../../../../settings/models';

@Component({
  selector: 'campus-post-registration-list',
  templateUrl: './post-registration-list.component.html',
  styleUrls: ['./post-registration-list.component.css'],
  animations: [PageAnimation]
})
export class PostRegistrationListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;
  public filterParams = {};

  public data: CampusPostRegistration[] = [new CampusPostRegistration()];
  public postRegistrationStates: WFStateModel[];
  public status: string;
  public listLabels = new CampusPostRegistration().listLabels;

  private superUser = localStorage.getItem('isSuperUser');

  public CRApplicationType = GLOBALS.CRApplicationType;
  public CRApplicationStatus = GLOBALS.CRApplicationStatus;
  public states = GLOBALS.WORKFLOW.POST_REGISTRATION.states;
  public assignedWFStates = [];
  public selectedState = GLOBALS.WORKFLOW.POST_REGISTRATION.COMPLETE_LIST.value;
  public stateForCompleteList = GLOBALS.WORKFLOW.POST_REGISTRATION.COMPLETE_LIST.value;
  displayedColumns = ['fullName', 'email', 'applicationType', 'cityName', 'state', 'options'];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private campusPostRegistrationService: CampusPostRegistrationService,
    private wfStateService: WFStateService,
    public layoutService: LayoutService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    const bc = this.activatedRoute.snapshot.data['breadcrumb'];
    this.layoutService.setPageTitle({ title: bc['title'] });

    this.getRecords();

    if (this.superUser === 'false') {
      this.findAllAssignedWFStates();
    }
  }

  /**
   * Getting post registartion list
   */
  getRecords(): void {
    this.campusPostRegistrationService.index(this.filterParams).subscribe(
      response => {
        this.data = response;
        this.data.map(element => {
          element.fullName = element.applicant.fullName;
          element.email = element.applicant.email;
          element.applicationType = this.CRApplicationType[element.applicant.applicationType].title;
          element.cityName = element.city.value;
          let state = this.states.find(item => {
            return item.value == element.wfState.state;
          });
          element.state = state ? state.label : '';
        });
        this.dataSource = new MatTableDataSource<CampusPostRegistration>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {

        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  findAllAssignedWFStates() {
    this.campusPostRegistrationService.findAllAssignedWFStates().subscribe(
      response => {
        this.assignedWFStates = response;
      }, error => {
        console.log('error: ', error);
      },
      () => {
        this.states = _.intersectionBy(this.states, this.assignedWFStates, 'value');
        this.states.unshift(GLOBALS.WORKFLOW.POST_REGISTRATION.COMPLETE_LIST);
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); //TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }

  loadApplications(state: string) {
    if (state == this.stateForCompleteList) {
      this.filterParams = {};
    } else {
      this.filterParams = { state: state };
    }
    this.getRecords();
  }
}
