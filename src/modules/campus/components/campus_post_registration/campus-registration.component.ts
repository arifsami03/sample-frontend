import { GLOBALS } from '../../../app/config/globals';
import { Component, OnInit, Input } from '@angular/core';
import { ConfigurationService } from '../../../shared/services';
import { CampusPostRegistrationService, CRApplicantService } from '../../services';
@Component({
  selector: 'campus-registration',
  templateUrl: './campus-registration.component.html',
  styleUrls: ['./campus-registration.component.css']
})
export class CampusRegistrationComponent implements OnInit {
  @Input() selectedButton: any;
  public registration: boolean = true;
  public remittanceStatus: boolean = false;
  private userId: number;
  private applicantId: number;
  constructor(
    // public configurationService: ConfigurationService,
    private campusPostRegistrationService: CampusPostRegistrationService,
    private applicantService: CRApplicantService,

  ) { }
  ngOnInit() {
    this.getApplicantId();
    var type = (localStorage.getItem('portal'));
    if (type == 'registration') {
      this.registration = true;
    }
    else {
      this.registration = false;
    }

  }

  getApplicantId() {
    this.userId = Number(localStorage.getItem('id'));
    return this.applicantService.getApplicantId(this.userId).subscribe(
      response => {

        this.applicantId = response['CRApplicantId'];
        this.checkremittanceStatus();
      },
      error => {
        console.log(error);
      },
      
    );
  }
  public checkremittanceStatus() {
    // finding remittance
    this.campusPostRegistrationService.findRemittanceStatus(this.applicantId).subscribe(
      response => {
        if (response) {
          this.remittanceStatus = response.remittanceStatus;
        }
      },
      error => {
        console.log(error);
      }
    );

  }
}
