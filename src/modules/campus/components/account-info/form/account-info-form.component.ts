import { Component, ViewChild, OnInit, Input } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { PageAnimation } from '../../../../shared/helper';

import { Subscription } from 'rxjs/Subscription';

import { AccountInfoService, CampusService } from '../../../services';
import { LayoutService } from '../../../../layout/services';

import { ConfirmDialogComponent } from '../../../../shared/components';
import { EditAccountInfoDialogComponent } from './edit-account-info/edit-account-info-dialog.component';
import { AccountInfoModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';
import { DocStateManagerService } from '../../../../shared/services';
import { BankService } from '../../../../settings/services/bank.service';
import { ProgramService, ProgramDetailsService } from '../../../../academic/services';
import { BankModel } from '../../../../settings/models/bank';
import { ProgramModel, ProgramDetailsModel } from '../../../../academic/models';


@Component({
  selector: 'campus-account-info-form',
  templateUrl: './account-info-form.component.html',
  styleUrls: ['./account-info-form.component.css'],
  animations: [PageAnimation]

})
export class AccountInfoFormComponent implements OnInit {


  @Input() CRApplicationId: number;

  public pageState = 'active';
  public loaded: boolean = false;
  public displayForm: boolean = true;
  public boxLoaded = false;
  public pageActions = GLOBALS.pageActions;
  public error: Boolean;
  public fg: FormGroup;
  public pageAct: string;
  public campusId: number;
  public stateId: number;
  public wfState: string;
  public accountInfoModel: AccountInfoModel;
  public selectedButton: string = 'accountInfo';
  displayedColumns = ['programDetail', 'branch', 'account', 'options'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public selected = [];

  public addButton: number = 0;
  public multiple: any = [];
  public campusPrograms: any = [];

  public educationLevelId: number;

  public componentLabels = AccountInfoModel.attributesLabels;

  public programs: ProgramModel[] = [new ProgramModel()];
  public programDetails: ProgramDetailsModel[] = [new ProgramDetailsModel()];
  challanTypes = ['Two Copy Chalan'];
  public banks = [new BankModel()];
  private CAMPUS_STATUS = {
    DONE: 'done',
    REJECT: 'reject'
  };

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private accountInfoService: AccountInfoService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private docStateManagerService: DocStateManagerService,
    private bankService: BankService,
    private programService: ProgramService,
    private programDetailService: ProgramDetailsService,
    private campusService: CampusService
  ) {
    this.resetValues();
    this.pageAct = activatedRoute.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {

    this.layoutService.setPageTitle({ title: 'Account Information' });

    this.getCurrentStateId();

    //TODO:low: Find method in backend model calass is overrided which should not...
    this.campusService.find(this.CRApplicationId).subscribe(res => {

      this.campusId = res['campus']['id'];
      this.getBanks();
      this.initializePage();

    })


  }
  /**
   * Initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new AccountInfoModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
      this.getListOfAlreadySaveProgamDetails()
    }
  }


  getListOfAlreadySaveProgamDetails() {
    return new Promise((resolve, reject) => {
      this.accountInfoService.getProgramDetailsAccountInfo(this.campusId).subscribe(
        response => {
          this.dataSource = new MatTableDataSource<any>(response['programDetailsWithAccountInfo']);

        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          reject(error);
        },
        () => { }
      );
    });
  }

  resetValues() {
    this.selected = [];
    this.programs = [];
    this.campusPrograms = [];
    this.programDetails = [];
    this.multiple = [];
    this.addButton = 0;
  }

  getCurrentStateId() {
    var postData = {
      CRApplicationId: this.CRApplicationId
    };

    return new Promise((resolve, reject) => {

      this.docStateManagerService.getCurrentStateId(postData).subscribe(
        response => {
          this.stateId = response['stateId'];
          resolve(response);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          reject(error);
        },
        () => { }
      );
    });


  }

  getBanks() {

    this.bankService.findAttributesList().subscribe(
      response => {

        this.banks = response;

      },

      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }



  /**
   * Initialize Create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Account Information' });
    this.accountInfoModel = new AccountInfoModel();
    this.fg.enable();

    this.loaded = true;
  }
  /**
   * Get Data
   */
  private getData() {

    this.loaded = false;

    return new Promise((resolve, reject) => {

      this.accountInfoService.getProgramDetails(this.campusId).subscribe(
        response => {
          if (response.cammpusProgramDetails) {
            this.loaded = true;
            //this.getCampusData();
            response.cammpusProgramDetails.forEach(element => {

              let programId = element['programId'];

              let programs = [];
              element.programDetails.forEach(programDetail => {
                programs.push(programDetail.programDetailId);
              });

              this.selected.push({ programId: programId, programs: programs });

            });
            this.getAllProgramDetails();
            resolve(response);

          }
          else {


          }
        },

        error => {
          console.log(error);
          this.loaded = true;
          reject(error);
        }, () => {
          this.loaded = true;
        }
      );
    })
  }

  getAllProgramDetails() {
    this.loaded = false;
    this.programDetailService.findAttributesList().subscribe(
      response => {

        for (let i = 0; i < this.selected.length; i++) {
          this.multiple.push({ program: [], programId: '', programDetails: [] });
          this.multiple[this.addButton]['programId'] = this.selected[i].programId;
          for (let j = 0; j < this.selected[i].programs.length; j++) {

            for (let k = 0; k < response.length; k++) {

              if (this.selected[i].programs[j] == response[k].id) {
                this.multiple[this.addButton]['programDetails'].push({
                  id: response[k].id,
                  name: response[k]['program']['educationLevel'].education + ' - ' + response[k]['program'].name + ' - ' + response[k].name
                });
              }
            }
          }

          this.addButton++;
        }

        for (let i = 0; i < this.multiple.length; i++) {

          if (this.multiple[i].programDetails.length == 0) {

            this.displayForm = false;
          }
          else {
            this.displayForm = true;
          }

        }
      },

      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );

  }
  checkChange(event, programId, proId) {
    var trueCondition = true;
    if (event.checked == true) {
      for (var i = 0; i < this.campusPrograms.length; i++) {
        if (this.campusPrograms[i]['programId'] == programId) {
          this.campusPrograms[i]['programDetails'].push(proId);
          trueCondition = false;
        }
      }
      if (trueCondition) {
        this.campusPrograms.push({
          programId: programId,
          programDetails: []
        });
        for (var i = 0; i < this.campusPrograms.length; i++) {
          if (this.campusPrograms[i]['programId'] == programId) {
            this.campusPrograms[i]['programDetails'].push(proId);
          }
        }
      }
    } else {
      for (var i = 0; i < this.campusPrograms.length; i++) {
        if (this.campusPrograms[i]['programId'] == programId) {
          for (var j = 0; j < this.campusPrograms[i]['programDetails'].length; j++) {
            if (this.campusPrograms[i]['programDetails'][j] == proId) {
              this.campusPrograms[i]['programDetails'].splice(j, 1);
            }
          }
          if (this.campusPrograms[i]['programDetails'].length != 0) {
          } else {
            this.campusPrograms.splice(i, 1);
          }
        }
      }
    }
    //this.campusProgramService.sendCRProgram(this.campusPrograms);
  }



  /**
   * Add or Update when save button is clicked
   */
  public saveData(item: AccountInfoModel) {

    this.boxLoaded = true;


    let programDetailIds = [];

    for (let i = 0; i < this.campusPrograms.length; i++) {
      for (let j = 0; j < this.campusPrograms[i].programDetails.length; j++) {
        programDetailIds.push(this.campusPrograms[i].programDetails[j])

      }

    }
    var postdata = {
      accountInfo: item,
      programDetailIds: programDetailIds,
      campusId: this.campusId
    }

    item.id = null;

    this.accountInfoService.create(postdata).subscribe(
      response => {
        if (response) {
          this.boxLoaded = false
          this.layoutService.flashMsg({ msg: 'Account Information has been created.', msgType: 'success' });
          this.resetValues();
          this.ngOnInit();
          //this.router.navigate([`/campus/post-registrations/forward-for-account-info/${this.campusId}`]);
        }
      },
      error => {
        this.boxLoaded = false;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        console.log(error);
      }, () => {      }
    );
  }

  onChangeStateEvent(item) {
    if (item && item.state == this.CAMPUS_STATUS.DONE) {
      this.approveCampus();
    } else if (item && item.state == this.CAMPUS_STATUS.REJECT) {
      this.rejectCampus();
    }
    this.router.navigate([`/campus/post-registrations`]);
  }

  approveCampus() {
    this.campusService.approveCampus(this.CRApplicationId).subscribe(
      response => {
        // Put some code here to show message campus is approved
      },
      error => {
        console.log('error: ', error);
      }
    )
  }

  rejectCampus() {
    this.campusService.rejectCampus(this.CRApplicationId).subscribe(
      response => {
        // Put some code here to show message campus is rejected
      },
      error => {
        console.log('error: ', error);
      }
    )
  }

  tabChange(type: string) {
    this.selectedButton = type;
  }

  editAccount(id, programDetail) {

    let dialogRef = this.dialog.open(EditAccountInfoDialogComponent, {
      width: '700px',
      // height: '400px',
      data: { accountInfoId: id, programDetail: programDetail }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getListOfAlreadySaveProgamDetails();
      }
    });


  }

}
