import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { FormBuilder, FormGroup } from '@angular/forms';
import { AccountInfoModel } from '../../../../models';
import { BankModel } from '../../../../../settings/models';
import { AccountInfoService, CampusService } from '../../../../services';
import { BankService } from '../../../../../settings/services';
import { LayoutService } from '../../../../../layout/services';
import { CampusPostRegistrationService } from '../../../../services';
import { ESCategoryModel, EvaluationSheetModel } from '../../../../../settings/models';

@Component({
  selector: 'edit-account-info-dialog',
  templateUrl: './edit-account-info-dialog.component.html',
  styleUrls: ['./edit-account-info-dialog.component.css'],
})
export class EditAccountInfoDialogComponent implements OnInit {

  public loaded: boolean = false;
  public fg: FormGroup;

  public banks = [new BankModel()];
  challanTypes = ['Two Copy Chalan'];

  public componentLabels = AccountInfoModel.attributesLabels;


  /**
   * Construstor 
   * 
   * @param router Router
   * @param route ActivatedRoute
   * @param fb FormBuilder
   * @param dialogRef MatDialogRef<AttachEvalSheetDialogComponent>
   * @param esCatService ESCategoryService
   * @param evalSheetService EvaluationSheetService
   * @param data any
   */
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditAccountInfoDialogComponent>,
    private accountInfoService: AccountInfoService,
    private layoutService: LayoutService,
    private bankService: BankService,
    private campusPostRegistrationService: CampusPostRegistrationService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  /**
   * ngOnInit
   * 
   */
  ngOnInit() {
    this.initializeForm();
  }

  /** 
   * Initialize form
  */
  private initializeForm() {
    this.getBanks();
    this.getData();

    this.fg = this.fb.group(new AccountInfoModel().validationRules());


  }







  /**
   * Close dialog.
   */
  public cancel() {
    this.dialogRef.close(false);
  }

  getBanks() {

    this.bankService.findAttributesList().subscribe(
      response => {

        this.banks = response;

      },

      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }

  getData() {

    this.accountInfoService.find(this.data.accountInfoId).subscribe(
      response => {

        this.fg.patchValue(response);

      },

      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }

  saveData(item:AccountInfoModel){
    this.accountInfoService.update(this.data.accountInfoId, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Account Info has been updated.', msgType: 'success' });
        this.dialogRef.close(true);
      },
      error => {
        console.log(error);
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        this.loaded = true;
      }, () => {
      }
    );
  }
}
