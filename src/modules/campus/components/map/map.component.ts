import { Observable } from 'rxjs/Observable';
import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AgmCoreModule, MouseEvent } from '@agm/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { MapsService } from '../../services/maps.service';

// import { InstituteModel } from 'modules/institute/models';
// import { InstituteService } from 'modules/institute/services';
@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css'],
  providers: [MapsService]
})
export class MapComponent implements OnInit {

  public loaded: boolean = false;
  public id: number;
  public lat: number;
  public lng: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private mapsService: MapsService,
    public dialogRef: MatDialogRef<MapComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  /**
   * ngOnInit
   */

  ngOnInit() {
    // 31.461358391483742
    // 74.41597938537598
    // this.lat = (this.data.lat != 0 ? this.data.lat : 31.582045);
    // this.lng = (this.data.lng != 0 ? this.data.lng : 74.329376);
    this.getLocation(this.data.cityLocation);

  }

  mapClicked($event: MouseEvent) {
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;

  }
  locationAdded() {
    let data = {
      lat: this.lat,
      lng: this.lng,
    }
    this.dialogRef.close(data);
  }


  getLocation(address) {
    this.mapsService.getLocation(address).subscribe(result => {
      if (!this.data.lat){
        this.lat = result['results'][0].geometry.location.lat
      }else{
        this.lat = this.data.lat;
      }
      if (!this.data.lng){
        this.lng = result['results'][0].geometry.location.lng
      }else{
        this.lng = this.data.lng;

      }
     
    }, error => {
      console.log('result : ', error)

    }, () => { })
  }

}
