import { GLOBALS } from '../../../../../app/config/globals';
import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageAnimation } from '../../../../../shared/helper';
import { LayoutService } from '../../../../../layout/services';
import { CampusPostRegistrationService } from '../../../../services';

import { CampusInformationModel } from '../../../../models';

@Component({
  selector: 'campus-detail-view',
  templateUrl: './campus-detail-view.component.html',
  styleUrls: ['./campus-detail-view.component.css'],
  animations: [PageAnimation]
})
export class CampusDetailViewComponent implements OnInit {
  public pageState = 'active';
  @Input() CRApplicationId:number;
  public loaded: boolean = false;
  public campus: CampusInformationModel;
  public componentLabels = CampusInformationModel.attributesLabels;
  public selectedButton: string = 'application';

  constructor(
    public layoutService: LayoutService,
    private campusPostRegistrationService: CampusPostRegistrationService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.layoutService.setPageTitle({
      title: 'Campus Detail'
    });
    this.getData();

  }

  getData() {
    this.campusPostRegistrationService.find(this.CRApplicationId).subscribe(
      response => {
        this.campus = response;
      },
      error => { },
      () => {
        this.loaded = true;
      }
    );
  }

  tabChange(type: string) {
    this.selectedButton = type;
  }
}
