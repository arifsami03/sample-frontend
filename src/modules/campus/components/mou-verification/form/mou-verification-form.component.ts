import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { PageAnimation } from '../../../../shared/helper';
import { CRMouVerificationService } from '../../../services';
import { DocStateManagerService } from '../../../../shared/services';
import { LayoutService } from '../../../../layout/services';
import { BankService } from '../../../../settings/services';
import { ConfigurationService } from '../../../../shared/services';

import { ConfirmDialogComponent } from '../../../../shared/components';
import { CRMouVerificationModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';
import { BankModel } from '../../../../settings/models';
import { ConfigurationModel } from '../../../../shared/models';


@Component({
  selector: 'campus-mou-verification-form',
  templateUrl: './mou-verification-form.component.html',
  styleUrls: ['./mou-verification-form.component.css'],
  animations: [PageAnimation]
})
export class MouVerificationFormComponent implements OnInit {

  public pageState = 'active';
  public selectedButton: string = 'mouVerification';
  public loaded: boolean = false;
  public boxLoaded = false;
  public pageActions = GLOBALS.pageActions;
  public error: Boolean;
  public fg: FormGroup;
  public pageAct: string;
  public mouVerificationModel: CRMouVerificationModel;
  public CRApplicationId: number;
  public stateId: number;
  public isRecordExists: boolean;
  public amountMatched: boolean;
  public mouVerificationPurposes : string[] = GLOBALS.MOU_VERIFICATION_PURPOSES;


  public componentLabels = CRMouVerificationModel.attributesLabels;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private mouVerificationService: CRMouVerificationService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private docStateManagerService: DocStateManagerService,
    private confService: ConfigurationService,
    private bankService: BankService
  ) {
    this.pageAct = activatedRoute.snapshot.data['act'];
  }

  public instrumentCategories = [new ConfigurationModel()];
  public banks = [new BankModel()];
  status = ['Active', 'In Active', 'Online', 'Offline', 'Pending', 'Approved'];

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.layoutService.setPageTitle({ title: 'MOU Verification' });

    this.getParams();
    this.initializePage();
    this.getInstrumentType();
    this.getBanks();
  }
  /**
   * Initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new CRMouVerificationModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      //this.loaded = true;
      this.getData();
    }
  }

  /**
   * Get Data
   */
  private getData() {
    this.mouVerificationService.find(this.CRApplicationId).subscribe(
      response => {
        if (response) {
          this.isRecordExists = true;
          this.mouVerificationModel = response;
          this.fg.patchValue(this.mouVerificationModel);
        }
        else {
          this.isRecordExists = false;

        }

      },

      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Initialize Create page
   */
  private initCreatePage() {
    this.mouVerificationModel = new CRMouVerificationModel();
    this.fg.enable();

    this.loaded = true;
  }
  getParams() {
    this.loaded = false;
    return new Promise((resolve, reject) => {
      this.activatedRoute.params.subscribe(params => {

        let cam = +params['id'];
        this.CRApplicationId = +params['id'];
        this.getCurrentStateId();
        this.loaded = true;
        resolve(params);

      }
        ,
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          reject(error);
        },
        () => { }
      );
    })
  }



  /**
   * Add or Update when save button is clicked
   */
  public saveData(item: CRMouVerificationModel) {

    this.boxLoaded = true;

    item.CRApplicationId = this.CRApplicationId;

    // TODO:HIGH Whta is the purpose of this comparison. 
    // If its a validation then form shoult not be saved. 
    // And validation error should be at the bottom of the input field
    // Also its is generating parallel call and not waiting for anything.
    // this.compareMOUAmounts(this.CRApplicationId, item.amount);


    this.mouVerificationService.compareMOUAmounts(this.CRApplicationId, item.amount).subscribe(
      response => {
        if (response) {

          if (!this.isRecordExists) {

            this.mouVerificationService.create(item).subscribe(
              response => {
                if (response) {
                  this.boxLoaded = false;
                  this.layoutService.flashMsg({ msg: 'MOU Verification has been created.', msgType: 'success' });

                  this.getData();
                }
              },
              error => {
                this.boxLoaded = false;
                this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
                console.log(error);
              }, () => { }
            );
          } else if (this.isRecordExists) {
            this.mouVerificationService.update(this.CRApplicationId, item).subscribe(
              response => {
                if (response) {
                  this.boxLoaded = false;
                  this.layoutService.flashMsg({ msg: 'MOU Verification has been updated.', msgType: 'success' });
                }
              },
              error => {
                this.boxLoaded = false;
                this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
                console.log(error);
              }, () => { }
            );
          }

        }
        else {

          this.amountMatched = false;
          this.fg.controls['amount'].setErrors({
            invalid: true
          })

        }

      },

      error => {
        console.log(error);
        this.boxLoaded = false;
      }, () => {
        this.boxLoaded = false;
      }
    );

  }


  compareMOUAmounts(CRApplicationId, amount) {

    // this.mouVerificationService.compareMOUAmounts(CRApplicationId, amount).subscribe(
    //   response => {
    //     if (response) {

    //       this.amountMatched = true;

    //     }
    //     else {

    //       this.amountMatched = false;
    //       this.fg.controls['amount'].setErrors({
    //         invalid: true
    //       })

    //     }

    //   },

    //   error => {
    //     console.log(error);
    //     this.loaded = true;
    //   }, () => {
    //     this.loaded = true;
    //   }
    // );



  }


  tabChange(type: string) {
    this.selectedButton = type;
    this.layoutService.setPageTitle({ title: 'MOU Verification' });

  }

  getInstrumentType() {

    this.confService.getAll('instrumentType').subscribe(
      response => {
        this.instrumentCategories = response;

      },

      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }
  getBanks() {

    this.bankService.findAttributesList().subscribe(
      response => {
        this.banks = response;

      },

      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }

  /**
  * Function to Get State ID of Post-Registration Work Flow
  */

  getCurrentStateId() {
    var postData = {
      CRApplicationId: this.CRApplicationId
    };

    return new Promise((resolve, reject) => {

      this.docStateManagerService.getCurrentStateId(postData).subscribe(
        response => {
          this.stateId = response['stateId'];
          resolve(response);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          reject(error);
        },
        () => { }
      );
    });


  }

  onChangeStateEvent(item) {
    if (item) {
      this.router.navigate([`/campus/post-registrations`]);
    }
  }

}
