import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatOptionSelectionChange } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from '../../../../../shared/helper';
import { CRMouService } from '../../../../services';
import { LayoutService } from '../../../../../layout/services';
import { ManagementFeeService } from '../../../../../fee-management/services';
import { ManagementFeeModel } from '../../../../../fee-management/models';
import { EducationLevelService } from '../../../../../institute/services';
import { AcademicCalendarService } from '../../../../../academic/services';
import { ConfirmDialogComponent } from '../../../../../shared/components';
import { DocStateManagerService } from '../../../../../shared/services';
import { CRMouModel } from '../../../../models';
import { GLOBALS } from '../../../../../app/config/globals';
import { EducationLevelModel } from '../../../../../institute/models';
import { AcademicCalendarModel } from '../../../../../academic/models';


@Component({
  selector: 'mou-view',
  templateUrl: './mou-view.component.html',
  styleUrls: ['./mou-view.component.css'],
  animations: [PageAnimation]
})
export class MouViewComponent implements OnInit {

  public pageState = 'active';
  public loaded: boolean = false;
  public boxLoaded = true;
  public pageActions = GLOBALS.pageActions;
  public error: Boolean;
  public fg: FormGroup;
  public pageAct: string;
  public mouModel: CRMouModel;
  public selectedButton: string = 'mou';
  public CRApplicationId: number;
  public isRecordExists: boolean;
  public stateId: number;

  public educationLevels = [new EducationLevelModel()];
  public academicCalendars = [new AcademicCalendarModel()];
  public managementFees: ManagementFeeModel[] = [];

  public componentLabels = CRMouModel.attributesLabels;


  public academicMonths = GLOBALS.MONTHS;
  public academicYears: number[] = GLOBALS.academicYears;
  public semSessions: any[] = [GLOBALS.RMCategories.fall,GLOBALS.RMCategories.spring];


  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private mouService: CRMouService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    public educationLevel: EducationLevelService,
    public academicCalendarService: AcademicCalendarService,
    public managementFeeService: ManagementFeeService,
    public docStateManagerService: DocStateManagerService
  ) {
    this.pageAct = activatedRoute.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.layoutService.setPageTitle({ title: 'MOU' });

    this.getParams();
    this.getEducationLevel();
    this.getAcademicYear();
    this.initializePage();
    this.fg.disable();

  }

  /**
   * Initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new CRMouModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      //this.loaded = true;
      this.getData();
    }
  }

  getParams() {
    this.loaded = false;
    return new Promise((resolve, reject) => {
      this.activatedRoute.params.subscribe(params => {

        this.CRApplicationId = +params['id'];
        this.getCurrentStateId();
        this.loaded = true;
        resolve(params);

      }
        ,
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          reject(error);
        },
        () => { }
      );
    })
  }

  /**
   * Get Data
   */
  private getData() {

    this.mouService.find(this.CRApplicationId).subscribe(response => {

      if (response) {
        this.isRecordExists = true;
        this.mouModel = response;
        this.fg.patchValue(this.mouModel);
      }
      else {
        this.isRecordExists = false;

      }

    },

      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );

  }

  /**
   * Initialize Create page
   */
  private initCreatePage() {
    this.mouModel = new CRMouModel();
    this.fg.enable();

    this.loaded = true;
  }
  tabChange(type: string) {
    this.selectedButton = type;
  }

  getEducationLevel() {

    this.educationLevel.findAttributesList().subscribe(
      response => {
        this.educationLevels = response;

      },

      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );


  }

  getAcademicYear() {

    this.academicCalendarService.findAttributesList().subscribe(
      response => {
        this.academicCalendars = response;

      },

      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );


  }
  educationLevelChange(event: MatOptionSelectionChange, educationLevelId: number) {
    if (event.source.selected) {
      this.managementFeeService.findAttributesList().subscribe(
        response => {
          this.managementFees = response;

        },

        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );

    }
  }



  /**
   * Add or Update when save button is clicked
   */
  public saveData(item: CRMouModel) {
    item.id = this.CRApplicationId;
    this.boxLoaded = false;

    if (!this.isRecordExists) {

      this.mouService.create(item).subscribe(
        response => {
          if (response) {
            this.layoutService.flashMsg({ msg: 'MOU has been created.', msgType: 'success' });
            // this.router.navigate([`/campus/post-registration/forward-for-create-mou/${this.CRApplicationId}`]);
          }
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
          this.boxLoaded = true;
        }
      );
    } else if (this.isRecordExists) {
      this.mouService.update(this.CRApplicationId, item).subscribe(
        response => {
          if (response) {
            this.layoutService.flashMsg({ msg: 'MOU has been updated.', msgType: 'success' });
            // this.router.navigate([
            //   `/campus/mous`
            // ]);
          }
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
          this.boxLoaded = true;
        }
      );
    }
  }


  /**
 * Function to Get State ID of Post-Registration Work Flow
 */

  getCurrentStateId() {
    var postData = {
      CRApplicationId: this.CRApplicationId
    };

    return new Promise((resolve, reject) => {

      this.docStateManagerService.getCurrentStateId(postData).subscribe(
        response => {
          this.stateId = response['stateId'];
          resolve(response);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          reject(error);
        },
        () => { }
      );
    });


  }


}
