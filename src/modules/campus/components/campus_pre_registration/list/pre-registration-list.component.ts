import { GLOBALS } from '../../../../app/config/globals';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material';
import { PageAnimation } from '../../../../shared/helper';
import { LayoutService } from '../../../../layout/services';
import { CampusPreRegistrationService } from '../../../services';
import { CampusPreRegistration } from '../../../../public/models';
import { WFStateModel } from '../../../../settings/models';
import { WFStateService } from '../../../../settings/services';
import { AddPreRegistrationComponent } from './add/add-pre-registration.component';

@Component({
  selector: 'campus-pre-registration-list',
  templateUrl: './pre-registration-list.component.html',
  styleUrls: ['./pre-registration-list.component.css'],
  animations: [PageAnimation]
})
export class PreRegistrationListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;
  public filterParams = {};

  public data: CampusPreRegistration[] = [new CampusPreRegistration()];
  public listLabels = new CampusPreRegistration().listLabels;

  private superUser = localStorage.getItem('isSuperUser');

  public CRApplicationType = GLOBALS.CRApplicationType;
  public CRApplicationStatus = GLOBALS.CRApplicationStatus;
  public preRegistrationStates = GLOBALS.WORKFLOW.PRE_REGISTRATION.states;
  public postRegistrationStates = GLOBALS.WORKFLOW.POST_REGISTRATION.states;
  public states = this.preRegistrationStates.concat(this.postRegistrationStates);
  public assignedWFStates = [];
  public selectedState = GLOBALS.WORKFLOW.PRE_REGISTRATION.COMPLETE_LIST.value;
  public stateForCompleteList = GLOBALS.WORKFLOW.PRE_REGISTRATION.COMPLETE_LIST.value;
  displayedColumns = ['fullName', 'email', 'applicationType', 'cityName', 'state', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private campusPreRegistrationService: CampusPreRegistrationService,
    private wfStateService: WFStateService,
    public layoutService: LayoutService,
    private activatedRoute: ActivatedRoute,
    public matDialog: MatDialog,
    private router: Router) { }

  ngOnInit(): void {
    const bc = this.activatedRoute.snapshot.data['breadcrumb'];

    this.layoutService.setPageTitle({ title: bc['title'] });

    this.getRecords();

    if (this.superUser === 'false') {
      this.findAllAssignedWFStates();
    }
  }

  /**
   * Getting pre registartion list
   */
  getRecords(): void {

    this.campusPreRegistrationService.index(this.filterParams).subscribe(
      response => {
        this.data = response;
        this.data.map(element => {
          element.fullName = element.applicant.fullName;
          element.email = element.applicant.email;
          element.applicationType = this.CRApplicationType[element.applicant.applicationType].title;
          element.cityName = element.city.value;
          let state = this.states.find(item => {
            return item.value == element.wfState.state;
          });
          // if (state) {
          //   element.state = state.label;
          // } else {
          //   let state = this.postRegistrationStates.find(item => {
          //     return item.value == element.wfState.state;
          //   });
          element.state = state ? state.label : '';
          // }

        });
        this.dataSource = new MatTableDataSource<CampusPreRegistration>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {

        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  findAllAssignedWFStates() {
    this.campusPreRegistrationService.findAllAssignedWFStates().subscribe(
      response => {
        this.assignedWFStates = response;
      }, error => {
        console.log('error: ', error);
      },
      () => {
        this.preRegistrationStates = _.intersectionBy(this.preRegistrationStates, this.assignedWFStates, 'value');
        this.preRegistrationStates.unshift(GLOBALS.WORKFLOW.POST_REGISTRATION.COMPLETE_LIST);
        // this.preRegistrationStates.push(GLOBALS.WORKFLOW.POST_REGISTRATION.ACTION_PERFORMED);
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); //TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }

  loadApplications(state: string) {
    if (state == this.stateForCompleteList) {
      this.filterParams = {};
    } else {
      this.filterParams = { state: state };
    }
    this.getRecords();
  }

  addNewApplication(){
    const dialogRef = this.matDialog.open(AddPreRegistrationComponent, {
      width: '1200px',
      data: { portal: 'registration' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getRecords();
        // this.findAllTimeTableDetails(this.timeTable.id);
      }
    });
  }
}
