import { Component, OnInit, Inject,Input } from '@angular/core';
import { NgControl, FormArray, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CampusPreRegistration, CRApplication, CRApplicant } from '../../../../../public/models';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { WFStateService } from '../../../../../settings/services';
import { DocStateManagerService, ConfigurationService } from '../../../../../shared/services';
import { InstituteTypeService } from '../../../../../institute/services';
import { CampusPreRegistrationFormService } from '../../../../../public/services';

import { ConfigurationModel, ConfigurationOptionModel } from '../../../../../shared/models';
import { InstituteTypeModel } from '../../../../../institute/models';

import { GLOBALS } from '../../../../../app/config/globals';

// for dat picker format
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
const moment = _moment;

// for Mobile Number
interface ICountryOption {
  id: number,
  abbreviation: string;
  countryCode: string;
}
@Component({
  selector: 'add-pre-registration',
  templateUrl: './add-pre-registration.component.html',
  styleUrls: ['./add-pre-registration.component.css'],
})
export class AddPreRegistrationComponent implements OnInit {
  public loaded: boolean = true;
  public boxLoaded: boolean = true;
  formGroup: FormGroup;
  campusPreRegistration: CampusPreRegistration;
  cities: ConfigurationModel[];
  instituteTypes: InstituteTypeModel[];
  applicationTypes: any[] = [GLOBALS.CRApplicationType.create, GLOBALS.CRApplicationType.transfer];

  componentsLabel = new CampusPreRegistration().labels;

  public _mobileNumberMask = GLOBALS.masks.mobile;
  public _cnicMask = GLOBALS.masks.cnic;

  public isValidEmail: boolean;

  public stateId: number;

  campus: CRApplication;

  public tentativeSessionStart = new FormControl(moment());

  // for Mobile Number
  public countriesOptions: ICountryOption[] = [];
  /**
   *
   * @param formBuilder for angular reactive forms
   */
  constructor(
    private formBuilder: FormBuilder,
    private configurationService: ConfigurationService,
    private campusPreRegistrationFormService: CampusPreRegistrationFormService,
    private router: Router,
    private wfStateService: WFStateService,
    private instituteTypeService: InstituteTypeService,
    private docStateManagerService: DocStateManagerService,
    public dialogRef: MatDialogRef<AddPreRegistrationComponent>,
    
  ) { }

  /**
   * ngOnInit()
   */
  ngOnInit() {
    this.getStateId();
    this.getInstituteTypes();
    this.findCountryAttrbutesList();
    this.initForm();
  }

  initForm() {
    /**
     * initializing from Group
     */
    this.formGroup = this.formBuilder.group(new CampusPreRegistration().validationRules());
    /**
     * for Datepicker
     */

    this.formGroup.enable();
  }

  saveData(data: any) {

    this.boxLoaded = false;

    //TODO:high Check for unique email adrress

    this.campusPreRegistrationFormService.validateUniqueEmail({ email: data.email }).subscribe(response => {

      const _applicant: CRApplicant = new CRApplicant();
      _applicant.fullName = data.fullName;
      // Mobile Number with country Code
      _applicant.mobileNumber = data.countryCode + '-' + data.mobileNumber;
      _applicant.cnic = data.cnic;
      _applicant.email = data.email;
      _applicant.applicationType = data.applicationType;

      let _user: any = {};
      _user['password'] = data.password;
      _user['username'] = data.email;
      //TODO need to change this portal
      _user['portal'] = 'registration';
      _user['isActive'] = false;

      const _campus: CRApplication = new CRApplication();
      _campus.tentativeSessionStart = data.tentativeSessionStart;
      _campus.cityId = data.cityId;
      // _campus.status = data.status;
      _campus.applicationType = data.applicationType;
      _campus.instituteTypeId = data.instituteTypeId;
      _campus.stateId = this.stateId;
      _campus.campusName = '';

      const _campusPreRegistration = {
        applicant: _applicant,
        campus: _campus,
        user: _user
      };

      this.campusPreRegistrationFormService.add(_campusPreRegistration).subscribe(
        response => {
          this.dialogRef.close(true)
          this.router.navigate([`/campus/pre-registrations/${response['id']}`]);
        },
        error => {
          this.loaded = true;
          this.boxLoaded = true;
        },
        () => {
          this.loaded = true;
          this.boxLoaded = true;
        }
      );
    }, error => {

      this.formGroup.controls['email'].setErrors({ exist: true });
      this.loaded = true;
      this.boxLoaded = true;

    });
  }
  cancel() {
    window.history.back();
  }

  getInstituteTypes() {
    this.instituteTypeService.findAttributesList().subscribe(
      response => {
        this.instituteTypes = response;
      },
      error => console.log(error),
      () => {
      }
    );
  }

  /**
   * Function to Get State ID of Pre-Registration Work Flow
   */

  getStateId() {
    var postData = {
      WFId: 'pre-registration',
      state: 'submit'
    };
    this.docStateManagerService.getStateID(postData).subscribe(
      response => {
        this.stateId = response['id'];
      },
      error => console.log(error),
      () => { }
    );
  }

  /**
   * Load cities of selected country
   * @param countryCode 
   */
  loadCities(countryCode) {
    // find selected country
    let country = this.countriesOptions.find(element => {
      return element.countryCode == countryCode;
    });

    this.configurationService.findAllCities(country.id).subscribe(
      response => {
        this.cities = response;
      },
      error => {
        console.log(error);
      },
      () => {
      }
    );
  }

  // For Mobile Number
  // Getting the countries from configuratiuon and then pushing into the array of object
  findCountryAttrbutesList() {
    this.configurationService.findAttributesList('country').subscribe(
      response => {
        // iterate countries
        response.forEach(country => {
          let abbr;
          let cCode;
          // iterate countries config options
          country.configurationOptions.forEach(configurationOption => {
            // check if option is abbreviation
            if (configurationOption.key === 'abbreviation') {
              abbr = configurationOption.value
              // check if option is country code
            } else if (configurationOption.key === 'countryCode') {
              cCode = configurationOption.value
            }
            // if both exists then push into the array
            if (abbr && cCode) {

              this.countriesOptions.push({ id: country.id, abbreviation: abbr, countryCode: cCode })
            }
          });
        });
        this.selectDefaultCountry();
      },
      error => console.log(error),
      () => { }
    );
  }

  /**
   * Setting default country and loading its cities
   */
  selectDefaultCountry() {
    
    let pak: ICountryOption;
    
    this.countriesOptions.forEach(country => {
      if (country.abbreviation.toLowerCase() === 'pak' || country.abbreviation.toLowerCase() === 'pk') {
        pak = country;
      }
    });
    
    if (pak) {
      this.formGroup.get('countryCode').setValue(pak.countryCode);
      this.loadCities(pak.countryCode);
    }
  }

}