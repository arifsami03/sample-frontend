import { EmailTemplateService } from '../../../../settings/services';
import { GLOBALS } from '../../../../app/config/globals';
import { Component, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { PageAnimation } from '../../../../shared/helper';
import { LayoutService } from '../../../../layout/services';
import { CampusPreRegistrationService } from '../../../services';
import { CampusPreRegistration } from '../../../../public/models';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'campus-pre-registration-view',
  templateUrl: './pre-registration-view.component.html',
  styleUrls: ['./pre-registration-view.component.css'],
  providers: [EmailTemplateService],
  animations: [PageAnimation]
})
export class PreRegistrationViewComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public boxLoaded: boolean = true;

  public isEmailSending = false;
  public CRApplicationType = GLOBALS.CRApplicationType;
  public CRApplicationStatus = GLOBALS.CRApplicationStatus;
  public session = GLOBALS.session;
  campusPreRegistration: CampusPreRegistration = new CampusPreRegistration();
  public labels = new CampusPreRegistration().labels;
  public preRegStatus: string;
  public selectedButton: string = 'personal';
  public userId: number;

  CRApplicationId: number;
  constructor(
    private campusPreRegistrationService: CampusPreRegistrationService,
    public layoutService: LayoutService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar
  ) { }

  
  ngOnInit(): void {
    this.layoutService.setPageTitle({
      title: 'Campus Pre Registrations Form'
    });
    this.getParams(); 
  }

  getParams(): void {
    this.activatedRoute.params.subscribe(params => {
      this.CRApplicationId = Number(params['id']);
      this.getRecords(this.CRApplicationId);
    });
  }
  /**
   * Getting record of single pre registration
   * @param _id
   */
  getRecords(_id: number): void {
    this.campusPreRegistrationService.find(_id).subscribe(
      response => {
        this.campusPreRegistration = response;
        // var dateFormat = require('dateformat');
        // var now = this.campusPreRegistration.tentativeSessionStart;
        // this.campusPreRegistration.tentativeSessionStart = dateFormat(now, " mmmm dS, yyyy");
      },
      error => { },
      () => {
        this.loaded = true;
      }
    );
  }
  /**
   * Change Status of pre registration
   * @param _status
   */
  // changeStatus(_status: string) {
  //   this.boxLoaded = false;
  //   // this.isEmailSending = true;
  //   this.campusPreRegistrationService.changeStatus(this.CRApplicationId, { status: _status }).subscribe(
  //     response => {
  //       // this.snackBar.open('Email has been sent.', _status.toUpperCase(), {
  //       //   duration: 2000
  //       // });

  //       this.layoutService.flashMsg({ msg: 'Email has been sent.', msgType: 'success' });
  //       this.preRegStatus = _status;
  //       // this.campusPreRegistration.status = _status;
  //       this.isEmailSending = false;
  //     },
  //     error => {
  //       // this.snackBar.open('Email is not sent.', 'ERROR', {
  //       //   duration: 2000
  //       // });
  //       this.layoutService.flashMsg({ msg: 'Email has not been sent.', msgType: 'error' });
  //       this.isEmailSending = false;
  //     },
  //     () => {
  //       this.boxLoaded = true;
  //     }
  //   );
  // }

  tabChange(type: string) {
    this.selectedButton = type;
  }

  getUserId() {
    this.campusPreRegistrationService.getUserId(this.CRApplicationId).subscribe(
      response => {
        this.userId = response['id'];
        this.activeUser(this.userId);
      },
      error => { },
      () => {
        this.loaded = true;
      }
    );
  }

  activeUser(userId) {
    let item = {
      isActive: true
    };
    this.campusPreRegistrationService.activeUser(userId, item).subscribe(
      response => {
        // this.userId=response['id'];
        // this.activeUser(this.userId);
      },
      error => { },
      () => {
        this.loaded = true;
      }
    );
  }

  onChangeStateEvent(item) {
    
    if (item['status'] == true && item['state'] == 'approve') {
      // make this user active.
      this.getUserId();
    }

    this.router.navigate([`/campus/pre-registrations`]);
  }
}
