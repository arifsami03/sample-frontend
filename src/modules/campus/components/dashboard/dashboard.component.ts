import { Component, ViewChild, OnInit } from '@angular/core';

import { PageAnimation } from '../../../shared/helper';
import { LayoutService } from '../../../layout/services';

@Component({
  selector: 'campus-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [PageAnimation]
})
export class DashboardComponent implements OnInit {

  public pageState = 'active';
  
  constructor(public layoutService: LayoutService) {}

  ngOnInit(): void {
    this.layoutService.setPageTitle({
      title: 'Dashboard'
    });
  }
}
