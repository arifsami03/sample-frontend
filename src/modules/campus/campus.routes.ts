import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GLOBALS } from '../app/config/globals';

import {
  ApplicationListComponent,
  PreRegistrationListComponent,
  PreRegistrationViewComponent,
  PersonalInformationComponent,
  CampusInfoComponent,
  RemittanceComponent,
  DashboardComponent,
  PostRegistrationViewComponent,
  SubmitComponent,
  PostRegistrationListComponent,
  MouFormComponent,
  MouListComponent,
  MouVerificationFormComponent,
  MouVerificationListComponent,
  CreateCampusFormComponent,
  CreateCampusListComponent,
  AnalysisViewComponent,
  AccountInfoListComponent,
  AccountInfoFormComponent,
  CampusListComponent,
  CampusViewComponent,
  EditPostRegistrationComponent,
  ProspectusSaleFormComponent
} from './components';
import { AuthGuardService } from '../app/services';

/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: '',
    data: { breadcrumb: { title: 'Campus', display: true } },
    children: [{
      path: '',
      data: { breadcrumb: { title: 'Campus', display: false } },
      canActivate: [AuthGuardService],
      component: DashboardComponent,
    },
    {
      path: 'applications',
      data: { breadcrumb: { title: 'Applications', display: true, disable: true } },
      children: [
        {
          path: '',
          canActivate: [AuthGuardService],
          component: ApplicationListComponent,
          data: {
            breadcrumb: { title: 'Applications', display: false },
          }
        }
      ]
    },
    {
      path: 'campus',
      data: { breadcrumb: { title: 'Campus', display: true, disable: true } },
      children: [
        {
          path: '',
          canActivate: [AuthGuardService],
          component: CampusListComponent,
          data: { breadcrumb: { title: 'Campus', display: false } }
        },
        {
          path: ':id',
          canActivate: [AuthGuardService],
          component: CampusViewComponent,
          data: { breadcrumb: { title: 'Detail', display: true } }
        }
      ]
    },
    {
      path: 'prospectusSale',
      data: { breadcrumb: { title: 'Prospectus Sale', display: true, disable: true } },
      children: [
        {
          path: '',
          canActivate: [AuthGuardService],
          component: ProspectusSaleFormComponent,
          data: {
            act: GLOBALS.pageActions.create,
            breadcrumb: { title: 'Prospectus Sale', display: false }
          }
        
        },
        
      ]
    },
    {
      path: 'pre-registrations',
      data: { breadcrumb: { title: 'Pre Registration', display: true, disable: true } },
      children: [
        {
          path: '',
          canActivate: [AuthGuardService],
          component: PreRegistrationListComponent,
          data: { breadcrumb: { title: 'Pre Registration', display: false } }
        },
        {
          path: ':id',
          canActivate: [AuthGuardService],
          component: PreRegistrationViewComponent,
          data: { breadcrumb: { title: 'Detail', display: true } }
        }
      ]
    },
    {
      path: 'post-registrations',
      data: { breadcrumb: { title: 'Post Registration', display: true, disable: true } },
      children: [
        {
          path: '',
          canActivate: [AuthGuardService],
          component: PostRegistrationListComponent,
          data: { breadcrumb: { title: 'Post Registration', display: false }, act: GLOBALS.pageActions.view }
        },
        {
          path: 'draft/:id',
          component: PostRegistrationViewComponent,
          data: { breadcrumb: { title: 'Detail', display: true }, act: GLOBALS.pageActions.view }
        },
        {
          path: 'submit/:id',
          canActivate: [AuthGuardService],
          component: PostRegistrationViewComponent,
          data: { breadcrumb: { title: 'Detail', display: true }, act: GLOBALS.pageActions.view }
        },
        {
          path: 'reject/:id',
          canActivate: [AuthGuardService],
          component: PreRegistrationViewComponent,
          data: { breadcrumb: { title: 'Detail', display: true } }
        },
        {
          path: 'forward-for-analysis/:id',
          canActivate: [AuthGuardService],
          component: AnalysisViewComponent,
          data: { breadcrumb: { title: 'Detail', display: true } }
        },
        {
          path: 'submit-analysis/:id',
          canActivate: [AuthGuardService],
          component: PostRegistrationViewComponent,
          data: { breadcrumb: { title: 'Detail', display: true } }
        },
        {
          path: 'forward-for-create-mou/:id',
          canActivate: [AuthGuardService],
          component: PostRegistrationViewComponent,
          data: { breadcrumb: { title: 'Detail', display: true } }
        },
        {
          path: 'mou-approval/:id',
          canActivate: [AuthGuardService],
          component: MouVerificationFormComponent,
          data: { breadcrumb: { title: 'Detail', display: true } }
        },
        {
          path: 'create-campus/:id',
          canActivate: [AuthGuardService],
          component: PostRegistrationViewComponent,
          data: { breadcrumb: { title: 'Detail', display: true } }
        },
        {
          path: 'forward-for-account-info/:id',
          canActivate: [AuthGuardService],
          component: PostRegistrationViewComponent,
          data: { breadcrumb: { title: 'Account Information', display: true }}
        },
        {
          path: 'done/:id',
          canActivate: [AuthGuardService],
          component: PostRegistrationViewComponent,
          data: { breadcrumb: { title: 'Detail', display: true },act: GLOBALS.pageActions.view}
        },
        {
          path: 'update/:id',
          component: EditPostRegistrationComponent,
          data: { breadcrumb: { title: 'Campus Post Registration', display: true }, act: GLOBALS.pageActions.update }
        },
        {
          path: 'create',
          component: EditPostRegistrationComponent,
          data: { breadcrumb: { title: 'Campus Post Registration', display: true }, act: GLOBALS.pageActions.create }
        }

      ]
    }]
  },

  {
    path: 'personalinfo',
    canActivate: [AuthGuardService],
    component: PersonalInformationComponent
  },
  {
    path: 'campusinfo',
    canActivate: [AuthGuardService],
    component: CampusInfoComponent
  },
  {
    path: 'remittance',
    canActivate: [AuthGuardService],
    component: RemittanceComponent
  },
  {
    path: 'submit',
    canActivate: [AuthGuardService],
    component: SubmitComponent
  }
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampusRoutes { }
