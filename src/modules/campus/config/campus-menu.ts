export const CampusMenu = [
  {
    heading: 'Campus',
    menuItems: [
      // { title: 'Dashboard', url: ['/campus/dashboard'], perm: 'n/a' },
      // { title: 'Applications', url: ['/campus/applications'], perm: 'applications.index' },
      { title: 'Campuses', url: ['/campus/campus'], perm: 'campus.index' },
    ]
  },
  {
    heading: 'Registrations',
    menuItems: [
      { title: 'Pre-Registrations', url: ['/campus/pre-registrations'], perm: 'preRegistrations.index' },
      { title: 'Post-Registrations', url: ['/campus/post-registrations'], perm: 'postRegistrations.index' },
      { title: 'Prospectus Sale', url: ['/campus/prospectusSale'], perm: 'postRegistrations.index' },
    ]
  }
];
