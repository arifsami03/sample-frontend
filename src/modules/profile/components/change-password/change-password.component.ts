import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormBuilder, FormGroup } from '@angular/forms';

import { ProfileService } from '../../services';
import { LayoutService } from '../../../layout/services';
import { ProfileModel } from '../../models';

@Component({
  selector: 'profile-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  public fg: FormGroup;
  public pageTitle: string;
  public profile: ProfileModel;
  private profileId: number;

  public profileLabels = ProfileModel.attributesLabels;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private profileService: ProfileService,
    private layoutService: LayoutService
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.profileId = param.id;
    });
    this.fg = this.fb.group(new ProfileModel().changePasswordValidationRules());
    this.modifyPageHeader();
  }

  private modifyPageHeader() {
    this.layoutService.setPageTitle({

      title: 'Change Password: ' + name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Profile' },
        { label: 'Change Password' }
      ]
    });
  }

  public saveData(item: ProfileModel) {
    this.profileService.changePassword(this.profileId, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Password has been changed.', msgType: 'success' });
        this.router.navigate([`/profile/view/${this.profileId}`]);
      },
      error => {
        this.fg.controls['currentPassword'].setErrors({ notValid: true });
      },
      () => { }
    );
  }

}
