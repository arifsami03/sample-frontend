import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormBuilder, FormGroup } from '@angular/forms';

import { ProfileService } from '../../services';
import { LayoutService } from '../../../layout/services';
import { ProfileModel } from '../../models';
import { GLOBALS } from '../../../app/config/globals';
import { ConfigurationService } from '../../../shared/services';

// for Mobile Number
interface ICountryOption {
  abbreviation: string;
  countryCode: string;
}
@Component({
  selector: 'profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.css']
})
export class ProfileFormComponent implements OnInit {
  public loaded: boolean = false;

  public fg: FormGroup;
  public pageTitle: string;
  public profile: ProfileModel;
  public phoneNumberMask = GLOBALS.masks.mobile;
  private profileId: number;

  public profileLabels = ProfileModel.attributesLabels;
  // for Mobile Number
  public countriesOptions: ICountryOption[] = [];

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private profileService: ProfileService,
    private configurationService: ConfigurationService,
    private layoutService: LayoutService
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.profileId = param.id;
    });
    this.getData(this.profileId);
    this.findCountryAttrbutesList();
  }

  private getData(profileId: number) {
    this.profileService.find(profileId).subscribe(
      response => {
        this.profile = response;
      },
      error => {
        this.loaded = true;
      },
      () => {
        this.fg = this.fb.group(new ProfileModel().validationRules());
        this.fg.patchValue(this.profile);
        this.patchMobileNumber();
        this.loaded = true;
        this.modifyPageHeader();
      }
    );
  }

  private modifyPageHeader() {
    let name = this.profile.name ? this.profile.name : this.profile.username;
    this.layoutService.setPageTitle({

      title: 'Profile: ' + name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Profile' },
        { label: name }
      ]
    });
  }

  public saveData(item: ProfileModel) {
    item.phoneNumber = item.countryCode + '-' + item.phoneNumber;
    this.profileService.update(this.profileId, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Profile has been updated.', msgType: 'success' });
        this.router.navigate([`/profile/view/${this.profile.id}`]);
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
      },
      () => {
        this.loaded = true;
      }
    );
  }
  // For Mobile Number
  // Getting the countries from configuratiuon and then pushing into the array of object
  findCountryAttrbutesList() {
    this.configurationService.findAttributesList('country').subscribe(
      response => {
        // iterate countries
        response.forEach(country => {
          let abbr;
          let cCode;
          // iterate countries config options
          country.configurationOptions.forEach(configurationOption => {
            // check if option is abbreviation
            if (configurationOption.key === 'abbreviation') {
              abbr = configurationOption.value
              // check if option is country code
            } else if (configurationOption.key === 'countryCode') {
              cCode = configurationOption.value
            }
            // if both exists then push into the array
            if (abbr && cCode) {

              this.countriesOptions.push({ abbreviation: abbr, countryCode: cCode })
            }
          });
        });
      },
      error => console.log(error),
      () => { }
    );
  }


  patchMobileNumber() {
    // for first mobile number
    if (this.profile.phoneNumber) {
      let mobileNumber: string = this.profile.phoneNumber;
      let countryCode: string = mobileNumber.substring(0, mobileNumber.indexOf('-'));
      let mobileNumberSecondPart: string = mobileNumber.substring(mobileNumber.indexOf('-') + 1, mobileNumber.length);

      this.fg.get('countryCode').patchValue(countryCode);
      this.fg.get('phoneNumber').patchValue(mobileNumberSecondPart);
    }



  }
}
