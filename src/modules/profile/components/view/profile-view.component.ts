import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ProfileModel } from '../../models';
import { ProfileService, } from '../../services';
import { LayoutService } from '../../../layout/services';

@Component({
  selector: 'profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.css']
})
export class ProfileViewComponent implements OnInit {
  public loaded: boolean = false;

  public pageTitle: string;
  public profile: ProfileModel = new ProfileModel();
  public profileLabels = ProfileModel.attributesLabels;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private profileService: ProfileService,
    private layoutService: LayoutService,
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.profile.id = param.id;
    });
    this.getData(this.profile.id);
  }

  private getData(id: number) {
    this.profileService.find(id).subscribe(
      response => {
        this.profile = response;
      },
      error => {
        this.loaded = true;
      },
      () => {
        this.loaded = true;
        this.modifyPageHeader();
      }
    );
  }


  private modifyPageHeader() {
    let name = this.profile.name ? this.profile.name : this.profile.username;
    this.layoutService.setPageTitle({

      title: 'Profile: ' + name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Profile' },
        { label: name }
      ]
    });
  }

}
