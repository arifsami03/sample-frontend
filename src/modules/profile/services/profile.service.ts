import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { BaseService } from '../../shared/services'; 

import { ProfileModel } from '../models';

@Injectable()
export class ProfileService extends BaseService {
  private routeURL: String = 'profile/profile';

  constructor(protected http: Http) {
    super(http)
  }

  find(id: number) {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return data.json();
    });
  }

  update(id: Number, profile: ProfileModel) {
    return this.__put(`${this.routeURL}/update/${id}`, profile).map(data => {
      return data.json();
    });
  }


  forgotPassword(item) {
    return this.__post(`${this.routeURL}/forgotPassword`, item)
      .map(response => {
        return response.json();
      })
      .catch(this.handleError);
  }

  changePassword(id: Number, profile: ProfileModel) {
    return this.__post(`${this.routeURL}/changePassword/${id}`, profile).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  resetPassword(profile: ProfileModel) {
    return this.__post(`${this.routeURL}/resetPassword`, profile).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  validateVerificationCode(verificationCode: string) {
    return this.__post(`${this.routeURL}/validateVerificationCode`, { verificationCode: verificationCode }).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

}
