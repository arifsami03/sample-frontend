import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  ProfileViewComponent,
  ProfileFormComponent,
  ChangePasswordComponent
} from './components';
import { AuthGuardService } from '../app/services';

/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: '',
    data: { breadcrumb: { title: 'Profile', display: false } },
    children: [
      {
        path: 'view/:id',
        canActivate: [AuthGuardService],
        component: ProfileViewComponent,
        data: { breadcrumb: { title: 'View', display: false } }
      },
      {
        path: 'update/:id',
        canActivate: [AuthGuardService],
        component: ProfileFormComponent,
        data: { breadcrumb: { title: 'Update', display: false } }
      },
      {
        path: 'changePassword/:id',
        canActivate: [AuthGuardService],
        component: ChangePasswordComponent,
        data: { breadcrumb: { title: 'Change Password', display: false } }
      }
    ]
  },
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutes { }
