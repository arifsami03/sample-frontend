// Import components and services etc here
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MatInputModule,
  MatFormFieldModule,
  MatIconModule,
  MatDialogModule,
  MatButtonModule,
  MatCardModule,
  MatTableModule,
  MatPaginatorModule,
  MatOptionModule,
  MatAutocompleteModule,
  MatSelectModule,
  MatDividerModule,
  MatListModule,
  MatExpansionModule,
  MatCheckboxModule,
  MatProgressBarModule,
  MatRadioModule
} from '@angular/material';
import { TextMaskModule } from 'angular2-text-mask';

import { ProfileService } from './services'

import {
  ProfileViewComponent,
  ProfileFormComponent,
  ChangePasswordComponent
} from './components';

export const __IMPORTS = [
  FormsModule,
  ReactiveFormsModule,
  FlexLayoutModule,
  MatInputModule,
  MatFormFieldModule,
  MatIconModule,
  MatDialogModule,
  MatButtonModule,
  MatCardModule,
  MatTableModule,
  MatPaginatorModule,
  MatOptionModule,
  MatAutocompleteModule,
  MatSelectModule,
  MatDividerModule,
  MatListModule,
  MatExpansionModule,
  MatCheckboxModule,
  MatProgressBarModule,
  MatRadioModule,
  TextMaskModule
];

export const __DECLARATIONS = [
  ProfileViewComponent,
  ProfileFormComponent,
  ChangePasswordComponent
];

export const __PROVIDERS = [
  ProfileService,
];

export const __ENTRY_COMPONENTS = [
];
