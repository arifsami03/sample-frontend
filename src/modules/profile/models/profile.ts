import { FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

export class ProfileModel {
  static attributesLabels = {
    username: 'Username',
    name: 'Name',
    phoneNumber: 'Phone Number',
    address: 'Address',
    gender: 'Gender',
    currentPassword: 'Current Password',
    newPassword: 'New Password',
    confirmNewPassword: 'Confirm New Password',
    countryCode : 'Country'


  };

  public id?: number;
  public name?: string;
  public phoneNumber?: string;
  public countryCode?: string;
  public address?: string;
  public gender?: string;
  public username: string;
  public verificationCode: string;
  public createdBy?: number;
  public updatedBy?: number;

  constructor() { }


  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      name: new FormControl(''),
      phoneNumber: new FormControl('', [Validators.required, Validators.pattern('([0-9]{3})-([0-9]{7})'), Validators.maxLength(12)]),
      address: new FormControl(''),
      gender: new FormControl(''),
      countryCode: new FormControl('', Validators.required),

    };
  }

  /**
   * Change Passwor Validation Rules
   */
  public changePasswordValidationRules?() {
    return {
      currentPassword: new FormControl('', [Validators.required]),
      newPassword: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(30)]),
      confirmNewPassword: new FormControl('', [Validators.required, this.equalTo('newPassword')]),

    };
  }

  /**
   * Reset Password Validation Rules
   */
  public resetPasswordValidationRules?() {
    return {
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(30)]),
      confirmPassword: new FormControl('', [Validators.required, this.equalTo('password')]),

    };
  }

  /**
     *
     * @param equalControlName
     */
  public equalTo?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      if (!control['_parent']) return null;

      if (!control['_parent'].controls[equalControlName])
        throw new TypeError(
          'Form Control ' + equalControlName + ' does not exists.'
        );

      let controlMatch = control['_parent'].controls[equalControlName];

      return controlMatch.value == control.value
        ? null
        : {
          equalTo: true
        };
    };
  }
}
