import { UserService } from '../../security/services';
import { Router, CanActivate, ActivatedRoute } from '@angular/router';
import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(
    public router: Router,
    private userService: UserService,
    private activatedRoute: ActivatedRoute
  ) { }

  canActivate(): boolean {

    if (!this.isAuthenticated()) {
      this.router.navigate(['/home']);
    }
    return true;
  }

  /**
   * TODO:low the same function is in employee.service.ts file. Do something about it.
   */
  isAuthenticated(): boolean {
    if (localStorage.getItem('id') && localStorage.getItem('token') && localStorage.getItem('token') !== undefined) {
      return true;
    } else {
      return false;
    }
  }
}
