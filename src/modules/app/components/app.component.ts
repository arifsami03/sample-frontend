import { Component, ChangeDetectorRef } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd } from '@angular/router';
import { PageAnimation } from '../../shared/helper';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [PageAnimation]
})
export class AppComponent {
  
  public pageState;

  public loadingRouteConfig: boolean;

  constructor( private ref: ChangeDetectorRef, private router: Router) {

    this.ref.detach();

    setInterval(() => {
      this.ref.detectChanges();
    }, 5);
    
  }

  ngOnInit () {
    this.router.events.subscribe(event => {
        if (event instanceof RouteConfigLoadStart) {
          this.pageState = 'active';
            this.loadingRouteConfig = true;
        } else if (event instanceof RouteConfigLoadEnd) {
          this.pageState = '';
          this.loadingRouteConfig = false;
        }
    });
}
}
