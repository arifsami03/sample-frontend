// import { UserFormComponent } from './security/users/form/user-form.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components';
import { AppRoutes } from './app.routes';
import { SharedModule } from '../shared/shared.module';
import { LayoutModule } from '../layout/layout.module';
import { PublicModule } from '../public/public.module';
import {
  __IMPORTS,
  __DECLARATIONS,
  __PROVIDERS,
  __ENTRY_COMPONENTS
} from './components.barrel';

@NgModule({
  declarations: [__DECLARATIONS],
  entryComponents: [__ENTRY_COMPONENTS],
  imports: [
    RouterModule,
    AppRoutes,
    SharedModule,
    LayoutModule,
    PublicModule,
    __IMPORTS
  ],
  providers: [__PROVIDERS],
  bootstrap: [AppComponent]
})
export class AppModule {}
