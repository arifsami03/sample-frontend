import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CdkTableModule } from '@angular/cdk/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';


import {
  MatIconRegistry,
  MatProgressSpinnerModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatTableModule,
  MatSelectModule,
  MatMenuModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatListModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatTabsModule
} from '@angular/material';
import { TextMaskModule } from 'angular2-text-mask';
import 'hammerjs';
import { AppComponent } from './components';
import { AuthGuardService } from './services';
import { UserService } from '../security/services';

/**************************************************** */

export const __IMPORTS = [
  CommonModule,
  BrowserModule,
  BrowserAnimationsModule,
  HttpModule,
  HttpClientModule,
  MatInputModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatTableModule,
  FormsModule,
  ReactiveFormsModule,
  CdkTableModule,
  MatSelectModule,
  MatMenuModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatListModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatFormFieldModule,
  FlexLayoutModule,
  MatTabsModule,
  TextMaskModule,
  MatProgressSpinnerModule
];

export const __DECLARATIONS = [
  AppComponent,
];

export const __PROVIDERS = [MatIconRegistry, AuthGuardService, UserService];

export const __ENTRY_COMPONENTS = [];
