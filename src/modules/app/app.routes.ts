import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';

import {
  AppComponent,
} from './components';
import { AuthGuardService as AuthGuard } from './services';

import {
  LayoutComponent,
  PublicLayoutComponent,
  PostRegistrationLayoutComponent
} from '../layout/components';
import { PersonalInformationComponent } from '../campus/components';
import { GLOBALS } from './config/globals';

/**
 * We need to check for root url that where should it go regarding logged in user's portal
 *
 * If portal is institute got to /institute/dashboard
 * If portal is registration got to /post-registration/campus/personalinfo
 * If portal is campus got to 'not implemented yet'.
 *
 */
let rootRedirectUrl = '';

//TODO:high We have to check this same in hader.component.ts WHY?
if (localStorage.getItem('portal') == 'university' || localStorage.getItem('portal') == 'institute') {
  rootRedirectUrl = '/institute/dashboard';
} else if (localStorage.getItem('portal') == 'registration') {
  rootRedirectUrl = '/post-registration/campus/personalinfo';
} else if (localStorage.getItem('portal') == 'campus') {
  rootRedirectUrl = 'n/a';
}

/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: 'home',
    component: PublicLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: 'modules/public/public.module#PublicModule'
      }
    ]
  },
  {
    path: 'post-registration',
    canActivate: [AuthGuard],
    component: PostRegistrationLayoutComponent,
    children: [
      {
        path: 'campus',
        loadChildren: 'modules/campus/campus.module#CampusModule',
        data: { breadcrumb: { display: false } }
      }
    ]
  },
  {
    path: '',
    canActivate: [AuthGuard],
    component: LayoutComponent,
    data: { breadcrumb: { title: 'Home', display: true } },
    children: [
      {
        path: '',
        redirectTo: rootRedirectUrl,
        pathMatch: 'full'
      },
      {
        path: 'institute',
        loadChildren: 'modules/institute/institute.module#InstituteModule',
        data: { breadcrumb: { title: 'Institute', display: false } }
      },
      {
        path: 'fee-management',
        loadChildren: 'modules/fee-management/fee-management.module#FeeManagementModule',
        data: { breadcrumb: { title: 'Fee Management', display: false } }
      },
      {
        path: 'campus',
        loadChildren: 'modules/campus/campus.module#CampusModule',
        data: { breadcrumb: { title: 'Campus', display: true, disable: true } }
      },
      {
        path: 'academic',
        loadChildren: 'modules/academic/academic.module#AcademicModule',
        data: { breadcrumb: { title: 'Academic', display: false } }
      },
      {
        path: 'admission',
        loadChildren: 'modules/admission/admission.module#AdmissionModule',
        data: { breadcrumb: { title: 'Admission', display: false } }
      },
      {
        path: 'time-table',
        loadChildren: 'modules/time-table/time-table.module#TimeTableModule',
        data: { breadcrumb: { title: 'Fee Management', display: false } }
      },
      
      {
        path: 'settings',
        loadChildren:
          'modules/settings/settings.module#SettingsModule',
        data: {
          breadcrumb: { title: 'Setting', display: true, disable: true }
        }
      },
      {
        path: 'security',
        loadChildren: 'modules/security/security.module#SecurityModule',
        data: {
          breadcrumb: { title: 'Security', display: true, disable: true }
        }
      },
      {
        path: 'profile',
        loadChildren: 'modules/profile/profile.module#ProfileModule',
        data: {
          breadcrumb: { title: 'Profile', display: true, disable: true }
        }
      }
    ]
  },

  { path: '**', redirectTo: '' }
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutes { }
