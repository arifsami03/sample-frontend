import { FormControl, Validators } from '@angular/forms';

export class FeeStructureClassesModel {


  id?: number;
  feeStructureId: number;
  classId: number;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() { }

}
