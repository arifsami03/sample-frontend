import { FormControl, Validators } from '@angular/forms';
import { FeeHeadsModel } from '.';
import { ConfigurationModel } from '../../shared/models';

export class FeeStructureFeeHeadsModel {

  /**
   * Set the labels for attributes 
   */
  static attributesLabels = {
    feeStructure: 'Fee Structure',
    feeHead: 'Fee Head',
    typeOfFeeHead: 'Type of FeeHead',
    amount: 'Amount',
    managementFeeFormat: 'Management Fee Format',
    managementFeeAmount: 'Management Fee Amount'
  };

  id?: number;
  feeStructureId: number;
  feeHeadsId: number;
  typeOfFeeHeadId: number;
  amount: number;
  managementFeeFormat: string;
  managementFeeAmount: number;
  feeHead?: FeeHeadsModel;
  typeOfFeeHead?: ConfigurationModel;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() { }


  /**
   * Form Validation Rules 
   */
  public validationRules?() {
    return {
      id: new FormControl(null),
      feeStructureId: new FormControl(null, [Validators.required]),
      feeHeadsId: new FormControl(null, [Validators.required]),
      typeOfFeeHeadId: new FormControl(null, [Validators.required]),
      amount: new FormControl(null, [Validators.required]),
      managementFeeFormat: new FormControl('', [Validators.required]),
      managementFeeAmount: new FormControl(null, [Validators.required])
    };
  }
}
