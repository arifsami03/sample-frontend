import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class ManagementFeeModel {
  /**
   * Set the labels for attributes
   */
  static attributesLabels = {
    educationLevelId: 'Level of Education',
    feeType: 'Type',
    amount: 'Amount'

  };

  id?: number;
  educationLevelId: number;
  feeType: string;
  amount: number;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      educationLevelId: new FormControl('', Validators.required),
      feeType: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
    };
  }
}
