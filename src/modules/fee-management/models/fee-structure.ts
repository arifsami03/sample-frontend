import { FormControl, Validators, FormArray } from '@angular/forms';
import { FeeStructureClassesModel, FeeStructureFeeHeadsModel, FS_InstallmentPlanModel } from '.';
import { ProgramModel, ProgramDetailsModel } from '../../academic/models';

export class FeeStructureModel {
  /**
   * Set the labels for attributes 
   */
  static attributesLabels = {
    title: 'Title',
    program: 'Program',
    programDetail: 'Program Detail',
    class: 'Class',
    feeHeads: 'Fee Heads'
  };

  id?: number;
  title: string;
  programId: number;
  program: ProgramModel;
  programDetailId: number;
  programDetail: ProgramDetailsModel;
  classes?: FeeStructureClassesModel[];
  feeHeads?: FeeStructureFeeHeadsModel[];
  installmentPlans?: FS_InstallmentPlanModel[];
  deletedClassIds?: number[];
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  // for listing
  programName ?: string;
  programDetailName ?: string;

  constructor() { }

  /**
   * Form Validation Rules 
   */
  public validationRules?() {
    return {
      id: new FormControl(null),
      title: new FormControl('', [Validators.required]),
      programId: new FormControl('', [Validators.required]),
      programDetailId: new FormControl('', [Validators.required]),
      classes: new FormArray([]),
    };
  }
}
