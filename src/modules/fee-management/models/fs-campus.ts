import { FormControl, Validators, FormArray } from '@angular/forms';
import { FeeStructureClassesModel, FeeStructureFeeHeadsModel, FeeHeadsModel, FeeStructureModel } from '.';
import { ProgramModel, ProgramDetailsModel } from '../../academic/models';
import { CampusModel } from '../../campus/models';

export class FSCampusModel {
  /**
   * Set the labels for attributes 
   */
  static attributesLabels = {
    feeStructure: 'Fee Structure',
    campus: 'Campus',
  };

  id?: number;
  feeStructureId: number;
  campusId: number;
  fscFeeHeads: FeeStructureFeeHeadsModel[];
  campus ?: CampusModel;
  feeStructure ?: FeeStructureModel;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  // for listing
  campusName ?: string;
  feeStructureTitle?: string;

  constructor() { }

  /**
   * Form Validation Rules 
   */
  public validationRules?() {
    return {
      id: new FormControl(null),
      feeStructureId: new FormControl(null, [Validators.required]),
      campusId: new FormControl(null, [Validators.required]),
      // campuses: new FormArray([]),
      fscFeeHeads: new FormArray([])
    };
  }
}
