import { FormControl, Validators, FormArray } from '@angular/forms';

export class FSCFeeHeadModel {
  /**
   * Set the labels for attributes 
   */
  static attributesLabels = {
    feeHead: 'Fee Heads',
    typeOfFeeHead: 'Type of Fee Head',
    amount: 'Amount',
    managementFeeType: 'Management Fee Type',
    managementFeeAmount: 'Management Fee Amount'
  };

  id?: number;
  FSCId: number;
  feeHeadId: number;
  typeOfFeeHeadId: number;
  amount: number;
  managementFeeType: string;
  managementFeeAmount: number;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules 
   */
  public validationRules?() {
    return {
      id: new FormControl(null),
      FSCId: new FormControl(null),
      feeHeadId: new FormControl(null, [Validators.required]),
      typeOfFeeHeadId: new FormControl(null, [Validators.required]),
      amount: new FormControl(null, [Validators.required]),
      managementFeeType: new FormControl('', [Validators.required]),
      managementFeeAmount: new FormControl(null, [Validators.required])
    };
  }
}
