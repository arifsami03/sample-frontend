import { FormControl, Validators, AbstractControl, ValidatorFn } from '@angular/forms';

export class ScholarshipModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    title: 'Title',
    abbreviation: 'Abbreviation',
    type: 'Type',
    percentage: 'Percentage'
  };

  id?: number;
  title: string;
  abbreviation: string;
  scholarshipFeeHeads: boolean;
  description: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() {}

  /**
   * Checking validation of number
   * @param equalControlName
   */
  public greaterThan?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
      [key: string]: any;
    } => {
      let controlMatch = 0;
      return controlMatch <= control.value && 100 >= control.value
        ? null
        : {
            equalTo: true
          };
    };
  }

  /**
   * Form Validation Rules for Level of Education
   */
  public validationRules?() {
    return {
      title: new FormControl('', [<any>Validators.required]),
      abbreviation: new FormControl('', [<any>Validators.required]),
      type: new FormControl('', [<any>Validators.required]),
      percentage: new FormControl('', [<any>Validators.required, this.greaterThan('percentage')]),
      scholarshipFeeHeads: new FormControl('')
    };
  }
}
