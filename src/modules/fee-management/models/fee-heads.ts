import { FormControl, Validators } from '@angular/forms';
import { ConfigurationModel } from '../../shared/models';

export class FeeHeadsModel {
  /**
   * Set the labels for attributes 
   */
  static attributesLabels = {
    title: 'Title',
    abbreviation: 'Abbreviation',
    code: 'Code',
    natureOfFeeHeads: 'Nature of Fee Heads'
  };

  id?: number;
  title: string;
  abbreviation: string;
  code: string;
  natureOfFeeHeadsId: number;
  natureOfFeeHeads: ConfigurationModel;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules 
   */
  public validationRules?() {
    return {
      id: new FormControl(null),
      title: new FormControl('', [Validators.required]),
      abbreviation: new FormControl('', [Validators.required]),
      code: new FormControl('', [Validators.required]),
      natureOfFeeHeadsId: new FormControl('', [Validators.required])
    };
  }
}
