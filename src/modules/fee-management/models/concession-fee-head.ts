import { FormControl, Validators } from '@angular/forms';

export class ConcessionFeeHeadModel {

  id?: number;
  consessionId?: number;
  feeHeadId?: number;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  static attributesLabels = {
    consessionId: 'Concession',
    feeHeadId: 'Fee Head',
  };

  constructor() { }

  /**
   * Form Validation Rules
   * 
   */
  public validationRules?() {

    return {
      consessionId: new FormControl('', [<any>Validators.required]),
      feeHeadId: new FormControl('', [<any>Validators.required]),
    };
  }
}
