export class FSILineItemModel {

  id?: number;
  FSInstallementId: number;
  feeHeadId: number;
  installementFormat: string;
  amount: number;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() { }
}
