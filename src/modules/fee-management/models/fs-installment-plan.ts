import { FormControl, Validators } from '@angular/forms';

export class FS_InstallmentPlanModel {

  /**
   * Set the labels for attributes 
   */
  static attributesLabels = {
    title: 'Title',
    dueDayOfMonth: 'Due Day of Month',
  };

  id?: number;
  feeStructureId: number;
  title: string;
  dueDayOfMonth: number;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() { }


  /**
   * Form Validation Rules 
   */
  public validationRules?() {
    return {
      id: new FormControl(null),
      feeStructureId: new FormControl(null, [Validators.required]),
      title: new FormControl(null, [Validators.required]),
      dueDayOfMonth: new FormControl(null, [Validators.required])
    };
  }
}
