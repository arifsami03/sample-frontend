import { FormControl, Validators, ValidatorFn, AbstractControl, FormArray } from '@angular/forms';
import { ConcessionFeeHeadModel } from '.';

export class ConcessionModel {

  id?: number;
  title?: string;
  MCConcessionTypeId?: number;
  MCConcessionCategoryId?: number;
  concessionFormat?: string;
  concessionAmount?: number;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;
  concessionFeeHeads?: ConcessionFeeHeadModel[];

  static attributesLabels = {
    title: 'Title',
    MCConcessionTypeId: 'Type',
    MCConcessionCategoryId: 'Category',
    concessionFormat: 'Format',
    concessionAmount: 'Amount/Percentage',
  };

  
  constructor() { }

  /**
   * Checking validation of percentage
   * @param equalControlName
   */
  public percentage?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
      [key: string]: any;
    } => {
      let controlMatch = 0;
      return controlMatch <= control.value && 100 >= control.value
        ? null
        : {
            equalTo: true
          };
    };
  }
  /**
   * Form Validation Rules
   * 
   */
  public validationRules?() {

    return {
      title: new FormControl('', [<any>Validators.required]),
      MCConcessionTypeId: new FormControl('', [<any>Validators.required]),
      MCConcessionCategoryId: new FormControl('', [<any>Validators.required]),
      concessionFormat: new FormControl('', [<any>Validators.required]),
      concessionAmount: new FormControl('', [ Validators.minLength(0), Validators.maxLength(100), this.percentage('concessionAmount')]),
      feeHeads : new FormArray([])
    };
  }
}
