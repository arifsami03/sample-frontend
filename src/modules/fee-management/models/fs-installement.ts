import { FormControl, Validators, FormArray } from '@angular/forms';
import { FSILineItemModel } from '.';

export class FSInstallementModel {
  /**
   * Set the labels for attributes 
   */
  static attributesLabels = {
    title: 'Title',
    feeStructure: 'Fee Structure',
    dueDate: 'Due Date',
    expiryDate: 'Expiry Date',
  };

  id?: number;
  title: string;
  dueDate: Date;
  expiryDate: Date;
  lineItems: FSILineItemModel[];
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules 
   */
  public validationRules?() {
    return {
      id: new FormControl(null),
      title: new FormControl('', [Validators.required]),
      dueDate: new FormControl('', [Validators.required]),
      expiryDate: new FormControl('', [Validators.required]),
      lineItems: new FormArray([]),
    };
  }
}
