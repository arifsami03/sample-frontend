export const FeeManagementMenu = [
  {
    heading: 'Fee Management',
    menuItems: [
      { title: 'Management Fees', url: ['/fee-management/managementFees'], perm: 'managementFees.index' },
      { title: 'Fee Heads', url: ['/fee-management/feeHeads'], perm: 'feeHeads.index' },
      { title: 'Concessions', url: ['/fee-management/concessions'], perm: 'concessions.index' },
      { title: 'Scholarships', url: ['/fee-management/scholarships'], perm: 'scholarships.index' }
    ]
  },
  {
    heading: 'Configuration',
    menuItems: [
      { title: 'Nature of Fee Heads', url: ['/fee-management/configurations/natureOfFeeHeads'], perm: 'configurations.index' },
      { title: 'Types of Fee Heads', url: ['/fee-management/configurations/typeOfFeeHeads'], perm: 'configurations.index' },
      { title: 'Concession Categories', url: ['/fee-management/configurations/concessionCategories'], perm: 'configurations.index' },
      { title: 'Concession Types', url: ['/fee-management/configurations/concessionTypes'], perm: 'configurations.index' },
    ]
  },

];
