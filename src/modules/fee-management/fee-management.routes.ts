import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfigurationComponent } from '../shared/components';

import {
  FeeHeadsListComponent, FeeHeadsFormComponent, FeeStructureListComponent, FeeStructureFormComponent, FSCampusListComponent,
  FSCampusFormComponent, ConcessionListComponent, ConcessionFormComponent, ScholarshipListComponent, ScholarshipFormComponent,
  ManagementFeeListComponent, ManagementFeeFormComponent, FeeStructureViewComponent, DashboardComponent
} from './components';
import { GLOBALS } from '../app/config/globals';
import { AuthGuardService } from '../app/services';

/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: '',
    data: { breadcrumb: { title: 'Fee Management', display: true } },
    children: [
      {
        path: '',
        data: { breadcrumb: { title: 'Management Fee', display: false } },
        canActivate: [AuthGuardService],
        component: DashboardComponent,
      },
      {
        path: 'managementFees',
        data: { breadcrumb: { title: 'Management Fee', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: ManagementFeeListComponent,
            data: { breadcrumb: { title: 'Management Fee', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: ManagementFeeFormComponent,
            data: {
              act: GLOBALS.pageActions.create,
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: ManagementFeeFormComponent,
            data: { act: GLOBALS.pageActions.view }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: ManagementFeeFormComponent,
            data: { act: GLOBALS.pageActions.update }
          }
        ]
      },
      {
        path: 'feeHeads',
        data: { breadcrumb: { title: 'Fee Heads', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: FeeHeadsListComponent,
            data: { breadcrumb: { title: 'Fee Heads', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: FeeHeadsFormComponent,
            data: {
              act: GLOBALS.pageActions.create,
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: FeeHeadsFormComponent,
            data: { act: GLOBALS.pageActions.view }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: FeeHeadsFormComponent,
            data: { act: GLOBALS.pageActions.update }
          }
        ]
      },
      {
        path: 'feeStructures',
        data: { breadcrumb: { title: 'Fee Structures', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: FeeStructureListComponent,
            data: { breadcrumb: { title: 'Fee Structures', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: FeeStructureFormComponent,
            data: {
              act: GLOBALS.pageActions.create,
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: FeeStructureViewComponent,
            data: { act: GLOBALS.pageActions.view }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: FeeStructureFormComponent,
            data: { act: GLOBALS.pageActions.update }
          }
        ]
      },
      {
        path: 'fsCampus',
        data: { breadcrumb: { title: 'Mapping Fee Structure', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: FSCampusListComponent,
            data: { breadcrumb: { title: 'Mapping Fee Structure', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: FSCampusFormComponent,
            data: {
              act: GLOBALS.pageActions.create,
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: FSCampusFormComponent,
            data: { act: GLOBALS.pageActions.view }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: FSCampusFormComponent,
            data: { act: GLOBALS.pageActions.update }
          }
        ]
      },
      {
        path: 'concessions',
        data: { breadcrumb: { title: 'Concessions', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: ConcessionListComponent,
            data: { breadcrumb: { title: 'Concessions', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: ConcessionFormComponent,
            data: {
              act: GLOBALS.pageActions.create,
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: ConcessionFormComponent,
            data: { act: GLOBALS.pageActions.view }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: ConcessionFormComponent,
            data: { act: GLOBALS.pageActions.update }
          }
        ]
      },
      {
        path: 'scholarships',
        data: { breadcrumb: { title: 'Scholarship', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: ScholarshipListComponent,
            data: { breadcrumb: { title: 'Scholarship', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: ScholarshipFormComponent,
            data: {
              act: GLOBALS.pageActions.create,
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: ScholarshipFormComponent,
            data: { act: GLOBALS.pageActions.view }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: ScholarshipFormComponent,
            data: { act: GLOBALS.pageActions.update }
          }
        ]
      },
      {
        path: 'configurations',
        data: { breadcrumb: { title: 'Configurations', display: false } },
        children: [
          {
            path: 'natureOfFeeHeads',
            canActivate: [AuthGuardService],
            component: ConfigurationComponent,
            data: {
              key: GLOBALS.configurationKeys.natureOfFeeHeads,
              title: 'Nature of Fee Heads',
              breadcrumb: { title: 'Nature of Fee Heads', display: true }
            }
          },
          {
            path: 'typeOfFeeHeads',
            canActivate: [AuthGuardService],
            component: ConfigurationComponent,
            data: {
              key: GLOBALS.configurationKeys.typeOfFeeHeads,
              title: 'Type of Fee Heads',
              breadcrumb: { title: 'Type of Fee Heads', display: true }
            }
          },
          {
            path: 'concessionCategories',
            canActivate: [AuthGuardService],
            component: ConfigurationComponent,
            data: {
              key: GLOBALS.configurationKeys.concessionCategory,
              title: 'Concession Category',
              breadcrumb: { title: 'Concession Category', display: true }
            }
          },
          {
            path: 'concessionTypes',
            canActivate: [AuthGuardService],
            component: ConfigurationComponent,
            data: {
              key: GLOBALS.configurationKeys.concessionType,
              title: 'Concession Type',
              breadcrumb: { title: 'Concession Type', display: true }
            }
          }
        ]
      },
    ]
  },
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeeManagementRoutes { }
