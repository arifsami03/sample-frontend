import {
  MatInputModule, MatFormFieldModule, MatIconModule, MatDialogModule, MatButtonModule, MatCardModule, MatTableModule,
  MatPaginatorModule, MatOptionModule, MatAutocompleteModule, MatSelectModule, MatDividerModule, MatListModule, MatExpansionModule,
  MatCheckboxModule, MatProgressBarModule, MatRadioModule, MatSortModule
} from '@angular/material';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

/**
 * Import custome services/components etc.
 */
import { FeeHeadsService, FeeStructureService, FSCampusService, ConcessionService, ScholarshipService, ManagementFeeService } from './services';
import { ProgramService, ProgramDetailsService, ClassesService } from '../academic/services';
import { CampusService } from '../campus/services';


import {
  FeeHeadsListComponent, FeeHeadsFormComponent, FeeStructureListComponent, FeeStructureFormComponent, FSCampusListComponent,
  FSCampusFormComponent, ConcessionListComponent, ConcessionFormComponent, ScholarshipListComponent, ScholarshipFormComponent,
  ManagementFeeFormComponent, ManagementFeeListComponent, FeeStructureFeeHeadFormComponent, FeeStructureViewComponent, DashboardComponent
} from './components';

export const __IMPORTS = [
  MatInputModule, MatFormFieldModule, MatIconModule, MatDialogModule, MatButtonModule, MatCardModule, FormsModule, ReactiveFormsModule,
  MatTableModule, MatPaginatorModule, MatAutocompleteModule, MatOptionModule, MatSelectModule, MatDividerModule, MatListModule,
  FlexLayoutModule, MatExpansionModule, MatCheckboxModule, MatProgressBarModule, MatRadioModule, MatSortModule];

export const __DECLARATIONS = [
  FeeHeadsListComponent, FeeHeadsFormComponent, FeeStructureListComponent, FeeStructureFormComponent, FSCampusListComponent,
  FSCampusFormComponent, ConcessionListComponent, ConcessionFormComponent, ScholarshipListComponent, ScholarshipFormComponent,
  ManagementFeeFormComponent, ManagementFeeListComponent, FeeStructureFeeHeadFormComponent, FeeStructureViewComponent, DashboardComponent
];

export const __PROVIDERS = [
  FeeHeadsService, FeeStructureService, FSCampusService, ConcessionService, ScholarshipService, ProgramService,
  ProgramDetailsService, ClassesService, CampusService, ManagementFeeService];

export const __ENTRY_COMPONENTS = [FeeStructureFeeHeadFormComponent];
