import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { PageAnimation } from '../../../../shared/helper';
import { FeeStructureService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { FeeStructureModel } from '../../../models';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { GLOBALS } from '../../../../app/config/globals';
import { FeeStructureFeeHeadFormComponent } from '../..';
import { ProgramModel, ProgramDetailsModel, ClassesModel } from '../../../../academic/models';
import { ProgramService, ProgramDetailsService, ClassesService } from '../../../../academic/services';

@Component({
  selector: 'setting-fee-structure-form',
  templateUrl: './fee-structure-form.component.html',
  styleUrls: ['./fee-structure-form.component.css'],
  animations: [PageAnimation]
})
export class FeeStructureFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public feeHeadsLoaded: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;

  public feeStructure: FeeStructureModel;
  public programs: ProgramModel[];
  public programDetails: ProgramDetailsModel[];
  public classes: ClassesModel[];

  public selectedClasses: number[] = [];

  public deletedClassIds: number[] = [];

  public componentLabels = FeeStructureModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private feeStructureService: FeeStructureService,
    private programService: ProgramService,
    private programDetailsService: ProgramDetailsService,
    private classesService: ClassesService,
    private layoutService: LayoutService,
    public matDialog: MatDialog,

  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  ngOnInit() {
    this.fg = this.fb.group(new FeeStructureModel().validationRules());
    if (this.pageAct === 'create') {
      this.initAddPage();
    } else {
      this.getParams();
    }

    this.getPrograms();

  }


  /**
   * Get all program list
   */
  public getPrograms() {
    this.programService.getProgramsList().subscribe(response => {

      this.programs = response;
    },
      error => {
        console.log(error);
      },
      () => { }
    );
  }

  /**
   * Get all program details of selected program 
   * 
   * @param programId 
   */
  public loadProgramDetails(programId: number) {
    this.getProgramDetails(programId);
    this.fg.controls['programDetailId'].setValue(null);
    this.initializeClassesFormArray();
    this.classes = [];
  }

  /**
   * Get all classes of selected program detail
   * 
   * @param programDetailId 
   */
  public loadClasses(programDetailId: number) {
    this.getClasses(programDetailId);
    this.initializeClassesFormArray();

  }


  /**
   * Reload angualr FormGroup's classes form array i.e remove all previously selected classes
   * 
   */
  private initializeClassesFormArray() {
    this.fg.setControl('classes', this.fb.array([]));
    this.selectedClasses = [];
    this.addNewClassItem();
  }

  /**
   * Get all program details wrt to programId
   * 
   * @param programId 
   */
  private getProgramDetails(programId: number) {
    this.programDetailsService.getProgramProgramDetails(programId).subscribe(response => {

      this.programDetails = response;
    },
      error => {
        console.log(error);
      },
      () => { }
    );
  }

  /**
   * Get all classes wrt to programDetailId
   * 
   * @param programDetailId 
   */
  public getClasses(programDetailId: number) {
    this.classesService.findProgramDetailsClasses(programDetailId).subscribe(response => {

      this.classes = response;
    },
      error => {
        console.log(error);
      },
      () => { }
    );
  }

  /**
   * Get Program list and initialize page for update or view
   */
  private getParams() {
    this.route.params.subscribe(params => {
      this.feeStructureService.find(params['id']).subscribe(
        response => {
          this.feeStructure = response;
        },
        error => {
          this.loaded = true;
        },
        () => {
          this.getProgramDetails(this.feeStructure.programId);
          this.getClasses(this.feeStructure.programDetailId);
          this.initializeClasses();
          this.fg.patchValue(this.feeStructure);
          if (this.pageAct == 'view') {
            this.initViewPage();
          } else if (this.pageAct == 'update') {
            this.initUpdatePage();
          }
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Initialize page for add scenario
   * 
   */
  private initAddPage() {
    this.layoutService.setPageTitle({ title: 'Add Fee Structure' });
    this.feeStructure = new FeeStructureModel();
    this.addNewClassItem();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * Initialize page for update scenario
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Fee Structure: ' + this.feeStructure.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Fee Structures', url: '/fee-management/feeStructures' },
        {
          label: this.feeStructure.title,
          url: `/fee-management/feeStructures/view/${this.feeStructure.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Initialize page for view scenario
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Fee Structure: ' + this.feeStructure.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Fee Structures',
          url: '/fee-management/feeStructures'
        },
        { label: this.feeStructure.title }
      ]
    });
  }

  /**
   * Calls create or update base on loaded page
   * 
   * @param item 
   */
  public saveData(item: FeeStructureModel) {
    this.loaded = false;
    if (this.pageAct === 'create') {
      this.create(item);
    } else {
      this.update(this.feeStructure.id, item);
    }
  }

  /**
   * Create fee structure
   * 
   * @param item 
   */
  private create(item: FeeStructureModel) {

    this.feeStructureService.create(item).subscribe(
      response => {
        this.feeStructure = response;
        this.layoutService.flashMsg({ msg: 'Fee Structure has been created.', msgType: 'success' });
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        console.log(error);
      },
      () => {
        this.loaded = true;
        this.router.navigate([`/fee-management/feeStructures/view/${this.feeStructure.id}`]);
      }
    );
  }

  /**
   * update fee structure
   * 
   * @param id 
   * @param item 
   */
  private update(id: number, item: FeeStructureModel) {
    this.loaded = false;

    item.deletedClassIds = this.deletedClassIds;

    this.feeStructureService.update(id, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Structure Heads has been updated.', msgType: 'success' });
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        console.log(error);
      },
      () => {
        this.loaded = true;
        this.router.navigate([`/fee-management/feeStructures/view/${this.feeStructure.id}`]);
      }
    );
  }

  /**
   * Initialize classes form array for update and add scenario of fee-structure
   */
  private initializeClasses() {
    if (this.feeStructure.classes.length > 0) {
      this.feeStructure.classes.forEach(element => {
        this.addNewClassItem({ id: element.id, classId: element.classId });
      });
    } else {
      this.addNewClassItem();
    }

  }

  /**
   * Prepare classes formarray to add new class 
   * 
   * @param item 
   */
  public addNewClassItem(item?): void {
    let classes = this.fg.get('classes') as FormArray;
    classes.push(this.createNewClassItem(item));
    this.storeSelectedClasses();

  }

  /**
   * Create new class form control in classes formarray
   * 
   * @param item 
   */
  private createNewClassItem(item?): FormGroup {
    let id = null;
    let classId = null;
    if (item) {
      id = item.id;
      classId = item.classId;
    }

    return this.fb.group({
      id: id,
      classId: classId,
    });
  }

  /**
   * Add selected class id to an array this.selectedClasses
   */
  private storeSelectedClasses() {
    this.fg.get('classes')['controls'].forEach(element => {
      if (element.value.classId) {
        if (this.selectedClasses.indexOf(element.value.classId) < 0) {
          this.selectedClasses.push(element.value.classId);
        }
      }
    });
  }

  /**
   * Remove class from classes form array and this.selectedClasses if exists and add classId to deletedClassId if id this record exists
   * @param i 
   * @param item 
   */
  removeClassItem(i: number, item) {
    let classes = <FormArray>this.fg.controls['classes'];
    classes.removeAt(i);
    if (item.controls.classId.value) {
      let index = this.selectedClasses.indexOf(item.controls.classId.value);
      if (index > -1) {
        this.selectedClasses.splice(index, 1);
      }
    }

    if (item.controls.id.value) {
      this.deletedClassIds.push(item.controls.id.value);
    }
    if (classes.controls.length < 1) {
      this.addNewClassItem();
    }
  }

  /**
   * Delete fee structure after get confirmation from user by dialog
   * @param id 
   */
  delete(id: number) {

    // Confirm dialog
    this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.feeStructureService.delete(id).subscribe(response => {
            this.layoutService.flashMsg({ msg: 'Fee Structure has been deleted.', msgType: 'success' });
            this.router.navigate(['/fee-management/feeStructures']);
          },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
              this.loaded = true;
            }
          );
        }
      });
  }
}
