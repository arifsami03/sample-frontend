import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

import { FeeStructureService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { FeeStructureFeeHeadsModel, FeeHeadsModel } from '../../../models';
import { ConfigurationService } from '../../../../shared/services';
import { GLOBALS } from '../../../../app/config/globals';
import { ConfigurationModel } from '../../../../shared/models';

@Component({
  selector: 'fee-structure-fee-head-form',
  templateUrl: './fs-fee-head-form.component.html',
  styleUrls: ['./fs-fee-head-form.component.css']
})
export class FeeStructureFeeHeadFormComponent {
  public loaded: boolean = false;
  private pageAct: string;
  public pageTitle: string;
  private feeStructureId: number;
  private feeHeadsId: number;
  private feeStructureTitle: string;
  private id: number;
  public fg: FormGroup;
  public feeStructureFeeHead: FeeStructureFeeHeadsModel;
  public typeOfFeeHeads: ConfigurationModel[];
  public feeHeads: FeeHeadsModel[];
  public typeOfFeeHeadsKey = GLOBALS.configurationKeys.typeOfFeeHeads;
  public managementFeeFormates = GLOBALS.AmountFormat;
  public selectedManagementFeeFormate = '';

  public componentLabels = FeeStructureFeeHeadsModel.attributesLabels;

  constructor(
    private feeStructureService: FeeStructureService,
    private configurationService: ConfigurationService,
    private layoutService: LayoutService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<FeeStructureFeeHeadFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.loaded = true;
  }

  ngOnInit() {
    this.feeStructureId = this.data.feeStructureId;
    this.feeStructureTitle = this.data.feeStructureTitle;
    this.id = this.data.id;
    this.feeHeadsId = this.data.feeHeadsId;
    this.pageAct = this.data.action
    this.initializePage();
    this.findAllTypeOfFeeHeads();
  }

  private initializePage() {
    this.fg = this.fb.group(new FeeStructureFeeHeadsModel().validationRules());
    this.fg.controls['feeStructureId'].setValue(this.feeStructureId);
    if (this.pageAct === 'create') {
      this.pageTitle = 'Add Fee Head'
      this.findAllRemainingFeeHeads(this.feeStructureId);
      this.initAddPage();
    } else {
      this.pageTitle = `Update Fee Head for`;
      this.findAllRemainingFeeHeads(this.feeStructureId, this.feeHeadsId);
      this.getData(this.id);
    }
  }

  private findAllTypeOfFeeHeads() {
    this.configurationService.findAttributesList(this.typeOfFeeHeadsKey).subscribe(
      response => {
        this.typeOfFeeHeads = response;
      },
      error => {
        console.log(error);
        this.typeOfFeeHeads = [new ConfigurationModel()];
      },
      () => { }
    );
  }

  private findAllRemainingFeeHeads(feeStructureId: number, feeHeadsId?: number) {
    this.feeStructureService.findAllRemainingFeeHeads(feeStructureId, feeHeadsId).subscribe(
      response => {
        this.feeHeads = response;
      },
      error => {
        console.log(error);
        this.feeHeads = [new FeeHeadsModel()];
      }
    )
  }

  private getData(id: number) {
    this.feeStructureService.findFeeHead(id).subscribe(
      response => {
        this.feeStructureFeeHead = response;
      },
      error => {
        this.loaded = true;
      },
      () => {
        this.fg.patchValue(this.feeStructureFeeHead);
        this.initUpdatePage();
        this.loaded = true;
      }
    );
  }

  private initAddPage() {
    this.fg.enable();

    this.loaded = true;
  }

  private initUpdatePage() {
    this.fg.controls['feeStructureId'].setValue(this.feeStructureId);
    this.fg.enable();

  }

  public saveData(item: FeeStructureFeeHeadsModel) {
    this.loaded = false;
    if (this.pageAct === 'create') {
      this.create(item);
    } else {
      this.update(this.id, item);
    }
  }

  private create(item: FeeStructureFeeHeadsModel) {
    this.feeStructureService.createFeeHead(item).subscribe(
      response => {
        this.feeStructureFeeHead = response;
        this.layoutService.flashMsg({ msg: 'Fee Head has been added to Fee Structure.', msgType: 'success' });
        this.dialogRef.close(response);
      },
      error => {
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        this.dialogRef.close(false);

        console.log(error);
      },
      () => { }
    );
  }

  private update(id: number, item: FeeStructureFeeHeadsModel) {

    this.feeStructureService.updateFeeHead(id, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Fee Head has been updated.', msgType: 'success' });
        this.dialogRef.close(true);
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        this.dialogRef.close(false);

        console.log(error);
      },
      () => {
        this.loaded = true;

      }
    );
  }

  /**
   * Change form input label on the base selected concession format.
   *
   * @param managementFeeFormate string
   */
  public mfFormatChange(managementFeeFormate: string) {
    this.selectedManagementFeeFormate = managementFeeFormate;

  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }


}
