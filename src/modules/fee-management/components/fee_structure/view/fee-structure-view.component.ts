import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { PageAnimation } from '../../../../shared/helper';
import { FeeStructureService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { FeeStructureModel } from '../../../models';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { GLOBALS } from '../../../../app/config/globals';
import { FeeStructureFeeHeadFormComponent } from '../fs_fee_head_form/fs-fee-head-form.component';
import { ProgramModel, ProgramDetailsModel, ClassesModel } from '../../../../academic/models';
import { ProgramService, ProgramDetailsService, ClassesService } from '../../../../academic/services';

@Component({
  selector: 'setting-fee-structure-view',
  templateUrl: './fee-structure-view.component.html',
  styleUrls: ['./fee-structure-view.component.css'],
  animations: [PageAnimation]
})
export class FeeStructureViewComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public feeHeadsLoaded: boolean = false;

  public fg: FormGroup;
  public managementFeeFormates = GLOBALS.AmountFormat;
  public pageAct: string;
  public pageTitle: string;

  public feeStructure: FeeStructureModel;
  public programs: ProgramModel[];
  public programDetails: ProgramDetailsModel[];
  public classes: ClassesModel[];

  public componentLabels = FeeStructureModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private feeStructureService: FeeStructureService,
    private programService: ProgramService,
    private programDetailsService: ProgramDetailsService,
    private classesService: ClassesService,
    private layoutService: LayoutService,
    public matDialog: MatDialog,

  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  ngOnInit() {
    this.fg = this.fb.group(new FeeStructureModel().validationRules());
    this.getParams();

    this.getPrograms();

  }


  /**
   * Get all program list
   */
  public getPrograms() {
    this.programService.getProgramsList().subscribe(response => {

      this.programs = response;
    },
      error => {
        console.log(error);
      },
      () => { }
    );
  }

  /**
   * Get all program details wrt to programId
   * 
   * @param programId 
   */
  private getProgramDetails(programId: number) {
    this.programDetailsService.getProgramProgramDetails(programId).subscribe(response => {

      this.programDetails = response;
    },
      error => {
        console.log(error);
      },
      () => { }
    );
  }

  /**
   * Get all classes wrt to programDetailId
   * 
   * @param programDetailId 
   */
  public getClasses(programDetailId: number) {
    this.classesService.findProgramDetailsClasses(programDetailId).subscribe(response => {

      this.classes = response;
    },
      error => {
        console.log(error);
      },
      () => { }
    );
  }

  /**
   * Get Program list and initialize page for update or view
   */
  private getParams() {
    this.route.params.subscribe(params => {
      this.feeStructureService.find(params['id']).subscribe(
        response => {
          this.feeStructure = response;
        },
        error => {
          this.loaded = true;
        },
        () => {
          this.getProgramDetails(this.feeStructure.programId);
          this.getClasses(this.feeStructure.programDetailId);
          this.initializeClasses();
          this.fg.patchValue(this.feeStructure);
          this.initViewPage();
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Initialize page for view scenario
   */
  private initViewPage() {
    this.findAllFeeHeads(this.feeStructure.id);
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Fee Structure: ' + this.feeStructure.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Fee Structures',
          url: '/fee-management/feeStructures'
        },
        { label: this.feeStructure.title }
      ]
    });
  }


  /**
   * Initialize classes form array for update and add scenario of fee-structure
   */
  private initializeClasses() {
    if (this.feeStructure.classes.length > 0) {
      this.feeStructure.classes.forEach(element => {
        this.addNewClassItem({ id: element.id, classId: element.classId });
      });
    } else {
      this.addNewClassItem();
    }

  }

  /**
   * Prepare classes formarray to add new class 
   * 
   * @param item 
   */
  public addNewClassItem(item?): void {
    let classes = this.fg.get('classes') as FormArray;
    classes.push(this.createNewClassItem(item));

  }

  /**
   * Create new class form control in classes formarray
   * 
   * @param item 
   */
  private createNewClassItem(item?): FormGroup {
    let id = null;
    let classId = null;
    if (item) {
      id = item.id;
      classId = item.classId;
    }

    return this.fb.group({
      id: id,
      classId: classId,
    });
  }

  /**
   * Delete fee structure after get confirmation from user by dialog
   * @param id 
   */
  delete(id: number) {

    // Confirm dialog
    this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.feeStructureService.delete(id).subscribe(response => {
            this.layoutService.flashMsg({ msg: 'Fee Structure has been deleted.', msgType: 'success' });
            this.router.navigate(['/fee-management/feeStructures']);
          },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
              this.loaded = true;
            }
          );
        }
      });
  }


  /**
   * **************************************************************************************************************************************
   * This section is for fee head and must be written in individual component
   */

  /**
   * Find all fee head of a fee structure base on feeStructureId
   * 
   * @param feeStructureId
   */
  private findAllFeeHeads(feeStructureId: number) {
    this.feeStructureService.findAllFeeHeads(feeStructureId).subscribe(
      response => {
        this.feeStructure.feeHeads = response;
      },
      error => {
        console.log(error);
      },
      () => {
        this.feeHeadsLoaded = true;
      }
    );
  }


  /**
   * Show dialog to add fee head.
   * 
   * @param id number
   */
  addFeeHead(id: number) {
    const dialogRef = this.matDialog.open(FeeStructureFeeHeadFormComponent, {
      width: '500px',
      data: { feeStructureId: id, feeStructureTitle: this.feeStructure.title, action: GLOBALS.pageActions.create }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.findAllFeeHeads(this.feeStructure.id);
      }
    });
  }

  /**
   * Show dialog to edit fee head
   * 
   * @param id 
   * @param feeHeadsId 
   */
  editFeeHead(id: number, feeHeadsId: number) {

    const dialogRef = this.matDialog.open(FeeStructureFeeHeadFormComponent, {
      width: '500px',
      data: { id: id, feeHeadsId: feeHeadsId, feeStructureId: this.feeStructure.id, feeStructureTitle: this.feeStructure.title, action: GLOBALS.pageActions.update }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.findAllFeeHeads(this.feeStructure.id);
      }
    });
  }

  /**
   * Get confirmation by dialog and then deletes record
   * @param id 
   */
  deleteFeeHead(id: number) {

    // Confirm dialog
    this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {

        this.feeStructureService.deleteFeeHead(id).subscribe(response => {
          this.layoutService.flashMsg({ msg: 'Fee Head has been removed from Fee Structure.', msgType: 'success' });
          this.findAllFeeHeads(this.feeStructure.id);
        },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
            this.loaded = true;
          }
        );
      }
    });
  }
}
