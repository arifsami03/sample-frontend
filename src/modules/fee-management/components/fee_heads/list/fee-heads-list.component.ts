import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';

import { PageAnimation } from 'modules/shared/helper';
import { GLOBALS } from 'modules/app/config/globals';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { LayoutService } from 'modules/layout/services';
import { FeeHeadsService } from 'modules/fee-management/services';
import { FeeHeadsModel } from 'modules/fee-management/models';

@Component({
  selector: 'settings-fee-heads-list',
  templateUrl: './fee-heads-list.component.html',
  styleUrls: ['./fee-heads-list.component.css'],
  animations: [PageAnimation]
})
export class FeeHeadsListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public data: FeeHeadsModel[] = [new FeeHeadsModel()];

  public attrLabels = FeeHeadsModel.attributesLabels;

  displayedColumns = ['title', 'abbreviation', 'code', 'options'];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public error: Boolean;

  constructor(
    private feeHeadsService: FeeHeadsService,
    public matDialog: MatDialog,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Fee Heads' });

    this.getRecords();
  }

  /**
   * Get data/records from backend
   */
  getRecords() {

    this.feeHeadsService.index().subscribe(response => {

      this.data = response;
      this.dataSource = new MatTableDataSource<FeeHeadsModel>(this.data);
      this.dataSource.paginator = this.paginator;

    },
      error => {
        console.log(error);
        this.pageState = 'active';
        this.loaded = true;
      },
      () => {
        this.pageState = 'active';
        this.loaded = true;

      }
    );
  }

  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {

    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;

  }

  /**
   * Delete record
   */
  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.feeHeadsService.delete(id).subscribe(
          response => {

            if (response && (response['children']) ? response['children'].length > 0 : false) {

              this.loaded = true;
              this.pageState = 'active';

              this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

            } else {

              this.layoutService.flashMsg({ msg: 'Fee Heads has been deleted.', msgType: 'success' });
              this.getRecords();

            }

          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
            this.loaded = true;
            this.pageState = 'active';
          }
        );
      }
    });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }

}
