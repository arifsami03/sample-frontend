import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { PageAnimation } from 'modules/shared/helper';
import { FeeHeadsService } from 'modules/fee-management/services';
import { ConfigurationService } from 'modules/shared/services';
import { LayoutService } from 'modules/layout/services';
import { FeeHeadsModel } from 'modules/fee-management/models';
import { ConfigurationModel } from 'modules/shared/models';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'setting-fee-heads-form',
  templateUrl: './fee-heads-form.component.html',
  styleUrls: ['./fee-heads-form.component.css'],
  animations: [PageAnimation]
})
export class FeeHeadsFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;

  public natureOfFeeHeads: ConfigurationModel[];

  public feeHeads: FeeHeadsModel;

  private natureOfFeeHeadsKey = GLOBALS.configurationKeys.natureOfFeeHeads;

  public componentLabels = FeeHeadsModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private feeHeadsService: FeeHeadsService,
    private configurationService: ConfigurationService,
    private layoutService: LayoutService,
    public matDialog: MatDialog,

  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  ngOnInit() {
    this.initializePage();

    this.configurationService.findAttributesList(this.natureOfFeeHeadsKey).subscribe(
      response => {
        this.natureOfFeeHeads = response;
      },
      error => {
        this.natureOfFeeHeads = [new ConfigurationModel()];
      }
    );
  }

  private initializePage() {
    this.fg = this.fb.group(new FeeHeadsModel().validationRules());
    if (this.pageAct === 'create') {
      this.initAddPage();
    } else {
      this.getData();
    }
  }

  private getData() {
    this.route.params.subscribe(params => {
      this.feeHeadsService.find(params['id']).subscribe(
        response => {
          this.feeHeads = response;
        },
        error => {
          this.loaded = true;
        },
        () => {
          this.fg.patchValue(this.feeHeads);
          if (this.pageAct == 'view') {
            this.initViewPage();
          } else if (this.pageAct == 'update') {
            this.initUpdatePage();
          }
          this.loaded = true;
        }
      );
    });
  }

  private initAddPage() {
    this.layoutService.setPageTitle({ title: 'Add Fee Heads' });
    this.feeHeads = new FeeHeadsModel();
    this.fg.enable();

    this.loaded = true;
  }

  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Fee Heads: ' + this.feeHeads.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Fee Heads',
          url: '/fee-management/feeHeads'
        },
        { label: this.feeHeads.title }
      ]
    });
  }

  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Fee Heads: ' + this.feeHeads.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Fee Heads', url: '/fee-management/feeHeads' },
        {
          label: this.feeHeads.title,
          url: `/fee-management/feeHeads/view/${this.feeHeads.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  public saveData(item: FeeHeadsModel) {

    item.abbreviation = item.abbreviation.toUpperCase();

    if (this.pageAct === 'create') {
      this.create(item);
    } else {
      this.update(this.feeHeads.id, item);
    }
  }

  private create(item: FeeHeadsModel) {

    this.feeHeadsService.create(item).subscribe(
      response => {
        this.feeHeads = response;
        this.layoutService.flashMsg({ msg: 'Fee Heads has been created.', msgType: 'success' });
      },
      error => {
        if (error.status == 400) {
          this.fg.controls['code'].setErrors({ duplicateCode: true });
        } else {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }

      },
      () => {
        this.loaded = true;
        this.router.navigate([`/fee-management/feeHeads`]);
      }
    );
  }

  private update(id: number, item: FeeHeadsModel) {
    this.loaded = false;

    this.feeHeadsService.update(id, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Fee Heads has been updated.', msgType: 'success' });
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        console.log(error);
      },
      () => {
        this.loaded = true;
        this.router.navigate([`/fee-management/feeHeads`]);
      }
    );
  }

  delete(id: number) {

    const dialogRef = this.matDialog.open(ConfirmDialogComponent,
      { width: GLOBALS.deleteDialog.width, data: { message: GLOBALS.deleteDialog.message } }
    );

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.feeHeadsService.delete(id).subscribe(response => {

          if (response && (response['children']) ? response['children'].length > 0 : false) {

            this.loaded = true;
            this.pageState = 'active';

            this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

          } else {

            this.layoutService.flashMsg({ msg: 'Fee Heads has been deleted.', msgType: 'success' });
            this.router.navigate(['/fee-management/feeHeads']);

          }

        },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
            this.loaded = true;
            this.pageState = 'active';
          }
        );
      }
    });
  }

}
