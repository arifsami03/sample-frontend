import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';
import * as _ from 'lodash';

import { GLOBALS } from '../../../../app/config/globals';
import { PageAnimation } from '../../../../shared/helper';
import { LayoutService } from '../../../../layout/services';
import { ConcessionService, FeeHeadsService } from '../../../services';
import { ConfigurationService } from '../../../../shared/services';
import { ConcessionModel, FeeHeadsModel, ConcessionFeeHeadModel } from '../../../models';
import { ConfigurationModel } from '../../../../shared/models';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';


@Component({
  selector: 'setting-concession-form',
  templateUrl: './concession-form.component.html',
  styleUrls: ['./concession-form.component.css'],
  animations: [PageAnimation]
})
export class ConcessionFormComponent implements OnInit {
  public pageState = 'active';
  public loaded: boolean = false;

  public feeHeadListloaded: boolean = false;

  public pageAct: string;
  public pageActions = GLOBALS.pageActions;

  public fg: FormGroup;
  public concessionModel: ConcessionModel;
  public attributeLabels = ConcessionModel.attributesLabels;

  public MCConcessionTypes: ConfigurationModel[];
  public MCConcessionCategories: ConfigurationModel[];
  public feeHeads: FeeHeadsModel[];
  public concessionFeeHeads: ConcessionFeeHeadModel[] = [];

  public concessionFormates = GLOBALS.AmountFormat;
  public selectedFormat = GLOBALS.AmountFormat.percentage;
  public concessionAmount;
  public totalAmount: number = 0;

  /**
   * constructor
   *
   * @param router Router
   * @param route ActivatedRoute
   * @param fb FormBuilder
   * @param concessionService ConcessionService
   * @param confService ConfigurationService
   * @param feeHeadService FeeHeadsService
   * @param layoutService LayoutService
   */
  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private fb: FormBuilder,
    private concessionService: ConcessionService,
    private confService: ConfigurationService,
    private feeHeadService: FeeHeadsService,
    private layoutService: LayoutService,
    public matDialog: MatDialog
  ) {
    // Get page action from data so render view accordingly (create or update form)
    this.pageAct = actRoute.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  /**
   * Initialize page for create or update form.
   *
   */
  private initializePage() {
    this.fg = this.fb.group(new ConcessionModel().validationRules());

    // Find list of ConcessionCategories saved in configuration table
    this.findMetaConf(GLOBALS.configurationKeys.concessionCategory)
      .then(result => {
        this.MCConcessionCategories = <ConfigurationModel[]>result;
      })
      .catch(error => {
        this.error(error);
      });

    // Find list of ConcessionTypes saved in configuration table
    this.findMetaConf(GLOBALS.configurationKeys.concessionType)
      .then(result => {
        this.MCConcessionTypes = <ConfigurationModel[]>result;
      })
      .catch(error => {
        this.error(error);
      });

    if (this.pageAct === this.pageActions.create) {
      // Find list of fee heads.
      this.findFeeHeads();

      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * This method will call only if its called for update or view screen
   *
   */
  private getData() {
    this.actRoute.params.subscribe(params => {
      this.concessionService.findWithFeeHeads(params['id']).subscribe(
        response => {
          this.concessionModel = response;

          this.fg.patchValue(this.concessionModel);

          if (this.pageAct === this.pageActions.update) {
            this.selectedFormat = this.concessionModel.concessionFormat;
            this.concessionFormatChange(this.selectedFormat);

            this.initUpdatePage();

          } else if (this.pageAct === this.pageActions.view) {
            this.initViewPage();

          }

          // Find list of fee heads.
          this.findFeeHeads();

          this.loaded = true;

        },
        error => {
          this.error(error);
        }
      );
    });
  }

  /**
   * Initialize create page
   */
  private initCreatePage() {
    // Default concession format is percentage.
    this.concessionFormatChange(this.selectedFormat);
    this.fg.patchValue({ concessionFormat: this.concessionFormates.percentage });

    this.layoutService.setPageTitle({ title: 'Concession: Create' });
    this.concessionModel = new ConcessionModel();
    this.fg.enable();
    this.loaded = true;
  }

  /**
   * Initialize update page
   *
   */
  private initUpdatePage() {
    this.fg.enable();

    //TODO: setting is missing from breadcrumbs
    this.layoutService.setPageTitle({
      title: 'Concession: ' + this.concessionModel.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Concessions', url: '/fee-management/concessions' },
        { label: this.concessionModel.title, url: `/fee-management/concessions/view/${this.concessionModel.id}` },
        { label: 'Update' }
      ]
    });
  }

  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Concessions: ' + this.concessionModel.title,
      breadCrumbs: [{ label: 'Home', url: '/home/dashboard' }, { label: 'Concessions ', url: '/fee-management/concessions' }, { label: this.concessionModel.title }]
    });
  }

  /**
   * Save record.
   *
   * @param item ConcessionModel
   */
  public saveData(item: ConcessionModel) {
    this.findfeeHeadsConcession();
    item['concessionFeeHeads'] = this.concessionFeeHeads;

    if (this.pageAct === this.pageActions.create) {
      //Save for create new

      this.concessionService.createWithFeeHeads(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Concession has been created.', msgType: 'success' });
          this.router.navigate([`/fee-management/concessions`]);
        },
        error => {
          this.error(error);
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      //Save for update

      this.concessionService.updateWithFeeHeads(this.concessionModel.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Concession has been updated.', msgType: 'success' });

          this.router.navigate([`/fee-management/concessions`]);
        },
        error => {
          this.error(error);
        }
      );
    }
  }

  /**
   * Find list of fee heads.
   *
   */
  private findFeeHeads() {
    this.feeHeadService.findAttributesList().subscribe(result => {
      this.feeHeads = result;
      this.feeHeadListloaded = true;
      this.feeHeads.forEach(element => {
        this.addFeeHeads(element.id);
      });
      if (this.pageAct === this.pageActions.update || this.pageAct === this.pageActions.view) {
        this.concessionFeeHeads = this.concessionModel.concessionFeeHeads;
        this.amountChange(this.concessionModel.concessionAmount);
      }

      this.patchingConcessionAmount();
      // Counting total amount
      this.countTotalAmount();
    });
  }

  /**
   * Find list of records from configuration
   *
   * @param configurationKey string
   */
  private findMetaConf(configurationKey: string) {
    return new Promise((resolve, reject) => {
      this.confService.findAttributesList(configurationKey).subscribe(
        response => {
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  }

  /**
   * Change form input on the base selected concession format.
   *
   * @param concessionFormat string
   */
  public concessionFormatChange(concessionFormat: string) {
    this.selectedFormat = concessionFormat;

    if (concessionFormat == this.concessionFormates.percentage) {
      this.attributeLabels.concessionAmount = 'Percentage';
      // checking the error
      if (this.fg.controls["concessionAmount"].value < 0 || this.fg.controls["concessionAmount"].value > 100) {
        this.fg.controls["concessionAmount"].setErrors({ percentage: true });

      }
    } else if (concessionFormat == this.concessionFormates.amount) {
      this.attributeLabels.concessionAmount = 'Amount';
      this.fg.controls["concessionAmount"].setErrors(null);

    }
  }

  /**
   * To Update concession amount in html view for list of heads which are checked
   *
   * @param evt ChangeEvent
   */
  amountChange(amount) {
    this.concessionAmount = amount;
    // Update concession amount in html view for list of heads which are checked.
    this.feeHeads.forEach(item => {
      this.concessionFeeHeads.forEach(cfhItem => {
        if (item['id'] == cfhItem['feeHeadId']) {
          item['concession'] = this.concessionAmount;
          item['status'] = true;
        }
      });
    });
  }

  /**
   * When checkbox is checked we need to store FeeHead id
   * to send it to server for saving/updating on save button click.
   *
   * @param event Event
   * @param item object
   */
  checkboxClicked(event: any, item) {
    let obj = { feeHeadId: item['id'] };

    if (event.checked) {
      item['concession'] = this.concessionAmount;
      item['status'] = true;
      this.concessionFeeHeads.push(obj);
    } else {
      const index = this.concessionFeeHeads.findIndex(x => x.feeHeadId == item['id']);

      if (index > -1) {
        item['concession'] = null;
        item['status'] = false;

        this.concessionFeeHeads.splice(index, 1);
      }
    }
  }

  private error(err) {
    console.log(err);
    this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
    this.loaded = true;
  }

  /*
  * Delete record withj associated fee heads
  *
  */
  deleteWithFeeHeads(id: number) {

    const dialogRef = this.matDialog.open(ConfirmDialogComponent,
      { width: GLOBALS.deleteDialog.width, data: { message: GLOBALS.deleteDialog.message } }
    );

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.concessionService.deleteWithFeeHeads(id).subscribe(response => {

          if (response && (response['children']) ? response['children'].length > 0 : false) {

            this.loaded = true;
            this.pageState = 'active';

            this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

          } else {

            this.layoutService.flashMsg({ msg: 'Concession has been deleted.', msgType: 'success' });
            this.router.navigate(['/fee-management/concessions']);

          }

        },
          error => {
            this.loaded = true;
            this.pageState = 'active';
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
          }
        );
      }
    });
  }


  /**
   * Delete single concession fee head
   */
  deleteConcessionFeeHead(concessionId: number, feeHeadId: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.concessionService.deleteConcessionFeeHead(concessionId, feeHeadId).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Concession Fee Head has been deleted.', msgType: 'success' });
              // getting updated record
              this.getData();
            },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
            }
          );
        }
      });
  }


  createFeeHeadItem(_feeHeadId: number): FormGroup {
    return this.fb.group({
      feeHeadId: _feeHeadId,
      concession: ['']
    });
  }
  addFeeHeads(_feeHeadId: number) {
    let feeHeads = this.fg.get('feeHeads') as FormArray;
    feeHeads.push(this.createFeeHeadItem(_feeHeadId));

  }

  /**
   * This function will find fee heads concession amount against fee Head Id 
   */
  findfeeHeadsConcession() {
    if (this.fg.controls['concessionFormat'].value === this.concessionFormates.amount) {
      let feeHeads: any = this.fg.controls['feeHeads'].value;
      this.concessionFeeHeads.forEach(concessionFeeHead => {
        feeHeads.forEach(feeHead => {
          if (concessionFeeHead.feeHeadId === feeHead.feeHeadId) {
            concessionFeeHead['concession'] = feeHead['concession']
          }
        });
      });
    } else {
      this.concessionFeeHeads.forEach(concessionFeeHead => {
        concessionFeeHead['concession'] = this.concessionAmount;
      });
    }

  }

  // Counting the total amount to add
  countTotalAmount() {
    if (this.fg.controls['concessionFormat'].value === this.concessionFormates.amount) {
      this.totalAmount = 0;
      let feeHeads: any = this.fg.controls['feeHeads'].value;

      this.concessionFeeHeads.forEach(concessionFeeHead => {
        feeHeads.forEach(feeHead => {
          if (concessionFeeHead.feeHeadId === feeHead.feeHeadId) {
            this.totalAmount = this.totalAmount + (+feeHead['concession'])
          }
        });
      });

    }
  }

  patchingConcessionAmount() {
    if (this.fg.controls['concessionFormat'].value === this.concessionFormates.amount) {
      this.totalAmount = 0;
      let feeHeads: any = this.fg.controls['feeHeads']['controls'];

      this.concessionFeeHeads.forEach(concessionFeeHead => {
        feeHeads.forEach(feeHead => {
          if (concessionFeeHead.feeHeadId === feeHead.value.feeHeadId) {
            feeHead.patchValue({ id: feeHead.value.feeHeadId, concession: concessionFeeHead['concessionAmount'] });
          }
        });
      });

    } else if (this.fg.controls['concessionFormat'].value === this.concessionFormates.percentage) {
      this.totalAmount = 0;
      let feeHeads: any = this.fg.controls['feeHeads']['controls'];

      this.concessionFeeHeads.forEach(concessionFeeHead => {
        feeHeads.forEach(feeHead => {
          if (concessionFeeHead.feeHeadId === feeHead.value.feeHeadId) {
            this.totalAmount = concessionFeeHead['concessionAmount'];
            feeHead.patchValue({ id: feeHead.value.feeHeadId, concession: concessionFeeHead['concessionAmount'] });
          }
        });
      });
      this.fg.get('concessionAmount').patchValue(this.totalAmount);
    }
  }
}
