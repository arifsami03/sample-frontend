import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router } from '@angular/router';

import { GLOBALS } from 'modules/app/config/globals';
import { PageAnimation } from 'modules/shared/helper';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { LayoutService } from 'modules/layout/services';
import { ConcessionService } from 'modules/fee-management/services';
import { ConcessionModel } from 'modules/fee-management/models';

@Component({
  selector: 'setting-concession-list',
  templateUrl: './concession-list.component.html',
  styleUrls: ['./concession-list.component.css'],
  animations: [PageAnimation]
})
export class ConcessionListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public attributeLabels = ConcessionModel.attributesLabels;

  public data: ConcessionModel[] = [new ConcessionModel()];
  public displayedColumns = ['title', 'options'];
  public dataSource: any;
  public length: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  /**
   * Constructor
   * 
   * @param concessionService ConcessionService
   * @param layoutService LayoutService
   * @param matDialog MatDialog
   */
  constructor(
    private concessionService: ConcessionService,
    public layoutService: LayoutService,
    public matDialog: MatDialog
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {

    this.layoutService.setPageTitle({ title: 'Concessions' });

    this.getRecords();
  }


  /**
   * Get records
   */
  private getRecords() {

    this.concessionService.index().subscribe(response => {

      this.data = response;
      this.dataSource = new MatTableDataSource<ConcessionModel>(this.data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.length = 20;

      this.loaded = true;
      this.pageState = 'active';

    }, error => {

      console.log(error);
      this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
      this.loaded = true;
      this.pageState = 'active';

    });

  }

  /**
   * Apply filter and serch in data grid
   * 
   * @param filterValue string
   */
  public applyFilter(filterValue: string) {

    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;

  }

  /**
   * Delete record after confirmation.
   * 
   * @param id number
   */
  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.concessionService.deleteWithFeeHeads(id).subscribe(
          response => {

            if (response && (response['children']) ? response['children'].length > 0 : false) {

              this.loaded = true;
              this.pageState = 'active';

              this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

            } else {

              this.layoutService.flashMsg({ msg: 'Concession has been deleted.', msgType: 'success' });
              this.getRecords();

            }

          }, error => {

            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
            this.pageState = 'active';
          });
      }
    });
  }

  sortData() {
    this.dataSource.sort = this.sort;
  }

}