import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { GLOBALS } from '../../../../app/config/globals';
import { CampusModel } from '../../../../campus/models';
import { CampusService } from '../../../../campus/services';
import { LayoutService } from '../../../../layout/services';
import { FSCFeeHeadModel, FSCampusModel, FeeHeadsModel, FeeStructureModel } from '../../../models';
import { ConfigurationModel } from '../../../../shared/models';
import { FSCampusService, FeeStructureService } from '../../../services';
import { ConfigurationService } from '../../../../shared/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { PageAnimation } from '../../../../shared/helper';


@Component({
  selector: 'setting-fs-campus-form',
  templateUrl: './fs-campus-form.component.html',
  styleUrls: ['./fs-campus-form.component.css'],
  animations: [PageAnimation]
})
export class FSCampusFormComponent implements OnInit {

  public pageState = 'active';
  public loaded: boolean = false;
  public pageAct: string;
  public pageTitle: string;

  public fg: FormGroup;
  public typeOfFeeHeads: ConfigurationModel[];
  public feeStructures: FeeStructureModel[];
  public feeHeads: FeeHeadsModel[];
  public fsCampus: FSCampusModel;
  public campuses: CampusModel[];
  private selectedCampuses: number[] = [];
  private deletedCampusIds: number[] = [];
  private typeOfFeeHeadsKey = GLOBALS.configurationKeys.typeOfFeeHeads;
  public componentLabels = FSCampusModel.attributesLabels;
  public fscFeeHeadLabels = FSCFeeHeadModel.attributesLabels;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private fsCampusService: FSCampusService,
    private feeStructureService: FeeStructureService,
    private configurationService: ConfigurationService,
    private campusService: CampusService,
    private layoutService: LayoutService,
    public matDialog: MatDialog,

  ) {
    this.pageAct = activatedRoute.snapshot.data['act'];
  }

  ngOnInit() {
    this.initializePage();



    this.feeStructureService.findAttributesList().subscribe(
      response => {
        this.feeStructures = response;
      },
      error => {
        this.feeStructures = [new FeeStructureModel()];
      }
    );

    this.configurationService.findAttributesList(this.typeOfFeeHeadsKey).subscribe(
      response => {
        this.typeOfFeeHeads = response;
      },
      error => {
        this.typeOfFeeHeads = [new ConfigurationModel()];
      }
    );

    this.campusService.findAttributesList().subscribe(
      response => {
        this.campuses = response;
      },
      error => {
        this.campuses = [new CampusModel()];
      }
    );
  }

  loadFeeHeads(feeStructureId: number) {
    // this.feeStructureService.findAlllFeeHeads(feeStructureId).subscribe(
    //   response => {
    //     this.feeHeads = response;
    //   },
    //   error => {
    //     this.feeHeads = [new FeeHeadsModel()];
    //   }, () => {
    //     this.loadFSCFeadHeads();
    //   }
    // );
  }

  private loadFSCFeadHeads() {
    this.fg.setControl('fscFeeHeads', this.fb.array([]));
    let fscFeeHeads = this.fg.get('fscFeeHeads') as FormArray;

    this.feeHeads.forEach(element => {
      fscFeeHeads.push(this.createNewFSCFeeHeadItem(element));

    });

  }

  private createNewFSCFeeHeadItem(item): FormGroup {

    return this.fb.group({
      id: null,
      feeHeadId: item.id,
      feeHeadTitle: item.title,
      typeOfFeeHeadId: null,
      amount: null,
      managementFeeType: '',
      managementFeeAmount: null
    });
  }

  private createNewFSCFeeHeadItemForUpdate(item): FormGroup {

    return this.fb.group({
      id: item.id,
      feeHeadId: item.feeHeadId,
      feeHeadTitle: item.feeHead.title,
      typeOfFeeHeadId: item.typeOfFeeHeadId,
      amount: item.amount,
      managementFeeType: item.managementFeeType,
      managementFeeAmount: item.managementFeeAmount
    });
  }

  private initializePage() {
    this.fg = this.fb.group(new FSCampusModel().validationRules());
    if (this.pageAct === 'create') {
      this.initAddPage();
    } else {
      this.getData();
    }
  }

  private getData() {
    this.activatedRoute.params.subscribe(params => {
      this.fsCampusService.find(params['id']).subscribe(
        response => {
          this.fsCampus = response;
        },
        error => {
          this.loaded = true;
        },
        () => {
          let fscFeeHeads = this.fg.get('fscFeeHeads') as FormArray;

          this.fsCampus.fscFeeHeads.forEach(element => {
            fscFeeHeads.push(this.createNewFSCFeeHeadItemForUpdate(element));

          });
          this.fg.patchValue(this.fsCampus);
          if (this.pageAct == 'view') {
            this.initViewPage();
          } else if (this.pageAct == 'update') {
            this.initUpdatePage();
          }
          this.loaded = true;
          this.loaded = true;
        }
      );
    });
  }

  private initAddPage() {
    this.layoutService.setPageTitle({ title: 'Add Mapping Fee Structure' });
    this.fsCampus = new FSCampusModel();
    this.fg.enable();

    this.loaded = true;
  }

  private initUpdatePage() {
    this.fg.enable();
    this.fg.controls['feeStructureId'].disable();

    this.layoutService.setPageTitle({
      title: 'Update Mapping Fee Structure: ',
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Mapping Fee Structure', url: '/fee-management/fsCampus' },
        { label: 'Update Mapping Fee Structure' }
      ]
    });
  }

  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Campus Fee Structure ',
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Mapping Fee Structure', url: '/fee-management/fsCampus' },
      ]
    });
  }

  public saveData(item: FSCampusModel) {
    
    if (this.pageAct === 'create') {
      this.create(item);
    } else {
      this.update(this.fsCampus.id, item);
    }
  }

  private create(item: FSCampusModel) {

    this.fsCampusService.create(item).subscribe(
      response => {
        this.fsCampus = response;
        this.layoutService.flashMsg({ msg: 'Mapping Fee Structure has been created.', msgType: 'success' });
      },
      error => {
        // if (error.status == 400) {
        //   this.fg.controls['code'].setErrors({ duplicateCode: true });
        // } else {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        // }

      },
      () => {
        this.loaded = true;
        this.router.navigate([`/fee-management/fsCampus/view/${this.fsCampus.id}`]);
      }
    );
  }

  private update(id: number, item: FSCampusModel) {
    this.loaded = false;

    this.fsCampusService.update(id, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Mapping Fee Structure has been updated.', msgType: 'success' });
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        console.log(error);
      },
      () => {
        this.loaded = true;
        this.router.navigate([`/fee-management/fsCampus/view/${this.fsCampus.id}`]);
      }
    );
  }

  delete(id: number) {

    // Confirm dialog
    this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.fsCampusService.delete(id).subscribe(response => {
            this.layoutService.flashMsg({ msg: 'Mapping Fee Structure has been deleted.', msgType: 'success' });
            this.router.navigate(['/fee-management/fsCampus']);
          },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
              this.loaded = true;
            }
          );
        }
      });
  }
}


// public addNewCampusItem(item?): void {
  //   let campuses = this.fg.get('campuses') as FormArray;
  //   campuses.push(this.createNewCampusItem(item));
  //   this.storeSelectedCampuses();

  // }

  // private createNewCampusItem(item?): FormGroup {
  //   let id = null;
  //   let campusId = null;
  //   if (item) {
  //     id = item.id;
  //     campusId = item.campusId;
  //   }

  //   return this.fb.group({
  //     id: id,
  //     campusId: campusId,
  //   });
  // }

  // private storeSelectedCampuses() {
  //   this.fg.get('campuses')['controls'].forEach(element => {
  //     if (element.value.campusId) {
  //       if (this.selectedCampuses.indexOf(element.value.campusId) < 0) {
  //         this.selectedCampuses.push(element.value.campusId);
  //       }
  //     }
  //   });
  // }

  // removeCampusItem(i: number, item) {
  //   let campuses = <FormArray>this.fg.controls['campuses'];
  //   campuses.removeAt(i);
  //   if (item.controls.campusId.value) {
  //     let index = this.selectedCampuses.indexOf(item.controls.campusId.value);
  //     if (index > -1) {
  //       this.selectedCampuses.splice(index, 1);
  //     }
  //   }

  //   if (item.controls.id.value) {
  //     this.deletedCampusIds.push(item.controls.id.value);
  //   }
  //   if (campuses.controls.length < 1) {
  //     this.addNewCampusItem();
  //   }
  // }