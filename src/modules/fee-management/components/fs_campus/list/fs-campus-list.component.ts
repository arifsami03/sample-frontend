import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';

import { PageAnimation } from '../../../../shared/helper';
import { GLOBALS } from '../../../../app/config/globals';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { LayoutService } from '../../../../layout/services';
import { FSCampusService } from '../../../services';
import { FSCampusModel } from '../../../models';

@Component({
  selector: 'settings-fs-campus-list',
  templateUrl: './fs-campus-list.component.html',
  styleUrls: ['./fs-campus-list.component.css'],
  animations: [PageAnimation]
})
export class FSCampusListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public data: FSCampusModel[] = [new FSCampusModel()];

  public attrLabels = FSCampusModel.attributesLabels;

  displayedColumns = ['campus', 'feeStructure', 'options'];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public error: Boolean;

  constructor(
    private fsCampusService: FSCampusService,
    public matDialog: MatDialog,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Campus Fee Structure' });

    this.getRecords();
  }

  /**
   * Get data/records from backend
   */
  getRecords() {

    this.fsCampusService.index().subscribe(response => {

      this.data = response;

       // for searching mechanism we are maping this in to single string
       this.data.map(element=>{
        element.campusName = element.campus.campusName;
        element.feeStructureTitle = element.feeStructure.title;
      })

      this.dataSource = new MatTableDataSource<FSCampusModel>(this.data);

      this.dataSource.paginator = this.paginator;

      this.dataSource.sort = this.sort;

    },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {

    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;

  }

  /**
   * Delete record
   */
  delete(id: number) {

    // Confirm dialog
    this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.fsCampusService.delete(id).subscribe(response => {
            this.layoutService.flashMsg({ msg: 'Campus Fee Structure has been deleted.', msgType: 'success' });
            this.getRecords();
          },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
              this.loaded = true;
            }
          );
        }
      });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }

}
