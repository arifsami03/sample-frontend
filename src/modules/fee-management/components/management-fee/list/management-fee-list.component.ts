import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { PageAnimation } from 'modules/shared/helper';
import { LayoutService } from 'modules/layout/services';
import { ManagementFeeService } from 'modules/fee-management/services';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { ManagementFeeModel } from 'modules/fee-management/models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'management-fee-list',
  templateUrl: './management-fee-list.component.html',
  styleUrls: ['./management-fee-list.component.css'],
  animations: [PageAnimation]
})
export class ManagementFeeListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;


  public data: ManagementFeeModel[] = [new ManagementFeeModel()];
  public feeTypes = GLOBALS.FEE_TYPES;
  public attrLabels = ManagementFeeModel.attributesLabels;

  displayedColumns = ['educationLevelId', 'feeType', 'amount', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public success: Boolean;

  constructor(
    private managementFeeService: ManagementFeeService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Management Fees' });
    this.getRecords();
  }

  /**
   * Get all records
   */
  getRecords(): void {
    this.managementFeeService.index().subscribe(
      response => {
        this.data = response;

        // for searching mechanism we are maping this in to single string
        this.data.map(element => {
          let selectedFeetype = this.feeTypes.find(item => {
            return item.key == element.feeType;
          });
          element.feeType = selectedFeetype ? selectedFeetype.label : '';
        });

        this.dataSource = new MatTableDataSource<ManagementFeeModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }

    );
  }

  /**
   * search
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete data against the id given
   */
  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.managementFeeService.delete(id).subscribe(
          response => {

            if (response && (response['children']) ? response['children'].length > 0 : false) {

              this.loaded = true;
              this.pageState = 'active';

              this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

            } else {

              this.layoutService.flashMsg({ msg: 'Management Fee has been deleted.', msgType: 'success' });
              this.getRecords();

            }

          },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
            this.pageState = 'active';
          },
          () => {
            this.getRecords();
            this.loaded = true;
            this.pageState = 'active';
          }
        );
      }
    });
  }

  sortData() {
    this.dataSource.sort = this.sort;
  }

}
