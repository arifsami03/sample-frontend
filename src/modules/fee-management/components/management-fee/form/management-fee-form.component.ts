import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { GLOBALS } from '../../../../app/config/globals';
import { PageAnimation } from '../../../../shared/helper';

import { LayoutService } from 'modules/layout/services';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { EducationLevelService } from 'modules/institute/services';
import { EducationLevelModel } from 'modules/institute/models';
import { ManagementFeeService } from 'modules/fee-management/services';
import { ManagementFeeModel } from 'modules/fee-management/models';

@Component({
  selector: 'management-fee-form',
  templateUrl: './management-fee-form.component.html',
  styleUrls: ['./management-fee-form.component.css'],
  animations: [PageAnimation],
  providers: [EducationLevelService]
})
export class ManagementFeeFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public boxLoaded = true;

  public pageActions = GLOBALS.pageActions;
  public error: Boolean;
  public educationLevel: EducationLevelModel[];
  public fg: FormGroup;
  public pageAct: string;
  public managementFee: ManagementFeeModel;
  public feeTypes = GLOBALS.FEE_TYPES;
  public educationName;

  public componentLabels = ManagementFeeModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private managementFeeService: ManagementFeeService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private educationLevelService: EducationLevelService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new ManagementFeeModel().validationRules());
    this.getEducationLevelList();
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get Data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.managementFeeService.find(params['id']).subscribe(
        response => {
          this.managementFee = response;
          this.educationName = response;
          this.fg.patchValue(this.managementFee);
          if (!this.error) {
            if (this.pageAct === this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct === this.pageActions.update) {
              this.initUpdatePage();
            }
            this.loaded = true;
          }
        },

        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Get Level of Education List
   */
  getEducationLevelList() {
    this.educationLevelService.findAttributesList().subscribe(
      response => {
        this.educationLevel = response;
      },
      error => console.log(error),
      () => { }
    );
  }

  /**
   * Initialize page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Management Fee: Create' });
    this.managementFee = new ManagementFeeModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * Initialize view page
   */
  private initViewPage() {
    this.fg.disable();
    this.layoutService.setPageTitle({
      title: 'Management Fee: ' + this.educationName.educationLevel.education,
      breadCrumbs: [{ label: 'Home', url: '/home/dashboard' }, { label: 'Management Fee ', url: '/fee-management/managementFees' }, { label: this.managementFee.amount }]
    });
  }

  /**
   * Initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Management Fee: ' + this.educationName.educationLevel.education,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Management Fee', url: '/fee-management/managementFees' },
        {
          label: this.managementFee.amount,
          url: `/fee-management/managementFees/view/${this.managementFee.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Add or Update when save button is clicked
   */
  public saveData(item: ManagementFeeModel) {
    this.boxLoaded = false;
    let id;
    if (this.pageAct === this.pageActions.create) {
      item.id = null;
      this.managementFeeService.create(item).subscribe(
        response => {
          if (response) {
            this.layoutService.flashMsg({ msg: 'Management Fee has been created.', msgType: 'success' });
            this.router.navigate([`/fee-management/managementFees`]);
          }
        },

        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        },
        () => {
          this.loaded = true;
          this.boxLoaded = true;
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.managementFeeService.update(this.managementFee.id, item).subscribe(
        response => {
          if (response) {
            this.layoutService.flashMsg({ msg: 'Management Fee has been updated.', msgType: 'success' });
            this.router.navigate([`/fee-management/managementFees`]);
          }
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        },
        () => {
          this.loaded = true;
          this.boxLoaded = true;
        }
      );
    }
  }

  /**
   * Delete
   */
  delete(id: number) {

    const dialogRef = this.matDialog.open(ConfirmDialogComponent,
      { width: GLOBALS.deleteDialog.width, data: { message: GLOBALS.deleteDialog.message } }
    );

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';
        this.managementFeeService.delete(id).subscribe(response => {

          if (response && (response['children']) ? response['children'].length > 0 : false) {

            this.loaded = true;
            this.pageState = 'active';

            this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

          } else {

            this.layoutService.flashMsg({ msg: 'Management Fee has been deleted.', msgType: 'success' });
            this.router.navigate(['fee-management/managementFees']);

          }

        },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
            this.pageState = 'active';
          },
          () => {
            this.loaded = true;
            this.pageState = 'active';
          }
        );
      }
    });
  }


}
