import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from 'modules/shared/helper';
import { GLOBALS } from 'modules/app/config/globals';
import { ScholarshipService, FeeHeadsService } from 'modules/fee-management/services';
import { LayoutService } from 'modules/layout/services';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { ScholarshipModel, FeeHeadsModel } from 'modules/fee-management/models';

@Component({
  selector: 'settings-scholarship-form',
  templateUrl: './scholarship-form.component.html',
  styleUrls: ['./scholarship-form.component.css'],
  animations: [PageAnimation]
})
export class ScholarshipFormComponent implements OnInit {
  public pageState = 'active';

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;

  public boxLoaded: boolean = true;

  public fg: FormGroup;
  public pageAct: string;
  public scholarship: ScholarshipModel;
  public options: Observable<string[]>;
  public componentLabels = ScholarshipModel.attributesLabels;

  public sFeeHeadIds: number[] = [];

  public scholarshipTypes: any[] = [GLOBALS.scholarshipTypes.annual, GLOBALS.scholarshipTypes.monthly];

  /**
   * for Fee Heads list
   */
  public data: FeeHeadsModel[] = [new FeeHeadsModel()];

  public attrLabels = FeeHeadsModel.attributesLabels;

  displayedColumns = ['title', , 'options'];

  dataSource: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private scholarshipService: ScholarshipService,
    private feeHeadService: FeeHeadsService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    this.findAttributesList();
  }

  /**
   * get all records for program details
   */
  findAttributesList(): void {
    this.feeHeadService.findAttributesList().subscribe(
      response => {
        this.data = response;
        // select existing id on check
        this.isCheckBoxesChecked();
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new ScholarshipModel().validationRules());

    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get record
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.scholarshipService.find(params['id']).subscribe(
        response => {
          this.scholarship = response;
          if (this.scholarship) {

            this.fg.patchValue(this.scholarship);
            //TODO:low need to check why we are dowing this after fetching data
            if (this.pageAct == this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct == this.pageActions.update) {
              // pushing ids to sFeeHeadIds
              let scholarshipFeeHeadsIds: any = this.scholarship['scholarshipFeeHeads'];
              scholarshipFeeHeadsIds.forEach(element => {
                this.sFeeHeadIds.push(element['feeHeadId']);
              });

              this.initUpdatePage();
            }
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Scholarship: Create' });

    this.scholarship = new ScholarshipModel();

    this.fg.enable();
  }

  /**
   * initialize the view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Scholarship: ' + this.scholarship.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Scholarship ',
          url: '/fee-management/scholarships'
        },
        { label: this.scholarship.title }
      ]
    });
  }

  /**
   * initialize the update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Scholarship: ' + this.scholarship.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Scholarship',
          url: '/fee-management/scholarships'
        },
        {
          label: this.scholarship.title,
          url: `/fee-management/scholarships/view/${this.scholarship.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: ScholarshipModel) {
    this.boxLoaded = false;

    item.abbreviation = item.abbreviation.toUpperCase();

    item['feeHeadId'] = this.sFeeHeadIds;
    if (this.pageAct === this.pageActions.create) {
      this.scholarshipService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Scholarship has been created.', msgType: 'success' });
          this.router.navigate([`/fee-management/scholarships`]);
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        },
        () => {
          this.boxLoaded = true;
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.scholarshipService.update(this.scholarship.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Scholarship has been updated.', msgType: 'success' });
          this.router.navigate([`/fee-management/scholarships`]);
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        },
        () => {
          this.boxLoaded = true;
        }
      );
    }
  }

  checkboxClicked(event: any, feeHeadId) {
    if (event.checked) {
      this.sFeeHeadIds.push(feeHeadId);
    } else {
      const index = this.sFeeHeadIds.indexOf(feeHeadId);

      if (index > -1) {
        this.sFeeHeadIds.splice(index, 1);
      }
    }
  }

  /**
   * Delete record
   *
   */
  delete(id: number) {

    const dialogRef = this.matDialog.open(ConfirmDialogComponent,
      { width: GLOBALS.deleteDialog.width, data: { message: GLOBALS.deleteDialog.message } }
    );

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.scholarshipService.delete(id).subscribe(response => {

          if (response && (response['children']) ? response['children'].length > 0 : false) {

            this.loaded = true;
            this.pageState = 'active';

            this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

          } else {

            this.layoutService.flashMsg({ msg: 'Scholarship has been deleted.', msgType: 'success' });
            this.router.navigate(['/fee-management/scholarships']);

          }

        },
          error => {
            this.loaded = true;
            this.pageState = 'active';
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
          }
        );
      }
    });
  }
  isCheckBoxesChecked() {
    if (this.data) {
      if (this.scholarship) {
        let scholarshipFeeHeadsArray: any = this.scholarship['scholarshipFeeHeads'];
        if (this.scholarship['scholarshipFeeHeads']) {
          for (let i = 0; i < this.data.length; i++) {
            scholarshipFeeHeadsArray.forEach(acpd_element => {
              if (this.data[i].id === acpd_element.feeHeadId) {
                this.data[i]['status'] = true;
              }
            });
          }
        }
      }
    }
  }
  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }
  /**
   * Delete record
   *
   */
  deleteScholarshipFeeHead(scholarshipId: number, feeHeadId: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.scholarshipService.deleteScholarshipFeeHead(scholarshipId, feeHeadId).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Scholarship Fee Head has been deleted.', msgType: 'success' });
              this.getData();
            },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
            }
          );
        }
      });
  }

}
