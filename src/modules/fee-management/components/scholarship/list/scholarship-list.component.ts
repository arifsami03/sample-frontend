import { IActionButton, IHeaderAttributes, ITableAttributes } from '../../../../shared/interfaces';
import { Component, ViewChild, OnInit, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { PageAnimation } from 'modules/shared/helper';
import { GLOBALS } from 'modules/app/config/globals';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { LayoutService } from 'modules/layout/services';
import { ScholarshipService } from 'modules/fee-management/services';
import { ScholarshipModel } from 'modules/fee-management/models';
import { DataTableService } from 'modules/shared/services';

@Component({
  selector: 'settings-scholarship-list',
  templateUrl: './scholarship-list.component.html',
  styleUrls: ['./scholarship-list.component.css'],
  animations: [PageAnimation],
  // providers: [DataTableService]

})
export class ScholarshipListComponent implements OnInit {
  public pageState;

  public loaded: boolean = false;

  public data: ScholarshipModel[];

  public attrLabels = ScholarshipModel.attributesLabels;

  displayedColumns = ['title', 'abbreviation', 'options'];

  dataSource: any;
  public lengthOfData: number = 0;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  // For Server side table 
  // public actionButtons: IActionButton[] = [{
  //   type: 'redirect',
  //   url: '/fee-management/scholarships/view',
  //   icon: 'visibility',
  //   toolTip: 'View',
  //   perm: 'scholarships.find',
  // }, {
  //   type: 'redirect',

  //   url: '/fee-management/scholarships/update',
  //   icon: 'edit',
  //   toolTip: 'Edit',
  //   perm: 'scholarships.update',
  // }, {
  //   type: 'delete-dialogue',
  //   url: '',
  //   icon: 'delete',
  //   toolTip: 'Delete',
  //   perm: 'scholarships.delete',
  // }];
  // public headerAttributes: IHeaderAttributes[] = [{
  //   field: 'title',
  //   label: 'Title',
  //   width: 40,
  //   key: 'title'
  // },
  // {
  //   field: 'abbreviation',
  //   label: 'Abbreviation',
  //   width: 40,
  //   key: 'abbreviation'
  // },
  // {
  //   field: 'options',
  //   width: 20,
  // }
  // ]


  /**
   * For Server Side paginaton
   */
  // public tableAttributes: ITableAttributes;



  constructor(private scholarshipService: ScholarshipService, public dialog: MatDialog, public matDialog: MatDialog, private router: Router, public layoutService: LayoutService,
    // private dts: DataTableService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Scholarship' });

    // this.getRecords({ offset: 0, limit: GLOBALS.dataTable.pageSize });
    this.getRecords();

  }


  /**
   * Get data/records from backend
   */
  // getRecords(_tableAttributes: ITableAttributes) {

  //   this.pageState = '';

  //   this.tableAttributes = _tableAttributes;
  //   this.tableAttributes.search
  //   this.scholarshipService.index(this.tableAttributes).subscribe(response => {

  //     if (response) {

  //       this.data = response['rows'];
  //       this.lengthOfData = response['count'];

  //       let itm = {
  //         headerAttributes: this.headerAttributes,
  //         dataLength: this.lengthOfData,
  //         actionButtons: this.actionButtons,
  //         data: this.data
  //       }

  //       this.dts.refreshDataTable(itm);
  //     }

  //     this.loaded = true;
  //   },
  //     error => {
  //       console.log(error);
  //       this.loaded = true;
  //       this.pageState = 'active';
  //     },
  //     () => {
  //       // this.dataSource = new MatTableDataSource<ScholarshipModel>(this.data);
  //       // this.dataSource.paginator = this.paginator;
  //       // this.dataSource.sort = this.sort;
  //       this.pageState = 'active';
  //     }
  //   );
  // }
  getRecords() {


    this.scholarshipService.index().subscribe(response => {

      if (response) {

        this.data = response;
      }

      this.loaded = true;
    },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.dataSource = new MatTableDataSource<ScholarshipModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.pageState = 'active';
      }
    );
  }

  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete record
   */
  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.scholarshipService.delete(id).subscribe(
          response => {

            if (response && (response['children']) ? response['children'].length > 0 : false) {

              this.loaded = true;
              this.pageState = 'active';

              this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

            } else {

              this.layoutService.flashMsg({ msg: 'Scholarship has been deleted.', msgType: 'success' });
              this.getRecords();

            }

          },
          error => {
            this.loaded = true;
            this.pageState = 'active';
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          }
        );
      }
    });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }

}
