import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { ManagementFeeModel } from '../models';
import { BaseService } from '../../shared/services';

@Injectable()
export class ManagementFeeService extends BaseService {
  private routeURL: String = 'feeManagement/managementFees';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * return data
   */
  index(): Observable<ManagementFeeModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <ManagementFeeModel[]>data.json();
    });
  }

  /**
   * Return single Data
   */
  find(id: number): Observable<ManagementFeeModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <ManagementFeeModel>data.json();
    });
  }

  /**
   * Find Fee by educationLevelId
   */
  findFee(educationLevelId: number): Observable<ManagementFeeModel> {
    return this.__get(`${this.routeURL}/findFee/${educationLevelId}`).map(data => {
      return <ManagementFeeModel>data.json();
    });
  }

  /**
   * Delete Record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }
  /**
   * find record
   */
  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }

  findByEducationLevelId(id: number): Observable<ManagementFeeModel[]> {
    return this.__get(`${this.routeURL}/findByEducationLevelId/${id}`).map(data => {
      return <ManagementFeeModel[]>data.json();
    });
  }

  /**
   * Create Record
   */
  create(managementFee: ManagementFeeModel) {
    return this.__post(`${this.routeURL}/create`, managementFee).map(data => {
      return data.json();
    });
  }

  /**
   * Update Record
   */
  update(id: number, managementFee: ManagementFeeModel) {
    return this.__put(`${this.routeURL}/update/${id}`, managementFee).map(data => {
      return data.json();
    });
  }
}
