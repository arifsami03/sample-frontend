import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../shared/services'; 
import { FSInstallementModel } from '../models';

@Injectable()
export class FSInstallementService extends BaseService {

  private routeURL: String = 'feeManagement/fsInstallements';

  constructor(protected http: Http) {
    super(http)
  }

  /**
  * Get single record
  */
  find(id: number): Observable<FSInstallementModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <FSInstallementModel>data.json();
    }).catch(this.handleError);
  }

  /**
   * Create record
   */
  create(item: FSInstallementModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  /**
 * Update record
 */
  update(id: number, item: FSInstallementModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

}
