import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from '../../shared/services';
import { ScholarshipModel } from '../models';

@Injectable()
export class ScholarshipService extends BaseService {
  private routeURL: String = 'feeManagement/scholarships';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  // index(_pagination?): Observable<ScholarshipModel[]> {
  //   return this.__get(`${this.routeURL}/index`, _pagination).map(data => {
  //     return <ScholarshipModel[]>data.json();
  //   });
  // }
  index(): Observable<ScholarshipModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <ScholarshipModel[]>data.json();
    });
  }

  /**
   * it will return all the data of programs
   */
  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }
  /**
   * will return all the data of programs by percentage range
   */
  findByPercentage(fromPercentage, toPercentage): Observable<ScholarshipModel[]> {
    return this.__get(`${this.routeURL}/findByPercentage/${fromPercentage}/${toPercentage}`).map(data => {
      return <ScholarshipModel[]>data.json();
    });
  }
  /**
   * Get single record
   */
  find(id: number) {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }
  /**
   * Delete record
   */
  deleteScholarshipFeeHead(scholarshipId: number, feeHeadId) {
    return this.__delete(`${this.routeURL}/deleteScholarshipFeeHead/${scholarshipId}/${feeHeadId}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: ScholarshipModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: ScholarshipModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }
}
