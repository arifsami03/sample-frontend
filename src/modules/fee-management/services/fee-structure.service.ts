import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../shared/services'; 
import { FeeStructureModel, FeeHeadsModel, FeeStructureFeeHeadsModel } from '../models';

@Injectable()
export class FeeStructureService extends BaseService {

  private routeURL: String = 'feeManagement/feeStructures';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<FeeStructureModel[]> {

    return this.__get(`${this.routeURL}/index`).map(data => {
      return <FeeStructureModel[]>data.json();
    }).catch(this.handleError);

  }

  /**
  * Get single record
  */
  find(id: number): Observable<FeeStructureModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <FeeStructureModel>data.json();
    }).catch(this.handleError);
  }

  /**
   * Create record
   */
  create(item: FeeStructureModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  /**
 * Update record
 */
  update(id: number, item: FeeStructureModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  /**
   * Get All records with only id, name attributes 
   */
  findAttributesList(): Observable<FeeStructureModel[]> {

    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <FeeStructureModel[]>data.json();
    }).catch(this.handleError);

  }

  /*************************************************************************************************************************************
  *************************************************************************************************************************************
   * This section is used for fee structure fee heads 
   */

  /**
 * Get All Fee Heads for a Fee Structure
 */
  findAllFeeHeads(feeStructureId: number): Observable<FeeStructureFeeHeadsModel[]> {

    return this.__get(`${this.routeURL}/findAllFeeHeads/${feeStructureId}`).map(data => {
      return <FeeStructureFeeHeadsModel[]>data.json();
    }).catch(this.handleError);

  }

  /**
   * Get All Fee Heads which are not add to current fee structure for add scenario
   * And for update scenario current fee head along with the remaining fee heads returns
   */
  findFeeHead(id: number): Observable<FeeStructureFeeHeadsModel> {

    return this.__get(`${this.routeURL}/findFeeHead/${id}`).map(data => {
      return <FeeStructureFeeHeadsModel>data.json();
    }).catch(this.handleError);

  }

  /**
 * Get All Fee Heads which are not add to current fee structure for add scenario
 * And for update scenario current fee head along with the remaining fee heads returns
 */
  findAllRemainingFeeHeads(feeStructureId: number, feeHeadsId?: number): Observable<FeeHeadsModel[]> {
    let url = null;
    url += feeStructureId;
    if (feeHeadsId) {
      url += '/' + feeHeadsId;
    }
    return this.__get(`${this.routeURL}/findAllRemainingFeeHeads/${url}`).map(data => {
      return <FeeHeadsModel[]>data.json();
    }).catch(this.handleError);

  }

  /**
  * Create record i.e add fee head to fee structure
  */
  createFeeHead(item: FeeStructureFeeHeadsModel) {
    return this.__post(`${this.routeURL}/createFeeHead`, item).map(data => {
      return <FeeStructureFeeHeadsModel>data.json();
    }).catch(this.handleError);
  }

  /**
 * Update record i.e fee-structure-fee-head
 */
  updateFeeHead(id: number, item: FeeStructureFeeHeadsModel) {
    return this.__put(`${this.routeURL}/updateFeeHead/${id}`, item).map(data => {
      return <FeeStructureFeeHeadsModel>data.json();
    }).catch(this.handleError);
  }

  /**
     * Delete record i.e fee-structure-fee-head
     */
  deleteFeeHead(id: number) {
    return this.__delete(`${this.routeURL}/deleteFeeHead/${id}`).map(data => {
      return data.json();
    }).catch(this.handleError);
  }


}
