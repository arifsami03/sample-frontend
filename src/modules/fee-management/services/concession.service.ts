import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from '../../shared/services';
import { ConcessionModel } from '../models';

@Injectable()
export class ConcessionService extends BaseService {

  private routeURL: String = 'feeManagement/concessions';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<ConcessionModel[]> {

    return this.__get(`${this.routeURL}/index`).map(data => {
      return <ConcessionModel[]>data.json();
    });

  }

  /**
   * Get single record
   */
  findWithFeeHeads(id: number): Observable<ConcessionModel> {
    return this.__get(`${this.routeURL}/findWithFeeHeads/${id}`).map(data => {
      return <ConcessionModel>data.json();
    });
  }

  /**
   * Delete record
   */
  deleteWithFeeHeads(id: number) {
    return this.__delete(`${this.routeURL}/deleteWithFeeHeads/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * createWithFeeHeads record
   */
  createWithFeeHeads(item: ConcessionModel) {
    return this.__post(`${this.routeURL}/createWithFeeHeads`, item).map(data => {
      return data.json();
    });
  }

  /**
   * updateWithFeeHeads record
   */
  updateWithFeeHeads(id: number, item: ConcessionModel) {
    return this.__put(`${this.routeURL}/updateWithFeeHeads/${id}`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Delete single concession fee head
   */
  deleteConcessionFeeHead(concessionId: number, feeHeadId) {
    return this.__delete(`${this.routeURL}/deleteConcessionFeeHead/${concessionId}/${feeHeadId}`).map(data => {
      return data.json();
    });
  }

}
