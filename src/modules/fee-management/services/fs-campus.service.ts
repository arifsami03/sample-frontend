import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../shared/services'; 
import { FSCampusModel } from '../models';

@Injectable()
export class FSCampusService extends BaseService {

  private routeURL: String = 'feeManagement/fsCampus';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<FSCampusModel[]> {

    return this.__get(`${this.routeURL}/index`).map(data => {
      return <FSCampusModel[]>data.json();
    }).catch(this.handleError);

  }

  /**
  * Get single record
  */
  find(id: number): Observable<FSCampusModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <FSCampusModel>data.json();
    }).catch(this.handleError);
  }

  /**
   * Create record
   */
  create(item: FSCampusModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  /**
 * Update record
 */
  update(id: number, item: FSCampusModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

}
