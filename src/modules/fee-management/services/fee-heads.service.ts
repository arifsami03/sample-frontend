import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../shared/services';
import { FeeHeadsModel } from '../models';

@Injectable()
export class FeeHeadsService extends BaseService {

  private routeURL: String = 'feeManagement/feeHeads';
  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<FeeHeadsModel[]> {

    return this.__get(`${this.routeURL}/index`).map(data => {
      return <FeeHeadsModel[]>data.json();
    }).catch(this.handleError);

  }

  /**
  * Get single record
  */
  find(id: number): Observable<FeeHeadsModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <FeeHeadsModel>data.json();
    }).catch(this.handleError);
  }

  /**
   * Create record
   */
  create(item: FeeHeadsModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  /**
 * Update record
 */
  update(id: number, item: FeeHeadsModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  /**
   * Get All records with only id, name attributes 
   */
  findAttributesList(): Observable<FeeHeadsModel[]> {

    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <FeeHeadsModel[]>data.json();
    }).catch(this.handleError);

  }

}
