import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../shared/services'; 
import { FS_InstallmentPlanModel } from '../models';

@Injectable()
export class FSInstallmentPlanService extends BaseService {

  private routeURL: String = 'feeManagement/fsInstallmentPlans';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Find all Installment Plans of a Fee Structure
   */
  findAll(feeStructureId: number): Observable<FS_InstallmentPlanModel[]> {

    return this.__get(`${this.routeURL}/findAll/${feeStructureId}`).map(data => {
      return <FS_InstallmentPlanModel[]>data.json();
    }).catch(this.handleError);

  }

  /**
  * Get single record
  */
  find(id: number): Observable<FS_InstallmentPlanModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <FS_InstallmentPlanModel>data.json();
    }).catch(this.handleError);
  }

  /**
   * Create record
   */
  create(item: FS_InstallmentPlanModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  /**
 * Update record
 */
  update(id: number, item: FS_InstallmentPlanModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    }).catch(this.handleError);
  }


}
