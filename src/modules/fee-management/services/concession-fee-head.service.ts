import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from '../../shared/services'; 
import { ConcessionFeeHeadModel } from '../models';

@Injectable()
export class ConcessionFeeHeadService extends BaseService {

  private routeURL: String = 'feeManagement/concessionFeeHeads';

  constructor(protected http: Http) {
    super(http)
  }

 
  /**
   * Get single record
   */
  findByConcession(concessionId: number): Observable<ConcessionFeeHeadModel> {
    return this.__get(`${this.routeURL}/findByConcession/${concessionId}`).map(data => {
      return <ConcessionFeeHeadModel>data.json();
    });
  }

}
