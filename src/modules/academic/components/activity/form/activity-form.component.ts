import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from '../../../../shared/helper';

import { ActivityService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { ActivityModel } from '../../../models';

@Component({
  selector: 'academic-activity-form',
  templateUrl: './activity-form.component.html',
  styleUrls: ['./activity-form.component.css'],
  animations: [PageAnimation]
})
export class ActivityFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public boxLoaded: boolean = true;

  private mNumber: ActivityModel;
  public fg: FormGroup;
  public pageAct: string;
  public activity: ActivityModel;
  public options: Observable<string[]>;
  public check = true;

  public componentLabels = ActivityModel.attributesLabels;

  public daysDifference: number=0;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private activityService: ActivityService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * it will initialize the activity form page
   */
  private initializePage() {
    this.fg = this.fb.group(new ActivityModel().validationRules());
    if (this.pageAct === 'add') {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get Data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.activityService.find(+params['id']).subscribe(
        response => {
          this.activity = response;
          if (response) {
            // display of Succes and Error Message
            this.fg.patchValue(this.activity);
            if (this.activity.startDate &&  this.activity.endDate){
              this.countDayDiffrence(new Date(this.activity.startDate),new Date(this.activity.endDate));
            }
            
          }
          if (this.pageAct === 'view') {
            this.initViewPage();
          } else if (this.pageAct === 'update') {
            this.initUpdatePage();
          }
          this.loaded = true;
        },

        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    });
  }



  /**
   * initialize page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Create Activity' });
    this.activity = new ActivityModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * initialize view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Activity: ' + this.activity.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Activity ', url: '/academic/activities' },
        { label: this.activity.title }
      ]
    });
  }

  /**
   * initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Activity: ' + this.activity.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Activity', url: '/academic/activities' },
        {
          label: this.activity.title,
          url: `/academic/activities/view/${this.activity.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * add or update when save button is clicked
   */
  public saveData(item: ActivityModel) {
    this.boxLoaded = false;
    this.fg.disable();
    let id;
    if (this.pageAct === 'add') {
      item.id = null;
      this.activityService.create(item).subscribe(
        response => {
          if (response) {
            id = response.id;

            this.layoutService.flashMsg({ msg: 'Activity has been created.', msgType: 'success' });
            this.router.navigate([`/academic/activities`]);
          }
        },

        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        }, () => {
          this.boxLoaded = true;
          this.fg.disable();
        }
      );
    } else if (this.pageAct === 'update') {
      this.activityService.update(this.activity.id, item).subscribe(
        response => {
          if (response) {
            this.layoutService.flashMsg({ msg: 'Activity has been updated.', msgType: 'success' });
            this.router.navigate([
              `/academic/activities`
            ]);
          }
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        }, () => {
          this.boxLoaded = true;
        }
      );
    }
  }

  /**
   * delete the selected activity
   */
  deleteRecord(id: number) {
    this.loaded = false;
    this.pageState = '';

    this.activityService.delete(id).subscribe(response => {
      this.layoutService.flashMsg({ msg: 'Activity has been deleted.', msgType: 'success' });
      this.router.navigate(['institute/activities']);
    }
      ,
      error => {
        console.log(error);
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        this.loaded = true;
        this.pageState = 'active';
      }, () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }
  /**
   * Calling on both date picker change date event
   */
  dateChangeEvent() {

    // get form control of dates
    let startDateControl = this.fg.get('startDate');
    let endDateControl = this.fg.get('endDate');

    // checking if both fields are filled
    if (startDateControl.value && endDateControl.value){

      let startDate = new Date(startDateControl.value);
      let endDate = new Date(endDateControl.value);
      
      if (startDate > endDate){
      
        startDateControl.setErrors({invalid : true})
      
      }else{
      
        startDateControl.setErrors(null);
      
        this.countDayDiffrence(startDate,endDate);
      
      }
     
    }

  }
  /**
   * Counting the number of days between two dates
   * @param startDate 
   * @param endDate 
   */
  countDayDiffrence(startDate: Date, endDate:Date) {
    let oneDay = 24*60*60*1000; 

    if (startDate && endDate) {
     
      this.daysDifference = Math.round(Math.abs((startDate.getTime() - endDate.getTime())/(oneDay)));
    
    }else{
    
      this.daysDifference = 0 ;
    
    }
  }

}
