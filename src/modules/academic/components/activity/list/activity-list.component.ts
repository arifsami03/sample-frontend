import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router } from '@angular/router';

import { PageAnimation } from '../../../../shared/helper';

import { LayoutService } from '../../../../layout/services';
import { ActivityService } from '../../../services';

import { ActivityModel } from '../../../models/activity';

import { DatePipe } from '@angular/common';
@Component({
  selector: 'academic-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css'],
  animations: [PageAnimation],
  providers: [DatePipe]
})
export class ActivityListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;


  public data: ActivityModel[] = [];
  public attrLabels = ActivityModel.attributesLabels;

  public daysDifference: number = 0;

  displayedColumns = [
    'title', 'dates', 'days', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private activityService: ActivityService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService,
    private datePipe: DatePipe
  ) { }

  /**
   * initialize the page of activitys list
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Activities' });
    this.getRecords();
  }

  /**
   * get all records
   */
  getRecords(): void {
    this.activityService.index().subscribe(
      response => {
        this.data = response;
        // mapping the ncademic calendar program detail on full name attributes
        this.data.map(element => {

          if (element.startDate && element.endDate) {
            element.days = this.countDayDiffrence(new Date(element.startDate), new Date(element.endDate));
            element.dates = this.datePipe.transform(new Date(element.startDate)) + ' - ' + this.datePipe.transform(new Date(element.endDate));
          }

        })
        this.dataSource = new MatTableDataSource<ActivityModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;


      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }

    );
  }

  /**
   * change alphabets into lowercase
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * delete data against the id given
   */
  deleteActivity(id: number) {
    // Confirm dialog
    // const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
    //   width: GLOBALS.deleteDialog.width,
    //   data: { message: GLOBALS.deleteDialog.message }
    // });
    // dialogRef.afterClosed().subscribe((accept: boolean) => {
    //   if (accept) {
    this.loaded = false;
    this.activityService.delete(id).subscribe(response => {
      this.layoutService.flashMsg({ msg: 'Activity has been deleted.', msgType: 'success' });
      this.getRecords();
    },
      error => {
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.getRecords();
        this.loaded = true;
      }
    );
    //   }
    // });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }

  /**
  * Counting the number of days between two dates
  * @param startDate 
  * @param endDate 
  */
  countDayDiffrence(startDate: Date, endDate: Date): number {
    let oneDay = 24 * 60 * 60 * 1000;

    if (startDate && endDate) {

      this.daysDifference = Math.round(Math.abs((startDate.getTime() - endDate.getTime()) / (oneDay)));

    } else {

      this.daysDifference = 0;

    }
    return this.daysDifference;
  }

}
