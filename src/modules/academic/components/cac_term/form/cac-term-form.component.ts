import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from '../../../../shared/helper';
import { GLOBALS } from '../../../../app/config/globals';
import { LayoutService } from '../../../../layout/services';

import { CACTermService, ProgramDetailsService, AcademicCalendarService, AcademicCalendarProgramDetailService } from '../../../services';
import { CACTermModel, ProgramDetailsModel, AcademicCalendarProgramDetailModel } from '../../../models';

import { CampusAcademicCalendarService } from '../../../../institute/services';
import { CampusAcademicCalendarModel } from '../../../../institute/models';

@Component({
  selector: 'academic-cac-term-form',
  templateUrl: './cac-term-form.component.html',
  styleUrls: ['./cac-term-form.component.css'],
  animations: [PageAnimation],
  providers: [CampusAcademicCalendarService]
})
export class CACTermFormComponent implements OnInit {
  public pageState = 'active';

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public boxLoaded: boolean = true;

  public fg: FormGroup;
  public options: Observable<string[]>;
  public componentLabels = CACTermModel.attributesLabels;
  public attrLabels = ProgramDetailsModel.attributesLabels;
  public academicCalendarProgramDetailId: number;
  public id: number;

  public academicCalendars: CampusAcademicCalendarModel[];
  public academicCalendarProgramDetails: AcademicCalendarProgramDetailModel[];
  public academicCalendarProgramDetail: AcademicCalendarProgramDetailModel;
  public cacTerms: CACTermModel[];
  public cacTerm: CACTermModel;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private cacTermService: CACTermService,
    private layoutService: LayoutService,
    public dialogRef: MatDialogRef<CACTermFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    // getting the data from the called component
    this.academicCalendarProgramDetailId = data.academicCalendarProgramDetail['id'];
    this.academicCalendarProgramDetail = data.academicCalendarProgramDetail;
    this.cacTerms = data.academicCalendarProgramDetail['cacTerms'];
    this.id = data.id;
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new CACTermModel().validationRules());

    if (this.data.type != 'Add Academic Calendar Term') {

      this.getData();

    } else {

      this.loaded = true;

    }
  }

  /**
   * Get record
   */
  private getData() {
    this.cacTermService.find(this.id).subscribe(

      response => {
        if (response) {

          // patching values
          this.cacTerm = response;
          this.fg.patchValue(this.cacTerm);

        }
      },
      error => {

        console.log(error);
        this.loaded = true;

      },
      () => {

        this.loaded = true;

      }
    );
  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: CACTermModel) {

    item.abbreviation = item.abbreviation.toUpperCase();

    // validating over lap date and start & end dates
    if (this.isEndDateGreater() && !this.isBetweenDates()) {

      this.boxLoaded = false;

      // giving the id of academic calendar program details
      item['academicCalendarProgramDetailId'] = this.academicCalendarProgramDetailId;

      if (this.data.type === 'Add Academic Calendar Term') {

        // creating the record
        this.cacTermService.create(item).subscribe(

          response => {

            this.layoutService.flashMsg({ msg: 'Academic Calendar Term has been created.', msgType: 'success' });
            this.dialogRef.close({ response: false });

          },
          error => {

            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;

          },
          () => {

            this.boxLoaded = true;

          }
        );
      } else {

        // updating the record
        this.cacTermService.update(this.cacTerm.id, item).subscribe(

          response => {

            this.layoutService.flashMsg({ msg: 'Academic Calendar Term has been updated.', msgType: 'success' });
            this.dialogRef.close({ response: false });

          },

          error => {

            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);

          },

          () => {

            this.boxLoaded = true;

          }
        );
      }
    }
  }

  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close({ response: false });
  }

  /**
   * Checking the end date is greater from start date
   */
  isEndDateGreater() {
    let startDate = this.fg.controls['startDate'].value;
    let endDate = this.fg.controls['endDate'].value;

    if (endDate < startDate) {

      // setting error on field
      this.fg.controls['endDate'].setErrors({ invalid: true });
      return false;

    } else {

      return true;

    }
  }

  /**
   * Validating that the given date is overlaping with the related academic calendar terms
   */
  isBetweenDates() {

    let startDate = new Date(this.fg.controls['startDate'].value);
    let endDate = new Date(this.fg.controls['endDate'].value);
    let acpdStartDate = new Date(this.academicCalendarProgramDetail.startDate);
    let acpdEndDate = new Date(this.academicCalendarProgramDetail.endDate);
    // for validating academic calendar program detail
    if (startDate < acpdStartDate) {
      this.fg.controls['startDate'].setErrors({ notIn: true });
      return true;

    }
    if (endDate > acpdEndDate) {
      this.fg.controls['endDate'].setErrors({ notIn: true });
      return true;
    }
    if (this.cacTerms && this.cacTerms.length) {

      for (let i = 0; i < this.cacTerms.length; i++) {

        let termStartDate = new Date(this.cacTerms[i].startDate);
        let termEndDate = new Date(this.cacTerms[i].endDate);

        if (this.cacTerm) {
          if (startDate >= termStartDate && startDate <= termEndDate && this.cacTerms[i].id !== this.cacTerm.id) {

            this.fg.controls['startDate'].setErrors({ invalid: true });
            return true;

          } if (endDate <= termEndDate && endDate >= termStartDate && this.cacTerms[i].id !== this.cacTerm.id) {
            this.fg.controls['endDate'].setErrors({ invalid: true });
            return true;
          }
        } else {
          if (startDate >= termStartDate && startDate <= termEndDate) {

            this.fg.controls['startDate'].setErrors({ invalid: true });
            return true;

          } if (endDate <= termEndDate && endDate >= termStartDate) {
            this.fg.controls['endDate'].setErrors({ invalid: true });
            return true;
          }
        }

      }
    }


  }
}
