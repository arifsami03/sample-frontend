import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from '../../../../shared/helper';
import { GLOBALS } from '../../../../app/config/globals';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { CACTermFormComponent } from '../form/cac-term-form.component';
import { CACTermService, AcademicCalendarProgramDetailService } from '../../../services';
import { CACTermModel, ProgramDetailsModel, AcademicCalendarProgramDetailModel } from '../../../models';
import { CampusAcademicCalendarService } from '../../../../institute/services';
import { CampusAcademicCalendarModel } from '../../../../institute/models';

@Component({
  selector: 'academic-cac-term-view',
  templateUrl: './cac-term-view.component.html',
  styleUrls: ['./cac-term-view.component.css'],
  animations: [PageAnimation],
  providers: [CampusAcademicCalendarService]
})
export class CACTermViewComponent implements OnInit {
  public pageState = 'active';

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public boxLoaded: boolean = true;

  public fg: FormGroup;
  public cacTerm: CACTermModel;
  public options: Observable<string[]>;
  public componentLabels = CACTermModel.attributesLabels;
  public cacTerms: CACTermModel[];
  public academicCalendarProgramDetail: AcademicCalendarProgramDetailModel;
  public campusAcademicCalendars: CampusAcademicCalendarModel[];

  public attrLabels = ProgramDetailsModel.attributesLabels;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private cacTermService: CACTermService,
    private academicCalendarProgramDetailService: AcademicCalendarProgramDetailService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private campusAcademicCalendarService: CampusAcademicCalendarService
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  /**
   * initialize page
   */
  private initializePage() {


    this.fg = this.fb.group(new CACTermModel().validationRules());
    // finding the academic calendar terms by ACPD
    this.findAttributesACPD();

    // find campuses that are mapped on acpd
    this.findCampusByACPD();
  }

  /**
   * Get find Academic Calendar PRogram Detail
   */
  private findAttributesACPD() {

    this.route.params.subscribe(params => {

      this.academicCalendarProgramDetailService.findAttributes(params['id']).subscribe(

        response => {
          this.academicCalendarProgramDetail = response;
          //TODO:low need to check why we are dowing this after fetching data
          this.initViewPage();

        },
        error => {

          console.log(error);
          this.loaded = true;

        },
        () => {

          this.loaded = true;

        }
      );
    });
  }

  /**
   * Get find Academic Calendar PRogram Detail
   */
  private findCampusByACPD() {

    this.route.params.subscribe(params => {

      this.campusAcademicCalendarService.findCampusByACPD(params['id']).subscribe(

        response => {

          this.campusAcademicCalendars = response;

        },

        error => {

          console.log(error);

        },

        () => { }
      );
    });
  }

  /**
   * initialize the view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Academic Calendar Term ',
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Academic Calendar Term ',
          url: '/academic/cacTerms'
        },
        {
          label:
            this.academicCalendarProgramDetail.academicCalendar.title +
            '-' +
            this.academicCalendarProgramDetail.programDetail['program']['name'] +
            '-' +
            this.academicCalendarProgramDetail.programDetail.name
        }
      ]
    });
  }

  // Opening the dialogue box to add
  addCACTermClick() {

    const dialogRef = this.matDialog.open(CACTermFormComponent, {

      width: '1000px',
      data: { type: 'Add Academic Calendar Term', academicCalendarProgramDetail: this.academicCalendarProgramDetail }

    });

    dialogRef.afterClosed().subscribe(() => {

      this.findAttributesACPD();

    });
  }

  // Opening the dialogue box to update
  editClick(id: number) {

    const dialogRef = this.matDialog.open(CACTermFormComponent, {
      width: '1000px',

      data: {  type: 'Update Academic Calendar Term', id: id, academicCalendarProgramDetail: this.academicCalendarProgramDetail }

    });

    dialogRef.afterClosed().subscribe(() => {

      this.findAttributesACPD();

    });
  }

  // Opening dialogue box to delete
  deleteCACTerm(id: number) {

    this.matDialog
      .open(ConfirmDialogComponent, {

        width: GLOBALS.deleteDialog.width,
        data: { message: 'Are you Sure?' }

      })
      .afterClosed()
      .subscribe((accept: boolean) => {

        if (accept) {

          this.loaded = false;

          this.cacTermService.delete(id).subscribe(

            () => {

              this.layoutService.flashMsg({ msg: 'Academic Calendar Term has been deleted.', msgType: 'success' });
              this.findAttributesACPD();

            },

            error => {

              console.log(error);
              this.loaded = true;
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });

            },

            () => {

              this.loaded = true;

            }
          );
        }
      });
  }
}
