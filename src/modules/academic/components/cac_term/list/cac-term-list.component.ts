import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { PageAnimation } from '../../../../shared/helper';
import { GLOBALS } from '../../../../app/config/globals';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { LayoutService } from '../../../../layout/services';
import { CACTermService } from '../../../services';
import { CACTermModel } from '../../../models';

@Component({
  selector: 'academic-cac-term-list',
  templateUrl: './cac-term-list.component.html',
  styleUrls: ['./cac-term-list.component.css'],
  animations: [PageAnimation]
})
export class CACTermListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public data: CACTermModel[] = [new CACTermModel()];

  public attrLabels = CACTermModel.attributesLabels;

  displayedColumns = ['acpd', 'cacTermsCount', 'cacCount', 'options'];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  constructor(private cacTermService: CACTermService, public dialog: MatDialog, public matDialog: MatDialog, private router: Router, public layoutService: LayoutService) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Academic Calendar Term' });

    this.getRecords();
  }

  /**
   * Get data/records from backend
   */
  getRecords() {

    this.cacTermService.index().subscribe(

      response => {

        if (response) {

          this.data = response;
          // mapping the ncademic calendar program detail on full name attributes
          this.data.map(element => {

            element.fullName = element.academicCalendar.title + '-' + element.programDetail['program']['name'] + '-' +
              element.programDetail.name;
              element.cacCount = element.campusAcademicCalendars.length;
              element.cacTermsCount = element.cacTerms.length;

          })

          this.dataSource = new MatTableDataSource<CACTermModel>(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      error => {

        console.log(error);
        this.loaded = true;
        this.pageState = 'active';

      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {

    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;

  }


  /**
   * will sort data on angular data grid
   */
  sortData() {
    this.dataSource.sort = this.sort;
  }
}
