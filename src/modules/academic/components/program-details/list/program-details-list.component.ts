import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { PageAnimation } from 'modules/shared/helper';
import { LayoutService } from 'modules/layout/services';
import { ProgramDetailsService } from 'modules/academic/services';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { ProgramDetailsModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'academic-program-list',
  templateUrl: './program-details-list.component.html',
  styleUrls: ['./program-details-list.component.css'],
  animations: [PageAnimation]
})
export class ProgramDetailsListComponent implements OnInit {

  public pageState;
  public loaded: boolean = false;

  public dtConf = GLOBALS.dataTable;
  public data: ProgramDetailsModel[] = [new ProgramDetailsModel()];
  public attrLabels = ProgramDetailsModel.attributesLabels;

  displayedColumns = [
    // 'instituteTypeId',
    'educationLevelId', 'programId', 'name', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    private programDetailService: ProgramDetailsService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) { }

  /**
   * initialize the page of programs list
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Program Details' });
    this.getRecords();
  }

  /**
   * get all records
   */
  getRecords(): void {
    this.programDetailService.index().subscribe(
      response => {
        this.data = response;
        // mapping for listing
        this.data.map(element => {
          element.instituteName = element.program.educationLevel.instituteType.name;
          element.educationLevelName = element.program.educationLevel.education;
          element.programName = element.program.name;
        });
        this.dataSource = new MatTableDataSource<ProgramDetailsModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  /**
   * change alphabets into lowercase
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * delete data against the id given
   */
  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.programDetailService.delete(id).subscribe(
          response => {

            if (response && (response['children']) ? response['children'].length > 0 : false) {

              this.loaded = true;
              this.pageState = 'active';

              this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

            } else {

              this.layoutService.flashMsg({ msg: 'Program Detail has been deleted.', msgType: 'success' });
              this.getRecords();

            }

          }
          , error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
            this.pageState = 'active';
          },
          () => {
            this.loaded = true;
            this.pageState = 'active';
          }
        );
      }
    });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }
}
