import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from 'modules/shared/helper';
import { ProgramService, ProgramDetailsService } from 'modules/academic/services';
import { LayoutService } from 'modules/layout/services';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { ProgramModel, ProgramDetailsModel } from 'modules/academic/models';
import { GLOBALS } from 'modules/app/config/globals';
import { EducationLevelModel } from 'modules/institute/models/index';

@Component({
  selector: 'academic-program-details-form',
  templateUrl: './program-details-form.component.html',
  styleUrls: ['./program-details-form.component.css'],
  animations: [PageAnimation]
})
export class ProgramDetailsFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public boxLoaded = true;

  // success and Error
  public success: Boolean;
  public successMessage: String;
  public errorMessage: String;
  public error: Boolean;
  public programDetail: ProgramDetailsModel;
  public programs: ProgramModel[];
  public fg: FormGroup;
  public pageAct: string;
  public options: Observable<string[]>;
  public check = true;

  public componentLabels = ProgramDetailsModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private programService: ProgramService,
    private programDetailsService: ProgramDetailsService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * it will initialize the program form page
   */
  private initializePage() {
    this.fg = this.fb.group(new ProgramDetailsModel().validationRules());
    this.findPrograms();
    if (this.pageAct === 'add') {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get Data
   */
  private getData() {
    this.route.params.subscribe(params => {

      this.programDetailsService.find(+params['id']).subscribe(response => {

        if (response) {
          this.programDetail = response['data'];
          // display of Succes and Error Message
          this.success = true;
          this.successMessage = response.success;
          this.fg.patchValue(this.programDetail);
        } else {
          this.error = true;
          this.errorMessage = response.error;
        }
        if (!this.error) {
          if (this.pageAct === 'view') {
            this.initViewPage();
          } else if (this.pageAct === 'update') {
            this.initUpdatePage();
          }
        }
      },

        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    });
  }

  findPrograms() {
    this.programService.getProgramsList().subscribe(
      response => {
        this.programs = response;
      },
      error => {
        console.log(error);

      }, () => { }
    );
  }



  /**
   * initialize page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Create Program Details' });
    this.programDetail = new ProgramDetailsModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * initialize view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Program Detail: ' + this.programDetail.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Program Detail', url: '/academic/programDetails' },
        { label: this.programDetail.name }
      ]
    });
  }

  /**
   * initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Program Detail: ' + this.programDetail.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Program Detail', url: '/academic/programDetails' },
        {
          label: this.programDetail.name,
          url: `/academic/programDetails/view/${this.programDetail.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * add or update when save button is clicked
   */
  public saveData(item: ProgramModel) {
    this.boxLoaded = false;

    item.abbreviation = item.abbreviation.toUpperCase();

    let id;
    if (this.pageAct === 'add') {
      item.id = null;
      this.programDetailsService.create(item).subscribe(
        response => {
          if (response) {
            id = response.data.id;
            // display of Succes and Error Message
            this.success = true;
            this.successMessage = response.success;
            this.layoutService.flashMsg({ msg: 'Program Detail has been created.', msgType: 'success' });
            this.router.navigate([`/academic/programDetails`]);
          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
        },

        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        }, () => {
          this.loaded = true;
          this.boxLoaded = true;
        }
      );
    } else if (this.pageAct === 'update') {
      this.programDetailsService.update(this.programDetail.id, item).subscribe(
        response => {
          if (response) {
            this.success = true;
            this.successMessage = response.success;
            this.layoutService.flashMsg({ msg: 'Program Detail has been updated.', msgType: 'success' });
            this.router.navigate([
              `/academic/programDetails`
            ]);
          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
          this.boxLoaded = true;
        }
      );
    }
  }

  /**
   * delete the selected program
   */
  delete(id: number) {

    const dialogRef = this.matDialog.open(ConfirmDialogComponent,
      { width: GLOBALS.deleteDialog.width, data: { message: GLOBALS.deleteDialog.message } }
    );

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';
        this.programDetailsService.delete(id).subscribe(response => {

          if (response && (response['children']) ? response['children'].length > 0 : false) {

            this.loaded = true;
            this.pageState = 'active';

            this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

          } else {

            this.layoutService.flashMsg({ msg: 'Program Detail has been deleted.', msgType: 'success' });
            this.router.navigate(['institute/programDetails']);

          }
        }
          ,
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
            this.pageState = 'active';
          }, () => {
            this.loaded = true;
            this.pageState = 'active';
          }
        );
      }
    });
  }

}
