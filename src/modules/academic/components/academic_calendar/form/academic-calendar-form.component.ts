import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from 'modules/shared/helper';
import { GLOBALS } from 'modules/app/config/globals';
import { AcademicCalendarService, ProgramDetailsService,ActivityService } from 'modules/academic/services';
import { LayoutService } from 'modules/layout/services';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { AcademicCalendarModel, ProgramDetailsModel,ActivityModel } from 'modules/academic/models';

@Component({
  selector: 'academic-academic-calendar-form',
  templateUrl: './academic-calendar-form.component.html',
  styleUrls: ['./academic-calendar-form.component.css'],
  animations: [PageAnimation]
})
export class AcademicCalendarFormComponent implements OnInit {

  public pageState = 'active';

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public boxLoaded: boolean = true;

  public fg: FormGroup;
  public pageAct: string;
  public academicCalendar: AcademicCalendarModel;
  public options: Observable<string[]>;
  public componentLabels = AcademicCalendarModel.attributesLabels;

  public acProgramDetailIds: number[] = [];

  public activities : ActivityModel[];

  public selectedActivities : number[]=[];

  /**
   * for Program details list
   */
  public data: ProgramDetailsModel[] = [new ProgramDetailsModel()];

  public attrLabels = ProgramDetailsModel.attributesLabels;

  displayedColumns = ['programId', 'name', , 'options'];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private academicCalendarService: AcademicCalendarService,
    private programDetailService: ProgramDetailsService,
    private activityService: ActivityService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.findAttributesList();
    this.findActivitiesAttributesList();
  }

  /**
   * get all records for program details
   */
  findAttributesList(): void {
    this.programDetailService.findAttributesList().subscribe(
      response => {
        this.data = response;
        // select existing id on check
        // this.isCheckBoxesChecked();
        this.dataSource = new MatTableDataSource<ProgramDetailsModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.initializePage();

        // this.loaded = true;
      }
    );
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new AcademicCalendarModel().validationRules(),{ activitiesList: this.fb.array([]) });
    this.addActivitiesClick();

    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get record
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.academicCalendarService.find(params['id']).subscribe(
        response => {
          if (response) {
            this.academicCalendar = response;
            console.log('AC : ', this.academicCalendar);
            this.patchingActivitiesValue();
            this.fg.patchValue(this.academicCalendar);
            //TODO:low need to check why we are dowing this after fetching data
            if (this.pageAct == this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct == this.pageActions.update) {
              // pushing ids to acProgramDetailIds

              let academicCalendarProgramDetailsIds: any = this.academicCalendar['academicCalendarProgramDetails'];
              academicCalendarProgramDetailsIds.forEach(element => {
                let selectedProgramDetail = this.data.find(item => {
                  return item.id == element['programDetailId'];
                });
                if (selectedProgramDetail) {
                  selectedProgramDetail['status'] = true;
                }
                this.acProgramDetailIds.push(element['programDetailId']);
              });



              this.initUpdatePage();
            }
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Academic Calendar: Create' });

    this.academicCalendar = new AcademicCalendarModel();

    this.fg.enable();

    this.loaded = true;
  }

  /**
   * initialize the view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Academic Calendar: ' + this.academicCalendar.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Academic Calendar ',
          url: '/academic/academicCalendars'
        },
        { label: this.academicCalendar.title }
      ]
    });
  }

  /**
   * initialize the update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Academic Calendar: ' + this.academicCalendar.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Academic Calendar',
          url: '/academic/academicCalendars'
        },
        {
          label: this.academicCalendar.title,
          url: `/academic/academicCalendars/view/${this.academicCalendar.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: AcademicCalendarModel) {
    this.boxLoaded = false;


console.log('Item : ', item);
    item.abbreviation = item.abbreviation.toUpperCase();

    item['programDetailId'] = this.acProgramDetailIds;

    if (this.pageAct === this.pageActions.create) {
      this.academicCalendarService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Academic Calendar has been created.', msgType: 'success' });
          this.router.navigate([`/academic/academicCalendars/view/${response.id}`]);
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        }, () => {
          this.boxLoaded = true;
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.academicCalendarService.update(this.academicCalendar.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Academic Calendar has been updated.', msgType: 'success' });
          this.router.navigate([`/academic/academicCalendars/view/${response.id}`]);
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        }, () => {
          this.boxLoaded = true;
        }
      );
    }
  }

  checkboxClicked(event: any, programDetailId) {
    if (event.checked) {
      this.acProgramDetailIds.push(programDetailId);
    } else {
      const index = this.acProgramDetailIds.indexOf(programDetailId);

      if (index > -1) {
        this.acProgramDetailIds.splice(index, 1);
      }
    }
  }

  /**
   * Delete record
   *
   */
  delete(id: number) {
    this.loaded = false;
    this.pageState = '';
    this.academicCalendarService.delete(id).subscribe(
      response => {

        if (response && (response['children']) ? response['children'].length > 0 : false) {

          this.loaded = true;
          this.pageState = 'active';

          this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

        } else {

          this.layoutService.flashMsg({ msg: 'Academic Calendar has been deleted.', msgType: 'success' });
          this.router.navigate(['/academic/academicCalendars']);

        }
      },
      error => {
        this.loaded = true;
        this.pageState = 'active';
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        console.log(error);
      }
    );
  }

  isCheckBoxesChecked() {
    if (this.data) {
      if (this.academicCalendar) {
        let academicCalendarProgramDetailsArray: any = this.academicCalendar['academicCalendarProgramDetails'];
        if (this.academicCalendar['academicCalendarProgramDetails']) {
          for (let i = 0; i < this.data.length; i++) {
            academicCalendarProgramDetailsArray.forEach(acpd_element => {
              if (this.data[i].id === acpd_element.programDetailId) {
                this.data[i]['status'] = true;
              }
            });
          }
        }
      }
    }
  }
  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

 /**
   * get all records for program details
   */
  findActivitiesAttributesList(): void {
    this.activityService.findAttributesList().subscribe(
      response => {
        this.activities = response;
        console.log('Activities : ', this.activities)
        
      },
      error => {
        console.log(error);
      },
      () => {
      }
    );
  }

  


  createActivityItem(): FormGroup {
    return this.fb.group({
      activityId: new FormControl({ value: '', disabled: false })
    });
  }

  addActivitiesClick(): void {
  
    let activitiesList = this.fg.get('activitiesList') as FormArray;
    activitiesList.push(this.createActivityItem());

    // for disabling if courses already in  list
    if (activitiesList.value[activitiesList.value.length - 2]) {
      let value = activitiesList.value[activitiesList.value.length - 2].activityId;
      const index = this.selectedActivities.indexOf(value);
      if (index < 0 && value) {
        this.selectedActivities.push(value);
      }
    }
    console.log('Add Count : ', this.selectedActivities);
  }
 

  removeActivities(i: number, item) {
    console.log('Remove Count')
    let control = <FormArray>this.fg.controls['activitiesList'];
    if (control.length !== 1) {
      //removing from selected arary of activities List
      const index = this.selectedActivities.indexOf(control.value[i].activityId);
      if (index > -1) {
        this.selectedActivities.splice(index, 1);
      }
    
      control.removeAt(i);
    } else {
      //removing from selected arary of activities List
      const index = this.selectedActivities.indexOf(control.value[i].activityId);
      if (index > -1) {
        this.selectedActivities.splice(index, 1);
      }
      control.removeAt(i);
      this.addActivitiesClick();
    }
  }

  patchingActivitiesValue() {

    let preReqCoursesList = this.fg.get('activitiesList') as FormArray;
    let saveIndex;
    for (let i = 0; i < this.academicCalendar['activitiesList'].length - 1; i++) {
      // pushing into array of selected pre req list
      this.selectedActivities.push(this.academicCalendar['activitiesList'][i].activityId);
      preReqCoursesList.push(this.createActivityItem());
      saveIndex = i;
    }
    if (this.academicCalendar['activitiesList'][saveIndex+1]) {
      this.selectedActivities.push(this.academicCalendar['activitiesList'][saveIndex+1].activityId);
    }
  }
}
