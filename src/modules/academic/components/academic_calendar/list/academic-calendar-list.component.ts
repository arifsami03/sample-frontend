import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { PageAnimation } from 'modules/shared/helper';
import { GLOBALS } from 'modules/app/config/globals';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { LayoutService } from 'modules/layout/services';
import { AcademicCalendarService } from 'modules/academic/services';
import { AcademicCalendarModel } from 'modules/academic/models';

@Component({
  selector: 'academic-academic-calendar-list',
  templateUrl: './academic-calendar-list.component.html',
  styleUrls: ['./academic-calendar-list.component.css'],
  animations: [PageAnimation]
})
export class AcademicCalendarListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public data: AcademicCalendarModel[] = [new AcademicCalendarModel()];

  public attrLabels = AcademicCalendarModel.attributesLabels;

  displayedColumns = ['title', 'abbreviation', 'options'];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(private academicCalendarService: AcademicCalendarService, public dialog: MatDialog, public matDialog: MatDialog, private router: Router, public layoutService: LayoutService) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Academic Calendar' });

    this.getRecords();
  }

  /**
   * Get data/records from backend
   */
  getRecords() {
    this.academicCalendarService.index().subscribe(
      response => {
        if (response) {
          this.data = response;
          this.dataSource = new MatTableDataSource<AcademicCalendarModel>(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete record
   */
  delete(id: number) {
    // Confirm dialog
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;
          this.pageState = '';

          this.academicCalendarService.delete(id).subscribe(
            response => {

              if (response && (response['children']) ? response['children'].length > 0 : false) {

                this.loaded = true;
                this.pageState = 'active';

                this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

              } else {

                this.layoutService.flashMsg({ msg: 'Academic Calendar has been deleted.', msgType: 'success' });
                this.getRecords();

              }


            },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            });
        }
      });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }


}
