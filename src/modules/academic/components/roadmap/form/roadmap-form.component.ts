import { Component, OnInit } from '@angular/core';
import { MatDialog, transformMenu } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { PageAnimation } from '../../../../shared/helper';
import { RoadMapService, ProgramDetailsService, AcademicCalendarService, AcademicCalendarProgramDetailService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { RoadMapModel, ProgramDetailsModel, AcademicCalendarModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'academic-roadmap-form',
  templateUrl: './roadmap-form.component.html',
  styleUrls: ['./roadmap-form.component.css'],
  animations: [PageAnimation]
})
export class RoadMapFormComponent implements OnInit {

  public pageState = 'active';
  
  public loaded: boolean = false;
  public boxLoaded = true;
  public pageActions = GLOBALS.pageActions;

  public error: Boolean;

  public fg: FormGroup;
  public pageAct: string;
  public roadMap: RoadMapModel;
  public id: number;
  public componentLabels = RoadMapModel.attributesLabels;

  public programDetails: ProgramDetailsModel[] = [];
  public academicYears: number[] = GLOBALS.academicYears;
  public categories: any[] = [GLOBALS.RMCategories.fall,GLOBALS.RMCategories.spring];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private roadMapService: RoadMapService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private programDetailsService: ProgramDetailsService,
     private academicCalendarProgramDetailService: AcademicCalendarProgramDetailService,
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    this.findProgramDetailsAttributesList();
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new RoadMapModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * get all data
   */
  /**
   * Get record
   */
  private getData() {
    this.loaded = false;
    this.route.params.subscribe(params => {
      this.roadMapService.find(params['id']).subscribe(
        response => {
          if (response) {
            this.roadMap = response;
            console.log('roadMap : ', this.roadMap);
            this.id = this.roadMap.id;
            this.fg.patchValue(this.roadMap);
            //TODO:low need to check why we are dowing this after fetching data
            if (this.pageAct == this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct == this.pageActions.update) {
              this.initUpdatePage();
            }
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {}
      );
    });
  }

  /**
   * initialize create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Roadmap: Create' });
    this.roadMap = new RoadMapModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * initialize view page
   */
  private initViewPage() {
    this.fg.disable();
    this.layoutService.setPageTitle({
      title: 'Roadmap: ' + this.roadMap.title,
      breadCrumbs: [{ label: 'Home', url: '/home/dashboard' }, { label: 'Roadmap ', url: '/academic/roadMaps' }, { label: this.roadMap.title }]
    });
  }

  /**
   * initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Roadmap: ' + this.roadMap.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Roadmap', url: '/academic/roadMaps' },
        {
          label: this.roadMap.title,
          url: `/academic/roadMaps/view/${this.roadMap.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Add or update data in database when save button is clicked
   */

  saveData(item: RoadMapModel) {
    this.boxLoaded = false;

    if (this.pageAct === this.pageActions.create) {
      this.roadMapService.create(item).subscribe(
        response => {
          this.router.navigate([`/academic/roadMaps/view/${response.id}`]);
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.boxLoaded = true;
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.roadMapService.update(this.roadMap.id, item).subscribe(
        response => {
          this.router.navigate([`/academic/roadMaps/view/${this.roadMap.id}`]);
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.boxLoaded = true;
        }
      );
    }
  }

  /**
   * delete record
   */
  deleteRecord(id: string) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: 'Are you Sure, You want to delete?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.roadMapService.delete(+id).subscribe(
          response => {
            this.router.navigate(['/academic/roadMap']);
          },
          error => {
            console.log(error);
            this.loaded = true;
          }
        );
      }
    });
  }
  findProgramDetailsAttributesList() {
    this.programDetailsService.findAttributesList().subscribe(
      response => {
        this.programDetails = response;
      },
      error => console.log(error),
      () => {
        this.loaded = true;
      }
    );
  }
 
}
