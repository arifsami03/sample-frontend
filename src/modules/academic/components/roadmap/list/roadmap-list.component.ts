import { map } from 'rxjs/operators/map';
import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { PageAnimation } from '../../../../shared/helper';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { LayoutService } from '../../../../layout/services';
import { RoadMapService } from '../../../services';
import { RoadMapModel, RoadMapCourseModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'academic-roadmap-list',
  templateUrl: './roadmap-list.component.html',
  styleUrls: ['./roadmap-list.component.css'],
  animations: [PageAnimation]
})
export class RoadMapListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public data: RoadMapModel[] = [new RoadMapModel()];
  public attrLabels = RoadMapModel.attributesLabels;

  displayedColumns = ['title', 'programDetail', 'numberOfCourses', 'totalCreditHours', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public error: Boolean;

  constructor(private roadMapService: RoadMapService, public dialog: MatDialog, public matDialog: MatDialog, private router: Router, public layoutService: LayoutService) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Roadmap' });
    this.getRecords();
  }

  /**
   * Get records
   */
  getRecords() {
    this.roadMapService.index().subscribe(
      response => {
        this.data = response;
        this.data.map(element => {
          element.programDetailName = element.programDetail.name;
          element.numberOfCourses = element.roadMapCourses.length
          element.totalCreditHours = this.countTotalCreditHourse(element.roadMapCourses);
        })
        this.dataSource = new MatTableDataSource<RoadMapModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
    return this.data;
  }

  /**
   * Apply filter and serch in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete record
   */
  deleteRoadMap(id: number) {
    // Confirm dialog
    this.matDialog
      .open(ConfirmDialogComponent, { width: GLOBALS.deleteDialog.width, data: { message: 'Are you Sure, You want to delete?' } })
      .afterClosed().subscribe((accept: boolean) => {

        if (accept) {

          this.loaded = false;
          this.pageState = '';

          this.roadMapService.delete(+id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Roadmap has been deleted.', msgType: 'success' });
              this.getRecords();
            },
            error => {
              console.log(error);
              this.loaded = true;
              this.pageState = 'active';
            }
          );
        }
      });
  }

  sortData() {
    this.dataSource.sort = this.sort;
  }

  countTotalCreditHourse(roadMapCourses: RoadMapCourseModel[]): number {
    let total: number = 0;
    if (roadMapCourses) {
      roadMapCourses.forEach(roadMapCourse => {
        total = total + roadMapCourse.creditHours
      });
    }
    return total;

  }

}
