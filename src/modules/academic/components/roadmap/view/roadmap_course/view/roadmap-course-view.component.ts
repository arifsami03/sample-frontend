import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator, MatDialog, MatSort } from '@angular/material';

import { RoadMapCourseService } from '../../../../../services';
import { LayoutService } from '../../../../../../layout/services';
import { RoadMapCourseFormComponent } from '../form/roadmap-course-form.component';
import { ConfirmDialogComponent } from '../../../../../../shared/components';

import { RoadMapCourseModel, RoadMapModel } from '../../../../../models';
import { GLOBALS } from '../../../../../../app/config/globals';
// interface ISemesterSummary {
//   numberOfCourses: number;
//   creditHours: number;
// }
@Component({
  selector: 'academic-roadmap-course-view',
  templateUrl: './roadmap-course-view.component.html',
  styleUrls: ['./roadmap-course-view.component.css']
})
export class RoadMapCourseViewComponent implements OnInit {
  public loaded: boolean = false;
  public pageActions = GLOBALS.pageActions;

  public error: Boolean;
  displayedColumns = ['courseCode', 'options'];
  public fg: FormGroup;
  public pageAct: string;
  public roadMapCourses: RoadMapCourseModel[];
  public id: string;
  public componentLabels = RoadMapCourseModel.attributesLabels;
  @Input() roadMap: RoadMapModel;
  public courseTypes = GLOBALS.courseTypes;
  public roadMapCoursesIds: number[];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private roadMapCourseService: RoadMapCourseService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.getData();
  }

  /**
   * get all data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.roadMapCourseService.findByRoadMap(this.roadMap.id).subscribe(
        response => {
          this.roadMapCourses = response;
          this.prepareSemesterSummary();
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  addCourseClick() {
    const dialogRef = this.matDialog.open(RoadMapCourseFormComponent, {
      width: '1200px',
      data: { roadMap: this.roadMap, type: 'Add Course', roadMapCourses: this.roadMapCourses, roadMapCoursesIds: this.roadMapCoursesIds }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }
  editClick(id: number) {
    const dialogRef = this.matDialog.open(RoadMapCourseFormComponent, {
      width: '1200px',
      data: { roadMap: this.roadMap, type: 'Update Course : ', id: id, roadMapCourses: this.roadMapCourses, roadMapCoursesIds: this.roadMapCoursesIds }
    });
    dialogRef.afterClosed().subscribe(result => {
      // if (result && result.response) {
      this.getData();
      //}
    });
  }

  deleteCourse(id: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: 'Are you Sure?' }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          this.roadMapCourseService.delete(id).subscribe(
            response => {
              this.getData();
              // this.stateFlowService.reloadCourseFlow(true);
            },
            error => {
              console.log(error);
              this.loaded = true;
            },
            () => {
              this.loaded = true;
            }
          );
        }
      });
  }
  /**
   * prepare the summary of each semester counting credit hours and saving road map courses Ids
   */
  private prepareSemesterSummary() {
    this.roadMapCoursesIds = [];
    for (let i = 0; i < this.roadMapCourses.length; i++) {
      let creditHoursInSemester = 0;

      this.roadMapCourses[i]['roadMapCourses'].forEach(roadMapCourse => {
        creditHoursInSemester = creditHoursInSemester + roadMapCourse['creditHours'];
        // saving RoadMap Courses Ids
        // validating if courseId already exitst then not added in array
        const index: number = this.roadMapCoursesIds.indexOf(roadMapCourse['courseId']);
        if (index === -1) {
          this.roadMapCoursesIds.push(roadMapCourse['courseId']);
        }
      });
      this.roadMapCourses[i]['roadMapCourses']['creditHours'] = creditHoursInSemester;
    }
  }
}
