import { ActivatedRoute } from '@angular/router';
import { Component, Inject, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';

import * as _ from 'lodash';
import { Promise } from 'bluebird';

import { RoadMapCourseService, ClassesService, CourseService, RoadMapService } from '../../../../../services';
import { RoadMapCourseModel, ClassesModel, CourseModel, RoadMapModel } from '../../../../../models';
import { GLOBALS } from '../../../../../../app/config/globals';
import { ConfigurationModel } from '../../../../../../shared/models';
import { ConfigurationService } from '../../../../../../shared/services';
import { AlertDialogComponent } from '../../../../../../shared/components';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

interface IValidationErrors {
  code: string;
  msg: string;
}
@Component({
  selector: 'academic-roadmap-course-form',
  templateUrl: './roadmap-course-form.component.html',
  styleUrls: ['./roadmap-course-form.component.css']
})
export class RoadMapCourseFormComponent {


  // For Course AUtoComplete

  courseId = new FormControl('', [Validators.required]);
  courseTitle: string;
  filteredOptions: Observable<string[]>;

  public loaded: boolean = false;
  public roadMapCourse: RoadMapCourseModel;
  public fg: FormGroup;
  public state: string;
  public componentLabels = RoadMapCourseModel.attributesLabels;
  public validationErrors: IValidationErrors[] = [];
  /**
   * Models
   */
  public classes: ClassesModel[] = [];
  public courses: CourseModel[] = [];
  public roadMaps: RoadMapModel[] = [];

  public courseAbbr: string;
  public selectedCourse: number;
  public selectedPreRequisiteCourse: number;
  public oldValue: number;
  /**
   * Data From Globals
   */
  private natureOfCourseKey = GLOBALS.configurationKeys.natureOfCourse;
  private maxCreditHourKey = GLOBALS.configurationKeys.maxCreditHour;
  public natureOfCourses: ConfigurationModel[];
  public maxCreditHour: number;
  public courseTypes: any[] = [GLOBALS.courseTypes.theoratical, GLOBALS.courseTypes.practical];
  public creditHours: any[] = GLOBALS.creditHours;

  public roadMapCourses: RoadMapCourseModel[];
  public preRequisiteCourses: CourseModel[];
  public selectedPreRequisiteCourses: number[] = [];

  private roadMapCoursesIds: number[];
  public semester: RoadMapCourseModel[] = [];
  public selectedSemester: ClassesModel;
  public roadMap: RoadMapModel;

  constructor(
    private roadMapCourseService: RoadMapCourseService,
    public configurationService: ConfigurationService,
    private classesService: ClassesService,
    private courseService: CourseService,
    private roadMapService: RoadMapService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    public matDialog: MatDialog,

    public dialogRef: MatDialogRef<RoadMapCourseFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.roadMapCourses = this.data['roadMapCourses'];
    this.roadMapCoursesIds = this.data['roadMapCoursesIds'];
    this.roadMap = this.data['roadMap'];
    // this.loaded = true;
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    
    let promissAry = [];

    promissAry.push(this.getNatureOfCourseList(this.natureOfCourseKey));
    promissAry.push(this.getMaxCreditHour(this.maxCreditHourKey));
    promissAry.push(this.findClassesByProgramDetails());
    promissAry.push(this.findRoadMapAttributesList());
    promissAry.push(this.findCourses());

    if (this.data.type != 'Add Course') {
      promissAry.push(this.getCourseData());
    } else {
      this.addCoursesClick();
      // finding courses that are not already added in road map
      promissAry.push(this.findRoadMapCourses());
      promissAry.push(this.findMaxCourseOrder(this.roadMap.id));
    }

    Promise.all(promissAry).then( () => {
      this.loaded = true;
    });

  }

  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new RoadMapCourseModel().validationRules(), { preReqCoursesList: this.fb.array([]) });
    
  }
  /**
   *    For Course Auto Complete
   */

  private _filter(value: string): any[] {
    if (this.courses) {
      if (value) {
        return this.courses.filter(option => option.fullName.toLowerCase().indexOf(value.toLowerCase()) === 0);
      } else {
        return this.courses.filter(option => option.fullName.toLowerCase().indexOf(value.toLowerCase()));
      }
    }
  }
  private filteredOptionForCourseAutoComplete() {
    this.filteredOptions = this.courseId.valueChanges
      .pipe(
        startWith<string | CourseModel>(''),
        map(value => typeof value === 'string' ? value : value.fullName),
        map(fullName => fullName ? this._filter(fullName) : this.courses.slice())
      );
  }
  displayFn(_courseId: number) {

    let _course: CourseModel;
    if (_courseId) {
      _course = _.find(this.courses, { id: _courseId });
    }
    return _course ? _course.fullName : undefined;
  }

  getDisplayFn() {

    return (val) => {

      return this.displayFn(val)
    };
  }



  public getNatureOfCourseList(key: string) {

    return new Promise((resolve, reject) => {
      this.configurationService.findAttributesList(key).subscribe(
        response => {
          this.natureOfCourses = response;
          this.sortNatureOfCourses();
          resolve(true);
        },
        error => {
          console.log(error);
          reject(error);
        }
      );

    })


  }
  public getMaxCreditHour(key: string) {

    return new Promise((resolve, reject) => {
      this.configurationService.findAttributesList(key).subscribe(
        response => {
          if (response) {
            this.maxCreditHour = +response[0].value;
          }
          resolve(true);
        },
        error => {
          console.log(error);
          reject(error);
        }
      );
    })

  }


  private sortNatureOfCourses() {

    this.natureOfCourses.map(element => {
      element['configurationOption'] = element.configurationOptions[0];
    })
    this.natureOfCourses = _.sortBy(this.natureOfCourses, [function (o) { return +o.configurationOption.value; }])
  }


  public findMaxCourseOrder(roadMapId: number) {

    return new Promise((resolve, reject) => {

      this.roadMapCourseService.findMaxCourseOrder(roadMapId).subscribe(response => {
        let maxCourseOrder = response + 1;
        this.fg.get('courseOrder').patchValue(maxCourseOrder);
        resolve(true);
      },
        error => {
          console.log(error);
          reject(error)
        }
      );

    })

  }


  getCourseData() {

    return new Promise((resolve, reject) => {

      this.roadMapCourseService.find(this.data.id).subscribe(response => {
        this.roadMapCourse = response;
        // removing the current element
        const index: number = this.roadMapCoursesIds.indexOf(this.roadMapCourse.courseId);
        if (index !== -1) {
          this.roadMapCoursesIds.splice(index, 1);
        }
        
        // for showing the semester info
        this.showSemesterCoursesList(this.roadMapCourse.classId);

        // let saveIndex;
        // for (let i = 0; i < this.roadMapCourse['preReqCoursesList'].length - 1; i++) {
        //   // pushing into array of selected pre req list
        //   this.selectedPreRequisiteCourses.push(this.roadMapCourse['preReqCoursesList'][i].courseId);
        //   this.addCoursesClick();
        //   saveIndex = i;
        // }
        // if (this.roadMapCourse['preReqCoursesList'][saveIndex + 1]) {
        //   this.selectedPreRequisiteCourses.push(this.roadMapCourse['preReqCoursesList'][saveIndex + 1].courseId);
        // }
       
        // this.fg.patchValue(response);

        resolve(true);

      },
        error => {
          console.log(error);
          this.loaded = true;
          reject(error);
        },
        () => {
          // this.loaded = true;
        }
      );

    }).then(() => {
      // finding courses that are not already added in road map
      return this.findRoadMapCourses().then(()=>{
        this.patchingPreRequisiteCoursesValue();
        this.fg.patchValue(this.roadMapCourse);

      });

    })


  }

  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close({ response: false });
  }

  /**
   * Add or update data in database when save button is clicked
   */

  saveData(item: RoadMapCourseModel) {
    item['roadMapId'] = this.roadMap.id;

    if (this.data.type === 'Add Course') {
      this.validate(item).then(() => {
        if (this.validationErrors.length === 0) {
          this.roadMapCourseService.create(item).subscribe(
            response => {
              this.roadMapCourse = response;
              this.dialogRef.close({ response: false });
            },
            error => {
              console.log(error);
              this.loaded = true;
            }
          );
        }
      });
    } else {
      item['roadMapId'] = this.roadMap.id;
      item['id'] = this.data.id;
      this.validate(item).then(() => {
        if (this.validationErrors.length === 0) {
          this.roadMapCourseService.update(this.data.id, item).subscribe(
            response => {
              this.dialogRef.close({ response: false });
            },
            error => {
              console.log(error);
              this.loaded = true;
            }
          );
        }
      });

    }

  }

  findClassesByProgramDetails() {
    return new Promise((resolve, reject) => {

      this.classesService.findByProgramDetail(this.roadMap.programDetailId).subscribe(response => {
        this.classes = response;
        resolve(true);
      },
        error => {
          console.log(error);
          reject(error);
        }
      );
    })

  }
  findRoadMapCourses() {

    return new Promise((resolve, reject) => {

      this.courseService.findRoadMapCourses(this.roadMapCoursesIds).subscribe(response => {
        this.courses = response;
        this.courses.map(element => {
          element.fullName = element.title + "-" + element.abbreviation;

        });
        if (this.roadMapCourse) {
          // selecting the course
          this.eventCourseSelection(this.roadMapCourse.courseId);
        }
        this.filteredOptionForCourseAutoComplete();
        if (this.data.type != 'Add Course') {
          this.getCourseTitle(this.roadMapCourse.courseId);

        }


        resolve(true);

      },
        error => {
          console.log(error);
          reject(error);
        }
      );
    })

  }
  findRoadMapAttributesList() {

    return new Promise((resolve, reject) => {

      return this.roadMapService.findAttributesList().subscribe(response => {
        this.roadMaps = response;
        resolve(true)
      },
        error => {
          console.log(error)
          reject(error);
        }
      );

    })

  }
  eventCourseSelection(eventValue: number) {
    if (this.courses) {
      if (eventValue !== this.selectedCourse) {
        while (this.selectedPreRequisiteCourses.length) {
          this.selectedPreRequisiteCourses.pop();
        }
        let control = <FormArray>this.fg.controls['preReqCoursesList'];
        while (control.length) {
          control.removeAt(0);
        }
        this.selectedCourse = -1;
        this.addCoursesClick();
      }
      this.courses.forEach(element => {
        if (element['id'] == eventValue) {
          this.courseAbbr = element['abbreviation'];
          this.selectedCourse = eventValue;
        }
      });
    }
    return eventValue;
  }
  createCourseItem(): FormGroup {
    return this.fb.group({
      courseId: new FormControl({ value: '', disabled: false })
    });
  }

  addCoursesClick(): void {
    console.log('Add Count');
    let preReqCoursesList = this.fg.get('preReqCoursesList') as FormArray;
    preReqCoursesList.push(this.createCourseItem());
    console.log('preReqCourses List : ', preReqCoursesList);
    // for disabling if courses already in  list
    if (preReqCoursesList.value[preReqCoursesList.value.length - 2]) {
      let value = preReqCoursesList.value[preReqCoursesList.value.length - 2].courseId;
      const index = this.selectedPreRequisiteCourses.indexOf(value);
      if (index < 0 && value) {
        this.selectedPreRequisiteCourses.push(value);
      }
    }
  }
  patchingPreRequisiteCoursesValue() {

    let preReqCoursesList = this.fg.get('preReqCoursesList') as FormArray;
    let saveIndex;
    for (let i = 0; i < this.roadMapCourse['preReqCoursesList'].length - 1; i++) {
      // pushing into array of selected pre req list
      this.selectedPreRequisiteCourses.push(this.roadMapCourse['preReqCoursesList'][i].courseId);
      preReqCoursesList.push(this.createCourseItem());
      saveIndex = i;
    }
    if (this.roadMapCourse['preReqCoursesList'][saveIndex+1]) {
      this.selectedPreRequisiteCourses.push(this.roadMapCourse['preReqCoursesList'][saveIndex+1].courseId);
    }
  }

  removeCourses(i: number, item) {
    console.log('Remove Count')
    let control = <FormArray>this.fg.controls['preReqCoursesList'];
    if (control.length !== 1) {
      //removing from selected arary of pre requiiste course List
      const index = this.selectedPreRequisiteCourses.indexOf(control.value[i].courseId);
      if (index > -1) {
        this.selectedPreRequisiteCourses.splice(index, 1);
      }
      // if selected remove then selected Course should be set -1
      if (this.selectedPreRequisiteCourse === control.value[i].courseId) {
        this.selectedPreRequisiteCourse = -1;
      }
      control.removeAt(i);
    } else {
      //removing from selected arary of pre requiiste course List
      const index = this.selectedPreRequisiteCourses.indexOf(control.value[i].courseId);
      if (index > -1) {
        this.selectedPreRequisiteCourses.splice(index, 1);
      }
      // if selected remove then selected Course should be set -1
      if (this.selectedPreRequisiteCourse === control.value[i].courseId) {
        this.selectedPreRequisiteCourse = -1;
      }
      control.removeAt(i);
      this.addCoursesClick();
      this.selectedPreRequisiteCourses.pop();
    }
  }
  /**
   *
   * @param courseId
   * @param semesterId
   */
  isCourseAlreadyExists(courseId: number, semesterId: number) {
    let isExist = false;
    if (this.roadMapCourses.length > 0) {
      let elementPos = this.roadMapCourses
        .map(function (x) {
          return x.id;
        })
        .indexOf(semesterId);
      if (elementPos > -1) {
        let roadMapCourse = this.roadMapCourses[elementPos]['roadMapCourses'];
        if (roadMapCourse) {
          roadMapCourse.forEach(element => {
            if (this.roadMapCourse) {
              if (courseId === element['courseId'] && courseId !== this.roadMapCourse.courseId) {
                isExist = true;
              }
            } else {
              if (courseId === element['courseId']) {
                isExist = true;
              }
            }
          });
        }
      }
    }
    return isExist;
  }

  eventPreRequisiteCourseSelection(event) {
    this.selectedPreRequisiteCourse = event.value;
  }
  findCourses() {
    return new Promise((resolve, reject) => {
      return this.roadMapCourseService.findCourses(this.roadMap.id).subscribe(response => {
        this.preRequisiteCourses = response;
        // console.log('response : ' , this.preRequisiteCourses);
        resolve(true);
      },
        error => {
          console.log(error);
          reject(error);
        }
      );
    })

  }
  validate(item: RoadMapCourseModel) {
    return new Promise((resolve, reject) => {
      this.roadMapCourseService.validate(item).subscribe(
        response => {
          this.validationErrors = response;
          if (this.isCourseAlreadyExists(item.courseId, item.classId)) {
            this.validationErrors.push({ code: 'INVALID_COURSE', msg: 'Course is already exists in this semester.' });
          }
          // setting errors on html form
          this.validationErrors.forEach(validationError => {
            if (validationError.code === 'INVALID_COURSE') {
              this.fg.controls['courseId'].setErrors({ exist: true });
            }
            if (validationError.code === 'INVALID_COURSE_ORDER') {
              this.fg.controls['courseOrder'].setErrors({ unique: true });
            }
            if (validationError.code === 'INVALID_COURSE_CODE') {
              this.fg.controls['courseCode'].setErrors({ unique: true });
            }
          });
        },
        error => {
          console.log(error);
          return reject(error);
        },
        () => {
          return resolve(this.validationErrors);
        }
      );
    });
  }

  showSemesterCoursesList(semesterId: number) {
    
    let isExist: Boolean = false;
    let index: number = -1;
    for (let i = 0; i < this.roadMapCourses.length; i++) {
      if (this.roadMapCourses[i]['id'] === semesterId) {
        isExist = true;
        index = i;
        break;
      }
    }
    if (isExist) {
      this.semester = this.roadMapCourses[index]['roadMapCourses'];
      this.semester['name'] = this.roadMapCourses[index]['name'];
    } else {
      this.semester = [];
    }
    this.selectedSemester = this.classes.find(element => {
      return element.id === semesterId
    })
    console.log('FG : ', this.fg);
  }
  /**
   * For Dynamically change of credit hours
   */
  courseCodeChangeEvent(event: any) {
    let courseCodeValue: string = event.target.value;
    if (this.fg.get('courseCode').valid) {
      let lastDigit: number = +courseCodeValue.substring(3, 4);
      if (lastDigit > this.maxCreditHour) {
        this.creditHourWarning();
      }
      this.fg.get('creditHours').setValue(lastDigit);
    }

  }
  /**
   * Credit Hours Warning
   */
  creditHourWarning() {
    const dialogRef = this.matDialog.open(AlertDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: 'Credit Hours are greater than ' + this.maxCreditHour }
    });

  }
  autoCompleteOptionSelections(value: string) {
    console.log('valuee : ', value);
    let existedCourse: CourseModel;
    existedCourse = _.find(this.courses, { fullName: value });

    if (!existedCourse) {
      this.courseId.setErrors({ invalid: true });
      while (this.selectedPreRequisiteCourses.length) {
        this.selectedPreRequisiteCourses.pop();
      }
      let control = <FormArray>this.fg.controls['preReqCoursesList'];
      while (control.length) {
        control.removeAt(0);
      }
      this.selectedCourse = -1;
      this.addCoursesClick();
    } else {
      this.courseId.setErrors(null);
      this.courseId.setValue(existedCourse.id);
    }
  }

  getCourseTitle(id: number) {
    let course: CourseModel = _.find(this.courses, { id: id })
    if (course) {
      this.courseTitle = course.title + "-" + course.abbreviation
    }
  }

}
