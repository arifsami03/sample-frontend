import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from 'modules/shared/helper';
import { CourseService } from 'modules/academic/services';
import { LayoutService } from 'modules/layout/services';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { CourseModel } from 'modules/academic/models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'academic-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.css'],
  animations: [PageAnimation]
})
export class CourseFormComponent implements OnInit {
  public pageState = 'active';

  public loaded: boolean = false;


  public boxLoaded = true;
  public pageActions = GLOBALS.pageActions;
  public error: Boolean;
  public fg: FormGroup;
  public pageAct: string;
  public course: CourseModel;

  public componentLabels = CourseModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private courseService: CourseService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * Initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new CourseModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get Data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.courseService.find(params['id']).subscribe(
        response => {
          this.course = response;
          this.fg.patchValue(this.course);
          if (!this.error) {
            if (this.pageAct === this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct === this.pageActions.update) {
              this.initUpdatePage();
            }
            this.loaded = true;
          }
        },

        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Initialize Create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Course: Create' });
    this.course = new CourseModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * Initialize view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Course: ' + this.course.title,
      breadCrumbs: [{ label: 'Home', url: '/home/dashboard' }, { label: 'Course ', url: '/academic/courses' }, { label: this.course.title }]
    });
  }

  /**
   * Initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Course: ' + this.course.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Course', url: '/academic/courses' },
        {
          label: this.course.title,
          url: `/academic/courses/view/${this.course.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Add or Update when save button is clicked
   */
  public saveData(item: CourseModel) {

    this.boxLoaded = false;

    item.abbreviation = item.abbreviation.toUpperCase();

    let id;
    if (this.pageAct === this.pageActions.create) {
      item.id = null;
      this.courseService.create(item).subscribe(
        response => {
          if (response) {
            this.layoutService.flashMsg({ msg: 'Course has been created.', msgType: 'success' });
            this.course = response;
            this.router.navigate([`/academic/courses`]);
          }
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
          this.boxLoaded = true;
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.courseService.update(this.course.id, item).subscribe(
        response => {
          if (response) {
            this.layoutService.flashMsg({ msg: 'Course has been updated.', msgType: 'success' });
            this.router.navigate([`/academic/courses`]);
          }
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
          this.boxLoaded = true;
        }
      );
    }
  }

  /**
   * Delete Record
   */
  delete(id: number) {

    const dialogRef = this.matDialog.open(ConfirmDialogComponent,
      { width: GLOBALS.deleteDialog.width, data: { message: GLOBALS.deleteDialog.message } }
    );

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';
        this.courseService.delete(id).subscribe(response => {

          if (response && (response['children']) ? response['children'].length > 0 : false) {

            this.loaded = true;
            this.pageState = 'active';

            this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

          } else {

            this.layoutService.flashMsg({ msg: 'Course has been deleted.', msgType: 'success' });
            this.router.navigate(['academic/courses']);

          }

        },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
            this.loaded = true;
            this.pageState = 'active';
          },
          () => {
            this.loaded = true;
            this.pageState = 'active';
          }
        );
      }
    });
  }

}
