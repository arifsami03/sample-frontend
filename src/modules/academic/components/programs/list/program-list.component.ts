import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { PageAnimation } from '../../../../shared/helper';

import { LayoutService } from '../../../../layout/services';
import { ProgramService } from '../../../services';

import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { ProgramModel } from '../../../models/program';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'academic-program-list',
  templateUrl: './program-list.component.html',
  styleUrls: ['./program-list.component.css'],
  animations: [PageAnimation]
})
export class ProgramListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public data: ProgramModel[] = [new ProgramModel()];
  public attrLabels = ProgramModel.attributesLabels;

  displayedColumns = [
    // 'instituteTypeId',
    'educationLevelId', 'name', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private programService: ProgramService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) { }

  /**
   * initialize the page of programs list
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Programs' });
    this.getRecords();
  }

  /**
   * get all records
   */
  getRecords(): void {
    this.programService.index().subscribe(
      response => {
        this.data = response;
        // mapping for listing
        this.data.map(element => {
          element.instituteName = element.educationLevel.instituteType.name;
          element.educationLevelName = element.educationLevel.education;
        })

        this.dataSource = new MatTableDataSource<ProgramModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }

    );
  }

  /**
   * change alphabets into lowercase
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * delete data against the id given
   */
  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.programService.delete(id).subscribe(
          response => {

            if (response && (response['children']) ? response['children'].length > 0 : false) {

              this.loaded = true;
              this.pageState = 'active';

              this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

            } else {

              this.layoutService.flashMsg({ msg: 'Program has been deleted.', msgType: 'success' });
              this.getRecords();

            }

          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
            this.loaded = true;
          },
          () => {
            this.getRecords();
            this.loaded = true;
          }
        );
      }
    });
  }

  sortData() {
    this.dataSource.sort = this.sort;
  }

}
