import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from 'modules/shared/helper';

import { ProgramService } from 'modules/academic/services';
import { EducationLevelService } from 'modules/institute/services';
import { LayoutService } from 'modules/layout/services';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { ProgramModel } from 'modules/academic/models';
import { GLOBALS } from 'modules/app/config/globals';
import { EducationLevelModel } from 'modules/institute/models/';

@Component({
  selector: 'academic-program-form',
  templateUrl: './program-form.component.html',
  styleUrls: ['./program-form.component.css'],
  animations: [PageAnimation]
})
export class ProgramFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public boxLoaded: boolean = true;

  // success and Error
  public success: Boolean;
  public successMessage: String;
  public errorMessage: String;
  public error: Boolean;
  private mNumber: ProgramModel;
  public educationLevelList: EducationLevelModel[];
  public fg: FormGroup;
  public pageAct: string;
  public program: ProgramModel;
  public options: Observable<string[]>;
  public check = true;

  public componentLabels = ProgramModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private programService: ProgramService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private educationLevelService: EducationLevelService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * it will initialize the program form page
   */
  private initializePage() {
    this.fg = this.fb.group(new ProgramModel().validationRules());
    this.getEducationLevelList();
    if (this.pageAct === 'add') {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get Data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.programService.find(+params['id']).subscribe(
        response => {
          this.program = response['data'];
          if (response) {
            // display of Succes and Error Message
            this.success = true;
            this.successMessage = response.success;
            this.fg.patchValue(this.program);
          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
          if (!this.error) {
            if (this.pageAct === 'view') {
              this.initViewPage();
            } else if (this.pageAct === 'update') {
              this.initUpdatePage();
            }
            this.loaded = true;
          }
        },

        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    });
  }

  getEducationLevelList() {
    this.educationLevelService.findAttributesList().subscribe(
      response => {
        this.educationLevelList = response;
      },
      error => console.log(error),
      () => { }
    );
  }

  /**
   * initialize page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Create Program' });
    this.program = new ProgramModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * initialize view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Program: ' + this.program.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Program ', url: '/academic/programs' },
        { label: this.program.name }
      ]
    });
  }

  /**
   * initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Program: ' + this.program.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Program', url: '/academic/programs' },
        {
          label: this.program.name,
          url: `/academic/programs/view/${this.program.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * add or update when save button is clicked
   */
  public saveData(item: ProgramModel) {
    this.boxLoaded = false;

    item.abbreviation = item.abbreviation.toUpperCase();

    this.fg.disable();
    let id;
    if (this.pageAct === 'add') {
      item.id = null;
      this.programService.create(item).subscribe(
        response => {
          if (response) {
            id = response.data.id;
            // display of Succes and Error Message
            this.success = true;
            this.successMessage = response.success;
            this.layoutService.flashMsg({ msg: 'Program has been created.', msgType: 'success' });
            this.router.navigate([`/academic/programs`]);
          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
        },

        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        }, () => {
          this.boxLoaded = true;
          this.fg.disable();
        }
      );
    } else if (this.pageAct === 'update') {
      this.programService.update(this.program.id, item).subscribe(
        response => {
          if (response) {
            this.success = true;
            this.successMessage = response.success;
            this.layoutService.flashMsg({ msg: 'Program has been updated.', msgType: 'success' });
            this.router.navigate([
              `/academic/programs`
            ]);
          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        }, () => {
          this.boxLoaded = true;
        }
      );
    }
  }

  /**
   * delete the selected program
   */
  delete(id: number) {

    const dialogRef = this.matDialog.open(ConfirmDialogComponent,
      { width: GLOBALS.deleteDialog.width, data: { message: GLOBALS.deleteDialog.message } }
    );

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.programService.delete(id).subscribe(response => {


          if (response && (response['children']) ? response['children'].length > 0 : false) {

            this.loaded = true;
            this.pageState = 'active';

            this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

          } else {

            this.layoutService.flashMsg({ msg: 'Program has been deleted.', msgType: 'success' });
            this.router.navigate(['institute/programs']);

          }

        }
          ,
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
            this.pageState = 'active';
          }, () => {
            this.loaded = true;
            this.pageState = 'active';
          }
        );
      }
    });
  }

}
