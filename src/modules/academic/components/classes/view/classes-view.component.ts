import { Component, ViewChild, OnInit } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatDialog,
  MatSort
} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { PageAnimation } from 'modules/shared/helper';
import { LayoutService } from 'modules/layout/services';
import { ProgramDetailsService, ClassesService } from 'modules/academic/services';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { ProgramDetailsModel, ClassesModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';
import { ClassesFormComponent } from '../form/classes-form.component';

@Component({
  selector: 'academic-classes-view',
  templateUrl: './classes-view.component.html',
  styleUrls: ['./classes-view.component.css'],
  animations: [PageAnimation]
})
export class ClassesViewComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public data: ClassesModel;
  public attrLabels = ClassesModel.attributesLabels;





  constructor(
    private route: ActivatedRoute,
    private programDetailService: ProgramDetailsService,
    private classesService: ClassesService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) { }

  /**
   * initialize the page of programs view
   */
  ngOnInit(): void {
    this.findWithProgramDetail();
  }

  /**
   * get all records
   */
  findWithProgramDetail() {
    return new Promise((resolve, reject) => {
      this.route.params.subscribe(params => {
        this.classesService.findWithProgramDetail(+params['id']).subscribe(
          response => {
            this.data = response;
            this.data.programDetailName = this.data.name;
            this.data.programName = this.data['program'].name;
            this.data.educationLevelName = this.data['program'].educationLevel.education;
            this.data.instituteTypeName = this.data['program'].educationLevel.instituteType.name;
            // this.layoutService.setPageTitle({ title: this.data.programDetailName + ' Classes' });
            this.initPage();
            resolve(response)

          },
          error => {
            console.log(error);
            reject(error);
            this.loaded = true;
            this.pageState = 'active';
          },
          () => {
            this.loaded = true;
            this.pageState = 'active';
          }
        );
      });
    });


  }


  /**
   * delete data against the id given
   */
  delete(id: number) {

    const dialogRef = this.matDialog.open(ConfirmDialogComponent,
      { width: GLOBALS.deleteDialog.width, data: { message: GLOBALS.deleteDialog.message } }
    );

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.classesService.delete(id).subscribe(response => {

          if (response && (response['children']) ? response['children'].length > 0 : false) {

            this.loaded = true;
            this.pageState = 'active';

            this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

          } else {

            this.layoutService.flashMsg({ msg: 'Class has been deleted.', msgType: 'success' });
            this.findWithProgramDetail().then(() => {
              if (this.data['classes'].length === 0) {
                this.router.navigate([`/academic/classes`]);
                this.loaded = true;
                this.pageState = 'active';

              }
              else {
                this.loaded = true;
                this.pageState = 'active';
              }
            });

          }

        }
          , error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
            this.loaded = true;
          },
          () => {
          }
        );
      }
    });
  }

  editClick(id: number) {
    const dialogRef = this.matDialog.open(ClassesFormComponent, {
      width: '1200px',
      data: { id: id, type: 'Update Class' }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.findWithProgramDetail();
    });
  }

  initPage() {
    this.layoutService.setPageTitle({
      title: `${this.data.programDetailName}  's Classes`,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Academics', url: '/academic' },
        {
          label: 'Classes',
          url: '/academic/classes'
        },
        {
          label: this.data.programDetailName,
          url: `/academic/classes/view/${this.data.programDetailId}`
        },
      ]
    });
  }
  addClass() {
    const dialogRef = this.matDialog.open(ClassesFormComponent, {
      width: '1200px',
      data: { type: 'Add Class', programDetail: this.data }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.findWithProgramDetail();
    });
  }

}
