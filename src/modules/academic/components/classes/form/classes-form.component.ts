import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from '../../../../shared/helper';
import { ProgramDetailsService, ClassesService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { ProgramDetailsModel, ClassesModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'academic-classes-form',
  templateUrl: './classes-form.component.html',
  styleUrls: ['./classes-form.component.css'],
  animations: [PageAnimation]
})
export class ClassesFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public boxLoaded = true;

  // success and Error
  public success: Boolean;
  public successMessage: String;
  public errorMessage: String;
  public error: Boolean;
  public programDetails: ProgramDetailsModel[];
  public programDetail: ProgramDetailsModel;
  public classes: ClassesModel;
  public fg: FormGroup;
  public options: Observable<string[]>;
  public check = true;
  public id: number;
  public componentLabels = ClassesModel.attributesLabels;
  public dialogueType: string;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private programDetailsService: ProgramDetailsService,
    private classesService: ClassesService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    public dialogRef: MatDialogRef<ClassesFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.id = this.data['id'];
    this.dialogueType = this.data['type'];
    this.programDetail = this.data['programDetail'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    this.dialogRef.updateSize('600px','');

  }
  /**
   * it will initialize the program form page
   */
  private initializePage() {
    this.fg = this.fb.group(new ClassesModel().validationRules());
    this.findProgramDetails();

    if (this.data.type != 'Add Class') {
      this.getData();

    } else {
      this.initCreatePage();

    }
  }

  /**
   * Get Data
   */
  private getData() {
    this.classesService.find(this.id).subscribe(
      response => {
        this.classes = response;
        this.updateDialoguSize();
        if (response) {
          // // display of Succes and Error Message
          // this.success = true;
          // this.successMessage = response.success;
          this.fg.patchValue(this.classes);
        } else {
        }
        if (!this.error) {
          if (this.data.type === 'Update Class') {
            this.initUpdatePage();
          }
          this.loaded = true;
        }
      },

      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  findProgramDetails() {
    this.programDetailsService.getProgramDetails().subscribe(
      response => {
        this.programDetails = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * initialize page
   */
  private initCreatePage() {
    this.loaded = true;
    if (this.programDetail) {
      this.fg.get('programDetailId').patchValue(this.programDetail.id);
      this.fg.get('programDetailId').disable();
      this.findWithProgramDetail(+this.fg.get('programDetailId').value);

    }
  }



  /**
   * initialize update page
   */
  private initUpdatePage() {
    this.fg.get('programDetailId').disable();
    this.findWithProgramDetail(+this.fg.get('programDetailId').value);
  }

  /**
   * add or update when save button is clicked
   */
  public saveData(formGroup: FormGroup) {
    let item: ClassesModel = formGroup.value
    item.programDetailId = this.fg.get('programDetailId').value;
    this.boxLoaded = false;
    let id;
    if (this.data.type === 'Add Class') {
      item.id = null;
      this.classesService.create(item).subscribe(
        response => {
          if (response) {
            id = response.id;
            // display of Succes and Error Message
            this.success = true;
            this.successMessage = response.success;
            this.layoutService.flashMsg({ msg: 'Class has been created.', msgType: 'success' });
            this.dialogRef.close({ response: false });
            this.router.navigate([`academic/classes/view/${item.programDetailId}`]);

          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
        },

        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
          this.boxLoaded = true;
        }
      );
    } else if (this.data.type != 'Add Class') {
      this.classesService.update(this.id, item).subscribe(
        response => {
          if (response) {
            this.success = true;
            this.successMessage = response.success;
            this.layoutService.flashMsg({ msg: 'Class has been updated.', msgType: 'success' });
            this.dialogRef.close({ response: false });

          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
          this.boxLoaded = true;
        }
      );
    }
  }


  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close({ response: false });
  }
  /**
  * get all records
  */
  findWithProgramDetail(programDetailId): void {
    this.loaded = false;
      this.classesService.findWithProgramDetail(programDetailId).subscribe(
        response => {
          this.classes = response;
        this.updateDialoguSize();
          
          this.classes.programDetailName = this.classes.name;
          this.classes.programName = this.classes['program'].name;
          this.classes.educationLevelName = this.classes['program'].educationLevel.education;
          this.classes.instituteTypeName = this.classes['program'].educationLevel.instituteType.name;

        },
        error => {
          this.loaded = true;          
          console.log(error);
        },
        () => {
          this.loaded = true;
        }
      );

  }
  updateDialoguSize(){
    if (this.classes){
      this.dialogRef.updateSize('1200px','');
    }else{
      this.dialogRef.updateSize('600px','');
    }
  }
}
