import { Component, ViewChild, OnInit } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatDialog,
  MatSort
} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { PageAnimation } from '../../../../shared/helper';
import { LayoutService } from '../../../../layout/services';
import { ProgramDetailsService, ClassesService } from '../../../services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { ProgramDetailsModel, ClassesModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';
import { ClassesFormComponent } from '../form/classes-form.component';

@Component({
  selector: 'academic-classes-list',
  templateUrl: './classes-list.component.html',
  styleUrls: ['./classes-list.component.css'],
  animations: [PageAnimation]
})
export class ClassesListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public data: ClassesModel[] = [new ClassesModel()];
  public attrLabels = ClassesModel.attributesLabels;

  displayedColumns = [
    // 'instituteTypeName', 
    'educationLevelName', 'programName', 'programDetailName','noOfClasses', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private programDetailService: ProgramDetailsService,
    private classesService: ClassesService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) { }

  /**
   * initialize the page of programs list
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Classes' });
    this.getRecords();
  }

  /**
   * get all records
   */
  getRecords(): void {
    this.classesService.index().subscribe(
      response => {
        this.data = response;
        // mapping for listing
        this.data.map(element => {
          element.programDetailName = element.name;
          element.programName = element['program'].name;
          element.educationLevelName = element['program'].educationLevel.education;
          element.instituteTypeName = element['program'].educationLevel.instituteType.name;
          element.noOfClasses = element['classes'].length;
        });

        this.dataSource = new MatTableDataSource<ClassesModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  /**
   * change alphabets into lowercase
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * delete data against the id given
   */
  deleteProgram(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.classesService.delete(id).subscribe(response => {
          if (response) {
            this.layoutService.flashMsg({ msg: 'Class has been deleted.', msgType: 'success' });
            this.getRecords();
          }
        }
          , error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
            this.loaded = true;
          },
          () => {
            this.loaded = true;
          }
        );
      }
    });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }
  addClass() {
    const dialogRef = this.matDialog.open(ClassesFormComponent, {
      width: '1200px',
      data: { type: 'Add Class' }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getRecords();
    });
  }

}
