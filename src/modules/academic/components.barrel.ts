import {
  MatInputModule, MatFormFieldModule, MatIconModule, MatDialogModule, MatButtonModule, MatCardModule, MatTableModule,
  MatPaginatorModule, MatOptionModule, MatAutocompleteModule, MatSelectModule, MatDividerModule, MatListModule, MatExpansionModule,
  MatCheckboxModule, MatProgressBarModule, MatRadioModule, MatChipsModule, MatDatepickerModule, MatSortModule,MatTooltipModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

/**
 * Import Services
 */
import {
  EducationLevelService,
  CampusAcademicCalendarService,
} from '../institute/services';
import {
  ProgramService, ProgramDetailsService, ClassesService, AcademicCalendarService, CourseService, RoadMapService, RoadMapCourseService,
  CACTermService, AcademicCalendarProgramDetailService, ActivityService,ACActivityService
} from './services';

/**
 * Import Components
 */
import {
  DashboardComponent, ProgramListComponent, ProgramFormComponent, ProgramDetailsListComponent, ProgramDetailsFormComponent, CourseFormComponent,
  CourseListComponent, ClassesListComponent, ClassesFormComponent, ClassesViewComponent, AcademicCalendarFormComponent, AcademicCalendarListComponent,
  RoadMapFormComponent, RoadMapListComponent, RoadMapViewComponent, RoadMapCourseFormComponent, RoadMapCourseViewComponent,
  CACTermFormComponent, CACTermListComponent, CACTermViewComponent, ActivityFormComponent,ActivityListComponent
} from './components';


export const __IMPORTS = [
  MatInputModule, MatFormFieldModule, MatIconModule, MatDialogModule, MatButtonModule, MatCardModule, FormsModule, ReactiveFormsModule,
  MatTableModule, MatPaginatorModule, MatAutocompleteModule, MatOptionModule, MatSelectModule, MatDividerModule, MatListModule,
  FlexLayoutModule, MatExpansionModule, MatCheckboxModule, MatProgressBarModule, MatRadioModule, MatChipsModule, MatDatepickerModule, MatSortModule,
  MatTooltipModule
];

export const __DECLARATIONS = [
  DashboardComponent, ProgramListComponent, ProgramFormComponent, ProgramDetailsListComponent, ProgramDetailsFormComponent, CourseFormComponent,
  CourseListComponent, ClassesListComponent, ClassesFormComponent, ClassesViewComponent, AcademicCalendarFormComponent, AcademicCalendarListComponent,
  RoadMapFormComponent, RoadMapListComponent, RoadMapViewComponent, RoadMapCourseFormComponent, RoadMapCourseViewComponent,
  CACTermFormComponent, CACTermListComponent, CACTermViewComponent, ActivityFormComponent, ActivityListComponent
];

export const __PROVIDERS = [
  EducationLevelService, CampusAcademicCalendarService, ProgramService, ProgramDetailsService, ClassesService, AcademicCalendarService,
  CourseService, RoadMapService, RoadMapCourseService, AcademicCalendarProgramDetailService, CACTermService,ActivityService,ACActivityService
];

export const __ENTRY_COMPONENTS = [RoadMapCourseFormComponent,ClassesFormComponent];
