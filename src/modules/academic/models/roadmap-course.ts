import { GLOBALS } from '../../app/config/globals';
import { FormControl, Validators, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';

export class RoadMapCourseModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    roadMapId: 'Road Map',
    courseId: 'Course Name',
    classId: 'Select Semester',
    courseCode: 'Course Code',
    natureOfCourseId: 'Nature of Course',
    courseType: 'Course Type',
    courseAbbreviation: 'Course',
    courseOrder: 'Course Order',
    creditHours: 'Credit Hours',
    preReqCoursesList: 'Pre Requisite Course'
  };

  id?: number;
  roadMapId: number;
  courseId: number;
  classId: number;
  courseCode: number;
  natureOfCourseId: number;
  courseType: string;
  courseOrder: number;
  creditHours: number;
  preReqCoursesList: any[]; //new FormArray([]); //any[]=[]; //TODO:Low Have to remove any;;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   *
   * @param equalControlName
   */
  public greaterThan?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      let controlMatch = 1;

      return (controlMatch <= control.value && 9999999999 >= control.value) || control.value === null
        ? null
        : {
          equalTo: true
        };
    };
  }

  /**
   * Form Validation Rules for Level of Education
   */
  public validationRules?() {
    return {
      courseId: new FormControl('', [<any>Validators.required]),
      classId: new FormControl('', [<any>Validators.required]),
      courseCode: new FormControl('', [<any>Validators.required, Validators.pattern('^([0-9]{4})$')]),
      natureOfCourseId: new FormControl(null, [Validators.required]),
      courseType: new FormControl('', [Validators.required]),
      courseOrder: new FormControl('', this.greaterThan('courseOrder')),
      creditHours: new FormControl('', [<any>Validators.required]),
      preReqCoursesList: new FormArray([])
    };
  }
}
