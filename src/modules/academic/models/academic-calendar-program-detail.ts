import { FormControl, Validators } from '@angular/forms';
import { ProgramDetailsModel, AcademicCalendarModel, CACTermModel } from '.';

export class AcademicCalendarProgramDetailModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    academicCalendarId: 'Academic Calendar',
    programDetailId: 'Progrma Detail'
  };

  id?: number;
  academicCalendarId?: number;
  programDetailId?: number;
  academicCalendar?: AcademicCalendarModel;
  programDetail?: ProgramDetailsModel;
  cacTerms?: CACTermModel[];
  startDate?: Date;
  endDate?: Date;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() {}
}
