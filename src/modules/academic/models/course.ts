import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class CourseModel {
  /**
   * Set the labels for attributes
   */
  static attributesLabels = {
    title: 'Course Title',
    abbreviation: 'Dep/Course Abbreviation',
    description: 'Description'
  };

  id?: number;
  title: string;
  abbreviation: string;
  description: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;
  fullName?: string;
  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      title: new FormControl('', [<any>Validators.required]),
      abbreviation: new FormControl('', [<any>Validators.required, Validators.maxLength(6)]),
      description: new FormControl('')
    };
  }
}
