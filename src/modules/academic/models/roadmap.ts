import { GLOBALS } from '../../app/config/globals';
import { FormControl, Validators, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';
import { ProgramDetailsModel, RoadMapCourseModel } from '.';

export class RoadMapModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    title: 'Roadmap Title',
    programDetailId: 'Program Details',
    academicYear: 'Year',
    category: 'Semester',
    totalCreditHours: 'Total Credit Hours',
    numberOfCourses: 'No of Courses'
  };

  id?: number;
  title?: string;
  programDetailId?: number;
  academicYear?: number;
  category?: string;
  programDetail?: ProgramDetailsModel;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;
  roadMapCourses ?: RoadMapCourseModel[]
  // for list

  programDetailName?: string;
  totalCreditHours?: number;
  numberOfCourses?: number;

  constructor() { }

  /**
   * Form Validation Rules for Level of Education
   */
  public validationRules?() {
    return {
      title: new FormControl('', [<any>Validators.required]),
      programDetailId: new FormControl('', [<any>Validators.required]),
      academicYear: new FormControl('', [<any>Validators.required]),
      category: new FormControl('', [<any>Validators.required]),
    };
  }
}
