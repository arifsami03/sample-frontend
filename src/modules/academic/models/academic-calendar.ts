import { FormControl, Validators, FormArray } from '@angular/forms';

export class AcademicCalendarModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    title: 'Title',
    abbreviation: 'Abbreviation',
    description: 'Description',
    activitiesList : 'Activity'
  };

  id?: number;
  title?: string;
  abbreviation?: string;
  academicCalendarProgramDetails?: boolean;
  description: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  
  

  constructor() {}

  /**
   * Form Validation Rules for Level of Education
   */
  public validationRules?() {
    return {
      title: new FormControl('', [<any>Validators.required]),
      abbreviation: new FormControl('', [<any>Validators.required]),
      description: new FormControl(''),
      academicCalendarProgramDetails: new FormControl(''),
      activitiesList: new FormArray([])
    };
  }
}
