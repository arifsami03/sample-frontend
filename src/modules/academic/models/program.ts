import { EducationLevelModel } from '../../institute/models';
import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class ProgramModel {
  /**
   * it will set the labels for attributes of program
   */
  static attributesLabels = {
    name: 'Program',
    educationLevelId: 'Level of Education',
    instituteTypeId: 'Institute Type',
    abbreviation: 'Abbreviation'
  };

  id?: number;
  educationLevelId: number;
  name: string;
  abbreviation?: string;
  instituteTypeId: number;
  educationLevel?: EducationLevelModel;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  // for listing 

  instituteName ?: string;
  educationLevelName ?: string;

  constructor() {}

  /**
   * Form Validation Rules for program
   */
  public validationRules?() {
    return {
      name: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      abbreviation: new FormControl('', [<any>Validators.required, Validators.maxLength(10)]),
      educationLevelId: new FormControl('', [<any>Validators.required])
    };
  }
}
