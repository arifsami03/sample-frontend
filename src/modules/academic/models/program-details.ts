import { FormControl, Validators, ValidatorFn } from '@angular/forms';
import { ProgramModel } from '.';

export class ProgramDetailsModel {
  /**
   * it will set the labels for attributes of program
   */
  static attributesLabels = {
    name: 'Program Detail',
    programId: 'Program',
    instituteTypeId: 'Institute Type',
    educationLevelId: 'Level of Education',
    abbreviation: 'Abbreviation',
    
  };

  id?: number;
  programId: number;
  name: string;
  abbreviation?: string;
  instituteTypeId: number;
  educationLevelId: number;
  program ?: ProgramModel; 
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

    // for listing 
    instituteName ?: string;
    educationLevelName ?: string;
    programName ?: string;
  constructor() {}

  /**
   * Form Validation Rules for program
   */
  public validationRules?() {
    return {
      name: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      abbreviation: new FormControl('', [<any>Validators.required, Validators.maxLength(10)]),
      programId: new FormControl('', [<any>Validators.required])
    };
  }
}
