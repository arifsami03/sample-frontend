import { FormControl, Validators } from '@angular/forms';
import { ActivityModel, AcademicCalendarModel } from '.';

export class ACActivityModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    academicCalendarId: 'Academic Calendar',
    activityId: 'Activity'
  };

  id?: number;
  academicCalendarId?: number;
  activityId?: number;
  academicCalendar?: AcademicCalendarModel;
  deleted?: boolean;
  activity?: ActivityModel;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() {}
}
