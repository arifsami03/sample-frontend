import { FormControl, Validators } from '@angular/forms';

export class ActivityModel {
  /**
   * it will set the labels for attributes of program
   */
  static attributesLabels = {
    title: 'Title',
    startDate: 'Start Date',
    endDate: 'End Date',
    dates : 'Date',
    days: 'Days'
  };

  id?: number;
  title: string;
  startDate?: Date;
  endDate?: Date;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  // for showing on list
  dates ?: string;
  days ?: number;


  constructor() {}

  /**
   * Form Validation Rules for program
   */
  public validationRules?() {
    return {
      title: new FormControl('', [<any>Validators.required]),
      startDate: new FormControl({value : null, disabled: false}),
      endDate: new FormControl({value : null, disabled: false})
    };
  }
}
