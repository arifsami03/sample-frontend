import { AcademicCalendarProgramDetailModel, AcademicCalendarModel, ProgramDetailsModel } from '.';
import { FormControl, Validators, FormArray } from '@angular/forms';
import { CampusAcademicCalendarModel } from '../../institute/models';

export class CACTermModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    campusAcademicCalendarId: 'Academic Calendar',
    startDate: 'Start Date',
    endDate: 'End Date',
    title: 'Title',
    abbreviation: 'Abbreviation'
  };

  id?: number;
  campusAcademicCalendarId?: number;
  campusAcademicCalendars?: CampusAcademicCalendarModel[];
  academicCalendarProgramDetailId? : number;
 
  title?: string;
  abbreviation?: string;
  startDate?: Date;
  endDate?: Date;
  status?: Boolean;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;
  fullName?: string;
  acpd ?: AcademicCalendarProgramDetailModel;
  academicCalendar?: AcademicCalendarModel;
  programDetail?: ProgramDetailsModel;

  // for listing
  cacTermsCount ?: number;
  cacCount ?:number;
  cacTerms? : CACTermModel[];
  constructor() {}

  /**
   * Form Validation Rules for Level of Education
   */
  public validationRules?() {
    return {
      title: new FormControl(null, [<any>Validators.required]),
      abbreviation: new FormControl(null, [<any>Validators.required]),
      startDate: new FormControl(null, [<any>Validators.required]),
      endDate: new FormControl(null, [<any>Validators.required])
    };
  }
}
