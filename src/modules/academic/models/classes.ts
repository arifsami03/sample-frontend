import { FormControl, Validators, ValidatorFn } from '@angular/forms';
import { ProgramDetailsModel } from '.';

export class ClassesModel {
  /**
   * Set the labels for attributes
   */
  static attributesLabels = {
    name: 'Class Name',
    programDetail: 'Program Detail',
    educationLevel: 'Level of Education',
    program: 'Program',
    instituteTypeId: 'Institute Type',
    noOfClasses : 'No of Classes'
  };

  id?: number;
  programDetailId: number;
  name: string;
  programDetail?: ProgramDetailsModel;
  programDetailName?: string;
  programName?: string;
  educationLevelName?: string;
  instituteTypeName?: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  // for listing
  noOfClasses ?: number;

  constructor() { }

  /**
   * Form Validation Rules for program
   */
  public validationRules?() {
    return {
      name: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      programDetailId: new FormControl('', [<any>Validators.required])
    };
  }
}
