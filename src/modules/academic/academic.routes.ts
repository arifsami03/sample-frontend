import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GLOBALS } from '../app/config/globals';

/**
 * Import Components
 * 
 */
import {
  DashboardComponent, ProgramListComponent, ProgramFormComponent, ProgramDetailsFormComponent, ProgramDetailsListComponent, CourseFormComponent,
  CourseListComponent, ClassesListComponent, ClassesFormComponent, ClassesViewComponent, AcademicCalendarFormComponent, AcademicCalendarListComponent,
  RoadMapFormComponent, RoadMapListComponent, RoadMapViewComponent, CACTermFormComponent, CACTermListComponent, CACTermViewComponent, ActivityListComponent, ActivityFormComponent,
} from './components';
import { AuthGuardService } from '../app/services';

/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: '',
    data: { breadcrumb: { title: 'Academics', display: true } },
    children: [{
      path: '',
      canActivate: [AuthGuardService],
      data: { breadcrumb: { title: 'Academics', display: false } },
      component: DashboardComponent,
    }, {
      path: 'academicCalendars',
      data: { breadcrumb: { title: 'Academic Calendar', display: true } },
      children: [
        {
          path: '',
          canActivate: [AuthGuardService],
          component: AcademicCalendarListComponent,
          data: { breadcrumb: { title: 'Academic Calendar', display: false } }
        },
        {
          path: 'create',
          canActivate: [AuthGuardService],
          component: AcademicCalendarFormComponent,
          data: {
            act: GLOBALS.pageActions.create,
            breadcrumb: { title: 'Create', display: true }
          }
        },
        {
          path: 'view/:id',
          canActivate: [AuthGuardService],
          component: AcademicCalendarFormComponent,
          data: { act: GLOBALS.pageActions.view }
        },
        {
          path: 'update/:id',
          canActivate: [AuthGuardService],
          component: AcademicCalendarFormComponent,
          data: { act: GLOBALS.pageActions.update }
        }
      ]
    },
    {
      path: 'roadMaps',
      data: { breadcrumb: { title: 'Roadmap', display: true } },
      children: [
        {
          path: '',
          canActivate: [AuthGuardService],
          component: RoadMapListComponent,
          data: { breadcrumb: { title: 'Roadmap', display: false } }
        },
        {
          path: 'create',
          canActivate: [AuthGuardService],
          component: RoadMapFormComponent,
          data: {
            act: GLOBALS.pageActions.create,
            breadcrumb: { title: 'Create', display: true }
          }
        },
        {
          path: 'view/:id',
          canActivate: [AuthGuardService],
          component: RoadMapViewComponent,
          data: { act: GLOBALS.pageActions.view }
        },
        {
          path: 'update/:id',
          canActivate: [AuthGuardService],
          component: RoadMapFormComponent,
          data: { act: GLOBALS.pageActions.update }
        }
      ]
    },

    {
      path: 'programs',
      data: { breadcrumb: { title: 'Programs', display: true } },
      children: [
        {
          path: '',
          canActivate: [AuthGuardService],
          component: ProgramListComponent,
          data: { breadcrumb: { title: 'Programs', display: false } }
        },
        {
          path: 'add',
          canActivate: [AuthGuardService],
          component: ProgramFormComponent,
          data: {
            act: 'add',
            breadcrumb: { title: 'Add', display: true }
          }
        },
        {
          path: 'view/:id',
          canActivate: [AuthGuardService],
          component: ProgramFormComponent,
          data: { act: 'view' }
        },
        {
          path: 'update/:id',
          canActivate: [AuthGuardService],
          component: ProgramFormComponent,
          data: { act: 'update' }
        }
      ]
    },
    {
      path: 'activities',
      data: { breadcrumb: { title: 'Activities', display: true } },
      children: [
        {
          path: '',
          canActivate: [AuthGuardService],
          component: ActivityListComponent,
          data: { breadcrumb: { title: 'Activities', display: false } }
        },
        {
          path: 'add',
          canActivate: [AuthGuardService],
          component: ActivityFormComponent,
          data: {
            act: 'add',
            breadcrumb: { title: 'Add', display: true }
          }
        },
        {
          path: 'view/:id',
          canActivate: [AuthGuardService],
          component: ActivityFormComponent,
          data: { act: 'view' }
        },
        {
          path: 'update/:id',
          canActivate: [AuthGuardService],
          component: ActivityFormComponent,
          data: { act: 'update' }
        }
      ]
    },
    {
      path: 'programDetails',
      data: { breadcrumb: { title: 'Program Details', display: true } },
      children: [
        {
          path: '',
          canActivate: [AuthGuardService],
          component: ProgramDetailsListComponent,
          data: { breadcrumb: { title: 'Programs', display: false } }
        },
        {
          path: 'add',
          canActivate: [AuthGuardService],
          component: ProgramDetailsFormComponent,
          data: {
            act: 'add',
            breadcrumb: { title: 'Add', display: true }
          }
        },
        {
          path: 'view/:id',
          canActivate: [AuthGuardService],
          component: ProgramDetailsFormComponent,
          data: { act: 'view' }
        },
        {
          path: 'update/:id',
          canActivate: [AuthGuardService],
          component: ProgramDetailsFormComponent,
          data: { act: 'update' }
        }
      ]
    },
    {
      path: 'classes',
      data: { breadcrumb: { title: 'Classes', display: true } },
      children: [
        {
          path: '',
          canActivate: [AuthGuardService],
          component: ClassesListComponent,
          data: { breadcrumb: { title: 'Classes', display: false } }
        },
        // {
        // //   path: 'add',
        // canActivate: [AuthGuardService],//   
        // component: ClassesFormComponent,
        //   data: {
        //     act: 'add',
        //     breadcrumb: { title: 'Add', display: true }
        //   }
        // },
        {
          path: 'view/:id',
          canActivate: [AuthGuardService],
          component: ClassesViewComponent,
          data: { act: 'view' }
        },
        // {
        //   path: 'update/:id',
        // canActivate: [AuthGuardService],//   
        // component: ClassesFormComponent,
        //   data: { act: 'update' }
        // }
      ]
    },
    {
      path: 'courses',
      data: { breadcrumb: { title: 'Course', display: true } },
      children: [
        {
          path: '',
          canActivate: [AuthGuardService],
          component: CourseListComponent,
          data: { breadcrumb: { title: 'Course', display: false } }
        },
        {
          path: 'create',
          canActivate: [AuthGuardService],
          component: CourseFormComponent,
          data: {
            act: GLOBALS.pageActions.create,
            breadcrumb: { title: 'Create', display: true }
          }
        },
        {
          path: 'view/:id',
          canActivate: [AuthGuardService],
          component: CourseFormComponent,
          data: { act: GLOBALS.pageActions.view }
        },
        {
          path: 'update/:id',
          canActivate: [AuthGuardService],
          component: CourseFormComponent,
          data: { act: GLOBALS.pageActions.update }
        }
      ]
    },
    {
      path: 'cacTerms',
      data: { breadcrumb: { title: 'Academic Calendar Term', display: true } },
      children: [
        {
          path: '',
          canActivate: [AuthGuardService],
          component: CACTermListComponent,
          data: { breadcrumb: { title: 'Academic Calendar Term', display: false } }
        },
        {
          path: 'create',
          canActivate: [AuthGuardService],
          component: CACTermFormComponent,
          data: {
            act: GLOBALS.pageActions.create,
            breadcrumb: { title: 'Create', display: true }
          }
        },
        {
          path: 'view/:id',
          canActivate: [AuthGuardService],
          component: CACTermViewComponent,
          data: { act: GLOBALS.pageActions.view }
        },
        {
          path: 'update/:id',
          canActivate: [AuthGuardService],
          component: CACTermFormComponent,
          data: { act: GLOBALS.pageActions.update }
        }
      ]
    },]
  },

];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcademicRoutes { }
