export const AcademicMenu = [
  {
    heading: 'Academic',
    menuItems: [
      { title: 'Programs', url: ['/academic/programs'], perm: 'programs.index' },
      { title: 'Program Details', url: ['/academic/programDetails'], perm: 'programDetails.index' },
      { title: 'Classes', url: ['/academic/classes'], perm: 'classes.index' },
      { title: 'Courses', url: ['/academic/courses'], perm: 'courses.index' },
      { title: 'Academic Calendars', url: ['/academic/academicCalendars'], perm: 'academicCalendars.index' },
      { title: 'Academic Calendar Terms', url: ['/academic/cacTerms'], perm: 'cacTerms.index' },
      { title: 'Roadmaps', url: ['/academic/roadMaps'], perm: 'roadMaps.index' },
      { title: 'Activities', url: ['/academic/activities'], perm: 'activities.index' },
    ]
  },
];
