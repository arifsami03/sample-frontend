import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from '../../shared/services'; 
import { CACTermModel } from '../models';

@Injectable()
export class CACTermService extends BaseService {
  private routeURL: String = 'academic/cacTerms';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<CACTermModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <CACTermModel[]>data.json();
    });
  }
  /**
   * it will return all the data of programs
   */
  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }
 
  /**
   * Get single record
   */
  find(id: number): Observable<CACTermModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <CACTermModel>data.json();
    });
  }

  /**
   * Get single record
   */
  findByACPD(acpdId: number): Observable<CACTermModel[]> {
    return this.__get(`${this.routeURL}/findByACPD/${acpdId}`).map(data => {
      return <CACTermModel[]>data.json();
    });
  }
 

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }
 
  /**
   * Create record
   */
  create(item: CACTermModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: CACTermModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }
}
