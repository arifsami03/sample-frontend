import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';

import { ProgramModel } from '../models';
import { BaseService } from '../../shared/services';

@Injectable()
export class ProgramService extends BaseService {
  private routeURL: String = 'academic/programs';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * it will return all the data of programs
   */
  index() {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return data.json();
    });
  }

  /**
   * it will return the data of a program against the id
   */
  find(id: number) {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * it will delete the selected program record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * it will create a new program entry
   */
  create(item: ProgramModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * it will update the selected program record
   */
  update(id: number, item: ProgramModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Get List Of Programs
   */
  getProgramsList() {
    return this.__get(`${this.routeURL}/getProgramsList`).map(data => {
      return data.json();
    });
  }
  /**
 * it will return all the data Of Program which are not in selected Courses list
 */

  getPreReqNotSelected(selectedPrograms): Observable<ProgramModel[]> {
    return this.__post(`${this.routeURL}/getPreReqNotSelected`, selectedPrograms).map(data => {
      return <ProgramModel[]>data.json();
    });
  }

  /**
   * Get All Data
   */
  getAllPrograms(educationLevelId: number, selectedPrograms: number[]) {
    return this.__post(`${this.routeURL}/getAllPrograms/${educationLevelId}`, selectedPrograms).map(data => {
      return data.json();
    });
  }

  getProgramsWithProgramDetails(educationLevelId: number) {
    return this.__get(`${this.routeURL}/getProgramsWithProgramDetails/${educationLevelId}`).map(data => {
      return data.json();
    });
  }

}
