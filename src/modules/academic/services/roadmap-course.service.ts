import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from '../../shared/services'; 
import { RoadMapCourseModel, CourseModel } from '../models';

@Injectable()
export class RoadMapCourseService extends BaseService {
  private routeURL: String = 'academic/roadMapCourses';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<RoadMapCourseModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <RoadMapCourseModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<RoadMapCourseModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <RoadMapCourseModel>data.json();
    });
  }
  /**
   * Get single record
   */
  findMaxCourseOrder(roadMapId: number): Observable<number> {
    return this.__get(`${this.routeURL}/findMaxCourseOrder/${roadMapId}`).map(data => {
      return <number>data.json();
    });
  }
  /**
   * Get Record Against Road Map
   */
  findByRoadMap(id: number): Observable<RoadMapCourseModel[]> {
    return this.__get(`${this.routeURL}/findByRoadMap/${id}`).map(data => {
      return <RoadMapCourseModel[]>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: RoadMapCourseModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: RoadMapCourseModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }
  /**
   * find record
   */
  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }

  /**
   * Get single record
   */
  findCourses(id: number): Observable<CourseModel[]> {
    return this.__get(`${this.routeURL}/findCourses/${id}`).map(data => {
      return <CourseModel[]>data.json();
    });
  }

  /**
   * Return array of errors
   */
  validate(item: RoadMapCourseModel) {
    return this.__post(`${this.routeURL}/validate`, item).map(data => {
      return data.json();
    });
  }
}
