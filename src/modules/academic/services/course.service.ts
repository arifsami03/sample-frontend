import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';

import { CourseModel } from '../models';
import { BaseService } from '../../shared/services';

@Injectable()
export class CourseService extends BaseService {
  private routeURL: String = 'academic/courses';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<CourseModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <CourseModel[]>data.json();
    });
  }

  /**
   * it will return all the data of programs
   */
  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }

  /**
   * it will return all the data Of courses which are not in selected Courses list
   */
  findPreReqCourses(postData): Observable<CourseModel[]> {
    return this.__post(`${this.routeURL}/findPreReqCourses`, postData).map(data => {
      return <CourseModel[]>data.json();
    });
  }

  /**
   *
   * it will return all the data Of courses which are not already added in road map
   */
  findRoadMapCourses(postData): Observable<CourseModel[]> {
    return this.__post(`${this.routeURL}/findRoadMapCourses`, postData).map(data => {
      return <CourseModel[]>data.json();
    });
  }
  /**
   * it will return all the data Of courses which are not in selected Courses list
   */

  getSubjectsNotSelected(selectedPassingYear): Observable<CourseModel[]> {
    return this.__post(`${this.routeURL}/getSubjectsNotSelected`, selectedPassingYear).map(data => {
      return <CourseModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<CourseModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <CourseModel>data.json();
    });
  }

  /**
   * Delete Record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create Record
   */
  create(course: CourseModel) {
    return this.__post(`${this.routeURL}/create`, course).map(data => {
      return data.json();
    });
  }

  /**
   * Update Record
   */
  update(id: number, course: CourseModel) {
    return this.__put(`${this.routeURL}/update/${id}`, course).map(data => {
      return data.json();
    });
  }

}
