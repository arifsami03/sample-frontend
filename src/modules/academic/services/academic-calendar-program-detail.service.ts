import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from '../../shared/services'; 
import { AcademicCalendarProgramDetailModel } from '../models';

@Injectable()
export class AcademicCalendarProgramDetailService extends BaseService {
  private routeURL: String = 'academic/academicCalendarProgramDetails';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<AcademicCalendarProgramDetailModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <AcademicCalendarProgramDetailModel[]>data.json();
    });
  }
  /**
   * it will return all the data of programs
   */
  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }
  /**
   * it will return all the data of programs on the basis of campus progrma details
   */
  findByCampusProgramDetails(campusIds) {
    return this.__post(`${this.routeURL}/findByCampusProgramDetails`,campusIds).map(data => {
      return data.json();
    });
  }

   /**
   * it will return all the data of single program with detail
   */
  findAttributes(id) {
    return this.__get(`${this.routeURL}/findAttributes/${id}`).map(data => {
      return data.json();
    });
  }
   /**
   * it will return all the data of  based on program detail
   */
  findByProgramDetail(programDetailId) {
    return this.__get(`${this.routeURL}/findByProgramDetail/${programDetailId}`).map(data => {
      return data.json();
    });
  }
  /**
   * Get single record
   */
  find(id: number): Observable<AcademicCalendarProgramDetailModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <AcademicCalendarProgramDetailModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: AcademicCalendarProgramDetailModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: AcademicCalendarProgramDetailModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }

   
}
