import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from '../../shared/services';
import { ClassesModel } from '../models';

@Injectable()
export class ClassesService extends BaseService {
  private routeURL: String = 'academic/classes';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<ClassesModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <ClassesModel[]>data.json();
    });
  }
  /**
   * it will return all the data of programs
   */
  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }
  /**
   * it will return all the data of programs
   */
  findByProgramDetail(programDetailId: number) {
    return this.__get(`${this.routeURL}/findByProgramDetail/${programDetailId}`).map(data => {
      return data.json();
    });
  }
  /**
   * it will return all the data of programs details with program detail
   */
  findWithProgramDetail(programDetailId: number) {
    return this.__get(`${this.routeURL}/findWithProgramDetail/${programDetailId}`).map(data => {
      return data.json();
    });
  }
  /**
   * Get single record
   */
  find(id: number): Observable<ClassesModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <ClassesModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: ClassesModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: ClassesModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }
  /**
   * Update record
   */
  updateByProgramDetail(id: number, programDetailId: number, item: ClassesModel) {
    return this.__put(`${this.routeURL}/updateByProgramDetail/${programDetailId}/${id}`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Get all classes against a programDetail
   */
  findProgramDetailsClasses(id: number): Observable<ClassesModel[]> {
    return this.__get(`${this.routeURL}/findProgramDetailsClasses/${id}`).map(data => {
      return <ClassesModel[]>data.json();
    });
  }

}
