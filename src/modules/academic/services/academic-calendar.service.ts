import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from '../../shared/services';
import { AcademicCalendarModel } from '../models';

@Injectable()
export class AcademicCalendarService extends BaseService {
  private routeURL: String = 'academic/academicCalendars';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<AcademicCalendarModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <AcademicCalendarModel[]>data.json();
    });
  }
  /**
   * it will return all the data of programs
   */
  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }
  /**
   * Get single record
   */
  find(id: number): Observable<AcademicCalendarModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <AcademicCalendarModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: AcademicCalendarModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: AcademicCalendarModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Find related data for deletion
   * 
   * @param id 
   */
  findRelatedData(id: number) {
    return this.__get(`${this.routeURL}/findRelatedData/${id}`).map(data => {
      return data.json();
    });
  }
}
