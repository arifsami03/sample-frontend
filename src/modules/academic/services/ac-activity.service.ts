import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from '../../shared/services'; 
import { ACActivityModel } from '../models';

@Injectable()
export class ACActivityService extends BaseService {
  private routeURL: String = 'academic/acActivities';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<ACActivityModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <ACActivityModel[]>data.json();
    });
  }
  /**
   * it will return all the data of programs
   */
  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }


   /**
   * it will return all the data of single program with detail
   */
  findAttributes(id) {
    return this.__get(`${this.routeURL}/findAttributes/${id}`).map(data => {
      return data.json();
    });
  }
   /**
   * it will return all the data of  based on program detail
   */
  findByActivity(activityId) {
    return this.__get(`${this.routeURL}/findByActivity/${activityId}`).map(data => {
      return data.json();
    });
  }
  /**
   * Get single record
   */
  find(id: number): Observable<ACActivityModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <ACActivityModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: ACActivityModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: ACActivityModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }

   
}
