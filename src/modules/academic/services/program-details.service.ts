import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { environment } from 'environments/environment';

import { ProgramModel } from '../models';
import { BaseService } from '../../shared/services';

@Injectable()
export class ProgramDetailsService extends BaseService {
  private routeURL: String = 'academic/programDetails';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * it will return all the data of programs
   */
  index() {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return data.json();
    });
  }
  /**
   * it will return all the data of programs
   */
  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }
  /**
   * it will return all the data of programs
   */
  findProgramDetailslistNotInCampusAdmission(campusId,campusAdmissionId) {
    return this.__get(`${this.routeURL}/findProgramDetailslistNotInCampusAdmission/${campusId}/${campusAdmissionId}`).map(data => {
      return data.json();
    });
  }

  /**
   * it will return the data of a program against the id
   */
  find(id: number) {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * it will delete the selected program record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * it will create a new program entry
   */
  create(item: ProgramModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * it will update the selected program record
   */
  update(id: number, item: ProgramModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }

  getProgramProgramDetails(id: number) {
    return this.__get(`${this.routeURL}/getProgramProgramDetails/${id}`).map(data => {
      return data.json();
    });
  }
  getProgramDetails() {
    return this.__get(`${this.routeURL}/getProgramDetails`).map(data => {
      return data.json();
    });
  }

  getClassesByProgramDetail(id: number) {
    return this.__get(`${this.routeURL}/getClassesByProgramDetail/${id}`).map(data => {
      return data.json();
    });
  }

}
