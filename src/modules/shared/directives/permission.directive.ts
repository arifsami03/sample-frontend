import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { environment } from 'environments/environment.prod';

@Directive({
  selector: '[ifPermitted]'
})
/**
 * This directive class is responsible for checking the permission and if found then render the view.
 *
 * how to use?
 * <div *ifPermitted="'permission_name'">
 *      Your content is here...
 * </div>
 *
 * @class PermissionDirective
 */
export class PermissionDirective {
  /**
   * Input permission name provided when using the directive.
   */
  @Input('ifPermitted') permissionName: string;

  /**
   * @constructor
   * @param templateRef TemplateRef
   * @param viewContainer ViewContainerRef
   */
  constructor(private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef) {}

  /**
   * On Init check the permissin.
   */
  ngOnInit() {
    // If logged in users is a super user then just display the content
    if (localStorage.getItem('role') == 'admin') {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }
    // Provided permission should not be null or undefined.
    else if (this.permissionName && this.permissionName != 'undefined') {
      let permissionsAry = JSON.parse(localStorage.getItem('permissions'));

      let foundItem = permissionsAry.find(o => o['name'].toLowerCase() == this.permissionName.toLowerCase());

      // Check if permission does not found/exist or it is found and true then and only then render content.
      if (!foundItem || (foundItem && foundItem['status'] == true)) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      }
    }
    //TODO:low: need to discuss what if *ifPermitted is called but not permission name is provided then should we display the content or not
    // else {
    //   //To display message to developer if it is not production mode.
    //   if (environment.production == false) {
    //     
    //   }
    //   this.viewContainer.createEmbeddedView(this.templateRef);
    // }
  }
}
