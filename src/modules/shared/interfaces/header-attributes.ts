export interface IHeaderAttributes {
    field: string,
    label?: string,
    width?: number,
    key?: string,
}