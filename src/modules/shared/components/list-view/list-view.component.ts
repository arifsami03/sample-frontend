import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {
  MatPaginator,
  MatTableDataSource,
  MatSort
} from '@angular/material';
/**
 * HOW TO USE IT
 * 
 * Examples
 *    <shared-list-view [data]="wfState" [options]="listViewOptions"></shared-list-view>
 * 
 * NOW IN TS FILE
 *    
 *    listViewOptions = {
    labels: YourModel.attributesLabels,
    fields: [
      { name: 'state', width: 20 },
      { name: 'title', width: 20 },
      { name: 'description', width: 20 },
    ]
  };
 */
@Component({
  selector: 'shared-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css']
})
export class ListViewComponent implements OnInit {

  /**
   * DATA COMING FROM THAT COMPONENT (COMPULSORY)
   */
  @Input() data;

  /**
   * FOR STORING ALL THE LABELS IN THE MODEL
   */
  labels: string[] = [];

  /**
   * FOR STORING HEIGHT OF THE ROW
   */
  rowHeight: string = '100px';
  /**
   * SPECIFY LABELS YOU WANT TO SHOW AND WIDTH FOR EACH COLUMN (COMPULSORY)
   */
  @Input() options = {};

  /**
   * FOR STORING FLEX WIDTH FOR THE LABELS
   */
  labelWidth: number[] = [];

  /**
   * FOR STORING MAXIMUM WORDS FOR AN ATTRIBUTE (OPTIONAL)
   */
  @Input() maxWordsLength = 50;

  /**
   * SHOW (more/less) LINK WHERE ATTRIBUTE WORDS LENGTH > maxWordsLength (OPTIONAL)
   */
  @Input() showMoreLessLink = true;

  /**
   * CHECKING WHETHER ATTRIBUTE DATA LENGTH > maxWordsLength OR NOT
   */
  public isLengthyData = [];

  /**
   * SAVING ATTRIBUTES DATA (NO OF WORDS WILL BE = maxWordsLength) WHERE DATA LENGTH > maxWordsLength
   */
  public lengthyData: string[] = [];

  /**
   * SHOW MORE AND LESS LINK 
   */
  public show: string[] = [];

  /**
   * FOR STORING COLUMNS WHICH SHOULD BE DISPLAYED 
   */
  // displayedColumns = ['state', 'title'];
  displayedColumns: string[] = [];// = ['state', 'title'];
  /**
   * Constructor
   * 
   */
  constructor() { }


  /**
   * NG ONINIT
   * 
   */
  ngOnInit() {

    // if (!this.options) {
    //   this.options = [];
    //   for (var key in this.labels) {
    //     this.options.push(key);
    //   }
    // }

    this.prepareKeys();

  }

  /**
   * FIRST GETTING ALL THE LABELS AND THEN GETTING LABELS WHICH IS REQUIRED TO SHOW IN THE LIST.
   * AND GETTING FLEX WIDTHS FOR EACH COLUMN
   * 
   * IF LENGHT > maxWordsLength THEN 
   *    NOW SAVE ORIGINAL DATA IN this.lengthyData[key] AND SAVE DATA WITH maxWordsLength (20,21,22,...) ...
   * LENGTH IN this.data[key] AND PUSH TRUE IN this.isLengthyData.push(true); AND PUT MORE IN this.show[key] = 'More';
   * 
   * ELSE IF LENGHT < maxWordsLength THEN PUSH FALSE IN this.isLengthyData.push(false);
   * 
   */
  prepareKeys() {
    let k = 0;
    for (var fields in this.options) {
      if (k == 0) {
        this.labels = this.options[fields];
        this.rowHeight = this.options['height'];
      }
      if (k > 0) {
        let field = this.options[fields];
        for (let j = 0; j < field.length; j++) {

          let i = 0;
          let label = field[j];

          for (var key in label) {

            if (i == 0) {

              this.displayedColumns.push(label[key]);
              // this.labels.push(label[key]);
            }
            else if (i == 1) {
              this.labelWidth.push(label[key]);
            }
            i++;
          }
        }
      }
      k++;
    }
    // this.displayedColumns.push(this.labels[key]);
    // put it in for loop


    // for (var key in this.labels) {
    //   this.keys.push(key);

    //   if (typeof this.data[key] == 'string') {

    //     let arr = this.data[key].split(' ');

    //     if (arr.length > this.maxWordsLength) {

    //       this.lengthyData[key] = this.data[key];
    //       this.show[key] = 'More';
    //       this.isLengthyData.push(true);
    //       let less = this.data[key];
    //       let arr = less.split(' ');
    //       arr = arr.slice(0, this.maxWordsLength);
    //       let str = arr.toString();
    //       let result = str.replace(/,/g, " ");
    //       this.data[key] = result;
    //     }
    //     else {
    //       this.isLengthyData.push(false);
    //     }
    //   }
    //   else {
    //     this.isLengthyData.push(false);
    //   }
    // }
  }

  /**
   * IT WILL BE CALLED ON CLICK OF MORE OR LESS LINK
   * IF this.show[key] === 'More'
   *    SHOW MORE DATA AND SET this.show[key] = 'Less';
   * ELSE IF this.show[key] === 'Less'
   *    SHOW LESS DATA AND SET this.show[key] = 'Less';
   */
  showDescription(key) {
    if (this.show[key] === 'More') {
      this.show[key] = 'Less';
      let temp = this.data[key];
      this.data[key] = this.lengthyData[key];
      this.lengthyData[key] = temp;
    }
    else if (this.show[key] === 'Less') {
      this.show[key] = 'More';
      let temp = this.data[key];
      this.data[key] = this.lengthyData[key];
      this.lengthyData[key] = temp;
    }
  }
}
