import { Component, ViewChild, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { GLOBALS } from '../../../../app/config/globals';
import { ConfirmDialogComponent } from '../../confirm-dialog/confirm-dialog.component';
import { LayoutService } from '../../../../layout/services';
import { AttachedFileService } from '../../../services';
import { AttachedFileModel } from '../../../models';
// File Saver Library to download file
import { saveAs } from 'file-saver';

/**
 * How to use it
 *
 * <shared-attachment-list
 *
 *    [parentId] //parent Id for which you want to attach files like [parentId]= 1
 *    [parent] // parent name for which you want to attach files like parent="'campus'"
 *
 * >
 *
 * </shared-attachment-list>
 *
 * @export
 * @class AttachmentListComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'shared-attachment-list',
  templateUrl: './attachment-list.component.html',
  styleUrls: ['./attachment-list.component.css']
})
export class AttachmentListComponent implements OnInit {
  @Input() parentId: number;
  @Input() parent: string;
  public isViewFile: boolean;
  public numberOfAttachedFiles: number;
  public loaded: boolean = false;

  public data: AttachedFileModel[] = [new AttachedFileModel()];

  public attrLabels = AttachedFileModel.attributesLabels;

  displayedColumns = ['originalName', 'createdAt', 'size', 'type', 'options'];

  public panelOpenState: boolean = false;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(private attachedFileService: AttachedFileService, public dialog: MatDialog, public matDialog: MatDialog, private router: Router, public layoutService: LayoutService) {}

  /**
   * Initialize page
   */
  ngOnInit(): void {
    // this.layoutService.setPageTitle({ title: 'Attachment List' });
    this.getRecords();
    this.attachedFileService.setIsAdded$.subscribe(res => {
      if (res) {
        this.getRecords();
      }
    });
  }

  /**
   * Get data/records from backend
   */
  getRecords() {
    let queryParams = {
      parent: this.parent,
      parentId: this.parentId
    };
    this.attachedFileService.list(queryParams).subscribe(
      response => {
        if (response) {
          this.numberOfAttachedFiles = response['count'];
          this.data = response['rows'];
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Delete record
   */
  delete(id: number) {
    // Confirm dialog
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.attachedFileService.delete(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Attachment has been deleted.', msgType: 'success' });
              this.getRecords();
              setTimeout((router: Router) => {
                this.success = false;
              }, 1000);
            },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            }
          );
        }
      });
  }

  /**
   * View the all attached file of specific attachment
   */
  viewAttachedFiles() {
    if (this.isViewFile) {
      this.isViewFile = false;
    } else {
      this.isViewFile = true;
    }
  }

  /**
   * Downloading File
   *
   */
  public download(id: number, fileName: string) {
    let item = { id: id };
    this.attachedFileService.download(item).subscribe(
      response => {
        saveAs(response, fileName);
      },
      error => {
        console.log(error);
      }
    );
  }
}
