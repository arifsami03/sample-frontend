import { LayoutService } from '../../../../layout/services';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { AttachedFileService, AttachmentService } from '../../../services';
import { environment } from 'environments/environment';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import * as path from 'path';

/**
 * Use ng2-file-upload for file uploading from angular side
 */
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';

/**
 * Given the backend API URL
 */
const URL = environment.backendAPIURL + '/settings/attachments/fileUpload/';

/**
 * How to use it
 *
 * <shared-attachment-form
 *
 *      [parentId] //parent Id for which you want to attach files like [parentId]= 1
 *      [parent] // parent name for which you want to attach files like parent="'campus'"
 *      [fileFilter] // add filtration of attachment from front end and backend both  like [fileFilter]="'.jpg,.jpeg,.png'" by default select all files
 *      [fileSize] // Attachment size in MB like [fileSize]=2 by default 1MB
 * >
 * </shared-attachment-form>
 */
@Component({
  selector: 'shared-attachment-form',
  templateUrl: './attachment-form.component.html',
  styleUrls: ['./attachment-form.component.css']
})
export class AttachmentFormComponent implements OnInit {
  @Input() parentId: number;
  @Input() parent: string;
  @Input() description: string;
  @Input() title: string;
  @Input() fileFilter: string;
  @Input() fileSize: number;
  @Input() uploadType: string;
  private navigateUrl: string;
  public uploader: FileUploader;

  private attSubscription: Subscription;

  private authHeader: Array<{
    name: string;
    value: string;
  }> = [];

  constructor(private attachedFileService: AttachedFileService, private attachmentService: AttachmentService, private router: Router, private layoutService: LayoutService) {}

  ngOnInit() {
    this.uploaderConfig();
    if (this.uploadType === 'onSave') {
      this.attSubscription = this.attachmentService.uploadAttachments$.subscribe(options => {
        if (options) {
          this.parent = options['parent'];
          this.parentId = options['parentId'];
          this.navigateUrl = options['navigateUrl'];

          this.uploader.setOptions({
            url: URL + this.parent + '/' + this.parentId + '/' + this.fileFilter + '/' + this.fileSize,
            additionalParameter: { parentId: this.parentId, parent: this.parent, description: this.description, title: this.title },
            headers: this.authHeader
          });

          // if (this.uploader.queue.length > 0) {
          //   this.layoutService.flashMsg({ msg: 'Please wait files are uploading.', msgType: 'success' });
          // }

          this.uploader.uploadAll();
        }
      });
    }
  }

  /**
   *
   * Setting the Configuiration of ng2-file-uploader
   * @memberof AttachmentFormComponent
   */
  public uploaderConfig() {
    // Setting authentication by given the jwt token in headers
    this.authHeader.push({ name: 'token', value: localStorage.getItem('token') });

    //Setting the default file size to 1MB
    if (!this.fileSize) {
      this.fileSize = 1;
    }

    // Given api url,with query params, additional parematers for atatchment and authentication header
    this.uploader = new FileUploader({
      url: URL + this.parent + '/' + this.parentId + '/' + this.fileFilter + '/' + this.fileSize,
      additionalParameter: { parentId: this.parentId, parent: this.parent, description: this.description, title: this.title },
      headers: this.authHeader
    });

    // With no credentials
    this.uploader.onBeforeUploadItem = item => {
      item.withCredentials = false;
    };
    this.uploader.onProgressItem = item => {
      if (item.file.size / 1024 / 1024 > this.fileSize) {
        item['error'] = true;
        item['errorStatus'] = 403;
        item.cancel();
      } else if (!this.validateFileType(item.file) && this.fileFilter) {
        item['error'] = true;
        item['errorStatus'] = 400;
        item.cancel();
        
      }
    };

    // setting the error when itemn is not uploaded
    this.uploader.onErrorItem = (item, response, status) => {
      item['error'] = true;
      item['errorStatus'] = status;
    };

    // in case of item successfully uploaded
    this.uploader.onSuccessItem = (item, response, status, headers) => {
      item['error'] = false;
      this.attachedFileService.setIsAdded(true);
    };
    this.uploader.onCompleteAll = () => {
      this.layoutService.flashMsg({ msg: 'Files uploading completed.', msgType: 'success' });
      if (this.navigateUrl) {
        this.router.navigate([this.navigateUrl]);
      }
    };
    this.uploader.onProgressAll = () => {
      if (this.uploadType === 'onSave') {
      }
    };
  }

  validateFileType(file) {
    let isAllow = false;
    let _fileFilter = this.fileFilter;
    // extension of existing file
    const ext = path.extname(file.name).replace(' ', '');
    const mimetype = file.mimetype;
    // remove spaces
    if (_fileFilter) {
      _fileFilter = _fileFilter.replace(' ', '');
      let _fileFilterArray = _fileFilter.split(',');

      _fileFilterArray.forEach(element => {
        if (ext === element) {
          isAllow = true;
          return;
        }
      });
    }

    return isAllow;
  }

  ngOnDestroy() {
    if (this.attSubscription) {
      this.attSubscription.unsubscribe();
    }
  }
}
