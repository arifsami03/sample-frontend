import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

/**
 * HOW TO USE IT
 * 
 * Examples
 *    Display all attrbiutes: <shared-detail-view [data]="yourDataObject" [labels]="yourLabelsObject"></shared-detail-view>
 *    Display few attributes: <shared-detail-view [data]="evaluationSheet" [labels]="yourLabelsObject" [showOnly]="['field1', 'field4', 'field7']"></shared-detail-view>
 *    Full Use: 
 *      <shared-detail-view 
 *          [data]="evaluationSheet" 
 *          [labels]="yourLabelsObject" 
 *          [showOnly]="['field1', 'field4', 'field7']" 
 *          [labelWidth]="20" 
 *          [separatorWidth]="25" 
 *          [maxWordsLength]="90"
 *          [showMoreLessLink]="false">
 *      </shared-detail-view>
 */
@Component({
  selector: 'shared-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.css']
})
export class DetailViewComponent implements OnInit {

  /**
   * DATA COMING FROM THAT COMPONENT (COMPULSORY)
   */
  @Input() data;

  /**
   * LABELS IN THE MODEL (COMPULSORY)
   */
  @Input() labels;

  /**
   * FLEX WIDTH FOR THE LABELS (OPTIONAL)
   */
  @Input() labelWidth = 20;

  /**
   * Separator width is in pixel
   * WIDTH BETWEEN LABEL AND DATA (OPTIONAL)
   */
  @Input() separatorWidth = 40;

  /**
   * MAXIMUM WORDS FOR AN ATTRIBUTE (OPTIONAL)
   */
  @Input() maxWordsLength = 50;

  /**
   * SHOW (more/less) LINK WHERE ATTRIBUTE WORDS LENGTH > maxWordsLength (OPTIONAL)
   */
  @Input() showMoreLessLink = true;

  /**
   * SPECIFY LABELS (AND DATA) YOU WANT TO SHOW (OPTIONAL)
   */
  @Input() showOnly: string[];


  public separatorWidthPX: string;

  /**
   * SAVING LABELS NAME OR KEYS IN ARRAY
   */
  public keys: string[] = [];

  /**
   * CHECKING WHETHER ATTRIBUTE DATA LENGTH > maxWordsLength OR NOT
   */
  public isLengthyData = [];

  /**
   * SAVING ATTRIBUTES DATA (NO OF WORDS WILL BE = maxWordsLength) WHERE DATA LENGTH > maxWordsLength
   */
  public lengthyData: string[] = [];

  /**
   * SHOW MORE AND LESS LINK 
   */
  public show: string[] = [];


  /**
   * Constructor
   * 
   */
  constructor() { }

  /**
   * CHECKING LABELS ARE SPECIFIED OR NOT IF SPECIFIED THEN USE THEM (THIS.showOnly) ELSE USE LABELS (this.labels)
   * 
   */
  ngOnInit() {

    if (!this.showOnly) {
      this.showOnly = [];
      for (var key in this.labels) {
        this.showOnly.push(key);
      }
    }

    this.prepareKeys();

    this.separatorWidthPX = this.separatorWidth + "px";
  }

  /**
   * PUSHING LABEL KEYS OR NAMES IN ARRAY IF ATTRIBUTE DATA TYPE IS STRING THEN WE WILL CHECK WORD LENGTH
   * IF LENGHT > maxWordsLength THEN 
   *    NOW SAVE ORIGINAL DATA IN this.lengthyData[key] AND SAVE DATA WITH maxWordsLength (20,21,22,...) ...
   * LENGTH IN this.data[key] AND PUSH TRUE IN this.isLengthyData.push(true); AND PUT MORE IN this.show[key] = 'More';
   * 
   * ELSE IF LENGHT < maxWordsLength THEN PUSH FALSE IN this.isLengthyData.push(false);
   * 
   */
  prepareKeys() {

    for (var key in this.labels) {

      this.keys.push(key);

      if (typeof this.data[key] == 'string') {

        let arr = this.data[key].split(' ');

        if (arr.length > this.maxWordsLength) {

          this.lengthyData[key] = this.data[key];
          this.show[key] = 'More';
          this.isLengthyData.push(true);
          let less = this.data[key];
          let arr = less.split(' ');
          arr = arr.slice(0, this.maxWordsLength);
          let str = arr.toString();
          let result = str.replace(/,/g, " ");
          this.data[key] = result;
        }
        else {
          this.isLengthyData.push(false);
        }
      }
      else {
        this.isLengthyData.push(false);
      }
    }
  }

  /**
   * IT WILL BE CALLED ON CLICK OF MORE OR LESS LINK
   * IF this.show[key] === 'More'
   *    SHOW MORE DATA AND SET this.show[key] = 'Less';
   * ELSE IF this.show[key] === 'Less'
   *    SHOW LESS DATA AND SET this.show[key] = 'Less';
   */
  showDescription(key) {
    if (this.show[key] === 'More') {
      this.show[key] = 'Less';
      let temp = this.data[key];
      this.data[key] = this.lengthyData[key];
      this.lengthyData[key] = temp;
    }
    else if (this.show[key] === 'Less') {
      this.show[key] = 'More';
      let temp = this.data[key];
      this.data[key] = this.lengthyData[key];
      this.lengthyData[key] = temp;
    }
  }
}
