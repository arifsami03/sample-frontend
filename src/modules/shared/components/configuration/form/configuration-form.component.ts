import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

import { ConfigurationService } from '../../../services';
import { ConfigurationModel } from '../../../models';
import { LayoutService } from '../../../../layout/services';
import { GLOBALS } from '../../../../app/config/globals';
import { ElementSchemaRegistry } from '@angular/compiler';

@Component({
  selector: 'settings-configuration-form',
  templateUrl: './configuration-form.component.html',
  styleUrls: ['./configuration-form.component.css'],
})
export class ConfigurationFormComponent implements OnInit {


  public loaded: boolean = false;

  public pageTitle: string;
  public fg: FormGroup;
  public configuration = new ConfigurationModel();

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  public componentLabels = ConfigurationModel.attributesLabels;

  public globalConfigurations = GLOBALS.configurations;

  public selectedConfiguration: any = [];

  public hasParent: boolean;
  public parentLabel: string;
  public parentList: ConfigurationModel[];



  constructor(
    public configurationService: ConfigurationService,
    public layoutService: LayoutService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<ConfigurationFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    if (this.data.action == 'Update') {
      this.getOneRecord(this.data.value);
    }
  }

  private initializePage() {
    this.fg = this.fb.group(new ConfigurationModel().validationRules());
    this.selectedConfiguration = this.globalConfigurations.find(element => {
      return element.key == this.data.key;
    });
    let loadAvailableOptions = this.fg.get('configurationOptions') as FormArray;

    this.selectedConfiguration.options.forEach(element => {
      loadAvailableOptions.push(this.addAvailableConfigOption(element));

    });

    if (this.selectedConfiguration.hasOwnProperty('parent')) {
      let parent = this.selectedConfiguration.parent;
      this.hasParent = true;
      this.parentLabel = parent.label;
      this.getParentList(parent.parentKey);
    }

  }

  private getParentList(key: string) {
    this.configurationService.findAttributesList(key).subscribe(
      response => {
        this.parentList = response;
      },
      error => {
        console.log(error);
      }
    )
  }

  getOneRecord(id: number) {
    this.configurationService.getOneRecord(this.data.key, id).subscribe(
      response => {
        this.configuration = response;
        this.fg.patchValue(this.configuration);

        // this.employee = response;
        // this.employeeCopy = JSON.parse(JSON.stringify(this.employee));
      },
      error => console.log(error),
      () => {
        this.loaded = true;

      }
    );
  }

  addAvailableConfigOption(element): FormGroup {
    return this.fb.group({
      id: null,
      key: element.key,
      label: element.label,
      value: ['', element.required ? Validators.required : ''],
      required: element.required ? element.required : false
    });
  }

  private performValidations(item) {

    return new Promise((resolve, reject) => {

      if (this.selectedConfiguration['validation']) {

        if (this.selectedConfiguration['validation'] == 'int') {

          if (!Number.isInteger(Number(item.value))) {
            this.fg.controls['value'].setErrors({ notInteger: true });
            reject({ validationError: 'Provided value is not a number' })
          } else {
            resolve(true);
          }
        }
      }
      else {
        resolve(true);
      }
    })
  }


  /**
   * Function to Save Configuration
   * @param item configurationModel
   */

  public saveData(item: ConfigurationModel) {



    item.key = this.selectedConfiguration.key;
    item.parentKey = (this.selectedConfiguration.parent ? this.selectedConfiguration.parent.parentKey : null);

    this.performValidations(item).then(valResult => {
      
      if (valResult) {

        // save abbreviation in UPPERCASE into database
        item.configurationOptions.forEach(element => {
          if (element.key == "abbreviation") {
            element.value = element.value.toUpperCase();
          }
        });

        if (this.data.action == 'Update') {

          this.updateData(item);

        } else {
          this.configurationService.add(item).subscribe(response => {
            this.success = true;
            this.successMessage = response.success;
            if (item.key == 'natureOfWork') {
              this.layoutService.flashMsg({ msg: 'Nature of Work has been created.', msgType: 'success' });
            } else if (item.key == 'instrumentType') {
              this.layoutService.flashMsg({ msg: 'Instrument Type has been created.', msgType: 'success' });
            } else {
              this.layoutService.flashMsg({ msg: this.selectedConfiguration.label + ' has been created.', msgType: 'success' });
            }

            this.dialogRef.close(true);
          },
            error => {
              console.log(error);
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              this.dialogRef.close(false);

            }
          );
        }

      }
    }).catch(error => console.log(error))


  }

  updateData(item: ConfigurationModel) {
    this.configurationService.update(this.data.value, item).subscribe(
      response => {
        this.success = true;
        this.successMessage = response.success;
        if (item.key == 'natureOfWork') {
          this.layoutService.flashMsg({ msg: 'Nature of Work has been updated.', msgType: 'success' });
        } else if (item.key == 'instrumentType') {
          this.layoutService.flashMsg({ msg: 'Instrument Type has been updated.', msgType: 'success' });
        } else {
          this.layoutService.flashMsg({ msg: this.selectedConfiguration.label + ' has been updated.', msgType: 'success' });
        }
        this.dialogRef.close(true);
      },
      error => {
        console.log(error);
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        this.dialogRef.close(false);

      }
    );
  }
  onNoClick() {
    this.dialogRef.close(false);
  }
}
