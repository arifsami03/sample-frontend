import { Component, Inject, Input, OnInit, EventEmitter, Output, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort, PageEvent } from '@angular/material';
import { GLOBALS } from '../../../app/config/globals';
import { IHeaderAttributes, IActionButton, ITableAttributes } from '../../interfaces';
import { DataTableService } from '../../services';

/**
 * How to use?
 * 
 * [data] pass the data of the required list 
 * [headerAttributes] pass the header atatributes array
 * [actionButtons] pass the action button array
 * 
 */
@Component({
  selector: 'shared-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css'],
  // providers : [DataTableService]
})
export class DataTableComponent {

  public displayedColumns: string[] = [];

  /**
   * For the display of columns, label and header width
   */
  headerAttributes: IHeaderAttributes[];

  /**
   * For action buttons
   */
  actionButtons: IActionButton[];


  /**
   * Update and Delete Emitter
   */
  @Output() deleteRecord = new EventEmitter<number>();
  @Output() updateRecord = new EventEmitter<number>();
  @Output() setTableAttributes = new EventEmitter<ITableAttributes>();


  /**
   * For the data to be displayed
   */
  data: any[];
  dataLength: number;

  /**
   * DataSource for mat table
   */
  public dataSource: any;


  /**
   * Getting pageSize and pageSizeOptions  from GLOBAL
   */
  public pageSize: number = GLOBALS.dataTable.pageSize;
  public pageSizeOptions: number[] = GLOBALS.dataTable.pageSizeOptions;


  /**
   * For pagination
   */
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /**
   * For Sorting
   */
  @ViewChild(MatSort) sort: MatSort;

  /**
   * For Server Side pagination initializing offset and limits 
   */
  public tableAttributes: ITableAttributes = { offset: 0, limit: this.pageSize };

  /**
   * Constructor
   * 
   */
  constructor(private dts: DataTableService) {

    this.dts.refreshDataTable$.subscribe(res => {

      this.headerAttributes = res['headerAttributes'];
      this.actionButtons = res['actionButtons'];
      this.data = res['data'];
      this.dataLength = res['dataLength'];

      this.tableSettings();

    });
  }

  /**
   * Setting the table attributes , preparing displayedColumns
   */
  tableSettings() {
    this.prepareDisplayedColumns();
    this.dataSource = new MatTableDataSource<any>(this.data);

  }

  /**
   * For Material Displayed Columns
   */
  prepareDisplayedColumns() {
    this.displayedColumns = [];
    this.headerAttributes.forEach(element => {
      this.displayedColumns.push(element.field)
    });
  }

  /**
   * Triggireng on the page change
   * @param event 
   */
  public pageEvent(event: PageEvent) {

    let _offset = event.pageIndex * event.pageSize;
    let _limit = event.pageSize;


    // Initializing offset and limit here
    this.tableAttributes.offset = _offset;
    this.tableAttributes.limit = _limit;

    // Setting the page size 
    this.pageSize = event.pageSize;

    // Now emit the change
    this.setTableAttributes.emit(this.tableAttributes);
  }


  /**
  * Apply filter and search in data grid
  */
  applyFilter(filterValue: string | number) {

    this.tableAttributes.search = filterValue;

    // Restting offset
    this.tableAttributes.offset = 0;

    // Now emit the change
    this.setTableAttributes.emit(this.tableAttributes)

    // Setting the paginator to display only the related data number of records
    this.dataSource.paginator = this.paginator;
  }

  /**
   * Sort data table 
   */
  sortData(event) {
    console.log('sort event  : ', event);

    // Setting the sort properties
    this.tableAttributes.sortAttribute = event.active;
    this.tableAttributes.sortDirection = event.direction;

    // Restting offset
    this.tableAttributes.offset = 0;

    // Now emit the change
    this.setTableAttributes.emit(this.tableAttributes)

    // Setting the paginator to display only the related dat number of records
     this.paginator.firstPage();

  }


  /**
   * Will call when delete action button  clicked and emit that record id
   * @param id 
   */
  deleteClicked(id: number) {
    this.deleteRecord.emit(id);
  }

  /**
   * Will call when update action button  clicked and emit that record id
   * @param id 
   */
  updateClicked(id: number) {
    this.updateRecord.emit(id);
  }

}
