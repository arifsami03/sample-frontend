import { Component, Inject, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DocStateManagerService } from '../../../services';
import { CampusPreRegistrationService } from '../../../../campus/services';
import { LayoutService } from '../../../../layout/services';

import { ConfirmDialogComponent } from '../../confirm-dialog/confirm-dialog.component';
import { AttachEvalSheetDialogComponent } from '../state-rule-actions/attach-eval-sheet/attach-eval-sheet-dialog.component';
import { CommentForApproveRejectDialogComponent } from '../state-rule-actions/comment-for-approve-reject/comment-for-approve-reject-dialog.component';
import { GLOBALS } from '../../../../app/config/globals';


@Component({
  selector: 'wf-states',
  templateUrl: './wf-states.component.html',
  styleUrls: ['./wf-states.component.css'],
})
export class WFStatesComponent {

  @Input() stateId: number;
  @Input() parent: string;
  @Input() parentId: number;
  @Output() onChangeStateId = new EventEmitter<any>();
  @Input() WFId: string;

  private stateFlowRules = GLOBALS.StateFlowRules;

  public wfStates: any;

  private postData: any;

  constructor(
    private layoutService: LayoutService,
    public dialog: MatDialog,
    private docStateManagerService: DocStateManagerService,
    private campusPreRegistrationService: CampusPreRegistrationService) {

  }

  ngOnInit(): void {
    this.loadWFStateFlows();
  }

  /**
   * Load next WF State Flow
   * 
   */
  private loadWFStateFlows() {

    //TODO:high: should not send userId from front-end. At backend system should automatic pick the logged in user.
    let postData = {
      userId: Number(localStorage.getItem('id')),
      stateId: this.stateId,
      WFId:this.WFId
    };
    this.docStateManagerService.getListOfActions(postData).subscribe(response => {
      if (response) {
        this.wfStates = response;
      }
    }, error => {
      console.log(error);
    });
  }

  /**
   * Change State Id 
   * 
   * @param id State Id to Change
   * @param state Current State of Workflow
   */
  public initStateUpdation(toStateId, state) {

    this.postData = {
      id: toStateId,
      state: state,
      WFId:this.WFId,
      previousStateId: this.stateId,
      stateId: toStateId,
      parent: this.parent,
      parentId: this.parentId
    };


    this.isRuleExists(state);
  }

  /**
   * Check whether the rule against the work state flow exists or not.
   * 
   * @param toState Change state
   */
  private isRuleExists(toState) {

    let isExists = false;

    this.stateFlowRules.forEach(item => {

      if (item.workFlow == this.WFId) {

        item.rules.forEach(ruleItem => {

          if (ruleItem.toState == toState) {

            isExists = true;

            // Call action function to perform action
            this.initAction(ruleItem);
          }
        });
      }

    });

    // If there is no rule then jus update state.
    if (!isExists) {
      this.updateState().then(res => {
        //Do somthing here if you need to.
      });
    }
  }

  /**
  * Initialize and perform action before state id change.
  * 
  * @param action Action to Perform Before state id change
  */
  initAction(ruleItem) {

    if (ruleItem.action == 'confirm-reject') {
      this.confirmReject(ruleItem);
    }
    else if (ruleItem.action == 'attach-evaluation-sheet') {
      this.attachEvaluationSheet(ruleItem);
    }
    else if (ruleItem.action == 'remarks-for-approve') {

      this.commentForApproveOrReject(ruleItem);

    }
    else if (ruleItem.action == 'remarks-for-reject') {

      this.commentForApproveOrReject(ruleItem);

    }
  }

  /**
   * Update state & emit response to host with status and posted data
   * 
   */
  private updateState() {

    return new Promise((resolve, reject) => {

      this.docStateManagerService.updateStateId(this.postData).subscribe(response => {

        if (response) {

          this.stateId = this.postData.id;

          this.onChangeStateId.emit({ status: true, state: this.postData.state });

          this.ngOnInit();
          // this.layoutService.flashMsg({ msg: 'Work flow state is changed.', msgType: 'success' });

          resolve(response);

        }
      },
        error => {
          this.onChangeStateId.emit(false);
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          reject(error);
        },
      );
    });
  }

  /**
   * A method to show dailoge for confirmation rejection state.
   * 
   * @param ruleItem object
   */
  private confirmReject(ruleItem) {

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '500px',
      data: { message: ruleItem.options.message }
    });

    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.updateState().then(res => {
          //Do somthing here if you need to.
        });
      }
    });
  }

  /**
   * To open dialog which contain the selection of evaluation sheet and need to forward to evaluator.
   * 
   * @param ruleItem object
   */
  private attachEvaluationSheet(ruleItem) {

    let dialogRef = this.dialog.open(AttachEvalSheetDialogComponent, {
      width: '500px',
      data: { parentId: this.parentId }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.updateState();
      }
    });
  }

  /**
   * To open dialog which contain the TextBox to enter Comments.
   * 
   * @param ruleItem object
   */
  private commentForApproveOrReject(ruleItem) {

    let dialogRef = this.dialog.open(CommentForApproveRejectDialogComponent, {
      width: '700px',
      // height: '400px',
      data: { parentId: this.parentId, ruleItem: ruleItem }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.updateState();
      }
    });
  }



}
