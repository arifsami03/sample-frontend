import { Component, Inject, Input, Output, EventEmitter } from '@angular/core';
import { WorkFlowLogService } from '../../../services';


@Component({
  selector: 'wf-log',
  templateUrl: './wf-log.component.html',
  styleUrls: ['./wf-log.component.css']
})
export class WFLogComponent {

  @Input() parent: string;
  @Input() parentId: number;

  constructor(private workFlowLogService: WorkFlowLogService) { }

  ngOnInit(): void {

    this.workFlowLogService.getLog(this.parent, this.parentId).subscribe(
      response => {
        if (response) {
          
        }

      },
      error => {
        console.log(error);

        //        this.loaded = true;
      },
      () => {
        //      this.loaded = true;
      }
    );

  }


}
