import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { CampusPostRegistrationService } from '../../../../../campus/services';

@Component({
  selector: 'comment-for-approve-reject-dialog',
  templateUrl: './comment-for-approve-reject-dialog.component.html',
  styleUrls: ['./comment-for-approve-reject-dialog.component.css'],
})
export class CommentForApproveRejectDialogComponent implements OnInit {

  public loaded: boolean = true;
  public fg: FormGroup;
  public remarks = new FormControl();


  /**
   * Construstor 
   * 
   * @param router Router
   * @param route ActivatedRoute
   * @param fb FormBuilder
   * @param dialogRef MatDialogRef<CommentForApproveRejectDialogComponent>
   * @param data any
   */
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<CommentForApproveRejectDialogComponent>,
    private campusPostRegistrationService: CampusPostRegistrationService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  /**
   * ngOnInit
   * 
   */
  ngOnInit() {
  }



  /**
   * Add Remarks
   * 
   */
  public addRemarks() {

    let postData = {};

    if (this.data.ruleItem.action == 'remarks-for-approve')
    {
      postData = {
        MOUApproveRemarks: this.remarks.value,
      }
    }
    else if(this.data.ruleItem.action == 'remarks-for-reject')
    {
      postData = {
        rejectRemarks: this.remarks.value,
      }

    }
      
    this.campusPostRegistrationService.updateCRApplication(this.data.parentId,postData).subscribe(result => {
      if (result) {
        this.dialogRef.close(true);
      }
    });

  }

  /**
   * Close dialog.
   */
  public cancel() {
    this.dialogRef.close(false);
  }
}
