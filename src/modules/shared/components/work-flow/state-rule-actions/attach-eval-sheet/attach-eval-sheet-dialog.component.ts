import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { FormBuilder, FormGroup } from '@angular/forms';

import { EvalSheetAssignmentModel } from '../../../../models';
import { ESCategoryService, EvaluationSheetService } from '../../../../../settings/services';
import { CampusPostRegistrationService } from '../../../../../campus/services';
import { ESCategoryModel, EvaluationSheetModel } from '../../../../../settings/models';

@Component({
  selector: 'attach-eval-sheet-dialog',
  templateUrl: './attach-eval-sheet-dialog.component.html',
  styleUrls: ['./attach-eval-sheet-dialog.component.css'],
  providers: [ESCategoryService, EvaluationSheetService]
})
export class AttachEvalSheetDialogComponent implements OnInit {

  public loaded: boolean = false;
  public fg: FormGroup;

  public esCatList: EvaluationSheetModel[];
  public evalSheetList: EvaluationSheetModel[];

  public componentLabels = EvalSheetAssignmentModel.attributesLabels;


  /**
   * Construstor 
   * 
   * @param router Router
   * @param route ActivatedRoute
   * @param fb FormBuilder
   * @param dialogRef MatDialogRef<AttachEvalSheetDialogComponent>
   * @param esCatService ESCategoryService
   * @param evalSheetService EvaluationSheetService
   * @param data any
   */
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<AttachEvalSheetDialogComponent>,
    private esCatService: ESCategoryService,
    private campusPostRegistrationService: CampusPostRegistrationService,
    private evalSheetService: EvaluationSheetService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  /**
   * ngOnInit
   * 
   */
  ngOnInit() {
    this.initializeForm();
  }

  /** 
   * Initialize form
  */
  private initializeForm() {

    this.fg = this.fb.group(new EvalSheetAssignmentModel().validationRules());

    this.findESCategories();

  }

  /**
   * Find evaluation sheet categories
   * 
   */
  private findESCategories() {

    this.esCatService.findCategoriesByES().subscribe(result => {
      this.esCatList = result;
      this.loaded = true;
    })

  }



  /**
   * Attach Evaluation Sheet
   * 
   * @param item object
   */
  public attachEvalSheet(item) {

  let postData={};
  for (let i = 0; i < this.esCatList.length; i++) {

     if(item.esCategoryId == this.esCatList[i]['ESCategoryId'])
     {
      postData = {
        status: 'draft',
        CRApplicationId: this.data.parentId,
        ESCategoryId: item.esCategoryId,
        ESId: this.esCatList[i].id,
        ESTitle:this.esCatList[i].title
      }
     }
    
  }
    
    this.campusPostRegistrationService.forwardForAnalysis(postData).subscribe(result => {
      if(result){
        this.dialogRef.close(true);  
      }
    });

  }

  /**
   * Close dialog.
   */
  public cancel() {
    this.dialogRef.close(false);
  }
}
