import { Component, Inject, Input, OnInit, EventEmitter, Output, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { GLOBALS } from '../../../app/config/globals';
import { IHeaderAttributes, IActionButton } from '../../interfaces';

/**
 * How to use?
 * 
 * [data] pass the data of the required list 
 * [headerAttributes] pass the header atatributes array
 * [actionButtons] pass the action button array
 * 
 */
@Component({
  selector: 'shared-local-data-table',
  templateUrl: './local-data-table.component.html',
  styleUrls: ['./local-data-table.component.css']
})
export class LocalDataTableComponent implements AfterViewInit {

  public displayedColumns: string[] = [];

  /**
   * For the display of columns, label and header width
   */
  @Input() headerAttributes: IHeaderAttributes[];


  /**
   * For action buttons
   */
  @Input() actionButtons: IActionButton[];


  /**
   * Update and Delete Emitter
   */
  @Output() deleteRecord = new EventEmitter<number>();
  @Output() updateRecord = new EventEmitter<number>();


  /**
   * For the data to be displayed
   */
  @Input() data: any[];




  /**
   * DataSource for mat table
   */
  public dataSource: any;


  /**
   * Getting pageSize and pageSizeOptions  from GLOBAL
   */
  public pageSize: number = GLOBALS.dataTable.pageSize;
  public pageSizeOptions: number[] = GLOBALS.dataTable.pageSizeOptions;

  public test = false;

  /**
   * For pagination
   */
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /**
   * For Sorting
   */
  @ViewChild(MatSort) sort: MatSort;


  /**
   * Constructor
   * 
   */
  constructor() {
  }

  ngAfterViewInit() {

    this.prepareDisplayedColumns();
    this.dataSource = new MatTableDataSource<any>(this.data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  /**
   * Will call when delete action button  clicked and emit that record id
   * @param id 
   */
  deleteClicked(id: number) {
    this.deleteRecord.emit(id);
  }
  /**
   * Will call when update action button  clicked and emit that record id
   * @param id 
   */
  updateClicked(id: number) {
    this.updateRecord.emit(id);
  }


  /**
  * Apply filter and search in data grid
  */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Sort data table 
   */
  sortData() {
    this.dataSource.sort = this.sort;
  }
  /**
   * For Material Displayed Columns
   */
  prepareDisplayedColumns() {
    this.headerAttributes.forEach(element => {
      this.displayedColumns.push(element.field)
    });
  }
}
