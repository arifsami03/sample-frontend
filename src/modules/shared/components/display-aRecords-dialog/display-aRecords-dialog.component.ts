import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

/**
 * Display Associated records Dialog Component
 *
**/


@Component({
  selector: 'shared-display-aRecords-dialog',
  templateUrl: './display-aRecords-dialog.component.html',
  styleUrls: ['./display-aRecords-dialog.component.css']
})
export class DisplayARecordsDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<DisplayARecordsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }
}
