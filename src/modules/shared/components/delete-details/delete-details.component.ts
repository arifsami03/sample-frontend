import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'shared-delete-details',
  templateUrl: './delete-details.component.html',
  styleUrls: ['./delete-details.component.css']
})
export class DeleteDetailsComponent implements OnInit {


  public showDelButton;

  @Input() data;

  @Output() confirm = new EventEmitter<boolean>();;

  ngOnInit() {

    this.showDelButton = true;

    //If children exists do not show delete button
    this.data['children'].forEach(element => {

      if(element['records'] && element['records'].length > 0)
      {
        this.showDelButton = false;
      }
    });

  }

  public returnConfirmation(confirmation: boolean) {

    this.confirm.emit(confirmation);

  }

}
