import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'shared-delete-items',
  templateUrl: './delete-items.component.html',
  styleUrls: ['./delete-items.component.css']
})
export class DeleteItemsComponent {

  public marginLeft: string;

  @Input() dataArray = [];


}
