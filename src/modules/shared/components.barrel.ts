// Import components and services etc here
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatSelectModule,
  MatFormFieldModule,
  MatIconModule,
  MatDialogModule,
  MatButtonModule,
  MatProgressSpinnerModule,
  MatInputModule,
  MatTableModule,
  MatCardModule,
  MatDividerModule,
  MatListModule,
  MatSidenavModule,
  MatRadioModule,
  MatProgressBarModule,
  MatChipsModule,
  MatPaginatorModule,
  MatSortModule,
  MatTooltipModule,
  MatExpansionModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BaseService, DownloadPdfService, DocStateManagerService, WorkFlowLogService, AttachmentService, AttachedFileService } from './services';

import {
  DBWLinkComponent,
  ConfigurationComponent,
  ConfigurationFormComponent,
  CMatErrorComponent,
  CMatSuccessComponent,
  ConfirmDialogComponent,
  PageLoaderComponent,
  AttachmentFormComponent,
  AttachmentListComponent,
  AlertDialogComponent,
  DetailViewComponent,
  SmallOverlayComponent,
  MatCardLoaderComponent,
  WFStatesComponent,
  WFLogComponent,
  ListViewComponent,
  AttachEvalSheetDialogComponent,
  CommentForApproveRejectDialogComponent,
  LocalDataTableComponent,
  DataTableComponent,
  DeleteDetailsComponent,
  DeleteItemsComponent,
  DisplayARecordsDialogComponent
} from './components';
import { FileUploadModule } from 'ng2-file-upload';

import { PermissionDirective, AutofocusDirective } from './directives';
import { CampusPreRegistrationService, CampusPostRegistrationService } from '../campus/services';

import { CapitalizePipe } from './helper';

export const __IMPORTS = [
  FormsModule,
  ReactiveFormsModule,
  MatFormFieldModule,
  MatSelectModule,
  MatIconModule,
  MatDialogModule,
  MatButtonModule,
  MatInputModule,
  FlexLayoutModule,
  MatTableModule,
  MatCardModule,
  MatDividerModule,
  FlexLayoutModule,
  MatProgressSpinnerModule,
  MatListModule,
  MatSidenavModule,
  MatRadioModule,
  FileUploadModule,
  MatProgressBarModule,
  MatChipsModule,
  MatPaginatorModule,
  MatSortModule,
  MatTooltipModule,
  MatExpansionModule
];

export const __DECLARATIONS = [
  ConfigurationComponent,
  DBWLinkComponent,
  ConfigurationFormComponent,
  CMatErrorComponent,
  CMatSuccessComponent,
  ConfirmDialogComponent,
  AttachEvalSheetDialogComponent,
  AlertDialogComponent,
  PageLoaderComponent,
  WFStatesComponent,
  CapitalizePipe,
  AttachmentFormComponent,
  AttachmentListComponent,
  PermissionDirective,
  AutofocusDirective,
  DetailViewComponent,
  SmallOverlayComponent,
  MatCardLoaderComponent,
  WFLogComponent,
  ListViewComponent,
  CommentForApproveRejectDialogComponent,
  LocalDataTableComponent,
  DataTableComponent,
  DeleteDetailsComponent,
  DeleteItemsComponent,
  DisplayARecordsDialogComponent
];

export const __PROVIDERS = [BaseService, DownloadPdfService, DocStateManagerService, CampusPostRegistrationService, CampusPreRegistrationService, WorkFlowLogService, AttachmentService, AttachedFileService];
export const __ENTRY_COMPONENTS = [ConfigurationFormComponent, ConfirmDialogComponent, AttachEvalSheetDialogComponent, AlertDialogComponent, CommentForApproveRejectDialogComponent, DisplayARecordsDialogComponent];
