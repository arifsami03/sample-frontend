import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from './base.service';

@Injectable()
export class WorkFlowLogService extends BaseService {
  private routeURL: String = 'settings/workFlowLog';

  constructor(protected http: Http) {
    super(http)
  }


  /**
   * Get List Of Actions
   */
  getLog(parent: string, parentId: number) {
    return this.__get(`${this.routeURL}/getLog/${parent}/${parentId}`).map(data => {
      return data.json();
    });
  }


}
