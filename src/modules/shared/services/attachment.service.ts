import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from './base.service';
import { AttachmentModel } from '../models';

@Injectable()
export class AttachmentService extends BaseService {
  private routeURL: String = 'settings/attachments';

  /**
   * upload
   */
  private uploadAttachmentsSource = new Subject<object>();
  uploadAttachments$ = this.uploadAttachmentsSource.asObservable();
  /**
   * set parent id
   */
  private setParentIdSource = new Subject<number>();
  setParentId$ = this.setParentIdSource.asObservable();

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Triggiring upload
   * @param {boolean} upload
   * @memberof AttachedFileService
   */
  uploadAttachments(options: object) {
    this.uploadAttachmentsSource.next(options);
  }
  /**
   * Setting parentId
   * @param {number} _parentId
   * @memberof AttachedFileService
   */
  setParentId(_parentId: number) {
    this.setParentIdSource.next(_parentId);
  }
  /**
   * Get All records
   */
  index(): Observable<AttachmentModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <AttachmentModel[]>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: AttachmentModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: AttachmentModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }
}
