import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http, RequestOptions, ResponseContentType, Headers } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from './base.service';
import { AttachedFileModel } from '../models';
import 'rxjs';
import { environment } from 'environments/environment';

@Injectable()
export class AttachedFileService extends BaseService {
  private routeURL: String = 'settings/attachedFiles';
  /**
   * isAdded
   */
  private setIsAddedSource = new Subject<boolean>();
  setIsAdded$ = this.setIsAddedSource.asObservable();

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Setting is Added
   * @param {boolean} isAdded
   * @memberof AttachedFileService
   */
  setIsAdded(isAdded: boolean) {
    this.setIsAddedSource.next(isAdded);
  }

  /**
   * Get All records
   */
  index(): Observable<AttachedFileModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <AttachedFileModel[]>data.json();
    });
  }
  /**
   * Get single record
   */
  list(_queryParams: any): Observable<AttachedFileModel[]> {
    return this.__get(`${this.routeURL}/list/${_queryParams['parent']}/${_queryParams['parentId']}`).map(data => {
      return <AttachedFileModel[]>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: AttachedFileModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: AttachedFileModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }
  /**
   *
   *
   * @param {number} id
   * @returns downloaded file
   * @memberof AttachedFileService
   */
  download(item: any) {
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    let options = new RequestOptions({ responseType: ResponseContentType.Blob, headers: headers });
    return this.http
      .post(`${environment.backendAPIURL}/${this.routeURL}/download`, item, options)
      .map(res => res.blob())
      .catch(this.handleError);
  }
}
