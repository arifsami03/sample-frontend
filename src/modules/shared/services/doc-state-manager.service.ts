import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from './base.service';

@Injectable()
export class DocStateManagerService extends BaseService {
  private routeURL: String = 'settings/docStateManagers';

  constructor(protected http: Http) {
    super(http)
  }


  /**
   * Get List Of Actions
   */
  getListOfActions(postData: any) {
    return this.__post(`${this.routeURL}/getListOfActions`, postData).map(data => {
      return data.json();
    });
  }

  updateStateId(postData: any) {
    return this.__post(`${this.routeURL}/updateStateId`, postData)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }

  getStateID(postData: any) {
    return this.__post(`${this.routeURL}/getStateId`, postData).map(data => {
      return data.json();
    });
  }
  getState(id: number) {
    return this.__get(`${this.routeURL}/getState/${id}`).map(data => {
      return data.json();
    });
  }
  getCurrentStateId(postData: any) {
    return this.__post(`${this.routeURL}/getCurrentStateId`, postData).map(data => {
      return data.json();
    });
  }

}
