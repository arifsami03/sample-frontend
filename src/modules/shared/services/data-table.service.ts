import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs';
import 'rxjs';

@Injectable()
export class DataTableService {

  private refreshDataTableSource = new Subject<any>();
  refreshDataTable$ = this.refreshDataTableSource.asObservable();

  constructor() {
  }

  refreshDataTable(dta: any) {
    this.refreshDataTableSource.next(dta);
  }

  
}
