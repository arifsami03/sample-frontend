import 'rxjs/add/operator/map';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { BaseService } from './base.service'; 
import { ConfigurationModel } from '../models';

@Injectable()
export class ConfigurationService extends BaseService {
  private _url: String = 'settings/configurations';

  constructor(protected http: Http) {
    super(http)
  }

  getCities(): Observable<ConfigurationModel[]> {
    return this.__get(`${this._url}/city`)
      .map(data => {
        return <ConfigurationModel[]>data.json();
      })
      .catch(this.handleError);
  }
  getAll(key: string): Observable<ConfigurationModel[]> {
    return this.__get(`${this._url}/index/${key}`)
      .map(data => {
        return <ConfigurationModel[]>data.json();
      })
      .catch(this.handleError);
  }
  getAllSingleConf(): Observable<ConfigurationModel[]> {
    return this.__get(`${this._url}/getAllSingleConf`)
      .map(data => {
        return <ConfigurationModel[]>data.json();
      })
      .catch(this.handleError);
  }
  getOneRecord(key: string, id: number): Observable<ConfigurationModel> {
    return this.__get(`${this._url}/view/${id}`)
      .map(data => {
        return <ConfigurationModel>data.json();
      })
      .catch(this.handleError);
  }
  findConfiguration(): Observable<ConfigurationModel> {
    return this.__get(`${this._url}/findConfiguration`)
      .map(data => {
        return <ConfigurationModel>data.json();
      })
      .catch(this.handleError);
  }
  add(item: ConfigurationModel) {
    return this.__post(`${this._url}/create`, item)
      .map(data => {
        return <ConfigurationModel[]>data.json();
      })
      .catch(this.handleError);
  }
  update(id: Number, item: ConfigurationModel) {
    return this.__put(`${this._url}/update/${id}`, item).map(data => {
      return data.json();
    });
  }

  delete(id: Number) {
    return this.__delete(`${this._url}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Get All records
   */
  findAttributesList(key: string): Observable<ConfigurationModel[]> {
    return this.__get(`${this._url}/findAttributesList/${key}`).map(data => {
      return <ConfigurationModel[]>data.json();
    });
  }

  /**
   * Get All records
   */
  findChildren(id: number): Observable<ConfigurationModel[]> {
    return this.__get(`${this._url}/findChildren/${id}`).map(data => {
      return <ConfigurationModel[]>data.json();
    });
  }

  /**
   * Get All Cities of a country
   */
  findAllCities(id: number): Observable<ConfigurationModel[]> {
    return this.__get(`${this._url}/findAllCities/${id}`).map(data => {
      return <ConfigurationModel[]>data.json();
    });
  }
}
