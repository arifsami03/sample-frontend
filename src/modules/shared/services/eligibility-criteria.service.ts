import { Injectable, EventEmitter } from '@angular/core';
import {
  Http,
  Headers,
  ResponseContentType,
  RequestOptions
} from '@angular/http';
import 'rxjs/add/operator/map';
import { environment } from 'environments/environment';
// import { ProgramModel } from '../models/';
import { BaseService } from './base.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DownloadPdfService extends BaseService {
  private routeURL: String = 'shared/eligibility-criteria';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * download pdf file
   */
  // download(): Observable<Blob> {

  //   let options = new RequestOptions({
  //     responseType: ResponseContentType.Blob
  //   });

  //   let url = `${environment.backendAPIURL}/${this.routeURL}/download`;

  //   return this.http.get(url, options).map(response => {
  //     return <Blob>response.blob();
  //   })
  //     .catch(this.handleError);
  // }

  // public download(): Observable<Blob> {
  //   let url = `${environment.backendAPIURL}/${this.routeURL}/download`;

  //   let requestOptions = new RequestOptions({
  //     responseType: ResponseContentType.Blob
  //   });

  //   return this.http.get(url, requestOptions).map(response => {
  //     let blob = response.blob();
  //     let filename = 'mypdf.pdf';
  //     FileSaver.saveAs(blob, filename);
  //     return <Blob>response.blob();
  //   });
  // }

  // download(): Observable<Blob> {
  //   // let headers = {
  //   //   'Content-Type': 'application/zip',
  //   //   'Accept': 'application/zip'
  //   // };

  //   let headers = new Headers();
  //   headers.append('Content-Type', 'application/pdf');
  //   headers.append('Accept', 'application/pdf');

  //   let requestOptions = new RequestOptions({
  //     headers: headers,
  //     responseType: ResponseContentType.ArrayBuffer
  //   });

  //   let url = `${environment.backendAPIURL}/${this.routeURL}/download`;

  //   return this.http
  //     .get(url, requestOptions)
  //     .map(response => {

  //       // return <Blob>response.blob();
  //     })
  //     .catch(this.handleError);
  // }
}
