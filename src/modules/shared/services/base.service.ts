import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
import { TokenService } from '.';
import { Router } from '@angular/router';

@Injectable()
export class BaseService {
  protected errorCode: number;

  constructor(protected http: Http) {
    this.setHeaders();
  }

  private setHeaders() {
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    // this.setRequestOptions(this.__headers);
    return new RequestOptions({ headers: headers })
  }


  private setRequestOptions(params) {

    let requestOptions = this.setHeaders();

    // let sp: URLSearchParams = new URLSearchParams();
    // sp.set('appid', 'asdf');
    // sp.set('cnt', 'sss');
    // requestOptions.search = sp;

    if (params) {

      let myParams = new URLSearchParams();

      Object.keys(params).forEach((key) => {
        myParams.append(key, params[key]);
      })

      requestOptions.params = myParams;

    }

    return requestOptions;
  }


  protected __get(url, params?) {

    let reqOptions = this.setRequestOptions(params);

    return this.http.get(
      `${environment.backendAPIURL}/${url}`,
      reqOptions
    );
  }

  __put(url, putBody) {

    return this.http.put(
      `${environment.backendAPIURL}/${url}`,
      putBody,
      this.setHeaders()
    );
  }

  __post(url, postBody) {

    return this.http.post(
      `${environment.backendAPIURL}/${url}`,
      postBody,
      this.setHeaders()
    );
  }

  __delete(url) {

    return this.http.delete(
      `${environment.backendAPIURL}/${url}`,
      this.setHeaders()
    );
  }

  /**
   * handle error
   *
   * @param error Response
   */
  protected handleError(error) {

    let errorJson = error.json()
    // console.log(errorJson);

    // if (error.json()['message'] == 'jwt expired') {

    //   TokenService.instance1.setTokenMessage('jwt expired');

    // }
    // this.errorCode = errorJson.code;
    // console.log('error code : ', this.errorCode);
    return Observable.throw(errorJson || 'Server error');

    // return Observable.throw(
    //   { message: errorJson['message'], status: error.status, code: error.code } ||
    //   'Server error'
    // );


  }
}
