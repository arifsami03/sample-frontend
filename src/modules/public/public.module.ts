import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PublicRoutes } from './public.routes';
import { SharedModule } from '../shared/shared.module';
import 'hammerjs';

import {
  __IMPORTS,
  __DECLARATIONS,
  __PROVIDERS,
  __ENTRY_COMPONENTS
} from './components.barrel';

@NgModule({
  imports: [RouterModule, __IMPORTS, PublicRoutes, SharedModule],
  declarations: [__DECLARATIONS],
  providers: [__PROVIDERS],
  entryComponents: [__ENTRY_COMPONENTS],
  exports: [__DECLARATIONS, __IMPORTS]
})
export class PublicModule {}
