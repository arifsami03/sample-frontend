import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DownloadPdfService } from '../../../shared/services/eligibility-criteria.service';
import { environment } from 'environments/environment';

// const express = require('express');
// const app = express();
// let saveAs: any;
@Component({
  selector: 'app-apply-for-registration',
  templateUrl: './apply-for-registration.component.html',
  styleUrls: ['./apply-for-registration.component.css']
})
export class ApplyForRegistrationComponent implements OnInit {
  public serverUrl = environment.backendAPIURL;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private downloadService: DownloadPdfService
  ) {}

  // downloadPdf() {
  //   this.downloadService.download().subscribe(
  //     response => {
  //       // FileSaver.saveAs(response, 'eligibility-criteria.pdf');
  //     },
  //     error => {
  //       console.log(error);
  //     }
  //   );
  // }

  /**
   * ngOnInit()
   */
  ngOnInit() {}
}
