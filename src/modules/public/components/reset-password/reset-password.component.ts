import { Component, OnInit } from '@angular/core';

import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';

import { ProfileService } from '../../../profile/services';
import { ProfileModel } from '../../../profile/models';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'public-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css'],
  providers: [ProfileService]
})
export class ResetPasswordComponent implements OnInit {
  public fg: FormGroup;

  public attributesLabels = ProfileModel.attributesLabels;

  private verificationCode: string;
  public error = false;
  public VCValidated;
  public loaded = true;

  public authError;
  public authSuccess;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private profileService: ProfileService, private fb: FormBuilder) {}

  ngOnInit() {
    this.fg = this.fb.group(new ProfileModel().resetPasswordValidationRules());
    this.activatedRoute.queryParams.subscribe(queryParam => {
      this.verificationCode = queryParam.verificationCode;
      this.validateVerificationCode(this.verificationCode);
    });
  }

  /**
   * check if verification code exists
   * @param verificationCode
   */
  validateVerificationCode(verificationCode: string) {
    this.profileService.validateVerificationCode(verificationCode).subscribe(
      response => {
        this.VCValidated = true;
      },
      error => {
        this.VCValidated = false;
        this.authError = 'Your token has been expired or you have not requested to reset password.';
      }
    );
  }

  /**
   * Login If user is valid
   * @param form NgForm
   */
  resetPassword(item: ProfileModel) {
    this.loaded = false;
    item.verificationCode = this.verificationCode;

    this.profileService.resetPassword(item).subscribe(
      response => {
        this.loaded = true;

        this.authSuccess = 'Password has changed successfully.';

        setTimeout(() => {
          this.router.navigate(['home']);
        }, 2000);
      },
      error => {
        this.loaded = true;
        this.error = true;
        if (error.status === 404) {
          this.authError = 'Your token has been expired. Please make another request to reset password';
        } else {
          this.authError = 'Your token has been expired or you have not requested to reset password.';
        }
        console.log(error);
      }
    );
  }
}
