import { Component, OnInit } from '@angular/core';
import { environment } from 'environments/environment';

@Component({
  selector: 'public-eligibility-criteria',
  templateUrl: './eligibility-criteria.component.html',
  styleUrls: ['./eligibility-criteria.component.css']
})
export class EligibilityCriteriaComponent implements OnInit {
  constructor() {}
  public serverUrl = environment.backendAPIURL;
  /**
   * ngOnInit
   */
  ngOnInit() {}
}
