import { Component, OnInit, Input } from '@angular/core';
import { NgxCarousel, NgxCarouselStore } from 'ngx-carousel';

import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';

import { LoginService } from '../../../security/services';
import { ProfileService } from '../../../profile/services';
import { Login } from '../../../security/models';
import 'hammerjs';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [LoginService, ProfileService]
})
export class HomeComponent implements OnInit {
  public carouselTileItems: Array<any>;
  public carouselTile: NgxCarousel;

  public fg: FormGroup;

  public forgotPasswordFG: FormGroup;

  public attributesLabels = Login.attributesLabels;

  public formTitle = 'Forgot Password';
  public passwordChanged = false;
  public loaded = true;
  public loginInProgress = false;

  public authError: string;

  public authSuccess: string;

  public signInView: boolean = true;

  constructor(private activatedRoute: Router, private loginService: LoginService, private profileService: ProfileService, private fb: FormBuilder) { }

  ngOnInit() {
    this.fg = this.fb.group(new Login().validationRules());
    this.forgotPasswordFG = this.fb.group(new Login().FPValidationRules());
    this.carouselTileItems = [
      'assets/images/home5.jpg',
      'assets/images/home6.jpg',
      'assets/images/home8.jpeg',
      'assets/images/home9.jpg',
      'assets/images/home4.gif',
      'assets/images/home2.jpg',
      'assets/images/home7.jpeg',
      'assets/images/home3.jpg'
    ];

    this.carouselTile = {
      grid: { xs: 1, sm: 2, md: 3, lg: 4, all: 0 },
      slide: 1,
      speed: 400,
      animation: 'lazy',
      point: {
        visible: true
      },
      load: 3,
      touch: true,
      easing: 'ease'
    };
  }
  /**
   * Login If user is valid
   * @param form NgForm
   */
  login(item: Login) {
    // Set it true to display spinner now this will become false in case of error or will remaint true untill route redirect.
    this.loginInProgress = true;

    this.loginService.login(item).subscribe(
      response => {
        if (response) {
          this.loginInProgress = false;
          if (response.toString() === 'registration') {
            this.activatedRoute.navigate(['/post-registration/campus/personalinfo']);
          } else {
            this.activatedRoute.navigate(['/institute/dashboard']);
          }
        }
      },
      error => {
        this.loginInProgress = false;
        this.authError = error;
      }
    );
  }

  forgotPassword(item: Login) {
    this.loaded = false;
    this.authSuccess = null;
    this.profileService.forgotPassword(item).subscribe(
      response => {
        this.formTitle = 'Email Sent';
        this.loaded = true;
        this.passwordChanged = true;
        this.authSuccess = `Account recovery email sent to ${item.username}. Please visit the link provided in email to reset password`;
      },
      error => {
        if (error.status === 404) {
          this.forgotPasswordFG.controls['username'].setErrors({ notExist: true });
        }
        this.loaded = true;
      },
      () => { }
    );
  }

  toggleSignInView() {
    // this.forgotPasswordFG.get('username').setValue(null);
    this.passwordChanged = false;
    this.formTitle = 'Forgot Password';
    this.authError = null;
    this.authSuccess = null;
    this.signInView = this.signInView ? false : true;
  }
}
