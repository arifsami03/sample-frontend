import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent, ApplyForRegistrationComponent, CampusPreRegistrationFormComponent, ThankyouComponent, EligibilityCriteriaComponent, ResetPasswordComponent } from './components';

/**
 * Available routing paths
 */
const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'applyForRegistration', component: ApplyForRegistrationComponent },
  { path: 'campusPreRegistration', component: CampusPreRegistrationFormComponent },
  { path: 'thankyou', component: ThankyouComponent },
  { path: 'eligibilityCriteria', component: EligibilityCriteriaComponent },
  { path: 'resetPassword', component: ResetPasswordComponent }
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutes { }
