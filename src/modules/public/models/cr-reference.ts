import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class CRReference {
  id: number;
  CRApplicantId: number;
  fullName: string;
  mobileNumber: string;
  address: string;
  createdBy: number;
  updatedBy: number;
  createdAt: Date;
  updatedAt: Date;

  labels = {
    CRApplicantId: 'Campus Owner',
    fullName: 'Full Name',
    mobileNumber: 'Mobile Number',
    address: 'Address'
  };

  constructor() {}

  public validationRules?() {
    return {
      CRApplicantId: new FormControl('', [Validators.required]),
      fullName: new FormControl('', [Validators.required, Validators.maxLength(55)]),
      mobileNumber: new FormControl('', [Validators.pattern('([0-9]{3})-([0-9]{7})'), Validators.maxLength(12)]),
      address: new FormControl('')
    };
  }
}
