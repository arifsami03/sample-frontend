import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class CRTargettedLocations {
  id: number;
  CRApplicationId: number;
  address: string;
  latitude: number;
  longitude: number;
  createdBy: number;
  updatedBy: number;
  createdAt: Date;
  updatedAt: Date;

  labels = {
    CRApplicationId: 'Campus',
    address: 'Address',
    latitude: 'Latitude',
    longitude: 'Longitude'
  };

  constructor() {}

  public validationRules?() {
    return {
      CRApplicantId: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      latitude: new FormControl(''),
      longitude: new FormControl('')
    };
  }
}
