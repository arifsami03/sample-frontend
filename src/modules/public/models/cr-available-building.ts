import { FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

export class CRAvailableBuilding {
  id: number;
  CRApplicationId: number;
  buildingOwn: boolean;
  rentAgreementUpTo: string;
  address: string;
  latitude: number;
  longitude: number;
  coverdArea: number;
  openArea: number;
  totalArea: number;
  roomsQuantity: number;
  washroomsQuantity: number;
  teachingStaffQuantity: number;
  labsQuantity: number;
  nonTeachingStaffQuantity: number;
  studentsQuantity: number;
  playGround: boolean;
  swimmingPool: boolean;
  healthClinic: boolean;
  mosque: boolean;
  cafeteria: boolean;
  transport: boolean;
  library: boolean;
  bankBranch: boolean;
  createdBy: number;
  updatedBy: number;
  createdAt: Date;
  updatedAt: Date;

  labels = {
    CRApplicationId: 'Campus',
    buildingOwn: 'Own Building',
    rentAgreementUpTo: 'Rent Agrrement To',
    address: 'Address',
    latitude: 'Latitude',
    longitude: 'Longitude',
    coverdArea: 'Coverd Area',
    openArea: 'Open Area',
    totalArea: 'Total Area',
    roomsQuantity: 'Rooms Quanitity',
    washroomsQuantity: 'Washroom Quanitity',
    teachingStaffQuantity: 'Technical Staff Quantity',
    labsQuantity: 'Labs Quanitity',
    nonTeachingStaffQuantity: 'Non Technical Staff Quanitity',
    studentsQuantity: 'Student Quanitity',
    playGround: 'Paly Ground',
    swimmingPool: 'Swimming Pool',
    healthClinic: 'Health Clinic',
    mosque: 'Mosque',
    cafeteria: 'Cafeteria',
    transport: 'Transport',
    library: 'Library',
    bankBranch: 'Bank Branch'
  };

  constructor() {}
  /**
   * Checking validation of number
   * @param equalControlName
   */
  public greaterThan?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
      [key: string]: any;
    } => {
      let controlMatch = 0;
      return controlMatch <= control.value && 9999999999 >= control.value
        ? null
        : {
            equalTo: true
          };
    };
  }
  public validationRules?() {
    return {
      // CRApplicationId: new FormControl(''),
      buildingOwn: new FormControl(''),
      rentAgreementUpTo: new FormControl(''),
      address: new FormControl(''),
      latitude: new FormControl(''),
      longitude: new FormControl(''),
      totalArea: ['', [this.greaterThan('totalArea')]],
      coverdArea: ['', [this.greaterThan('coverdArea')]],
      openArea: ['', [this.greaterThan('openArea')]],
      roomsQuantity: new FormControl('', [this.greaterThan('roomsQuantity'), Validators.pattern('^(-)?[0-9]*$')]),
      washroomsQuantity: new FormControl('', [this.greaterThan('washroomsQuantity'), Validators.pattern('^(-)?[0-9]*$')]),
      teachingStaffQuantity: new FormControl('', [this.greaterThan('teachingStaffQuantity'), Validators.pattern('^(-)?[0-9]*$')]),
      labsQuantity: new FormControl('', [this.greaterThan('labsQuantity'), Validators.pattern('^(-)?[0-9]*$')]),
      nonTeachingStaffQuantity: new FormControl('', [this.greaterThan('nonTeachingStaffQuantity'), Validators.pattern('^(-)?[0-9]*$')]),
      studentsQuantity: new FormControl('', [this.greaterThan('studentsQuantity'), Validators.pattern('^(-)?[0-9]*$')]),
      playGround: new FormControl(''),
      swimmingPool: new FormControl(''),
      healthClinic: new FormControl(''),
      mosque: new FormControl(''),
      cafeteria: new FormControl(''),
      transport: new FormControl(''),
      library: new FormControl(''),
      bankBranch: new FormControl('')
    };
  }
}
