import { CampusPreRegistrationFormService } from '../services';
import { GLOBALS } from '../../app/config/globals';
import { ConfigurationModel } from '../../shared/models';
import { CRApplicant } from './cr-applicant';
import { FormControl, Validators, ValidatorFn, FormGroup, AbstractControl } from '@angular/forms';
import { retry } from 'rxjs/operators';
import { WFStateModel } from '../../settings/models';

export class CampusPreRegistration {
  id?: number;
  fullName?: string;
  cnic?: string;
  mobileNumber?: string;
  email?: string;
  instituteTypeId?: number;
  applicationType: string;
  campus?: string;
  cityId?: number;
  tentativeSessionStart?: Date;
  password?: string;
  confirmPassword?: string;
  cityName?: string;
  applicant?: CRApplicant = new CRApplicant();
  city?: ConfigurationModel = new ConfigurationModel();
  state?: string;
  wfState?: WFStateModel;
  stateId: number;

  createdBy: number;
  updatedBy: number;
  createdAt: Date;
  updatedAt: Date;

  listLabels = {
    fullName: 'Applicant Name',
    email: 'Email',
    applicationType: 'Application Type',
    instituteType: 'Applied For',
    city: 'City',
    state: 'State',
    filter: 'Filter'
  };

  labels = {
    fullName: 'Full Name',
    cnic: 'CNIC',
    mobileNumber: 'Mobile Number',
    email: 'Email',
    instituteType: 'Institute Type (Applying for)',
    applicationType: 'Application Type',
    cityId: 'City ( Target Campus Location )',
    tentativeSessionStart: 'Tentative Session Start',
    password: 'Password',
    confirmPassword: 'Confirm Password',
    sessionStartDate: 'Session Start Date',
    createdAt: 'Created At',
    countryCode: 'Country',
    state: 'State',


  };

  constructor() { }

  /**
   *
   * @param equalControlName
   */
  public equalTo?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      if (!control['_parent']) return null;

      if (!control['_parent'].controls[equalControlName]) throw new TypeError('Form Control ' + equalControlName + ' does not exists.');

      let controlMatch = control['_parent'].controls[equalControlName];

      // cehecking the field is dirty
      if (controlMatch.dirty === true) {
        if (controlMatch.value == control.value) {
          controlMatch.setErrors(null);
          return null;
        } else {

          return { equalTo: true };
        }
      }

    };
  }

  public validationRules?() {
    return {
      fullName: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      cnic: new FormControl('', [Validators.required, Validators.pattern('([0-9]{5})-([0-9]{7})-([0-9]{1})'), Validators.maxLength(16)]),
      mobileNumber: new FormControl('', [Validators.required, Validators.pattern('([0-9]{3})-([0-9]{7})'), Validators.maxLength(12)]),
      email: new FormControl('', [Validators.required, Validators.email, Validators.maxLength(30)]),
      applicationType: new FormControl('', Validators.required),
      instituteTypeId: new FormControl('', [Validators.required]),
      cityId: new FormControl('', [Validators.required]),
      tentativeSessionStart: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required, Validators.maxLength(30), this.equalTo('confirmPassword')]),
      confirmPassword: new FormControl('', [Validators.required, Validators.maxLength(30), this.equalTo('password')]),
      countryCode: new FormControl('', Validators.required),
    };
  }
}
