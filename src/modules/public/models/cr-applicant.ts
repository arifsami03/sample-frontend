import { FormControl, Validators, ValidatorFn } from '@angular/forms';
import { ConfigurationModel } from '../../shared/models';

export class CRApplicant {
  id: number;
  fullName: string;
  cnic: string;
  mobileNumber: string;
  email: string;
  applicationType: string;
  ntn: string;
  countryId: number;
  provinceId: number;
  cityId: number;
  tehsilId: number;
  natureOfWorkId: number;
  approxMonthlyIncome: number;
  nearestBankId: number;
  city?: ConfigurationModel = new ConfigurationModel();
  createdBy: number;
  updatedBy: number;
  createdAt: Date;
  updatedAt: Date;

  labels = {
    fullName: 'Full Name',
    cnic: 'CNIC',
    mobileNumber: 'Mobile Number',
    email: 'Email',
    applicationType: 'Applicationn Type',
    ntn: 'NTN',
    countryId: 'Country',
    provinceId: 'Province',
    cityId: 'City',
    tehsilId: 'Tehsil',
    natureOfWorkId: 'Work Type',
    approxMonthlyIncome: 'Monmthly Income',
    nearestBankId: 'Nearest Bank'
  };

  constructor() { }

  public validationRules?() {
    return {
      fullName: new FormControl('', [Validators.required, Validators.maxLength(55)]),
      cnic: new FormControl('', [Validators.required, Validators.pattern('([0-9]{5})-([0-9]{7})-([0-9]{1})'), Validators.maxLength(20)]),
      mobileNumber: new FormControl('', [ Validators.pattern('([0-9]{3})-([0-9]{7})'), Validators.maxLength(12)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      applicationType: new FormControl('', [Validators.maxLength(55)]),
      ntn: new FormControl('', [Validators.maxLength(20)]),
      countryId: new FormControl(''),
      provinceId: new FormControl(''),
      cityId: new FormControl(''),
      tehsilId: new FormControl(''),
      natureOfWorkId: new FormControl(''),
      approxMonthlyIncome: new FormControl(''),
      nearestBankId: new FormControl('')
    };
  }
}
