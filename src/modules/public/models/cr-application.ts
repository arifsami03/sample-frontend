import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class CRApplication {
  
  id: number;
  CRApplicantId: number;
  campusName: string;
  educationLevelId: number;
  tentativeSessionStart: Date;
  campusLocation: string;
  applicationType: string;
  instituteTypeId: number;
  buildingAvailable: boolean;
  remittanceStatus: boolean;
  cityId: number;
  stateId: number;
  createdBy: number;
  updatedBy: number;
  createdAt: Date;
  updatedAt: Date;

  static attributesLabels = {
    CRApplicantId: 'Campus Owner',
    campusName: 'Campus Name',
    levelOfEducation: 'Level of Education',
    tentativeSessionStart: 'Tentative session start',
    campusLocation: 'Campus Location',
    buildingAvailable: 'Building Available',
    cityId: 'City',
    stateId: 'State',
    applicationType: 'Application Type',
    // status: 'Status'
  };

  constructor() {}

  public validationRules?() {
    return {
      CRApplicantId: new FormControl('', [Validators.required]),
      campusName: new FormControl('', [Validators.maxLength(55)]),
      educationLevelId: new FormControl(''),
      instituteTypeId: new FormControl(''),
      tentativeSessionStart: new FormControl(''),
      campusLocation: new FormControl(''),
      cityId: new FormControl(''),
      buildingAvailable: new FormControl('')
    };
  }
}
