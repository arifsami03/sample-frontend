// Import components and services etc here
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TextMaskModule } from 'angular2-text-mask';
import { NgxCarouselModule } from 'ngx-carousel';
import { EligibilityCriteriaComponent } from './components';
import { WFStateService } from '../settings/services';
import { ConfigurationService } from '../shared/services';
import { CampusPreRegistrationFormService } from './services';
import { DownloadPdfService } from '../shared/services';
import { DocStateManagerService } from '../shared/services/doc-state-manager.service';
import { InstituteTypeService } from '../institute/services';

import { MatFormFieldModule, MatIconModule, MatInputModule, MatButtonModule, MatCardModule, MatSelectModule, MatDatepickerModule, MatRadioModule, MatDialogModule } from '@angular/material';

import { HomeComponent, ApplyForRegistrationComponent, CampusPreRegistrationFormComponent, ThankyouComponent, ResetPasswordComponent } from './components';

export const __IMPORTS = [
  CommonModule,
  FormsModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  ReactiveFormsModule,
  TextMaskModule,
  MatSelectModule,
  FlexLayoutModule,
  NgxCarouselModule,
  MatRadioModule,
  MatDialogModule,

];

export const __DECLARATIONS = [HomeComponent, ApplyForRegistrationComponent, CampusPreRegistrationFormComponent, ThankyouComponent, EligibilityCriteriaComponent, ResetPasswordComponent];

export const __PROVIDERS = [ConfigurationService, DownloadPdfService, WFStateService, DocStateManagerService, InstituteTypeService, CampusPreRegistrationFormService];

export const __ENTRY_COMPONENTS = [];
