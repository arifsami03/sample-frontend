import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { BaseService } from '../../shared/services'; 
import { Group } from '../models';

@Injectable()
export class GroupService extends BaseService {
  private routeURL: String = 'security/groups';

  constructor(protected http: Http) {
    super(http)
  }

  getAll() {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return data.json();
    });
  }

  find(id: number) {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return data.json();
    });
  }

  findDetail(id: number) {
    return this.__get(`${this.routeURL}/findDetail/${id}`).map(data => {
      return data.json();
    });
  }

  findAllUsers(id: number) {
    return this.__get(`${this.routeURL}/findAllUsers/${id}`).map(data => {
      return data.json();
    });
  }

  findAllRoles(id: number) {
    return this.__get(`${this.routeURL}/findAllRoles/${id}`).map(data => {
      return data.json();
    });
  }

  findAllPermissions(id: number) {
    return this.__get(`${this.routeURL}/findAllPermissions/${id}`).map(data => {
      return data.json();
    });
  }

  create(group: Group) {
    return this.__post(`${this.routeURL}/create`, group).map(data => {
      return data.json();
    });
  }

  update(id: Number, group: Group) {
    return this.__put(`${this.routeURL}/update/${id}`, group).map(data => {
      return data.json();
    });
  }

  delete(id: Number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }
}
