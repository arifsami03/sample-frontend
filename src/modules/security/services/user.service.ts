import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { BaseService } from '../../shared/services'; 
import { User } from '../models';

@Injectable()
export class UserService extends BaseService {
  private routeURL: String = 'security/users';

  constructor(protected http: Http) {
    super(http)
  }

  index() {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return data.json();
    });
  }

  find(id: number) {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return data.json();
    });
  }

  create(user: User) {
    return this.__post(`${this.routeURL}/create`, user).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  update(id: Number, user: User) {
    return this.__put(`${this.routeURL}/update/${id}`, user).map(data => {
      return data.json();
    }).catch(this.handleError);
  }

  delete(id: Number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  findAllGroups(id: number) {
    return this.__get(`${this.routeURL}/findAllGroups/${id}`).map(data => {
      return data.json();
    });
  }

  findAllRoles(id: number) {
    return this.__get(`${this.routeURL}/findAllRoles/${id}`).map(data => {
      return data.json();
    });
  }

  findAllPermissions(id: number) {
    return this.__get(`${this.routeURL}/findAllPermissions/${id}`).map(data => {
      return data.json();
    });
  }

  login(user: User) {
    return this.__post(`${this.routeURL}/login`, user).map(data => {
      return data.json();
    });
  }
  getPortal(_id: number) {
    return this.__post(`${this.routeURL}/getPortal`, { id: _id }).map(data => {
      return data.json();
    });
  }

  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }
}
