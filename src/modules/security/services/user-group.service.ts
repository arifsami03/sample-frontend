import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { BaseService } from '../../shared/services'; 
import { UserGroup } from '../models';

@Injectable()
export class UserGroupService extends BaseService {
  private routeURL: String = 'security/userGroups';

  constructor(protected http: Http) {
    super(http)
  }

  assign(userGroup: UserGroup) {
    return this.__post(`${this.routeURL}/assign`, userGroup).map(data => {
      return data.json();
    });
  }

  revoke(id: Number) {
    return this.__delete(`${this.routeURL}/revoke/${id}`).map(data => {
      return data.json();
    });
  }
}
