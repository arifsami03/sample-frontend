import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { BaseService } from '../../shared/services'; 
import { FeaturePermission } from '../models';

@Injectable()
export class FeaturePermissionService extends BaseService {
  private routeURL: String = 'security/featurePermissions';

  constructor(protected http: Http) {
    super(http)
  }

  updateRolePermissions(roleId: number, featurePermissions: FeaturePermission[]) {

    return this.__post(`${this.routeURL}/updateRolePermissions/${roleId}`, featurePermissions).map(data => {
      return data.json();
    });
  }

  updateGroupPermissions(groupId: number, featurePermissions: FeaturePermission[]) {

    return this.__post(`${this.routeURL}/updateGroupPermissions/${groupId}`, featurePermissions).map(data => {
      return data.json();
    });

  }

  updateUserPermissions(userId: number, featurePermissions: FeaturePermission[]) {

    return this.__post(`${this.routeURL}/user/${userId}`, featurePermissions).map(data => {
      return data.json();
    });
  }
}
