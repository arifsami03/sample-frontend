import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { BaseService } from '../../shared/services'; 
import { Role, FeaturePermission } from '../models';

@Injectable()
export class RoleService extends BaseService {
  private routeURL: String = 'security/roles';

  constructor(protected http: Http) {
    super(http)
  }

  getAll() {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return data.json();
    });
  }

  find(id: number) {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return data.json();
    });
  }

  findAllGroups(id: number) {
    return this.__get(`${this.routeURL}/findAllGroups/${id}`).map(data => {
      return data.json();
    });
  }

  findAllUsers(id: number) {
    return this.__get(`${this.routeURL}/findAllUsers/${id}`).map(data => {
      return data.json();
    });
  }

  findAllFeatures(id: number) {
    return this.__get(`${this.routeURL}/findAllFeatures/${id}`).map(data => {
      return data.json();
    });
  }

  create(role: Role) {
    return this.__post(`${this.routeURL}/create`, role).map(data => {
      return data.json();
    });
  }

  update(id: Number, role: Role) {
    return this.__put(`${this.routeURL}/update/${id}`, role).map(data => {
      return data.json();
    });
  }

  delete(id: Number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }
}
