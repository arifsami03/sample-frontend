import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { BaseService } from '../../shared/services'; 
import { RoleWFStateFlowModel } from '../models';

@Injectable()
export class RoleWFStateFlowService extends BaseService {
  private routeURL: String = 'security/roleWFStateFlow';

  constructor(protected http: Http) {
    super(http)
  }

  getAll() {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return data.json();
    });
  }

  find(id: number) {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return data.json();
    });
  }

  findAllGroups(id: number) {
    return this.__get(`${this.routeURL}/findAllGroups/${id}`).map(data => {
      return data.json();
    });
  }

  findAllUsers(id: number) {
    return this.__get(`${this.routeURL}/findAllUsers/${id}`).map(data => {
      return data.json();
    });
  }
  list(id:number){
    return this.__get(`${this.routeURL}/list/${id}`).map(data => {
      return data.json();
    });
  }
  getWFStateFlowByRole(id:number){
    return this.__get(`${this.routeURL}/getWFStateFlowByRole/${id}`).map(data => {
      return data.json();
    });
  }

  findAllFeatures(id: number) {
    return this.__get(`${this.routeURL}/findAllFeatures/${id}`).map(data => {
      return data.json();
    });
  }

  create(item) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  update(id: Number, role: RoleWFStateFlowModel) {
    return this.__put(`${this.routeURL}/update/${id}`, role).map(data => {
      return data.json();
    });
  }

  delete(id: Number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }
}
