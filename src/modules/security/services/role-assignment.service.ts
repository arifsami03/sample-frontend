import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { BaseService } from '../../shared/services'; 
import { RoleAssignment } from '../models';

@Injectable()
export class RoleAssignmentService extends BaseService {
  private routeURL: String = 'security/roleAssignments';

  constructor(protected http: Http) {
    super(http)
  }

  assignToUser(roleAssignment: RoleAssignment) {
    return this.__post(`${this.routeURL}/assignToUser`, roleAssignment).map(
      data => {
        return data.json();
      }
    );
  }
  assignToGroup(roleAssignment: RoleAssignment) {
    return this.__post(`${this.routeURL}/assignToGroup`, roleAssignment).map(
      data => {
        return data.json();
      }
    );
  }

  delete(id: Number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }
}
