import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { BaseService } from '../../shared/services'; 
import { AppFeature } from '../models';

@Injectable()
export class AppFeatureService extends BaseService {
  private routeURL: String = 'security/appFeatures';

  constructor(protected http: Http) {
    super(http)
  }

  getAll() {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return data.json();
    });
  }

  findAllParents() {
    return this.__get(`${this.routeURL}/findAllParents`).map(data => {
      return data.json();
    });
  }

  find(id: number) {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return data.json();
    });
  }

  create(appFeature: AppFeature) {
    return this.__post(`${this.routeURL}/create`, appFeature).map(data => {
      return data.json();
    });
  }

  update(id: Number, appFeature: AppFeature) {
    return this.__put(`${this.routeURL}/update/${id}`, appFeature).map(data => {
      return data.json();
    });
  }

  delete(id: Number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }
}
