import { FormControl, Validators, ValidatorFn } from '@angular/forms';
import { RoleAssignment } from './role-assignment';
import { FeaturePermission } from './feature-permission';

export class Role {
  static attributesLabels = {
    name: 'Name',
    description: 'Description'
  };

  public id?: number;
  public name: string;
  public description: string;
  public assignedUsers?: RoleAssignment[];
  public assignedGroups?: RoleAssignment[];
  public assignedPermissions: FeaturePermission[] = [new FeaturePermission()];
  public createdBy?: number;
  public updatedBy?: number;

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      name: new FormControl('', [Validators.required]),
      description: new FormControl('')
    };
  }
}
