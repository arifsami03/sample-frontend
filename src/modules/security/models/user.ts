import { FormControl, Validators, ValidatorFn, FormGroup, AbstractControl } from '@angular/forms';
import { Group } from './group';
import { UserGroup } from './user-group';
import { RoleAssignment } from './role-assignment';
import { FeaturePermission } from './feature-permission';

export class User {
  static attributesLabels = {
    username: 'Email',
    name: 'Name',
    password: 'Password',
    confirmPassword: 'Confirm Password',
    isSuperUser: 'Super User',
    superUserLabel: 'Super User',
    isActive: 'Active',
    activeLabel: 'Active',
    portal: 'Portal',
    phoneNumber: 'Mobile Number',
    address: 'Address',
    gender: 'Gender',
    createdBy: 'Created by',
    updatedBy: 'Updated by',
    countryCode: 'Country',
    groups : 'Groups',
    roles : 'Roles',
  };

  public id?: number;
  public name?: string;
  public phoneNumber?: string;
  public countryCode?: string;
  public address?: string;
  public gender?: string;
  public username: string;
  public password?: string;
  public confirmPassword?: string;
  public isSuperUser: boolean;
  public superUserLabel: string;
  public isActive: boolean;
  public activeLabel: string;
  public portal: string;
  public userGroups?: UserGroup[];
  public roleAssignments?: RoleAssignment[];
  public assignedPermissions: FeaturePermission[] = [new FeaturePermission()];
  public createdBy?: number;
  public updatedBy?: number;
public groupsName ?: string; 
public rolesName ?: string; 
  constructor() { }

  /**
    *
    * @param equalControlName
    */
  public equalTo?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      if (!control['_parent']) return null;

      if (!control['_parent'].controls[equalControlName]) throw new TypeError('Form Control ' + equalControlName + ' does not exists.');

      let controlMatch = control['_parent'].controls[equalControlName];

      // cehecking the field is dirty
      if (controlMatch.dirty === true) {
        if (controlMatch.value == control.value) {
          controlMatch.setErrors(null);
          return null;
        } else {

          return { equalTo: true };
        }
      }

    };
  }

  /**
   * Create Form Validation Rules
   */
  public createValidationRules?() {
    return {
      username: new FormControl('', [Validators.required, Validators.email]),
      name: new FormControl('', Validators.required),
      phoneNumber: new FormControl(''),
      address: new FormControl(''),
      gender: new FormControl(''),
      password: new FormControl('', [
        Validators.required,
        Validators.maxLength(30),
        this.equalTo('confirmPassword')
      ]),
      confirmPassword: new FormControl('', [
        Validators.required,
        Validators.maxLength(30),
        this.equalTo('password')
      ]),
      isActive: new FormControl(''),
      isSuperUser: new FormControl(''),
      countryCode: new FormControl(),
    };
  }

  /**
   * Create Form Validation Rules
   */
  public updateValidationRules?() {
    return {
      username: new FormControl('', [Validators.required, Validators.email]),
      name: new FormControl('', Validators.required),
      phoneNumber: new FormControl(''),
      address: new FormControl(''),
      gender: new FormControl(''),
      isActive: new FormControl(''),
      countryCode: new FormControl(),

    };
  }
}
