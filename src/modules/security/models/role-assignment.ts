import { Role, User, Group } from '.';

export class RoleAssignment {
  public id: number;
  public roleId: number;
  public roleName: string;
  public role: Role;
  public userId: number;
  public userName: string;
  public user: User;
  public groupId: number;
  public groupName: string;
  public group: Group;
  public createdBy?: number;
  public updatedBy?: number;

  constructor() {}
}
