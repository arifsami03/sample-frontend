import { Role } from './role';
import { User } from './user';
import { Group } from './group';
import { AppFeature } from './app-feature';

export class FeaturePermission {
  public id: number;
  public name: string;
  public title: string;
  public status: boolean = false;
  public children: AppFeature[];

  // following properites must be deleted after implementing permissions screen succesfully
  public featureId: number;
  public features: AppFeature[] = [new AppFeature()];
  public roleId: number;
  public roleName: string;
  public role: Role;
  public userId: number;
  public userName: string;
  public user: User;
  public groupId: number;
  public groupName: string;
  public group: Group;
  public createdBy?: number;
  public updatedBy?: number;

  constructor() {}
}
