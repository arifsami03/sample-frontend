import { Group, User } from '.';

export class UserGroup {
  public id: number;
  public user?: User;
  public userId: number;
  public userName?: string;
  public group?: Group;
  public groupId: number;
  public groupName?: string;
  public createdBy?: number;
  public updatedBy?: number;

  constructor() {}
}
