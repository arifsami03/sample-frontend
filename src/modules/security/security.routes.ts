import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  UserFormComponent,
  UserViewComponent,
  UserListComponent,
  GroupFormComponent,
  GroupListComponent,
  GroupViewComponent,
  RoleListComponent,
  RoleFormComponent,
  RoleViewComponent,
  DashboardComponent
} from './components';
import { AuthGuardService } from '../app/services';

/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardService],
    component: DashboardComponent,
    data: { breadcrumb: { title: 'Dashboard', display: true } }
  },
  {
    path: 'users',
    data: { breadcrumb: { title: 'Users', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: UserListComponent,
        data: { breadcrumb: { title: 'Users', display: false } }
      },
      {
        path: 'add',
        canActivate: [AuthGuardService],
        component: UserFormComponent,
        data: {
          act: 'add',
          breadcrumb: { title: 'Add', display: true }
        }
      },
      {
        path: 'view/:id',
        canActivate: [AuthGuardService],
        component: UserViewComponent
      },
      {
        path: 'update/:id',
        canActivate: [AuthGuardService],
        component: UserFormComponent,
        data: { act: 'update' }
      }
    ]
  },
  {
    path: 'groups',
    data: { breadcrumb: { title: 'Groups', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: GroupListComponent,
        data: { breadcrumb: { title: 'Groups', display: false } }
      },
      {
        path: 'add',
        canActivate: [AuthGuardService],
        component: GroupFormComponent,
        data: {
          act: 'add',
          breadcrumb: { title: 'Add', display: true }
        }
      },
      {
        path: 'view/:id',
        canActivate: [AuthGuardService],
        component: GroupViewComponent,
        data: { act: 'view' }
      },
      {
        path: 'update/:id',
        canActivate: [AuthGuardService],
        component: GroupFormComponent,
        data: { act: 'update' }
      }
    ]
  },
  {
    path: 'roles',
    data: { breadcrumb: { title: 'Roles', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: RoleListComponent,
        data: { breadcrumb: { title: 'Roles', display: false } }
      },
      {
        path: 'add',
        canActivate: [AuthGuardService],
        component: RoleFormComponent,
        data: {
          act: 'add',
          breadcrumb: { title: 'Add', display: true }
        }
      },
      {
        path: 'view/:id',
        canActivate: [AuthGuardService],
        component: RoleViewComponent,
        data: { act: 'view' }
      },
      {
        path: 'update/:id',
        canActivate: [AuthGuardService],
        component: RoleFormComponent,
        data: { act: 'update' }
      }
    ]
  },
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecurityRoutes { }
