// Import components and services etc here
import { TextMaskModule } from 'angular2-text-mask';

import {
  MatInputModule,
  MatFormFieldModule,
  MatIconModule,
  MatDialogModule,
  MatButtonModule,
  MatCardModule,
  MatTableModule,
  MatPaginatorModule,
  MatOptionModule,
  MatAutocompleteModule,
  MatSelectModule,
  MatDividerModule,
  MatListModule,
  MatExpansionModule,
  MatCheckboxModule,
  MatProgressBarModule,
  MatRadioModule,
  MatSortModule
} from '@angular/material';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  UserService,
  GroupService,
  UserGroupService,
  RoleService,
  RoleAssignmentService,
  FeaturePermissionService,
  AppFeatureService,
  RoleWFStateFlowService
} from './services';
import { WFStateService, WFStateFlowService, WorkFlowService } from '../settings/services';

import {
  UserFormComponent,
  UserViewComponent,
  UserListComponent,
  GroupFormComponent,
  GroupListComponent,
  GroupViewComponent,
  RoleFormComponent,
  RoleListComponent,
  RoleViewComponent,
  AssignGroupToUserComponent,
  AssignRoleToUserComponent,
  AssignUserToGroupComponent,
  AssignRoleToGroupComponent,
  AssignGroupToRoleComponent,
  AssignUserToRoleComponent,
  DashboardComponent,
  RoleWFStateFlowComponent,
  RoleWFStateFlowFormComponent
} from './components';

export const __IMPORTS = [
  MatInputModule,
  MatFormFieldModule,
  MatIconModule,
  MatDialogModule,
  MatButtonModule,
  MatCardModule,
  FormsModule,
  ReactiveFormsModule,
  MatTableModule,
  MatPaginatorModule,
  TextMaskModule,
  MatAutocompleteModule,
  MatOptionModule,
  MatSelectModule,
  MatDividerModule,
  MatListModule,
  FlexLayoutModule,
  MatExpansionModule,
  MatCheckboxModule,
  MatProgressBarModule,
  MatRadioModule,
  MatSortModule
];

export const __DECLARATIONS = [
  UserFormComponent,
  UserViewComponent,
  UserListComponent,
  GroupFormComponent,
  GroupListComponent,
  GroupViewComponent,
  RoleFormComponent,
  RoleListComponent,
  RoleViewComponent,
  AssignGroupToUserComponent,
  AssignRoleToUserComponent,
  AssignUserToGroupComponent,
  AssignRoleToGroupComponent,
  AssignGroupToRoleComponent,
  AssignUserToRoleComponent,
  DashboardComponent,
  RoleWFStateFlowComponent,
  RoleWFStateFlowFormComponent
];

export const __PROVIDERS = [
  UserService,
  GroupService,
  UserGroupService,
  RoleService,
  RoleAssignmentService,
  FeaturePermissionService,
  AppFeatureService,
  RoleWFStateFlowService,
  WorkFlowService,
  WFStateService,
  WFStateFlowService
];

export const __ENTRY_COMPONENTS = [
  AssignGroupToUserComponent,
  AssignRoleToUserComponent,
  AssignUserToGroupComponent,
  AssignRoleToGroupComponent,
  AssignGroupToRoleComponent,
  AssignUserToRoleComponent,
  RoleWFStateFlowFormComponent
];
