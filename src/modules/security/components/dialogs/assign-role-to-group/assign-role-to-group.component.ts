import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { GroupService, RoleAssignmentService } from '../../../services';
import { Role, RoleAssignment } from '../../../models';

@Component({
  selector: 'security-assign-role',
  templateUrl: './assign-role-to-group.component.html',
  styleUrls: ['./assign-role-to-group.component.css']
})
export class AssignRoleToGroupComponent {
  public loaded: boolean = false;
  public roleAssignment: RoleAssignment;
  public roles: Role[];

  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;
  public success: Boolean;

  constructor(
    private groupService: GroupService,
    private roleAssignmentService: RoleAssignmentService,
    public dialogRef: MatDialogRef<AssignRoleToGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RoleAssignment
  ) {
    this.roleAssignment = data;
    this.findAllRoles(this.roleAssignment.groupId);
  }

  submit() {

    this.roleAssignmentService.assignToGroup(this.roleAssignment).subscribe(
      response => {
        this.dialogRef.close(true);
      },
      error => {
        this.dialogRef.close(false);
      },
      () => { }
    );
  }

  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close(false);
  }

  private findAllRoles(groupId: number) {
    let data;
    this.groupService.findAllRoles(groupId).subscribe(
      response => {
        data = response;
      },
      error => {
        console.log(error);
      },
      () => {
        this.roles = data;
        this.loaded = true;
      }
    );
  }
}
