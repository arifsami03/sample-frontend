import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { GroupService, UserGroupService } from '../../../services';
import { UserGroup, User } from '../../../models';

@Component({
  selector: 'security-assign-group',
  templateUrl: './assign-user-to-group.component.html',
  styleUrls: ['./assign-user-to-group.component.css']
})
export class AssignUserToGroupComponent {
  public loaded: boolean = false;
  public userGroup: UserGroup;
  public users: User[];

  constructor(
    private groupService: GroupService,
    private userGroupService: UserGroupService,
    public dialogRef: MatDialogRef<AssignUserToGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserGroup
  ) {
    this.userGroup = data;
    this.findAllUsers(this.userGroup.groupId);
  }

  submit() {
    this.userGroupService.assign(this.userGroup).subscribe(
      response => {
        this.dialogRef.close(true);
      },
      error => {
        this.dialogRef.close(false);
      },
      () => { }
    );
  }


  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close(false);
  }

  private findAllUsers(groupId) {
    this.groupService.findAllUsers(groupId).subscribe(
      response => {
        this.users = response;
      },
      error => {
        console.log(error);
      },
      () => {
        this.loaded = true;
      }
    );
  }
}
