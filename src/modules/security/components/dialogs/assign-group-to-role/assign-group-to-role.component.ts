import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { RoleService, RoleAssignmentService } from '../../../services';
import { Group, RoleAssignment } from '../../../models';

@Component({
  selector: 'security-assign-group-to-role',
  templateUrl: './assign-group-to-role.component.html',
  styleUrls: ['./assign-group-to-role.component.css']
})
export class AssignGroupToRoleComponent {
  public loaded: boolean = false;
  public roleAssignment: RoleAssignment;
  public groups: Group[];

  constructor(
    private roleService: RoleService,
    private roleAssignmentService: RoleAssignmentService,
    public dialogRef: MatDialogRef<AssignGroupToRoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RoleAssignment
  ) {
    this.roleAssignment = data;
    this.findAllGroups(this.roleAssignment.roleId);
  }

  submit() {

    this.roleAssignmentService.assignToGroup(this.roleAssignment).subscribe(
      response => {
        this.dialogRef.close(true);
      },
      error => {
        this.dialogRef.close(false);

      },
      () => { }
    );
  }

  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close(false);
  }

  private findAllGroups(roleId: number) {
    let data;
    this.roleService.findAllGroups(roleId).subscribe(
      response => {
        data = response;
      },
      error => {
        console.log(error);
      },
      () => {
        this.groups = data;
        this.loaded = true;
      }
    );
  }
}
