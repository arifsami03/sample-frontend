import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { UserService, UserGroupService } from '../../../services';
import { Group, UserGroup } from '../../../models';

@Component({
  selector: 'security-assign-group',
  templateUrl: './assign-group-to-user.component.html',
  styleUrls: ['./assign-group-to-user.component.css']
})
export class AssignGroupToUserComponent {
  public loaded: boolean = false;
  public userGroup: UserGroup;
  public groups: Group[];

  constructor(
    private userService: UserService,
    private userGroupService: UserGroupService,
    public dialogRef: MatDialogRef<AssignGroupToUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserGroup
  ) {
    this.userGroup = data;
    this.findAllGroups();
  }

  /**
   * submit a note against complaint suggestion or appreciation
   */
  submit() {

    this.userGroupService.assign(this.userGroup).subscribe(
      response => {
        this.dialogRef.close(true);
      },
      error => {
        this.dialogRef.close(false);
      },
      () => {
      }
    );
  }


  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close(false);
  }

  private findAllGroups() {
    let data;
    this.userService.findAllGroups(this.userGroup.userId).subscribe(
      response => {
        data = response;
      },
      error => {
        console.log(error);
      },
      () => {
        this.groups = data;
        this.loaded = true;
      }
    );
  }
}
