import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { RoleService, RoleAssignmentService } from '../../../services';
import { RoleAssignment, User } from '../../../models';

@Component({
  selector: 'security-assign-user-to-role',
  templateUrl: './assign-user-to-role.component.html',
  styleUrls: ['./assign-user-to-role.component.css']
})
export class AssignUserToRoleComponent {
  public loaded: boolean = false;
  public roleAssignment: RoleAssignment;
  public users: User[];

  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;
  public success: Boolean;

  constructor(
    private roleService: RoleService,
    private roleAssignmentService: RoleAssignmentService,
    public dialogRef: MatDialogRef<AssignUserToRoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RoleAssignment
  ) {
    this.roleAssignment = data;
    this.findAllUsers(this.roleAssignment.roleId);
  }

  /**
   * submit a note against complaint suggestion or appreciation
   */
  submit() {
    this.roleAssignmentService.assignToUser(this.roleAssignment).subscribe(
      response => {
        this.dialogRef.close(true);
      },
      error => {
        this.dialogRef.close(true);
      },
      () => {
      }
    );
  }


  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close(false);
  }

  private findAllUsers(roleId) {
    let data;
    this.roleService.findAllUsers(roleId).subscribe(
      response => {
        data = response;
      },
      error => {
        console.log(error);
      },
      () => {
        this.users = data;
        this.loaded = true;
      }
    );
  }
}
