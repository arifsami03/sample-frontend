import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { LayoutService } from '../../../../layout/services';
import { UserService, RoleAssignmentService } from '../../../services';
import { Role, RoleAssignment } from '../../../models';

@Component({
  selector: 'security-assign-role',
  templateUrl: './assign-role-to-user.component.html',
  styleUrls: ['./assign-role-to-user.component.css']
})
export class AssignRoleToUserComponent {
  public loaded: boolean = false;
  public roleAssignment: RoleAssignment;
  public roles: Role[];

  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;
  public success: Boolean;

  constructor(
    private router: Router,
    private userService: UserService,
    private roleAssignmentService: RoleAssignmentService,
    private layoutService: LayoutService,
    public dialogRef: MatDialogRef<AssignRoleToUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RoleAssignment
  ) {
    this.roleAssignment = data;
    this.findAllRoles();
  }

  /**
   * submit a note against complaint suggestion or appreciation
   */
  submit() {

    this.roleAssignmentService.assignToUser(this.roleAssignment).subscribe(
      response => {
        this.dialogRef.close(true);
      },
      error => {
        this.dialogRef.close(false);
      },
      () => { }
    );
  }

  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close(false);
  }

  private findAllRoles() {
    let data;
    this.userService.findAllRoles(this.roleAssignment.userId).subscribe(
      response => {
        data = response;
      },
      error => {
        console.log(error);
      },
      () => {
        this.roles = data;
        this.loaded = true;
      }
    );
  }
}
