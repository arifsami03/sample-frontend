import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { PageAnimation } from '../../../../shared/helper';
import { Group } from '../../../models';
import { GroupService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';

import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';

@Component({
  selector: 'security-group-form',
  templateUrl: './group-form.component.html',
  styleUrls: ['./group-form.component.css'],
  animations: [PageAnimation]
})
export class GroupFormComponent implements OnInit {
  public pageState = 'active';
  public loaded: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;
  public group: Group;

  public componentLabels = Group.attributesLabels;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private groupService: GroupService,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = activatedRoute.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  private initializePage() {
    this.fg = this.fb.group(new Group().validationRules());
    if (this.pageAct === 'add') {
      this.initAddPage();
    } else {
      this.getData();
    }
  }

  private getData() {
    this.activatedRoute.params.subscribe(params => {
      let data;
      this.groupService.find(params['id']).subscribe(
        response => {
          data = response;
        },
        error => console.log(error),
        () => {
          this.group = data;
          this.fg.patchValue(this.group);
          this.initUpdatePage();
          this.loaded = true;
        }
      );
    });
  }

  private initAddPage() {
    this.layoutService.setPageTitle({ title: 'Add Group' });
    this.group = new Group();
    this.fg.enable();

    this.loaded = true;
  }

  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Group: ' + this.group.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Groups', url: '/security/groups' },
        {
          label: this.group.name,
          url: `/security/groups/view/${this.group.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  public saveData(item: Group) {
    this.loaded = false;
    if (this.pageAct === 'add') {
      this.create(item);
    } else {
      this.update(this.group.id, item);
    }
  }

  public delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.groupService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Group has been deleted.', msgType: 'success' });
            this.router.navigate(['/security/groups']);
          },
          error => {
            this.loaded = true;
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          },

          () => {
            this.loaded = true;
          }
        );
      }
    });
  }

  private create(item: Group) {
    this.groupService.create(item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Group has been created.', msgType: 'success' });
        this.router.navigate([`/security/groups/view/${response.id}`]);
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
      },
      () => {
        this.loaded = true;
      }
    );
  }

  private update(id: number, item: Group) {
    this.loaded = false;

    this.groupService.update(id, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Group has been updated.', msgType: 'success' });
        this.router.navigate([`/security/groups/view/${id}`]);
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
      },
      () => {
        this.loaded = true;
      }
    );
  }

}
