import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { PageAnimation } from '../../../../shared/helper';

import { User, Group, Role } from '../../../models';
import {
  GroupService,
  UserGroupService,
  RoleAssignmentService,
  FeaturePermissionService
} from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';

import { AssignUserToGroupComponent } from '../../dialogs/assign-user-to-group/assign-user-to-group.component';
import { AssignRoleToGroupComponent } from '../../dialogs/assign-role-to-group/assign-role-to-group.component';

@Component({
  selector: 'security-group-view',
  templateUrl: './group-view.component.html',
  styleUrls: ['./group-view.component.css'],
  animations: [PageAnimation]
})
export class GroupViewComponent implements OnInit {
  public pageState = 'active';
  public loaded: boolean = false;

  public showDivBar: boolean = false;

  public pageTitle: string;
  public group: Group = new Group();
  public userLabels = User.attributesLabels;
  public groupLabels = Group.attributesLabels;
  public roleLabels = Role.attributesLabels;

  userDisplayedColumns = ['username', 'options'];
  roleDisplayedColumns = ['name', 'options'];
  userDataSource: any;
  roleDataSource: any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private groupService: GroupService,
    private featurePermissionService: FeaturePermissionService,
    private userGroupService: UserGroupService,
    private roleAssignmentService: RoleAssignmentService,
    private layoutService: LayoutService,
    private matDialog: MatDialog
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.group.id = param.id;
    });
    this.getData(this.group.id);
    // this.findAllPermissions(this.group.id);
  }

  private getData(id: number) {
    this.groupService.find(id).subscribe(
      response => {
        this.group = response;
      },
      error => {
        console.log(error),
          this.loaded = true;
      },
      () => {
        this.userDataSource = new MatTableDataSource(this.group.userGroups);
        this.roleDataSource = new MatTableDataSource(this.group.roleAssignments);
        this.modifyPageHeader();
        this.loaded = true;
      }
    );
  }

  public assignUser(groupId: number) {
    const dialogRef = this.matDialog.open(AssignUserToGroupComponent, {
      width: '500px',
      data: { groupId: groupId, groupName: this.group.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.layoutService.flashMsg({ msg: 'User has been added to group.', msgType: 'success' });
        this.getData(this.group.id);
      }
    });
  }

  public deleteUser(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.userGroupService.revoke(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'User has been deleted from group.', msgType: 'success' });
            this.getData(this.group.id);
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          },

          () => { }
        );
      }
    });
  }

  public assignRole(id: number) {
    const dialogRef = this.matDialog.open(AssignRoleToGroupComponent, {
      width: '500px',
      data: { groupId: id, groupName: this.group.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.layoutService.flashMsg({ msg: 'Role has been add to group.', msgType: 'success' });
        this.getData(this.group.id);
      }
    });
  }

  public deleteRole(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.roleAssignmentService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Role has been deleted from group.', msgType: 'success' });
            this.getData(this.group.id);
          },
          error => {
          },

          () => { }
        );
      }
    });
  }

  public changePermission(roleId: number) {
    this.showDivBar = true;
    this.featurePermissionService
      .updateGroupPermissions(roleId, this.group.assignedPermissions)
      .subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Permisssions has been updated.', msgType: 'success' });
        },
        error => {
          this.showDivBar = false;
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        },

        () => {
          this.showDivBar = false;
        }
      );
  }

  public delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.groupService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Group has been deleted.', msgType: 'success' });
            this.router.navigate(['/security/groups']);
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          },
          () => { }
        );
      }
    });
  }

  private modifyPageHeader() {
    this.layoutService.setPageTitle({
      title: 'Group: ' + this.group.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Groups', url: '/security/groups' },
        { label: this.group.name }
      ]
    });
  }

}
