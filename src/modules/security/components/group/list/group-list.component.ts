import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';

import { PageAnimation } from '../../../../shared/helper';
import { LayoutService } from '../../../../layout/services';
import { GroupService } from '../../../services';
import { ConfirmDialogComponent } from '../../../../shared/components';

import { Group } from '../../../models';

@Component({
  selector: 'security-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.css'],
  animations: [PageAnimation]
})
export class GroupListComponent implements OnInit {
  public pageState;
  public loaded: boolean = false;

  public attrLabels = Group.attributesLabels;

  displayedColumns = ['name', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    public layoutService: LayoutService,
    private groupService: GroupService,
    public dialog: MatDialog,
    public matDialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Groups' });
    this.getRecords();
  }

  getRecords(): void {
    let data;
    this.groupService.getAll().subscribe(
      response => {
        data = response;
      },
      error => {
        this.pageState = 'active';
        this.loaded = true;
        console.log(error);
      },
      () => {
        this.dataSource = new MatTableDataSource<Group>(data);
        this.dataSource.paginator = this.paginator;
        this.pageState = 'active';
        this.loaded = true;
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.groupService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Group has been deleted.', msgType: 'success' });
            this.getRecords();
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          },
          () => { }
        );
      }
    });
  }
}
