import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { FormBuilder } from '@angular/forms';

import { PageAnimation } from '../../../../shared/helper';
import { LayoutService } from '../../../../layout/services';
import { UserService } from '../../../services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { User, RoleAssignment, UserGroup } from '../../../models';

@Component({
  selector: 'security-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  animations: [PageAnimation]
})
export class UserListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public users: User[] = [new User()];

  public attrLabels = User.attributesLabels;

  displayedColumns = ['name', 'username', 'isActive', 'portal', 'groups', 'roles', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    public layoutService: LayoutService,
    private userService: UserService,
    public dialog: MatDialog,
    public matDialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Users' });
    this.getRecords();
  }

  getRecords(): void {
    this.userService.index().subscribe(
      response => {
        this.users = response;
        this.users.map(element => {
          element.groupsName = this.getGroupNames(element.userGroups)
          element.rolesName = this.getRoleNames(element.roleAssignments)
        })
        this.dataSource = new MatTableDataSource<User>(this.users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';

        // hide popup message if displayed
        if (this.success) {
          this.success = false;
        }
      }

    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.userService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'User has been deleted.', msgType: 'success' });
            this.getRecords();
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          },
          () => {
          }
        );
      }
    });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }

  /**
   * get Comma Seperated Roles
   * @param roleAssignment 
   */
  getRoleNames(roleAssignment: RoleAssignment[]) {
    let rolesName: string = '';
    let rolesNameArray: string[] = [];
    roleAssignment.forEach(element => {
      rolesNameArray.push(element.role.name)
    });
    rolesName = rolesNameArray.join(', ');

    return rolesName;
  }

  /**
   * Get Comma Seperated Groups
   */
  getGroupNames(userGroups: UserGroup[]) {
    let groupsName: string = '';
    let groupsNameArray: string[] = [];
    userGroups.forEach(element => {
      groupsNameArray.push(element.group.name)
    });
    groupsName = groupsNameArray.join(', ');

    return groupsName;
  }
}
