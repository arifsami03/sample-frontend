import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';

import { PageAnimation } from '../../../../shared/helper';
import { User } from '../../../models';
import { UserService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { GLOBALS } from '../../../../app/config/globals';
import { ConfigurationService } from '../../../../shared/services'
// for Mobile Number
interface ICountryOption {
  abbreviation: string;
  countryCode: string;
}
@Component({
  selector: 'security-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css'],
  animations: [PageAnimation]
})
export class UserFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public boxLoaded: boolean = true;
  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;
  public user: User;
  public options: Observable<string[]>;

  public userStatus = [{ name: 'Active', value: true }, { name: 'Inactive', value: false }];
  public phoneNumberMask = GLOBALS.masks.mobile;

  public userLabels = User.attributesLabels;

  // for Mobile Number
  public countriesOptions: ICountryOption[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private userService: UserService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private configurationService: ConfigurationService,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    this.findCountryAttrbutesList();
  }

  private initializePage() {
    if (this.pageAct === 'add') {
      this.initAddPage();
    } else {
      this.getData();
    }
  }

  private getData() {
    this.route.params.subscribe(params => {
      this.userService.find(params['id']).subscribe(
        response => {
          this.user = response;
        },
        error => {
          this.loaded = true;
        },
        () => {
          this.fg = this.fb.group(new User().updateValidationRules());
          this.fg.patchValue(this.user);
          this.patchMobileNumber();
          if (this.pageAct === 'view') {
            this.initViewPage();
          } else if (this.pageAct === 'update') {
            this.initUpdatePage();
          }
          this.loaded = true;
        }
      );
    });
  }

  private initAddPage() {
    this.layoutService.setPageTitle({ title: 'Add User' });
    this.user = new User();
    this.fg = this.fb.group(new User().createValidationRules());

    this.fg.enable();

    this.loaded = true;
  }

  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'User: ' + this.user.username,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Users', url: '/security/users' },
        { label: this.user.username }
      ]
    });
  }

  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'User: ' + this.user.username,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Users', url: '/security/users' },
        {
          label: this.user.username,
          url: `/security/users/view/${this.user.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  public saveData(item: User) {

    this.boxLoaded = false;
    // this.loaded = false;
    if (item.countryCode && item.phoneNumber) {

      item.phoneNumber = item.countryCode + '-' + item.phoneNumber;

    }
    if (this.pageAct === 'add') {
      this.create(item);
    } else {
      this.update(this.user.id, item);
    }
  }

  public delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        let result;
        this.userService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'User has been deleted.', msgType: 'success' });
          },
          error => {
            this.loaded = false;
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            // TODO: low reset form data if error occurs
          },

          () => {
            this.loaded = false;
          }
        );
      }
    });
  }

  private create(item: User) {

    this.userService.create(item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'User has been created.', msgType: 'success' });
        this.router.navigate([`/security/users/view/${response.id}`]);

      },
      error => {
        if (error.status == 403) {
          this.fg.controls['username'].setErrors({ exists: true });
        } else {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }

      },
      () => {
        this.loaded = true;
        this.boxLoaded = true;
      }
    );
  }

  private update(id: number, item: User) {

    this.userService.update(id, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'User has been updated.', msgType: 'success' });
        this.router.navigate([`/security/users/view/${id}`]);
      },
      error => {
        if (error.status == 403) {
          this.fg.controls['username'].setErrors({ exists: true });
        } else {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      },
      () => {
        this.loaded = true;
        this.boxLoaded = true;
      }
    );
  }


  // For Mobile Number
  // Getting the countries from configuratiuon and then pushing into the array of object
  findCountryAttrbutesList() {
    this.configurationService.findAttributesList('country').subscribe(
      response => {
        // iterate countries
        response.forEach(country => {
          let abbr;
          let cCode;
          // iterate countries config options
          country.configurationOptions.forEach(configurationOption => {
            // check if option is abbreviation
            if (configurationOption.key === 'abbreviation') {
              abbr = configurationOption.value
              // check if option is country code
            } else if (configurationOption.key === 'countryCode') {
              cCode = configurationOption.value
            }
            // if both exists then push into the array
            if (abbr && cCode) {

              this.countriesOptions.push({ abbreviation: abbr, countryCode: cCode })
            }
          });
        });
      },
      error => console.log(error),
      () => { }
    );
  }

  // patching mobile number values
  patchMobileNumber() {
    // for mobile number
    let mobileNumber: string = this.user.phoneNumber;
    if (mobileNumber) {
      let countryCode: string = mobileNumber.substring(0, mobileNumber.indexOf('-'));
      let mobileNumberSecondPart: string = mobileNumber.substring(mobileNumber.indexOf('-') + 1, mobileNumber.length);

      this.fg.get('countryCode').patchValue(countryCode);
      this.fg.get('phoneNumber').patchValue(mobileNumberSecondPart);
    }


  }
}
