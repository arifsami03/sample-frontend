import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

import { PageAnimation } from '../../../../shared/helper';
import { User, Group, Role } from '../../../models';

import { UserService, UserGroupService, RoleAssignmentService, FeaturePermissionService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { AssignGroupToUserComponent } from '../../dialogs/assign-group-to-user/assign-group-to-user.component';
import {  AssignRoleToUserComponent } from '../../dialogs/assign-role-to-user/assign-role-to-user.component';

@Component({
  selector: 'security-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css'],
  animations: [PageAnimation]
})
export class UserViewComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public boxLoaded: boolean = true;
  public showDivBar: boolean = false;

  public pageTitle: string;
  public user: User = new User();
  public fg: FormGroup;
  public userLabels = User.attributesLabels;
  public groupLabels = Group.attributesLabels;
  public roleLabels = Role.attributesLabels;
  roleDisplayedColumns = ['name', 'options'];
  groupDisplayedColumns = ['name', 'options'];
  roleDataSource: any;
  groupDataSource: any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private featurePermissionService: FeaturePermissionService,
    private userGroupService: UserGroupService,
    private roleAssignmentService: RoleAssignmentService,
    private layoutService: LayoutService,
    private matDialog: MatDialog,
    private fb: FormBuilder,
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.user.id = param.id;
    });
    this.getData(this.user.id);

    // initialize formGroup to view data 
    this.fg = this.fb.group(new User().createValidationRules());
  }

  private getData(id: number) {
    this.userService.find(id).subscribe(
      response => {
        this.user = response;
        this.user.activeLabel = this.user.isActive ? 'Yes' : 'No';
        this.user.superUserLabel = this.user.isSuperUser ? 'Yes' : 'No';
      },
      error => {
        this.loaded = true;
      },
      () => {
        this.loaded = true;
        this.fg.patchValue(this.user);
        this.roleDataSource = new MatTableDataSource(this.user.roleAssignments);
        this.groupDataSource = new MatTableDataSource(this.user.userGroups);
        this.fg.disable();
        this.modifyPageHeader();
      }
    );
  }

  public assignGroup(userId: number) {
    const dialogRef = this.matDialog.open(AssignGroupToUserComponent, {
      width: '500px',
      data: { userId: userId, userName: this.user.username }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.layoutService.flashMsg({ msg: 'Group has been added to user.', msgType: 'success' });
        this.getData(userId);
      }
    });
  }
  public assignRole(userId: number) {
    const dialogRef = this.matDialog.open(AssignRoleToUserComponent, {
      width: '500px',
      data: { userId: userId, userName: this.user.username }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.layoutService.flashMsg({ msg: 'Role has been added to user.', msgType: 'success' });
        this.getData(userId);
      }
    });
  }

  public delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.userService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'User has been deleted.', msgType: 'success' });
            this.router.navigate(['/security/users']);
          },
          error => {
            this.loaded = true;
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          },

          () => {
            this.loaded = true;
          }
        );
      }
    });
  }
  public deleteFromGroup(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.userGroupService.revoke(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Group has deleted from user', msgType: 'success' });
            this.getData(this.user.id);
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          },

          () => {

          }
        );
      }
    });
  }
  public deleteFromRole(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        // this.loaded = false;this.loaded = true;
        let result;
        this.roleAssignmentService.delete(id).subscribe(
          response => {
            result = response;
            this.layoutService.flashMsg({ msg: 'Role has been deleted from user.', msgType: 'success' });
            this.getData(this.user.id);
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          },

          () => {

          }
        );
      }
    });
  }

  private modifyPageHeader() {
    this.layoutService.setPageTitle({
      title: 'User: ' + this.user.username,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Users', url: '/security/users' },
        { label: this.user.username }
      ]
    });
  }

}
