import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { FormBuilder } from '@angular/forms';

import { PageAnimation } from '../../../../shared/helper';
import { LayoutService } from '../../../../layout/services';
import { RoleService } from '../../../services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { Role } from '../../../models';

@Component({
  selector: 'security-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css'],
  animations: [PageAnimation]
})
export class RoleListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public attrLabels = Role.attributesLabels;

  displayedColumns = ['name', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    public layoutService: LayoutService,
    private roleService: RoleService,
    public dialog: MatDialog,
    public matDialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Roles' });
    this.getRecords();
  }

  getRecords(): void {
    let data;
    this.roleService.getAll().subscribe(
      response => {
        data = response;
      },
      error => {
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.dataSource = new MatTableDataSource<Role>(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        let result;
        this.roleService.delete(id).subscribe(
          response => {
            result = response;
            this.layoutService.flashMsg({ msg: 'Role has been deleted.', msgType: 'success' });
            this.getRecords();
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          },
          () => {
            // this.loaded = true;
            // if (result.success) {
            //   this.success = true;
            //   this.successMessage = result.message;
            //   setTimeout(() => {
            //     this.getRecords();
            //   }, 1000);
            // } else {
            //   // Error Display
            //   this.error = true;
            //   this.errorMessage = result.error;
            //   // Hide error message after one sec
            //   setTimeout(() => {
            //     this.error = false;
            //   }, 1000);
            // }
          }
        );
      }
    });
  }

  sortData() {
    this.dataSource.sort = this.sort;
  }
}
