import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { PageAnimation } from '../../../../shared/helper';
import { Role } from '../../../models';
import { RoleService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';

@Component({
  selector: 'security-role-form',
  templateUrl: './role-form.component.html',
  styleUrls: ['./role-form.component.css'],
  animations: [PageAnimation]
})
export class RoleFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;
  public role: Role;

  public componentLabels = Role.attributesLabels;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private roleService: RoleService,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = activatedRoute.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  private initializePage() {
    this.fg = this.fb.group(new Role().validationRules());
    if (this.pageAct === 'add') {
      this.initAddPage();
    } else {
      this.getData();
    }
  }

  private getData() {
    this.activatedRoute.params.subscribe(params => {
      this.roleService.find(params['id']).subscribe(
        response => {
          this.role = response;
        },
        error => {
          this.loaded = true;
        },
        () => {

          this.fg.patchValue(this.role);
          if (this.pageAct === 'view') {
            this.initViewPage();
          } else if (this.pageAct === 'update') {
            this.initUpdatePage();
          }
          this.loaded = true;
        }
      );
    });
  }

  private initAddPage() {
    this.layoutService.setPageTitle({ title: 'Add Role' });
    this.role = new Role();
    this.fg.enable();

    this.loaded = true;
  }

  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Role: ' + this.role.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Roles', url: '/security/roles' },
        { label: this.role.name }
      ]
    });
  }

  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Role: ' + this.role.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Roles', url: '/security/roles' },
        {
          label: this.role.name,
          url: `/security/roles/view/${this.role.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  public saveData(item: Role) {
    this.loaded = false;
    if (this.pageAct === 'add') {
      this.create(item);
    } else {
      this.update(this.role.id, item);
    }
  }

  public delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        let result;
        this.roleService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Role has been deleted.', msgType: 'success' });
            this.router.navigate(['/security/roles']);
          },
          error => {
            this.loaded = true;
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          },

          () => {
            this.loaded = true;
          }
        );
      }
    });
  }

  private create(item: Role) {

    this.roleService.create(item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Role has been created.', msgType: 'success' });
        this.router.navigate([`/security/roles/view/${response.id}`]);
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
      },
      () => {
        this.loaded = true;
      }
    );
  }

  private update(id: number, item: Role) {
    this.loaded = false;

    this.roleService.update(id, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Role has been updated.', msgType: 'success' });
        this.router.navigate([`/security/roles/view/${id}`]);
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
      },
      () => {
        this.loaded = true;
      }
    );
  }

}
