import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';


import { PageAnimation } from '../../../../shared/helper';
import { User, Role, FeaturePermission, Group } from '../../../models';
import { RoleService, RoleAssignmentService, FeaturePermissionService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { AssignUserToRoleComponent } from '../../dialogs/assign-user-to-role/assign-user-to-role.component';
import { AssignGroupToRoleComponent } from '../../dialogs/assign-group-to-role/assign-group-to-role.component';

@Component({
  selector: 'security-role-view',
  templateUrl: './role-view.component.html',
  styleUrls: ['./role-view.component.css'],
  animations: [PageAnimation]
})
export class RoleViewComponent implements OnInit {

  public pageState = 'active';
  public loaded: boolean = false;
  public disableClass: boolean = true;

  public showBoxLoader: boolean = false;

  public fg: FormGroup;
  public pageTitle: string;
  public role: Role = new Role();
  public userLabels = User.attributesLabels;
  public groupLabels = Group.attributesLabels;
  public roleLabels = Role.attributesLabels;


  userDisplayedColumns = ['username', 'options'];
  groupDisplayedColumns = ['name', 'options'];
  userDataSource: any;
  groupDataSource: any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private roleService: RoleService,
    private featurePermissionService: FeaturePermissionService,
    private roleAssignmentService: RoleAssignmentService,
    private layoutService: LayoutService,
    private matDialog: MatDialog,
    private fb: FormBuilder,
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.role.id = param.id;
    });
    this.getData(this.role.id);
    // this.findAllFeatures(this.role.id);
  }

  private getData(id: number) {
    this.roleService.find(id).subscribe(
      response => {
        this.role = response;
      },
      error => {
        this.loaded = true;
      },
      () => {
        // initialize formGroup to view data 
        this.fg = this.fb.group(new Role().validationRules());
        this.fg.patchValue(this.role);
        this.fg.disable();
        this.userDataSource = new MatTableDataSource(this.role.assignedUsers);
        this.groupDataSource = new MatTableDataSource(this.role.assignedGroups);
        this.loaded = true;
        this.findAllFeatures(this.role.id);
        this.modifyPageHeader();
      }
    );
  }

  private findAllFeatures(roleId: number) {
    this.roleService.findAllFeatures(roleId).subscribe(
      response => {
        this.role.assignedPermissions = response;
      },
      error => { },
      () => { }
    );
  }

  public addGroup(id: number) {
    const dialogRef = this.matDialog.open(AssignGroupToRoleComponent, {
      width: '500px',
      data: { roleId: id, roleName: this.role.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.layoutService.flashMsg({ msg: 'Group has been added to role.', msgType: 'success' });
        this.getData(this.role.id);
      }
    });
  }

  public deleteGroup(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.roleAssignmentService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Group has been deleted from role.', msgType: 'success' });
            this.getData(this.role.id);
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          },

          () => {
          }
        );
      }
    });
  }

  public addUser(id: number) {
    const dialogRef = this.matDialog.open(AssignUserToRoleComponent, {
      width: '500px',
      data: { roleId: id, roleName: this.role.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.layoutService.flashMsg({ msg: 'User has been added to role.', msgType: 'success' });
        this.getData(this.role.id);
      }
    });
  }

  public deleteUser(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.roleAssignmentService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'User has been deleted from role.', msgType: 'success' });
            this.getData(this.role.id);
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          },

          () => {
          }
        );
      }
    });
  }

  public changePermission(roleId: number) {
    this.showBoxLoader = true;

    this.featurePermissionService
      .updateRolePermissions(roleId, this.role.assignedPermissions)
      .subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Permisssions has been updated.', msgType: 'success' });
        },
        error => {
          this.showBoxLoader = false;
          this.disableEditMode(true);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        },

        () => {
          this.showBoxLoader = false;
          this.disableEditMode(true);
        }
      );
  }

  public delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.roleService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Role has been deleted.', msgType: 'success' });
            this.router.navigate(['/security/roles']);
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          },

          () => { }
        );
      }
    });
  }

  private modifyPageHeader() {
    this.layoutService.setPageTitle({
      title: 'Role: ' + this.role.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Roles', url: '/security/roles' },
        { label: this.role.name }
      ]
    });
  }

  private getLoggedInUserInfo(parameter: string): string {
    return localStorage.getItem(parameter);
  }


  public disableEditMode(param: boolean) {
    this.disableClass = param;
  }
}
