import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  MatPaginator,
  MatTableDataSource,
  MatDialog,
  MatSort
} from '@angular/material';
import { WFStateFlowService, WFStateService, WorkFlowService, } from '../../../../../../settings/services';
import { LayoutService } from '../../../../../../layout/services';
import { RoleWFStateFlowService } from '../../../../../services';
import { WFStateModel, WFStateFlowModel, WorkFlowModel } from '../../../../../../settings/models';

import { RoleWFStateFlowModel } from '../../../../../models';


@Component({
  selector: 'role-wf-state-flow-form',
  templateUrl: './role-wf-state-flow-form.component.html',
  styleUrls: ['./role-wf-state-flow-form.component.css']
})
export class RoleWFStateFlowFormComponent {
  public loaded: boolean = false;
  public wfState: WFStateModel[];
  public wfStateFlow: WFStateFlowModel[];
  public workFlow: WorkFlowModel[];
  displayedColumns = ['from', 'to', 'option'];
  public roleWFStateFlow: RoleWFStateFlowModel;
  public fg: FormGroup;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;
  public success: Boolean;
  public state: string;
  public displayFlows: boolean = false;
  public selectedFlow = [];

  public flowsList = [];
  deletedWorkFlows = [];
  dataSource: any;
  listOfFlows: any = [];
  public componentLabels = RoleWFStateFlowModel.attributesLabels;

  constructor(
    private wfStateService: WFStateService,
    private wfStateFlowService: WFStateFlowService,
    private wfService: WorkFlowService,
    private layoutService: LayoutService,
    private roleWFStateFlowService: RoleWFStateFlowService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<RoleWFStateFlowFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.loaded = true;
  }

  /**
  * ngOnInit
  */
  ngOnInit() {
    this.initializePage();
    this.getWorkFlow();
    this.getWFStateFlowByRole();

    if (this.data.type != 'Add Work Flow Permission') {
      this.fg.controls['WFId'].setValue(this.data.WFId);
      this.allStatesOfWF();
    }
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new RoleWFStateFlowModel().validationRules());

  }

  /**
   * Function to get Work Flows List To bind into Work Flow Drop down.
   */
  getWorkFlow() {
    this.loaded = false;
    this.wfService.index().subscribe(
      response => {
        this.workFlow = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Function to get all the list of flows assigned to Role id.
   */

  getWFStateFlowByRole() {
    this.loaded = false;
    this.roleWFStateFlowService.getWFStateFlowByRole(this.data.roleId).subscribe(
      response => {
        if (response) {
          for (var i = 0; i < response.length; i++) {
            this.listOfFlows.push(response[i].WFStateFlowId)
          }
        }

      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {

        this.loaded = true;

      }
    );
  }

  /**
   * Function to get list of flows associated with the work flow.
   * @param event Work Flow Drop down change Event
   */

  wfChange(event) {
    this.flowsList = [];
    this.wfStateFlowService.getFlowByWFId(event.value).subscribe(
      response => {
        this.wfStateFlow = response;

        for (var i = 0; i < response.length; i++) {

          if (this.listOfFlows.includes(response[i]['id'])) {
            this.flowsList.push(
              {
                id: response[i]['id'],
                from: response[i].from,
                to: response[i].to,
                fromId: response[i].fromId,
                toId: response[i].toId,
                checked: true
              }
            )
          }
          else {
            this.flowsList.push(
              {
                id: response[i]['id'],
                from: response[i].from,
                to: response[i].to,
                fromId: response[i].fromId,
                toId: response[i].toId,
                checked: false
              }
            )
            //}
          }

        }

        this.dataSource = new MatTableDataSource<WFStateFlowModel>(this.flowsList);
        this.displayFlows = true;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Funcation to get all list of work flows.
   */

  allStatesOfWF() {
    this.loaded = false;
    this.flowsList = [];
    this.wfStateFlowService.getFlowByWFId(this.data.WFId).subscribe(
      response => {
        this.wfStateFlow = response;
        for (var i = 0; i < response.length; i++) {

          if (this.listOfFlows.includes(response[i]['id'])) {
            this.flowsList.push(
              {
                id: response[i]['id'],
                from: response[i].from,
                to: response[i].to,
                fromId: response[i].fromId,
                toId: response[i].toId,
                checked: true
              }
            )
          }
          else {
            this.flowsList.push(
              {
                id: response[i]['id'],
                from: response[i].from,
                to: response[i].to,
                fromId: response[i].fromId,
                toId: response[i].toId,
                checked: false
              }
            )
            //}
          }

        }
        this.dataSource = new MatTableDataSource<WFStateFlowModel>(this.flowsList);
        this.displayFlows = true;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Function Check if Record is going to delete; add that id into DeletedWorkFlowsIds arrays.
   * @param event CheckBox Event
   * @param id WF State Flow ID
   */
  checkChange(event, id) {
    if (event.checked) {
      this.selectedFlow.push({
        roleId: this.data.roleId,
        WFStateFlowId: id
      });

      for (var i = 0; i < this.deletedWorkFlows.length; i++) {
        if (id == this.deletedWorkFlows[i].WFStateFlowId) {
          this.deletedWorkFlows.splice(i, 1);
        }
      }


    }
    else {
      for (var i = 0; i < this.selectedFlow.length; i++) {
        if (id == this.selectedFlow[i]['WFStateFlowId']) {
          this.selectedFlow.splice(i, 1);
        }

      }
      this.deletedWorkFlows.push({ WFStateFlowId: id, roleId: this.data.roleId });


    }

  }




  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close(false);
  }

  /**
 * Add, update, and Delete data in database when save button is clicked
 */
  public saveData() {
    let check = false;
    if (this.selectedFlow) {
      check = true;
    } else {
      check = false;
    }
    var postData = {
      data: this.selectedFlow,
      deletedIds: this.deletedWorkFlows
    }
    this.roleWFStateFlowService.create(postData).subscribe(
      response => {
        if (!check) {
          this.layoutService.flashMsg({ msg: 'Work Flow Permission is added.', msgType: 'success' });
        }
        else {
          this.layoutService.flashMsg({ msg: 'Work Flow Permission is updated.', msgType: 'success' });
        }
        // this.layoutService.flashMsg({ msg: 'Work Flow Permission is added.', msgType: 'success' });
        this.dialogRef.close(true);
      },
      error => {
        console.log(error);
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        this.loaded = true;
      }
    );

  }



}
