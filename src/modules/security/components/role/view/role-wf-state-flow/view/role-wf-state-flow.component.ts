import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';

import { RoleWFStateFlowService } from '../../../../../services';
import { LayoutService } from '../../../../../../layout/services';
import { RoleWFStateFlowFormComponent } from '../form/role-wf-state-flow-form.component';
import { ConfirmDialogComponent } from '../../../../../../shared/components';

import { RoleWFStateFlowModel } from '../../../../../models';
import { GLOBALS } from '../../../../../../app/config/globals';

@Component({
  selector: 'role-wf-state-flow',
  templateUrl: './role-wf-state-flow.component.html',
  styleUrls: ['./role-wf-state-flow.component.css']
})
export class RoleWFStateFlowComponent implements OnInit {
  public loaded: boolean = false;
  public pageActions = GLOBALS.pageActions;

  public error: Boolean;
  displayedColumns = ['from', 'to'];
  public fg: FormGroup;
  public pageAct: string;
  public roleWFStateFlow: RoleWFStateFlowModel[];
  public id: string;
  dataSource: any;
  public componentLabels = RoleWFStateFlowModel.attributesLabels;
  @Input() roleId: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private roleWFStateFlowService: RoleWFStateFlowService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    // this.wfStateFlowService.reloadStateFlow$.subscribe((res) => {
    //   if (res == true) {
    //     this.getData();
    //   }
    // })
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  loadFlows(event) {
    this.getData();
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.getData();
  }

  /**
   * get all data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.roleWFStateFlowService.list(this.roleId).subscribe(
        response => {
          this.roleWFStateFlow = response;
          this.dataSource = new MatTableDataSource<RoleWFStateFlowModel>(this.roleWFStateFlow['flows']);
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  addClick() {
    const dialogRef = this.matDialog.open(RoleWFStateFlowFormComponent, {
      width: '1230px',
      maxWidth: '1230px',
      // height:'500px',
      data: { roleId: this.roleId, type: 'Add Work Flow Permission', WFId: '' },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getData();
      }
    });
  }
  editClick(WFId: string) {
    const dialogRef = this.matDialog.open(RoleWFStateFlowFormComponent, {
      width: '1230px',
      maxWidth: '1230px',
      // height:'700px',
      data: { roleId: this.roleId, type: 'Update Work Flow Permission: ' + WFId, WFId: WFId }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getData();
      }
    });
  }
}
