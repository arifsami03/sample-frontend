import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from '../../../../shared/helper';
import { MeritListService } from '../../../services';
import { ScholarshipService } from '../../../../fee-management/services';
import { ProgramDetailsService } from '../../../../academic/services';

import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { MeritListModel } from '../../../models';
import { ScholarshipModel } from '../../../../fee-management/models';
import { ProgramDetailsModel } from '../../../../academic/models';
import { GLOBALS } from '../../../../app/config/globals';
import * as _ from 'lodash';

@Component({
  selector: 'setting-merit-list-form',
  templateUrl: './merit-list-form.component.html',
  styleUrls: ['./merit-list-form.component.css'],
  animations: [PageAnimation],
  providers: [ProgramDetailsService, ScholarshipService]
})
export class MeritListFormComponent implements OnInit {
  public pageState = 'active';

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public scholarships: ScholarshipModel[];
  public programDetails: ProgramDetailsModel[];

  public fg: FormGroup;
  public pageAct: string;
  public meritList: MeritListModel;
  public options: Observable<string[]>;
  public componentLabels = MeritListModel.attributesLabels;
  public _yearMask = GLOBALS.masks.year;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private meritListService: MeritListService,
    private scholarshipService: ScholarshipService,
    private programDetailsService: ProgramDetailsService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();

    this.findProgramDetail();
    this.findScholarshipAttributesList();
  }

  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new MeritListModel().validationRules());

    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get record
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.meritListService.find(params['id']).subscribe(
        response => {
          if (response) {
            this.meritList = response;
            this.fg.patchValue(this.meritList);
            this.findScholarshipAttributesList();

            //TODO:low need to check why we are dowing this after fetching data
            if (this.pageAct == this.pageActions.view) {

              this.initViewPage();
            } else if (this.pageAct == this.pageActions.update) {
              this.initUpdatePage();
            }
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Merit List: Create' });

    this.meritList = new MeritListModel();

    this.fg.enable();

    this.loaded = true;
  }

  /**
   * initialize the view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Merit List : ' + this.meritList.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Merit List ',
          url: '/admission/meritLists'
        },
        { label: this.meritList.title }
      ]
    });
  }

  /**
   * initialize the update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Merit List: ' + this.meritList.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Merit List',
          url: '/admission/meritLists'
        },
        {
          label: this.meritList.title,
          url: `/admission/meritLists/view/${this.meritList.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: MeritListModel) {
    if (this.pageAct === this.pageActions.create) {
      this.meritListService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Merit List has been created.', msgType: 'success' });
          this.router.navigate([`/admission/meritLists`]);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.meritListService.update(this.meritList.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Merit List has been updated.', msgType: 'success' });
          this.router.navigate([`/admission/meritLists`]);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      );
    }
  }

  /**
   * Delete record
   *
   */
  delete(id: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          this.meritListService.delete(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Merit List has been deleted.', msgType: 'success' });
              this.router.navigate(['/admission/meritLists']);
            },
            error => {
              console.log(error);
              this.loaded = true;
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            }
          );
        }
      });
  }
  findScholarshipAttributesList() {
    this.scholarshipService.findAttributesList().subscribe(
      _scholarships => {
        this.scholarships = _scholarships;

        
      },
      error => { },
      () => { }
    );
  }
  findProgramDetail() {
    this.programDetailsService.findAttributesList().subscribe(
      _programDetails => {
        this.programDetails = _programDetails;
      },
      error => { },
      () => { }
    );
  }



}
