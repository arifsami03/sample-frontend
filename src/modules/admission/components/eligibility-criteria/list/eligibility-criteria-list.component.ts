import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router } from '@angular/router';

import { PageAnimation } from 'modules/shared/helper';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { LayoutService } from 'modules/layout/services';
import { EligibilityCriteriaService } from 'modules/admission/services';
import { EligibilityCriteriaModel } from 'modules/admission/models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'eligibility-criteria-list',
  templateUrl: './eligibility-criteria-list.component.html',
  styleUrls: ['./eligibility-criteria-list.component.css'],
  animations: [PageAnimation]
})
export class EligibilityCriteriaListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public data: EligibilityCriteriaModel[] = [new EligibilityCriteriaModel()];
  public attrLabels = EligibilityCriteriaModel.attributesLabels;

  displayedColumns = ['targetProgramTitle', 'ageFromYY', 'ageFromMM', 'ageToYY', 'ageToMM', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public error: Boolean;

  constructor(
    private eligibilityCriteriaService: EligibilityCriteriaService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Eligibility Criteria' });
    this.getRecords();
  }

  /**
   * Get records
   */
  getRecords() {
    this.loaded = true;
    this.eligibilityCriteriaService.index().subscribe(
      response => {
        this.data = response;
        for (let i = 0; i < response.length; i++) {
          //this.data[i].targetProgramTitle=
        }

        this.dataSource = new MatTableDataSource<EligibilityCriteriaModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
    return this.data;
  }

  /**
   * Apply filter and serch in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete record
   */
  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.eligibilityCriteriaService.delete(id).subscribe(
          response => {

            if (response && (response['children']) ? response['children'].length > 0 : false) {

              this.loaded = true;
              this.pageState = 'active';

              this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

            } else {

              this.layoutService.flashMsg({ msg: 'Eligibility Criteria has been deleted.', msgType: 'success' });
              this.getRecords();

            }

          },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
            this.pageState = 'active';
          }
        );
      }
    });
  }

  sortData() {
    this.dataSource.sort = this.sort;
  }

}
