import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { PageAnimation } from 'modules/shared/helper';
import { EligibilityCriteriaService } from 'modules/admission/services';
import { PassingYearService } from 'modules/settings/services';
import { AcademicCalendarService, CourseService, ProgramService } from 'modules/academic/services';
import { AcademicCalendarModel, CourseModel, ProgramModel } from 'modules/academic/models';
import { LayoutService } from 'modules/layout/services';
import { EligibilityCriteriaModel } from 'modules/admission/models';
import { PassingYearModel } from 'modules/settings/models';
import { GLOBALS } from 'modules/app/config/globals';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from '../../../../shared/components';


/**
 * TODO:high Need to verify/optimize this componennt. Line of code for this component is abocve 1000. There must be some better way to do this.
 */
@Component({
  selector: 'eligibility-criteria-form',
  templateUrl: './eligibility-criteria-form.component.html',
  styleUrls: ['./eligibility-criteria-form.component.css'],
  animations: [PageAnimation],
  providers: [PassingYearService, AcademicCalendarService, CourseService, ProgramService]
})
export class EligibilityCriteriaFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;

  public showDivBar = false;
  public pageActions = GLOBALS.pageActions;

  public error: Boolean;
  public fg: FormGroup;
  public pageAct: string;
  public eligibilityCriteria: EligibilityCriteriaModel;

  public yearMonthMask = GLOBALS.masks.yearMonth;

  public academicCalendars = [new AcademicCalendarModel()];
  public editedECPreReq = [];
  public editedECSubjects = [];
  public editedECPassingYear = [];

  public componentLabels = EligibilityCriteriaModel.attributesLabels;

  //Passing Year DropDown
  public multiplePassingYear: any = [];
  public passingYearList = [new PassingYearModel()];
  public allPassingYearList = [new PassingYearModel()];
  public selectedPassingYear = [];
  public addButtonPassingYear: number = 0;

  //PreReq DropDown
  public multipleSubjects: any = [];
  public subjectsList = [new CourseModel];
  public allSubjectsList = [new CourseModel()];
  public selectedSubjects = [];
  public addButtonSubjects: number = 0;

  //Pre Requisite Programs DropDown
  public multiplePreReq: any = [];
  public PreReqList = [new ProgramModel];
  public allPreReqList = [new ProgramModel()];
  public selectedPreReq = [];
  public addButtonPreReq: number = 0;

  ///////////Target Program DropDown
  public targetProgramsList: ProgramModel[] = [];


  public academicMonths = GLOBALS.MONTHS;
  public academicYears: number[] = GLOBALS.academicYears;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private eligibilityCriteriaService: EligibilityCriteriaService,
    private academicCalendarService: AcademicCalendarService,
    private passingYearService: PassingYearService,
    private subjectsService: CourseService,
    private programService: ProgramService,
    private preReqService: ProgramService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    this.getAcademicYear();
    this.getTargetPrograms();

  }
  /**
   * Initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new EligibilityCriteriaModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
      this.getPassingYear();
      this.getSubjects();
      // this.getPreReq();
    } else {
      this.getData();
    }
  }

  /**
   * Get all data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.eligibilityCriteriaService.find(params['id']).subscribe(
        response => {

          //Setting Values
          this.eligibilityCriteria = response.eligibilityCriteria;
          this.selectedPassingYear = response.ecPassingYear;
          this.selectedSubjects = response.ecSubject;
          this.selectedPreReq = response.ecPreReq;

          //Setting Edited Values of the Selected passing Year, Subject and Pre Requisite to compare Further
          //const object2 = Object.assign({}, object1);
          this.editedECPreReq = Object.assign([], response.ecPreReq);
          this.editedECSubjects = Object.assign([], response.ecSubject);
          this.editedECPassingYear = Object.assign([], response.ecPassingYear);

          //Setting Values of Eligibility Criteria
          this.fg.patchValue(this.eligibilityCriteria);
          if (!this.error) {

            if (this.pageAct === this.pageActions.view) {
              this.initViewPage();
              this.getPassingYear();
              this.getSubjects();
              this.getPreReq();

            } else if (this.pageAct === this.pageActions.update) {
              this.initUpdatePage();
              this.getPassingYear();
              this.getSubjects();
              this.getPreReq();
            }
            this.loaded = true;
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Initialize create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Eligibility Criteria: Create' });
    this.eligibilityCriteria = new EligibilityCriteriaModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * Initialize view page
   */
  private initViewPage() {
    this.fg.disable();
    this.layoutService.setPageTitle({
      title: 'Eligibility Criteria: View ',
      // breadCrumbs: [
      //   { label: 'Home', url: '/home/dashboard' },
      //   { label: 'Eligibility Criteria ', url: '/admission/eligibilityCriteria' },
      //   { label: this.eligibilityCriteria.targetProgramTitle }
      // ]
    });
  }

  /**
   * Initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Eligibility Criteria: Update',
      // breadCrumbs: [
      //   { label: 'Home', url: '/home/dashboard' },
      //   { label: 'Eligibility Criteria', url: '/admission/eligibilityCriteria' },
      //   {
      //     url: `/admission/eligibilityCriteria/view/${this.eligibilityCriteria.id}`
      //   },
      //   { label: 'Update' }
      // ]
    });
  }

  /**
   * Add or update data in database when save button is clicked
   */
  public saveData(item: EligibilityCriteriaModel) {

    let deletedPreReqIds = [];
    let deletedSubjectIds = [];
    let deletedPassingYearIds = [];

    //If data is going to be saved in Edit Mode
    if (this.pageAct === this.pageActions.update) {

      //Check for it User Delete the record
      //If Selected array does not contain the id which ispresent in Editted array this mean it is going to be delete.

      for (let i = 0; i < this.editedECPreReq.length; i++) {
        if (this.selectedPreReq.includes(this.editedECPreReq[i])) {

        }
        else {
          deletedPreReqIds.push(this.editedECPreReq[i])
        }

      }

      //Check for it User Delete the record
      //If Selected array does not contain the id which ispresent in Editted array this mean it is going to be delete.

      for (let i = 0; i < this.editedECPassingYear.length; i++) {
        if (this.selectedPassingYear.includes(this.editedECPassingYear[i])) {

        }
        else {
          deletedPassingYearIds.push(this.editedECPassingYear[i])
        }

      }

      //Check for it User Delete the record
      //If Selected array does not contain the id which ispresent in Editted array this mean it is going to be delete.

      for (let i = 0; i < this.editedECSubjects.length; i++) {
        if (this.selectedSubjects.includes(this.editedECSubjects[i])) {

        }
        else {
          deletedSubjectIds.push(this.editedECSubjects[i])
        }

      }
    }

    //Data to be posted on Backend for the save or update process.

    let postData = {
      eligibilityCriteria: item,
      ecPreReqProgram: this.selectedPreReq,
      ecSubject: this.selectedSubjects,
      ecPassingYear: this.selectedPassingYear,
      deletedPreReqIds: deletedPreReqIds,
      deletedSubjectIds: deletedSubjectIds,
      deletedPassingYearIds: deletedPassingYearIds,
    }

    this.showDivBar = true;
    let id;
    if (this.pageAct === this.pageActions.create) {
      item.id = null;
      this.eligibilityCriteriaService.create(postData).subscribe(response => {

        this.layoutService.flashMsg({ msg: 'Eligibility Criteria has been created.', msgType: 'success' });
        this.router.navigate([`/admission/eligibilityCriteria`]);

      },
        error => {

          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;

        }, () => {

          this.showDivBar = false;

        }
      );
    } else if (this.pageAct === this.pageActions.update) {

      this.eligibilityCriteriaService.update(this.eligibilityCriteria.id, postData).subscribe(response => {

        this.layoutService.flashMsg({ msg: 'Eligibility Criteria has been updated.', msgType: 'success' });
        this.router.navigate([
          `/admission/eligibilityCriteria`
        ]);
      },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        }, () => {
          this.showDivBar = false;
        }

      );
    }



  }

  /**
   * Delete record
   */
  delete(id: number) {

    const dialogRef = this.matDialog.open(ConfirmDialogComponent,
      { width: GLOBALS.deleteDialog.width, data: { message: GLOBALS.deleteDialog.message } }
    );

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.eligibilityCriteriaService.delete(id).subscribe(response => {

          if (response && (response['children']) ? response['children'].length > 0 : false) {

            this.loaded = true;
            this.pageState = 'active';

            this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

          } else {

            this.layoutService.flashMsg({ msg: 'Eligibility Criteria has been deleted.', msgType: 'success' });
            this.router.navigate(['/admission/eligibilityCriteria']);

          }

        },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
            this.pageState = 'active';
          }
        );
      }
    });
  }


  /**
   * Function to get Academic Year List
   */
  getAcademicYear() {

    this.academicCalendarService.findAttributesList().subscribe(
      response => {
        this.academicCalendars = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }

  targetProgramChange(event) {

    this.selectedPreReq = [];
    this.multiplePreReq = [];
    this.addButtonPreReq = 0;
    // this.selectedPreReq.push(event.value);
    this.getPreReq();

    // if (this.selectedPreReq.length == 0) {
    //   this.getPreReq();

    // }
    // else {
    //   this.selectedPreReq=[];
    //   this.multiplePreReq=[];
    //   this.selectedPreReq.push(event.value);
    //   this.getPreReq();
    //   // this.deletePreReq(event.value)
    // }

  }


  /////////////////////////////////SUBJECTS///////////////////////////////////////////////

  getSubjects() {

    //Get list of subjects not in the list of selected array
    this.subjectsService.getSubjectsNotSelected(this.selectedSubjects).subscribe(
      response => {
        this.subjectsList = response;

        //If selected array is empty then initialize the dropdowm.

        if (this.selectedSubjects.length == 0) {

          this.multipleSubjects.push({ subjects: [], subjectId: '' });

          for (var i = 0; i < this.subjectsList.length; i++) {
            this.multipleSubjects[this.addButtonSubjects]['subjects'].push({
              id: this.subjectsList[i].id,
              title: this.subjectsList[i].title
            });
          }

        }

        //If Selected array is not empty. 

        else {

          //if the response of 'getSubjectsNotSelected' is empty that means all the subjects are selected and should be open in edit mode.

          if (this.subjectsList.length == 0) {
            this.allSubjectsSelected();

          }

          //if the response of 'getSubjectsNotSelected' is not empty that means all the subjects are not selected but should be open in edit mode.

          else {

            this.notAllSubjectSelected();

          }
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Function Fires when all the Subjects is selected
   */
  allSubjectsSelected() {
    //To get List of all Subjects
    this.subjectsService.findAttributesList().subscribe(
      response => {

        this.allSubjectsList = response;

        this.multipleSubjects.push({ subjects: [], subjectId: '' });

        //to Bind dropdown when it opens in edit mode

        for (var i = 0; i < this.selectedSubjects.length; i++) {

          this.multipleSubjects[this.addButtonSubjects]['subjectId'] = this.selectedSubjects[i];

          if (i + 1 != this.selectedSubjects.length) {
            for (let j = 0; j < this.allSubjectsList.length; j++) {
              this.multipleSubjects[this.addButtonSubjects]['subjects'].push({
                id: this.allSubjectsList[j].id,
                title: this.allSubjectsList[j].title
              });
            }
          }
          else {
            for (let j = 0; j < this.allSubjectsList.length; j++) {
              if (this.allSubjectsList[j].id == this.selectedSubjects[i]) {
                this.multipleSubjects[this.addButtonSubjects]['subjects'].push({
                  id: this.allSubjectsList[j].id,
                  title: this.allSubjectsList[j].title
                });
              }

            }
          }

          if (i + 1 != this.selectedSubjects.length) {
            this.multipleSubjects.push({ subjects: [], subjectId: '' });
            this.addButtonSubjects = this.addButtonSubjects + 1;
          }


        }
      },
      error => { console.log(error) },
      () => { }
    );
  }

  /**
   * This Function is used on Change of The Value of SUbjects
   */
  subjectsChange(event) {

    let subjectId = event.value;

    //Push The Subject Id in selected array of subject.
    this.selectedSubjects.push(subjectId);
  }

  /**
   * this Function allows us to add Multiple SUbject
   */
  addMultipleSubject() {



    //Get list of Subjects not selected.

    this.subjectsService.getSubjectsNotSelected(this.selectedSubjects).subscribe(

      response => {

        this.subjectsList = response;
        if (this.subjectsList.length != 0) {
          //Increment the add subject button.

          this.addButtonSubjects = this.addButtonSubjects + 1;

          this.multipleSubjects.push({ subjects: [], subjectId: '' });
          for (var i = 0; i < this.subjectsList.length; i++) {
            this.multipleSubjects[this.addButtonSubjects]['subjects'].push({
              id: this.subjectsList[i].id,
              title: this.subjectsList[i].title
            });
          }
        }

      },
      error => console.log(error),
      () => {

      }
    );
  }

  /**
   * This Function Fires when the User Click The Add Button of the SUbject
   */
  addSubjectClick() {

    //Check if User does not select anything from dropdown and click add button then show him warning message to first select value then click add.

    if (this.multipleSubjects[this.addButtonSubjects].subjectId == '') {

      this.layoutService.flashMsg({ msg: 'First Select Subject than Add Next.', msgType: 'warning' });

    }
    else {

      this.addMultipleSubject();

    }

  }

  /**
   * When Page loads on the Edit Mode this Function fires
   */
  notAllSubjectSelected() {

    //This Functions First get list of all the subjects.
    //Then map the drop down according to the selected list of subjects.

    this.subjectsService.findAttributesList().subscribe(
      response => {
        this.allSubjectsList = response;
        this.multipleSubjects.push({ subjects: [], subjectId: '' });
        for (var i = 0; i < this.selectedSubjects.length; i++) {
          this.multipleSubjects[this.addButtonSubjects]['subjectId'] = this.selectedSubjects[i];

          for (let i = 0; i < this.allSubjectsList.length; i++) {
            this.multipleSubjects[this.addButtonSubjects]['subjects'].push({
              id: this.allSubjectsList[i].id,
              title: this.allSubjectsList[i].title
            });
          }

          this.multipleSubjects.push({ subjects: [], subjectId: '' });
          this.addButtonSubjects = this.addButtonSubjects + 1;

        }
        for (let i = 0; i < this.subjectsList.length; i++) {
          this.multipleSubjects[this.addButtonSubjects]['subjects'].push({
            id: this.subjectsList[i].id,
            title: this.subjectsList[i].title
          });
        }

      },
      error => { console.log(error) },
      () => { }
    );


  }

  deleteSubjects(subjectId) {

    //remove one index from the add button of subject.

    this.addButtonSubjects = this.addButtonSubjects - 1;

    //remove item from the NgModel of Subject which contain the same subject id

    for (var i = 0; i < this.multipleSubjects.length; i++) {

      if (this.multipleSubjects[i].subjectId == subjectId) {

        this.multipleSubjects.splice(i, 1);

      }
    }

    //remove item from the selected list of Subject which contain the same subject id

    for (var i = 0; i < this.selectedSubjects.length; i++) {

      if (this.selectedSubjects[i] == subjectId) {

        this.selectedSubjects.splice(i, 1);

      }
    }

    //then call the subjects not in the array of selected

    this.subjectsService.getSubjectsNotSelected(this.selectedSubjects).subscribe(
      response => {

        this.subjectsList = response;

        if (this.subjectsList.length != 0) {
          if (this.multipleSubjects[this.multipleSubjects.length - 1]['subjectId'] == '') {
            this.multipleSubjects[this.multipleSubjects.length - 1]['subjects'] = [];
            for (var i = 0; i < this.subjectsList.length; i++) {
              this.multipleSubjects[this.addButtonSubjects]['subjects'].push({
                id: this.subjectsList[i].id,
                title: this.subjectsList[i].title
              });
            }
          } else {
            this.addButtonSubjects = this.addButtonSubjects + 1;
            this.multipleSubjects.push({ subjects: [], subjectId: '' });
            for (var i = 0; i < this.subjectsList.length; i++) {
              this.multipleSubjects[this.addButtonSubjects]['subjects'].push({
                id: this.subjectsList[i].id,
                title: this.subjectsList[i].title
              });
            }
          }
        }
      },
      error => console.log(error),
      () => { }
    );

  }
  /////////////////////////////////Pre Requisite Programs--Same Code as Subject but change of Variables names///////////////////////////////////////////////

  getPreReq() {

    this.preReqService.getPreReqNotSelected(this.selectedPreReq).subscribe(
      response => {
        this.PreReqList = response;

        for (let i = 0; i < this.PreReqList.length; i++) {

          if (this.PreReqList[i].id == this.fg.value['programId']) {
            this.PreReqList.splice(i, 1);
          }

        }
        if (this.selectedPreReq.length == 0) {
          this.multiplePreReq.push({ preReq: [], preReqId: '' });
          for (var i = 0; i < this.PreReqList.length; i++) {
            this.multiplePreReq[this.addButtonPreReq]['preReq'].push({
              id: this.PreReqList[i].id,
              name: this.PreReqList[i].name
            });
          }
        } else {

          if (this.PreReqList.length == 0) {
            this.allPreReqSelected();

          }
          else {

            this.notAllPreReqelected();

          }
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Function Fires when all the PreReq is selected
   */
  allPreReqSelected() {
    //To get List of all PreReq
    this.preReqService.getProgramsList().subscribe(
      response => {
        this.allPreReqList = response;
        this.multiplePreReq.push({ preReq: [], preReqId: '' });
        for (var i = 0; i < this.selectedPreReq.length; i++) {

          this.multiplePreReq[this.addButtonPreReq]['preReqId'] = this.selectedPreReq[i];

          if (i + 1 != this.selectedPreReq.length) {
            for (let j = 0; j < this.allPreReqList.length; j++) {
              this.multiplePreReq[this.addButtonPreReq]['preReq'].push({
                id: this.allPreReqList[j].id,
                name: this.allPreReqList[j].name
              });
            }
          }
          else {
            for (let j = 0; j < this.allPreReqList.length; j++) {
              if (this.allPreReqList[j].id == this.selectedPreReq[i]) {
                this.multiplePreReq[this.addButtonPreReq]['preReq'].push({
                  id: this.allPreReqList[j].id,
                  name: this.allPreReqList[j].name
                });
              }

            }
          }

          if (i + 1 != this.selectedPreReq.length) {
            this.multiplePreReq.push({ preReq: [], preReqId: '' });
            this.addButtonPreReq = this.addButtonPreReq + 1;
          }


        }
      },
      error => { console.log(error) },
      () => { }
    );
  }

  /**
   * This Function is used on Change of The Value of PreReq
   */
  preReqChange(event) {


    let preReqId = event.value;

    //Push The selected Pre-Req Program Id.
    this.selectedPreReq.push(preReqId);
  }

  /**
   * this Function allows us to add Multiple SUbject
   */
  addMultiplePreReq() {



    this.preReqService.getPreReqNotSelected(this.selectedPreReq).subscribe(

      response => {

        this.PreReqList = response;
        for (let i = 0; i < this.PreReqList.length; i++) {

          if (this.PreReqList[i].id == this.fg.value['programId']) {
            this.PreReqList.splice(i, 1);
          }

        }
        if (this.PreReqList.length != 0) {
          this.addButtonPreReq = this.addButtonPreReq + 1;
          this.multiplePreReq.push({ preReq: [], preReqId: '' });
          for (var i = 0; i < this.PreReqList.length; i++) {
            this.multiplePreReq[this.addButtonPreReq]['preReq'].push({
              id: this.PreReqList[i].id,
              name: this.PreReqList[i].name
            });
          }
        }

      },
      error => console.log(error),
      () => {

      }
    );
  }

  /**
   * This Function Fires when the User Click The Add Button of the SUbject
   */
  addPreReqClick() {

    if (this.multiplePreReq[this.addButtonPreReq].preReqId == '') {
      this.layoutService.flashMsg({ msg: 'First Select Pre-Requisite Program than Add Next.', msgType: 'warning' });
    }
    else {
      this.addMultiplePreReq();
    }

  }

  /**
   * When Page loads on the Edit Mode this Function fires
   */
  notAllPreReqelected() {

    this.preReqService.getProgramsList().subscribe(
      response => {
        this.allPreReqList = response;


        this.multiplePreReq.push({ preReq: [], preReqId: '' });
        for (var i = 0; i < this.selectedPreReq.length; i++) {
          if (this.selectedPreReq[i] == this.eligibilityCriteria.programId) {

          }
          else {
            this.multiplePreReq[this.addButtonPreReq]['preReqId'] = this.selectedPreReq[i];

            for (let i = 0; i < this.allPreReqList.length; i++) {
              this.multiplePreReq[this.addButtonPreReq]['preReq'].push({
                id: this.allPreReqList[i].id,
                name: this.allPreReqList[i].name
              });
            }

            this.multiplePreReq.push({ preReq: [], preReqId: '' });
            this.addButtonPreReq = this.addButtonPreReq + 1;
          }


        }
        for (let i = 0; i < this.PreReqList.length; i++) {
          this.multiplePreReq[this.addButtonPreReq]['preReq'].push({
            id: this.PreReqList[i].id,
            name: this.PreReqList[i].name
          });
        }



      },
      error => { console.log(error) },
      () => { }
    );



  }

  deletePreReq(preReqId) {
    this.addButtonPreReq = this.addButtonPreReq - 1;
    for (var i = 0; i < this.multiplePreReq.length; i++) {
      if (this.multiplePreReq[i].preReqId == preReqId) {
        this.multiplePreReq.splice(i, 1);
      }
    }
    for (var i = 0; i < this.selectedPreReq.length; i++) {
      if (this.selectedPreReq[i] == preReqId) {
        this.selectedPreReq.splice(i, 1);
      }
    }

    this.preReqService.getPreReqNotSelected(this.selectedPreReq).subscribe(
      response => {
        this.PreReqList = response;
        for (let i = 0; i < this.PreReqList.length; i++) {

          if (this.PreReqList[i].id == this.fg.value['programId']) {
            this.PreReqList.splice(i, 1);
          }

        }
        if (this.PreReqList.length != 0) {
          if (this.multiplePreReq[this.multiplePreReq.length - 1]['preReqId'] == '') {
            this.multiplePreReq[this.multiplePreReq.length - 1]['preReq'] = [];
            for (var i = 0; i < this.PreReqList.length; i++) {
              this.multiplePreReq[this.addButtonPreReq]['preReq'].push({
                id: this.PreReqList[i].id,
                name: this.PreReqList[i].name
              });
            }
          } else {
            this.addButtonPreReq = this.addButtonPreReq + 1;
            this.multiplePreReq.push({ preReq: [], preReqId: '' });
            for (var i = 0; i < this.PreReqList.length; i++) {
              this.multiplePreReq[this.addButtonPreReq]['preReq'].push({
                id: this.PreReqList[i].id,
                name: this.PreReqList[i].name
              });
            }
          }
        }
      },
      error => console.log(error),
      () => { }
    );

  }
  /////////////////////////////////Passing Year///////////////////////////////////////////////

  getPassingYear() {

    this.passingYearService.getPassingYearNotSelected(this.selectedPassingYear).subscribe(
      response => {
        this.passingYearList = response;
        if (this.selectedPassingYear.length == 0) {
          this.multiplePassingYear.push({ passingYears: [], passingYearId: '' });
          for (var i = 0; i < this.passingYearList.length; i++) {
            this.multiplePassingYear[this.addButtonPassingYear]['passingYears'].push({
              id: this.passingYearList[i].id,
              year: this.passingYearList[i].year
            });
          }
        } else {

          if (this.passingYearList.length == 0) {
            this.allPassingYearSelected();

          }
          else {

            this.notAllPassingYearSelected();

          }
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Function Fires when all the PreReq is selected
   */
  allPassingYearSelected() {
    //To get List of all PreReq
    this.passingYearService.findAttributesList().subscribe(
      response => {
        this.allPassingYearList = response;
        this.multiplePassingYear.push({ passingYears: [], passingYearId: '' });
        for (var i = 0; i < this.selectedPassingYear.length; i++) {

          this.multiplePassingYear[this.addButtonPassingYear]['passingYearId'] = this.selectedPassingYear[i];

          if (i + 1 != this.selectedPassingYear.length) {
            for (let j = 0; j < this.allPassingYearList.length; j++) {
              this.multiplePassingYear[this.addButtonPassingYear]['passingYears'].push({
                id: this.allPassingYearList[j].id,
                year: this.allPassingYearList[j].year
              });
            }
          }
          //////////////////////////////////////
          else {
            for (let j = 0; j < this.allPassingYearList.length; j++) {
              if (this.allPassingYearList[j].id == this.selectedPassingYear[i]) {
                this.multiplePassingYear[this.addButtonPassingYear]['passingYears'].push({
                  id: this.allPassingYearList[j].id,
                  year: this.allPassingYearList[j].year
                });
              }

            }
          }

          if (i + 1 != this.selectedPassingYear.length) {
            this.multiplePassingYear.push({ passingYears: [], passingYearId: '' });
            this.addButtonPassingYear = this.addButtonPassingYear + 1;
          }


        }
      },
      error => { console.log(error) },
      () => { }
    );
  }

  /**
   * This Function is used on Change of The Value of PreReq
   */
  passingYearChange(event) {

    let passingYearId = event.value;

    this.selectedPassingYear.push(passingYearId);
  }

  /**
   * this Function allows us to add Multiple SUbject
   */
  addmultiplePassingYear() {

    this.passingYearService.getPassingYearNotSelected(this.selectedPassingYear).subscribe(

      response => {

        this.passingYearList = response;

        if (this.passingYearList.length != 0) {
          this.addButtonPassingYear = this.addButtonPassingYear + 1;
          this.multiplePassingYear.push({ passingYears: [], passingYearId: '' });
          for (var i = 0; i < this.passingYearList.length; i++) {
            this.multiplePassingYear[this.addButtonPassingYear]['passingYears'].push({
              id: this.passingYearList[i].id,
              year: this.passingYearList[i].year
            });
          }
        }


      },
      error => console.log(error),
      () => {

      }
    );
  }

  /**
   * This Function Fires when the User Click The Add Button of the SUbject
   */
  addPassingYearClick() {


    if (this.multiplePassingYear[this.addButtonPassingYear].passingYearId == '') {
      this.layoutService.flashMsg({ msg: 'First Select Passing Year than Add Next.', msgType: 'warning' });
    }
    else {
      this.addmultiplePassingYear();
    }

  }

  /**
   * When Page loads on the Edit Mode this Function fires
   */
  notAllPassingYearSelected() {

    this.passingYearService.findAttributesList().subscribe(
      response => {
        this.allPassingYearList = response;
        this.multiplePassingYear.push({ passingYears: [], passingYearId: '' });
        for (var i = 0; i < this.selectedPassingYear.length; i++) {
          this.multiplePassingYear[this.addButtonPassingYear]['passingYearId'] = this.selectedPassingYear[i];

          for (let i = 0; i < this.allPassingYearList.length; i++) {
            this.multiplePassingYear[this.addButtonPassingYear]['passingYears'].push({
              id: this.allPassingYearList[i].id,
              year: this.allPassingYearList[i].year
            });
          }

          this.multiplePassingYear.push({ passingYears: [], passingYearId: '' });
          this.addButtonPassingYear = this.addButtonPassingYear + 1;

        }
        for (let i = 0; i < this.passingYearList.length; i++) {
          this.multiplePassingYear[this.addButtonPassingYear]['passingYears'].push({
            id: this.passingYearList[i].id,
            year: this.passingYearList[i].year
          });
        }

      },
      error => { console.log(error) },
      () => { }
    );


  }

  deletePassingYear(passingYearId) {
    this.addButtonPassingYear = this.addButtonPassingYear - 1;
    for (var i = 0; i < this.multiplePassingYear.length; i++) {
      if (this.multiplePassingYear[i].passingYearId == passingYearId) {
        this.multiplePassingYear.splice(i, 1);
      }
    }
    for (var i = 0; i < this.selectedPassingYear.length; i++) {
      if (this.selectedPassingYear[i] == passingYearId) {
        this.selectedPassingYear.splice(i, 1);
      }
    }

    this.passingYearService.getPassingYearNotSelected(this.selectedPassingYear).subscribe(
      response => {
        this.passingYearList = response;
        if (this.passingYearList.length != 0) {
          if (this.multiplePassingYear[this.multiplePassingYear.length - 1]['passingYearId'] == '') {
            this.multiplePassingYear[this.multiplePassingYear.length - 1]['passingYears'] = [];
            for (var i = 0; i < this.passingYearList.length; i++) {
              this.multiplePassingYear[this.addButtonPassingYear]['passingYears'].push({
                id: this.passingYearList[i].id,
                year: this.passingYearList[i].year
              });
            }
          } else {
            this.addButtonPassingYear = this.addButtonPassingYear + 1;
            this.multiplePassingYear.push({ passingYears: [], passingYearId: '' });
            for (var i = 0; i < this.passingYearList.length; i++) {
              this.multiplePassingYear[this.addButtonPassingYear]['passingYears'].push({
                id: this.passingYearList[i].id,
                year: this.passingYearList[i].year
              });
            }
          }
        }
      },
      error => console.log(error),
      () => { }
    );

  }

  ///////////////////////Target Program Dropdown

  getTargetPrograms() {
    this.programService.getProgramsList().subscribe(
      response => {
        this.targetProgramsList = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );

  }

  isValidDate() {
    let fromY = +this.fg.controls['ageFromYY'].value
    let fromM = +this.fg.controls['ageFromMM'].value
    let toY = +this.fg.controls['ageToYY'].value
    let toM = +this.fg.controls['ageToMM'].value
    let from = fromY + fromM;
    let to = toY + toM;

    if (toY - fromY > 0) {
      this.fg.controls['ageToYY'].setErrors(null);
      this.fg.controls['ageToMM'].setErrors(null);

    }
    else if (toY - fromY === 0) {
      if (toM - fromM > 0) {
        this.fg.controls['ageToYY'].setErrors(null);
        this.fg.controls['ageToMM'].setErrors(null);
      } else {
        this.fg.controls['ageToYY'].setErrors({ invalid: true });
        this.fg.controls['ageToMM'].setErrors({ invalid: true });
      }
    }
    else {

      this.fg.controls['ageToYY'].setErrors({ invalid: true });
      this.fg.controls['ageToMM'].setErrors({ invalid: true });
    }
  }
  prepareAcademicYears() {
    let year: number = new Date().getFullYear();
    for (let startYear = 2000; startYear <= year; startYear++) {
      this.academicYears.push(startYear);

    }
  }

}
