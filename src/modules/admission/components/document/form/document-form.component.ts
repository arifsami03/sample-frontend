import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from '../../../../shared/helper';
import { DocumentService } from '../../../services';

import { LayoutService } from '../../../../layout/services';
import { DocumentModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';
import * as _ from 'lodash';
import { ConfirmDialogComponent } from '../../../../shared/components';

@Component({
  selector: 'admission-document-form',
  templateUrl: './document-form.component.html',
  styleUrls: ['./document-form.component.css'],
  animations: [PageAnimation],
})
export class DocumentFormComponent implements OnInit {
  public pageState = 'active';

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public id: number;
  public dialogueType: string;
  public from: string;
  public docFor: string;
  public fg: FormGroup;
  public document: DocumentModel;
  public options: Observable<string[]>;
  public componentLabels = DocumentModel.attributesLabels;
  public docFors: any[] = [GLOBALS.docFors.admissionFormCollection];
  public docTypes: any[] = [GLOBALS.docTypes.original, GLOBALS.docTypes.photocopy];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private documentService: DocumentService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    public dialogRef: MatDialogRef<DocumentFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.id = this.data['id'];
    this.dialogueType = this.data['type'];
    this.from = this.data['from'];
    this.docFor = this.data['docFor'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new DocumentModel().validationRules());

    if (this.data.type != 'Add Document') {
      this.getData();

    } else {
      this.initCreatePage();
    }
  }

  /**
   * Get record
   */
  private getData() {
    this.documentService.find(this.id).subscribe(
      response => {
        if (response) {
          this.document = response;
          this.fg.patchValue(this.document);

          //TODO:low need to check why we are dowing this after fetching data
          // if (this.pageAct == this.pageActions.view) {

          //   this.initViewPage();
          // } else if (this.pageAct == this.pageActions.update) {
          //   this.initUpdatePage();
          // }
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Init create page
   */
  private initCreatePage() {

    this.document = new DocumentModel();
    if (this.data.from === 'view') {
      this.fg.get('docFor').setValue(this.docFor);
    }
    this.fg.enable();

    this.loaded = true;
  }





  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: DocumentModel) {

    if (this.data.type === 'Add Document') {
      this.documentService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Document has been created.', msgType: 'success' });
          this.dialogRef.close({ response: false });
          if (this.data.from === 'view') {
            this.router.navigate([`/admission/documents/view/${this.docFor}`]);
          } else {
            this.router.navigate([`/admission/documents`]);
          }
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      );
    } else if (this.data.type === 'Update Document') {
      this.documentService.update(this.document.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Document has been updated.', msgType: 'success' });
          this.dialogRef.close({ response: false });
          if (this.data.from === 'view') {
            this.router.navigate([`/admission/documents/view/${this.docFor}`]);
          } else {
            this.router.navigate([`/admission/documents`]);
          }
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      );
    }
  }

  /**
   * Delete record
   *
   */
  delete(id: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          this.documentService.delete(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Document has been deleted.', msgType: 'success' });
              this.router.navigate(['/admission/documents']);
            },
            error => {
              console.log(error);
              this.loaded = true;
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            }
          );
        }
      });
  }
  /**
    * cancel form and go back
    */
  onNoClick(): void {
    this.dialogRef.close({ response: false });
  }



}
