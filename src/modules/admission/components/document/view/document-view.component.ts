import { Component, ViewChild, OnInit } from '@angular/core';
import {  MatDialog,  } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { PageAnimation } from '../../../../shared/helper';
import { GLOBALS } from '../../../../app/config/globals';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { LayoutService } from '../../../../layout/services';
import { DocumentService } from '../../../services';
import { DocumentModel } from '../../../models';
import { DocumentFormComponent } from '../form/document-form.component';

@Component({
  selector: 'admission-document-view',
  templateUrl: './document-view.component.html',
  styleUrls: ['./document-view.component.css'],
  animations: [PageAnimation],
})
export class DocumentViewComponent implements OnInit {
  public pageState;

  public loaded: boolean = false;

  public data: DocumentModel[] = [];

  public attrLabels = DocumentModel.attributesLabels;

  public docFors = GLOBALS.docFors;
  public docTypes = GLOBALS.docTypes;
public params:string;


  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(private documentService: DocumentService, public dialog: MatDialog, public matDialog: MatDialog, private router: Router,private route: ActivatedRoute, public layoutService: LayoutService) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    

    this.getRecords();
  }

  /**
   * Get data/records from backend
   */
  getRecords() {
    this.route.params.subscribe(params => {
      this.params = params['docFor'];
      this.initViewPage();
      this.documentService.findByDocFor(this.params ).subscribe(
        response => {
          if (response) {
            this.data = response;
            
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
          this.pageState = 'active';
        },
        () => {
          this.loaded = true;
          this.pageState = 'active';
        }
      );

    })
    
  }



editDocument(id:number){
  const dialogRef = this.matDialog.open(DocumentFormComponent, {
    width: '800px',
    data: { id: id, type: 'Update Document', from : 'view' , docFor : this.params }
  });
  dialogRef.afterClosed().subscribe(result => {
    this.getRecords();
  });
}
  /**
   * Delete record
   */
  deleteDocument(id: number) {
    // Confirm dialog
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.documentService.delete(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Document has been deleted.', msgType: 'success' });
              this.getRecords();
              setTimeout((router: Router) => {
                this.success = false;
              }, 1000);
            },
            error => {
              this.loaded = true;
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            }
          );
        }
      });
  }

  addDocument(){
    const dialogRef = this.matDialog.open(DocumentFormComponent, {
      width: '800px',
      data: { type: 'Add Document', from : 'view', docFor : this.params }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getRecords();
    });
  }
  /**
   * initialize the view page
   */
  private initViewPage() {

    this.layoutService.setPageTitle({
      title: this.docFors[this.params].title + "\'s Documents",
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Admission', url: '/admission' },
        {
          label: 'Document ',
          url: '/admission/documents'
        },
        { label: this.docFors[this.params].title + "\'s Documents", }
      ]
    });
  }

}
