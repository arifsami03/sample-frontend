import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from '../../../../shared/helper';
import { CampusAdmissionService } from '../../../services';
import { ScholarshipService } from '../../../../fee-management/services';
import { ProgramDetailsService } from '../../../../academic/services';
import { CampusService } from '../../../../campus/services';

import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { CampusAdmissionModel } from '../../../models';
import { CampusModel } from '../../../../campus/models';
import { ScholarshipModel } from '../../../../fee-management/models';
import { ProgramDetailsModel } from '../../../../academic/models';
import { GLOBALS } from '../../../../app/config/globals';
import * as _ from 'lodash';

@Component({
  selector: 'campus-admission-form',
  templateUrl: './campus-admission-form.component.html',
  styleUrls: ['./campus-admission-form.component.css'],
  animations: [PageAnimation],
  providers: [ProgramDetailsService, ScholarshipService]
})
export class CampusAdmissionFormComponent implements OnInit {
  public pageState = 'active';

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public campuses: CampusModel[];
  public programDetails: ProgramDetailsModel[];
  public months = GLOBALS.MONTHS;
  public years = [2018, 2019, 2020, 2021, 2022, 2023];
  public campuseIds = [];
  public programDetailIds = [];
  public fg: FormGroup;
  public pageAct: string;
  public meritList: CampusAdmissionModel;
  public options: Observable<string[]>;
  public componentLabels = CampusAdmissionModel.attributesLabels;
  public _yearMask = GLOBALS.masks.year;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private campusAdmissionService: CampusAdmissionService,
    private scholarshipService: ScholarshipService,
    private programDetailsService: ProgramDetailsService,
    private campusService: CampusService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();

    this.findAllCampuses();
    this.findAllProgramDetails();
  }

  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new CampusAdmissionModel().validationRules());

    this.initCreatePage();

  }

  /**
   * Init create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Admission For Classes: Create' });

    this.meritList = new CampusAdmissionModel();

    this.fg.enable();

    this.loaded = true;
  }



  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: CampusAdmissionModel) {

    
    item['campusIds']=this.campuseIds;
    item['programDetailIds']=this.programDetailIds;
    if (this.campuseIds.length!=0 && this.programDetailIds.length!=0) {
      this.campusAdmissionService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Campus Admission has been created.', msgType: 'success' });
          this.router.navigate([`/admission/campusAdmissions`]);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      );
    }
    else{
      this.layoutService.flashMsg({ msg: 'Select Campus or Program Detail.', msgType: 'warning' });
    }
  }



  findAllProgramDetails() {
    this.programDetailsService.findAttributesList().subscribe(
      _programDetails => {
        this.programDetails = _programDetails;
      },
      error => { },
      () => { }
    );
  }
  findAllCampuses() {
    this.campusService.findAttributesList().subscribe(
      response => {
        this.campuses = response;
      },
      error => { },
      () => { }
    );
  }
  onCampusSelect(event, campId) {
    if (event.checked) {
      this.campuseIds.push(campId);
    }
    else {
      for (let i = 0; i < this.campuseIds.length; i++) {


        if (this.campuseIds[i] == campId) {
          this.campuseIds.splice(i, 1);
        }
      }

    }

  }
  onProgramSelect(event, pdId) {

    if (event.checked) {
      this.programDetailIds.push(pdId);
    }
    else {
      for (let i = 0; i < this.programDetailIds.length; i++) {


        if (this.programDetailIds[i] == pdId) {
          this.programDetailIds.splice(i, 1);
        }
      }

    }


  }


}
