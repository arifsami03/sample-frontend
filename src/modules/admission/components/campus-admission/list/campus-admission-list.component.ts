import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { PageAnimation } from '../../../../shared/helper';
import { GLOBALS } from '../../../../app/config/globals';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { LayoutService } from '../../../../layout/services';
import { CampusAdmissionService } from '../../../services';
import { CampusAdmissionModel } from '../../../models';

@Component({
  selector: 'campus-admission-list',
  templateUrl: './campus-admission-list.component.html',
  styleUrls: ['./campus-admission-list.component.css'],
  animations: [PageAnimation],
})
export class CampusAdmissionListComponent implements OnInit {
  public pageState;

  public loaded: boolean = false;

  public deleteView = false;
  public deleteDetailsData: any;
  private toDeleteId: number;

  public data: CampusAdmissionModel[] = [new CampusAdmissionModel()];

  public attrLabels = CampusAdmissionModel.attributesLabels;

  displayedColumns = ['campus', 'admissionMonth', 'admissionYear', 'noOfProgramDetails', 'options'];

  dataSource: any;

  public months = GLOBALS.MONTHS;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;



  constructor(private campusAdmissionService: CampusAdmissionService, public dialog: MatDialog, public matDialog: MatDialog, private router: Router, public layoutService: LayoutService) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Admission For Classes' });

    this.getRecords();
  }

  /**
   * Get data/records from backend
   */
  getRecords() {
    this.campusAdmissionService.index().subscribe(
      response => {
        if (response) {
          this.data = response;

          this.data.map(element => {
            element.campus = element['campus']['campusName'];
            element.admissionYear = element['admissionYear'];
            element.noOfProgramDetails = element['caprogramDetails'].length;

            this.months.forEach(month => {

              if (month['number'] == element['admissionMonth']) {
                element.admissionMonthTitle = month['name'];
              }

            });



          });



          this.dataSource = new MatTableDataSource<CampusAdmissionModel>(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete record
   */

  /**
   * Delete record
   */
  delete(id: number) {

    this.loaded = false;
    this.pageState='';

    //TODO:high: server is sending hardcoded sucess message and error
    // It should return true if delete and error responce in case of any problem
    // and our base servers errorHandler will handel that error.
    this.campusAdmissionService.delete(id).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Admission has been deleted.', msgType: 'success' });
        this.getRecords();

      },
      error => {
        this.loaded = true;
        this.pageState='active';
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
      }
    );

  }
  sortData() {
    this.dataSource.sort = this.sort;
  }

  /**
   * Find related data to display before deletion
   * 
   * @param id 
   */
  findRelatedData(id: number) {
    this.loaded = false;
    this.pageState = '';

    this.campusAdmissionService.findRelatedData(id).subscribe(response => {

      this.deleteDetailsData = response;
      this.toDeleteId = id;
      this.deleteView = true;
      this.loaded = true;
      this.pageState = 'active';
    }, error => {
      console.log('error: ', error);
      this.loaded = true;
      this.pageState = 'active';
    }, () => {

    })
  }

  public checkConfirmation(event) {

    this.deleteView = false;
    this.pageState = 'active';

    if (event) {
      this.delete(this.toDeleteId);
    }

  }
}
