import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as dateFormat from 'dateformat';
import { PageAnimation } from '../../../../shared/helper';
import { CampusAdmissionService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { CampusAdmissionModel } from '../../../models';
import { ConfirmDialogComponent } from '../../../../shared/components';
// import { AddProgramDetailFormComponent,EditCampusAdmissionComponent } from 'modules/admission/components/campus-admission/a';
import { AddProgramDetailFormComponent } from './add-pd-form/add-pd-form.component'
import { EditCampusAdmissionComponent } from './edit-campus-admission/edit-campus-admission.component'


import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'campus-admission-view',
  templateUrl: './campus-admission-view.component.html',
  styleUrls: ['./campus-admission-view.component.css'],
  animations: [PageAnimation]
})

export class CampusAdmissionViewComponent implements OnInit {

  public pageState = 'active';


  public deleteView = false;
  public deleteDetailsData: any;
  private toDeleteId: number;

  public loaded: boolean = false;
  public loadedGrid: boolean = false;
  dataSource: any;
  public expanded: number = 0;
  public months = GLOBALS.MONTHS

  public data: CampusAdmissionModel[] = [new CampusAdmissionModel()];
  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;
  displayedColumns = ['programDetail', 'options'];

  public campusAdmission: CampusAdmissionModel;

  public componentLabels = CampusAdmissionModel.attributesLabels;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private campusAdmissionService: CampusAdmissionService,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
  }

  ngOnInit() {


    this.getData();
    this.getListOfCampusAdmissionProgramDetails();

  }

  /**
   * Function to get Time Table
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.campusAdmissionService.find(params['id']).subscribe(response => {

        this.campusAdmission = response;
        // this.data = this.campusAdmission['caprogramDetails'];
        // this.dataSource = new MatTableDataSource<CampusAdmissionModel>(this.data);
        this.months.forEach(month => {

          if (month['number'] == this.campusAdmission['admissionMonth']) {
            this.campusAdmission.admissionMonthTitle = month['name'];
          }

        });

      },
        error => {
          this.loaded = true;
        },
        () => {

          this.loaded = true;

        }
      );
    });
  }



  addTimeTableDetail(id: number) {

    // const dialogRef = this.matDialog.open(TTDetailFormComponent, {
    //   width: '800px',
    //   data: { timTableId: id, action: GLOBALS.pageActions.create }
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.findAllTimeTableDetails(this.timeTable.id);
    //   }
    // });

  }





  /**
   * Delete record
   */
  delete(id: number) {
   
          this.loaded = false;
          this.pageState='';

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.campusAdmissionService.delete(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Admission has been deleted.', msgType: 'success' });
              this.router.navigate([`/admission/campusAdmissions`]);

            },
            error => {
              this.loaded = true;
              this.pageState='active';
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            }
          );
   
  }




  /**
   * Function to Get List of all the Sections
   */
  getListOfCampusAdmissionProgramDetails() {
    this.loadedGrid = false;
    this.route.params.subscribe(params => {
      this.campusAdmissionService.findCampusAdmissionProgramDetailsList(params['id']).subscribe(response => {
        this.data = response;
        this.dataSource = new MatTableDataSource<CampusAdmissionModel>(this.data);


      },
        error => {
          console.log(error);
        },
        () => {
          this.loadedGrid = true;
        }
      );

    })
  }

  deleteCAProgramDetail(id) {

    // Confirm dialog
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loadedGrid = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.campusAdmissionService.deleteCAProgramDetail(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Program Detail has been deleted.', msgType: 'success' });
              this.getListOfCampusAdmissionProgramDetails();

            },
            error => {
              this.loadedGrid = true;
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            }
          );
        }
      });

  }

  addProgramDetail() {
    const dialogRef = this.matDialog.open(AddProgramDetailFormComponent, {
      width: '500px',
      data: { campusAdmissionId: this.campusAdmission.id, admissionMonth: this.campusAdmission.admissionMonth, admissionYear: this.campusAdmission.admissionYear, campusId: this.campusAdmission.campusId }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getListOfCampusAdmissionProgramDetails();
      }
    });
  }

  editAdmission(){
    const dialogRef = this.matDialog.open(EditCampusAdmissionComponent, {
      width: '700px',
      data: { campusAdmissionId: this.campusAdmission.id, admissionMonth: this.campusAdmission.admissionMonth, admissionYear: this.campusAdmission.admissionYear, campusId: this.campusAdmission.campusId ,isActive:this.campusAdmission.isActive}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getData();
      }
    });
  }

  /**
   * Find related data to display before deletion
   * 
   * @param id 
   */
  findRelatedData(id: number) {
    this.loaded = false;
    this.pageState = '';

    this.campusAdmissionService.findRelatedData(id).subscribe(response => {

      this.deleteDetailsData = response;
      this.toDeleteId = id;
      this.deleteView = true;
      this.loaded = true;
      this.pageState = 'active';
    }, error => {
      console.log('error: ', error);
      this.loaded = true;
      this.pageState = 'active';
    }, () => {

    })
  }

  public checkConfirmation(event) {

    this.deleteView = false;
    this.pageState = 'active';

    if (event) {
      this.delete(this.toDeleteId);
    }

  }


}
