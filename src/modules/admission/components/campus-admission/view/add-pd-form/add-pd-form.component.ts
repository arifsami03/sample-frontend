import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as dateFormat from 'dateformat';
import { GLOBALS } from '../../../../../app/config/globals';
import { ProgramDetailsService } from '../../../../../academic/services';
import { CampusAdmissionService } from '../../../../services';
import { LayoutService } from '../../../../../layout/services';
import { CampusAdmissionModel } from '../../../../models';
import { ProgramDetailsModel } from '../../../../../academic/models';

@Component({
  selector: 'add-pd-form',
  templateUrl: './add-pd-form.component.html',
  styleUrls: ['./add-pd-form.component.css']
})

export class AddProgramDetailFormComponent {

  public loaded: boolean = false;
  private pageAct: string;
  public pageTitle: string;
  private timeTableId: number;
  private id: number;
  public fg: FormGroup;
  public programDetails: ProgramDetailsModel[];
  public componentLabels = CampusAdmissionModel.attributesLabels;
  private pdId: number;
  public campusAdmission: CampusAdmissionModel;

  constructor(
    private campusAdmissionService: CampusAdmissionService,
    private fb: FormBuilder,
    
    private layoutService: LayoutService,
    private programDetailsService: ProgramDetailsService,
    public dialogRef: MatDialogRef<AddProgramDetailFormComponent>,

    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.loaded = true;
  }

  ngOnInit() {

    this.timeTableId = this.data.timTableId;
    this.id = this.data.id;
    this.pageAct = this.data.action
    this.getProgramDetails();
    

  }

  


  public saveData() {

    if(this.pdId){
      let postData={
        campusAdmissionId:this.data.campusAdmissionId,
        programDetailId:this.pdId
      }
      this.campusAdmissionService.createCAProgramDetail(postData).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Program Detail has been Added to Admission.', msgType: 'success' });
          this.dialogRef.close(true);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      );
    }
    // this.loaded = false;
    // if (this.pageAct === 'create') {

    //   this.create(item);

    // } 
    // else {

    //   this.update(this.id, item);

    // }
  }


  onNoClick(): void {

    this.dialogRef.close(false);

  }

  getProgramDetails(){
    let campusId=this.data.campusId;
    let campusAdmissionId=this.data.campusAdmissionId;
    this.programDetailsService.findProgramDetailslistNotInCampusAdmission(campusId,campusAdmissionId).subscribe(
      _programDetails => {
        this.programDetails = _programDetails;
      },
      error => { },
      () => { }
    );
  }




}
