import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as dateFormat from 'dateformat';
import { GLOBALS } from '../../../../../app/config/globals';
import { ProgramDetailsService } from '../../../../../academic/services';
import { CampusAdmissionService } from '../../../../services';
import { CampusService } from '../../../../../campus/services';
import { CampusModel } from '../../../../../campus/models';
import { LayoutService } from '../../../../../layout/services';
import { CampusAdmissionModel } from '../../../../models';
import { ProgramDetailsModel } from '../../../../../academic/models';

@Component({
  selector: 'edit-campus-admission',
  templateUrl: './edit-campus-admission.component.html',
  styleUrls: ['./edit-campus-admission.component.css']
})

export class EditCampusAdmissionComponent {

  public loaded: boolean = false;
  private pageAct: string;
  public pageTitle: string;
  public months = GLOBALS.MONTHS;
  public years = [2018, 2019, 2020, 2021, 2022, 2023];
  private id: number;
  public fg: FormGroup;
  public programDetails: ProgramDetailsModel[];
  public componentLabels = CampusAdmissionModel.attributesLabels;
  private campusId: number;
  public campuses: CampusModel[];
  public campusAdmission: CampusAdmissionModel;

  constructor(
    private campusAdmissionService: CampusAdmissionService,
    private fb: FormBuilder,
    private campusService: CampusService,
    private layoutService: LayoutService,
    private programDetailsService: ProgramDetailsService,
    public dialogRef: MatDialogRef<EditCampusAdmissionComponent>,

    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.loaded = true;
  }

  ngOnInit() {

    this.fg = this.fb.group(new CampusAdmissionModel().validationRules());
    this.findAllCampuses();
    this.fg.patchValue(this.data);
    this.campusId=this.data.campusId;    

  }

  


  public saveData(item:CampusAdmissionModel) {

    item['campusId']=this.data.campusId;
      this.campusAdmissionService.update(this.data.campusAdmissionId,item).subscribe(
        response => {
          if(response){
            this.layoutService.flashMsg({ msg: 'Campus Admission has been Updated.', msgType: 'success' });
            this.dialogRef.close(true);
          }
          else{
            this.layoutService.flashMsg({ msg: 'Admission Already Scheduled against this Month & Year.', msgType: 'warning' });
          }
          
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      );
   
  }


  onNoClick(): void {

    this.dialogRef.close(false);

  }

  findAllCampuses() {
    this.campusService.findAttributesList().subscribe(
      response => {
        this.campuses = response;
      },
      error => { },
      () => { }
    );
  }




}
