import { FormControl, Validators, ValidatorFn, FormArray, AbstractControl } from '@angular/forms';


export class EligibilityCriteriaModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    programId: 'Target Program',
    targetProgramTitle: 'Target Program',
    ageFromYY: 'Age From (Years)',
    ageFromMM: 'Age From (Months)',
    ageToYY: 'Age To (Years)',
    ageToMM: 'Age To (Months)',
    requiredMarksFigure: 'Required Marks/CGPA/Grade',
    requiredMarksPercent: 'Required Marks (in percentage)',
    totalMarks: 'Total Marks/CGPA/Grade',
    academicYear: 'Academic Year',
    academicMonth: 'Academic Month',
  };

  public id?: number;
  public programId: number;
  public ageFromYY: number;
  public ageFromMM: number;
  public ageToYY: number;
  public ageToMM: number;
  public requiredMarksFigure: number;
  public requiredMarksPercent: number;
  public totalMarks: number;
  public targetProgramTitle: string;
  public createdBy?: number;
  public updatedBy?: number;
  public createdAt?: Date;
  public updatedAt?: Date;

  public academicMonth?: number;
  public academicYear?: number;


  constructor() { }

  /**
 *
 * @param equalControlName
 */
  public equalTo?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {


      let controlMatch = 0;

      return controlMatch <= control.value && 11 >= control.value
        ? null
        : {
          equalTo: true
        };
    };
  }
  public percentageCheck?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {


      let controlMatch = 0;

      return controlMatch <= control.value && 100 >= control.value
        ? null
        : {
          percentageCheck: true
        };
    };
  }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      programId: ['', [<any>Validators.required]],
      ageFromYY: ['', [<any>Validators.required, Validators.pattern('([0-9]{2})')]],
      ageFromMM: ['', [<any>Validators.required, this.equalTo('ageFromMM')]],
      ageToYY: ['', [<any>Validators.required, Validators.pattern('([0-9]{2})')]],
      ageToMM: ['', [<any>Validators.required, this.equalTo('ageToMM')]],
      requiredMarksFigure: ['', [<any>Validators.required]],
      requiredMarksPercent: ['', [<any>Validators.required, this.percentageCheck('requiredMarksPercent')]],
      totalMarks: ['', [<any>Validators.required]],

      academicMonth: new FormControl('', [<any>Validators.required]),
      academicYear: new FormControl('', [<any>Validators.required]),

    };
  }
}
