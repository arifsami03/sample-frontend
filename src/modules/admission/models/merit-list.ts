import { FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

export class MeritListModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    programDetailId: 'Program Detail',
    academicYear: 'Academic Year',
    title: 'Title',
    percentageFrom: 'Percntage From',
    percentageTo: 'Percentage To',
    scholarshipId: 'Scholarship (Select scholarship for the defined range)',
    numberOfSeats: 'Number of seats'
  };

  id?: number;
  programDetailId: number;
  academicYear: number;
  title: string;
  percentageFrom: number;
  percentageTo: number;
  scholarshipId: number;
  numberOfSeats: number;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  // for listing 
  programDetailName ?: string;
  scholarshipTitle ?: string;
  
  /**
   * Checking validation of percentage
   * @param equalControlName
   */
  public percentage?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
      [key: string]: any;
    } => {
      let controlMatch = 0;
      return controlMatch <= control.value && 100 >= control.value
        ? null
        : {
            equalTo: true
          };
    };
  }
  /**
   * Checking validation of number
   * @param equalControlName
   */
  public greaterThan?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
      [key: string]: any;
    } => {
      let controlMatch = 0;
      return controlMatch <= control.value && 9999999999 >= control.value
        ? null
        : {
            equalTo: true
          };
    };
  }

  /**
   *
   * @param equalControlName
   */
  public greaterThanFrom?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
      [key: string]: any;
    } => {
      if (!control['_parent']) return null;

      if (!control['_parent'].controls[equalControlName]) throw new TypeError('Form Control ' + equalControlName + ' does not exists.');

      let controlMatch = control['_parent'].controls[equalControlName];

      return controlMatch.value <= control.value
        ? null
        : {
            equalTo: true
          };
    };
  }
  /**
   *
   * @param equalControlName
   */
  public lessThanTo?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
      [key: string]: any;
    } => {
      if (!control['_parent']) return null;

      if (!control['_parent'].controls[equalControlName]) throw new TypeError('Form Control ' + equalControlName + ' does not exists.');

      let controlMatch = control['_parent'].controls[equalControlName];

      return controlMatch.value > control.value
        ? null
        : {
            equalTo: true
          };
    };
  }


  
  constructor() {}

  /**
   * Form Validation Rules for Level of Education
   */
  public validationRules?() {
    return {
      programDetailId: new FormControl('', [<any>Validators.required]),
      academicYear: new FormControl('', [<any>Validators.required, Validators.pattern('([0-9]{4})')]),
      title: new FormControl('', [<any>Validators.required]),
      percentageFrom: new FormControl('', [<any>Validators.required, this.percentage('percentageFrom'), Validators.pattern('([0-9]{1}([0-9])?(.[0-9]([0-9])?)?)')]),
      percentageTo: new FormControl('', [<any>Validators.required, this.percentage('percentageTo'), this.greaterThanFrom('percentageFrom'), Validators.pattern('([0-9]{1}([0-9])?(.[0-9]([0-9])?)?)')]),
      scholarshipId: new FormControl(''),
      numberOfSeats: new FormControl('', [<any>Validators.required, this.greaterThan('numberOfSeats')])
    };
  }
}
