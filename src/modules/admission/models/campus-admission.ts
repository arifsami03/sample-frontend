import { FormControl, Validators, ValidatorFn, FormArray, AbstractControl } from '@angular/forms';


export class CampusAdmissionModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    campusId: 'Campus',
    campus: 'Campus',
    admissionMonth: 'Admission Month',
    admissionMonthTitle: 'Admission Month',
    admissionYear: 'Admission Year',
    noOfProgramDetails: '# of Program Details',
    programDetail: 'Program Detail',
    isActive: 'Is Active',
  };

  public id?: number;
  public campusId: number;
  public campus: string;
  public programDetailId: number;
  public noOfProgramDetails: number;
  public admissionMonth: number;
  public admissionMonthTitle: string;
  public admissionYear: number;
  public isActive: boolean;
  public createdBy?: number;
  public updatedBy?: number;
  public createdAt?: Date;
  public updatedAt?: Date;


  constructor() { }



  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      admissionMonth: ['', [<any>Validators.required]],
      admissionYear: ['', [<any>Validators.required]],
      isActive: [''],

    };
  }
}
