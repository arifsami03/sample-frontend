import { FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

export class DocumentModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    docFor: 'Process / Form',
    title: 'Documnt Title',
    docType: 'Document Type',
  };

  id?: number;
  docFor: string;
  title: string;
  docType: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() {}

  /**
   * Form Validation Rules for Level of Education
   */
  public validationRules?() {
    return {
      docFor: new FormControl('', [<any>Validators.required]),
      title: new FormControl('', [<any>Validators.required]),
      docType: new FormControl('', [<any>Validators.required]),
    };
  }
}
