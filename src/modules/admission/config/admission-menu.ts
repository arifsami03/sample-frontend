export const AdmissionMenu = [
  {
    heading: 'Admission',
    menuItems: [
      { title: 'Eligibility Criterias', url: ['/admission/eligibilityCriteria'], perm: 'eligibilityCriterias.index' },
      { title: 'Merit Lists', url: ['/admission/meritLists'], perm: 'meritLists.index' },
      { title: 'Documents', url: ['/admission/documents'], perm: 'documents.index' },
      { title: 'Admission For Classes', url: ['/admission/campusAdmissions'], perm: 'campusAdmissions.index' },
      // { title: 'Prospectus Sale & Invoice', url: [''], perm: 'n/a' },
      // { title: 'Registration Informaiton', url: [''], perm: 'n/a' },
      // { title: 'Prospectus Sale Invoice', url: [''], perm: 'n/a' },
      // { title: 'Prospectus Publish', url: [''], perm: 'n/a' },
      // { title: 'Isuue Prosectus', url: [''], perm: 'n/a' },
      // { title: 'Prospectus Requistion Approval', url: [''], perm: 'n/a' },
      // { title: 'Addmission Form', url: [''], perm: 'n/a' },
      // { title: 'Challan Forms', url: [''], perm: 'n/a' },
      // { title: 'Interview/Entry Tests', url: [''], perm: 'n/a' },
    ]
  },
];
