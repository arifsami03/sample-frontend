import { TextMaskModule } from 'angular2-text-mask';
import {
  MatInputModule, MatFormFieldModule, MatIconModule, MatDialogModule, MatButtonModule, MatCardModule, MatTableModule,
  MatPaginatorModule, MatOptionModule, MatAutocompleteModule, MatSelectModule, MatDividerModule, MatListModule, MatExpansionModule,
  MatCheckboxModule, MatProgressBarModule, MatRadioModule, MatSortModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CampusService } from '../campus/services';
import { ProgramDetailsService } from '../academic/services';
/**
 * Import custome services/components etc.
 */
import { EligibilityCriteriaService, MeritListService,DocumentService,CampusAdmissionService } from './services';
import {
  EligibilityCriteriaListComponent,AddProgramDetailFormComponent,EditCampusAdmissionComponent,CampusAdmissionViewComponent,CampusAdmissionListComponent, EligibilityCriteriaFormComponent,CampusAdmissionFormComponent, MeritListListComponent, MeritListFormComponent, DashboardComponent, DocumentFormComponent, DocumentListComponent,DocumentViewComponent
} from './components';

export const __IMPORTS = [
  TextMaskModule, MatInputModule, MatFormFieldModule, MatIconModule, MatDialogModule, MatButtonModule, MatCardModule, FormsModule, ReactiveFormsModule,
  MatTableModule, MatPaginatorModule, MatAutocompleteModule, MatOptionModule, MatSelectModule, MatDividerModule, MatListModule,
  FlexLayoutModule, MatExpansionModule, MatCheckboxModule, MatProgressBarModule, MatRadioModule, MatSortModule];

export const __DECLARATIONS = [
  EligibilityCriteriaListComponent, EligibilityCriteriaFormComponent, MeritListListComponent, MeritListFormComponent,CampusAdmissionFormComponent,
  DashboardComponent,CampusAdmissionViewComponent,EditCampusAdmissionComponent, AddProgramDetailFormComponent,DocumentFormComponent, DocumentListComponent,DocumentViewComponent,CampusAdmissionListComponent];

export const __PROVIDERS = [EligibilityCriteriaService,CampusService, MeritListService,DocumentService,CampusAdmissionService,ProgramDetailsService];

export const __ENTRY_COMPONENTS = [AddProgramDetailFormComponent,EditCampusAdmissionComponent];
