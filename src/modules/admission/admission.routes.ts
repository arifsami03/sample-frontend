import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GLOBALS } from '../app/config/globals';

import {
  EligibilityCriteriaListComponent, EligibilityCriteriaFormComponent, MeritListListComponent, MeritListFormComponent, DashboardComponent, DocumentFormComponent, DocumentListComponent
  , DocumentViewComponent,CampusAdmissionListComponent,CampusAdmissionFormComponent,CampusAdmissionViewComponent
} from './components';
import { AuthGuardService } from '../app/services';

/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: '',
    data: { breadcrumb: { title: 'Admission', display: true } },
    children: [
      {
        path: '',
        data: { breadcrumb: { title: 'Time Table', display: false } },
        canActivate: [AuthGuardService],
        component: DashboardComponent,
      },
      {
        path: 'eligibilityCriteria',
        data: { breadcrumb: { title: 'Eligibility Criteria', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: EligibilityCriteriaListComponent,
            data: { breadcrumb: { title: 'Eligibility Criteria List', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: EligibilityCriteriaFormComponent,
            data: {
              act: GLOBALS.pageActions.create,
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: EligibilityCriteriaFormComponent,
            data: { act: GLOBALS.pageActions.view }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: EligibilityCriteriaFormComponent,
            data: { act: GLOBALS.pageActions.update }
          }
        ]
      },
      {
        path: 'meritLists',
        data: { breadcrumb: { title: 'Merit List', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: MeritListListComponent,
            data: { breadcrumb: { title: 'Merit List', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: MeritListFormComponent,
            data: {
              act: GLOBALS.pageActions.create,
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: MeritListFormComponent,
            data: { act: GLOBALS.pageActions.view }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: MeritListFormComponent,
            data: { act: GLOBALS.pageActions.update }
          }
        ]
      },
      {
        path: 'documents',
        data: { breadcrumb: { title: 'Document', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: DocumentListComponent,
            data: { breadcrumb: { title: 'Document', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: DocumentFormComponent,
            data: {
              act: GLOBALS.pageActions.create,
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:docFor',
            canActivate: [AuthGuardService],
            component: DocumentViewComponent,
            data: { act: GLOBALS.pageActions.view }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: DocumentFormComponent,
            data: { act: GLOBALS.pageActions.update }
          }
        ]
      },
      {
        path: 'campusAdmissions',
        data: { breadcrumb: { title: 'Admission For Classes', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: CampusAdmissionListComponent,
            data: { breadcrumb: { title: 'Admission For Classes', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: CampusAdmissionFormComponent,
            data: {
              act: GLOBALS.pageActions.create,
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: CampusAdmissionViewComponent,
            data: { act: GLOBALS.pageActions.view }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: DocumentFormComponent,
            data: { act: GLOBALS.pageActions.update }
          }
        ]
      },

    ]
  }
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdmissionRoutes { }
