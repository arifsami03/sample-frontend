import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from '../../shared/services';
import { CampusAdmissionModel } from '../models';

@Injectable()
export class CampusAdmissionService extends BaseService {
  private routeURL: String = 'admission/campusAdmissions';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<CampusAdmissionModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <CampusAdmissionModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<CampusAdmissionModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <CampusAdmissionModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<any> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <any>data.json();
    });
  }

  /**
   * Get Campus Admission Program Detail List
   */
  findCampusAdmissionProgramDetailsList(id: number): Observable<any> {
    return this.__get(`${this.routeURL}/findCampusAdmissionProgramDetailsList/${id}`).map(data => {
      return <any>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }
  /**
   * Delete record
   */
  deleteCAProgramDetail(id: number) {
    return this.__delete(`${this.routeURL}/deleteCAProgramDetail/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(postData) {
    return this.__post(`${this.routeURL}/create`, postData).map(data => {
      return data.json();
    });
  }
  /**
   * Create record
   */
  createCAProgramDetail(postData) {
    return this.__post(`${this.routeURL}/createCAProgramDetail`, postData).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, postData: any) {
    return this.__put(`${this.routeURL}/update/${id}`, postData).map(data => {
      return data.json();
    });
  }

  /**
   * Find related data for deletion
   * 
   * @param id 
   */
  findRelatedData(id: number) {
    return this.__get(`${this.routeURL}/findRelatedData/${id}`).map(data => {
      return data.json();
    });
  }
}
