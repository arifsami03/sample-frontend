import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from '../../shared/services'; 
import { DocumentModel } from '../models';

@Injectable()
export class DocumentService extends BaseService {
  private routeURL: String = 'admission/documents';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<DocumentModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <DocumentModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<DocumentModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <DocumentModel>data.json();
    });
  }
  /**
   * Get single record
   */
  findByDocFor(docFor : string): Observable<DocumentModel[]> {
    return this.__get(`${this.routeURL}/findByDocFor/${docFor}`).map(data => {
      return <DocumentModel[]>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: DocumentModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: DocumentModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }
}
