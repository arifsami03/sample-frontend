import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from '../../shared/services'; 
import { MeritListModel } from '../models';

@Injectable()
export class MeritListService extends BaseService {
  private routeURL: String = 'admission/meritLists';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<MeritListModel[]> {
    return this.__get(`${this.routeURL}/list`).map(data => {
      return <MeritListModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<MeritListModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <MeritListModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: MeritListModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: MeritListModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }
}
