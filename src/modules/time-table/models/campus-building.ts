import { FormControl, Validators, ValidatorFn, AbstractControl, FormArray, FormGroup } from '@angular/forms';
import { CampusModel } from '../../campus/models';

export class CampusBuildingModel {


  static attributesLabels = {
    name: 'Name',
    abbreviation: 'Abbreviation',
    code: 'Code',
    campus: 'Campus',
    campusName: 'Campus Name',
    buildingDetails: 'Building Details'
  };


  id?: number;
  campusId: number;
  name: string;
  abbreviation: string;
  code: string;
  campus?: CampusModel;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  // for list 
  campusName ?: string;

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      id: new FormControl(null),
      campusId: new FormControl(null, [Validators.required]),
      name: new FormControl('', [Validators.required]),
      abbreviation: new FormControl('', [Validators.required]),
      code: new FormControl('', [Validators.required])
    };

  }
}
