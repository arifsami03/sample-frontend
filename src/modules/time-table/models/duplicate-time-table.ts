import { FormControl, Validators, ValidatorFn, FormArray, AbstractControl } from '@angular/forms';


export class DuplicateTimeTableModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    timeTableId: 'Time Table',
    campusId: 'Campus',
    timeTableTitle: 'Title',
    campusTitle: 'Campus',
    validFrom: 'Valid From',
    validTo: 'Valid To',
  };

  public id?: number;
  public timeTableId: number;
  public campusId: number;
  public timeTableTitle: string;
  public campusTitle: string;
  public validFrom: Date;
  public validTo: Date;

  public createdBy?: number;
  public updatedBy?: number;
  public createdAt?: Date;
  public updatedAt?: Date;

  constructor() { }



  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {

      timeTableId: ['', [<any>Validators.required]],
      timeTableTitle: ['', [<any>Validators.required]],
      validFrom: ['', [<any>Validators.required]],
      validTo: ['', [<any>Validators.required]],
     
    };
  }
}
