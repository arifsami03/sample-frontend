import { FormControl, Validators, ValidatorFn, FormArray, AbstractControl } from '@angular/forms';


export class SlotModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    fromTime: 'From',
    toTime: 'To',
  };

  public id?: number;
  public fromTime: string;
  public toTime: string;

  public createdBy?: number;
  public updatedBy?: number;
  public createdAt?: Date;
  public updatedAt?: Date;

  constructor() { }



  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      fromTime: ['', [<any>Validators.required, Validators.pattern('^(([0-1]{1}[0-9]{1}|[2]{1}[0-3]{1}):[0-5]{1}[0-9]{1})$')]],
      toTime: ['', [<any>Validators.required ,Validators.pattern('^(([0-1]{1}[0-9]{1}|[2]{1}[0-3]{1}):[0-5]{1}[0-9]{1})$')]],
     
    };
  }
}
