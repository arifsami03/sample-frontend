import { FormControl, Validators, ValidatorFn, FormArray, AbstractControl } from '@angular/forms';


export class TimeTableModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    title: 'Title',
    programDetailId: 'Program Detail',
    classId: 'Class / Semester',
    programDetail:'Program Detail',
    classTitle:'Class',

  };

  public id?: number;
  public title: string;
  public programDetail: string;
  public classTitle: string;
  public programDetailId: number;
  public classId: number;

  public createdBy?: number;
  public updatedBy?: number;
  public createdAt?: Date;
  public updatedAt?: Date;

  constructor() { }



  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      title: ['', [<any>Validators.required, Validators.minLength(0), Validators.maxLength(100)]],
      programDetailId: ['', [<any>Validators.required]],
      classId: ['', [<any>Validators.required]],
     
    };
  }
}
