import { FormControl, Validators, ValidatorFn, AbstractControl, FormArray, FormGroup } from '@angular/forms';
import { CampusBuildingModel, CBFloorModel } from '.';

export class CBFRoomModel {


  static attributesLabels = {
    name: 'Name',
    abbreviation: 'Abbreviation',
    code: 'Code',
    seatingCapability: 'Seating Cap.',
    building: 'Building',
    campus: 'Campus',
    floor: 'Floor',
    room: 'Room',
    roomDetails: 'Room Details'
  };


  id?: number;
  CBFloorId: number;
  name: string;
  abbreviation: string;
  code: string;
  seatingCapability: number;
  classroomType: string;
  classroomTypes: string[];
  cbFloor?: CBFloorModel;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;


  // for listing
  roomFloorBuildingCampusName?: string;
  roomFloorBuildingName?: string;
  roomFloorName?: string;


  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      id: new FormControl(null),
      CBFloorId: new FormControl(null, [Validators.required]),
      name: new FormControl('', [Validators.required]),
      abbreviation: new FormControl('', [Validators.required]),
      code: new FormControl('', [Validators.required]),
      seatingCapability: new FormControl(null, [Validators.required, this.greaterThan('seatingCapability')]),
      classroomTypes: new FormArray([])
    };

  }

  /**
 * Validation for seatingCapability to be not in negative i.e must be greator than zero
 * @param controlName
 */
  public greaterThan?(controlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      let controlMatch = 0;
      return controlMatch <= control.value ? null : { greator: true };
    };
  }

}
