import { FormControl, Validators, ValidatorFn, FormArray, AbstractControl } from '@angular/forms';


export class TTDetailModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    slotOrder: 'Slot Order',
    courseId: 'Course / Subject',
    slotId: 'Slot',

  };

  public id?: number;
  public slotOrder: number;
  public courseId: number;
  public slotId: number;
  public timeTableId: number;
  public course: string;
  public slot: string;
  public selSections = [];
  public selDays=[];

  public createdBy?: number;
  public updatedBy?: number;
  public createdAt?: Date;
  public updatedAt?: Date;

  constructor() { }



  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      slotOrder: ['', [<any>Validators.required, Validators.min(0), Validators.max(30)]],
      courseId: ['', [<any>Validators.required]],
      slotId: ['', [<any>Validators.required]],
     
    };
  }
}
