import { FormControl, Validators, ValidatorFn, FormArray, AbstractControl } from '@angular/forms';


export class SectionModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    title: 'Title',
    abbreviation: 'Abbreviation',
    sectionId: 'ID',
  };

  public id?: number;
  public title: string;
  public abbreviation: string;
  public sectionId: string;

  public createdBy?: number;
  public updatedBy?: number;
  public createdAt?: Date;
  public updatedAt?: Date;

  constructor() { }



  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      title: ['', [<any>Validators.required, Validators.minLength(0), Validators.maxLength(100)]],
      abbreviation: ['', [<any>Validators.required, Validators.minLength(0), Validators.maxLength(10)]],
      sectionId: ['', [<any>Validators.required, Validators.minLength(0), Validators.maxLength(20)]],
     
    };
  }
}
