import { FormControl, Validators, ValidatorFn, AbstractControl, FormArray, FormGroup } from '@angular/forms';
import { CampusBuildingModel } from '.';

export class CBFloorModel {


  static attributesLabels = {
    name: 'Name',
    abbreviation: 'Abbreviation',
    code: 'Code',
    building: 'Building',
    buildingName: 'Building Name',
    campusName: 'Campus Name',
    floorDetails: 'Floor Details'
  };


  id?: number;
  CBId: number;
  name: string;
  abbreviation: string;
  code: string;
  building?: CampusBuildingModel;
  campusBuilding ?: CampusBuildingModel;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;



  // for listing
  buildingCampusName ?: string;
  campusBuildingName ?: string;

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      id: new FormControl(null),
      CBId: new FormControl(null, [Validators.required]),
      name: new FormControl('', [Validators.required]),
      abbreviation: new FormControl('', [Validators.required]),
      code: new FormControl('', [Validators.required])
    };

  }
}
