import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GLOBALS } from '../app/config/globals';

import {
  SlotFormComponent, SlotListComponent, SectionListComponent, SectionFormComponent, CampusBuildingFormComponent, CampusBuildingListComponent,
  CBFloorListComponent, CBFloorFormComponent, CBFRoomFormComponent, CBFRoomListComponent, TimeTableListComponent, TimeTableFormComponent,
  TimeTableViewComponent, MappingTimeTableListComponent, MappingTimeTableFormComponent, DuplicateTimeTableFormComponent, ViewTimeTableListComponent,
  ViewTTViewComponent, DashboardComponent
} from './components';
import { AuthGuardService } from '../app/services';

/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: '',
    data: { breadcrumb: { title: 'Time Table', display: true } },
    children: [
      {
        path: '',
        data: { breadcrumb: { title: 'Time Table', display: false } },
        canActivate: [AuthGuardService],
        component: DashboardComponent,
      },

      {
        path: 'slot',
        data: { breadcrumb: { title: 'Slots', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: SlotListComponent,
            data: { breadcrumb: { title: 'Slot List', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: SlotFormComponent,
            data: {
              act: GLOBALS.pageActions.create,
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: SlotFormComponent,
            data: { act: GLOBALS.pageActions.view }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: SlotFormComponent,
            data: { act: GLOBALS.pageActions.update }
          }
        ]
      },
      {
        path: 'section',
        data: { breadcrumb: { title: 'Section', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: SectionListComponent,
            data: { breadcrumb: { title: 'Section List', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: SectionFormComponent,
            data: {
              act: GLOBALS.pageActions.create,
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: SectionFormComponent,
            data: { act: GLOBALS.pageActions.view }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: SectionFormComponent,
            data: { act: GLOBALS.pageActions.update }
          }
        ]
      },
      // {
      //   path: 'campusBuildings',
      //   data: { breadcrumb: { title: 'Campus Buildings', display: true } },
      //   children: [
      //     {
      //       path: '',
      // canActivate: [AuthGuardService],//       
      // component: CampusBuildingListComponent,
      //       data: { breadcrumb: { title: 'Campus Buildings', display: false } }
      //     },
      //     {
      //       path: 'create',
      // canActivate: [AuthGuardService],//       
      // component: CampusBuildingFormComponent,
      //       data: {
      //         act: GLOBALS.pageActions.create,
      //         breadcrumb: { title: 'Create', display: true }
      //       }
      //     },
      //     {
      //       path: 'view/:id',
      // canActivate: [AuthGuardService],//       
      // component: CampusBuildingFormComponent,
      //       data: { act: GLOBALS.pageActions.view }
      //     },
      //     {
      //       path: 'update/:id',
      // canActivate: [AuthGuardService],//       
      // component: CampusBuildingFormComponent,
      //       data: { act: GLOBALS.pageActions.update }
      //     }
      //   ]
      // },
      // {
      //   path: 'cbFloors',
      //   data: { breadcrumb: { title: 'Building Floors', display: true } },
      //   children: [
      //     {
      //       path: '',
      // canActivate: [AuthGuardService],//       
      // component: CBFloorListComponent,
      //       data: { breadcrumb: { title: 'Building Floors', display: false } }
      //     },
      //     {
      //       path: 'create',
      // canActivate: [AuthGuardService],//       
      // component: CBFloorFormComponent,
      //       data: {
      //         act: GLOBALS.pageActions.create,
      //         breadcrumb: { title: 'Create', display: true }
      //       }
      //     },
      //     {
      //       path: 'view/:id',
      // canActivate: [AuthGuardService],//       
      // component: CBFloorFormComponent,
      //       data: { act: GLOBALS.pageActions.view }
      //     },
      //     {
      //       path: 'update/:id',
      // canActivate: [AuthGuardService],//       
      // component: CBFloorFormComponent,
      //       data: { act: GLOBALS.pageActions.update }
      //     }
      //   ]
      // },
      // {
      //   path: 'cbfRooms',
      //   data: { breadcrumb: { title: 'Floor Rooms', display: true } },
      //   children: [
      //     {
      //       path: '',
      // canActivate: [AuthGuardService],//       
      // component: CBFRoomListComponent,
      //       data: { breadcrumb: { title: 'Floor Rooms', display: false } }
      //     },
      //     {
      //       path: 'create',
      // canActivate: [AuthGuardService],//       
      // component: CBFRoomFormComponent,
      //       data: {
      //         act: GLOBALS.pageActions.create,
      //         breadcrumb: { title: 'Create', display: true }
      //       }
      //     },
      //     {
      //       path: 'view/:id',
      // canActivate: [AuthGuardService],//       
      // component: CBFRoomFormComponent,
      //       data: { act: GLOBALS.pageActions.view }
      //     },
      //     {
      //       path: 'update/:id',
      // canActivate: [AuthGuardService],//       
      // component: CBFRoomFormComponent,
      //       data: { act: GLOBALS.pageActions.update }
      //     }
      //   ]
      // },
      {
        path: 'timeTable',
        data: { breadcrumb: { title: 'Time Table', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: TimeTableListComponent,
            data: { breadcrumb: { title: 'Section List', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: TimeTableFormComponent,
            data: {
              act: GLOBALS.pageActions.create,
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: TimeTableViewComponent,
            data: { act: GLOBALS.pageActions.view }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: TimeTableFormComponent,
            data: { act: GLOBALS.pageActions.update }
          }
        ]
      },
      {
        path: 'mappingTimeTable',
        data: { breadcrumb: { title: 'Mapping Time Table', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: MappingTimeTableListComponent,
            data: { breadcrumb: { title: 'Mapping Time Table List', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: MappingTimeTableFormComponent,
            data: {
              act: GLOBALS.pageActions.create,
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: MappingTimeTableFormComponent,
            data: { act: GLOBALS.pageActions.view }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: MappingTimeTableFormComponent,
            data: { act: GLOBALS.pageActions.update }
          }
        ]
      },
      {
        path: 'viewTimeTable',
        data: { breadcrumb: { title: 'View Time Table', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: ViewTimeTableListComponent,
            data: { breadcrumb: { title: 'View Time Table List', display: false } }
          },

          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: ViewTTViewComponent,
            data: { act: GLOBALS.pageActions.view }
          },

        ]
      },
      {
        path: 'duplicateTimeTable',
        data: { breadcrumb: { title: 'Duplicate Time Table', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: DuplicateTimeTableFormComponent,
            data: { breadcrumb: { title: 'Duplicate Time Table', display: false } }
          },

        ]
      },
    ],
  },
  { path: '**', redirectTo: '' }
];;

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimeTableRoutes { }
