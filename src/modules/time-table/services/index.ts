export * from './campus-building.service';
export * from './cb-floor.service';
export * from './cbf-room.service';
export * from './slot.service';
export * from './section.service';
export * from './time-table.service';
export * from './tt-detail.service';
export * from './mapping-time-table.service';
