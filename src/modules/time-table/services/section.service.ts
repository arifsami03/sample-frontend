import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from '../../shared/services'; 
import { SectionModel } from '../models';

@Injectable()
export class SectionService extends BaseService {
  
  private routeURL: String = 'timeTable/sections';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<SectionModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <SectionModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<SectionModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <SectionModel[]>data.json();
    });
  }


  /**
   * Get single record
   */
  find(id: number): Observable<SectionModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <SectionModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(postData: SectionModel) {
    return this.__post(`${this.routeURL}/create`, postData).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, postData: SectionModel) {
    return this.__put(`${this.routeURL}/update/${id}`, postData).map(data => {
      return data.json();
    });
  }
}
