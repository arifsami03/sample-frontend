import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from '../../shared/services'; 
import { MappingTimeTableModel } from '../models';

@Injectable()
export class MappingTimeTableService extends BaseService {
  
  private routeURL: String = 'timeTable/mappingTimeTables';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<MappingTimeTableModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <MappingTimeTableModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<MappingTimeTableModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <MappingTimeTableModel[]>data.json();
    });
  }


  /**
   * Get single record
   */
  find(id: number): Observable<MappingTimeTableModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <MappingTimeTableModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(postData) {
    return this.__post(`${this.routeURL}/create`, postData).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, postData) {
    return this.__put(`${this.routeURL}/update/${id}`, postData).map(data => {
      return data.json();
    });
  }
}
