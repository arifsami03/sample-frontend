import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';

import { CBFRoomModel } from '../models';
import { BaseService } from '../../shared/services'; 

@Injectable()
export class CBFRoomService extends BaseService {
    private routeURL: String = 'campus/cbfRooms';

    constructor(protected http: Http) {
        super(http)
    }

    /**
     * Get All records
     */
    index(): Observable<CBFRoomModel[]> {
        return this.__get(`${this.routeURL}/index`).map(data => {
            return <CBFRoomModel[]>data.json();
        });
    }

    /**
     * Get single record
     */
    find(id: number): Observable<CBFRoomModel> {
        return this.__get(`${this.routeURL}/find/${id}`).map(data => {
            return <CBFRoomModel>data.json();
        });
    }

    /**
     * Delete Record
     */
    delete(id: number) {
        return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
            return data.json();
        });
    }

    /**
     * Create Record
     */
    create(item: CBFRoomModel) {
        return this.__post(`${this.routeURL}/create`, item).map(data => {
            return <CBFRoomModel>data.json();
        });
    }

    /**
     * Update Record
     */
    update(id: number, item: CBFRoomModel) {
        return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
            return data.json();
        });
    }
}
