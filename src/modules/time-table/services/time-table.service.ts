import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from '../../shared/services';
import { TimeTableModel } from '../models';

@Injectable()
export class TimeTableService extends BaseService {

  private routeURL: String = 'timeTable/timeTables';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<TimeTableModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <TimeTableModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<TimeTableModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <TimeTableModel[]>data.json();
    });
  }

  getListOfSlotsInTimeTable(id): Observable<any[]> {
    return this.__get(`${this.routeURL}/getListOfSlotsInTimeTable/${id}`).map(data => {
      return <any[]>data.json();
    });
  }


  /**
   * Get single record
   */
  find(id: number): Observable<TimeTableModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <TimeTableModel>data.json();
    });
  }

  /**
   * Get All Time table Details
   */
  viewTTDetails(id: number): Observable<any> {
    return this.__get(`${this.routeURL}/viewTTDetails/${id}`).map(data => {
      return <any>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(postData: TimeTableModel) {
    return this.__post(`${this.routeURL}/create`, postData).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, postData: TimeTableModel) {
    return this.__put(`${this.routeURL}/update/${id}`, postData).map(data => {
      return data.json();
    });
  }
  /**
   * Save Duplicate Time Table
   */
  saveDuplicateTimeTable(id: number, postData) {
    return this.__put(`${this.routeURL}/saveDuplicateTimeTable/${id}`, postData).map(data => {
      return data.json();
    });
  }

}
