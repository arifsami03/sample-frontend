import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';

import { CampusBuildingModel } from '../models';
import { BaseService } from '../../shared/services'; 

@Injectable()
export class CampusBuildingService extends BaseService {
    private routeURL: String = 'campus/campusBuildings';

    constructor(protected http: Http) {
        super(http)
    }

    /**
     * Get All records
     */
    index(): Observable<CampusBuildingModel[]> {
        return this.__get(`${this.routeURL}/index`).map(data => {
            return <CampusBuildingModel[]>data.json();
        });
    }

    /**
     * Get All building with their campus name
     */
    findAttributesListWithCampusName(): Observable<CampusBuildingModel[]> {
        return this.__get(`${this.routeURL}/findAttributesListWithCampusName`).map(data => {
            return <CampusBuildingModel[]>data.json();
        });
    }

    /**
     * Get single record
     */
    find(id: number): Observable<CampusBuildingModel> {
        return this.__get(`${this.routeURL}/find/${id}`).map(data => {
            return <CampusBuildingModel>data.json();
        });
    }

    /**
     * Delete Record
     */
    delete(id: number) {
        return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
            return data.json();
        });
    }

    /**
     * Create Record
     */
    create(item: CampusBuildingModel) {
        return this.__post(`${this.routeURL}/create`, item).map(data => {
            return <CampusBuildingModel>data.json();
        });
    }

    /**
     * Update Record
     */
    update(id: number, item: CampusBuildingModel) {
        return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
            return data.json();
        });
    }
}
