import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';

import { CBFloorModel } from '../models';
import { BaseService } from '../../shared/services'; 

@Injectable()
export class CBFloorService extends BaseService {
    private routeURL: String = 'campus/cbFloors';

    constructor(protected http: Http) {
        super(http)
    }

    /**
     * Get All records
     */
    index(): Observable<CBFloorModel[]> {
        return this.__get(`${this.routeURL}/index`).map(data => {
            return <CBFloorModel[]>data.json();
        });
    }

    /**
     * Get single record
     */
    find(id: number): Observable<CBFloorModel> {
        return this.__get(`${this.routeURL}/find/${id}`).map(data => {
            return <CBFloorModel>data.json();
        });
    }

    /**
     * Delete Record
     */
    delete(id: number) {
        return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
            return data.json();
        });
    }

    /**
     * Create Record
     */
    create(item: CBFloorModel) {
        return this.__post(`${this.routeURL}/create`, item).map(data => {
            return <CBFloorModel>data.json();
        });
    }

    /**
     * Update Record
     */
    update(id: number, item: CBFloorModel) {
        return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
            return data.json();
        });
    }


    /**
     * Get All Floors with their building and campus name
     */
    findAttributesListWithCampusAndBuilding(): Observable<CBFloorModel[]> {
        return this.__get(`${this.routeURL}/findAttributesListWithCampusAndBuilding`).map(data => {
            return <CBFloorModel[]>data.json();
        });
    }
}
