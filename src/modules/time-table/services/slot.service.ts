import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from '../../shared/services'; 
import { SlotModel } from '../models';

@Injectable()
export class SlotService extends BaseService {
  
  private routeURL: String = 'timeTable/slots';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<SlotModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <SlotModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<SlotModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <SlotModel[]>data.json();
    });
  }


  /**
   * Get single record
   */
  find(id: number): Observable<SlotModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <SlotModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(postData: SlotModel) {
    return this.__post(`${this.routeURL}/create`, postData).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, postData: SlotModel) {
    return this.__put(`${this.routeURL}/update/${id}`, postData).map(data => {
      return data.json();
    });
  }
}
