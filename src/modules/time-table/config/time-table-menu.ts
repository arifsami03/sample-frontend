export const TimeTableMenu = [
  {
    heading: 'Time Table',
    menuItems: [
      // { title: 'Buildings', url: ['/time-table/campusBuildings'], perm: 'campusBuildings.index' },
      // { title: 'Floors', url: ['/time-table/cbFloors'], perm: 'cbFloors.index' },
      // { title: 'Rooms', url: ['/time-table/cbfRooms'], perm: 'cbfRooms.index' },
      { title: 'Time Tables', url: ['/time-table/timeTable'], perm: 'timeTables.index' },
      
      { title: 'Mapping Time Tables', url: ['/time-table/mappingTimeTable'], perm: 'mappingTimeTables.index' },
      { title: 'Duplicate Time Table', url: ['/time-table/duplicateTimeTable'], perm: 'timeTables.create' },
      { title: 'View Time Tables', url: ['/time-table/viewTimeTable'], perm: 'timeTables.index' },
      // { title: 'Location / Teacher Info', url: [''], perm: 'n/a' },
      // { title: 'Time Table Detail View', url: [''], perm: 'n/a' },
    ]
  },
  {
    heading: 'Configuration',
    menuItems: [
      { title: 'Slots', url: ['/time-table/slot'], perm: 'slots.index' },
      { title: 'Sections', url: ['/time-table/section'], perm: 'sections.index' },
      
    ]
  },
];
