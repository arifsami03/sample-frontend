import {
  MatInputModule, MatFormFieldModule, MatIconModule, MatDialogModule, MatButtonModule, MatCardModule, MatTableModule,
  MatPaginatorModule, MatOptionModule, MatAutocompleteModule, MatSelectModule, MatDividerModule, MatListModule, MatExpansionModule,
  MatCheckboxModule, MatProgressBarModule, MatRadioModule, MatChipsModule, MatDatepickerModule,MatSortModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TextMaskModule } from 'angular2-text-mask';

/**
 * Import custome services/components etc.
 */
import {
  SlotService, SectionService, CampusBuildingService, CBFloorService, CBFRoomService, TimeTableService, TTDetailService, MappingTimeTableService,
} from './services';

import{
  CourseService
} from '../academic/services'

/**
 * Import Components
 */
import {
  SlotListComponent, SlotFormComponent, SectionFormComponent, SectionListComponent, CampusBuildingFormComponent, CampusBuildingListComponent,
  CBFloorFormComponent, CBFloorListComponent, CBFRoomFormComponent, CBFRoomListComponent, TimeTableListComponent, TimeTableFormComponent,
  TimeTableViewComponent, TTDetailFormComponent, MappingTimeTableListComponent, MappingTimeTableFormComponent, DuplicateTimeTableFormComponent,
  ViewTimeTableListComponent, ViewTTViewComponent, DashboardComponent
} from './components';

export const __IMPORTS = [
  MatInputModule, MatFormFieldModule, MatIconModule, MatDialogModule, MatButtonModule, MatCardModule, FormsModule, ReactiveFormsModule,
  MatTableModule, MatPaginatorModule, MatAutocompleteModule, MatOptionModule, MatSelectModule, MatDividerModule, MatListModule,
  FlexLayoutModule, MatExpansionModule, MatCheckboxModule, MatProgressBarModule, MatRadioModule, MatChipsModule, MatDatepickerModule,
  TextMaskModule,MatSortModule
];

export const __DECLARATIONS = [

  SlotListComponent, SlotFormComponent, SectionFormComponent, SectionListComponent, CampusBuildingFormComponent, CampusBuildingListComponent,
  CBFloorFormComponent, CBFloorListComponent, CBFRoomFormComponent, CBFRoomListComponent, TimeTableListComponent, TimeTableFormComponent,
  TimeTableViewComponent, TTDetailFormComponent, MappingTimeTableListComponent, MappingTimeTableFormComponent, DuplicateTimeTableFormComponent,
  ViewTimeTableListComponent, ViewTTViewComponent, DashboardComponent
];

export const __PROVIDERS = [
  SlotService, SectionService, CampusBuildingService, CBFloorService, CBFRoomService, TimeTableService, TTDetailService, MappingTimeTableService,
  CourseService
];

export const __ENTRY_COMPONENTS = [TTDetailFormComponent];
