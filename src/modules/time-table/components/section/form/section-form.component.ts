import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from '../../../../shared/helper';
import { GLOBALS } from '../../../../app/config/globals';
import { SectionService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { SectionModel } from '../../../models';

@Component({
  selector: 'section-form',
  templateUrl: './section-form.component.html',
  styleUrls: ['./section-form.component.css'],
  animations: [PageAnimation]
})
export class SectionFormComponent implements OnInit {
  public pageState = 'active';

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public boxLoaded: boolean = true;

  public mobileMask = GLOBALS.masks.mobile;

  public fg: FormGroup;
  public pageAct: string;
  public section: SectionModel;
  public options: Observable<string[]>;
  public componentLabels = SectionModel.attributesLabels;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private sectionService: SectionService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();

  }


  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new SectionModel().validationRules());

    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get record
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.sectionService.find(params['id']).subscribe(
        response => {
          this.section = response;
          if (this.section) {
            this.fg.patchValue(this.section);
            //TODO:low need to check why we are dowing this after fetching data
            if (this.pageAct == this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct == this.pageActions.update) {

              this.initUpdatePage();
            }
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {
    this.loaded = true;
    this.layoutService.setPageTitle({ title: 'Section: Create' });

    this.section = new SectionModel();

    this.fg.enable();
  }

  /**
   * initialize the view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Section: ' + this.section.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Section ',
          url: '/time-table/section'
        },
        { label: this.section.title }
      ]
    });
  }

  /**
   * initialize the update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Section: ' + this.section.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Section',
          url: '/time-table/section'
        },
        {
          label: this.section.title,
          url: `/time-table/section/view/${this.section.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: SectionModel) {
    this.boxLoaded = false;

    item.abbreviation = item.abbreviation.toUpperCase();

    if (this.pageAct === this.pageActions.create) {
      this.sectionService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Section has been created.', msgType: 'success' });
          this.router.navigate([`/time-table/section`]);
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        },
        () => {
          this.boxLoaded = true;
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.sectionService.update(this.section.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Section has been updated.', msgType: 'success' });
          this.router.navigate([`/time-table/section`]);
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        },
        () => {
          this.boxLoaded = true;
        }
      );
    }
  }


  /**
   * Delete record
   *
   */
  delete(id: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          this.sectionService.delete(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Section has been deleted.', msgType: 'success' });
              this.router.navigate(['/time-table/section']);
            },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
            }
          );
        }
      });
  }



}
