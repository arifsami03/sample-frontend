import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import * as dateFormat from 'dateformat';

import { PageAnimation } from '../../../../shared/helper';
import { GLOBALS } from '../../../../app/config/globals';
import { SlotService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { SlotModel } from '../../../models';
@Component({
  selector: 'slot-form',
  templateUrl: './slot-form.component.html',
  styleUrls: ['./slot-form.component.css'],
  animations: [PageAnimation]
})
export class SlotFormComponent implements OnInit {
  public pageState = 'active';

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public boxLoaded: boolean = true;

  public timeMask = GLOBALS.masks.time;

  public fg: FormGroup;
  public pageAct: string;
  public slot: SlotModel;
  public options: Observable<string[]>;
  public componentLabels = SlotModel.attributesLabels;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private slotService: SlotService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();

  }


  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new SlotModel().validationRules());

    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get record
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.slotService.find(params['id']).subscribe(
        response => {
          this.slot = response;
          if (this.slot) {

            this.slot.toTime = dateFormat(this.slot.toTime, "UTC:HH:MM");
            this.slot.fromTime = dateFormat(this.slot.fromTime, "UTC:HH:MM");

            this.fg.patchValue(this.slot);
            //TODO:low need to check why we are dowing this after fetching data
            if (this.pageAct == this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct == this.pageActions.update) {

              this.initUpdatePage();
            }
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {
    this.loaded = true;
    this.layoutService.setPageTitle({ title: 'Slot: Create' });

    this.slot = new SlotModel();

    this.fg.enable();
  }

  /**
   * initialize the view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Slot: ' + this.slot.fromTime + ' - ' + this.slot.toTime,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Slot ',
          url: '/time-table/slot'
        },
        { label: this.slot.fromTime + " - " + this.slot.toTime }
      ]
    });
  }

  /**
   * initialize the update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Slot: ' + this.slot.fromTime + ' - ' + this.slot.toTime,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Slot',
          url: '/time-table/slot'
        },
        {
          label: this.slot.fromTime,
          url: `/time-table/slot/view/${this.slot.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: SlotModel) {
    // validation of time range
    if (this.isEndTimeGreater()) {
      this.boxLoaded = false;
      if (this.pageAct === this.pageActions.create) {
        this.slotService.create(item).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Slot has been created.', msgType: 'success' });
            this.router.navigate([`/time-table/slot`]);
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
            this.boxLoaded = true;
          },
          () => {
            this.boxLoaded = true;
          }
        );
      } else if (this.pageAct === this.pageActions.update) {
        this.slotService.update(this.slot.id, item).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Slot has been updated.', msgType: 'success' });
            this.router.navigate([`/time-table/slot`]);
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
            this.boxLoaded = true;
          },
          () => {
            this.boxLoaded = true;
          }
        );
      }
    }

  }


  /**
   * Delete record
   */
  delete(id: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          this.slotService.delete(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Slot has been deleted.', msgType: 'success' });
              this.router.navigate(['/time-table/slot']);
            },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
            }
          );
        }
      });
  }

  isEndTimeGreater() {

    let fromTime = this.fg.controls['fromTime'].value;
    let toTime = this.fg.controls['toTime'].value;

    if (toTime < fromTime) {

      // setting error on field
      this.fg.controls['toTime'].setErrors({ invalid: true });
      return false;

    } else {

      return true;

    }
  }

}
