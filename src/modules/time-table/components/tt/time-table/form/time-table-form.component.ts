import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatOptionSelectionChange } from '@angular/material';
import { PageAnimation } from '../../../../../shared/helper';
import { TimeTableService } from '../../../../services';
import { LayoutService } from '../../../../../layout/services';
import { TimeTableModel } from '../../../../models';
import { ProgramDetailsModel, ClassesModel } from '../../../../../academic/models';
import { ProgramDetailsService } from '../../../../../academic/services';

@Component({
  selector: 'time-table-form',
  templateUrl: './time-table-form.component.html',
  styleUrls: ['./time-table-form.component.css'],
  animations: [PageAnimation],
  providers: [ProgramDetailsService]
})

export class TimeTableFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;

  public timeTable: TimeTableModel;
  public programDetails : ProgramDetailsModel[];
  public classDetails = [new ClassesModel()];


  public componentLabels = TimeTableModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private timeTableService: TimeTableService,
    private programDetailService: ProgramDetailsService,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  ngOnInit() {

    this.initializePage();

    this.getProgramDetailList();


  }

  private initializePage() {

    this.fg = this.fb.group(new TimeTableModel().validationRules());

    if (this.pageAct === 'create') {

      this.initAddPage();

    }
    else {

      this.getData();

    }
  }

  /**
   * Get Data of Time Table by Id
   */
  private getData() {

    this.route.params.subscribe(params => {
      this.timeTableService.find(params['id']).subscribe(response => {

        this.timeTable = response;
        this.getClassesData(this.timeTable.programDetailId);

      },
        error => {
          this.loaded = true;
        },
        () => {

          this.fg.patchValue(this.timeTable);

          if (this.pageAct === 'view') {

            this.initViewPage();

          }
          else if (this.pageAct === 'update') {

            this.initUpdatePage();

          }

          this.loaded = true;
        }
      );
    });
  }

  private initAddPage() {

    this.layoutService.setPageTitle({ title: 'Add Time Table' });

    this.timeTable = new TimeTableModel();
    this.fg.enable();

    this.loaded = true;
  }

  private initViewPage() {

    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Time Table: ' + this.timeTable.title,
      breadCrumbs: [{ label: 'Home', url: '/home/dashboard' }, { label: 'Time Table', url: '/time-table/timeTable' }, { label: this.timeTable.title }]
    });

  }

  private initUpdatePage() {

    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Time Table: ' + this.timeTable.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Time Table', url: '/time-table/timeTable' },
        {
          label: this.timeTable.title,
          url: `/time-table/timeTable/view/${this.timeTable.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  public saveData(item: TimeTableModel) {

    this.loaded = false;
    if (this.pageAct === 'create') {

      this.create(item);

    }
    else {

      this.update(this.timeTable.id, item);

    }
  }

  private create(item: TimeTableModel) {

    // get id of loggedIn userId
    item.createdBy = +this.getLoggedInUserInfo('id');

    this.timeTableService.create(item).subscribe(response => {

      this.timeTable = response;
      this.layoutService.flashMsg({ msg: 'Time Table has been created.', msgType: 'success' });

    },
      error => {

        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });

      },
      () => {

        this.loaded = true;
        this.router.navigate([`/time-table/timeTable`]);

      }
    );
  }

  private update(id: number, item: TimeTableModel) {

    this.loaded = false;
    // get id of loggedIn userId
    item.updatedBy = +this.getLoggedInUserInfo('id');

    this.timeTableService.update(id, item).subscribe(response => {

      this.layoutService.flashMsg({ msg: 'Time Table has been updated.', msgType: 'success' });

    },
      error => {

        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });

      },
      () => {

        this.loaded = true;
        this.router.navigate([`/time-table/timeTable/view/${this.timeTable.id}`]);

      }
    );
  }

  private getLoggedInUserInfo(parameter: string): string {

    return localStorage.getItem(parameter);

  }

  /**
   * Get List of all the Program Details
   */
  public getProgramDetailList() {

    this.programDetailService.findAttributesList().subscribe(response => {

      this.programDetails = response;

    },
      error => {
      }
    );

  }

  /**
   * Function to Fire when Program detail Changes
   * @param event Change event of Select Box
   */
  programDetailChange(event) {

    if (event.source.selected) {

      this.getClassesData(event.value)

    }
  }

  /**
   * Function to Classes based on Program Detail Id
   * @param id Program Detail id
   */
  getClassesData(id) {

    this.programDetailService.getClassesByProgramDetail(id).subscribe(response => {

      this.classDetails = response;

    },

      error => {

        this.loaded = true;

      },
      () => {

        this.loaded = true;
        
      }
    );
  }
}
