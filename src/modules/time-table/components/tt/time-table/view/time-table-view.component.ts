import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as dateFormat from 'dateformat';
import { PageAnimation } from '../../../../../shared/helper';
import { TimeTableService, TTDetailService } from '../../../../services';
import { SlotService, SectionService } from '../../../../services';
import { CourseService } from '../../../../../academic/services';
import { LayoutService } from '../../../../../layout/services';
import { TimeTableModel, TTDetailModel } from '../../../../models';
import { SlotModel, SectionModel } from '../../../../models';
import { CourseModel } from '../../../../../academic/models';
import { TTDetailFormComponent } from './detail-dialog-form/tt-detail-form.component';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';

import { GLOBALS } from '../../../../../app/config/globals';

@Component({
  selector: 'time-table-view',
  templateUrl: './time-table-view.component.html',
  styleUrls: ['./time-table-view.component.css'],
  animations: [PageAnimation]
})

export class TimeTableViewComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;

  public expanded: number = 0;


  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;

  public slotModel: SlotModel[];

  public timeTable: TimeTableModel;
  public ttDetails: TTDetailModel[];

  public courseModel = [new CourseModel()];
  public selectedSections = [];

  public sectionModel = [new SectionModel()];
  public sections = new FormControl();

  public selectedDays = [];
  public days = new FormControl();
  public weekDays = GLOBALS.weekDays;

  public componentLabels = TimeTableModel.attributesLabels;
  public componentLabelsForDetails = TTDetailModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private timeTableService: TimeTableService,
    private ttDetailService: TTDetailService,
    private slotService: SlotService,
    private courseService: CourseService,
    private sectionService: SectionService,
    private layoutService: LayoutService,
    public matDialog: MatDialog
  ) {
  }

  ngOnInit() {

    this.fg = this.fb.group(new TTDetailModel().validationRules());
    this.fg.enable();
    this.getListOfSlots();
    this.getListOfCourses();
    this.getListOfSections();
    this.getData();

  }

  /**
   * Function to get Time Table
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.timeTableService.find(params['id']).subscribe(response => {

        this.timeTable = response;
      },
        error => {
          this.loaded = true;
        },
        () => {

          this.initViewPage();
          //Call Function to find all the Time table Details of The Time Table
          this.findAllTimeTableDetails(this.timeTable.id);
          this.loaded = true;

        }
      );
    });
  }

  private initViewPage() {
    //this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Time Table: ' + this.timeTable.title,
      breadCrumbs: [{ label: 'Home', url: '/home/dashboard' }, { label: 'Time Table', url: '/time-table/timeTable' }, { label: this.timeTable.title }]
    });

  }

  findAllTimeTableDetails(id: number) {

    this.ttDetailService.findAllTTDetails(id).subscribe(response => {

      this.ttDetails = response;

      for (let i = 0; i < response.length; i++) {

        this.ttDetails[i].selSections = [];

        this.ttDetails[i].selDays = [];

        for (let j = 0; j < this.sectionModel.length; j++) {

          for (let k = 0; k < response[i]['sections'].length; k++) {

            if (this.sectionModel[j].id == response[i]['sections'][k]) {
              this.ttDetails[i].selSections.push(this.sectionModel[j].title);
            }


          }

        }
        for (let j = 0; j < this.weekDays.length; j++) {

          for (let k = 0; k < response[i]['days'].length; k++) {

            if (this.weekDays[j].dayId == response[i]['days'][k]) {
              this.ttDetails[i].selDays.push(this.weekDays[j].name);
            }


          }

        }


      }
      this.ttDetails.forEach((item) => {
        item.slot = dateFormat(item.slot['fromTime'], "UTC:hh:MM TT") + ' - ' + dateFormat(item.slot['toTime'], "UTC:hh:MM TT");
      })
    },
      error => {
        console.log(error);
      },
      () => {
      }
    );
  }


  addTimeTableDetail(id: number) {

    const dialogRef = this.matDialog.open(TTDetailFormComponent, {
      width: '800px',
      data: { timTableId: id, action: GLOBALS.pageActions.create }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.findAllTimeTableDetails(this.timeTable.id);
      }
    });

  }

  editTTDetail(timeTableId: number, id: number) {

    const dialogRef = this.matDialog.open(TTDetailFormComponent, {
      width: '800px',
      data: { id: id, action: GLOBALS.pageActions.update, timeTableId: timeTableId, }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.findAllTimeTableDetails(this.timeTable.id);
      }

    });

  }

  deleteTTDetail(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        // this.loaded = false;
        this.ttDetailService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Time Table Detail has been deleted.', msgType: 'success' });

            this.findAllTimeTableDetails(this.timeTable.id);
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
          },
          () => {

          }
        );
      }
    });
  }


  delete(id: number) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, { width: GLOBALS.deleteDialog.width, data: { message: GLOBALS.deleteDialog.message } });

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.timeTableService.delete(id).subscribe(response => {

          if (response && (response['children']) ? response['children'].length > 0 : false) {

            this.loaded = true;
            this.pageState = 'active';

            this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

          } else {

            this.layoutService.flashMsg({ msg: 'Time Table is deleted.', msgType: 'success' });
            this.router.navigate(['/time-table/timeTable']);

          }


        },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
            this.pageState = 'active';
          },
          () => { }
        );
      }
    });
  }


  /**
   * Function To get List of all the slots
   */
  getListOfSlots() {

    this.slotService.findAttributesList().subscribe(response => {

      this.slotModel = response;

      this.slotModel.forEach((item) => {
        item.toTime = dateFormat(item.toTime, "UTC:hh:MM");
        item.fromTime = dateFormat(item.fromTime, "UTC:hh:MM");
      })

    },
      error => {
        console.log(error);
      },
      () => {
      }
    );

  }


  /**
   * Function to get List of all the Courses
   */
  getListOfCourses() {

    this.courseService.findAttributesList().subscribe(response => {

      this.courseModel = response;

    },
      error => {
        console.log(error);
      },
      () => {
      }
    );

  }

  /**
   * Function to Get List of all the Sections
   */
  getListOfSections() {

    this.sectionService.findAttributesList().subscribe(response => {

      this.sectionModel = response;

    },
      error => {
        console.log(error);
      },
      () => {
      }
    );

  }

}
