import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as dateFormat from 'dateformat';
import { GLOBALS } from '../../../../../../app/config/globals';
import { TTDetailService } from '../../../../../services';
import { SlotService, SectionService } from '../../../../../services';
import { CourseService } from '../../../../../../academic/services';
import { LayoutService } from '../../../../../../layout/services';
import { TTDetailModel } from '../../../../../models';
import { SlotModel, SectionModel } from '../../../../../models';
import { CourseModel } from '../../../../../../academic/models';

@Component({
  selector: 'tt-detail-form',
  templateUrl: './tt-detail-form.component.html',
  styleUrls: ['./tt-detail-form.component.css']
})

export class TTDetailFormComponent {

  public loaded: boolean = false;
  private pageAct: string;
  public pageTitle: string;
  private timeTableId: number;
  private id: number;
  public fg: FormGroup;
  public sections = new FormControl();
  public days = new FormControl();
  public componentLabels = TTDetailModel.attributesLabels;

  public ttDetail: TTDetailModel;
  public slotModel = [new SlotModel()];
  public courseModel = [new CourseModel()];
  public sectionModel = [new SectionModel()];
  public selectedSections = [];
  public weekDays=GLOBALS.weekDays;
  public selectedDays = [];

  constructor(
    private ttDetailService: TTDetailService,
    private slotService: SlotService,
    private courseService: CourseService,
    private sectionService: SectionService,
    private fb: FormBuilder,
    private layoutService: LayoutService,
    public dialogRef: MatDialogRef<TTDetailFormComponent>,

    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.loaded = true;
  }

  ngOnInit() {

    this.timeTableId = this.data.timTableId;
    this.id = this.data.id;
    this.pageAct = this.data.action
    this.initializePage();
    this.getListOfSlots();
    this.getListOfCourses();
    this.getListOfSections();

  }

  private initializePage() {

    this.fg = this.fb.group(new TTDetailModel().validationRules());

    if (this.pageAct === 'create') {

      this.pageTitle = 'Create Detail'
      this.initAddPage();

    } 
    else {

      this.getData(this.id);

    }
  }

  /**
   * Funtion to get Time table Details of the Time Table
   * @param id Time Table Id
   */
  private getData(id: number) {

    this.loaded=false;
    this.ttDetailService.find(id).subscribe(response => {

        this.ttDetail = response;
        this.fg.patchValue(this.ttDetail);

        //Set Values of all the Sections assocaited with the Time Table Detail
        this.sections.setValue(response['sections']);
        this.selectedSections=response['sections'];
        this.sectionChange();

        //Set Values of all the Days assocaited with the Time Table Detail
        this.days.setValue(response['days']);
        this.selectedDays=response['days'];
        this.dayChange();
        
      },
      error => {
        this.loaded = true;
      },
      () => {
       
        this.initUpdatePage();
        this.loaded = true;
      }
    );
  }

  private initAddPage() {

    this.ttDetail = new TTDetailModel();
    this.fg.enable();

    this.loaded = true;
  }

  private initUpdatePage() {

    this.fg.enable();
    this.pageTitle = `Update`;

  }

  public saveData(item: TTDetailModel) {

    this.loaded = false;
    if (this.pageAct === 'create') {

      this.create(item);

    } 
    else {

      this.update(this.id, item);

    }
  }
public listOfErrors=[];
  private create(item: TTDetailModel) {
    
    // get id of loggedIn userId
    item.createdBy = +this.getLoggedInUserInfo('id');
    item.timeTableId = this.timeTableId;

    //Object to be send at Backend
    let postData={
      ttDetail:item,
      sectionIds:this.sections.value,
      dayIds:this.days.value,
    }

    this.ttDetailService.create(postData).subscribe(response => {
      this.loaded = true;

        this.ttDetail = response;
        this.layoutService.flashMsg({ msg: 'Time Table Detail has been created.', msgType: 'success' });
        this.dialogRef.close(true);

      },
      error => {
        let res=error['_body'].split(',');
        for (let i = 0; i < res.length; i++) {
          res[i]=res[i].replace('"','');
          res[i]=res[i].replace('[','');
          res[i]=res[i].replace(']','');
          res[i]=res[i].replace('e"','e');
          
        }

        this.listOfErrors=res
        this.loaded = true;
        
        
        // this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });

      },
      () => {
        this.loaded = true;

      }
    );
  }

  private update(id: number, item: TTDetailModel) {

    this.loaded = false;
    // get id of loggedIn userId
    item.updatedBy = +this.getLoggedInUserInfo('id');
    item.timeTableId = this.data.timeTableId;

    //Object to be send at Backend
    let postData={
      ttDetail:item,
      sectionIds:this.sections.value,
      dayIds:this.days.value,
    }

    this.ttDetailService.update(id, postData).subscribe(response => {

        this.layoutService.flashMsg({ msg: 'Time Table Detail has been updated.', msgType: 'success' });
        this.dialogRef.close(true);

      },
      error => {
        let res=error['_body'].split(',');
        for (let i = 0; i < res.length; i++) {
          res[i]=res[i].replace('"','');
          res[i]=res[i].replace('[','');
          res[i]=res[i].replace(']','');
          res[i]=res[i].replace('e"','e');
          
        }

        this.listOfErrors=res
        this.loaded = true;

      },
      () => {
        this.loaded = true;

      }
    );
  }

  private getLoggedInUserInfo(parameter: string): string {

    return localStorage.getItem(parameter);

  }

  onNoClick(): void {

    this.dialogRef.close(false);

  }

  /**
   * Get List Of all the Slots
   */
  getListOfSlots() {

    this.slotService.findAttributesList().subscribe(response => {

        this.slotModel = response;

        //Convet format of time of Slots to be Displayed according to Standard Time
        this.slotModel.forEach((item) => {
          item.toTime = dateFormat(item.toTime, "UTC:hh:MM");
          item.fromTime = dateFormat(item.fromTime, "UTC:hh:MM");
        })

      },
      error => {
        console.log(error);
      },
      () => {
      }
    );

  }

  /**
   * Function to get List of all the Courses
   */
  getListOfCourses() {

    this.courseService.findAttributesList().subscribe(response => {

        this.courseModel = response;

      },
      error => {
        console.log(error);
      },
      () => {
      }
    );

  }

  /**
   * Function To get List of all the Sections
   */
  getListOfSections() {

    this.sectionService.findAttributesList().subscribe(response => {

        this.sectionModel = response;

      },
      error => {
        console.log(error);
      },
      () => {
      }
    );

  }

  /**
   * Function Fires on the Change or selected of Section from Multiple Select box
   */
  sectionChange() {
   
    this.selectedSections=[];

    //Place the selected ids into array of selectedSections
    this.sectionModel.forEach(item => {

      if (this.sections.value.includes(item.id)) {
        this.selectedSections.push(item.title);
      }

    });
  }

  /**
   * Function Fires on the Change or selected of Days from Multiple Select box
   */
  dayChange() {
   
    this.selectedDays=[];

    //Place the selected ids into array of selectedSDays
    this.weekDays.forEach(item => {
      if (this.days.value.includes(item.dayId)) {
        this.selectedDays.push(item.name);
      }

    });
  }


}
