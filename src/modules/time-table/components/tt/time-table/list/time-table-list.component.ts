import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';

import { PageAnimation } from 'modules/shared/helper';
import { TimeTableService } from 'modules/time-table/services';
import { LayoutService } from 'modules/layout/services';
import { TimeTableModel } from 'modules/time-table/models';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { GLOBALS } from '../../../../../app/config/globals';

@Component({
  selector: 'time-table-list',
  templateUrl: './time-table-list.component.html',
  styleUrls: ['./time-table-list.component.css'],
  animations: [PageAnimation]
})
export class TimeTableListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public timeTables: TimeTableModel[] = [new TimeTableModel()];

  public attrLabels = TimeTableModel.attributesLabels;

  displayedColumns = ['title', 'programDetail', 'classTitle', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private timeTableService: TimeTableService,
    public layoutService: LayoutService,
    public matDialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Time Tables' });
    this.getRecords();
  }

  /**
   * Get all the Time Tables
   */
  getRecords(): void {

    this.timeTableService.index().subscribe(response => {

      this.timeTables = response;
      this.dataSource = new MatTableDataSource<TimeTableModel>(this.timeTables);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    },
      error => {
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, { width: GLOBALS.deleteDialog.width, data: { message: GLOBALS.deleteDialog.message } });

    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.timeTableService.delete(id).subscribe(response => {

          if (response && (response['children']) ? response['children'].length > 0 : false) {

            this.loaded = true;
            this.pageState = 'active';

            this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

          } else {

            this.getRecords();
            this.layoutService.flashMsg({ msg: 'Time Table has been deleted.', msgType: 'success' });

          }


        },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
            this.pageState = 'active';
          },
          () => {
            this.getRecords();
            this.loaded = true;
            this.pageState = 'active';
          }
        );
      }
    });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }
}
