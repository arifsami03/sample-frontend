import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import * as dateFormat from 'dateformat';
import { PageAnimation } from '../../../../../shared/helper';
import { GLOBALS } from '../../../../../app/config/globals';
import { MappingTimeTableService, TimeTableService } from '../../../../services';
import { SlotService } from '../../../../services';
import { CampusService } from '../../../../../campus/services';
import { LayoutService } from '../../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../../shared/components';
import { DuplicateTimeTableModel, TimeTableModel } from '../../../../models';

import { SlotModel } from '../../../../models';
import { CampusModel } from '../../../../../campus/models';

@Component({
  selector: 'duplicate-time-table-form',
  templateUrl: './duplicate-time-table-form.component.html',
  styleUrls: ['./duplicate-time-table-form.component.css'],
  animations: [PageAnimation],
  providers: [SlotService, CampusService]
})

export class DuplicateTimeTableFormComponent implements OnInit {

  public pageState = 'active';
  public loaded: boolean = false;
  public boxLoaded: boolean = true;
  public timeTableModel = [new TimeTableModel()];
  public slotModel = [new SlotModel()];
  public displayedColumns = ['slotTitle', 'newSlot', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
  public campuses = new FormControl();

  public fg: FormGroup;
  public mappingTimeTable: DuplicateTimeTableModel;
  public componentLabels = DuplicateTimeTableModel.attributesLabels;

  public selectedCampuses = [];
  public campusModel = [new CampusModel()];

  public slotDaysModel = [];


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private mappingTimeTableService: MappingTimeTableService,
    private campusService: CampusService,
    private slotService: SlotService,
    private timeTableService: TimeTableService,
    private layoutService: LayoutService
  ) {
  }

  /**
   * ngOnInit
   */
  ngOnInit() {

    this.initializePage();
    this.getListOfTimeTable();
    this.getListOfAllCampuses();
    this.getListOfSlots();

  }

  /**
   * initialize page
   */
  private initializePage() {

    this.fg = this.fb.group(new DuplicateTimeTableModel().validationRules());

    this.initCreatePage();

  }

  /**
   * Init create page
   */
  private initCreatePage() {

    this.loaded = true;
    this.layoutService.setPageTitle({ title: 'Duplicate Time Table' });

    this.mappingTimeTable = new DuplicateTimeTableModel();

    this.fg.enable();
  }

  /**
   * initialize the view page
   */
  private initViewPage() {

    this.fg.disable();
    this.campuses.disable();

    this.layoutService.setPageTitle({
      title: 'Mapping Time Table: ' + this.mappingTimeTable.timeTableTitle,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Mapping Time Table ',
          url: '/time-table/mappingTimeTable'
        },
        { label: this.mappingTimeTable.timeTableTitle }
      ]
    });

  }

  /**
   * initialize the update page
   */
  private initUpdatePage() {

    this.fg.enable();
    this.fg.get('campusId').disable();

    this.layoutService.setPageTitle({

      title: 'Update : ' + this.mappingTimeTable.timeTableTitle,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Mapping Time Table',
          url: '/time-table/mappingTimeTable'
        },
        {
          label: this.mappingTimeTable.timeTableTitle,
          url: `/time-table/mappingTimeTable/view/${this.mappingTimeTable.id}`
        },
        { label: 'Update' }
      ]

    });

  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: DuplicateTimeTableModel) {

    //Object to be send on Backend
    let postData = {
      timeTable: item,
      campusIds: this.campuses.value,
      slotDetails: this.slotDaysModel
    }

    if (this.isValidDate()) {
      this.boxLoaded = false;

      this.timeTableService.saveDuplicateTimeTable(item.timeTableId, postData).subscribe(
        response => {

          this.layoutService.flashMsg({ msg: 'Duplicate Time Table has been Created.', msgType: 'success' });
          this.router.navigate([`/time-table/timeTable/view/${response}`]);

        },
        error => {

          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.boxLoaded = true;

        },
        () => {
          this.boxLoaded = true;
        }
      );
    }


  }



  /**
   * Function to get List of All the Time Tables
   */
  getListOfTimeTable() {

    this.timeTableService.findAttributesList().subscribe(response => {

      this.timeTableModel = response;

    },
      error => {
      }
    );
  }

  /**
   * Function to Get List of all the Campuses
   */
  getListOfAllCampuses() {

    this.campusService.findAllCampuses().subscribe(response => {

      this.campusModel = response;

    },
      error => {
      }
    );

  }

  /**
   * Function to be Fired on Campus Change
   */
  campusChange() {

    this.selectedCampuses = [];

    this.campusModel.forEach(item => {

      if (this.campuses.value.includes(item.id)) {
        this.selectedCampuses.push(item.campusName);
      }

    });
  }


  /**
   * Function to be Fired on Time Table Change
   */
  timeTableChange(event) {

    this.slotDaysModel = [];

    this.timeTableService.getListOfSlotsInTimeTable(event.value).subscribe(response => {

      for (let i = 0; i < response.length; i++) {

        for (let j = 0; j < response[i]['days'].length; j++) {

          var monday = false;
          var tuesday = false;
          var wednesday = false;
          var thursday = false;
          var friday = false;
          var saturday = false;
          var sunday = false;

          //If the Item from the List contains The Day add in model True else Add false
          if (response[i]['days'].includes('MON')) {
            monday = true;
          }
          if (response[i]['days'].includes('TUE')) {
            tuesday = true;
          }
          if (response[i]['days'].includes('WED')) {
            wednesday = true;
          }
          if (response[i]['days'].includes('THR')) {
            thursday = true;
          }
          if (response[i]['days'].includes('FRI')) {
            friday = true;
          }
          if (response[i]['days'].includes('SAT')) {
            saturday = true;
          }
          if (response[i]['days'].includes('SUN')) {
            sunday = true;
          }



        }
        //Create array of slotDaysModel 
        this.slotDaysModel.push({
          slotId: response[i].slotId,
          slotTitle: response[i].slotTitle,
          slotOrder: response[i].slotOrder,
          courseId: response[i].courseId,
          sections: response[i].sections,
          monday: monday,
          tuesday: tuesday,
          wednesday: wednesday,
          thursday: thursday,
          friday: friday,
          saturday: saturday,
          sunday: sunday,
        })


      }

    },
      error => {
      }
    );


  }

  /**
   * Function to Get List of all the Slots
   */
  getListOfSlots() {

    this.slotService.findAttributesList().subscribe(response => {

      this.slotModel = response;

      //Set Value of from time and to time according to Standard time
      this.slotModel.forEach((item) => {
        item.toTime = dateFormat(item.toTime, "UTC:hh:MM");
        item.fromTime = dateFormat(item.fromTime, "UTC:hh:MM");
      })

    },
      error => {
        console.log(error);
      },
      () => {
      }
    );

  }
  /**
     * Validating that the given dates
     */
  isValidDate() {

    let validFrom = new Date(this.fg.controls['validFrom'].value);
    let validTo = new Date(this.fg.controls['validTo'].value);
    if (validTo > validFrom) {
      this.fg.controls['validTo'].setErrors(null);
      return true;
    } else {
      this.fg.controls['validTo'].setErrors({ invalid: true })
      return false;
    }

  }
}
