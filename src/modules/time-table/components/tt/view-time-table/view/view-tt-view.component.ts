import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as dateFormat from 'dateformat';
import { PageAnimation } from '../../../../../shared/helper';
import { TimeTableService, TTDetailService } from '../../../../services';
import { LayoutService } from '../../../../../layout/services';
import { TimeTableModel, TTDetailModel } from '../../../../models';
import { ConfirmDialogComponent } from '../../../../../shared/components';

import { GLOBALS } from '../../../../../app/config/globals';

@Component({
  selector: 'view-tt-view',
  templateUrl: './view-tt-view.component.html',
  styleUrls: ['./view-tt-view.component.css'],
  animations: [PageAnimation]
})

export class ViewTTViewComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;

  public expanded: number = 0;
  public mappedTT = []

  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;


  public timeTable: TimeTableModel;
  public ttDetails: TTDetailModel[];


  public selectedDays = [];
  public days = new FormControl();
  public weekDays = GLOBALS.weekDays;

  public componentLabels = TimeTableModel.attributesLabels;
  public componentLabelsForDetails = TTDetailModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private timeTableService: TimeTableService,
    private ttDetailService: TTDetailService,
    private layoutService: LayoutService,
    public matDialog: MatDialog
  ) {
  }

  ngOnInit() {

    this.fg = this.fb.group(new TTDetailModel().validationRules());
    this.fg.enable();

    this.getData();

  }

  /**
   * Function to get Time Table
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.timeTableService.viewTTDetails(params['id']).subscribe(response => {

        this.timeTable = response;
        let tt = response;
        
        for (let i = 0; i < response['timeTableDetails'].length; i++) {



          if (i > 0) {

            if (response['timeTableDetails'][i]['slotId'] == response['timeTableDetails'][i - 1]['slotId']) {

              let days = [];

              for (let j = 0; j < response['timeTableDetails'][i]['days'].length; j++) {

                this.mappedTT[this.mappedTT.length - 1].days.push({
                  day: response['timeTableDetails'][i]['days'][j],
                  course: response['timeTableDetails'][i].courseTitle,
                  sections: response['timeTableDetails'][i].sectionTitles
                })

              }
              // mappedTT[mappedTT.length-1].days.push(days);

            }
            else {
              let days = [];

              for (let j = 0; j < response['timeTableDetails'][i]['days'].length; j++) {

                days.push({
                  day: response['timeTableDetails'][i]['days'][j],
                  course: response['timeTableDetails'][i].courseTitle,
                  sections: response['timeTableDetails'][i].sectionTitles
                })

              }

              this.mappedTT.push(
                {
                  slotId: response['timeTableDetails'][i].slotId,
                  slotOrder: response['timeTableDetails'][i].slotOrder,
                  slotTitle: response['timeTableDetails'][i].slotTitle,
                  days: days

                }
              )
            }

          }
          else {
            let days = [];

            for (let j = 0; j < response['timeTableDetails'][i]['days'].length; j++) {

              days.push({
                day: response['timeTableDetails'][i]['days'][j],
                course: response['timeTableDetails'][i].courseTitle,
                sections: response['timeTableDetails'][i].sectionTitles
              })

            }

            this.mappedTT.push(
              {
                slotId: response['timeTableDetails'][i].slotId,
                slotOrder: response['timeTableDetails'][i].slotOrder,
                slotTitle: response['timeTableDetails'][i].slotTitle,
                days: days

              }
            )
          }

        }
      },
        error => {
          this.loaded = true;
        },
        () => {

          this.initViewPage();

          this.loaded = true;

        }
      );
    });
  }

  private initViewPage() {
    //this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Time Table: ' + this.timeTable.title + ' ( ' + this.timeTable.programDetail + ' - ' + this.timeTable.classTitle + ' ) ',
      breadCrumbs: [{ label: 'Home', url: '/home/dashboard' }, { label: 'View Time Table', url: '/time-table/viewTimeTable' }, { label: this.timeTable.title }]
    });

  }

search(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
        if (myArray[i].day === nameKey) {

            return myArray[i];
        }
    }
}





}
