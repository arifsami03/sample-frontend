import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog,MatSort } from '@angular/material';

import { PageAnimation } from '../../../../../shared/helper';
import { TimeTableService } from '../../../../services';
import { LayoutService } from '../../../../../layout/services';
import { TimeTableModel } from '../../../../models';
import { ConfirmDialogComponent } from '../../../../../shared/components';

@Component({
  selector: 'view-time-table-list',
  templateUrl: './view-time-table-list.component.html',
  styleUrls: ['./view-time-table-list.component.css'],
  animations: [PageAnimation]
})
export class ViewTimeTableListComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;

  public timeTables: TimeTableModel[] = [new TimeTableModel()];

  public attrLabels = TimeTableModel.attributesLabels;

  displayedColumns = ['title', 'programDetail','classTitle', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private timeTableService: TimeTableService,
    public layoutService: LayoutService,
    public matDialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'View Time Tables' });
    this.getRecords();
  }

  /**
   * Get all the Time Tables
   */
  getRecords(): void {

    this.timeTableService.index().subscribe(response => {

        this.timeTables = response;
        this.dataSource = new MatTableDataSource<TimeTableModel>(this.timeTables);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      },
      error => {
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.timeTableService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Time Table has been deleted.', msgType: 'success' });
          },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
          },
          () => {
            this.getRecords();
            this.loaded = true;
          }
        );
      }
    });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }
}
