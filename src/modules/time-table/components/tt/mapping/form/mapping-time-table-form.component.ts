import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { PageAnimation } from '../../../../../shared/helper';
import { GLOBALS } from '../../../../../app/config/globals';
import { MappingTimeTableService, TimeTableService } from '../../../../services';
import { CampusService } from '../../../../../campus/services';
import { LayoutService } from '../../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../../shared/components';
import { MappingTimeTableModel, TimeTableModel } from '../../../../models';
import { CampusModel } from '../../../../../campus/models';
import * as dateFormat from 'dateformat';

@Component({
  selector: 'mapping-time-table-form',
  templateUrl: './mapping-time-table-form.component.html',
  styleUrls: ['./mapping-time-table-form.component.css'],
  animations: [PageAnimation],
  providers: [CampusService]
})

export class MappingTimeTableFormComponent implements OnInit {

  public pageState = 'active';
  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public boxLoaded: boolean = true;
  public timeTableModel = [new TimeTableModel()];
  public programDetailTitle: string;
  public camId: number;
  public classTitle: string;
  public campuses = new FormControl();

  public displayDetails: boolean = false;
  public displayAcademiCalendar: boolean = false;
  public academicCalendar: string;

  public fg: FormGroup;
  public pageAct: string;
  public mappingTimeTable: MappingTimeTableModel;
  public componentLabels = MappingTimeTableModel.attributesLabels;

  public selectedCampuses = [];
  public campusModel = [];


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private mappingTimeTableService: MappingTimeTableService,
    private campusService: CampusService,
    private timeTableService: TimeTableService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {

    this.initializePage();
    this.getListOfTimeTable();
    //this.getListOfAllCampuses();

  }


  /**
   * initialize page
   */
  private initializePage() {

    this.fg = this.fb.group(new MappingTimeTableModel().validationRules());

    if (this.pageAct === this.pageActions.create) {

      this.initCreatePage();

    }
    else {

      this.getData();

    }
  }

  /**
   * Get record
   */
  private getData() {

    this.route.params.subscribe(params => {

      this.mappingTimeTableService.find(params['id']).subscribe(response => {

        this.mappingTimeTable = response;

        if (this.mappingTimeTable) {

          this.fg.patchValue(this.mappingTimeTable);
          let event = {}
          event['value'] = this.mappingTimeTable.timeTableId
          this.timeTableChange(event);
          this.camId = this.mappingTimeTable.campusId



          //TODO:low need to check why we are dowing this after fetching data
          if (this.pageAct == this.pageActions.view) {

            this.initViewPage();

          }
          else if (this.pageAct == this.pageActions.update) {

            this.initUpdatePage();

          }
        }
      },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {

    this.loaded = true;
    this.layoutService.setPageTitle({ title: 'Mapping Time Table: Create' });

    this.mappingTimeTable = new MappingTimeTableModel();

    this.fg.enable();

  }

  /**
   * initialize the view page
   */
  private initViewPage() {

    this.fg.disable();
    this.campuses.disable();

    this.layoutService.setPageTitle({
      title: 'Mapping Time Table: ' + this.mappingTimeTable.timeTableTitle,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Mapping Time Table ',
          url: '/time-table/mappingTimeTable'
        },
        { label: this.mappingTimeTable.timeTableTitle }
      ]
    });

  }

  /**
   * initialize the update page
   */
  private initUpdatePage() {

    this.fg.enable();
    this.fg.get('campusId').disable();

    this.layoutService.setPageTitle({
      title: 'Update : ' + this.mappingTimeTable.timeTableTitle,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Mapping Time Table',
          url: '/time-table/mappingTimeTable'
        },
        {
          label: this.mappingTimeTable.timeTableTitle,
          url: `/time-table/mappingTimeTable/view/${this.mappingTimeTable.id}`
        },
        { label: 'Update' }
      ]
    });

  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: MappingTimeTableModel) {

    let startDate = this.fg.controls['validFrom']['value'];
    let endDate = this.fg.controls['validTo']['value'];

    if (endDate < startDate) {

      this.fg.controls['validTo'].setErrors({ invalid: true });


    }
    else {

      let saveData = false;
      if (this.pageAct == this.pageActions.update) {

        for (var i = 0; i < this.campusModel.length; i++) {

          let sDate=new Date(startDate);
          let eDate=new Date(endDate);
          let csDate=new Date(this.campusModel[i].startDate);
          let ceDate=new Date(this.campusModel[i].endDate);

          if (this.campusModel[i].id == this.mappingTimeTable.campusId) {



            if (sDate >= csDate) {

              if (eDate <= ceDate) {

                saveData = true;

              }
              else {

                this.fg.controls['validTo'].setErrors({ dirty: true });

              }
            }
            else {
              this.fg.controls['validFrom'].setErrors({ dirty: true });

              if (eDate <= ceDate) {

              }
              else {

                this.fg.controls['validTo'].setErrors({ dirty: true });

              }
            }
          }
        }
      }
      else {
        for (var i = 0; i < this.campusModel.length; i++) {
          let sDate=new Date(startDate);
          let eDate=new Date(endDate);
          let csDate=new Date(this.campusModel[i].startDate);
          let ceDate=new Date(this.campusModel[i].endDate);

          if (this.campusModel[i].id == item.campusId) {

            if (sDate >= csDate) {

              if (eDate <= ceDate) {

                saveData = true;

              }
              else {
                this.fg.controls['validTo'].setErrors({ dirty: true });
                // this.fg.controls['validFrom'].setErrors({ dirty: true });
              }
            }
            else {
              this.fg.controls['validFrom'].setErrors({ dirty: true });
              if (eDate <= ceDate) {
              }
              else {
                this.fg.controls['validTo'].setErrors({ dirty: true });
              }
            }

          }
        }
      }


      if (saveData) {
        let postData = {};

        //If Form is in Update Mode then pass Campus Id From the mappingTimeTable Mode
        //Else If it is in Create Mode the Campus Id from the Select box
        if (this.pageAct == this.pageActions.update) {

          postData = {
            ttValidityData: item,
            campusIds: this.mappingTimeTable.campusId
          }

        }
        else {

          postData = {
            ttValidityData: item,
            campusIds: item.campusId
          }

        }



        //Save Functionality
        if (this.pageAct === this.pageActions.create) {
          if (this.isValidDate()) {
            this.boxLoaded = false;

            this.mappingTimeTableService.create(postData).subscribe(response => {

              this.layoutService.flashMsg({ msg: 'Mapping Time Table has been created.', msgType: 'success' });
              this.router.navigate([`/time-table/mappingTimeTable`]);

            },

              error => {

                this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
                this.boxLoaded = true;

              },
              () => {
                this.boxLoaded = true;
              }
            );
          }

        }
        //Update Functionality
        else if (this.pageAct === this.pageActions.update) {
          if (this.isValidDate()) {
            this.mappingTimeTableService.update(this.mappingTimeTable.id, postData).subscribe(response => {

              this.layoutService.flashMsg({ msg: 'Mapping Time Table has been updated.', msgType: 'success' });
              this.router.navigate([`/time-table/mappingTimeTable/view/${this.mappingTimeTable.id}`]);

            },
              error => {

                this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
                this.boxLoaded = true;

              },
              () => {

                this.boxLoaded = true;

              }
            );
          }

        }
      }
      else {
        // this.fg.controls['validTo'].setErrors({ dirty: true });
        // this.fg.controls['validFrom'].setErrors({ dirty: true });
      }

    }
  }


  /**
   * Delete record
   *
   */
  delete(id: number) {

    this.matDialog.open(ConfirmDialogComponent, {

      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }

    }).afterClosed().subscribe((accept: boolean) => {

      if (accept) {
        this.loaded = false;

        this.mappingTimeTableService.delete(id).subscribe(response => {

          this.layoutService.flashMsg({ msg: 'Mapping Time Table has been deleted.', msgType: 'success' });
          this.router.navigate(['/time-table/mappingTimeTable']);

        },
          error => {

            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });

          }
        );
      }
    });
  }

  /**
   * Function to Get list of all the Time Tables
   */
  getListOfTimeTable() {

    this.timeTableService.findAttributesList().subscribe(response => {

      this.timeTableModel = response;

    },
      error => {
      }
    );
  }


  /**
   * Function to get List of all the campuses
   */
  getListOfAllCampuses(programDetailID) {

    this.campusService.getCampusByProgramDetailId(programDetailID).subscribe(response => {

      this.campusModel = response;
      if (this.pageAct === this.pageActions.update) {
        for (var i = 0; i < this.campusModel.length; i++) {

          if (this.campusModel[i].id == this.camId) {
            this.academicCalendar = this.campusModel[i].academicCalendar
          }
        }
        this.displayAcademiCalendar = true;
      }


    },
      error => {
      }
    );

  }

  campusChange(event) {

    for (var i = 0; i < this.campusModel.length; i++) {

      if (this.campusModel[i].id == event.value) {
        this.academicCalendar = this.campusModel[i].academicCalendar
      }
    }
    this.displayAcademiCalendar = true;

  }

  /**
    * Validating that the given dates
    */
  isValidDate() {

    let validFrom = new Date(this.fg.controls['validFrom'].value);
    let validTo = new Date(this.fg.controls['validTo'].value);
    if (validTo > validFrom) {
      this.fg.controls['validTo'].setErrors(null);
      return true;
    } else {
      this.fg.controls['validTo'].setErrors({ invalid: true })
      return false;
    }

  }

  timeTableChange(event) {
    this.timeTableService.find(event.value).subscribe(response => {
      this.programDetailTitle = response.programDetail;
      this.classTitle = response.classTitle;
      this.displayDetails = true;
      this.getListOfAllCampuses(response.programDetailId)

    },
      error => {
      }
    );
  }

}
