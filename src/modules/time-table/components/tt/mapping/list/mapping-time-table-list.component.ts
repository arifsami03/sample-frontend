import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import * as dateFormat from 'dateformat';

import { PageAnimation } from '../../../../../shared/helper';
import { GLOBALS } from '../../../../../app/config/globals';
import { ConfirmDialogComponent } from '../../../../../shared/components';
import { LayoutService } from '../../../../../layout/services';
import { MappingTimeTableService } from '../../../../services';
import { MappingTimeTableModel } from '../../../../models';


@Component({
  selector: 'mapping-time-table-list',
  templateUrl: './mapping-time-table-list.component.html',
  styleUrls: ['./mapping-time-table-list.component.css'],
  animations: [PageAnimation]
})

export class MappingTimeTableListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public data: MappingTimeTableModel[] = [new MappingTimeTableModel()];

  public attrLabels = MappingTimeTableModel.attributesLabels;

  displayedColumns = ['timeTableTitle', 'campusTitle', 'validFrom', 'validTo', 'options'];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(

    private mappingTimeTableService: MappingTimeTableService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService

  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {

    this.layoutService.setPageTitle({ title: 'Mapping Time Table' });

    this.getRecords();
  }

  /**
   * Get data/records from backend
   */
  getRecords() {

    this.mappingTimeTableService.index().subscribe(response => {

      if (response) {

        this.data = response;

        //Set Format of time to Standard Time 
        this.data.forEach(element => {

          element.validFrom = dateFormat(element.validFrom, "mmmm dS , yyyy");
          element.validTo = dateFormat(element.validTo, "mmmm dS , yyyy");

        });

        this.dataSource = new MatTableDataSource<MappingTimeTableModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      }
    },
      error => {

        this.loaded = true;
        this.pageState = 'active';

      },
      () => {

        this.loaded = true;
        this.pageState = 'active';

      }
    );
  }

  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {

    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;

  }

  /**
   * Delete record
   */
  delete(id: number) {

    this.matDialog.open(ConfirmDialogComponent, {

      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }

    })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.mappingTimeTableService.delete(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Mapping Against Time Table has been deleted.', msgType: 'success' });
              this.getRecords();
              // setTimeout((router: Router) => {
              //   this.success = false;
              // }, 1000);
            },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            }
          );
        }
      });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }
}
