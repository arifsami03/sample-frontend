export * from './dashboard/dashboard.component';
export * from './slot/list/slot-list.component';
export * from './slot/form/slot-form.component';
export * from './section/list/section-list.component';
export * from './section/form/section-form.component';
export * from './campus_building/list/campus-building-list.component'
export * from './campus_building/form/campus-building-form.component'
export * from './campus_building/cb_floor/list/cb-floor-list.component'
export * from './campus_building/cb_floor/form/cb-floor-form.component'
export * from './campus_building/cb_floor/cbf_room/list/cbf-room-list.component'
export * from './campus_building/cb_floor/cbf_room/form/cbf-room-form.component'
export * from './tt/time-table/list/time-table-list.component';
export * from './tt/time-table/form/time-table-form.component';
export * from './tt/time-table/view/time-table-view.component';
export * from './tt/time-table/view/detail-dialog-form/tt-detail-form.component';
export * from './tt/mapping/list/mapping-time-table-list.component';
export * from './tt/mapping/form/mapping-time-table-form.component';
export * from './tt/duplicate/form/duplicate-time-table-form.component';
export * from './tt/view-time-table/list/view-time-table-list.component';
export * from './tt/view-time-table/view/view-tt-view.component';
