import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { PageAnimation } from '../../../../../shared/helper';
import { CBFloorService, CampusBuildingService } from '../../../../services';
import { LayoutService } from '../../../../../layout/services';
import { CBFloorModel, CampusBuildingModel } from '../../../../models';
import { ConfirmDialogComponent } from '../../../../../shared/components';
import { GLOBALS } from '../../../../../app/config/globals';

@Component({
  selector: 'setting-cb-floor-form',
  templateUrl: './cb-floor-form.component.html',
  styleUrls: ['./cb-floor-form.component.css'],
  animations: [PageAnimation]
})
export class CBFloorFormComponent implements OnInit {

  public pageState = 'active';
  public loaded: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;

  public componentLabels = CBFloorModel.attributesLabels;
  public cbFloor: CBFloorModel;
  public campusBuildings: CampusBuildingModel[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private cbFloorService: CBFloorService,
    private campusBuildingService: CampusBuildingService,
    private layoutService: LayoutService,
    public matDialog: MatDialog,

  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  ngOnInit() {
    this.campusBuildingService.findAttributesListWithCampusName().subscribe(
      response => {
        this.campusBuildings = response;
      },
      error => {
        this.campusBuildings = [new CampusBuildingModel()];
      },
      () => {
        this.initializePage();
      }
    );
  }

  private initializePage() {
    this.fg = this.fb.group(new CBFloorModel().validationRules());
    if (this.pageAct === 'create') {
      this.initAddPage();
    } else {
      this.getData();
    }
  }

  private getData() {
    this.route.params.subscribe(params => {
      this.cbFloorService.find(params['id']).subscribe(
        response => {
          this.cbFloor = response;
        },
        error => {
          this.loaded = true;
        },
        () => {
          this.fg.patchValue(this.cbFloor);
          if (this.pageAct == 'view') {
            this.initViewPage();
          } else if (this.pageAct == 'update') {
            this.initUpdatePage();
          }
          this.loaded = true;
        }
      );
    });
  }

  private initAddPage() {
    this.layoutService.setPageTitle({ title: 'Add Floor' });
    this.cbFloor = new CBFloorModel();
    this.fg.enable();
    this.loaded = true;
  }

  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Floor: ' + this.cbFloor.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Floor',
          url: '/time-table/cbFloors'
        },
        { label: this.cbFloor.name }
      ]
    });
  }

  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Floor: ' + this.cbFloor.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Floor', url: '/time-table/cbFloors' },
        {
          label: this.cbFloor.name,
          url: `/time-table/cbFloors/view/${this.cbFloor.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  public saveData(item: CBFloorModel) {

    item.abbreviation = item.abbreviation.toUpperCase();

    if (this.pageAct === 'create') {
      this.create(item);
    } else {
      this.update(this.cbFloor.id, item);
    }
  }

  private create(item: CBFloorModel) {

    this.cbFloorService.create(item).subscribe(
      response => {
        this.cbFloor = response;
        this.layoutService.flashMsg({ msg: 'Floor has been created.', msgType: 'success' });
      },
      error => {
        if (error.status == 400) {
          this.fg.controls['code'].setErrors({ duplicateCode: true });
        } else {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }

      },
      () => {
        this.loaded = true;
        this.router.navigate([`/time-table/cbFloors/view/${this.cbFloor.id}`]);
      }
    );
  }

  private update(id: number, item: CBFloorModel) {
    this.cbFloorService.update(id, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Floor has been updated.', msgType: 'success' });
      },
      error => {
        if (error.status == 400) {
          this.fg.controls['code'].setErrors({ duplicateCode: true });
        } else {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      },
      () => {
        this.loaded = true;
        this.router.navigate([`/time-table/cbFloors/view/${this.cbFloor.id}`]);
      }
    );
  }

  delete(id: number) {

    // Confirm dialog
    this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.cbFloorService.delete(id).subscribe(response => {
            this.layoutService.flashMsg({ msg: 'Floor has been deleted.', msgType: 'success' });
            this.router.navigate(['/time-table/cbFloors']);
          },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
              this.loaded = true;
            }
          );
        }
      });
  }

}
