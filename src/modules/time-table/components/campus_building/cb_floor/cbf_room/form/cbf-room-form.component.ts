import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { PageAnimation } from '../../../../../../shared/helper';
import { CBFRoomService, CBFloorService } from '../../../../../services';
import { LayoutService } from '../../../../../../layout/services';
import { CBFRoomModel, CBFloorModel } from '../../../../../models';
import { ConfirmDialogComponent } from '../../../../../../shared/components';
import { GLOBALS } from '../../../../../../app/config/globals';

@Component({
  selector: 'setting-cbf-room-form',
  templateUrl: './cbf-room-form.component.html',
  styleUrls: ['./cbf-room-form.component.css'],
  animations: [PageAnimation]
})
export class CBFRoomFormComponent implements OnInit {

  public pageState = 'active';
  public loaded: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;
  public classrromTypes = GLOBALS.classRoomTypes;

  public componentLabels = CBFRoomModel.attributesLabels;
  public cbfRoom: CBFRoomModel;
  public campusBuildingFloors: CBFloorModel[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private cbfRoomService: CBFRoomService,
    private cbFloorService: CBFloorService,
    private layoutService: LayoutService,
    public matDialog: MatDialog,

  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  ngOnInit() {
    this.cbFloorService.findAttributesListWithCampusAndBuilding().subscribe(
      response => {
        this.campusBuildingFloors = response;
      },
      error => {
        this.campusBuildingFloors = [new CBFloorModel()];
      },
      () => {
        this.initializePage();
      }
    );
  }

  private initializePage() {
    this.fg = this.fb.group(new CBFRoomModel().validationRules());
    if (this.pageAct === 'create') {
      this.initAddPage();
    } else {
      this.getData();
    }
  }

  private getData() {
    this.route.params.subscribe(params => {
      this.cbfRoomService.find(params['id']).subscribe(
        response => {
          this.cbfRoom = response;
        },
        error => {
          this.loaded = true;
        },
        () => {

          let classroomTypes = this.fg.get('classroomTypes') as FormArray;
          this.classrromTypes.forEach(type => {

            let checked = this.cbfRoom.classroomTypes.find(element => {
              return element == type.value;
            });
            classroomTypes.push(this.addClassroomType(type, checked));
          });

          this.fg.patchValue(this.cbfRoom);
          if (this.pageAct == 'view') {
            this.initViewPage();
          } else if (this.pageAct == 'update') {
            this.initUpdatePage();
          }
          this.loaded = true;
        }
      );
    });
  }

  private addClassroomType(type, checked) {
    return this.fb.group({
      checked: checked,
      label: type.label,
      value: type.value,
    });
  }

  private initAddPage() {

    let classroomTypes = this.fg.get('classroomTypes') as FormArray;
    this.classrromTypes.forEach(type => {
      classroomTypes.push(this.addClassroomType(type, false));
    });
    this.layoutService.setPageTitle({ title: 'Add Room' });
    this.cbfRoom = new CBFRoomModel();
    this.fg.enable();
    this.loaded = true;
  }

  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Room: ' + this.cbfRoom.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Room',
          url: '/time-table/cbfRooms'
        },
        { label: this.cbfRoom.name }
      ]
    });
  }

  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Room: ' + this.cbfRoom.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Room', url: '/time-table/cbfRooms' },
        {
          label: this.cbfRoom.name,
          url: `/time-table/cbfRooms/view/${this.cbfRoom.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  public saveData(item: CBFRoomModel) {

    item.abbreviation = item.abbreviation.toUpperCase();

    if (this.pageAct === 'create') {
      this.create(item);
    } else {
      this.update(this.cbfRoom.id, item);
    }
  }

  private create(item: CBFRoomModel) {

    this.cbfRoomService.create(item).subscribe(
      response => {
        this.cbfRoom = response;
        this.layoutService.flashMsg({ msg: 'Room has been created.', msgType: 'success' });
      },
      error => {
        if (error.status == 400) {
          this.fg.controls['code'].setErrors({ duplicateCode: true });
        } else {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }

      },
      () => {
        this.loaded = true;
        this.router.navigate([`/time-table/cbfRooms/view/${this.cbfRoom.id}`]);
      }
    );
  }

  private update(id: number, item: CBFRoomModel) {
    this.cbfRoomService.update(id, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Room has been updated.', msgType: 'success' });
      },
      error => {
        if (error.status == 400) {
          this.fg.controls['code'].setErrors({ duplicateCode: true });
        } else {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      },
      () => {
        this.loaded = true;
        this.router.navigate([`/time-table/cbfRooms/view/${this.cbfRoom.id}`]);
      }
    );
  }

  delete(id: number) {

    // Confirm dialog
    this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.cbfRoomService.delete(id).subscribe(response => {
            this.layoutService.flashMsg({ msg: 'Room has been deleted.', msgType: 'success' });
            this.router.navigate(['/time-table/cbfRooms']);
          },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
              this.loaded = true;
            }
          );
        }
      });
  }

}
