import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';

import { PageAnimation } from '../../../../../shared/helper';
import { GLOBALS } from '../../../../../app/config/globals';
import { ConfirmDialogComponent } from '../../../../../shared/components';
import { LayoutService } from '../../../../../layout/services';
import { CBFloorService } from '../../../../services';
import { CBFloorModel } from '../../../../models';

@Component({
  selector: 'campus-cb-floor-list',
  templateUrl: './cb-floor-list.component.html',
  styleUrls: ['./cb-floor-list.component.css'],
  animations: [PageAnimation]
})
export class CBFloorListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public data: CBFloorModel[] = [new CBFloorModel()];

  public attrLabels = CBFloorModel.attributesLabels;

  displayedColumns = ['campusName', 'buildingName', 'name', 'abbreviation', 'code', 'options'];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public error: Boolean;

  constructor(
    private cbFloorService: CBFloorService,
    public matDialog: MatDialog,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Floor' });

    this.getRecords();
  }

  /**
   * Get data/records from backend
   */
  getRecords() {

    this.cbFloorService.index().subscribe(response => {

      this.data = response;

       // for searching mechanism we are maping this in to single string
       this.data.map(element=>{
        element.buildingCampusName = element.campusBuilding.campus.campusName;
        element.campusBuildingName = element.campusBuilding.name;
      })

      this.dataSource = new MatTableDataSource<CBFloorModel>(this.data);

      this.dataSource.paginator = this.paginator;

      this.dataSource.sort = this.sort;

    },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {

    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;

  }

  /**
   * Delete record
   */
  delete(id: number) {

    // Confirm dialog
    this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.cbFloorService.delete(id).subscribe(response => {
            this.layoutService.flashMsg({ msg: 'Floor has been deleted.', msgType: 'success' });
            this.getRecords();
          },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
              this.loaded = true;
            }
          );
        }
      });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }
}
