import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { PageAnimation } from '../../../../shared/helper';
import { CampusBuildingService } from '../../../services';
import { CampusService } from '../../../../campus/services';
import { LayoutService } from '../../../../layout/services';
import { CampusBuildingModel } from '../../../models';
import { CampusModel } from '../../../../campus/models';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'setting-campus-building-form',
  templateUrl: './campus-building-form.component.html',
  styleUrls: ['./campus-building-form.component.css'],
  animations: [PageAnimation],
  providers: [CampusService]
})
export class CampusBuildingFormComponent implements OnInit {

  public pageState = 'active';
  public loaded: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;

  public componentLabels = CampusBuildingModel.attributesLabels;
  public campusBuilding: CampusBuildingModel;
  public campuses: CampusModel[];
  public deletedbuildingIds: number[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private campusBuildingService: CampusBuildingService,
    private campusService: CampusService,
    private layoutService: LayoutService,
    public matDialog: MatDialog,

  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  ngOnInit() {
    this.campusService.findAttributesList().subscribe(
      response => {
        this.campuses = response;
      },
      error => {
        this.campuses = [new CampusModel()];
      },
      () => {
        this.initializePage();
      }
    );
  }

  private initializePage() {
    this.fg = this.fb.group(new CampusBuildingModel().validationRules());
    if (this.pageAct === 'create') {
      this.initAddPage();
    } else {
      this.getData();
    }
  }

  private getData() {
    this.route.params.subscribe(params => {
      this.campusBuildingService.find(params['id']).subscribe(
        response => {
          this.campusBuilding = response;
        },
        error => {
          this.loaded = true;
        },
        () => {
          this.fg.patchValue(this.campusBuilding);
          if (this.pageAct == 'view') {
            this.initViewPage();
          } else if (this.pageAct == 'update') {
            this.initUpdatePage();
          }
          this.loaded = true;
        }
      );
    });
  }

  private initAddPage() {
    this.layoutService.setPageTitle({ title: 'Add Campus Building' });
    this.campusBuilding = new CampusBuildingModel();
    this.fg.enable();
    this.loaded = true;
  }

  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Campus Building: ' + this.campusBuilding.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Campus Building',
          url: '/time-table/campusBuildings'
        },
        { label: this.campusBuilding.name }
      ]
    });
  }

  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Campus Building: ' + this.campusBuilding.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Campus Building', url: '/time-table/campusBuildings' },
        {
          label: this.campusBuilding.name,
          url: `/time-table/campusBuildings/view/${this.campusBuilding.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  public saveData(item: CampusBuildingModel) {

    item.abbreviation = item.abbreviation.toUpperCase();

    if (this.pageAct === 'create') {
      this.create(item);
    } else {
      this.update(this.campusBuilding.id, item);
    }
  }

  private create(item: CampusBuildingModel) {

    this.campusBuildingService.create(item).subscribe(
      response => {
        this.campusBuilding = response;
        this.layoutService.flashMsg({ msg: 'Campus Building has been created.', msgType: 'success' });
      },
      error => {
        if (error.status == 400) {
          this.fg.controls['code'].setErrors({ duplicateCode: true });
        } else {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }

      },
      () => {
        this.loaded = true;
        this.router.navigate([`/time-table/campusBuildings/view/${this.campusBuilding.id}`]);
      }
    );
  }

  private update(id: number, item: CampusBuildingModel) {
    this.campusBuildingService.update(id, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Campus Building has been updated.', msgType: 'success' });
      },
      error => {
        if (error.status == 400) {
          this.fg.controls['code'].setErrors({ duplicateCode: true });
        } else {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      },
      () => {
        this.loaded = true;
        this.router.navigate([`/time-table/campusBuildings/view/${this.campusBuilding.id}`]);
      }
    );
  }

  delete(id: number) {

    // Confirm dialog
    this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.campusBuildingService.delete(id).subscribe(response => {
            this.layoutService.flashMsg({ msg: 'Campus Building has been deleted.', msgType: 'success' });
            this.router.navigate(['/time-table/campusBuildings']);
          },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
              this.loaded = true;
            }
          );
        }
      });
  }

}
