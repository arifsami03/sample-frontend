import { FormControl, Validators, ValidatorFn, FormArray, AbstractControl } from '@angular/forms';
import { MinLengthValidator } from '@angular/forms/src/directives/validators';

export class InstituteModel {
  id?: number;
  name: string;
  logo: string;
  abbreviation: string;
  website: string;
  officeEmail: string;
  headOfficeLocation: string;
  latitude: number;
  longitude: number;
  instituteTypeId: number;
  establishedScince: string;
  contactNumbers: any[]; //new FormArray([]); //any[]=[]; //TODO:Low Have to remove any;
  affiliation: any[]; //TODO:Low Have to remove any;
  focalPerson: string;
  Description: string;

  static attributesLabels = {
    name: 'Institution Name',
    logo: 'logo',
    abbreviation: 'Abbreviation',
    website: 'Website',
    officeEmail: 'Official Email',
    headOfficeLocation: 'Head Office Location',
    latitude: 'Latitude',
    longitude: 'Longitude',
    establishedScince: 'Established Since',
    contactNumbers: 'Contact No',
    affiliations: 'Affiliation',
    focalPerson: 'Focal Person',
    Description: 'Description',
    countryCode: 'Country',
    instituteTypeId: 'Institute Type',

  };

  constructor() { }

  /**
   *
   * @param equalControlName
   */
  public equalTo?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      //if (!control['_parent']) return null;

      // if (!control['_parent'].controls[equalControlName])
      //   throw new TypeError(
      //     'Form Control ' + equalControlName + ' does not exists.'
      //   );

      let controlMatch = 1850;

      return controlMatch <= control.value && 2018 >= control.value
        ? null
        : {
          equalTo: true
        };
    };
  }
  // ,[<any>Validators.pattern('^(http:\/\/|https:\/\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?$')]
  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      name: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      officeEmail: ['', [<any>Validators.required, Validators.email]],
      logo: [''],
      abbreviation: [''],
      website: new FormControl('', [<any>Validators.required, Validators.email]),
      headOfficeLocation: [''],
      latitude: [''],
      longitude: [''],
      establishedScince: ['', [this.equalTo('establishedScince')]],
      affiliations: new FormArray([]),
      focalPerson: [''],
      Description: [''],
      instituteTypeId:  ['', [<any>Validators.required]],
      contactNumbers: new FormArray([])
    };
  }
}
