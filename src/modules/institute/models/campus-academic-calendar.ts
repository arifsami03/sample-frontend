import { CampusInformationModel } from '../../campus/models';
import { FormControl, Validators, FormArray } from '@angular/forms';

import { AcademicCalendarProgramDetailModel } from '../../academic/models';

export class CampusAcademicCalendarModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    campusId: 'Campus',
    academicCalendarProgramDetailId: 'Academic Calendar Program Detail',
    startDate: 'Start Date',
    endDate: 'End Date'
  };

  id?: number;
  campusId?: number;
  academicCalendarProgramDetailId?: number;
  startDate?: Date;
  endDate?: Date;
  status ?: Boolean;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  campus?: CampusInformationModel;
  academicCalendarProgramDetail?: AcademicCalendarProgramDetailModel;

  constructor() {}

  /**
   * Form Validation Rules for Level of Education
   */
  public validationRules?() {
    return {
      campusId: new FormControl(null, [<any>Validators.required]),
      academicCalendarProgramDetailId: new FormControl(null, [<any>Validators.required]),
      mapDates : new FormArray([]),
    };
  }
}
