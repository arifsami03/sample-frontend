import { InstituteTypeModel } from '.';
import { FormControl, Validators } from '@angular/forms';

export class EducationLevelModel {
  /**
   * Set the labels for attributes
   */
  static attributesLabels = {
    education: 'Level of Education',
    instituteTypeId: 'Institute Type'
  };

  id?: number;
  education: string;
  instituteTypeId: number;
  instituteType?: InstituteTypeModel;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      education: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      instituteTypeId: new FormControl('', [<any>Validators.required])
    };
  }
}
