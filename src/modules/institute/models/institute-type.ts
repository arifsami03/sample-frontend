import { FormControl, Validators } from '@angular/forms';

export class InstituteTypeModel {
  /**
   * Set the labels for attributes
   */
  static attributesLabels = {
    name: 'Institute Type'
  };

  id?: number;
  name: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() {}

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      name: new FormControl('', [<any>Validators.required, Validators.maxLength(30)])
    };
  }
}
