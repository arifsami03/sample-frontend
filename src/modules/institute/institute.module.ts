import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { InstituteRoutes } from './institute.routes';

import { SharedModule } from '../shared/shared.module';
import { PDFModule } from '../pdf/pdf.module';

import {
  __IMPORTS,
  __DECLARATIONS,
  __PROVIDERS,
  __ENTRY_COMPONENTS
} from './components.barrel';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    InstituteRoutes,
    SharedModule,
    PDFModule,
    __IMPORTS
  ],
  declarations: [__DECLARATIONS],
  providers: [__PROVIDERS],
  entryComponents: [__ENTRY_COMPONENTS]
})
export class InstituteModule {}
