import 'rxjs/add/operator/map';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { BaseService } from '../../shared/services'; 
import { InstituteModel } from '../models';


@Injectable()
export class InstituteService extends BaseService {

  private _url: String = 'institute/institutes';

  constructor(protected http: Http) {
    super(http)
  }

  getAll(): Observable<InstituteModel> {
    
    return this.__get(`${this._url}/index`).map(data => { return <InstituteModel>data.json(); }).catch(this.handleError);

  }
  update(id: Number, item) {
    return this.__put(`${this._url}/update/${id}`, item).map(data => {
      return data.json();
    });
  }
 

}
