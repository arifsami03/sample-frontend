import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../shared/services';
import { InstituteTypeModel } from '../models';

@Injectable()
export class InstituteTypeService extends BaseService {

  private routeURL: String = 'institute/instituteTypes';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<InstituteTypeModel[]> {

    return this.__get(`${this.routeURL}/index`).map(data => {
      return <InstituteTypeModel[]>data.json();
    });

  }

  /**
   * Get All records with only id, name attributes 
   */
  findAttributesList(): Observable<InstituteTypeModel[]> {

    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <InstituteTypeModel[]>data.json();
    });

  }

  /**
   * Get single record
   */
  find(id: number): Observable<InstituteTypeModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <InstituteTypeModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: InstituteTypeModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: InstituteTypeModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }

}
