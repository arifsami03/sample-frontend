import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from '../../shared/services';
import { CampusAcademicCalendarModel } from '../models';

@Injectable()
export class CampusAcademicCalendarService extends BaseService {
  private routeURL: String = 'institute/campusAcademicCalendars';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<CampusAcademicCalendarModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <CampusAcademicCalendarModel[]>data.json();
    });
  }
  /**
   * it will return all the data of programs
   */
  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }
  /**
   * it will return all the data of programs with associated campuses
   */
  findCampusByACPD(acpdId) {
    return this.__get(`${this.routeURL}/findCampusByACPD/${acpdId}`).map(data => {
      return data.json();
    });
  }
  /**
   * Get single record
   */
  find(id: number): Observable<CampusAcademicCalendarModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <CampusAcademicCalendarModel>data.json();
    });
  }
  /**
   * Get single record
   */
  findWithAcademicCalendars(id: number): Observable<CampusAcademicCalendarModel[]> {
    return this.__get(`${this.routeURL}/findWithAcademicCalendars/${id}`).map(data => {
      return <CampusAcademicCalendarModel[]>data.json();
    });
  }
  /**
   * Get single record on the basis of campus and academic calendar program details
   */
  findByCampusAndAcademicCalendarProgramDetail(campusId: number, acpdId): Observable<CampusAcademicCalendarModel> {
    return this.__get(`${this.routeURL}/findByCampusAndAcademicCalendarProgramDetail/${campusId}/${acpdId}`).map(data => {
      return <CampusAcademicCalendarModel>data.json();
    });
  }

  /**
   * Delete record
   */
  deleteAll(campusId: number) {
    return this.__delete(`${this.routeURL}/deleteAll/${campusId}`).map(data => {
      return data.json();
    });
  }
  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  createWithCampusAndAcademicCalendarProgramDetails(item: CampusAcademicCalendarModel) {
    return this.__post(`${this.routeURL}/createWithCampusAndAcademicCalendarProgramDetails`, item).map(data => {
      return data.json();
    });
  }
  /**
   * Create record
   */
  updateWithCampusAndAcademicCalendarProgramDetails(campusId, item: CampusAcademicCalendarModel) {
    return this.__put(`${this.routeURL}/updateWithCampusAndAcademicCalendarProgramDetails/${campusId}`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: CampusAcademicCalendarModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Find related data for deletion
   * 
   * @param id 
   */
  findRelatedData(id: number) {
    return this.__get(`${this.routeURL}/findRelatedData/${id}`).map(data => {
      return data.json();
    });
  }
}
