import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GLOBALS } from '../app/config/globals';

/**
 * Import Components
 * 
 */
import {
  InstituteComponent,  InstituteViewComponent,  EducationLevelListComponent,  EducationLevelFormComponent,
  InstituteTypeFormComponent,  InstituteTypeListComponent,  CampusAcademicCalendarListComponent,  CampusAcademicCalendarFormComponent,
  CampusAcademicCalendarViewComponent,  CampusAcademicCalendarUpdateComponent,
} from './components';
import { AuthGuardService } from '../app/services';


/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: 'dashboard',
    data: { breadcrumb: { title: 'Institute', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: InstituteViewComponent,
        data: { breadcrumb: { title: 'Dashboard', display: false } }
      },
      {
        path: 'update',
        canActivate: [AuthGuardService],
        component: InstituteComponent,
        data: { breadcrumb: { title: 'Update', display: true } }
      }
    ]
  },
  
  {
    path: 'educationLevels',
    data: { breadcrumb: { title: 'Level of Education', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: EducationLevelListComponent,
        data: { breadcrumb: { title: 'Level of Education', display: false } }
      },
      {
        path: 'create',
        canActivate: [AuthGuardService],
        component: EducationLevelFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        canActivate: [AuthGuardService],
        component: EducationLevelFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        canActivate: [AuthGuardService],
        component: EducationLevelFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'instituteTypes',
    data: { breadcrumb: { title: 'Institute Type', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: InstituteTypeListComponent,
        data: { breadcrumb: { title: 'Institute Type', display: false } }
      },
      {
        path: 'create',
        canActivate: [AuthGuardService],
        component: InstituteTypeFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        canActivate: [AuthGuardService],
        component: InstituteTypeFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        canActivate: [AuthGuardService],
        component: InstituteTypeFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'campusAcademicCalendars',
    data: { breadcrumb: { title: 'Campus Academic Calendar', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: CampusAcademicCalendarListComponent,
        data: { breadcrumb: { title: 'Campus Academic Calendar', display: false } }
      },
      {
        path: 'create',
        canActivate: [AuthGuardService],
        component: CampusAcademicCalendarFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        canActivate: [AuthGuardService],
        component: CampusAcademicCalendarViewComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        canActivate: [AuthGuardService],
        component: CampusAcademicCalendarUpdateComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstituteRoutes { }
