export const InstituteMenu = [
  {
    heading: 'Institute',
    menuItems: [
      { title: 'Dashboard', url: ['/institute/dashboard'], perm: 'institutes.index' },
      { title: 'Institute Types', url: ['/institute/instituteTypes'], perm: 'instituteTypes.index' },
      { title: 'Level of Education', url: ['/institute/educationLevels'], icon: 'view_list', perm: 'educationLevels.index' },
      { title: 'Campus Academic Calendars', url: ['/institute/campusAcademicCalendars'], perm: 'campusAcademicCalendars.index' },
    ]
  }
];
