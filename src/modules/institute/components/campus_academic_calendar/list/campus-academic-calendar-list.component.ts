import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';

import { PageAnimation } from 'modules/shared/helper';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { LayoutService } from 'modules/layout/services';
import { CampusAcademicCalendarService } from 'modules/institute/services';
import { CampusAcademicCalendarModel } from 'modules/institute/models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'institute-campus-academic-calendar-list',
  templateUrl: './campus-academic-calendar-list.component.html',
  styleUrls: ['./campus-academic-calendar-list.component.css'],
  animations: [PageAnimation]
})
export class CampusAcademicCalendarListComponent implements OnInit {
  public pageState;

  public loaded: boolean = false;

  public data: CampusAcademicCalendarModel[] = [new CampusAcademicCalendarModel()];
  public attrLabels = CampusAcademicCalendarModel.attributesLabels;

  displayedColumns = ['campusName', 'count', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;



  constructor(
    private campusAcademicCalendarService: CampusAcademicCalendarService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Campus Academic Calendar' });
    this.getRecords();
  }

  /**
   * Get records
   */
  getRecords() {
    this.campusAcademicCalendarService.index().subscribe(
      response => {
        this.data = response;

        this.dataSource = new MatTableDataSource<CampusAcademicCalendarModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
    return this.data;
  }

  /**
   * Apply filter and serch in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete record on the basis of campusId
   */
  deleteCampusAcademicCalendar(id: number) {
    // Confirm dialog
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: 'Are you Sure, You want to delete?' }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
    this.loaded = false;
    this.pageState = '';

    this.campusAcademicCalendarService.deleteAll(+id).subscribe(
      response => {

        if (response && (response['children']) ? response['children'].length > 0 : false) {

          this.loaded = true;
          this.pageState = 'active';

          this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

        } else {

          this.getRecords();
          this.layoutService.flashMsg({ msg: 'Campus Academic Calendar has been deleted.', msgType: 'success' });

        }

      },
      error => {
        console.log(error);
        this.loaded = true;
      }
    );
      }
    });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }

}
