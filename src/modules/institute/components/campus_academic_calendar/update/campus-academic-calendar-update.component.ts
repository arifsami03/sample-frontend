import { Promise } from 'bluebird';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

import { PageAnimation } from '../../../../shared/helper';
import { CampusAcademicCalendarService } from '../../../services';
import { AcademicCalendarProgramDetailService } from '../../../../academic/services';
import { LayoutService } from '../../../../layout/services';
import { CampusAcademicCalendarModel } from '../../../models';
import { AcademicCalendarProgramDetailModel } from '../../../../academic/models';
import { GLOBALS } from '../../../../app/config/globals';
import { CampusService } from '../../../../campus/services';
import { CampusModel } from '../../../../campus/models';

// for date picker format
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
// import { resolve } from 'q';
const moment = _moment;
const MY_FORMATS = {
  parse: {
    dateInput: 'LL'
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};

@Component({
  selector: 'institute-campus-academic-calendar-update',
  templateUrl: './campus-academic-calendar-update.component.html',
  styleUrls: ['./campus-academic-calendar-update.component.css'],
  animations: [PageAnimation],
  providers: [CampusService,AcademicCalendarProgramDetailService, { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] }, { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }]
})


export class CampusAcademicCalendarUpdateComponent implements OnInit {
  public pageState = 'active';

  public loaded: boolean = false;
  public boxLoaded = true;
  public pageActions = GLOBALS.pageActions;
  public error: Boolean;
  public fg: FormGroup;
  public campusAcademicCalendar: CampusAcademicCalendarModel;
  public id: number;
  public componentLabels = CampusAcademicCalendarModel.attributesLabels;

  public academicCalendarProgramDetails: AcademicCalendarProgramDetailModel[] = [];
  public campus: CampusModel;
  public campusAcademicCalendars: CampusAcademicCalendarModel[];
  public selectedAcademicCalendarProgramDetails: AcademicCalendarProgramDetailModel[] = [];


  public selectedCampus: CampusModel;
  public patchedObject: any;
  public mapDates: any[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private campusAcademicCalendarService: CampusAcademicCalendarService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private campusService: CampusService,
    private academicCalendarProgramDetailService: AcademicCalendarProgramDetailService
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage()
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new CampusAcademicCalendarModel().validationRules(), { mapDates: this.fb.array([this.initMapDates()]) });
    this.getData();
  }

  /**
   * initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Campus Academic Calendar Mapping : ' + this.selectedCampus.campusName,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Campus Academic Calendar Mapping ', url: '/institute/campusAcademicCalendars' },
        {
          label: this.selectedCampus.campusName,
          url: `/institute/campusAcademicCalendars/view/${this.selectedCampus.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * get all data
   */
  private getData() {

    this.route.params.subscribe(params => {

      this.campusAcademicCalendarService.findWithAcademicCalendars(params['id']).subscribe(

        response => {

          this.campusAcademicCalendars = response;
          this.findCampus();
          this.findAcademicCalendarProgramDetailByCampusProgramDetails([+params['id']]);
        },

        error => {

          console.log(error);
          this.loaded = true;

        },

        () => {
          this.loaded = true;
        }

      );

    });
  }

  /**
   * Add or update data in database when save button is clicked
   */
  saveData(item: CampusAcademicCalendarModel) {

    this.boxLoaded = false;

    this.validate().then(result => {

      if (result === 0) {

        this.campusAcademicCalendarService.updateWithCampusAndAcademicCalendarProgramDetails(this.selectedCampus.id, item).subscribe(

          () => {

            this.router.navigate([`/institute/campusAcademicCalendars/view/${this.selectedCampus.id}`]);
            this.layoutService.flashMsg({ msg: 'Academic Calendar has been updated.', msgType: 'success' });

          },

          error => {

            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;

          },

          () => {

            this.boxLoaded = true;

          }
        );
      }
    });
  }

  /**
   * Finding campuses list
   */
  findCampus() {

    this.route.params.subscribe(params => {

      this.campusService.findAttributesData(params['id']).subscribe(

        response => {

          this.selectedCampus = response;
          this.fg.controls['campusId'].setValue(this.selectedCampus.id);
          this.initUpdatePage();

        },

        error => console.log(error),

        () => {

          this.loaded = true;

        }

      );

    });
  }

  /**
   *  Getting desired object by sending array and id
   * @param arrayOfObjects
   * @param id
   */
  getDesiredObject(arrayOfObjects: any, id: number) {
    return arrayOfObjects.find(x => x.id === id);
  }

  /**
   * Find academic calendar prgram detail on the basis of selected ids
   */
  findInAcademicCalendarProgramDetail() {
  
    // getting the array of academicCalendarProgramDetailIds
    let academicCalendarProgramDetailIds: number[] = this.fg.get('academicCalendarProgramDetailId').value;

    // matching given campusIds with selected
    academicCalendarProgramDetailIds.forEach(academicCalendarProgramDetailId => {
  
      // if not exist then push into the array of selected academic calendar program details
      if (!this.getDesiredObject(this.selectedAcademicCalendarProgramDetails, academicCalendarProgramDetailId)) {
  
        let acpd = this.getDesiredObject(this.academicCalendarProgramDetails, academicCalendarProgramDetailId);
  
        // adding disbaled property
        if (acpd['startDate'] && acpd['endDate']) {
  
          acpd['isDisable'] = true;
  
        }
  
        this.selectedAcademicCalendarProgramDetails.push(acpd);
        this.addMapDates();
  
        // pushing in to mapDates array for patchin after ward
        this.mapDates.push({ startDate: acpd['startDate'], endDate: acpd['endDate'] });
  
      }
  
    });

    
    // patching values
    this.fg.patchValue({ mapDates: this.mapDates });
  
    // remove it by searching on the basis of index
    for (let i = 0; i < this.selectedAcademicCalendarProgramDetails.length; i++) {
  
      let isRemove = true;

      for (let j = 0; j < academicCalendarProgramDetailIds.length; j++) {
  
        if (this.selectedAcademicCalendarProgramDetails[i].id === academicCalendarProgramDetailIds[j]) {
          isRemove = false;
          break;
  
        }
      }
  
      if (isRemove) {

        this.selectedAcademicCalendarProgramDetails.splice(i, 1);
        this.removeMapDates(i);
      
      }
    }
   
  }

  initMapDates() {
    // initialize our date
    return this.fb.group({
    
      startDate: new FormControl(moment(''), [Validators.required]),
      endDate: new FormControl(moment(''), [Validators.required])
    
    });
  }
  addMapDates() {
  
    // add date to the list
    let startDates = this.fg.get('mapDates') as FormArray;
    startDates.push(this.initMapDates());
  
  }

  removeMapDates(i: number) {
  
    // remove date from the list
    const control = <FormArray>this.fg.controls['mapDates'];
    control.removeAt(i);
  
  }


  /**
   * Validating if any of the academic calendar program details  already exist against campus then plus the counter and
   * validate on the basis of that counter
   */
  validate() {
    
    return new Promise((resolve, reject) => {
      
      let count = 0;
  
      // itearte each selected academic calendar program detail
      this.selectedAcademicCalendarProgramDetails.forEach((selectedAcademicCalendarProgramDetail, index) => {
    
        if (!this.isValidDates(index)) {
    
          count++;
    
        }
        resolve(count);
    
      });
    
    });
  }

  /**
   * Validating
   * @param index index of map dates
   */
  isValidDates(index: number) {
  
    let startDate = this.fg.controls['mapDates']['controls'][index]['controls']['startDate']['value'];
    let endDate = this.fg.controls['mapDates']['controls'][index]['controls']['endDate']['value'];
  
    if (endDate < startDate) {
  
      this.fg.controls['mapDates']['controls'][index]['controls']['endDate'].setErrors({ invalid: true });
      return false;
  
    } else {
  
      return true;
  
    }
  }

  /**
   * Preparimg path object for patching the values 
   */
  pathchingValues() {
  
    let academicCalendarProgramDetailId: number[] = [];
    this.campusAcademicCalendars.forEach(campusAcademicCalendar => {
  
      // setting selected academic calendar program details into the array
      let acpd = this.getDesiredObject(this.academicCalendarProgramDetails, campusAcademicCalendar.academicCalendarProgramDetailId);
      // adding disbaled property
      if (acpd['startDate'] && acpd['endDate']) {
        acpd['isDisable'] = true;
      }
  
      this.selectedAcademicCalendarProgramDetails.push(acpd);
      // adding mapDates  
      this.addMapDates();
      academicCalendarProgramDetailId.push(campusAcademicCalendar.academicCalendarProgramDetailId);
      this.mapDates.push({ startDate: campusAcademicCalendar.academicCalendarProgramDetail.startDate, endDate: campusAcademicCalendar.academicCalendarProgramDetail.endDate })
  
    });
  
    // Preparing patch object
    this.patchedObject = {
      academicCalendarProgramDetailId: academicCalendarProgramDetailId,
      mapDates: this.mapDates
    }
  }

  /**
   * Finding academic calendar program details list
   */
  findAcademicCalendarProgramDetailByCampusProgramDetails(campusIds : number[]) {

    this.academicCalendarProgramDetailService.findByCampusProgramDetails({campusIds : campusIds}).subscribe(

      response => {
        this.academicCalendarProgramDetails = response;
        this.pathchingValues();
        this.fg.patchValue(this.patchedObject);
      },

      error => console.log(error),

      () => {

        this.loaded = true;

      }
    );
  }

}
