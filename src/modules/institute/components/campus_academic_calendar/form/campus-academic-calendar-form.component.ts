import { Promise } from 'bluebird';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

import { PageAnimation } from '../../../../shared/helper';
import { CampusAcademicCalendarService } from '../../../services';
import { AcademicCalendarProgramDetailService } from '../../../../academic/services';
import { LayoutService } from '../../../../layout/services';
import { CampusAcademicCalendarModel } from '../../../models';
import { AcademicCalendarProgramDetailModel } from '../../../../academic/models';
import { GLOBALS } from '../../../../app/config/globals';
import { CampusService } from '../../../../campus/services';
import { CampusModel } from '../../../../campus/models';

// for date picker format
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';

//TODO:low: why we need to have constant out of component class?
const moment = _moment;

//TODO:high: date format should be in global file from where it should be utilized as one single setting for entire app
const MY_FORMATS = {
  parse: {
    dateInput: 'LL'
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};

@Component({
  selector: 'institute-campus-academic-calendar-form',
  templateUrl: './campus-academic-calendar-form.component.html',
  styleUrls: ['./campus-academic-calendar-form.component.css'],
  animations: [PageAnimation],
  //TODO:high: providers regarding date adapter should be in moudle.
  providers: [CampusService, AcademicCalendarProgramDetailService, { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] }, { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }]
})
export class CampusAcademicCalendarFormComponent implements OnInit {
  public pageState = 'active';

  public loaded: boolean = false;
  public boxLoaded = true;
  public pageActions = GLOBALS.pageActions;
  public error: Boolean;
  public fg: FormGroup;
  public id: number;
  public componentLabels = CampusAcademicCalendarModel.attributesLabels;

  public academicCalendarProgramDetails: AcademicCalendarProgramDetailModel[] = [];
  public campuses: CampusModel[] = [];
  public campusAcademicCalendar: CampusAcademicCalendarModel;

  public mapDates: any[] = [];

  public selectedAcademicCalendarProgramDetails: AcademicCalendarProgramDetailModel[] = [];
  public selectedCampuses: CampusModel[] = [];
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private campusAcademicCalendarService: CampusAcademicCalendarService,
    private layoutService: LayoutService,
    private campusService: CampusService,
    private academicCalendarProgramDetailService: AcademicCalendarProgramDetailService
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {

    this.initializePage();
    this.findCreateCampusAttributesList();

  }

  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new CampusAcademicCalendarModel().validationRules(), { mapDates: this.fb.array([this.initMapDates()]) });
    this.initCreatePage();
  }

  /**
   * initialize create page
   */
  private initCreatePage() {

    this.layoutService.setPageTitle({ title: 'Campus Academic Calendar Mapping : Create' });
    this.campusAcademicCalendar = new CampusAcademicCalendarModel();
    this.fg.enable();
    this.loaded = true;
  }

  /**
   * Add or update data in database when save button is clicked
   */

  saveData(item: CampusAcademicCalendarModel) {
    this.boxLoaded = false;

    this.validate().then(result => {
      if (result === 0) {

        this.campusAcademicCalendarService.createWithCampusAndAcademicCalendarProgramDetails(item).subscribe(

          () => {

            this.router.navigate([`/institute/campusAcademicCalendars`]);
            this.layoutService.flashMsg({ msg: 'Academic Calendar has been created.', msgType: 'success' });

          },

          error => {

            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;

          },
          () => {

            this.boxLoaded = true;

          }
        );
      }
    });
  }

  /**
   * Finding campuses list
   */
  findCreateCampusAttributesList() {

    this.campusService.findAllCampuses().subscribe(

      response => {

        this.campuses = response;

      },

      error => console.log(error),

      () => {

        this.loaded = true;

      }
    );
  }

  /**
   * Finding academic calendar program details list
   */
  findAcademicCalendarProgramDetailByCampusProgramDetails(campusIds : number[]) {

    this.academicCalendarProgramDetailService.findByCampusProgramDetails({campusIds : campusIds}).subscribe(

      response => {
        this.academicCalendarProgramDetails = response;

      },

      error => console.log(error),

      () => {

        this.loaded = true;

      }
    );
  }

  /**
   *  Getting desired object by sending array and id
   * @param arrayOfObjects
   * @param id
   */
  getDesiredObject(arrayOfObjects: any, id: number) {
    return arrayOfObjects.find(x => x.id === id);
  }

  /**
   * Find Campus on the basis of selected campus
   */
  findInCampus() {
    // getting the array of campus ids
    let campusIds: number[] = this.fg.get('campusId').value;

    // matching given campusIds with selected
    campusIds.forEach(campusId => {

      // if not exist then push into the array of selected campuses
      if (!this.getDesiredObject(this.selectedCampuses, campusId)) {

        this.selectedCampuses.push(this.getDesiredObject(this.campuses, campusId));

      }
    });

    // remove it by searching on the basis of index
    for (let i = 0; i < this.selectedCampuses.length; i++) {
      let isRemove = true;

      for (let j = 0; j < campusIds.length; j++) {

        if (this.selectedCampuses[i].id === campusIds[j]) {

          isRemove = false;
          break;

        }

      }
      if (isRemove) {

        this.selectedCampuses.splice(i, 1);

      }
    }


    // 
    this.findAcademicCalendarProgramDetailByCampusProgramDetails(this.fg.get('campusId').value);

  }

  /**
   * Find academic calendar prgram detail on the basis of selected ids
   */
  findInAcademicCalendarProgramDetail() {

    // getting the array of academicCalendarProgramDetailIds
    let academicCalendarProgramDetailIds: number[] = this.fg.get('academicCalendarProgramDetailId').value;

    // matching given campusIds with selected
    academicCalendarProgramDetailIds.forEach(academicCalendarProgramDetailId => {

      // if not exist then push into the array of selected academic calendar program details
      if (!this.getDesiredObject(this.selectedAcademicCalendarProgramDetails, academicCalendarProgramDetailId)) {
        let acpd = this.getDesiredObject(this.academicCalendarProgramDetails, academicCalendarProgramDetailId);

        // adding disbaled property
        if (acpd['startDate'] && acpd['endDate']) {

          acpd['isDisable'] = true;

        }

        this.selectedAcademicCalendarProgramDetails.push(acpd);
        this.addMapDates();

        // pushing in to mapDates array for patching after ward
        this.mapDates.push({ startDate: acpd['startDate'], endDate: acpd['endDate'] });
      }
    });

    // patching values
    this.fg.patchValue({ mapDates: this.mapDates });

    // remove it by searching on the basis of index
    for (let i = 0; i < this.selectedAcademicCalendarProgramDetails.length; i++) {

      let isRemove = true;

      for (let j = 0; j < academicCalendarProgramDetailIds.length; j++) {

        if (this.selectedAcademicCalendarProgramDetails[i].id === academicCalendarProgramDetailIds[j]) {

          isRemove = false;
          break;

        }

      }
      if (isRemove) {

        this.selectedAcademicCalendarProgramDetails.splice(i, 1);

        // removing from patch value object to 
        this.mapDates.splice(i, 1);

        this.removeMapDates(i);

      }
    }
  }

  initMapDates() {
    // initialize our date

    return this.fb.group({
      startDate: new FormControl(moment(''), [Validators.required]),
      endDate: new FormControl(moment(''), [Validators.required])
    });

  }

  addMapDates() {
    // add date to the list
    let startDates = this.fg.get('mapDates') as FormArray;
    startDates.push(this.initMapDates());

  }

  removeMapDates(i: number) {

    // remove date from the list
    const control = <FormArray>this.fg.controls['mapDates'];
    control.removeAt(i);

  }

  /**
   * finding the whether or not academic calendar program details exist against campus
   * @param cId campusId
   * @param acpdId academic calendar program detail id
   * @param index index of selected academic calendar program details
   */
  findAlreadyAssignedACPD(cId, acpdId, index) {

    return new Promise((resolve, reject) => {
      this.campusAcademicCalendarService.findByCampusAndAcademicCalendarProgramDetail(cId, acpdId).subscribe(

        response => {
          if (response) {

            // setting the error
            this.selectedAcademicCalendarProgramDetails[index]['error'] = true;
            resolve(true);

          } else {

            resolve(false);

          }
        },

        error => {

          reject(false);
          console.log(error);

        },

        () => { }
      );
    });
  }

  /**
   * Validating if any of the academic calendar program details  already exist against campus then plus the counter and
   * validate on the basis of that counter
   */
  validate() {

    return new Promise((resolve, reject) => {
      let count = 0;

      // iterate each selected campus
      return Promise.each(this.selectedCampuses, selectedCampus => {

        // itearte each selected academic calendar program detail
        return Promise.each((this.selectedAcademicCalendarProgramDetails), (selectedAcademicCalendarProgramDetail, index) => {
          return this.findAlreadyAssignedACPD(selectedCampus.id, selectedAcademicCalendarProgramDetail.id, index).then(result => {

            if (result || !this.isValidDates(index)) {
              count++;
            }

          });

        });

      }).then(() => {
        resolve(count);
      });

    });


  }

  /**
   * Validating
   * @param index index of map dates
   */
  isValidDates(index: number) {

    let startDate = this.fg.controls['mapDates']['controls'][index]['controls']['startDate']['value'];
    let endDate = this.fg.controls['mapDates']['controls'][index]['controls']['endDate']['value'];

    if (endDate < startDate) {

      this.fg.controls['mapDates']['controls'][index]['controls']['endDate'].setErrors({ invalid: true });
      return false;

    } else {

      return true;

    }
  }
}
