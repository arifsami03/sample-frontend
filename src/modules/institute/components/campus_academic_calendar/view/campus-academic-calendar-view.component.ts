import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { PageAnimation } from '../../../../shared/helper';
import { CampusAcademicCalendarService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { CampusAcademicCalendarModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';
import { CampusModel } from '../../../../campus/models';

@Component({
  selector: 'institute-campus-academic-calendar-view',
  templateUrl: './campus-academic-calendar-view.component.html',
  styleUrls: ['./campus-academic-calendar-view.component.css'],
  animations: [PageAnimation]
})
export class CampusAcademicCalendarViewComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public pageActions = GLOBALS.pageActions;


  public error: Boolean;

  public fg: FormGroup;
  public campusAcademicCalendars: CampusAcademicCalendarModel[];
  public campusAcademicCalendar: CampusAcademicCalendarModel;
  public id: number;
  public attrLabels = CampusAcademicCalendarModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private campusAcademicCalendarService: CampusAcademicCalendarService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
  }

  public campus: CampusModel

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new CampusAcademicCalendarModel().validationRules());
    this.getData();

  }

  /**
   * get all data
   */
  private getData() {

    this.route.params.subscribe(params => {

      this.campusAcademicCalendarService.findWithAcademicCalendars(params['id']).subscribe(

        response => {
          if (response && response.length) {

            this.campusAcademicCalendars = response;
            console.log('Campus Academic Calendars : ',this.campusAcademicCalendars)
            this.campusAcademicCalendar = this.campusAcademicCalendars[0];
            this.initViewPage()

          }else{
            this.router.navigate([`/institute/campusAcademicCalendars`]);
          }
        },

        error => {

          console.log(error);
          this.loaded = true;

        },

        () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * initialize view page
   */
  private initViewPage() {

    // this.fg.disable();
    this.layoutService.setPageTitle({
      title: 'Campus Academic Calendar: ' + this.campusAcademicCalendar.campus.campusName,
      breadCrumbs: [{ label: 'Home', url: '/home/dashboard' }, { label: 'Campus Academic Calendar ', url: '/institute/campusAcademicCalendars' }, { label: this.campusAcademicCalendar.campus.campusName }]
    });

  }

  /**
   * delete record
   */
  deleteRecord(campusAcademicCalendarId: number) {

   
    this.matDialog
    .open(ConfirmDialogComponent, {

      width: GLOBALS.deleteDialog.width,
      data: { message: 'Are you Sure?' }

    })
    .afterClosed()
    .subscribe((accept: boolean) => {
      if (accept) {

        this.loaded = false;
        this.pageState = '';
        this.campusAcademicCalendarService.deleteAll(campusAcademicCalendarId).subscribe(

          () => {
    
            this.layoutService.flashMsg({ msg: 'Campus Academic Calendars have been deleted.', msgType: 'success' });
            this.router.navigate(['/institute/campusAcademicCalendars']);
    
          },
    
          error => {
    
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
    
          }
        );
      }
    });

   

    
  }

  deleteCampusAcademicCalendar(id: number) {

    this.matDialog
      .open(ConfirmDialogComponent, {

        width: GLOBALS.deleteDialog.width,
        data: { message: 'Are you Sure?' }

      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {

          this.loaded = false;
          this.pageState = '';
          this.campusAcademicCalendarService.delete(id).subscribe(

            () => {
              this.layoutService.flashMsg({ msg: 'Campus Academic Calendar has been deleted.', msgType: 'success' });
              this.getData();
            },

            error => {
              console.log(error);
              this.loaded = true;
              this.pageState = 'active';
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            },

            () => {
              this.loaded = true;
              this.pageState = 'active';
            }

          );
        }
      });
  }


}
