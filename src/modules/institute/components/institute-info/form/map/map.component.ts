import { Observable } from 'rxjs/Observable';
import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AgmCoreModule, MouseEvent } from '@agm/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'


@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  public loaded: boolean = false;
  public id: number;
  public lat: number;
  public lng: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public dialogRef: MatDialogRef<MapComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  /**
   * ngOnInit
   */

  ngOnInit() {
    // 31.461358391483742
    // 74.41597938537598
    this.lat = (this.data.lat != 0 ? this.data.lat : 31.505971031689416);
    this.lng = (this.data.lng != 0 ? this.data.lng : 74.35272216796875);

  }

  mapClicked($event: MouseEvent) {
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;

  }
  locationAdded() {
    let data = {
      lat: this.lat,
      lng: this.lng,
    }
    this.dialogRef.close(data);
  }



}
