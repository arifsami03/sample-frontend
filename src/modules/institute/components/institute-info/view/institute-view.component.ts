import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { AgmCoreModule, MouseEvent } from '@agm/core';
import { ActivatedRoute, Router } from '@angular/router';

import { PageAnimation } from '../../../../shared/helper';
import { LayoutService } from '../../../../layout/services';
import { InstituteModel } from '../../../models';
import { InstituteService } from '../../../services';
import { PrintActionComponent } from '../../../../pdf/components';

@Component({
  selector: 'institute-view',
  templateUrl: './institute-view.component.html',
  styleUrls: ['./institute-view.component.css'],
  animations: [PageAnimation]
})
export class InstituteViewComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public pageAct: string;
  public pageTitle: string = 'Institute Information';
  public institute = new InstituteModel();
  public componentLabels = InstituteModel.attributesLabels;
  public showOnlyFields = ['establishedScince', 'headOfficeLocation', 'officeEmail', 'website', 'Description']
  public title: string = 'My first AGM project';
  public show: string = 'Less';
  public less: string;
  public more: string;
  public checkLength: string;
  public configuration: any = {
    title: 'Institute Report',
    template: 'institute'

  };
  public columns = ["ID", "Name", "Country"];
  public rows = [
    [1, "Shaw", "Tanzania"],
    [2, "Nelson", "Kazakhstan"],
    [3, "Garcia", "Madagascar"],

  ];
  public data: any
  // = {
  //   detailView: this.institute,
  //   gridView: {columns:this.columns, rows:this.rows}
  // };
  zoom: number = 8;

  // initial center position for the map
  lat: number;// = 51.673858;
  lng: number;// = 7.815982;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private layoutService: LayoutService,
    private instituteEditService: InstituteService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */

  ngOnInit() {

    this.layoutService.setPageTitle({ title: this.pageTitle });
    this.initializePage();



  }

  private initializePage() {
    this.getInstituteRecord();


  }

  getInstituteRecord(): void {
    this.instituteEditService.getAll().subscribe(response => {
      this.institute = response;
      this.data = this.institute;
      // {
      //   detailView: this.institute,
      //   gridView: {columns:this.columns, rows:this.rows},
      //   listView:this.institute
      // };
      this.more = this.institute.Description;
      this.showDescription();
      this.lat = this.institute.latitude;
      this.lng = this.institute.longitude;
      //this.loaded = false;
    },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  showDescription() {
    this.checkLength = this.more;
    if (this.checkLength) {
      let arr = this.checkLength.split(' ');
      if (arr.length > 40) {
        if (this.show === 'More') {
          this.show = 'Less';
          this.institute.Description = this.more;
        }
        else if (this.show === 'Less') {
          this.show = 'More';
          this.less = this.institute.Description;
          let arr = this.less.split(' ');
          arr = arr.slice(0, 40);
          let str = arr.toString();
          let result = str.replace(/,/g, " ");
          this.institute.Description = result;
        }
      }
      else {
        this.show = '';
      }
    }
  }

  mapClicked($event: MouseEvent) {
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;

  }



}


