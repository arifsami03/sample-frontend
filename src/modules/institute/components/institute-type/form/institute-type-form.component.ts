import { Component, OnInit,Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PageAnimation } from '../../../../shared/helper';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { InstituteTypeService } from '../../../services';
import { LayoutService } from '../../../../layout/services';

import { ConfirmDialogComponent } from '../../../../shared/components';
import { InstituteTypeModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'institute-institute-type-form',
  templateUrl: './institute-type-form.component.html',
  styleUrls: ['./institute-type-form.component.css'],
  animations: [PageAnimation]
})
export class InstituteTypeFormComponent implements OnInit {

  public pageState = 'active';
  
  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public boxLoaded = true;

  public error: Boolean;
  public check = false;
  public fg: FormGroup;
  public pageAct: string;
  public instituteType: InstituteTypeModel;

  public componentLabels = InstituteTypeModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<InstituteTypeFormComponent>,
    private instituteTypeService: InstituteTypeService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  /**
   * Initialize page
   */
  private initializePage() {

    this.fg = this.fb.group(new InstituteTypeModel().validationRules());

    if (this.data.type === 'Create') {

      this.initCreatePage();

    } else {

      this.getData();

    }

  }

  /**
   * Get record
   */
  private getData() {

    this.route.params.subscribe(params => {

      this.instituteTypeService.find(this.data.id).subscribe(response => {

        if (response) {
          this.instituteType = response;

          this.check = true;
          this.fg.patchValue(this.instituteType);

          
        }

      },
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {

    this.layoutService.setPageTitle({ title: 'Institute Type: Create' });

    this.instituteType = new InstituteTypeModel();

    this.fg.enable();

    this.loaded = true;
  }



  /**
   * Create or Update record in database when save button is clicked
   * 
   */
  public saveData(item: InstituteTypeModel,redirect:boolean) {
    this.boxLoaded = false;
    if (this.data.type === 'Create') {

      this.instituteTypeService.create(item).subscribe(response => {
        this.layoutService.flashMsg({ msg: 'Institute Type has been created.', msgType: 'success' });
        this.dialogRef.close(true);
        if(redirect){
          this.router.navigate([`/institute/dashboard/update`]);
        }

      },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        }, () => {
          this.boxLoaded = true;
        }
      );
    }
    else  {

      this.instituteTypeService.update(this.instituteType.id, item).subscribe(response => {
        this.layoutService.flashMsg({ msg: 'Institute Type has been updated.', msgType: 'success' });
        this.dialogRef.close(true);
        if(redirect){
          this.router.navigate([`/institute/dashboard/update`]);
        }
      },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        }, () => {
          this.boxLoaded = true;
        }
      );
    }
  }

  /**
   * Delete record
   * 
   */
  delete(id: number) {

    this.matDialog.open(ConfirmDialogComponent, {

      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }

    }).afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;

        this.instituteTypeService.delete(id).subscribe(response => {
          this.layoutService.flashMsg({ msg: 'Institute Type has been deleted.', msgType: 'success' });
          this.router.navigate(['/institute/instituteTypes']);
        }, error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        });
      }
    });
  }

  noClick(){
    this.dialogRef.close(false);
  }
}
