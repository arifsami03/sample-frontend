import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { PageAnimation } from '../../../../shared/helper';
import { InstituteTypeFormComponent } from '../form/institute-type-form.component';
import { LayoutService } from 'modules/layout/services';
import { InstituteTypeService } from 'modules/institute/services';
import { InstituteTypeModel } from 'modules/institute/models';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { GLOBALS } from 'modules/app/config/globals';


@Component({
  selector: 'institute-institute-type-list',
  templateUrl: './institute-type-list.component.html',
  styleUrls: ['./institute-type-list.component.css'],
  animations: [PageAnimation]
})
export class InstituteTypeListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public data: InstituteTypeModel[] = [new InstituteTypeModel()];

  public attrLabels = InstituteTypeModel.attributesLabels;

  displayedColumns = ['name', 'options'];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public error: Boolean;

  constructor(
    private instituteTypeService: InstituteTypeService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Institute Type' });

    this.getRecords();
  }

  /**
   * Get data/records from backend
   */
  getRecords() {

    this.instituteTypeService.index().subscribe(response => {

      this.data = response;

      this.dataSource = new MatTableDataSource<InstituteTypeModel>(this.data);

      this.dataSource.paginator = this.paginator;

      this.dataSource.sort = this.sort;

    },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {

    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;

  }

  /**
   * Delete record
   */
  delete(id: number) {

    // Confirm dialog
    this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    }).afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.instituteTypeService.delete(id).subscribe(
          response => {
            if (response && (response['children']) ? response['children'].length > 0 : false) {

              this.loaded = true;
              this.pageState = 'active';

              this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

            } else {

              this.getRecords();
              this.layoutService.flashMsg({ msg: 'Institute Type has been deleted.', msgType: 'success' });

            }

          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
            this.loaded = true;
            this.pageState = 'active';
          }
        );
      }
    });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }

  addClick() {

    let dialogRef = this.dialog.open(InstituteTypeFormComponent, {
      width: '400px',
      // height: '400px',
      data: { type: 'Create' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getRecords();
      }
    });


  }
  editClick(id) {

    let dialogRef = this.dialog.open(InstituteTypeFormComponent, {
      width: '400px',
      data: { type: 'Update', id: id }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getRecords();
      }
    });
  }

}
