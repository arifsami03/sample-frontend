export * from './institute-info/form/institute.component';
export * from './institute-info/view/institute-view.component';
export * from './institute-info/form/map/map.component';


//TODO:low directory name should be education_level instead of education-level
export * from './education-level/form/education-level-form.component';
export * from './education-level/list/education-level-list.component';

export * from './institute-type/form/institute-type-form.component';
export * from './institute-type/list/institute-type-list.component';

/**
 * Campus Academic Calendar
 */
export * from './campus_academic_calendar/form/campus-academic-calendar-form.component';
export * from './campus_academic_calendar/list/campus-academic-calendar-list.component';
export * from './campus_academic_calendar/view/campus-academic-calendar-view.component';
export * from './campus_academic_calendar/update/campus-academic-calendar-update.component';




