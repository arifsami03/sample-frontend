import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { PageAnimation } from '../../../../shared/helper';

import { EducationLevelService } from '../../../services';
import { InstituteTypeService } from '../../../services';
import { LayoutService } from '../../../../layout/services';

import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { EducationLevelModel } from 'modules/institute/models';
import { InstituteTypeModel } from 'modules/institute/models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'institute-education-level-form',
  templateUrl: './education-level-form.component.html',
  styleUrls: ['./education-level-form.component.css'],
  animations: [PageAnimation]
})
export class EducationLevelFormComponent implements OnInit {

  public pageState = 'active';

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public boxLoaded = true;

  public error: Boolean;

  public fg: FormGroup;
  public pageAct: string;
  public educationLevel: EducationLevelModel;

  public instituteTypes: InstituteTypeModel[];

  public componentLabels = EducationLevelModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private eduLevelService: EducationLevelService,
    private instituteTypeService: InstituteTypeService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  /**
   * Initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new EducationLevelModel().validationRules());
    this.findAttributesList();

    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get record
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.eduLevelService.find(params['id']).subscribe(
        response => {
          if (response) {
            this.educationLevel = response;

            this.fg.patchValue(this.educationLevel);

            //TODO:low need to check why we are dowing this after fetching data
            if (this.pageAct == this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct == this.pageActions.update) {
              this.initUpdatePage();
            }
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }
  /**
   * Get Institute Type List
   */
  findAttributesList() {
    this.instituteTypeService.findAttributesList().subscribe(
      response => {
        this.instituteTypes = response;
      },
      error => console.log(error),
      () => { }
    );
  }

  /**
   * Init create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Level of Education: Create' });

    this.educationLevel = new EducationLevelModel();

    this.fg.enable();

    this.loaded = true;
  }

  /**
   * Initialize the view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Level of Education: ' + this.educationLevel.education,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Level of Education ',
          url: '/institute/educationLevels'
        },
        { label: this.educationLevel.education }
      ]
    });
  }

  /**
   * Initialize the update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Level of Education: ' + this.educationLevel.education,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Level of Education', url: '/institute/educationLevels' },
        {
          label: this.educationLevel.education,
          url: `/institute/educationLevels/view/${this.educationLevel.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: EducationLevelModel) {
    this.boxLoaded = false;
    if (this.pageAct === this.pageActions.create) {
      this.eduLevelService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Level of Education has been created.', msgType: 'success' });
          this.router.navigate([`/institute/educationLevels/`]);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        },
        () => {
          this.boxLoaded = true;
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.eduLevelService.update(this.educationLevel.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Level of Education has been updated.', msgType: 'success' });
          this.router.navigate([`/institute/educationLevels/`]);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        },
        () => {
          this.boxLoaded = true;
        }
      );
    }
  }

  /**
   * Delete record
   *
   */
  delete(id: number) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, { width: GLOBALS.deleteDialog.width, data: { message: GLOBALS.deleteDialog.message } });

    dialogRef.afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.eduLevelService.delete(id).subscribe(
          response => {

            if (response && (response['children']) ? response['children'].length > 0 : false) {

              this.loaded = true;
              this.pageState = 'active';

              this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

            } else {

              this.router.navigate(['/institute/educationLevels']);
              this.layoutService.flashMsg({ msg: 'Level of Education has been deleted.', msgType: 'success' });

            }

          },
          error => {
            this.loaded = true;
            this.pageState = 'active';
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });

          }
        );
      }
    });
  }

}
