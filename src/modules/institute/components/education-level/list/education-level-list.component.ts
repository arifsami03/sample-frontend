import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { PageAnimation } from 'modules/shared/helper';
import { GLOBALS } from 'modules/app/config/globals';
import { ConfirmDialogComponent, DisplayARecordsDialogComponent } from 'modules/shared/components';
import { LayoutService } from 'modules/layout/services';
import { EducationLevelService } from 'modules/institute/services';
import { EducationLevelModel } from 'modules/institute/models';

@Component({
  selector: 'institute-education-level-list',
  templateUrl: './education-level-list.component.html',
  styleUrls: ['./education-level-list.component.css'],
  animations: [PageAnimation]
})
export class EducationLevelListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public data: EducationLevelModel[] = [new EducationLevelModel()];

  public attrLabels = EducationLevelModel.attributesLabels;

  displayedColumns = [
    // 'instituteTypeId',
    'education',
    'options'
  ];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  public error: Boolean;

  constructor(private eduLevelService: EducationLevelService, public matDialog: MatDialog, private router: Router, public layoutService: LayoutService) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Level of Education' });

    this.getRecords();
  }

  /**
   * Get data/records from backend
   */
  getRecords() {
    this.eduLevelService.index().subscribe(
      response => {
        if (response) {
          this.data = response;
          this.dataSource = new MatTableDataSource<EducationLevelModel>(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete record
   */
  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, { width: GLOBALS.deleteDialog.width, data: { message: GLOBALS.deleteDialog.message } });

    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {

        this.loaded = false;
        this.pageState = '';

        this.eduLevelService.delete(id).subscribe(
          response => {

            if (response && (response['children']) ? response['children'].length > 0 : false) {

              this.loaded = true;
              this.pageState = 'active';

              this.matDialog.open(DisplayARecordsDialogComponent, { width: GLOBALS.displayARecordDialog.width, data: response });

            } else {

              this.getRecords();
              this.layoutService.flashMsg({ msg: 'Level of Education has been deleted.', msgType: 'success' });

            }
          },
          error => {
            this.loaded = true;
            this.pageState = 'active';
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          }
        );
      }
    });
  }

  sortData() {
    this.dataSource.sort = this.sort;
  }

}

