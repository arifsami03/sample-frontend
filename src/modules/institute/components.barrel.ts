/**
 * Import angular, material and other related modules here
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatTableModule,
  MatSelectModule,
  MatMenuModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatListModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatTabsModule,
  MatChipsModule,
  MatProgressBarModule,
  MatSortModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TextMaskModule } from 'angular2-text-mask';
import 'hammerjs';
import { AgmCoreModule } from '@agm/core';

/**
 * Import Self created components, model, services etc here.
 */
import {
  InstituteService, EducationLevelService, InstituteTypeService, CampusAcademicCalendarService,
} from './services';

import {
  InstituteComponent, InstituteViewComponent, MapComponent, EducationLevelFormComponent, EducationLevelListComponent,
  InstituteTypeFormComponent, InstituteTypeListComponent, CampusAcademicCalendarFormComponent, CampusAcademicCalendarListComponent,
  CampusAcademicCalendarViewComponent, CampusAcademicCalendarUpdateComponent,
} from './components';

/**
 * Export consts here.
 */
export const __IMPORTS = [
  CommonModule,
  HttpModule,
  HttpClientModule,
  MatInputModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatTableModule,
  FormsModule,
  ReactiveFormsModule,
  MatSelectModule,
  MatMenuModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatListModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatFormFieldModule,
  FlexLayoutModule,
  MatTabsModule,
  TextMaskModule,
  MatChipsModule,
  MatProgressBarModule,
  MatSortModule,
  AgmCoreModule.forRoot({
    apiKey: 'AIzaSyAaYDoRGc-X4Q5DPpCBxqrbgx5XOp8YisE'
  })
];

export const __DECLARATIONS = [
  InstituteComponent,
  InstituteViewComponent,
  MapComponent,
  EducationLevelFormComponent,

  EducationLevelListComponent,
  InstituteTypeFormComponent,
  InstituteTypeListComponent,
  CampusAcademicCalendarFormComponent,
  CampusAcademicCalendarListComponent,
  CampusAcademicCalendarViewComponent,
  CampusAcademicCalendarUpdateComponent,

];

export const __PROVIDERS = [
  InstituteService, EducationLevelService, InstituteTypeService, CampusAcademicCalendarService,
];

export const __ENTRY_COMPONENTS = [MapComponent,InstituteTypeFormComponent];
