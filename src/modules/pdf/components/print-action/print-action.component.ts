var jsPDF = require('jspdf');
require('jspdf-autotable');

import { Component, Inject, Input, Output, EventEmitter } from '@angular/core';
import { PDFReport, InstituteTemplate, CampusPostRegistrationTemplate } from '../../helper';

import { DatePipe, CurrencyPipe, DecimalPipe } from '@angular/common';

@Component({
  selector: 'print-action',
  templateUrl: './print-action.component.html',
  styleUrls: ['./print-action.component.css']
})
export class PrintActionComponent {

  @Input() configuration: any;
  @Input() data: any;
  @Input() labels: any;
  @Input() fields: any;
  public generateRpt = new PDFReport();

  constructor() { }

  ngOnInit(): void {

  }

  actionClick() {

    var doc = new jsPDF('portrait');
    var date = new Date().toLocaleString();

    var y = 10;

    if (this.configuration.template == 'institute') {
      this.generateRpt.data = this.data;
      this.generateRpt.template = new InstituteTemplate(doc, this.generateRpt.data);
      this.generateRpt.template.generatePdf(this.configuration, this.labels, this.fields);
    }
    else if (this.configuration.template == 'campusPostRegistration') {
      this.generateRpt.data = this.data;
      this.generateRpt.template = new CampusPostRegistrationTemplate(doc, this.generateRpt.data);
      this.generateRpt.template.generatePdf(this.configuration, this.labels, this.fields);
    }






    //doc.autoPrint();
    doc.save(`${this.configuration.template}_${date}.pdf`);

  }

}
