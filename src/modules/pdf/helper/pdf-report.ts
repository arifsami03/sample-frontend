export class PDFReport  {

  public templateMapping;
  public template;
  public data;

}


/*

PDFReport: 
Properties: 
  1. Template
    a. Placeholders
      1. Logo
      2. Report Title
      3. Section Heading
      4. Section Data
      5. Footer
    b. Settings (colors, fonts etc.)
  2. data 
  3. template-mapping
    a. placeholder -> data sections

Methods:
  1. PDF Stream
  2. Return PDF as download or print
  3. 


 let pdfr = new PDFReport();
 let doc = new jspdf();
 pdfr.data = JSON got from service
 pdfr.dataMapping = {}
 pdfr.template = new InstituteTemplate(doc, data)



*/


