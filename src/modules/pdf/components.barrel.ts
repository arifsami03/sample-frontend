// Import components and services etc here

import {
  PrintActionComponent
} from './components';
import { MatButtonModule, } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

export const __IMPORTS = [
  MatButtonModule,
  FlexLayoutModule,
];

export const __DECLARATIONS = [
  PrintActionComponent
];



export const __PROVIDERS = [];

export const __ENTRY_COMPONENTS = [];
