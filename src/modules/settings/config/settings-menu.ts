export const SettingMenu = [
  {
    heading: 'Settings',
    menuItems: [
      // { title: 'Dashboard', url: ['/settings/dashboard'], perm: 'n/a' },
      { title: 'App Configurations', url: ['/settings/configurations/singleConf'], perm: 'configurations.index' },
      { title: 'Nature of Work', url: ['/settings/configurations/natureofwork'], perm: 'configurations.index' },
      { title: 'Instrument Types', url: ['/settings/configurations/instrumentType'], perm: 'configurations.index' },
      { title: 'Banks', url: ['/settings/configurations/banks'], perm: 'banks.index' },
      { title: 'Passing Years', url: ['/settings/passingYear'], perm: 'passingYears.index' },
      { title: 'Nature of Course', url: ['/settings/configurations/natureOfCourse'], perm: 'configurations.index' },
      { title: 'Campus Default Users', url: ['/settings/campusDefaultUsers'], perm: 'campusDefaultUsers.index' },
      // { title: 'Max Credit Hour', url: ['/settings/configurations/maxCreditHour'], perm: 'configurations.index' },
      { title: 'Boards', url: ['/settings/board'], perm: 'boards.index' },
      { title: 'Vendors', url: ['/settings/vendor'], perm: 'vendors.index' },
      { title: 'Work Flows', url: ['/settings/workFlow'], perm: 'workFlows.index' },
    ]
  },
  {
    heading: 'Evaluation Sheet',
    menuItems: [
      { title: 'ES Categories', url: ['/settings/esCategories'], perm: 'esCategories.index' },
      { title: 'Evaluation Sheets', url: ['/settings/evaluationSheets'], perm: 'evaluationSheets.index' }
    ]
  },
  {
    heading: 'Email',
    menuItems: [
      { title: 'Out Going Mail Servers', url: ['/settings/outGoingMailServers'], perm: 'outGoingMailServers.index' },
      { title: 'Email Templates', url: ['/settings/emailTemplates'], perm: 'emailTemplates.index' }
    ]
  },
  {
    heading: 'Locations',
    menuItems: [
      { title: 'Countries', url: ['/settings/configurations/country'], perm: 'configurations.index' },
      { title: 'Provinces', url: ['/settings/configurations/province'], perm: 'configurations.index' },
      { title: 'Cities', url: ['/settings/configurations/city'], perm: 'configurations.index' },
      { title: 'Tehsils', url: ['/settings/configurations/tehsil'], perm: 'configurations.index' }
    ]
  }
];
