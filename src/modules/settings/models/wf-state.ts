import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class WFStateModel {
  /**
   * it will set the labels for attributes of Work Flow State
   */
  static attributesLabels = {
    WFId: 'WFId',
    title: 'Title',
    state: 'State',
    description: 'Description'
  };
  id: number;
  WFId?: string;
  title: string;
  state?: string;
  description?: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules for
  */
  public validationRules?() {
    return {
      state: new FormControl('', [<any>Validators.required, Validators.maxLength(50)]),
      title: new FormControl('', [<any>Validators.maxLength(50)]),
      description: ['']
    };
  }
}
