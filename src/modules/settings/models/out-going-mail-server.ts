import { FormControl, Validators } from '@angular/forms';

export class OutGoingMailServerModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    name: 'Name',
    description: 'Description',
    isDefault: 'Default',
    smtpServer: 'SMTP Server',
    smtpPort: 'SMTP Port',
    connectionSecurity: 'Connection Security',
    ownerId: 'Owner',
    username: 'Username',
    password: 'Password'
  };

  id?: number;
  name?: string;
  description?: string;
  isDefault?: boolean;
  smtpServer?: string;
  smtpPort?: number;
  connectionSecurity?: string;
  ownerId?: number;
  username?: string;
  password?: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() {}

  /**
   * Form Validation Rules for Level of Education
   */
  public validationRules?() {
    return {
      name: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      description: new FormControl(),
      isDefault: new FormControl('', [<any>Validators.required]),
      smtpServer: new FormControl('', [<any>Validators.required]),
      smtpPort: new FormControl('', [<any>Validators.required]),
      connectionSecurity: new FormControl(''),
      ownerId: new FormControl('', [<any>Validators.required]),
      username: new FormControl('', [<any>Validators.required]),
      password: new FormControl('', [<any>Validators.required])
    };
  }
}
