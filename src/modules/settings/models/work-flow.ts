import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class WorkFlowModel {
  /**
   * it will set the labels for attributes of Work Flow
   */
  static attributesLabels = {
    WFId: 'ID',
    title: 'Title',
    description: 'Description'
  };

  WFId: string;
  title: string;
  description: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() {}

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      WFId: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      title: new FormControl('', [<any>Validators.required, Validators.maxLength(50)]),
      description: ['']
    };
  }
}
