import { FormControl, Validators, ValidatorFn, FormArray, AbstractControl } from '@angular/forms';


export class PassingYearModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    year: 'Passing Year',
    examinationType: 'Examination Type',
    annual: 'Annual',
    supplimentary: 'Supplimentary',
  };

  public id?: number;
  public year: number;
  public examinationType: string;
  public annual: boolean = false;
  public supplimentary: boolean = false;
  public createdBy?: number;
  public updatedBy?: number;
  public createdAt?: Date;
  public updatedAt?: Date;

  constructor() { }


  /**
 *
 * @param equalControlName
 */
  public equalTo?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      let controlMatch = 1850;

      return controlMatch <= control.value && 2018 >= control.value
        ? null
        : {
          equalTo: true
        };
    };
  }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      year: ['', [<any>Validators.required, this.equalTo('establishedScince')]],
      annual: [false],
      supplimentary: [false],
    };
  }
}
