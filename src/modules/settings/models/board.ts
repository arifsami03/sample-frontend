import { FormControl, Validators, ValidatorFn, FormArray, AbstractControl } from '@angular/forms';


export class BoardModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    name: 'Board Name',
    abbreviation: 'Abbreviation',
    website: 'Website',
    mobileNumber: 'Mobile No.',
    address: 'Address',
    countryCode : 'Country'
  };

  public id?: number;
  public name: string;
  public abbreviation: string;
  public website: string;
  public mobileNumber: string;
  public address: string;

  public createdBy?: number;
  public updatedBy?: number;
  public createdAt?: Date;
  public updatedAt?: Date;

  constructor() { }



  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      name: ['', [<any>Validators.required, Validators.minLength(0), Validators.maxLength(100)]],
      abbreviation: ['', [<any>Validators.required, Validators.minLength(0), Validators.maxLength(10)]],
      website: ['', [<any>Validators.required, Validators.minLength(0), Validators.maxLength(100)]],
      address: ['', [<any>Validators.required, Validators.minLength(0), Validators.maxLength(200)]],
      mobileNumber:new FormControl('', [<any>Validators.maxLength(12), Validators.pattern('([0-9]{3})-([0-9]{7})')]),
      countryCode: new FormControl('', Validators.required),
     
    };
  }
}
