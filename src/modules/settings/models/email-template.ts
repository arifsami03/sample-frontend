import { FormControl, Validators } from '@angular/forms';

export class EmailTemplateModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    name: 'Name',
    subject: 'Subject',
    emailBody: 'Eamil Body',
    outGoingMailServerId: 'Out Going Mail Server',
    to: 'To',
    cc: 'CC',
    bcc: 'BCC'
  };

  id?: number;
  name: string;
  subject: string;
  emailBody: string;
  outGoingMailServerId: number;
  to: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() {}

  /**
   * Form Validation Rules for Level of Education
   */
  public validationRules?() {
    return {
      name: new FormControl('', [<any>Validators.required, Validators.maxLength(100)]),
      subject: new FormControl('', [<any>Validators.required]),
      emailBody: new FormControl('', [<any>Validators.required]),
      outGoingMailServerId: new FormControl(null),
      to: new FormControl('', [<any>Validators.required])
    };
  }
}
