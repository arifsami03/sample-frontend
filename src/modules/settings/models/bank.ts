import { FormControl, Validators } from '@angular/forms';

export class BankModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    name: 'Bank Name',
    abbreviation: 'Abbreviation'
  };

  id?: number;
  name: string;
  abbreviation: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() {}

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      name: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      abbreviation: ['', <any>Validators.maxLength(10)]
    };
  }
}
