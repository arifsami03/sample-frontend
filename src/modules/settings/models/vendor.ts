import { FormControl, Validators, ValidatorFn, FormArray, AbstractControl } from '@angular/forms';


export class VendorModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    name: 'Full Name',
    website: 'Website',
    mobileNumber: 'Mobile No.',
    address: 'Address',
    email: 'Email',
    ntnNumber: 'NTN',
    faxNumber: 'Fax No.',
    contactPerson: 'Contact Person',
    salesTaxNumber: 'Sales Tax No.',
    countryCode : 'Country'
  };

  public id?: number;
  public name: string;
  public website: string;
  public mobileNumber: string;
  public address: string;
  public email: string;
  public ntnNumber: string;
  public faxNumber: string;
  public contactPerson: string;
  public salesTaxNumber: string;

  public createdBy?: number;
  public updatedBy?: number;
  public createdAt?: Date;
  public updatedAt?: Date;

  constructor() { }



  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      name: ['', [<any>Validators.required, Validators.minLength(0), Validators.maxLength(100)]],
      website: ['', [<any>Validators.required, Validators.minLength(0), Validators.maxLength(100),Validators.pattern('^(http://|https://)?(www.)?([0-9A-Za-z]+([0-9A-Za-z])*)([.][A-Za-z]([A-Za-z]+))+')]],
      address: ['', [<any>Validators.required, Validators.minLength(0), Validators.maxLength(200)]],
      mobileNumber: ['', [<any>Validators.required, Validators.minLength(0), Validators.maxLength(15)]],
      email: ['', [ Validators.minLength(0), Validators.maxLength(50), Validators.email]],
      ntnNumber: ['', [<any>Validators.required, ]],
      faxNumber: ['', [<any>Validators.required, ]],
      contactPerson: ['', [<any>Validators.required, Validators.minLength(0), Validators.maxLength(100)]],
      salesTaxNumber: ['', [<any>Validators.required]],
      countryCode: new FormControl('', Validators.required),
     
    };
  }
}
