import { FormControl, Validators, ValidatorFn, FormArray } from '@angular/forms';

export class WFStateFlowModel {
  /**
   * it will set the labels for attributes of Work Flow
   */
  static attributesLabels = {
    fromId: 'From',
    toId: 'To',
    description: 'Description',
    emailTemplateId: 'Select Email Template'
  };

  fromId: number;
  toId: number;
  from: string;
  to: string;
  WFId: string;
  emailTemplateId: number;
  description: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() {}

  /**
   * Form Validation Rules for
   */
  public validationRules?() {
    return {
      fromId: new FormControl('', [<any>Validators.required]),
      toId: new FormControl('', [<any>Validators.required]),
      description: [''],
      emailTemplateId: [''],
      emailTemplateList: new FormArray([])
    };
  }
}
