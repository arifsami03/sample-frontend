import { FormControl, Validators } from '@angular/forms';

export class CampusDefaultUserModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    userName: 'User Name',
    abbreviation: 'Abbreviation',
    roleId: 'Role',
  };

  id?: number;
  userName: string;
  abbreviation: string;
  roleId?: number;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      userName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      abbreviation: new FormControl('', [Validators.required, Validators.maxLength(6)]),
      roleId: new FormControl('', [Validators.required]),
    };
  }
}
