import { FormControl, Validators } from '@angular/forms';
import { ESCategoryModel } from './es-category';

export class EvaluationSheetModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    title: 'Title',
    category: 'Category',
    form: 'Form',
    active: 'Active'
  };

  id?: number;
  title: string;
  ESForm: string;
  active: number | string;
  ESCategoryId: number;
  esCategory: ESCategoryModel;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  // for listing
  esCategoryName?: string;
  esFormName?: string;


  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      title: new FormControl('', [Validators.required]),
      ESCategoryId: new FormControl('', [Validators.required]),
      ESForm: new FormControl('', [Validators.required]),
      active: new FormControl(null)
    };
  }
}
