import { FormControl, Validators } from '@angular/forms';

export class ESCategoryModel {
  /**
   * Set the labels for attributes 
   */
  static attributesLabels = {
    name: 'Name'
  };

  id?: number;
  name: string;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules 
   */
  public validationRules?() {
    return {
      name: new FormControl('', [Validators.required])
    };
  }
}
