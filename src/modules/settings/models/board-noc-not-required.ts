import { FormControl, Validators, FormArray, FormGroup } from '@angular/forms';
import { BoardModel } from '.';


export class BoardNOCNotRequiredModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    board: 'University/Board',
    boardNOCNotRequired: 'University/Board(NOC not required)'
  };

  public id?: number;
  public name?: string;
  public boardId: number;
  public board: BoardModel;

  public createdBy?: number;
  public updatedBy?: number;
  public createdAt?: Date;
  public updatedAt?: Date;

  constructor() { }



  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      id: new FormControl(null),
      boardId: new FormControl(null, [Validators.required]),
      boardNOCNotRequired: new FormArray([])
      // boardNOCNotRequired: new FormArray([
      //   new FormGroup({
      //     boardNOCNotRequiredId: new FormControl(null)
      //   })
      // ])
    };
  }
  /**
     * Form Validation Rules to create single record
     */
  public singleRecordValidationRules?() {
    return {
      id: new FormControl(null),
      boardId: new FormControl(null, [Validators.required]),
      boardNOCNotRequiredId: new FormControl(null, [Validators.required])
    };
  }
}
