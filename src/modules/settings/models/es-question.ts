import { FormControl, Validators } from '@angular/forms';

export class ESQuestionModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    question: 'Question',
    optionType: 'Options',
    remarksField: 'Remarks'
  };

  public id?: number;
  public question: string;
  public ESSectionId?: number;
  public countField: boolean = false;
  public remarksField: boolean = false;
  public optionType: string;
  public options: string[] = [];
  public createdBy?: number;
  public updatedBy?: number;
  public createdAt?: Date;
  public updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      question: new FormControl('', Validators.required),
      optionType: new FormControl('', Validators.required),
      remarksField: new FormControl(''),
    };
  }
}
