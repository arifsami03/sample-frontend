import { FormControl, Validators } from '@angular/forms';

export class ESQuestionOptionModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    optionString: 'Option'
  };

  id?: number;
  ESQuestionId?: number;
  optionString?: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      optionString: new FormControl('', [Validators.required]),
    };
  }
}
