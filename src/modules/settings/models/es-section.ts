import { FormControl, Validators } from '@angular/forms';
import { ESQuestionModel } from '.';

export class ESSectionModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    title: 'Title'
  };

  id?: number;
  title: string;
  ESId?: number;
  questions?: ESQuestionModel[];
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      title: new FormControl('', [Validators.required]),
    };
  }
}
