import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from '../../shared/services'; 
import { EmailTemplateModel } from '../models';

@Injectable()
export class EmailTemplateService extends BaseService {
  private routeURL: String = 'settings/emailTemplates';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<EmailTemplateModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <EmailTemplateModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<EmailTemplateModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <EmailTemplateModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<EmailTemplateModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <EmailTemplateModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: EmailTemplateModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: EmailTemplateModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }
}
