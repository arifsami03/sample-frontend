import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from '../../shared/services'; 
import { OutGoingMailServerModel } from '../models';

@Injectable()
export class OutGoingMailServerService extends BaseService {
  private routeURL: String = 'settings/outGoingMailServers';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<OutGoingMailServerModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <OutGoingMailServerModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<OutGoingMailServerModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <OutGoingMailServerModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<OutGoingMailServerModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <OutGoingMailServerModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: OutGoingMailServerModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: OutGoingMailServerModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }
}
