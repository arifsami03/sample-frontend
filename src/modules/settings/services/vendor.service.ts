import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from '../../shared/services'; 
import { VendorModel } from '../models';

@Injectable()
export class VendorService extends BaseService {
  private routeURL: String = 'settings/vendors';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<VendorModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <VendorModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<VendorModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <VendorModel[]>data.json();
    });
  }


  /**
   * Get single record
   */
  find(id: number): Observable<VendorModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <VendorModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(postData: VendorModel) {
    return this.__post(`${this.routeURL}/create`, postData).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, postData: VendorModel) {
    return this.__put(`${this.routeURL}/update/${id}`, postData).map(data => {
      return data.json();
    });
  }
}
