import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../shared/services'; 
import { ESCategoryModel ,EvaluationSheetModel} from '../models';

@Injectable()
export class ESCategoryService extends BaseService {

  private routeURL: String = 'settings/esCategories';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<ESCategoryModel[]> {

    return this.__get(`${this.routeURL}/index`).map(data => {
      return <ESCategoryModel[]>data.json();
    });

  }

  /**
  * Get single record
  */
  find(id: number): Observable<ESCategoryModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <ESCategoryModel>data.json();
    });
  }

  /**
   * Create record
   */
  create(item: ESCategoryModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
 * Update record
 */
  update(id: number, item: ESCategoryModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Get All records with only id, name attributes 
   */
  findAttributesList(): Observable<ESCategoryModel[]> {

    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <ESCategoryModel[]>data.json();
    });

  }

  findCategoriesByES(): Observable<EvaluationSheetModel[]> {

    return this.__get(`${this.routeURL}/findCategoriesByES`).map(data => {
      return <EvaluationSheetModel[]>data.json();
    });

  }



}
