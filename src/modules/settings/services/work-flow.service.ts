import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';

import { BaseService } from '../../shared/services'; 
import { WorkFlowModel } from '../models';

@Injectable()
export class WorkFlowService extends BaseService {
  private routeURL: String = 'settings/workFlows';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<WorkFlowModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <WorkFlowModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<WorkFlowModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <WorkFlowModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<WorkFlowModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <WorkFlowModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: string) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(workFlow: WorkFlowModel) {
    return this.__post(`${this.routeURL}/create`, workFlow).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: string, postData: WorkFlowModel) {
    return this.__put(`${this.routeURL}/update/${id}`, postData).map(data => {
      return data.json();
    });
  }

  /**
   * Check Record Exists or Not
   */

  checkRecordExists(item: WorkFlowModel) {
    var id = item.WFId;
    return this.__get(`${this.routeURL}/checkRecordExists/${id}`).map(data => {
      return data.json();
    });
  }
}
