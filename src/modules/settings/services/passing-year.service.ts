import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from '../../shared/services'; 
import { PassingYearModel } from '../models';

@Injectable()
export class PassingYearService extends BaseService {
  private routeURL: String = 'settings/passingYears';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<PassingYearModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <PassingYearModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<PassingYearModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <PassingYearModel[]>data.json();
    });
  }
  /**
   * Get All records Of Passing Year whoes id not present in the available array
   */
  getPassingYearNotSelected(selectedPassingYear): Observable<PassingYearModel[]> {
    return this.__post(`${this.routeURL}/getPassingYearNotSelected`,selectedPassingYear).map(data => {
      return <PassingYearModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<PassingYearModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <PassingYearModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(postData: PassingYearModel) {
    return this.__post(`${this.routeURL}/create`, postData).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, postData: PassingYearModel) {
    return this.__put(`${this.routeURL}/update/${id}`, postData).map(data => {
      return data.json();
    });
  }
}
