import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from '../../shared/services'; 
import { EvaluationSheetModel, ESSectionModel } from '../models';

@Injectable()
export class EvaluationSheetService extends BaseService {
  private routeURL: String = 'settings/evaluationSheets';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<EvaluationSheetModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <EvaluationSheetModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<EvaluationSheetModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <EvaluationSheetModel[]>data.json();
    });
  }

  /**
   * Get All records
   */
  findAttributesListByCat(catId): Observable<EvaluationSheetModel[]> {
    return this.__get(`${this.routeURL}/findAttributesListByCat/${catId}`).map(data => {
      return <EvaluationSheetModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<EvaluationSheetModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <EvaluationSheetModel>data.json();
    });
  }
  /**
   * Get single record
   */
  findCampusEvaluationSheet(): Observable<EvaluationSheetModel> {
    return this.__get(`${this.routeURL}/findCampusEvaluationSheet`).map(data => {
      return <EvaluationSheetModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: EvaluationSheetModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: EvaluationSheetModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }

}
