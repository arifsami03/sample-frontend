import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BaseService } from '../../shared/services'; 
import { WFStateFlowModel } from '../models';

@Injectable()
export class WFStateFlowService extends BaseService {
  private routeURL: String = 'settings/wfStateFlows';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<WFStateFlowModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <WFStateFlowModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<WFStateFlowModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <WFStateFlowModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  list(id: string): Observable<WFStateFlowModel[]> {
    return this.__get(`${this.routeURL}/list/${id}`).map(data => {
      return <WFStateFlowModel[]>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(workFlow: WFStateFlowModel) {
    return this.__post(`${this.routeURL}/create`, workFlow).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: string, postData: WFStateFlowModel) {
    return this.__put(`${this.routeURL}/update/${id}`, postData).map(data => {
      return data.json();
    });
  }

  /**
  * Get single record
  */
  findById(id: number): Observable<WFStateFlowModel> {
    return this.__get(`${this.routeURL}/findById/${id}`).map(data => {
      return <WFStateFlowModel>data.json();
    });
  }
  getFlowByWFId(id: number): Observable<WFStateFlowModel[]> {
    return this.__get(`${this.routeURL}/getFlowByWFId/${id}`).map(data => {
      return <WFStateFlowModel[]>data.json();
    });
  }


  /**
   * Reload WF State Flow data
   */
  private reloadStateFlowSource = new Subject<boolean>();
  reloadStateFlow$ = this.reloadStateFlowSource.asObservable();

  /**
   * Set Application Title
   * @param titleData string
   */
  reloadStateFlow(doReload: boolean) {
    this.reloadStateFlowSource.next(doReload);
  }

}
