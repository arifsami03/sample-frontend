import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from '../../shared/services'; 
import { CampusDefaultUserModel } from '../models';

@Injectable()
export class CampusDefaultUserService extends BaseService {
  private routeURL: String = 'settings/campusDefaultUsers';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<CampusDefaultUserModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <CampusDefaultUserModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<CampusDefaultUserModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <CampusDefaultUserModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<CampusDefaultUserModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <CampusDefaultUserModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: CampusDefaultUserModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: CampusDefaultUserModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Get All ESSections against an Evaluation Sheet
   */
  findAllESSections(ESId: number): Observable<CampusDefaultUserModel[]> {
    return this.__get(`${this.routeURL}/findAllESSections/${ESId}`).map(data => {
      return <CampusDefaultUserModel[]>data.json();
    });
  }

}
