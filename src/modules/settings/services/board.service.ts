import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from '../../shared/services'; 
import { BoardModel, BoardNOCNotRequiredModel } from '../models';

@Injectable()
export class BoardService extends BaseService {
  private routeURL: String = 'settings/boards';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<BoardModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <BoardModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<BoardModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <BoardModel[]>data.json();
    });
  }


  /**
   * Get single record
   */
  find(id: number): Observable<BoardModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <BoardModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(postData: BoardModel) {
    return this.__post(`${this.routeURL}/create`, postData).map(data => {
      return data.json();
    });
  }


  /**
   * Update record
   */
  update(id: number, postData: BoardModel) {
    return this.__put(`${this.routeURL}/update/${id}`, postData).map(data => {
      return data.json();
    });
  }


  // NOC (not required functionality)

  /**
   * Add NOC (not required) to Board
   */
  addNOCNotRequired(item: BoardNOCNotRequiredModel) {
    return this.__post(`${this.routeURL}/addNOCNotRequired`, item).map(data => {
      return <BoardNOCNotRequiredModel>data.json();
    });
  }

  /**
   * Deletes a Board NOC (not required) from a board
   */
  deleteNOCNotRequuired(id: number) {
    return this.__delete(`${this.routeURL}/deleteNOCNotRequuired/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Finds NOC (not required) board list against a board
   */
  findAllNOCNotRequiredBoards(id: number): Observable<BoardModel[]> {
    return this.__get(`${this.routeURL}/findAllNOCNotRequiredBoards/${id}`).map(data => {
      return <BoardModel[]>data.json();
    });
  }

  /**
   * Get find All board to add NOC except the existing NOC
   */
  findAllBoardsToAddNOC(id: number): Observable<BoardModel[]> {
    return this.__get(`${this.routeURL}/findAllBoardsToAddNOC/${id}`).map(data => {
      return <BoardModel[]>data.json();
    });
  }

}
