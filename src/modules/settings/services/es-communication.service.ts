import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ESCommunicationService {

  // Observable string sources
  private refreshSectionSource = new Subject<string>();
  private refreshQuestionSource = new Subject<string>();

  // Observable string streams
  refreshSection$ = this.refreshSectionSource.asObservable();
  refreshQuestion$ = this.refreshQuestionSource.asObservable();

  // Service message commands
  refreshSection(section: string) {
    this.refreshSectionSource.next(section);
  }

  // Service message commands
  refreshQuestion(question: string) {
    this.refreshSectionSource.next(question);
  }

}