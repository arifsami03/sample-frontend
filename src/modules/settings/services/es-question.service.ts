import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from '../../shared/services'; 
import { ESQuestionModel } from '../models';

@Injectable()
export class ESQuestionService extends BaseService {
  private routeURL: String = 'settings/esQuestions';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<ESQuestionModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <ESQuestionModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<ESQuestionModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <ESQuestionModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<ESQuestionModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <ESQuestionModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: ESQuestionModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: ESQuestionModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Get All ESQuestions against an ESSection
   */
  findAllESQuestions(ESSectionId: number): Observable<ESQuestionModel[]> {
    return this.__get(`${this.routeURL}/findAllESQuestions/${ESSectionId}`).map(data => {
      return <ESQuestionModel[]>data.json();
    });
  }

}
