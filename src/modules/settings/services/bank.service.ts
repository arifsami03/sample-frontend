import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from '../../shared/services'; 
import { BankModel } from '../models';

@Injectable()
export class BankService extends BaseService {
  private routeURL: String = 'settings/banks';

  constructor(protected http: Http) {
    super(http)
  }

  /**
   * Get All records
   */
  index(): Observable<BankModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <BankModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<BankModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <BankModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<BankModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <BankModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: BankModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: BankModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }
}
