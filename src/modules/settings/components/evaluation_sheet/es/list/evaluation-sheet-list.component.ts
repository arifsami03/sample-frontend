import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';

import { PageAnimation } from '../../../../../shared/helper';
import { EvaluationSheetService } from '../../../../services';
import { LayoutService } from '../../../../../layout/services';
import { EvaluationSheetModel } from '../../../../models';
import { ConfirmDialogComponent } from '../../../../../shared/components';
import { GLOBALS } from '../../../../../app/config/globals';

@Component({
  selector: 'setting-evaluation-sheet-list',
  templateUrl: './evaluation-sheet-list.component.html',
  styleUrls: ['./evaluation-sheet-list.component.css'],
  animations: [PageAnimation]
})
export class EvaluationListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public evaluationSheets: EvaluationSheetModel[] = [new EvaluationSheetModel()];
  esForms = GLOBALS.ESForms;

  public attrLabels = EvaluationSheetModel.attributesLabels;

  displayedColumns = ['title', 'category', 'form', 'active', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private evaluationSheetService: EvaluationSheetService,
    public layoutService: LayoutService,
    public matDialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Evaluation Sheets' });
    this.getRecords();
  }

  getRecords(): void {
    this.evaluationSheetService.index().subscribe(
      response => {
        this.evaluationSheets = response;

        // for searching mechanism we are maping this in to single string
        this.evaluationSheets.map(element => {
          element.esCategoryName = element.esCategory.name;
          let selectedESForm = this.esForms.find(item => {
            return item.value == element.ESForm;
          });
          element.esFormName = selectedESForm ? selectedESForm.label : '';
        });
        this.dataSource = new MatTableDataSource<EvaluationSheetModel>(this.evaluationSheets);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.evaluationSheetService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Evaluation Sheet has been deleted.', msgType: 'success' });
          },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
          },
          () => {
            this.getRecords();
            this.loaded = true;
          }
        );
      }
    });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }
}
