import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

import { ESQuestionService, ESCommunicationService } from '../../../../../services';
import { LayoutService } from '../../../../../../layout/services';
import { ESQuestionModel } from '../../../../../models';

import { ESQuestionFormComponent } from '../form/es-question-form.component';
import { ConfirmDialogComponent } from '../../../../../../shared/components';

import { GLOBALS } from '../../../../../../app/config/globals';

@Component({
  selector: 'settings-es-question-view',
  templateUrl: './es-question-view.component.html',
  styleUrls: ['./es-question-view.component.css']
})
export class ESQuestionViewComponent implements OnInit {


  public loaded: boolean = false;
  public esQuestions: ESQuestionModel[];
  subscription: Subscription;

  @Input() ESSectionId: number;

  constructor(
    private esQuestionService: ESQuestionService,
    private esCommunicationService: ESCommunicationService,
    private layoutService: LayoutService,
    public matDialog: MatDialog) {
    this.subscription = esCommunicationService.refreshQuestion$.subscribe(
      response => {
        this.getRecord(this.ESSectionId);
      });
  }

  ngOnInit() {
    this.getRecord(this.ESSectionId);
  }

  getRecord(ESSectionId: number) {
    this.esQuestionService.findAllESQuestions(ESSectionId).subscribe(
      response => {
        this.esQuestions = response;
      },
      error => {
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  editESQuestion(id: number) {
    const dialogRef = this.matDialog.open(ESQuestionFormComponent, {
      width: '800px',
      data: { id: id, action: GLOBALS.pageActions.update }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getRecord(this.ESSectionId);
      }

    });
  }

  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.esQuestionService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Question has been deleted.', msgType: 'success' });
            this.getRecord(this.ESSectionId);
          },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
          },
          () => {

            this.loaded = true;
          }
        );
      }
    });
  }

}
