import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ESQuestionService } from '../../../../../services';
import { ESQuestionModel } from '../../../../../models';
import { LayoutService } from '../../../../../../layout/services';

import { GLOBALS } from '../../../../../../app/config/globals';

@Component({
  selector: 'setting-es-question',
  templateUrl: './es-question-form.component.html',
  styleUrls: ['./es-question-form.component.css']
})
export class ESQuestionFormComponent {
  public loaded: boolean = false;
  private pageAct: string;
  public pageTitle: string;
  public options //= GLOBALS.ESQOptions;
  // public selectedOption: string;
  private ESSectionId: number;
  private id: number;
  public fg: FormGroup;
  public componentLabels = ESQuestionModel.attributesLabels;

  public esQuestion: ESQuestionModel;

  constructor(
    private esQuestionService: ESQuestionService,
    private fb: FormBuilder,
    private layoutService: LayoutService,
    public dialogRef: MatDialogRef<ESQuestionFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  ngOnInit() {
    if (this.data.form == 'campus-application-form') {
      GLOBALS.ESQOptions.forEach(element => {

        if (element['form'] == 'campus-application-form') {
          this.options = element['questionOptions']
        }


      });
    }
    else {
      GLOBALS.ESQOptions.forEach(element => {

        if (element['form'] == 'all') {
          this.options = element['questionOptions']
        }


      });
    }
    this.ESSectionId = this.data.ESSectionId;
    this.id = this.data.id;
    this.pageAct = this.data.action
    this.initializePage();
  }

  private initializePage() {
    this.fg = this.fb.group(new ESQuestionModel().validationRules());
    if (this.pageAct === 'create') {
      this.pageTitle = 'Create Question'
      this.initAddPage();
    } else {
      this.getData(this.id);
    }
  }

  private initAddPage() {
    this.esQuestion = new ESQuestionModel();
    this.fg.enable();

    this.loaded = true;
  }

  private getData(id: number) {
    this.esQuestionService.find(id).subscribe(
      response => {
        this.esQuestion = response;
      },
      error => {
        this.loaded = true;
      },
      () => {
        this.fg.patchValue(this.esQuestion);
        this.initUpdatePage();
        this.loaded = true;
      }
    );
  }

  private initUpdatePage() {
    this.fg.enable();
    this.pageTitle = `Update: ${this.esQuestion.question}`;

  }

  public saveData(item: ESQuestionModel) {

    this.loaded = false;
    // get id of loggedIn userId
    this.esQuestion.createdBy = +this.getLoggedInUserInfo('id');
    this.esQuestion.ESSectionId = this.ESSectionId;
    this.esQuestion.question = item.question;

    if (item.remarksField) {
      this.esQuestion.remarksField = true;
    } else {
      this.esQuestion.remarksField = false;
    }

    if (item.optionType) {
      this.esQuestion.optionType = item.optionType;
      if (item.optionType == 'selectionWithCount') {
        this.esQuestion.countField = true;
      } else {
        this.esQuestion.countField = false;
      }
      let options = this.options.find(element => {
        return element.type == item.optionType;
      });

      this.esQuestion.options = options.value;
    }

    if (this.pageAct === 'create') {
      this.create(this.esQuestion);
    } else {
      this.update(this.esQuestion.id, this.esQuestion);
    }
  }

  private create(item: ESQuestionModel) {

    this.esQuestionService.create(item).subscribe(
      response => {
        this.esQuestion = response;
        this.layoutService.flashMsg({ msg: 'Question has been created.', msgType: 'success' });
        this.dialogRef.close(true);
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        console.log(error);
      },
      () => {
        this.loaded = true;

      }
    );
  }

  private update(id: number, item: ESQuestionModel) {

    this.esQuestionService.update(id, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Question has been updated.', msgType: 'success' });
        this.dialogRef.close(true);
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        console.log(error);
      },
      () => {
        this.loaded = true;

      }
    );
  }

  private getLoggedInUserInfo(parameter: string): string {
    return localStorage.getItem(parameter);
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }


}
