import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';

import { PageAnimation } from '../../../../../shared/helper';
import { EvaluationSheetService, ESCategoryService } from '../../../../services';
import { LayoutService } from '../../../../../layout/services';
import { EvaluationSheetModel, ESCategoryModel } from '../../../../models';
import { GLOBALS } from '../../../../../app/config/globals';

@Component({
  selector: 'setting-evaluation-sheet-form',
  templateUrl: './evaluation-sheet-form.component.html',
  styleUrls: ['./evaluation-sheet-form.component.css'],
  animations: [PageAnimation]
})
export class EvaluationFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;

  public esCategories: ESCategoryModel[];

  public evaluationSheet: EvaluationSheetModel;

  public esForms = GLOBALS.ESForms;
  public componentLabels = EvaluationSheetModel.attributesLabels;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private evaluationSheetService: EvaluationSheetService,
    private esCategoryService: ESCategoryService,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  ngOnInit() {
    this.initializePage();

    this.esCategoryService.findAttributesList().subscribe(
      response => {
        this.esCategories = response;
      },
      error => {
        this.esCategories = [new ESCategoryModel()];
      }
    );
  }

  private initializePage() {
    this.fg = this.fb.group(new EvaluationSheetModel().validationRules());
    if (this.pageAct === 'create') {
      this.initAddPage();
    } else {
      this.getData();
    }
  }

  private getData() {
    this.route.params.subscribe(params => {
      this.evaluationSheetService.find(params['id']).subscribe(
        response => {
          this.evaluationSheet = response;
        },
        error => {
          this.loaded = true;
        },
        () => {
          this.fg.patchValue(this.evaluationSheet);
          if (this.pageAct === 'view') {
            this.initViewPage();
          } else if (this.pageAct === 'update') {
            this.initUpdatePage();
          }
          this.loaded = true;
        }
      );
    });
  }

  private initAddPage() {
    this.layoutService.setPageTitle({ title: 'Add Evaluation Sheet' });
    this.evaluationSheet = new EvaluationSheetModel();
    this.fg.enable();

    this.loaded = true;
  }

  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Evaluation Sheet: ' + this.evaluationSheet.title,
      breadCrumbs: [{ label: 'Home', url: '/home/dashboard' }, { label: 'Evaluation Sheets', url: '/settings/evaluationSheets' }, { label: this.evaluationSheet.title }]
    });
  }

  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Evaluation Sheet: ' + this.evaluationSheet.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Evaluation Sheets', url: '/settings/evaluationSheets' },
        {
          label: this.evaluationSheet.title,
          url: `/settings/evaluationSheets/view/${this.evaluationSheet.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  public saveData(item: EvaluationSheetModel) {
    if (this.pageAct === 'create') {
      this.create(item);
    } else {
      this.update(this.evaluationSheet.id, item);
    }
  }

  private create(item: EvaluationSheetModel) {
    // get id of loggedIn userId
    item.createdBy = +this.getLoggedInUserInfo('id');

    this.evaluationSheetService.create(item).subscribe(
      response => {
        this.evaluationSheet = response;
        this.layoutService.flashMsg({ msg: 'Evaluation Sheet has been created.', msgType: 'success' });
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        console.log(error);
      },
      () => {
        this.loaded = true;
        this.router.navigate([`/settings/evaluationSheets/view/${this.evaluationSheet.id}`]);
      }
    );
  }

  private update(id: number, item: EvaluationSheetModel) {

    this.evaluationSheetService.update(id, item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Evaluation Sheet has been updated.', msgType: 'success' });
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        console.log(error);
      },
      () => {
        this.loaded = true;
        this.router.navigate([`/settings/evaluationSheets/view/${this.evaluationSheet.id}`]);
      }
    );
  }

  private getLoggedInUserInfo(parameter: string): string {
    return localStorage.getItem(parameter);
  }
}
