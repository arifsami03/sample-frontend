import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';

import { PageAnimation } from '../../../../../shared/helper';
import { EvaluationSheetService, ESCategoryService, ESSectionService, ESQuestionService, ESCommunicationService } from '../../../../services';
import { LayoutService } from '../../../../../layout/services';
import { EvaluationSheetModel, ESSectionModel, ESCategoryModel } from '../../../../models';
import { ESSectionFormComponent } from '../section/form/es-section-form.component';
import { ESQuestionFormComponent } from '../question/form/es-question-form.component';
import { ConfirmDialogComponent } from '../../../../../shared/components';

import { GLOBALS } from '../../../../../app/config/globals';

@Component({
  selector: 'setting-evaluation-sheet-view',
  templateUrl: './evaluation-sheet-view.component.html',
  styleUrls: ['./evaluation-sheet-view.component.css'],
  animations: [PageAnimation]
})
export class EvaluationViewComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;

  public expanded: number = 0;


  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;

  public esCategories: ESCategoryModel[];

  public evaluationSheet: EvaluationSheetModel;
  public esSections: ESSectionModel[];
  esForms = GLOBALS.ESForms;

  public componentLabels = EvaluationSheetModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private evaluationSheetService: EvaluationSheetService,
    private esCategoryService: ESCategoryService,
    private esSectionService: ESSectionService,
    private esQuestionService: ESQuestionService,
    private esCommunicationService: ESCommunicationService,
    private layoutService: LayoutService,
    public matDialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.getData();
    this.esCategoryService.findAttributesList().subscribe(
      response => {
        this.esCategories = response;
      },
      error => {
        this.esCategories = [new ESCategoryModel()];
      }
    );
  }
public esFormName='';
  private getData() {
    this.route.params.subscribe(params => {
      this.evaluationSheetService.find(params['id']).subscribe(
        response => {
          this.evaluationSheet = response;
          this.esFormName=this.evaluationSheet.ESForm;
          let selectedESForm = this.esForms.find(element => {
            return element.value == this.evaluationSheet.ESForm;
          });
          this.evaluationSheet.ESForm = selectedESForm ? selectedESForm.label : '';
          this.evaluationSheet.active = this.evaluationSheet.active ? 'Yes' : 'No';
        },
        error => {
          this.loaded = true;
        },
        () => {
          this.initViewPage();
          this.findAllESSections(this.evaluationSheet.id);
          this.loaded = true;
        }
      );
    });
  }

  findAllESSections(ESId: number) {
    this.esSectionService.findAllESSections(ESId).subscribe(
      response => {
        this.esSections = response;
      },
      error => {
        console.log(error);
      },
      () => {
      }
    );
  }

  checkForSections(){
    if(this.esFormName == 'campus-application-form'){
      if(this.esSections.length > 0){
        return false;
      }
      else{
        return true;
      }
    }
    else{
      return true;
    }
    
  }

  findAllESQuestions(ESSectionId: number) {
    // save record which expansion panel is currently opened
    this.expanded = ESSectionId;

    let esSection = this.esSections.find(element => {
      return element.id == ESSectionId;
    });

    if (esSection && (esSection.questions == undefined || esSection.questions.length == 0)) {
      this.esQuestionService.findAllESQuestions(ESSectionId).subscribe(
        response => {
          esSection.questions = response;
        },
        error => {
          console.log(error);
          // this.loaded = true;
        },
        () => {
        }
      );
    }

  }

  private initViewPage() {
    this.fg = this.fb.group(new EvaluationSheetModel().validationRules());
    this.fg.patchValue(this.evaluationSheet);
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Evaluation Sheet: ' + this.evaluationSheet.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Evaluation Sheets', url: '/settings/evaluationSheets' },
        { label: this.evaluationSheet.title }
      ]
    });
  }

  addESSection(id: number) {
    const dialogRef = this.matDialog.open(ESSectionFormComponent, {
      width: '500px',
      data: { ESId: id, action: GLOBALS.pageActions.create }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.findAllESSections(this.evaluationSheet.id);
      }
    });
  }

  editESSection(id: number) {
    const dialogRef = this.matDialog.open(ESSectionFormComponent, {
      width: '500px',
      data: { id: id, action: GLOBALS.pageActions.update }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.findAllESSections(this.evaluationSheet.id);
      }

    });
  }

  deleteESSection(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        // this.loaded = false;
        this.esSectionService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Section has been deleted.', msgType: 'success' });

            this.findAllESSections(this.evaluationSheet.id);
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
          },
          () => {

          }
        );
      }
    });
  }

  addESQuestion(ESSectionId: number) {
    const dialogRef = this.matDialog.open(ESQuestionFormComponent, {
      width: '800px',
      data: { ESSectionId: ESSectionId, action: GLOBALS.pageActions.create, form:this.esFormName }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

        this.reloadESQuestions(ESSectionId);
      }
    });
  }

  editESQuestion(ESSectionId: number, ESQuestionId: number) {
    const dialogRef = this.matDialog.open(ESQuestionFormComponent, {
      width: '800px',
      data: { id: ESQuestionId, action: GLOBALS.pageActions.update, form:this.esFormName }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.reloadESQuestions(ESSectionId);
      }
    });
  }

  deleteESQuestion(ESSectionId: number, ESQuestionId: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.esQuestionService.delete(ESQuestionId).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Question has been deleted.', msgType: 'success' });

            this.reloadESQuestions(ESSectionId);
          },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          },
          () => {

          }
        );
      }
    });
  }

  /**
   * If a new question is added to section, then reload its questions from data base.
   * @param ESSectionId 
   */
  reloadESQuestions(ESSectionId: number) {
    let esSection = this.esSections.find(element => {
      return element.id == ESSectionId;
    });

    if (esSection) {
      this.esQuestionService.findAllESQuestions(ESSectionId).subscribe(
        response => {
          esSection.questions = response;
        },
        error => {
          console.log(error);
          // this.loaded = true;
        },
        () => {
        }
      );
    }
  }

  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.evaluationSheetService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Evaluation Sheet is deleted.', msgType: 'success' });
          },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
          },
          () => {
            this.router.navigate(['settings/evaluationSheets']);
            this.loaded = true;
          }
        );
      }
    });
  }


}
