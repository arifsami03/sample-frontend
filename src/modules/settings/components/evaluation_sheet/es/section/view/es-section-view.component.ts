import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

import { ESSectionService, ESCommunicationService } from '../../../../../services';
import { LayoutService } from '../../../../../../layout/services';
import { ESSectionModel } from '../../../../../models';

import { ESSectionFormComponent } from '../form/es-section-form.component';

import { ESQuestionFormComponent } from '../../question/form/es-question-form.component';
import { ConfirmDialogComponent } from '../../../../../../shared/components';

import { GLOBALS } from '../../../../../../app/config/globals';

@Component({
  selector: 'setting-es-section-view',
  templateUrl: './es-section-view.component.html',
  styleUrls: ['./es-section-view.component.css']
})
export class ESSectionViewComponent implements OnInit {


  public loaded: boolean = false;
  public esSections: ESSectionModel[];
  subscription: Subscription;

  @Input() ESId: number;

  constructor(
    private esSectionService: ESSectionService,
    private esCommunicationService: ESCommunicationService,
    private layoutService: LayoutService,
    public matDialog: MatDialog) {
    this.subscription = esCommunicationService.refreshSection$.subscribe(
      response => {
        this.getRecord(this.ESId);
      });
  }

  ngOnInit() {
    this.getRecord(this.ESId);
  }

  getRecord(ESId: number) {
    this.loaded = false;
    this.esSectionService.findAllESSections(ESId).subscribe(
      response => {
        this.esSections = response;
      },
      error => {
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  addESQuestion(ESSectionId: number) {
    const dialogRef = this.matDialog.open(ESQuestionFormComponent, {
      width: '800px',
      data: { ESSectionId: ESSectionId, action: GLOBALS.pageActions.create }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.esCommunicationService.refreshQuestion('refresh');
      }
    });
  }


  editESSection(id: number) {
    const dialogRef = this.matDialog.open(ESSectionFormComponent, {
      width: '500px',
      data: { id: id, action: GLOBALS.pageActions.update }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // this.getRecord(this.ESId);
      }

    });
  }

  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        // this.loaded = false;
        this.esSectionService.delete(id).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Section has been deleted.', msgType: 'success' });
            this.getRecord(this.ESId);
          },
          error => {
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            console.log(error);
          },
          () => {

          }
        );
      }
    });
  }

}
