import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { PageAnimation } from '../../../../../shared/helper';
import { ESCategoryService } from '../../../../services';
import { LayoutService } from '../../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../../shared/components';
import { ESCategoryModel } from '../../../../models';
import { GLOBALS } from '../../../../../app/config/globals';

@Component({
  selector: 'settings-es-category-form',
  templateUrl: './es-category-form.component.html',
  styleUrls: ['./es-category-form.component.css'],
  animations: [PageAnimation]
})
export class ESCategoryFormComponent implements OnInit {

  public pageState = 'active';
  
  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public boxLoaded = true;

  public error: Boolean;
  public check = false;
  public fg: FormGroup;
  public pageAct: string;
  public esCategory: ESCategoryModel;

  public componentLabels = ESCategoryModel.attributesLabels;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private esCategoryService: ESCategoryService,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = activatedRoute.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  /**
   * Initialize page
   */
  private initializePage() {

    this.fg = this.fb.group(new ESCategoryModel().validationRules());

    if (this.pageAct === this.pageActions.create) {

      this.initCreatePage();

    } else {

      this.getData();

    }

  }

  /**
   * Get record
   */
  private getData() {

    this.activatedRoute.params.subscribe(params => {

      this.esCategoryService.find(params['id']).subscribe(response => {

        if (response) {
          this.esCategory = response;

          this.check = true;
          this.fg.patchValue(this.esCategory);

          //TODO:low need to check why we are dowing this after fetching data
          if (this.pageAct == this.pageActions.view) {
            this.initViewPage();
          } else if (this.pageAct == this.pageActions.update) {
            this.initUpdatePage();
          }
        }

      },
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {

    this.layoutService.setPageTitle({ title: 'ES Categories: Create' });

    this.esCategory = new ESCategoryModel();

    this.fg.enable();

    this.loaded = true;
  }

  /**
   * Initialize the view page
   */
  private initViewPage() {

    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'ES Categories: ' + this.esCategory.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'ES Categories ',
          url: '/settings/esCategories'
        },
        { label: this.esCategory.name }
      ]
    });
  }

  /**
   * Initialize the update page
   */
  private initUpdatePage() {

    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'ES Categories: ' + this.esCategory.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'ES Categories', url: '/settings/esCategories' },
        {
          label: this.esCategory.name,
          url: `/settings/esCategories/view/${this.esCategory.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Create or Update record in database when save button is clicked
   * 
   */
  public saveData(item: ESCategoryModel) {
    this.boxLoaded = false;
    if (this.pageAct === this.pageActions.create) {

      this.esCategoryService.create(item).subscribe(response => {
        this.layoutService.flashMsg({ msg: 'ES Category has been created.', msgType: 'success' });
        this.router.navigate([`/settings/esCategories`]);

      },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        }, () => {
          this.boxLoaded = true;
        }
      );
    }
    else if (this.pageAct === this.pageActions.update) {

      this.esCategoryService.update(this.esCategory.id, item).subscribe(response => {
        this.layoutService.flashMsg({ msg: 'ES Category has been updated.', msgType: 'success' });
        this.router.navigate([`/settings/esCategories`]);
      },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        }, () => {
          this.boxLoaded = true;
        }
      );
    }
  }

  /**
   * Delete record
   * 
   */
  delete(id: number) {

    this.matDialog.open(ConfirmDialogComponent, {

      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }

    }).afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;

        this.esCategoryService.delete(id).subscribe(response => {
          this.layoutService.flashMsg({ msg: 'ES Category has been deleted.', msgType: 'success' });
          this.router.navigate(['/settings/esCategories']);
        }, error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        });
      }
    });
  }
}
