import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { PageAnimation } from '../../../../shared/helper';
import { PassingYearService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { PassingYearModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'passing-year-form',
  templateUrl: './passing-year-form.component.html',
  styleUrls: ['./passing-year-form.component.css'],
  animations: [PageAnimation]
})
export class PassingYearFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public showDivBar = false;
  public pageActions = GLOBALS.pageActions;

  public error: Boolean;
  public fg: FormGroup;
  public pageAct: string;
  public passingYear: PassingYearModel;

  public componentLabels = PassingYearModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private passingYearService: PassingYearService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * Initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new PassingYearModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get all data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.passingYearService.find(params['id']).subscribe(
        response => {
          this.passingYear = response;
          this.fg.patchValue(response);
          if (!this.error) {
            if (this.pageAct === this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct === this.pageActions.update) {
              this.initUpdatePage();
            }
            this.loaded = true;
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Initialize create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Passing Year: Create' });
    this.passingYear = new PassingYearModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * Initialize view page
   */
  private initViewPage() {
    this.fg.disable();
    this.layoutService.setPageTitle({
      title: 'Passing Year: ' + this.passingYear.year,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Passing Year ', url: '/settings/passingYear' },
        { label: this.passingYear.year }
      ]
    });
  }

  /**
   * Initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Passing Year: ' + this.passingYear.year,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Passing Year', url: '/settings/passingYear' },
        {
          label: this.passingYear.year,
          url: `/settings/passingYear/view/${this.passingYear.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Add or update data in database when save button is clicked
   */
  public saveData(item: PassingYearModel) {

    if (item.annual == false && item.supplimentary == false) {
      this.layoutService.flashMsg({ msg: 'Must Select Examination Type.', msgType: 'warning' });
    }
    else {
      this.showDivBar = true;
      let id;
      if (this.pageAct === this.pageActions.create) {
        item.id = null;
        this.passingYearService.create(item).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Passing Year has been created.', msgType: 'success' });
            this.router.navigate([`/settings/passingYear`]);
          },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
          }, () => {
            this.showDivBar = false;
          }
        );
      } else if (this.pageAct === this.pageActions.update) {
        this.passingYearService.update(this.passingYear.id, item).subscribe(
          response => {
            this.layoutService.flashMsg({ msg: 'Passing Year has been updated.', msgType: 'success' });
            this.router.navigate([
              `/settings/passingYear`
            ]);
          },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
          }, () => {
            this.showDivBar = false;
          }
        );
      }
    }


  }

  /**
   * Delete record
   */
  deleteRecord(id: number) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.passingYearService.delete(id).subscribe(response => {
          this.layoutService.flashMsg({ msg: 'Passing Year has been deleted.', msgType: 'success' });
          this.router.navigate(['/settings/passingYear']);
        },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
          }
        );
      }
    });
  }
}
