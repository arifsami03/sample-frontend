import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router } from '@angular/router';

import { PageAnimation } from '../../../../shared/helper';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { LayoutService } from '../../../../layout/services';
import { PassingYearService } from '../../../services';
import { PassingYearModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'passing-year-list',
  templateUrl: './passing-year-list.component.html',
  styleUrls: ['./passing-year-list.component.css'],
  animations: [PageAnimation]
})
export class PassingYearListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;
  public data: PassingYearModel[] = [new PassingYearModel()];
  public attrLabels = PassingYearModel.attributesLabels;

  displayedColumns = ['year', 'examinationType', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public error: Boolean;

  constructor(
    private passingYearService: PassingYearService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Passing Year' });
    this.getRecords();
  }

  /**
   * Get records
   */
  getRecords() {
    this.passingYearService.index().subscribe(
      response => {
        this.data = response;
        for (let i = 0; i < response.length; i++) {
          if (response[i].annual && response[i].supplimentary) {
            this.data[i].examinationType = "Annual , Supplimentary";
          }
          else if (response[i].annual) {
            this.data[i].examinationType = "Annual";
          }
          else if (response[i].supplimentary) {
            this.data[i].examinationType = "Supplimentary";
          }
        }

        this.dataSource = new MatTableDataSource<PassingYearModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
    return this.data;
  }

  /**
   * Apply filter and serch in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete record
   */
  deletePassingYear(id: number) {
    // Confirm dialog
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.passingYearService.delete(id).subscribe(response => {
            this.layoutService.flashMsg({ msg: 'Passing Year has been deleted.', msgType: 'success' });
            this.getRecords();
          },
            error => {
              console.log(error);
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              this.loaded = true;
            }
          );
        }
      });
  }

  sortData() {
    this.dataSource.sort = this.sort;
  }
}
