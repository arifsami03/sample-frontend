import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { PageAnimation } from '../../../../shared/helper';
import { BankService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { BankModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'setting-bank-form',
  templateUrl: './bank-form.component.html',
  styleUrls: ['./bank-form.component.css'],
  animations: [PageAnimation]
})
export class BankFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public showDivBar = false;
  public pageActions = GLOBALS.pageActions;

  public error: Boolean;
  public fg: FormGroup;
  public pageAct: string;
  public bank: BankModel;

  public componentLabels = BankModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private bankService: BankService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * Initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new BankModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get all data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.bankService.find(params['id']).subscribe(
        response => {
          this.bank = response;
          this.fg.patchValue(response);
          if (!this.error) {
            if (this.pageAct === this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct === this.pageActions.update) {
              this.initUpdatePage();
            }
            this.loaded = true;
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Initialize create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Bank: Create' });
    this.bank = new BankModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * Initialize view page
   */
  private initViewPage() {
    this.fg.disable();
    this.layoutService.setPageTitle({
      title: 'Bank: ' + this.bank.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Bank ', url: '/settings/configurations/banks' },
        { label: this.bank.name }
      ]
    });
  }

  /**
   * Initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Bank: ' + this.bank.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Bank', url: '/settings/configurations/banks' },
        {
          label: this.bank.name,
          url: `/settings/configurations/banks/view/${this.bank.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Add or update data in database when save button is clicked
   */
  public saveData(item: BankModel) {
    this.showDivBar = true;

    item.abbreviation = item.abbreviation.toUpperCase();

    let id;
    if (this.pageAct === this.pageActions.create) {
      item.id = null;
      this.bankService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Bank has been created.', msgType: 'success' });
          this.router.navigate([`/settings/configurations/banks`]);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        }, () => {
          this.showDivBar = false;
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.bankService.update(this.bank.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Bank has been updated.', msgType: 'success' });
          this.router.navigate([
            `/settings/configurations/banks`
          ]);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        }, () => {
          this.showDivBar = false;
        }
      );
    }
  }

  /**
   * Delete record
   */
  deleteRecord(id: number) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.bankService.delete(id).subscribe(response => {
          this.layoutService.flashMsg({ msg: 'Bank has been deleted.', msgType: 'success' });
          this.router.navigate(['/settings/configurations/banks']);
        },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
          }
        );
      }
    });
  }
}
