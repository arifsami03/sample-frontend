import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router } from '@angular/router';

import { PageAnimation } from '../../../../shared/helper';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { LayoutService } from '../../../../layout/services';
import { BankService } from '../../../services';
import { BankModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'setting-bank-list',
  templateUrl: './bank-list.component.html',
  styleUrls: ['./bank-list.component.css'],
  animations: [PageAnimation]
})
export class BankListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;
  public data: BankModel[] = [new BankModel()];
  public attrLabels = BankModel.attributesLabels;

  displayedColumns = ['name', 'abbreviation', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public error: Boolean;

  constructor(
    private bankService: BankService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Banks' });
    this.getRecords();
  }

  /**
   * Get records
   */
  getRecords() {
    this.bankService.index().subscribe(
      response => {
        this.data = response;
        this.dataSource = new MatTableDataSource<BankModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
    return this.data;
  }

  /**
   * Apply filter and serch in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete record
   */
  deleteBank(id: number) {
    // Confirm dialog
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.pageState = '';

          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.bankService.delete(id).subscribe(response => {
            this.layoutService.flashMsg({ msg: 'Bank has been deleted.', msgType: 'success' });
            this.getRecords();
          },
            error => {
              console.log(error);
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              this.loaded = true;
              this.pageState = 'active';

            },()=>{
              this.loaded = true;

              this.pageState = 'active';

            }
          );
        }
      });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }
}
