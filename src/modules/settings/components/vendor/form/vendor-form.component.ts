import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from '../../../../shared/helper';
import { GLOBALS } from '../../../../app/config/globals';
import { VendorService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { VendorModel } from '../../../models';
import { ConfigurationService } from '../../../../shared/services';


// for Mobile Number
interface ICountryOption {
  abbreviation : string;
  countryCode : string;
} 
@Component({
  selector: 'vendor-form',
  templateUrl: './vendor-form.component.html',
  styleUrls: ['./vendor-form.component.css'],
  animations: [PageAnimation]
})
export class VendorFormComponent implements OnInit {
  public pageState = 'active';

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public boxLoaded: boolean = true;

  public mobileMask = GLOBALS.masks.mobile;

  public fg: FormGroup;
  public pageAct: string;
  public vendor: VendorModel;
  public options: Observable<string[]>;
  public componentLabels = VendorModel.attributesLabels;



  // for Mobile Number
  public countriesOptions: ICountryOption[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private vendorService: VendorService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private configurationService : ConfigurationService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    this.findCountryAttrbutesList();

  }


  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new VendorModel().validationRules());

    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get record
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.vendorService.find(params['id']).subscribe(
        response => {
          this.vendor = response;
          if (this.vendor) {
            
            this.fg.patchValue(this.vendor);
            this.patchMobileNumber();
            //TODO:low need to check why we are dowing this after fetching data
            if (this.pageAct == this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct == this.pageActions.update) {

              this.initUpdatePage();
            }
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {
    this.loaded = true;
    this.layoutService.setPageTitle({ title: 'Vendor: Create' });

    this.vendor = new VendorModel();

    this.fg.enable();
  }

  /**
   * initialize the view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Vendor: ' + this.vendor.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Vendor ',
          url: '/settings/vendor'
        },
        { label: this.vendor.name }
      ]
    });
  }

  /**
   * initialize the update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Vendor: ' + this.vendor.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Vendor',
          url: '/settings/vendor'
        },
        {
          label: this.vendor.name,
          url: `/settings/vendor/view/${this.vendor.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: VendorModel) {
    this.boxLoaded = false;
    item.mobileNumber = item['countryCode'] + '-' + item.mobileNumber;
    if (this.pageAct === this.pageActions.create) {
      this.vendorService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Vendor has been created.', msgType: 'success' });
          this.router.navigate([`/settings/vendor`]);
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
          this.boxLoaded = true;
        },
        () => {
          this.boxLoaded = true;
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.vendorService.update(this.vendor.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Vendor has been updated.', msgType: 'success' });
          this.router.navigate([`/settings/vendor`]);
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
          this.boxLoaded = true;
        },
        () => {
          this.boxLoaded = true;
        }
      );
    }
  }


  /**
   * Delete record
   *
   */
  delete(id: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          this.vendorService.delete(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Vendor has been deleted.', msgType: 'success' });
              this.router.navigate(['/settings/vendor']);
            },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
            }
          );
        }
      });
  }

 // For Mobile Number
  // Getting the countries from configuratiuon and then pushing into the array of object
  findCountryAttrbutesList() {
    this.configurationService.findAttributesList('country').subscribe(
      response => {
        // iterate countries
        response.forEach(country => {
          let abbr;
          let cCode;
          // iterate countries config options
          country.configurationOptions.forEach(configurationOption => {
            // check if option is abbreviation
           if (configurationOption.key === 'abbreviation'){
             abbr = configurationOption.value
             // check if option is country code
           }else if(configurationOption.key === 'countryCode'){
             cCode = configurationOption.value
           }
           // if both exists then push into the array
           if (abbr && cCode){

            this.countriesOptions.push({abbreviation : abbr, countryCode : cCode })
           }
         });
        });
      },
      error => console.log(error),
      () => { }
    );
  }
  patchMobileNumber(){
    // for first mobile number
    let mobileNumber : string = this.vendor.mobileNumber;
    let countryCode : string = mobileNumber.substring(0,mobileNumber.indexOf('-'));
    let mobileNumberSecondPart: string = mobileNumber.substring(mobileNumber.indexOf('-')+1,mobileNumber.length);
    
    this.fg.get('countryCode').patchValue(countryCode);
    this.fg.get('mobileNumber').patchValue(mobileNumberSecondPart);

  }
}
