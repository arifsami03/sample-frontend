import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from '../../../../shared/helper';
import { GLOBALS } from '../../../../app/config/globals';
import { BoardService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { BoardModel, BoardNOCNotRequiredModel } from '../../../models';
import { AddNOCDialogComponent } from '../add_noc/add-noc.component';
import { ConfigurationService } from '../../../../shared/services';

// for Mobile Number
interface ICountryOption {
  abbreviation: string;
  countryCode: string;
}
@Component({
  selector: 'board-form',
  templateUrl: './board-form.component.html',
  styleUrls: ['./board-form.component.css'],
  animations: [PageAnimation]
})
export class BoardFormComponent implements OnInit {
  public pageState = 'active';

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public boxLoaded: boolean = true;

  public mobileMask = GLOBALS.masks.mobile;

  public fg: FormGroup;
  public pageAct: string;
  public board: BoardModel;
  public boardNOC: BoardModel[];
  public options: Observable<string[]>;
  public componentLabels = BoardModel.attributesLabels;

  // for Mobile Number
  public countriesOptions: ICountryOption[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private boardService: BoardService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private configurationService: ConfigurationService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    this.findCountryAttrbutesList();

  }


  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new BoardModel().validationRules());

    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get record
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.boardService.find(params['id']).subscribe(
        response => {
          this.board = response;
          if (this.board) {

            this.fg.patchValue(this.board);
            this.patchMobileNumber();
            //TODO:low need to check why we are dowing this after fetching data
            if (this.pageAct == this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct == this.pageActions.update) {

              this.initUpdatePage();
            }
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.findAllNOCNotRequiredBoards(this.board.id);
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {
    this.loaded = true;
    this.layoutService.setPageTitle({ title: 'Board: Create' });

    this.board = new BoardModel();

    this.fg.enable();
  }

  /**
   * initialize the view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Board: ' + this.board.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Board ',
          url: '/settings/board'
        },
        { label: this.board.name }
      ]
    });
  }

  /**
   * initialize the update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Board: ' + this.board.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Board',
          url: '/settings/board'
        },
        {
          label: this.board.name,
          url: `/settings/board/view/${this.board.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: BoardModel) {
    this.boxLoaded = false;

    item.abbreviation = item.abbreviation.toUpperCase();

    item.mobileNumber = item['countryCode'] + '-' + item.mobileNumber;
    if (this.pageAct === this.pageActions.create) {
      this.boardService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Board has been created.', msgType: 'success' });
          this.router.navigate([`/settings/board/view/${response.id}`]);
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        },
        () => {
          this.boxLoaded = true;
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.boardService.update(this.board.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Board has been updated.', msgType: 'success' });
          this.router.navigate([`/settings/board/view/${this.board.id}`]);
        },
        error => {
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          console.log(error);
        },
        () => {
          this.boxLoaded = true;
        }
      );
    }
  }


  /**
   * Delete record
   *
   */
  delete(id: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          this.boardService.delete(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Board has been deleted.', msgType: 'success' });
              this.router.navigate(['/settings/board']);
            },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
            }
          );
        }
      });
  }


  /*****************************************************************************************************************************
   * Board NOC functionality
   * 
   * ***************************************************************************************************************************/
  findAllNOCNotRequiredBoards(boardId: number) {
    this.boardService.findAllNOCNotRequiredBoards(boardId).subscribe(
      response => {
        this.boardNOC = response;
      },
      error => {
        console.log(error);
      },
      () => {
      }
    );
  }

  addBoardNOC(id: number) {
    const dialogRef = this.matDialog.open(AddNOCDialogComponent, {
      width: '500px',
      data: { boardId: id, boardName: this.board.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.findAllNOCNotRequiredBoards(this.board.id);
      }
    });
  }

  deleteBoardNOC(id: number) {
    this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.boardService.deleteNOCNotRequuired(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'NOC has been deleted.', msgType: 'success' });
              this.findAllNOCNotRequiredBoards(this.board.id);
            },
            error => {
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              console.log(error);
            }
          );
        }
      });
  }
  // For Mobile Number
  // Getting the countries from configuratiuon and then pushing into the array of object
  findCountryAttrbutesList() {
    this.configurationService.findAttributesList('country').subscribe(
      response => {
        // iterate countries
        response.forEach(country => {
          let abbr;
          let cCode;
          // iterate countries config options
          country.configurationOptions.forEach(configurationOption => {
            // check if option is abbreviation
            if (configurationOption.key === 'abbreviation') {
              abbr = configurationOption.value
              // check if option is country code
            } else if (configurationOption.key === 'countryCode') {
              cCode = configurationOption.value
            }
            // if both exists then push into the array
            if (abbr && cCode) {

              this.countriesOptions.push({ abbreviation: abbr, countryCode: cCode })
            }
          });
        });
      },
      error => console.log(error),
      () => { }
    );
  }

  patchMobileNumber() {
    // for first mobile number
    let mobileNumber: string = this.board.mobileNumber;
    let countryCode: string = mobileNumber.substring(0, mobileNumber.indexOf('-'));
    let mobileNumberSecondPart: string = mobileNumber.substring(mobileNumber.indexOf('-') + 1, mobileNumber.length);

    this.fg.get('countryCode').patchValue(countryCode);
    this.fg.get('mobileNumber').patchValue(mobileNumberSecondPart);

  }


}
