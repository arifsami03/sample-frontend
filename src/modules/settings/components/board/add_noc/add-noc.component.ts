import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { GLOBALS } from '../../../../app/config/globals';
import { BoardModel, BoardNOCNotRequiredModel } from '../../../models';
import { BoardService } from '../../../services';
import { LayoutService } from '../../../../layout/services';

/**
 * This component is used to add NOC (not required) board to a board an acts as dialog component 
 */

@Component({
  selector: 'setting-board-add-noc',
  templateUrl: './add-noc.component.html',
  styleUrls: ['./add-noc.component.css']
})
export class AddNOCDialogComponent {
  public loaded: boolean = false;
  private boardId: number;
  public boardName: string;
  public fg: FormGroup;
  public componentLabels = BoardModel.attributesLabels;

  public boards: BoardModel[];
  public bnocModel: BoardNOCNotRequiredModel;

  constructor(
    public dialogRef: MatDialogRef<AddNOCDialogComponent>,
    private fb: FormBuilder,
    private boardService: BoardService,
    private layoutService: LayoutService,

    @Inject(MAT_DIALOG_DATA) public data: any
  ) {

  }

  ngOnInit() {
    this.boardId = this.data.boardId;
    this.boardName = this.data.boardName;
    this.fg = this.fb.group(new BoardNOCNotRequiredModel().singleRecordValidationRules());
    this.fg.get('boardId').setValue(this.boardId);
    this.findAllBoardsToAddNOC(this.boardId);
  }

  private findAllBoardsToAddNOC(boardId: number) {
    this.boardService.findAllBoardsToAddNOC(boardId).subscribe(
      response => {
        this.boards = response;
      },
      error => {
        this.boards = [new BoardModel()];
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }

  private create(item: BoardNOCNotRequiredModel) {
    this.boardService.addNOCNotRequired(item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'NOC has beed added.', msgType: 'success' });
        this.dialogRef.close(true);
      },
      error => {
        this.loaded = true;
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        console.log(error);
      },
      () => {
        this.loaded = true;
      }
    );
  }


  onNoClick(): void {
    this.dialogRef.close(false);
  }


}
