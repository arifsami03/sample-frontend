import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { PageAnimation } from '../../../../shared/helper';
import { WorkFlowService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { WorkFlowModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'work-flow-view',
  templateUrl: './work-flow-view.component.html',
  styleUrls: ['./work-flow-view.component.css'],
  animations: [PageAnimation]
})
export class WorkFlowViewComponent implements OnInit {

  public pageState = 'active';
  
  public loaded: boolean = false;
  public pageActions = GLOBALS.pageActions;

  public error: Boolean;

  public fg: FormGroup;
  public pageAct: string;
  public workFlow: WorkFlowModel;
  public id: string;
  public componentLabels = WorkFlowModel.attributesLabels;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private workFlowService: WorkFlowService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new WorkFlowModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * get all data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.id = params['WFId'];
      this.workFlowService.find(params['WFId']).subscribe(
        response => {
          this.workFlow = response[0];
          this.fg.patchValue(response[0]);
          if (!this.error) {
            if (this.pageAct === this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct === this.pageActions.update) {
              this.initUpdatePage();
            }
            this.loaded = true;
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        }
      );
    });
  }

  /**
   * initialize create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Work Flow: Create' });
    this.workFlow = new WorkFlowModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * initialize view page
   */
  private initViewPage() {
    this.fg.disable();
    this.layoutService.setPageTitle({
      title: 'Work Flow: ' + this.workFlow.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Work Flow ', url: '/settings/workFlow' },
        { label: this.workFlow.title }
      ]
    });
  }

  /**
   * initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Work Flow: ' + this.workFlow.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Work Flow', url: '/settings/workFlow' },
        {
          label: this.workFlow.WFId,
          url: `/settings/workFlow/view/${this.workFlow.WFId}`
        },
        { label: 'Update' }
      ]
    });
  }


  /**
   * delete record
   */
  deleteRecord(WFId: string) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: 'By Deleting this the Association with State Flows also Delete, Are you Sure?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.workFlowService.delete(WFId).subscribe(response => {
          this.layoutService.flashMsg({ msg: 'Work Flow has been deleted.', msgType: 'success' });
          this.router.navigate(['/settings/workFlow']);
        },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
          }
        );
      }
    });
  }
}
