import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';

import { WFStateFlowService, WFStateService, EmailTemplateService } from '../../../../../services';
import { LayoutService } from '../../../../../../layout/services';

import { WFStateModel, WFStateFlowModel, EmailTemplateModel } from '../../../../../models';

@Component({
  selector: 'wf-state-flow-form',
  templateUrl: './wf-state-flow-form.component.html',
  styleUrls: ['./wf-state-flow-form.component.css']
})
export class WFStateFlowFormComponent {
  public loaded: boolean = false;
  public wfState: WFStateModel[];
  public fg: FormGroup;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;
  public success: Boolean;
  public state: string;
  public componentLabels = WFStateFlowModel.attributesLabels;

  public emailTemplates: EmailTemplateModel[];

  public selectedEmailTemplates: number[] = [];
  public selectedEmailTemplate: number;

  constructor(
    private wfStateService: WFStateService,
    private wfStateFlowService: WFStateFlowService,
    private emailTemplateService: EmailTemplateService,
    private layoutService: LayoutService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<WFStateFlowFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.loaded = true;
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    this.getWFState();
    this.findEmailTemplateAttibutesList();
    if (this.data.type != 'Add Flow') {
      this.getFlowData();
    }
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new WFStateFlowModel().validationRules(), { emailTemplateList: this.fb.array([]) });
    // this.addEmailTemplatesClick();
  }

  getWFState() {
    this.wfStateService.list(this.data.WFId).subscribe(
      response => {
        this.wfState = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  getFlowData() {
    this.wfStateFlowService.findById(this.data.id).subscribe(
      response => {
        //this.state=response.state;
        for (let i = 0; i < response['emailTemplateList'].length; i++) {
          // pushing into array of selected pre req list
          this.selectedEmailTemplates.push(response['emailTemplateList'][i].emailTemplateId);
          this.addEmailTemplatesClick();
        }
        this.fg.patchValue(response);
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close({ response: false });
  }

  /**
   * Add or update data in database when save button is clicked
   */
  public saveData(item: WFStateFlowModel) {
    item.WFId = this.data.WFId;
    this.saveRecord(item);
    //  this.checkRecordExist(item);
  }
  saveRecord(item: WFStateFlowModel) {
    item['emailTemplates'] = this.selectedEmailTemplates;
    if (this.data.type === 'Add Flow') {
      this.wfStateFlowService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'State Flow has been created.', msgType: 'success' });
          this.dialogRef.close({ response: false });
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        }
      );
    } else {
      this.wfStateFlowService.update(this.data.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'State Flow has been updated.', msgType: 'success' });
          this.dialogRef.close({ response: false });
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        }
      );
    }
  }

  // public isRecordExists: boolean = false;
  // checkRecordExist(item: WFStateModel) {
  //   this.wfStateService.checkRecordExists(item).subscribe(
  //     response => {
  //       if (response.length != 0 && response[0].state !=this.state ) {
  //         this.fg.controls['state'].setErrors({ exist: true });
  //       }
  //       else {
  //         this.saveRecord(item);
  //       }
  //     },
  //     error => {
  //       console.log(error);
  //       this.loaded = true;
  //     },
  //     () => {
  //       this.loaded = true;
  //     }
  //   );

  // }
  findEmailTemplateAttibutesList() {
    this.emailTemplateService.findAttributesList().subscribe(
      response => {
        this.emailTemplates = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }
  createEmailTemplateItem(): FormGroup {
    return this.fb.group({
      emailTemplateId: ''
    });
  }

  addEmailTemplatesClick(): void {
    let emailTemplateList = this.fg.get('emailTemplateList') as FormArray;
    emailTemplateList.push(this.createEmailTemplateItem());

    if (emailTemplateList.value[emailTemplateList.value.length - 2]) {
      let value = emailTemplateList.value[emailTemplateList.value.length - 2].emailTemplateId;
      const index = this.selectedEmailTemplates.indexOf(value);
      if (index < 0 && value) {
        this.selectedEmailTemplates.push(value);
      }
    }
  }

  removeEmailTemplates(i: number, item) {
    let control = <FormArray>this.fg.controls['emailTemplateList'];
    const index = this.selectedEmailTemplates.indexOf(control.value[i].emailTemplateId);
    if (index > -1) {
      this.selectedEmailTemplates.splice(index, 1);
    }
    // if selected remove then selected Course should be set -1
    if (this.selectedEmailTemplates === control.value[i].emailTemplateId) {
      this.selectedEmailTemplate = -1;
    }
    control.removeAt(i);
  }
  eventEmailTemplateSelection(event) {
    this.selectedEmailTemplate = event.value;
  }
}
