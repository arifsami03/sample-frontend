import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';

import { PageAnimation } from '../../../../../../shared/helper';
import { WFStateFlowService } from '../../../../../services';
import { LayoutService } from '../../../../../../layout/services';
import { WFStateFlowFormComponent } from '../form/wf-state-flow-form.component';
import { ConfirmDialogComponent } from '../../../../../../shared/components';
import { WFStateFlowModel } from '../../../../../models';
import { GLOBALS } from '../../../../../../app/config/globals';

@Component({
  selector: 'wf-state-flow',
  templateUrl: './wf-state-flow.component.html',
  styleUrls: ['./wf-state-flow.component.css'],
  animations: [PageAnimation]
})
export class WFStateFlowComponent implements OnInit {

  public pageState = 'active';
  
  public loaded: boolean = false;
  public pageActions = GLOBALS.pageActions;

  public error: Boolean;
  displayedColumns = ['from', 'to', 'options'];
  public fg: FormGroup;
  public pageAct: string;
  public wfStateFlow: WFStateFlowModel[];
  public id: string;
  dataSource: any;
  public componentLabels = WFStateFlowModel.attributesLabels;
  @Input() WFId: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private wfStateFlowService: WFStateFlowService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.wfStateFlowService.reloadStateFlow$.subscribe((res) => {
      if (res == true) {
        this.getData();
      }
    })
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  loadFlows(event) {
    this.getData();
  }
  /**
   * initialize page
   */
  private initializePage() {

    this.getData();

  }

  /**
   * get all data
   */
  private getData() {
    this.route.params.subscribe(params => {

      this.wfStateFlowService.list(this.WFId).subscribe(
        response => {
          this.wfStateFlow = response;
          this.dataSource = new MatTableDataSource<WFStateFlowModel>(this.wfStateFlow);

        },
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    });
  }

  addStateClick() {
    const dialogRef = this.matDialog.open(WFStateFlowFormComponent, {
      width: '700px',
      data: { WFId: this.WFId, type: 'Add Flow' }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }
  editClick(id: number, state: string) {
    const dialogRef = this.matDialog.open(WFStateFlowFormComponent, {
      width: '700px',
      data: { WFId: this.WFId, type: 'Update Flow ', id: id }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });

  }

  deleteWFStateFlow(id: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          this.wfStateFlowService.delete(id).subscribe(response => {
            this.layoutService.flashMsg({ msg: 'State Flow has been deleted.', msgType: 'success' });
            this.getData();
          },
            error => {
              console.log(error);
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              this.loaded = true;
            }
          );
        }
      });
  }

}
