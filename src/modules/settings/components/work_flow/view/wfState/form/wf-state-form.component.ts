import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

import { WFStateService } from '../../../../../services';
import { LayoutService } from '../../../../../../layout/services';

import { WFStateModel } from '../../../../../models';

@Component({
  selector: 'wf-state-form',
  templateUrl: './wf-state-form.component.html',
  styleUrls: ['./wf-state-form.component.css']
})
export class WFStateFormComponent {
  public loaded: boolean = false;
  public wfState: WFStateModel;
  public fg: FormGroup;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;
  public success: Boolean;
  public state: string;
  public editMode: boolean = false;
  public componentLabels = WFStateModel.attributesLabels;

  constructor(
    private wfStateService: WFStateService,
    private fb: FormBuilder,
    private layoutService: LayoutService,
    public dialogRef: MatDialogRef<WFStateFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.loaded = true;
  }

  /**
  * ngOnInit
  */
  ngOnInit() {
    this.initializePage();
    if (this.data.type != 'Add State') {
      this.editMode = true;
      this.getStateData();
    }
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new WFStateModel().validationRules());

  }

  getStateData() {
    this.wfStateService.findById(this.data.id).subscribe(
      response => {
        this.state = response.state;
        this.fg.patchValue(response);
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }


  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close({ response: false });
  }

  /**
 * Add or update data in database when save button is clicked
 */
  public saveData(item: WFStateModel) {
    item.WFId = this.data.WFId
    this.checkRecordExist(item);
  }
  saveRecord(item: WFStateModel) {

    if (this.data.type === 'Add State') {
      this.wfStateService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'State has been created.', msgType: 'success' });
          this.dialogRef.close({ response: false });
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        }
      );
    }
    else {
      this.wfStateService.update(this.data.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'State has been updated.', msgType: 'success' });
          this.dialogRef.close({ response: false });
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        }
      );
    }
  }

  public isRecordExists: boolean = false;
  checkRecordExist(item: WFStateModel) {
    this.wfStateService.checkRecordExists(item).subscribe(
      response => {
        if (response.length != 0 && response[0].state != this.state) {
          this.fg.controls['state'].setErrors({ exist: true });
        }
        else {
          this.saveRecord(item);
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );

  }


}
