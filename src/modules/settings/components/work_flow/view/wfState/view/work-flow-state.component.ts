import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';

import { PageAnimation } from '../../../../../../shared/helper';
import { WFStateService, WFStateFlowService } from '../../../../../services';
import { LayoutService } from '../../../../../../layout/services';
import { WFStateFormComponent } from '../form/wf-state-form.component';
import { ConfirmDialogComponent } from '../../../../../../shared/components';
import { WFStateModel } from '../../../../../models';
import { GLOBALS } from '../../../../../../app/config/globals';

@Component({
  selector: 'work-flow-state',
  templateUrl: './work-flow-state.component.html',
  styleUrls: ['./work-flow-state.component.css'],
  animations: [PageAnimation]
})
export class WorkFlowStateComponent implements OnInit {

  public pageState = 'active';
  
  public loaded: boolean = false;
  public pageActions = GLOBALS.pageActions;

  public error: Boolean;
  listViewOptions = {
    labels: WFStateModel.attributesLabels,
    fields: [
      { name: 'state', width: 25 },
      { name: 'title', width: 25 },
      { name: 'description', width: 50 },
    ],
    height: '30px'
  };
  displayedColumns = ['state', 'title','description', 'options'];
  public fg: FormGroup;
  public pageAct: string;
  public wfState: WFStateModel[];
  public id: string;
  dataSource: any;
  public componentLabels = WFStateModel.attributesLabels;
  @Input() WFId: string;
  @Output() loadFlows = new EventEmitter<any>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private wdStateService: WFStateService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private stateFlowService: WFStateFlowService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * initialize page
   */
  private initializePage() {

    this.getData();

  }

  /**
   * get all data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.wdStateService.list(this.WFId).subscribe(
        response => {
          this.wfState = response;
          this.dataSource = new MatTableDataSource<WFStateModel>(this.wfState);
        },
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    });
  }

  addStateClick() {
    const dialogRef = this.matDialog.open(WFStateFormComponent, {
      width: '700px',
      data: { WFId: this.WFId, type: 'Add State' }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getData();
    });
  }
  editClick(id: number, state: string) {
    const dialogRef = this.matDialog.open(WFStateFormComponent, {
      width: '700px',
      data: { WFId: this.WFId, type: 'Update State : ' + state, id: id }
    });
    dialogRef.afterClosed().subscribe(result => {
      // if (result && result.response) {
      this.getData();
      //}
    });

  }

  deleteState(id: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: 'By Deleting this the Association with State Flows also Delete, Are you Sure?' }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          this.wdStateService.delete(id).subscribe(response => {
            this.layoutService.flashMsg({ msg: 'State has been deleted.', msgType: 'success' });
            this.getData();
            this.stateFlowService.reloadStateFlow(true);
          },
            error => {
              console.log(error);
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
              this.loaded = true;
            }, () => {
              this.loaded = true;
            }
          );
        }
      });
  }

}
