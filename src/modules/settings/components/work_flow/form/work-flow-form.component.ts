import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ConfirmDialogComponent } from '../../../../shared/components';

import { PageAnimation } from '../../../../shared/helper';
import { WorkFlowService } from '../../../services';
import { LayoutService } from '../../../../layout/services';
import { WorkFlowModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';

@Component({
  selector: 'work-flow-form',
  templateUrl: './work-flow-form.component.html',
  styleUrls: ['./work-flow-form.component.css'],
  animations: [PageAnimation]
})
export class WorkFlowFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public pageActions = GLOBALS.pageActions;

  public error: Boolean;
  public editMode: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public workFlow: WorkFlowModel;
  public id: string;
  public componentLabels = WorkFlowModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private workFlowService: WorkFlowService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new WorkFlowModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.editMode = true;
      this.getData();
    }
  }

  /**
   * get all data
   */
  private getData() {
    this.route.params.subscribe(params => {

      this.id = params['WFId'];
      this.workFlowService.find(params['WFId']).subscribe(
        response => {
          this.workFlow = response[0];
          this.fg.patchValue(response[0]);
          if (!this.error) {
            if (this.pageAct === this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct === this.pageActions.update) {
              this.initUpdatePage();
            }
            this.loaded = true;
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        }
      );
    });
  }

  /**
   * initialize create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Work Flow: Create' });
    this.workFlow = new WorkFlowModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * initialize view page
   */
  private initViewPage() {
    this.fg.disable();
    this.layoutService.setPageTitle({
      title: 'Work Flow: ' + this.workFlow.WFId,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Work Flow ', url: '/settings/workFlow' },
        { label: this.workFlow.WFId }
      ]
    });
  }

  /**
   * initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Work Flow: ' + this.workFlow.WFId,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Work Flow', url: '/settings/workFlow' },
        {
          label: this.workFlow.WFId,
          url: `/settings/workFlow/view/${this.workFlow.WFId}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Add or update data in database when save button is clicked
   */
  public saveData(item: WorkFlowModel) {
    this.checkRecordExist(item);
  }


  saveRecord(item: WorkFlowModel) {
    let WFId;
    if (this.pageAct === this.pageActions.create) {
      this.workFlowService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Work Flow has been created.', msgType: 'success' });
          this.router.navigate([`/settings/workFlow/view/${response.WFId}`]);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.workFlowService.update(this.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Work Flow has been updated.', msgType: 'success' });
          this.router.navigate([
            `/settings/workFlow/view/${item.WFId}`
          ]);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        }
      );
    }
  }

  public isRecordExists: boolean = false;

  /**
   * Function to Check that is record of Workflow Already Exists or not.
   */
  checkRecordExist(item: WorkFlowModel) {
    this.workFlowService.checkRecordExists(item).subscribe(
      response => {

        if (response.length != 0 && response[0]['WFId'] != this.id) {
          this.fg.controls['WFId'].setErrors({ exist: true });
        }
        else {
          this.saveRecord(item);
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );

  }

  /**
   * delete record
   */
  deleteRecord(WFId: string) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: 'By Deleting this the Association with State Flows also Delete, Are you Sure?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.workFlowService.delete(WFId).subscribe(response => {
          this.layoutService.flashMsg({ msg: 'Work Flow has been deleted.', msgType: 'success' });
          this.router.navigate(['/settings/workFlow']);
        },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
          }
        );
      }
    });
  }
}
