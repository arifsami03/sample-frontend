import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from '../../../../../shared/helper';
import { EmailTemplateService, OutGoingMailServerService } from '../../../../services';
import { LayoutService } from '../../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../../shared/components';
import { EmailTemplateModel, OutGoingMailServerModel } from '../../../../models';
import { GLOBALS } from '../../../../../app/config/globals';

@Component({
  selector: 'setting-email-template-form',
  templateUrl: './email-template-form.component.html',
  styleUrls: ['./email-template-form.component.css'],
  animations: [PageAnimation]
})
export class EmailTemplateFormComponent implements OnInit {
  public pageState = 'active';

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public outGoingMailServers: OutGoingMailServerModel[];
  // success and Error
  public success: Boolean;
  public successMessage: String;
  public errorMessage: String;
  public error: Boolean;

  public fg: FormGroup;
  public pageAct: string;
  public emailTemplate: EmailTemplateModel;
  public options: Observable<string[]>;
  public componentLabels = EmailTemplateModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private emailTemplateService: EmailTemplateService,
    private outGoingMailServerService: OutGoingMailServerService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    this.findOutGoingMailServer();
  }

  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new EmailTemplateModel().validationRules());

    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get record
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.emailTemplateService.find(params['id']).subscribe(
        response => {
          if (response) {
            this.emailTemplate = response;

            this.fg.patchValue(this.emailTemplate);

            //TODO:low need to check why we are dowing this after fetching data
            if (this.pageAct == this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct == this.pageActions.update) {
              this.initUpdatePage();
            }
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Email Template: Create' });

    this.emailTemplate = new EmailTemplateModel();

    this.fg.enable();

    this.loaded = true;
  }

  /**
   * initialize the view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Email Tempalte: ' + this.emailTemplate.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Email Template ',
          url: '/settings/emailTemplates'
        },
        { label: this.emailTemplate.name }
      ]
    });
  }

  /**
   * initialize the update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Email Template: ' + this.emailTemplate.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Out Going Mail Server',
          url: '/settings/emailTemplates'
        },
        {
          label: this.emailTemplate.name,
          url: `/settings/emailTemplates/view/${this.emailTemplate.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: EmailTemplateModel) {
    if (this.pageAct === this.pageActions.create) {
      this.emailTemplateService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Email Template has been created.', msgType: 'success' });
          this.router.navigate([`/settings/emailTemplates/view/${response.id}`]);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.emailTemplateService.update(this.emailTemplate.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Email Template has been updated.', msgType: 'success' });
          this.router.navigate([`/settings/emailTemplates/view/${this.emailTemplate.id}`]);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      );
    }
  }

  /**
   * Delete record
   *
   */
  delete(id: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          this.emailTemplateService.delete(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Email Template has been deleted.', msgType: 'success' });
              this.router.navigate(['/settings/emailTemplates']);
            },
            error => {
              console.log(error);
              this.loaded = true;
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            }
          );
        }
      });
  }
  findOutGoingMailServer() {
    this.outGoingMailServerService.findAttributesList().subscribe(
      _servers => {
        this.outGoingMailServers = _servers;
      },
      error => {},
      () => {}
    );
  }
}
