import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { PageAnimation } from '../../../../../shared/helper';
import { GLOBALS } from '../../../../../app/config/globals';
import { ConfirmDialogComponent } from '../../../../../shared/components';
import { LayoutService } from '../../../../../layout/services';
import { EmailTemplateService } from '../../../../services';
import { EmailTemplateModel } from '../../../../models';

@Component({
  selector: 'setting-email-template-list',
  templateUrl: './email-template-list.component.html',
  styleUrls: ['./email-template-list.component.css'],
  animations: [PageAnimation]
})
export class EmailTemplateListComponent implements OnInit {
  public pageState;

  public loaded: boolean = false;

  public data: EmailTemplateModel[] = [new EmailTemplateModel()];

  public attrLabels = EmailTemplateModel.attributesLabels;

  displayedColumns = ['name', 'subject', 'options'];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(private emailTemplateService: EmailTemplateService, public dialog: MatDialog, public matDialog: MatDialog, private router: Router, public layoutService: LayoutService) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Email Template' });

    this.getRecords();
  }

  /**
   * Get data/records from backend
   */
  getRecords() {
    this.emailTemplateService.index().subscribe(
      response => {
        if (response) {
          this.data = response;
          this.dataSource = new MatTableDataSource<EmailTemplateModel>(this.data);
          
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete record
   */

  /**
   * Delete record
   */
  delete(id: number) {
    // Confirm dialog
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.emailTemplateService.delete(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Email Template has been deleted.', msgType: 'success' });
              this.getRecords();
              setTimeout((router: Router) => {
                this.success = false;
              }, 1000);
            },
            error => {
              this.loaded = true;
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            }
          );
        }
      });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }
}
