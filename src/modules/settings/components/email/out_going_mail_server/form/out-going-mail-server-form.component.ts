import { UserService } from '../../../../../security/services';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { PageAnimation } from '../../../../../shared/helper';
import { OutGoingMailServerService } from '../../../../services';
import { LayoutService } from '../../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../../shared/components';
import { OutGoingMailServerModel } from '../../../../models';
import { User } from '../../../../../security/models';
import { GLOBALS } from '../../../../../app/config/globals';

@Component({
  selector: 'setting-out-going-mail-server-form',
  templateUrl: './out-going-mail-server-form.component.html',
  styleUrls: ['./out-going-mail-server-form.component.css'],
  animations: [PageAnimation]
})
export class OutGoingMailServerFormComponent implements OnInit {

  public pageState = 'active';

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public users: User[];
  // success and Error
  public success: Boolean;
  public successMessage: String;
  public errorMessage: String;
  public error: Boolean;

  public fg: FormGroup;
  public pageAct: string;
  public outGoingMailServer: OutGoingMailServerModel;
  public options: Observable<string[]>;
  public connections = [{ value: '', viewValue: 'None' },{ value: 'ssl', viewValue: 'SSL' }, { value: 'tls', viewValue: 'TLS' }, { value: 'ssl-tls', viewValue: 'SSL/TLS' }];
  public componentLabels = OutGoingMailServerModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private outGoingMailServerService: OutGoingMailServerService,
    private userService: UserService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    this.getUserByPortal();
  }

  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new OutGoingMailServerModel().validationRules());

    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get record
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.outGoingMailServerService.find(params['id']).subscribe(
        response => {
          if (response) {
            this.outGoingMailServer = response;

            this.fg.patchValue(this.outGoingMailServer);

            //TODO:low need to check why we are dowing this after fetching data
            if (this.pageAct == this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct == this.pageActions.update) {
              this.initUpdatePage();
            }
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Out Going Mail Server: Create' });

    this.outGoingMailServer = new OutGoingMailServerModel();

    this.fg.enable();

    this.loaded = true;
  }

  /**
   * initialize the view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Out Going Mail Server: ' + this.outGoingMailServer.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Out Going Mail Server ',
          url: '/settings/outGoingMailServers'
        },
        { label: this.outGoingMailServer.name }
      ]
    });
  }

  /**
   * initialize the update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Out Going Mail Server: ' + this.outGoingMailServer.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Out Going Mail Server',
          url: '/settings/outGoingMailServers'
        },
        {
          label: this.outGoingMailServer.name,
          url: `/settings/outGoingMailServers/view/${this.outGoingMailServer.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: OutGoingMailServerModel) {
    if (this.pageAct === this.pageActions.create) {
      this.outGoingMailServerService.create(item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Out Going Mail Server has been created.', msgType: 'success' });
          this.router.navigate([`/settings/outGoingMailServers/view/${response.id}`]);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.outGoingMailServerService.update(this.outGoingMailServer.id, item).subscribe(
        response => {
          this.layoutService.flashMsg({ msg: 'Out Going Mail Server has been updated.', msgType: 'success' });
          this.router.navigate([`/settings/outGoingMailServers/view/${this.outGoingMailServer.id}`]);
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        }
      );
    }
  }

  /**
   * Delete record
   *
   */
  delete(id: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          this.outGoingMailServerService.delete(id).subscribe(
            response => {
              this.layoutService.flashMsg({ msg: 'Out Going Mail Server has been deleted.', msgType: 'success' });
              this.router.navigate(['/settings/outGoingMailServers']);
            },
            error => {
              console.log(error);
              this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            }
          );
        }
      });
  }
  getUserByPortal() {
    this.userService.findAttributesList().subscribe(
      _userByPortal => {
        this.users = _userByPortal;
      },
      error => { },
      () => { }
    );
  }
}
