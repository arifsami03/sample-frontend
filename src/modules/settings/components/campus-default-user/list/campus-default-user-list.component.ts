import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { PageAnimation } from '../../../../shared/helper';
import { LayoutService } from '../../../../layout/services';
import { CampusDefaultUserService } from '../../../services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { CampusDefaultUserModel } from '../../../models';
import { GLOBALS } from '../../../../app/config/globals';
import { CampusDefaultUserFormComponent } from '../form/campus-default-user-form.component';

@Component({
  selector: 'campus-default-user-list',
  templateUrl: './campus-default-user-list.component.html',
  styleUrls: ['./campus-default-user-list.component.css'],
  animations: [PageAnimation]
})
export class CampusDefaultUserListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;
  public data: CampusDefaultUserModel[] = [new CampusDefaultUserModel()];
  public feeTypes = GLOBALS.FEE_TYPES;
  public attrLabels = CampusDefaultUserModel.attributesLabels;

  displayedColumns = ['userName', 'abbreviation', 'roleId', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public success: Boolean;

  constructor(
    private campusDefaultUserService: CampusDefaultUserService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Campus Default Users' });
    this.getRecords();
  }

  /**
   * Get all records
   */
  getRecords(): void {
    this.campusDefaultUserService.index().subscribe(
      response => {
        this.data = response;

        

        this.dataSource = new MatTableDataSource<CampusDefaultUserModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }

    );
  }

  /**
   * search
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete data against the id given
   */
  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.campusDefaultUserService.delete(id).subscribe(response => {
          this.layoutService.flashMsg({ msg: 'Campus Default User has been deleted.', msgType: 'success' });
          this.getRecords();
        },
          error => {
            console.log(error);
            this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
            this.loaded = true;
          },
          () => {
            this.getRecords();
            this.loaded = true;
          }
        );
      }
    });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }

  addCampusDefaultUser(){
    const dialogRef = this.matDialog.open(CampusDefaultUserFormComponent, {
      width: '700px',
      data: { pageAct: 'create',}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getRecords();
      }
    });
  }
  editCampusDefaultUser(id){
    const dialogRef = this.matDialog.open(CampusDefaultUserFormComponent, {
      width: '700px',
      data: { pageAct: 'update',id:id}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getRecords();
      }
    });
  }

}
