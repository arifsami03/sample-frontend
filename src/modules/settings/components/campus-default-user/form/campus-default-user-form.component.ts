import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GLOBALS } from '../../../../app/config/globals';
import { PageAnimation } from '../../../../shared/helper';

import { LayoutService } from '../../../../layout/services';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { RoleService } from '../../../../security/services';
import { Role } from '../../../../security/models';
import { CampusDefaultUserService } from '../../../services';
import { CampusDefaultUserModel } from '../../../models';

@Component({
  selector: 'campus-default-user-form',
  templateUrl: './campus-default-user-form.component.html',
  styleUrls: ['./campus-default-user-form.component.css'],
  animations: [PageAnimation],
  providers: [RoleService]
})
export class CampusDefaultUserFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public boxLoaded = true;
  public pageActions = GLOBALS.pageActions;
  public error: Boolean;
  public roles: Role[];
  public fg: FormGroup;
  public pageAct: string;
  public title: string;
  public campusDefaultUser: CampusDefaultUserModel;
  public feeTypes = GLOBALS.FEE_TYPES;

  public componentLabels = CampusDefaultUserModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private campusDefaultUserService: CampusDefaultUserService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private roleService: RoleService,
    public dialogRef: MatDialogRef<CampusDefaultUserFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new CampusDefaultUserModel().validationRules());
    this.getRoleList();
    if (this.data.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get Data
   */
  private getData() {
      this.campusDefaultUserService.find(this.data.id).subscribe(
        response => {
          this.campusDefaultUser = response;

          this.fg.patchValue(this.campusDefaultUser);
          if (!this.error) {
            if (this.data.pageAct === this.pageActions.update) {
              this.initUpdatePage();
            }
            this.loaded = true;
          }
        },

        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
  }

  /**
   * Get Level of Education List
   */
  getRoleList() {
    this.roleService.getAll().subscribe(
      response => {
        this.roles = response;
      },
      error => console.log(error),
      () => { }
    );
  }

  onNoClick(): void {

    this.dialogRef.close(false);

  }

  /**
   * Initialize page
   */
  private initCreatePage() {
    this.title = 'Campus Default User: Create';
    this.campusDefaultUser = new CampusDefaultUserModel();
    this.fg.enable();

    this.loaded = true;
  }


  /**
   * Initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.title = 'Campus Default User: ' + this.campusDefaultUser.userName

  }

  /**
   * Add or Update when save button is clicked
   */
  public saveData(item: CampusDefaultUserModel) {
    this.boxLoaded = false;
    let id;
    if (this.data.pageAct === this.pageActions.create) {
      item.id = null;
      this.campusDefaultUserService.create(item).subscribe(
        response => {
          if (response) {
            this.layoutService.flashMsg({ msg: 'Campus Default User has been created.', msgType: 'success' });
            this.dialogRef.close(true);
          }
        },

        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        },
        () => {
          this.loaded = true;
          this.boxLoaded = true;
        }
      );
    } else if (this.data.pageAct === this.pageActions.update) {
      this.campusDefaultUserService.update(this.campusDefaultUser.id, item).subscribe(
        response => {
          if (response) {
            this.layoutService.flashMsg({ msg: 'Campus Default User has been updated.', msgType: 'success' });
            this.dialogRef.close(true);
          }
        },
        error => {
          console.log(error);
          this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
          this.loaded = true;
        },
        () => {
          this.loaded = true;
          this.boxLoaded = true;
        }
      );
    }
  }

  avoidSpace(evt ) {  
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 32 ) {
        return false;
    }
    return true;

  }

}
