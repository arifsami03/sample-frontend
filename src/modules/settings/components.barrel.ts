// Import angular and material modules here
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { CampusService } from '../campus/services';
import {
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatDividerModule,
  MatIconModule,
  MatTableModule,
  MatPaginatorModule,
  MatButtonModule,
  MatTooltipModule,
  MatCardModule,
  MatCheckboxModule,
  MatSortModule,
  MatChipsModule,
  MatRadioModule,
  MatProgressBarModule,
  MatExpansionModule,
  MatAutocompleteModule,
  MatDatepickerModule
} from '@angular/material';
import {
  BankService,
  OutGoingMailServerService,
  EmailTemplateService,
  WorkFlowService,
  WFStateService,
  WFStateFlowService,
  EvaluationSheetService,
  ESSectionService,
  ESQuestionService,
  ESCommunicationService,
  ESCategoryService,
  PassingYearService,
  CampusDefaultUserService
} from './services';
import { ConfigurationService } from '../shared/services';

import {
  BoardService,
  VendorService
} from './services';

import { AcademicCalendarService, CourseService, ProgramService, ProgramDetailsService, ClassesService } from '../academic/services';

import { FlexLayoutModule } from '@angular/flex-layout';
import { DocStateManagerService } from '../shared/services/doc-state-manager.service';
import {
  DashboardComponent,
  BankFormComponent,
  BankListComponent,
  OutGoingMailServerListComponent,
  OutGoingMailServerFormComponent,
  EmailTemplateListComponent,
  EmailTemplateFormComponent,
  WorkFlowFormComponent,
  WorkFlowListComponent,
  WorkFlowViewComponent,
  WorkFlowStateComponent,
  WFStateFormComponent,
  WFStateFlowComponent,
  WFStateFlowFormComponent,
  EvaluationFormComponent,
  EvaluationListComponent,
  EvaluationViewComponent,
  ESSectionViewComponent,
  ESSectionFormComponent,
  ESQuestionViewComponent,
  ESQuestionFormComponent,
  ESCategoryListComponent,
  ESCategoryFormComponent,
  PassingYearListComponent,
  PassingYearFormComponent,
  BoardListComponent,
  BoardFormComponent,
  AddNOCDialogComponent,
  VendorListComponent,
  VendorFormComponent,
  CampusDefaultUserListComponent,
  CampusDefaultUserFormComponent
} from './components';
// Import components services here

export const __IMPORTS = [
  MatFormFieldModule,
  MatInputModule,
  FormsModule,
  ReactiveFormsModule,
  MatListModule,
  MatSelectModule,
  MatDividerModule,
  MatIconModule,
  MatTableModule,
  MatPaginatorModule,
  MatButtonModule,
  MatTooltipModule,
  MatCardModule,
  FlexLayoutModule,
  MatCheckboxModule,
  MatSortModule,
  MatChipsModule,
  MatRadioModule,
  MatProgressBarModule,
  MatExpansionModule,
  TextMaskModule,
  MatAutocompleteModule,
  MatDatepickerModule
];

export const __DECLARATIONS = [
  DashboardComponent,
  BankFormComponent,
  BankListComponent,
  OutGoingMailServerListComponent,
  OutGoingMailServerFormComponent,
  EmailTemplateListComponent,
  EmailTemplateFormComponent,
  WorkFlowFormComponent,
  WorkFlowListComponent,
  WorkFlowViewComponent,
  WorkFlowStateComponent,
  WFStateFormComponent,
  WFStateFlowComponent,
  WFStateFlowFormComponent,
  EvaluationFormComponent,
  EvaluationListComponent,
  EvaluationViewComponent,
  ESSectionViewComponent,
  ESSectionFormComponent,
  ESQuestionViewComponent,
  ESQuestionFormComponent,
  ESCategoryListComponent,
  ESCategoryFormComponent,
  PassingYearListComponent,
  PassingYearFormComponent,
  BoardListComponent,
  BoardFormComponent,
  AddNOCDialogComponent,
  VendorListComponent,
  VendorFormComponent,
  CampusDefaultUserListComponent,
  CampusDefaultUserFormComponent
];

export const __PROVIDERS = [
  ConfigurationService,
  BankService,
  OutGoingMailServerService,
  EmailTemplateService,
  WorkFlowService,
  WFStateService,
  WFStateFlowService,
  EvaluationSheetService,
  ESSectionService,
  ESQuestionService,
  ESCommunicationService,
  DocStateManagerService,
  ESCategoryService,
  PassingYearService,
  AcademicCalendarService,
  ProgramService,
  ProgramDetailsService,
  ClassesService,
  CourseService,
  CourseService,
  BoardService,
  VendorService,
  CampusService,
  CampusDefaultUserService
];

export const __ENTRY_COMPONENTS = [WFStateFormComponent, WFStateFlowFormComponent,CampusDefaultUserFormComponent, ESSectionFormComponent, ESQuestionFormComponent, AddNOCDialogComponent];
