import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  BankFormComponent,
  BankListComponent,
  DashboardComponent,
  OutGoingMailServerListComponent,
  OutGoingMailServerFormComponent,
  EmailTemplateListComponent,
  EmailTemplateFormComponent,
  WorkFlowFormComponent,
  WorkFlowListComponent,
  WorkFlowViewComponent,
  EvaluationFormComponent,
  EvaluationListComponent,
  EvaluationViewComponent,
  ESCategoryListComponent,
  ESCategoryFormComponent,
  PassingYearListComponent,
  PassingYearFormComponent,
  BoardListComponent,
  BoardFormComponent,
  VendorFormComponent,
  VendorListComponent,
  CampusDefaultUserListComponent
} from './components';

import { ConfigurationComponent } from '../shared/components';

import { GLOBALS } from '../app/config/globals';
import { AuthGuardService } from '../app/services';

/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardService],
    component: DashboardComponent,
    data: { breadcrumb: { title: 'Dashboard', display: true } }
  },
  {
    path: 'configurations',
    data: { breadcrumb: { title: 'Configurations', display: false } },
    children: [
      {
        path: 'country',
        canActivate: [AuthGuardService],
        component: ConfigurationComponent,
        data: {
          key: GLOBALS.configurationKeys.country,
          title: 'Country',
          breadcrumb: { title: 'Country', display: true }
        }
      },
      {
        path: 'province',
        canActivate: [AuthGuardService],
        component: ConfigurationComponent,
        data: {
          key: GLOBALS.configurationKeys.province,
          title: 'Province',
          breadcrumb: { title: 'Province', display: true }
        }
      },
      {
        path: 'city',
        canActivate: [AuthGuardService],
        component: ConfigurationComponent,
        data: {
          key: GLOBALS.configurationKeys.city,
          title: 'City',
          breadcrumb: { title: 'City', display: true }
        }
      },
      {
        path: 'tehsil',
        canActivate: [AuthGuardService],
        component: ConfigurationComponent,
        data: {
          key: GLOBALS.configurationKeys.tehsil,
          title: 'Tehsil',
          breadcrumb: { title: 'Tehsil', display: true }
        }
      },
      {
        path: 'natureOfFeeHeads',
        canActivate: [AuthGuardService],
        component: ConfigurationComponent,
        data: {
          key: GLOBALS.configurationKeys.natureOfFeeHeads,
          title: 'Nature of Fee Head',
          breadcrumb: { title: 'Nature of Fee Head', display: true }
        }
      },
      {
        path: 'typeOfFeeHeads',
        canActivate: [AuthGuardService],
        component: ConfigurationComponent,
        data: {
          key: GLOBALS.configurationKeys.typeOfFeeHeads,
          title: 'Type of Fee Head',
          breadcrumb: { title: 'Type of Fee Head', display: true }
        }
      },
      {
        path: 'natureofwork',
        canActivate: [AuthGuardService],
        component: ConfigurationComponent,
        data: {
          key: GLOBALS.configurationKeys.natureOfWork,
          title: 'Nature of Work',
          breadcrumb: { title: 'Nature of Work', display: true }
        }
      },
      {
        path: 'natureOfCourse',
        canActivate: [AuthGuardService],
        component: ConfigurationComponent,
        data: {
          key: GLOBALS.configurationKeys.natureOfCourse,
          title: 'Nature of Course',
          breadcrumb: { title: 'Nature of Course', display: true }
        }
      },
      // {
      //   path: 'maxCreditHour',
      //   canActivate: [AuthGuardService],
      //   component: ConfigurationComponent,
      //   data: {
      //     key: GLOBALS.configurationKeys.maxCreditHour,
      //     title: 'Max Credit Hour',
      //     breadcrumb: { title: 'Max Credit Hour', display: true }
      //   }
      // },
      {
        path: 'instrumentType',
        canActivate: [AuthGuardService],
        component: ConfigurationComponent,
        data: {
          key: GLOBALS.configurationKeys.instrumentType,
          title: 'Instrument Type',
          breadcrumb: { title: 'Instrument Type', display: true }
        }
      },
      {
        path: 'singleConf',
        canActivate: [AuthGuardService],
        component: ConfigurationComponent,
        data: {
          key: GLOBALS.configurationKeys.singleConf,
          title: 'App Configurations',
          breadcrumb: { title: 'App Configurations', display: true }
        }
      },
      {
        path: 'banks',
        data: { breadcrumb: { title: 'Banks', display: true } },
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            component: BankListComponent,
            data: { breadcrumb: { title: 'Banks', display: false } }
          },
          {
            path: 'create',
            canActivate: [AuthGuardService],
            component: BankFormComponent,
            data: {
              act: 'create',
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            canActivate: [AuthGuardService],
            component: BankFormComponent,
            data: { act: 'view' }
          },
          {
            path: 'update/:id',
            canActivate: [AuthGuardService],
            component: BankFormComponent,
            data: { act: 'update' }
          }
        ]
      },
      {
        path: 'concessionCategories',
        canActivate: [AuthGuardService],
        component: ConfigurationComponent,
        data: {
          key: GLOBALS.configurationKeys.concessionCategory,
          title: 'Concession Category',
          breadcrumb: { title: 'Concession Category', display: true }
        }
      },
      {
        path: 'concessionTypes',
        canActivate: [AuthGuardService],
        component: ConfigurationComponent,
        data: {
          key: GLOBALS.configurationKeys.concessionType,
          title: 'Concession Type',
          breadcrumb: { title: 'Concession Type', display: true }
        }
      }
    ]
  },

  {
    path: 'outGoingMailServers',
    data: { breadcrumb: { title: 'Out Going Mail Server', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: OutGoingMailServerListComponent,
        data: { breadcrumb: { title: 'Out Going Mail Server', display: false } }
      },
      {
        path: 'create',
        canActivate: [AuthGuardService],
        component: OutGoingMailServerFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        canActivate: [AuthGuardService],
        component: OutGoingMailServerFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        canActivate: [AuthGuardService],
        component: OutGoingMailServerFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'workFlow',
    data: { breadcrumb: { title: 'Work Flow', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: WorkFlowListComponent,
        data: { breadcrumb: { title: 'Work Flow List', display: false } }
      },
      {
        path: 'create',
        canActivate: [AuthGuardService],
        component: WorkFlowFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:WFId',
        canActivate: [AuthGuardService],
        component: WorkFlowViewComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:WFId',
        canActivate: [AuthGuardService],
        component: WorkFlowFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'passingYear',
    data: { breadcrumb: { title: 'Passing Year', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: PassingYearListComponent,
        data: { breadcrumb: { title: 'Passing Year List', display: false } }
      },
      {
        path: 'create',
        canActivate: [AuthGuardService],
        component: PassingYearFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        canActivate: [AuthGuardService],
        component: PassingYearFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        canActivate: [AuthGuardService],
        component: PassingYearFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'board',
    data: { breadcrumb: { title: 'Boards', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: BoardListComponent,
        data: { breadcrumb: { title: 'Board List', display: false } }
      },
      {
        path: 'create',
        canActivate: [AuthGuardService],
        component: BoardFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        canActivate: [AuthGuardService],
        component: BoardFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        canActivate: [AuthGuardService],
        component: BoardFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'vendor',
    data: { breadcrumb: { title: 'Vendor', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: VendorListComponent,
        data: { breadcrumb: { title: 'Vendor List', display: false } }
      },
      {
        path: 'create',
        canActivate: [AuthGuardService],
        component: VendorFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        canActivate: [AuthGuardService],
        component: VendorFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        canActivate: [AuthGuardService],
        component: VendorFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'campusDefaultUsers',
    data: { breadcrumb: { title: 'Campus Default User', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: CampusDefaultUserListComponent,
        data: { breadcrumb: { title: 'Campus Default User List', display: false } }
      },
      
    ]
  },
  {
    path: 'emailTemplates',
    data: { breadcrumb: { title: 'Email Template', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: EmailTemplateListComponent,
        data: { breadcrumb: { title: 'Email Template', display: false } }
      },
      {
        path: 'create',
        canActivate: [AuthGuardService],
        component: EmailTemplateFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        canActivate: [AuthGuardService],
        component: EmailTemplateFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        canActivate: [AuthGuardService],
        component: EmailTemplateFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'esCategories',
    data: { breadcrumb: { title: 'ES Categories', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: ESCategoryListComponent,
        data: { breadcrumb: { title: 'ES Categories', display: false } }
      },
      {
        path: 'create',
        canActivate: [AuthGuardService],
        component: ESCategoryFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        canActivate: [AuthGuardService],
        component: ESCategoryFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        canActivate: [AuthGuardService],
        component: ESCategoryFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'evaluationSheets',
    data: { breadcrumb: { title: 'Evaluation Sheet', display: true } },
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: EvaluationListComponent,
        data: { breadcrumb: { title: 'Evaluation Sheet', display: false } }
      },
      {
        path: 'create',
        canActivate: [AuthGuardService],
        component: EvaluationFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'update/:id',
        canActivate: [AuthGuardService],
        component: EvaluationFormComponent,
        data: { act: GLOBALS.pageActions.update }
      },
      {
        path: 'view/:id',
        canActivate: [AuthGuardService],
        component: EvaluationViewComponent,
        data: { act: GLOBALS.pageActions.view }
      }
    ]
  },

  { path: '**', redirectTo: '' }
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutes { }
