import { Component, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Subscription } from 'rxjs/Subscription';
import { LayoutService } from '../../../services/layout.service';


@Component({
  selector: 'prospectus-print',
  templateUrl: './prospectus-print.component.html',
  styleUrls: ['./prospectus-print.component.css'],

})
export class ProspectusPrintComponent {

  @Input() data: any;

  constructor() {


  }

  ngOnInit() {
    console.log('i am in Prospectus Print', this.data)
    setTimeout(function () {
      window.print();
    }, 1000)
  }

}
