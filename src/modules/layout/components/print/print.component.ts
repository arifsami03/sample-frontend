import { Component, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Subscription } from 'rxjs/Subscription';
import { LayoutService } from '../../services/layout.service';
import { PrintService } from '../../services/print.service';

@Component({
  selector: 'print',
  templateUrl: './print.component.html',
  styleUrls: ['./print.component.css'],

})
export class PrintComponent {

  public fmSubscription: Subscription;

  data: any;

  constructor(private printService: PrintService) {

  
  }

  ngOnInit() {
      this.fmSubscription = this.printService.sendDataMethod$.subscribe((data) => {
        console.log('i am Khan',this.data)
      this.data = data; // And he have data here too!
      console.log('i am Khan',this.data)
    }
    )
  }

  /** 
   * Destroy the world :D
   * 
  */
  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.fmSubscription.unsubscribe();
  }
}
