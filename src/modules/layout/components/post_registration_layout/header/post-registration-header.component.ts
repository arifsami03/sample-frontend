import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-registration-header',
  templateUrl: './post-registration-header.component.html',
  styleUrls: ['./post-registration-header.component.css']
})
export class PostRegistrationHeaderComponent implements OnInit {
  public username: string;

  constructor(private router: Router) {}
  ngOnInit() {
    this.username = localStorage.getItem('username');
  }
  logout() {
    localStorage.clear();
    this.router.navigate(['home']);
  }
}
