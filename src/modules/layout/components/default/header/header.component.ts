import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { LayoutService } from '../../../services';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  public username: string;
  public appTitle: string = 'Idrak';
  public subscription: Subscription;
  // logo image
  logo = { name: 'logo', path: 'assets/images/munivLogo.png' };

  constructor(private router: Router, private layoutService: LayoutService) {
    // currenty Modified Application Title
    this.subscription = this.layoutService.setTitle$.subscribe(response => {
      this.appTitle = response;
    });
  }
  /**
   * ngOnInit()
   */
  ngOnInit() {
    this.username = localStorage.getItem('username');
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  /**
   * logout
   */
  logout() {
    localStorage.clear();
    this.router.navigate(['home']);
  }
}
