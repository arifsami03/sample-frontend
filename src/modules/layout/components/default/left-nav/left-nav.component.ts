import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'layout-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.css']
})
export class LeftNavComponent implements OnInit {
  public clicked: string = 'dashboard';

  links = [
    { text: 'Dashboard', link: '/dashboard', icon: 'dashboard' },
    { text: 'Link 1', link: '/link1', icon: 'view_list' },
    { text: 'Link 2', link: '/link2', icon: 'vpn_key' },
    {
      text: 'Remittance',
      link: '/post-registration/campus/remittance',
      icon: 'view_list'
    },
    {
      text: 'Country',
      link: '/settings/configuration/country',
      icon: 'view_list'
    },
    {
      text: 'Province',
      link: '/settings/configuration/province',
      icon: 'view_list'
    },
    { text: 'City', link: '/settings/configuration/city', icon: 'view_list' },
    {
      text: 'Tehsil',
      link: '/settings/configuration/tehsil',
      icon: 'view_list'
    },
    {
      text: 'Nature of Work',
      link: '/settings/configuration/natureofwork',
      icon: 'view_list'
    },
    // { text: 'Security', link: '/security/users', icon: 'vpn_key' },
    { text: 'Personal Information', link: '/personalinfo', icon: 'info' },
    { text: 'Institute', link: '/institute', icon: 'business' },

    { text: 'Users', link: '/security/users', icon: 'people' },
    { text: 'Groups', link: '/security/groups', icon: 'group' },
    { text: 'Roles', link: '/security/roles', icon: 'view_list' }
  ];

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  /**
   * ngOnInit()
   */
  ngOnInit() {
    this.clicked = this.router.url;
  }

  /**
   * route links for main navigations
   * @param link String
   */
  getChildType(link: string) {
    this.clicked = link;

    this.router.navigate([link]);
    // complaints, suggestions and appreciations are located in cms module so we have to route them with cms prefix
    // TODO:high , have to proper implement routing
    // if (link === 'dashboard' || link === 'changePassword') {
    //   this.router.navigate(['home', link]);
    // } else if (link === 'cms') {
    //   this.router.navigate(['cms', link]);
    // } else {
    //   this.router.navigate([link]);
    // }
  }
}
