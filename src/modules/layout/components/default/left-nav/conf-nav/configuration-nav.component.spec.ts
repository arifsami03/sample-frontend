import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationNavComponent } from './settings-nav.component';

describe('ConfigurationNavComponent', () => {
  let component: ConfigurationNavComponent;
  let fixture: ComponentFixture<ConfigurationNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurationNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
