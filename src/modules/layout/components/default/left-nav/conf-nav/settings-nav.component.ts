import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'layout-settings-nav',
  templateUrl: './settings-nav.component.html',
  styleUrls: ['./settings-nav.component.css']
})
export class SettingsNavComponent implements OnInit {
  public clicked: string;
  configLinks = [
    {
      text: 'Designation',
      link: '/settings/designation',
      icon: 'view_list'
    },
    {
      text: 'Department',
      link: '/settings/department',
      icon: 'view_list'
    },
    { text: 'Gender', link: '/settings/gender', icon: 'view_list' },
    {
      text: 'Application Title',
      link: '/settings/appTitle',
      icon: 'view_list'
    },
    { text: 'Feedback', link: '/settings/feedback', icon: 'view_list' },
    {
      text: 'Marital Status',
      link: '/settings/maritalStatus',
      icon: 'view_list'
    },
    { text: 'MS', link: '/settings/ms', icon: 'person' }
    // { text: 'Video', link: 'uploadVideo' }
  ];

  constructor(private router: Router, private route: ActivatedRoute) { }

  /**
   * ngOnInit()
   */
  ngOnInit() {
    this.clicked = this.router.url;
  }

  /**
   * route links for settings
   * @param link String
   */
  getConfigType(link: string) {
    this.clicked = link;
    this.router.navigate([link]);
  }
}
