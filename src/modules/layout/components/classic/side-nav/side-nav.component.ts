import { Component, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {
  MatPaginator,
  MatTableDataSource,
  MatSort
} from '@angular/material';
import { Subscription } from 'rxjs/Subscription';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatSidenav } from '@angular/material/sidenav';
import { Output, EventEmitter } from '@angular/core';
import { LayoutService } from '../../../services';
/**
 * HOW TO USE IT
 * 
 * Examples
 *    <shared-list-view [data]="wfState" [options]="listViewOptions"></shared-list-view>
 * 
 * NOW IN TS FILE
 *    
 *    listViewOptions = {
    labels: YourModel.attributesLabels,
    fields: [
      { name: 'state', width: 20 },
      { name: 'title', width: 20 },
      { name: 'description', width: 20 },
    ]
  };
 */
@Component({
  selector: 'layout-classic-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {

  // public isShow: boolean = false;

  public isOpen = false;

  public subscription: Subscription;
  reason = '';

  // we use this 'showSideNav' parameter for not showing scroll bar
  // @Output() showSideNav = new EventEmitter<boolean>();

  @ViewChild('sidenav') sidenav: MatSidenav;

  constructor(private layoutService: LayoutService) {

    this.subscription = this.layoutService.toggleSideNav$.subscribe(res => {
      // this.isShow = message;
      if (res) {
        if (this.isOpen == false) {
          this.sidenav.open();
          this.isOpen = true;
        }
        else {
          this.sidenav.close();
          this.isOpen = false;
        }

        // this.showSideNav.emit(this.isShow);
      }
      else {
      }
    });
  }


  close(reason: string) {
    this.reason = reason;
    // this.showSideNav.emit(false);
    this.sidenav.close();
  }

  ngOnInit() {
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}
