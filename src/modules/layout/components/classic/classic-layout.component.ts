import { Subscription } from 'rxjs/Subscription';
import { LayoutService } from '../../services';
import { Component, OnDestroy, ViewChild, Input } from '@angular/core';
import { element } from 'protractor';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'layout-classic',
  templateUrl: './classic-layout.component.html',
  styleUrls: ['./classic-layout.component.css']
})
export class ClassicLayoutComponent implements OnDestroy {
  public isShow = true;
  public subNavSub: Subscription;
  @ViewChild('sidenav') sidenav: MatSidenav;
  public subNavActive: boolean = false;
  // public sideNav: boolean;
  constructor(private layoutService: LayoutService) {
    this.subNavSub = this.layoutService.subNav$.subscribe(response => {
      this.subNavActive = response;
    });
  }
  close(reason: string) {
    this.sidenav.close();
  }

  ngOnDestroy() {
    this.subNavSub.unsubscribe();
  }

  /**
   * For removing scroll bar
   * @param element 
   */
  onShowSideNav(element) {
    this.isShow = element;
    this.isShow = !this.isShow;
  }
}
