import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LayoutService } from '../../../services';
import { Subscription } from 'rxjs';

import { InstituteMenu } from '../../../../institute/config/institute-menu';
import { CampusMenu } from '../../../../campus/config/campus-menu';
import { SecurityMenu } from '../../../../security/config/security-menu';
import { SettingMenu } from '../../../../settings/config/settings-menu';

@Component({
  selector: 'layout-classic-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.css']
})
export class ClassicLeftNavComponent implements OnInit {

  public loaded = false;

  public clicked: string = 'dashboard';

  public layoutSubscription: Subscription;

  public moduleMenuItems: any[];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private layoutService: LayoutService
  ) {
    this.layoutSubscription = this.layoutService.initModuleNav$.subscribe(moduleMenu => {

      if (moduleMenu) {
        this.assignModuleMenu(moduleMenu);
      }
    }
    );
  }

  /**
   * ngOnInit()
   */
  ngOnInit() {

    this.clicked = this.router.url;
  }

  private assignModuleMenu(moduleMenu) {

    this.moduleMenuItems = null;

    this.moduleMenuItems = moduleMenu;

    this.loaded = false;

    this.sortPermission();

    if (this.moduleMenuItems.length) {
      // this.navigateToUrl(this.moduleMenuItems[0]['menuItems'][0]);
      this.loaded = true;
    } else {
      console.log('This module has no menu item with perm attribute');
    }
  }


  //TODO:high: Following is a big chapee. Did it because of node is maintaing references and when we splic the record from object
  // It splic it from where it was first initilized and to make changing happen we need to refresh it.
  public sortPermission() {

    // Convert permission string to object array
    let permissionsAry = JSON.parse(localStorage.getItem('permissions'));

    // Store permissionsAry indexes to delete it if there is no permission ture in its menuItems
    let spliceIndexes = [];

    // Iterate moduleMenuItems which contains headings and menuItems
    for (let c1 = 0; c1 < this.moduleMenuItems.length; c1++) {

      let MMItem = this.moduleMenuItems[c1];

      let found = false;

      // Iterate menuItems in each index of moduleMenu
      for (let c2 = 0; c2 < MMItem['menuItems'].length; c2++) {

        // Get permission name i.e. 'institute.update'
        let permissionName = MMItem['menuItems'][c2]['perm'];

        // If permission name is provided
        if (permissionName && permissionName != 'undefined') {

          // find that item in permissionsAry
          let foundItem = permissionsAry.find(o => o['name'].toLowerCase() == permissionName.toLowerCase());

          // Display menu if it is not found in permissionAry or found and it is true
          if (!foundItem || (foundItem && foundItem['status'] == true)) {

            found = true;
          }
        }
      }

      if (found == false) {
        spliceIndexes.push(c1);
      }
    }

    // Set all heading menu ture 
    for (let c4 = 0; c4 < this.moduleMenuItems.length; c4++) {
      this.moduleMenuItems[c4]['display'] = true;
    }

    // Now just set false only those which have no sub menu active
    for (let c3 = 0; c3 < spliceIndexes.length; c3++) {
      this.moduleMenuItems[spliceIndexes[c3]]['display'] = false;
      // this.moduleMenuItems.splice(spliceIndexes[c3]);
    }

  }
  /**
   * route links for main navigations
   * @param link String
   */
  public navigateToUrl(item) {
    this.clicked = item.title;
    this.router.navigate(item.url);
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.layoutSubscription.unsubscribe();
  }
}
