import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LayoutService } from '../../../services';
import { ModuleMenu } from '../../../config/module-menu';

@Component({
  selector: 'layout-classic-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class ClassicHeaderComponent implements OnInit {

  public moduleMenu = ModuleMenu;
  public isShow: boolean = false;
  public username: string;
  public userId: number;

  public selectedModuleName: string;

  constructor(private router: Router, private layoutService: LayoutService) { }

  /**
   * ngOnInit()
   */
  ngOnInit() {

    this.checkForModulePermission();

    this.username = localStorage.getItem('username');
    this.userId = +localStorage.getItem('id');

    // If page is refreshed then maintain the navigation state
    let urlAry = this.router.url.split('/');
    if (urlAry.length > 1) {
      if(urlAry[1] != 'profile'){
        let item = this.moduleMenu.find(o => o.name === urlAry[1]);
        if(item){
          this.navigateAndLoadMenu(item);
        }
        
      }
      
    }
  }

  /**
   * logout
   */
  logout() {
    localStorage.clear();
    this.router.navigate(['home']);
  }

  public navigateAndLoadMenu(item) {

    let moduleMenu = item['moduleMenu'];

    this.selectedModuleName = item['name'];

    this.layoutService.initModuleNav(moduleMenu);
  }

  private checkForModulePermission() {
    
    this.moduleMenu.forEach((item) => {
      
      item['display'] = (this.checkForPermission(item['moduleMenu']) == true) ? true : false;

    })
  }

  private checkForPermission(moduleMenuItems) {

    let modHasPerm = false;

    // Convert permission string to object array
    let permissionsAry = JSON.parse(localStorage.getItem('permissions'));

    // Store permissionsAry indexes to delete it if there is no permission ture in its menuItems
    let spliceIndexes = [];

    // Iterate moduleMenuItems which contains headings and menuItems
    for (let c1 = 0; c1 < moduleMenuItems.length; c1++) {

      let MMItem = moduleMenuItems[c1];

      let found = false;

      // Iterate menuItems in each index of moduleMenu
      for (let c2 = 0; c2 < MMItem['menuItems'].length; c2++) {

        // Get permission name i.e. 'institute.update'
        let permissionName = MMItem['menuItems'][c2]['perm'];

        // If permission name is provided
        if (permissionName && permissionName != 'undefined') {

          // find that item in permissionsAry
          let foundItem = permissionsAry.find(o => o['name'].toLowerCase() == permissionName.toLowerCase());

          // Display menu if it is not found in permissionAry or found and it is true
          if (!foundItem || (foundItem && foundItem['status'] == true)) {
            modHasPerm = true;
            found = true;
          }
        }
      }

      if (found == false) {
        spliceIndexes.push(c1);
      }
    }

    // Set all heading menu ture 
    for (let c4 = 0; c4 < moduleMenuItems.length; c4++) {
      moduleMenuItems[c4]['display'] = true;
    }

    // Now just set false only those which have no sub menu active
    for (let c3 = 0; c3 < spliceIndexes.length; c3++) {
      moduleMenuItems[spliceIndexes[c3]]['display'] = false;
      // moduleMenuItems.splice(spliceIndexes[c3]);
    }

    return modHasPerm;

  }

  emitShowSideNav() {
    this.isShow = true;
    this.layoutService.toggleSideNav(this.isShow);
  }
}
