import {
  Router,
  ActivatedRoute,
  NavigationEnd,
  PRIMARY_OUTLET
} from '@angular/router';
import 'rxjs/add/operator/filter';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { LayoutService } from '../../../../services';

import { IBreadcrumb } from '../../../../models';

@Component({
  selector: 'layout-classic-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.css']
})
export class ClassicSubHeaderComponent implements OnInit, OnDestroy {
  public pageTitle: string;
  public breadcrumbs: IBreadcrumb[];
  public pageTitleSubscription: Subscription;
  public breadCrumbsSubscription: Subscription;

  constructor(
    private layoutService: LayoutService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.breadcrumbs = [];
  }

  ngOnInit() {
    /**
     * Implemented via hard coded array of bread crumbs
     */
    this.pageTitleSubscription = this.layoutService.setPageTitle$.subscribe(
      response => {
        this.pageTitle = response['title'];
        
        if (!response['breadCrumbs'] || response['breadCrumbs'] == null) {
          this.initializeBC();
        } else {
          this.breadcrumbs = response['breadCrumbs'];
        }
      }
    );

    // this.subscribeRouterEventForBC();
  }

  private subscribeRouterEventForBC() {
    /**
     * Implementing BreadCrumbs via routing
     */
    const ROUTE_DATA_BREADCRUMB: string = 'breadcrumb';
    // subscribe to the NavigationEnd event
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(event => {
        this.initializeBC();
      });
  }

  private initializeBC() {
    // set breadcrumbs
    const root: ActivatedRoute = this.activatedRoute.root;
    // this.breadcrumbs = this.getBreadcrumbs(root);
    this.breadcrumbs = [
      {
        label: 'Home',
        url: ['/home/dashboard']
      }
    ];
    this.breadcrumbs = this.getBreadcrumbs(root);
  }

  /**
   * Returns array of IBreadcrumb objects that represent the breadcrumb
   *
   * @class DetailComponent
   * @method getBreadcrumbs
   * @param {ActivateRoute} route
   * @param {string} url
   * @param {IBreadcrumb[]} breadcrumbs
   */
  private getBreadcrumbs(
    route: ActivatedRoute,
    url: string = '',
    breadcrumbs: IBreadcrumb[] = []
  ): IBreadcrumb[] {
    const ROUTE_DATA_BREADCRUMB: string = 'breadcrumb';

    // get the child routes
    const children: ActivatedRoute[] = route.children;

    // return if there are no more children
    if (children.length === 0) {
      return breadcrumbs;
    }

    // iterate over each children
    for (const child of children) {
      // verify primary route
      if (child.outlet !== PRIMARY_OUTLET) {
        continue;
      }

      // verify the custom data property "breadcrumb" is specified on the route
      if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
        return this.getBreadcrumbs(child, url, breadcrumbs);
      }

      // get the route's URL segment
      const routeURL: string = child.snapshot.url
        .map(segment => segment.path)
        .join('/');

      // append route URL to URL
      url += `/${routeURL}`;

      // add breadcrumb
      const breadcrumb: IBreadcrumb = {
        label: child.snapshot.data[ROUTE_DATA_BREADCRUMB]['title'],
        url: [url],
        disable: child.snapshot.data[ROUTE_DATA_BREADCRUMB]['disable']
      };

      if (child.snapshot.data[ROUTE_DATA_BREADCRUMB]['display'] === true) {
        breadcrumbs.push(breadcrumb);
      }

      // recursive
      return this.getBreadcrumbs(child, url, breadcrumbs);
    }
  }

  /**
   * cancel form and go back
   */
  previous() {
    window.history.back();
  }
  ngOnDestroy() {
    this.pageTitleSubscription.unsubscribe();
  }
}
