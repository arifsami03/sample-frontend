import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PrintService {

  sendDataMethod$: Observable<any>;
  private sendData = new Subject<any>();

    constructor() {
        this.sendDataMethod$ = this.sendData.asObservable();
    }

    sendDataMethod(data) {
        console.log(data); // I have data! Let's return it so subscribers can use it!
        // we can do stuff with data if we want
        this.sendData.next(data);
    }
}
