// Import components and services etc here
import { CommonModule } from '@angular/common';

import {
  MatSidenavModule,
  MatToolbarModule,
  MatMenuModule,
  MatListModule,
  MatButtonModule,
  MatTooltipModule,
  MatIconModule,
  MatCheckboxModule,
  MatProgressSpinnerModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutService,PrintService} from './services';
import { DataTableService } from '../shared/services';

import {
  LayoutComponent,
  DefaultLayoutComponent,
  HeaderComponent,
  FooterComponent,
  LeftNavComponent,
  SubHeaderComponent,
  SettingsNavComponent,
  PublicLayoutComponent,
  PublicHeaderComponent,
  ClassicLayoutComponent,
  ClassicHeaderComponent,
  ClassicSubHeaderComponent,
  ClassicLeftNavComponent,
  ClassicFooterComponent,
  PostRegistrationHeaderComponent,
  PostRegistrationLayoutComponent,
  FlashMessageComponent,
  SideNavComponent,
  PrintComponent,
  ProspectusPrintComponent
} from './components';

export const __IMPORTS = [
  CommonModule,
  MatSidenavModule,
  MatToolbarModule,
  MatMenuModule,
  MatListModule,
  MatButtonModule,
  MatTooltipModule,
  FlexLayoutModule,
  MatIconModule,
  MatCheckboxModule,
  MatProgressSpinnerModule
];

export const __DECLARATIONS = [
  LayoutComponent,
  DefaultLayoutComponent,
  HeaderComponent,
  FooterComponent,
  LeftNavComponent,
  SettingsNavComponent,
  SubHeaderComponent,
  PublicHeaderComponent,
  PublicLayoutComponent,
  ClassicLayoutComponent,
  ClassicHeaderComponent,
  ClassicSubHeaderComponent,
  ClassicLeftNavComponent,
  ClassicFooterComponent,
  PostRegistrationHeaderComponent,
  PostRegistrationLayoutComponent,
  FlashMessageComponent,
  SideNavComponent,
  PrintComponent,
  ProspectusPrintComponent
];

export const __PROVIDERS = [LayoutService, DataTableService,PrintService];

export const __ENTRY_COMPONENTS = [];
