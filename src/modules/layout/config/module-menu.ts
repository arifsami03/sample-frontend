import { InstituteMenu } from '../../institute/config/institute-menu';
import { CampusMenu } from '../../campus/config/campus-menu';
import { SecurityMenu } from '../../security/config/security-menu';
import { SettingMenu } from '../../settings/config/settings-menu';
import { FeeManagementMenu } from '../../fee-management/config/fee-management-menu';
import { AcademicMenu } from '../../academic/config/academic-menu';
import { TimeTableMenu } from '../../time-table/config/time-table-menu';
import { AdmissionMenu } from '../../admission/config/admission-menu';

export const ModuleMenu = [
  { name: 'institute', title: 'Institute', url: ['/institute/dashboard'], icon: '', moduleMenu: InstituteMenu },
  { name: 'campus', title: 'Campus', url: ['/campus'], icon: '', moduleMenu: CampusMenu, },
  { name: 'academic', title: 'Academic', url: ['/academic'], icon: '', moduleMenu: AcademicMenu },
  { name: 'fee-management', title: 'Fee Management', url: ['/fee-management'], icon: '', moduleMenu: FeeManagementMenu },
  { name: 'time-table', title: 'Time Table', url: ['/time-table'], icon: '', moduleMenu: TimeTableMenu },
  { name: 'admission', title: 'Admission', url: ['/admission'], icon: '', moduleMenu: AdmissionMenu },
  { name: 'security', title: 'Security', url: ['/security'], icon: '', moduleMenu: SecurityMenu },
  { name: 'settings', title: 'Settings', url: ['/settings'], icon: '', moduleMenu: SettingMenu }
];
